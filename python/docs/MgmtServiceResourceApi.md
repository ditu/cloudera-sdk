# cm_client.MgmtServiceResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auto_assign_roles**](MgmtServiceResourceApi.md#auto_assign_roles) | **PUT** /cm/service/autoAssignRoles | Automatically assign roles to hosts and create the roles for the Cloudera Management Service.
[**auto_configure**](MgmtServiceResourceApi.md#auto_configure) | **PUT** /cm/service/autoConfigure | Automatically configures roles of the Cloudera Management Service.
[**delete_cms**](MgmtServiceResourceApi.md#delete_cms) | **DELETE** /cm/service | Delete the Cloudera Management Services.
[**enter_maintenance_mode**](MgmtServiceResourceApi.md#enter_maintenance_mode) | **POST** /cm/service/commands/enterMaintenanceMode | Put Cloudera Management Service into maintenance mode.
[**exit_maintenance_mode**](MgmtServiceResourceApi.md#exit_maintenance_mode) | **POST** /cm/service/commands/exitMaintenanceMode | Take Cloudera Management Service out of maintenance mode.
[**list_active_commands**](MgmtServiceResourceApi.md#list_active_commands) | **GET** /cm/service/commands | List active Cloudera Management Services commands.
[**list_role_types**](MgmtServiceResourceApi.md#list_role_types) | **GET** /cm/service/roleTypes | List the supported role types for the Cloudera Management Services.
[**read_service**](MgmtServiceResourceApi.md#read_service) | **GET** /cm/service | Retrieve information about the Cloudera Management Services.
[**read_service_config**](MgmtServiceResourceApi.md#read_service_config) | **GET** /cm/service/config | Retrieve the configuration of the Cloudera Management Services.
[**restart_command**](MgmtServiceResourceApi.md#restart_command) | **POST** /cm/service/commands/restart | Restart the Cloudera Management Services.
[**setup_cms**](MgmtServiceResourceApi.md#setup_cms) | **PUT** /cm/service | Setup the Cloudera Management Services.
[**start_command**](MgmtServiceResourceApi.md#start_command) | **POST** /cm/service/commands/start | Start the Cloudera Management Services.
[**stop_command**](MgmtServiceResourceApi.md#stop_command) | **POST** /cm/service/commands/stop | Stop the Cloudera Management Services.
[**update_service_config**](MgmtServiceResourceApi.md#update_service_config) | **PUT** /cm/service/config | Update the Cloudera Management Services configuration.


<a name="auto_assign_roles" id="auto_assign_roles"></a>
# **auto_assign_roles**
> auto_assign_roles()

Automatically assign roles to hosts and create the roles for the Cloudera Management Service.

Automatically assign roles to hosts and create the roles for the Cloudera Management Service. <p> Assignments are done based on number of hosts in the deployment and hardware specifications. If no hosts are part of the deployment, an exception will be thrown preventing any role assignments. Existing roles will be taken into account and their assignments will be not be modified. The deployment should not have any clusters when calling this endpoint. If it does, an exception will be thrown preventing any role assignments. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Automatically assign roles to hosts and create the roles for the Cloudera Management Service.
    api_instance.auto_assign_roles()
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->auto_assign_roles: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="auto_configure" id="auto_configure"></a>
# **auto_configure**
> auto_configure()

Automatically configures roles of the Cloudera Management Service.

Automatically configures roles of the Cloudera Management Service. <p> Overwrites some existing configurations. Only default role config groups must exist before calling this endpoint. Other role config groups must not exist. If they do, an exception will be thrown preventing any configuration. Ignores any clusters (and their services and roles) colocated with the Cloudera Management Service. To avoid over-committing the heap on hosts, place the Cloudera Management Service roles on machines not used by any of the clusters. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Automatically configures roles of the Cloudera Management Service.
    api_instance.auto_configure()
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->auto_configure: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_cms" id="delete_cms"></a>
# **delete_cms**
> ApiService delete_cms()

Delete the Cloudera Management Services.

Delete the Cloudera Management Services. <p> This method will fail if a CMS instance doesn't already exist.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Delete the Cloudera Management Services.
    api_response = api_instance.delete_cms()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->delete_cms: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enter_maintenance_mode" id="enter_maintenance_mode"></a>
# **enter_maintenance_mode**
> ApiCommand enter_maintenance_mode()

Put Cloudera Management Service into maintenance mode.

Put Cloudera Management Service into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v18.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Put Cloudera Management Service into maintenance mode.
    api_response = api_instance.enter_maintenance_mode()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->enter_maintenance_mode: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="exit_maintenance_mode" id="exit_maintenance_mode"></a>
# **exit_maintenance_mode**
> ApiCommand exit_maintenance_mode()

Take Cloudera Management Service out of maintenance mode.

Take Cloudera Management Service out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v18.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Take Cloudera Management Service out of maintenance mode.
    api_response = api_instance.exit_maintenance_mode()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->exit_maintenance_mode: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_active_commands" id="list_active_commands"></a>
# **list_active_commands**
> ApiCommandList list_active_commands(view=view)

List active Cloudera Management Services commands.

List active Cloudera Management Services commands.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List active Cloudera Management Services commands.
    api_response = api_instance.list_active_commands(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->list_active_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_role_types" id="list_role_types"></a>
# **list_role_types**
> ApiRoleTypeList list_role_types()

List the supported role types for the Cloudera Management Services.

List the supported role types for the Cloudera Management Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # List the supported role types for the Cloudera Management Services.
    api_response = api_instance.list_role_types()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->list_role_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleTypeList**](ApiRoleTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_service" id="read_service"></a>
# **read_service**
> ApiService read_service(view=view)

Retrieve information about the Cloudera Management Services.

Retrieve information about the Cloudera Management Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    # Retrieve information about the Cloudera Management Services.
    api_response = api_instance.read_service(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->read_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_service_config" id="read_service_config"></a>
# **read_service_config**
> ApiServiceConfig read_service_config(view=view)

Retrieve the configuration of the Cloudera Management Services.

Retrieve the configuration of the Cloudera Management Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Retrieve the configuration of the Cloudera Management Services.
    api_response = api_instance.read_service_config(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->read_service_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="restart_command" id="restart_command"></a>
# **restart_command**
> ApiCommand restart_command()

Restart the Cloudera Management Services.

Restart the Cloudera Management Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Restart the Cloudera Management Services.
    api_response = api_instance.restart_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->restart_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="setup_cms" id="setup_cms"></a>
# **setup_cms**
> ApiService setup_cms(body=body)

Setup the Cloudera Management Services.

Setup the Cloudera Management Services. <p> Configure the CMS instance and create all the management roles. The provided configuration data can be used to set up host mappings for each role, and required configuration such as database connection information for specific roles. <p> Regardless of the list of roles provided in the input data, all management roles are created by this call. The input is used to override any default settings for the specific roles. <p> This method needs a valid CM license to be installed beforehand. <p> This method does not start any services or roles. <p> This method will fail if a CMS instance already exists. <p> Available role types: <ul> <li>SERVICEMONITOR</li> <li>ACTIVITYMONITOR</li> <li>HOSTMONITOR</li> <li>REPORTSMANAGER</li> <li>EVENTSERVER</li> <li>ALERTPUBLISHER</li> <li>NAVIGATOR</li> <li>NAVIGATORMETASERVER</li> </ul>  <p/> REPORTSMANAGER, NAVIGATOR and NAVIGATORMETASERVER are only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiService() # ApiService | Role configuration overrides. (optional)

try:
    # Setup the Cloudera Management Services.
    api_response = api_instance.setup_cms(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->setup_cms: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiService**](ApiService.md)| Role configuration overrides. | [optional] 

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_command" id="start_command"></a>
# **start_command**
> ApiCommand start_command()

Start the Cloudera Management Services.

Start the Cloudera Management Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Start the Cloudera Management Services.
    api_response = api_instance.start_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->start_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stop_command" id="stop_command"></a>
# **stop_command**
> ApiCommand stop_command()

Stop the Cloudera Management Services.

Stop the Cloudera Management Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Stop the Cloudera Management Services.
    api_response = api_instance.stop_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->stop_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_service_config" id="update_service_config"></a>
# **update_service_config**
> ApiServiceConfig update_service_config(message=message, body=body)

Update the Cloudera Management Services configuration.

Update the Cloudera Management Services configuration. <p> If a value is set in the given configuration, it will be added to the service's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtServiceResourceApi(cm_client.ApiClient(configuration))
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiServiceConfig() # ApiServiceConfig | Configuration changes. (optional)

try:
    # Update the Cloudera Management Services configuration.
    api_response = api_instance.update_service_config(message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtServiceResourceApi->update_service_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiServiceConfig**](ApiServiceConfig.md)| Configuration changes. | [optional] 

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

