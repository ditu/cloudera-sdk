# ApiTenantUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenant_name** | **str** | Name of the tenant. | [optional] 
**cpu_utilization_percentage** | **float** | Percentage of CPU resource used by workloads. | [optional] 
**memory_utilization_percentage** | **float** | Percentage of memory used by workloads. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


