# ApiTimeSeriesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time_series** | [**list[ApiTimeSeries]**](ApiTimeSeries.md) | The time series data for this single query response. | [optional] 
**warnings** | **list[str]** | The warnings for this single query response. | [optional] 
**time_series_query** | **str** | The query for this single query response. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


