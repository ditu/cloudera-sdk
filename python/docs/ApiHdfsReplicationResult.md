# ApiHdfsReplicationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**progress** | **float** | The file copy progress percentage. | [optional] 
**throughput** | **float** | The data throughput in KB/s. | [optional] 
**remaining_time** | **float** | The time remaining for mapper phase (seconds). | [optional] 
**estimated_completion_time** | **str** | The estimated completion time for the mapper phase. | [optional] 
**counters** | [**list[ApiHdfsReplicationCounter]**](ApiHdfsReplicationCounter.md) | The counters collected from the replication job. &lt;p/&gt; Starting with API v4, the full list of counters is only available in the full view. | [optional] 
**num_files_dry_run** | **float** | The number of files found to copy. | [optional] 
**num_bytes_dry_run** | **float** | The number of bytes found to copy. | [optional] 
**num_files_expected** | **float** | The number of files expected to be copied. | [optional] 
**num_bytes_expected** | **float** | The number of bytes expected to be copied. | [optional] 
**num_files_copied** | **float** | The number of files actually copied. | [optional] 
**num_bytes_copied** | **float** | The number of bytes actually copied. | [optional] 
**num_files_skipped** | **float** | The number of files that were unchanged and thus skipped during copying. | [optional] 
**num_bytes_skipped** | **float** | The aggregate number of bytes in the skipped files. | [optional] 
**num_files_deleted** | **float** | The number of files deleted since they were present at destination, but missing from source. | [optional] 
**num_files_copy_failed** | **float** | The number of files for which copy failed. | [optional] 
**num_bytes_copy_failed** | **float** | The aggregate number of bytes in the files for which copy failed. | [optional] 
**setup_error** | **str** | The error that happened during job setup, if any. | [optional] 
**job_id** | **str** | Read-only. The MapReduce job ID for the replication job. Available since API v4. &lt;p/&gt; This can be used to query information about the replication job from the MapReduce server where it was executed. Refer to the \&quot;/activities\&quot; resource for services for further details. | [optional] 
**job_details_uri** | **str** | Read-only. The URI (relative to the CM server&#39;s root) where to find the Activity Monitor page for the job. Available since API v4. | [optional] 
**dry_run** | **bool** | Whether this was a dry run. | [optional] 
**snapshotted_dirs** | **list[str]** | The list of directories for which snapshots were taken and used as part of this replication. | [optional] 
**run_as_user** | **str** | Returns run-as user name. Available since API v11. | [optional] 
**run_on_source_as_user** | **str** | Returns run-as user name for source cluster. Available since API v18. | [optional] 
**failed_files** | **list[str]** | The list of files that failed during replication. Available since API v11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


