# ApiActivity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Activity name. | [optional] 
**type** | [**ApiActivityType**](ApiActivityType.md) | Activity type. Whether it&#39;s an MR job, a Pig job, a Hive query, etc. | [optional] 
**parent** | **str** | The name of the parent activity. | [optional] 
**start_time** | **str** | The start time of this activity. | [optional] 
**finish_time** | **str** | The finish time of this activity. | [optional] 
**id** | **str** | Activity id, which is unique within a MapReduce service. | [optional] 
**status** | [**ApiActivityStatus**](ApiActivityStatus.md) | Activity status. | [optional] 
**user** | **str** | The user who submitted this activity. | [optional] 
**group** | **str** | The user-group of this activity. | [optional] 
**input_dir** | **str** | The input data directory of the activity. An HDFS url. | [optional] 
**output_dir** | **str** | The output result directory of the activity. An HDFS url. | [optional] 
**mapper** | **str** | The mapper class. | [optional] 
**combiner** | **str** | The combiner class. | [optional] 
**reducer** | **str** | The reducer class. | [optional] 
**queue_name** | **str** | The scheduler queue this activity is in. | [optional] 
**scheduler_priority** | **str** | The scheduler priority of this activity. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


