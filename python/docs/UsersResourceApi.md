# cm_client.UsersResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_users2**](UsersResourceApi.md#create_users2) | **POST** /users | Creates a list of users.
[**delete_user2**](UsersResourceApi.md#delete_user2) | **DELETE** /users/{userName} | Deletes a user from the system.
[**get_sessions**](UsersResourceApi.md#get_sessions) | **GET** /users/sessions | Return a list of the sessions associated with interactive authenticated users in Cloudera Manager.
[**read_user2**](UsersResourceApi.md#read_user2) | **GET** /users/{userName} | Returns detailed information about a user.
[**read_users2**](UsersResourceApi.md#read_users2) | **GET** /users | Returns a list of the user names configured in the system.
[**update_user2**](UsersResourceApi.md#update_user2) | **PUT** /users/{userName} | Updates the given user&#39;s information.


<a name="create_users2" id="create_users2"></a>
# **create_users2**
> ApiUser2List create_users2(body=body)

Creates a list of users.

Creates a list of users. <p> When creating new users, the <i>password</i> property of each user should be their plain text password. The returned user information will not contain any password information. <p/>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.UsersResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiUser2List() # ApiUser2List | List of users to create. (optional)

try:
    # Creates a list of users.
    api_response = api_instance.create_users2(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersResourceApi->create_users2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiUser2List**](ApiUser2List.md)| List of users to create. | [optional] 

### Return type

[**ApiUser2List**](ApiUser2List.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_user2" id="delete_user2"></a>
# **delete_user2**
> ApiUser2 delete_user2(user_name)

Deletes a user from the system.

Deletes a user from the system. <p/>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.UsersResourceApi(cm_client.ApiClient(configuration))
user_name = 'user_name_example' # str | The name of the user to delete.

try:
    # Deletes a user from the system.
    api_response = api_instance.delete_user2(user_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersResourceApi->delete_user2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_name** | **str**| The name of the user to delete. | 

### Return type

[**ApiUser2**](ApiUser2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_sessions" id="get_sessions"></a>
# **get_sessions**
> ApiUserSessionList get_sessions()

Return a list of the sessions associated with interactive authenticated users in Cloudera Manager.

Return a list of the sessions associated with interactive authenticated users in Cloudera Manager. <p> Note that these sessions are only associated with users who log into the web interface. API users will not appear.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.UsersResourceApi(cm_client.ApiClient(configuration))

try:
    # Return a list of the sessions associated with interactive authenticated users in Cloudera Manager.
    api_response = api_instance.get_sessions()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersResourceApi->get_sessions: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiUserSessionList**](ApiUserSessionList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_user2" id="read_user2"></a>
# **read_user2**
> ApiUser2 read_user2(user_name)

Returns detailed information about a user.

Returns detailed information about a user.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.UsersResourceApi(cm_client.ApiClient(configuration))
user_name = 'user_name_example' # str | The user to read.

try:
    # Returns detailed information about a user.
    api_response = api_instance.read_user2(user_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersResourceApi->read_user2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_name** | **str**| The user to read. | 

### Return type

[**ApiUser2**](ApiUser2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_users2" id="read_users2"></a>
# **read_users2**
> ApiUser2List read_users2(view=view)

Returns a list of the user names configured in the system.

Returns a list of the user names configured in the system.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.UsersResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    # Returns a list of the user names configured in the system.
    api_response = api_instance.read_users2(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersResourceApi->read_users2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiUser2List**](ApiUser2List.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_user2" id="update_user2"></a>
# **update_user2**
> ApiUser2 update_user2(user_name, body=body)

Updates the given user's information.

Updates the given user's information. Note that the user's name cannot be changed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.UsersResourceApi(cm_client.ApiClient(configuration))
user_name = 'user_name_example' # str | User name being updated.
body = cm_client.ApiUser2() # ApiUser2 | The user information. (optional)

try:
    # Updates the given user's information.
    api_response = api_instance.update_user2(user_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersResourceApi->update_user2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_name** | **str**| User name being updated. | 
 **body** | [**ApiUser2**](ApiUser2.md)| The user information. | [optional] 

### Return type

[**ApiUser2**](ApiUser2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

