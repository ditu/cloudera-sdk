# ApiImpalaCancelResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warning** | **str** | The warning response. If there was no warning this will be null. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


