# ApiHBaseSnapshotError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**table_name** | **str** | Name of the table. | [optional] 
**snapshot_name** | **str** | Name of the snapshot. | [optional] 
**storage** | [**Storage**](Storage.md) | The location of the snapshot. | [optional] 
**error** | **str** | Description of the error. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


