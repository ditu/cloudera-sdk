# ApiImpalaQueryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**queries** | [**list[ApiImpalaQuery]**](ApiImpalaQuery.md) | The list of queries for this response. | [optional] 
**warnings** | **list[str]** | This list of warnings for this response. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


