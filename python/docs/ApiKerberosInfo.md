# ApiKerberosInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kerberized** | **bool** |  | [optional] 
**kdc_type** | **str** |  | [optional] 
**kerberos_realm** | **str** |  | [optional] 
**kdc_host** | **str** |  | [optional] 
**admin_host** | **str** |  | [optional] 
**domain** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


