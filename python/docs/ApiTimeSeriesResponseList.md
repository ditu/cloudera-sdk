# ApiTimeSeriesResponseList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[ApiTimeSeriesResponse]**](ApiTimeSeriesResponse.md) | The list of responses for this query response list. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


