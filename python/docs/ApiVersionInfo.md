# ApiVersionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **str** | Version. | [optional] 
**snapshot** | **bool** | Whether this build is a development snapshot. | [optional] 
**build_user** | **str** | The user performing the build. | [optional] 
**build_timestamp** | **str** | Build timestamp. | [optional] 
**git_hash** | **str** | Source control management hash. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


