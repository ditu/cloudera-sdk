# ApiHdfsUsageReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_update_time** | **str** | The time when HDFS usage info was last collected. No information beyond this time can be provided. | [optional] 
**items** | [**list[ApiHdfsUsageReportRow]**](ApiHdfsUsageReportRow.md) | A list of per-user usage information at the requested time granularity. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


