# ApiParcelUsageRack

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hosts** | [**list[ApiParcelUsageHost]**](ApiParcelUsageHost.md) | A collection of the hosts in the rack. | [optional] 
**rack_id** | **str** | The rack ID for the rack. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


