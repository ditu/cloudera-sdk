# ApiTimeSeriesEntityAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the of the attribute. This name uniquely identifies this attribute. | [optional] 
**display_name** | **str** | Display name of the attribute. | [optional] 
**description** | **str** | Description of the attribute. | [optional] 
**is_value_case_sensitive** | **bool** | Returns whether to treat attribute values as case-sensitive. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


