# ApiSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | The schedule id. | [optional] 
**display_name** | **str** | The schedule display name. | [optional] 
**description** | **str** | The schedule description. | [optional] 
**start_time** | **str** | The time at which the scheduled activity is triggered for the first time. | [optional] 
**end_time** | **str** | The time after which the scheduled activity will no longer be triggered. | [optional] 
**interval** | **float** | The duration between consecutive triggers of a scheduled activity. | [optional] 
**interval_unit** | [**ApiScheduleInterval**](ApiScheduleInterval.md) | The unit for the repeat interval. | [optional] 
**next_run** | **str** | Readonly. The time the scheduled command will run next. | [optional] 
**paused** | **bool** | The paused state for the schedule. The scheduled activity will not be triggered as long as the scheduled is paused. | [optional] 
**alert_on_start** | **bool** | Whether to alert on start of the scheduled activity. | [optional] 
**alert_on_success** | **bool** | Whether to alert on successful completion of the scheduled activity. | [optional] 
**alert_on_fail** | **bool** | Whether to alert on failure of the scheduled activity. | [optional] 
**alert_on_abort** | **bool** | Whether to alert on abort of the scheduled activity. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


