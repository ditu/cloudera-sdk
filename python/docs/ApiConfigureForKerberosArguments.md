# ApiConfigureForKerberosArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**datanode_transceiver_port** | **float** | The HDFS DataNode transceiver port to use. This will be applied to all DataNode role configuration groups. If not specified, this will default to 1004. | [optional] 
**datanode_web_port** | **float** | The HDFS DataNode web port to use.  This will be applied to all DataNode role configuration groups. If not specified, this will default to 1006. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


