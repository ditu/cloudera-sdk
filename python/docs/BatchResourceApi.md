# cm_client.BatchResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**execute**](BatchResourceApi.md#execute) | **POST** /batch | Executes a batch of API requests in one database transaction.


<a name="execute" id="execute"></a>
# **execute**
> ApiBatchResponse execute(body=body)

Executes a batch of API requests in one database transaction.

Executes a batch of API requests in one database transaction. If any request fails, execution halts and the transaction is rolled back.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.BatchResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiBatchRequest() # ApiBatchRequest | Batch of request to execute. (optional)

try:
    # Executes a batch of API requests in one database transaction.
    api_response = api_instance.execute(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BatchResourceApi->execute: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiBatchRequest**](ApiBatchRequest.md)| Batch of request to execute. | [optional] 

### Return type

[**ApiBatchResponse**](ApiBatchResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

