# cm_client.ReplicationsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**collect_diagnostic_data**](ReplicationsResourceApi.md#collect_diagnostic_data) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/collectDiagnosticData | Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.
[**create_schedules**](ReplicationsResourceApi.md#create_schedules) | **POST** /clusters/{clusterName}/services/{serviceName}/replications | Creates one or more replication schedules.
[**delete_all_schedules**](ReplicationsResourceApi.md#delete_all_schedules) | **DELETE** /clusters/{clusterName}/services/{serviceName}/replications | Deletes all existing replication schedules.
[**delete_schedule**](ReplicationsResourceApi.md#delete_schedule) | **DELETE** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Deletes an existing replication schedule.
[**get_replication_state**](ReplicationsResourceApi.md#get_replication_state) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/replicationState | returns the replication state.
[**read_history**](ReplicationsResourceApi.md#read_history) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/history | Returns a list of commands triggered by a schedule.
[**read_schedule**](ReplicationsResourceApi.md#read_schedule) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Returns information for a specific replication schedule.
[**read_schedules**](ReplicationsResourceApi.md#read_schedules) | **GET** /clusters/{clusterName}/services/{serviceName}/replications | Returns information for all replication schedules.
[**run_copy_listing**](ReplicationsResourceApi.md#run_copy_listing) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/hdfsCopyListing | Run the hdfs copy listing command.
[**run_schedule**](ReplicationsResourceApi.md#run_schedule) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/run | Run the schedule immediately.
[**update_schedule**](ReplicationsResourceApi.md#update_schedule) | **PUT** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Updates an existing replication schedule.


<a name="collect_diagnostic_data" id="collect_diagnostic_data"></a>
# **collect_diagnostic_data**
> ApiCommand collect_diagnostic_data(cluster_name, schedule_id, service_name, view=view, body=body)

Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.

Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.  The returned command's resultDataUrl property, upon the commands completion, will refer to the generated diagnostic data. Available since API v11.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
schedule_id = 3.4 # float | Schedule ID
service_name = 'service_name_example' # str | The service name.
view = 'summary' # str | view to materialize (optional) (default to summary)
body = cm_client.ApiReplicationDiagnosticsCollectionArgs() # ApiReplicationDiagnosticsCollectionArgs | Replication collection arguments (optional)

try:
    # Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.
    api_response = api_instance.collect_diagnostic_data(cluster_name, schedule_id, service_name, view=view, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->collect_diagnostic_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **schedule_id** | **float**| Schedule ID | 
 **service_name** | **str**| The service name. | 
 **view** | **str**| view to materialize | [optional] [default to summary]
 **body** | [**ApiReplicationDiagnosticsCollectionArgs**](ApiReplicationDiagnosticsCollectionArgs.md)| Replication collection arguments | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_schedules" id="create_schedules"></a>
# **create_schedules**
> ApiReplicationScheduleList create_schedules(cluster_name, service_name, body=body)

Creates one or more replication schedules.

Creates one or more replication schedules. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
body = cm_client.ApiReplicationScheduleList() # ApiReplicationScheduleList | List of the replication schedules to create. (optional)

try:
    # Creates one or more replication schedules.
    api_response = api_instance.create_schedules(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->create_schedules: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **body** | [**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)| List of the replication schedules to create. | [optional] 

### Return type

[**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_all_schedules" id="delete_all_schedules"></a>
# **delete_all_schedules**
> ApiReplicationScheduleList delete_all_schedules(cluster_name, service_name)

Deletes all existing replication schedules.

Deletes all existing replication schedules. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.

try:
    # Deletes all existing replication schedules.
    api_response = api_instance.delete_all_schedules(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->delete_all_schedules: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_schedule" id="delete_schedule"></a>
# **delete_schedule**
> ApiReplicationSchedule delete_schedule(cluster_name, schedule_id, service_name)

Deletes an existing replication schedule.

Deletes an existing replication schedule. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
schedule_id = 3.4 # float | Id of an existing replication schedule.
service_name = 'service_name_example' # str | The service name.

try:
    # Deletes an existing replication schedule.
    api_response = api_instance.delete_schedule(cluster_name, schedule_id, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->delete_schedule: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **schedule_id** | **float**| Id of an existing replication schedule. | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiReplicationSchedule**](ApiReplicationSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_replication_state" id="get_replication_state"></a>
# **get_replication_state**
> ApiReplicationState get_replication_state(cluster_name, service_name, view=view)

returns the replication state.

returns the replication state. for example if incremental export is enabled, etc

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
view = 'summary' # str | view to materialize (optional) (default to summary)

try:
    # returns the replication state.
    api_response = api_instance.get_replication_state(cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->get_replication_state: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **view** | **str**| view to materialize | [optional] [default to summary]

### Return type

[**ApiReplicationState**](ApiReplicationState.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_history" id="read_history"></a>
# **read_history**
> ApiReplicationCommandList read_history(cluster_name, schedule_id, service_name, limit=limit, offset=offset, view=view)

Returns a list of commands triggered by a schedule.

Returns a list of commands triggered by a schedule.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
schedule_id = 3.4 # float | Id of an existing replication schedule.
service_name = 'service_name_example' # str | The service name.
limit = 20 # int | Maximum number of commands to retrieve. (optional) (default to 20)
offset = 0 # int | Index of first command to retrieve. (optional) (default to 0)
view = 'summary' # str | The view to materialize. (optional) (default to summary)

try:
    # Returns a list of commands triggered by a schedule.
    api_response = api_instance.read_history(cluster_name, schedule_id, service_name, limit=limit, offset=offset, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->read_history: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **schedule_id** | **float**| Id of an existing replication schedule. | 
 **service_name** | **str**| The service name. | 
 **limit** | **int**| Maximum number of commands to retrieve. | [optional] [default to 20]
 **offset** | **int**| Index of first command to retrieve. | [optional] [default to 0]
 **view** | **str**| The view to materialize. | [optional] [default to summary]

### Return type

[**ApiReplicationCommandList**](ApiReplicationCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_schedule" id="read_schedule"></a>
# **read_schedule**
> ApiReplicationSchedule read_schedule(cluster_name, schedule_id, service_name, view=view)

Returns information for a specific replication schedule.

Returns information for a specific replication schedule. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
schedule_id = 3.4 # float | Id of an existing replication schedule.
service_name = 'service_name_example' # str | The service name.
view = 'summary' # str | The view to materialize. (optional) (default to summary)

try:
    # Returns information for a specific replication schedule.
    api_response = api_instance.read_schedule(cluster_name, schedule_id, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->read_schedule: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **schedule_id** | **float**| Id of an existing replication schedule. | 
 **service_name** | **str**| The service name. | 
 **view** | **str**| The view to materialize. | [optional] [default to summary]

### Return type

[**ApiReplicationSchedule**](ApiReplicationSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_schedules" id="read_schedules"></a>
# **read_schedules**
> ApiReplicationScheduleList read_schedules(cluster_name, service_name, limits=limits)

Returns information for all replication schedules.

Returns information for all replication schedules. <p> Available since API v11.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
limits = 'limits_example' # str | Various limits on contents of returned schedules. (optional)

try:
    # Returns information for all replication schedules.
    api_response = api_instance.read_schedules(cluster_name, service_name, limits=limits)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->read_schedules: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **limits** | **str**| Various limits on contents of returned schedules. | [optional] 

### Return type

[**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="run_copy_listing" id="run_copy_listing"></a>
# **run_copy_listing**
> ApiCommand run_copy_listing(cluster_name, service_name, body=body)

Run the hdfs copy listing command.

Run the hdfs copy listing command <p> The copy listing command will be triggered with the provided arguments <p> Available since API v18. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
body = 'body_example' # str |  (optional)

try:
    # Run the hdfs copy listing command.
    api_response = api_instance.run_copy_listing(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->run_copy_listing: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **body** | **str**|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="run_schedule" id="run_schedule"></a>
# **run_schedule**
> ApiCommand run_schedule(cluster_name, schedule_id, service_name, dry_run=dry_run)

Run the schedule immediately.

Run the schedule immediately. <p> The replication command will be triggered with the configured arguments, and will be recorded in the schedule's history. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
schedule_id = 3.4 # float | Id of an existing replication schedule.
service_name = 'service_name_example' # str | The service name.
dry_run = false # bool | Whether to execute a dry run. (optional) (default to false)

try:
    # Run the schedule immediately.
    api_response = api_instance.run_schedule(cluster_name, schedule_id, service_name, dry_run=dry_run)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->run_schedule: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **schedule_id** | **float**| Id of an existing replication schedule. | 
 **service_name** | **str**| The service name. | 
 **dry_run** | **bool**| Whether to execute a dry run. | [optional] [default to false]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_schedule" id="update_schedule"></a>
# **update_schedule**
> ApiReplicationSchedule update_schedule(cluster_name, schedule_id, service_name, body=body)

Updates an existing replication schedule.

Updates an existing replication schedule. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ReplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
schedule_id = 3.4 # float | Id of an existing replication schedule.
service_name = 'service_name_example' # str | The service name.
body = cm_client.ApiReplicationSchedule() # ApiReplicationSchedule |  (optional)

try:
    # Updates an existing replication schedule.
    api_response = api_instance.update_schedule(cluster_name, schedule_id, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReplicationsResourceApi->update_schedule: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **schedule_id** | **float**| Id of an existing replication schedule. | 
 **service_name** | **str**| The service name. | 
 **body** | [**ApiReplicationSchedule**](ApiReplicationSchedule.md)|  | [optional] 

### Return type

[**ApiReplicationSchedule**](ApiReplicationSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

