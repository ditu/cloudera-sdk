# ApiClusterRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cluster_name** | **str** | The name of the cluster, which uniquely identifies it in a CM installation. | [optional] 
**display_name** | **str** | The display name of the cluster. This is available from v30. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


