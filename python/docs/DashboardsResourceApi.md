# cm_client.DashboardsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_dashboards**](DashboardsResourceApi.md#create_dashboards) | **POST** /timeseries/dashboards | Creates the list of dashboards.
[**delete_dashboard**](DashboardsResourceApi.md#delete_dashboard) | **DELETE** /timeseries/dashboards/{dashboardName} | Deletes a dashboard.
[**get_dashboard**](DashboardsResourceApi.md#get_dashboard) | **GET** /timeseries/dashboards/{dashboardName} | Returns a dashboard definition for the specified name.
[**get_dashboards**](DashboardsResourceApi.md#get_dashboards) | **GET** /timeseries/dashboards | Returns the list of all user-customized dashboards.


<a name="create_dashboards" id="create_dashboards"></a>
# **create_dashboards**
> ApiDashboardList create_dashboards(body=body)

Creates the list of dashboards.

Creates the list of dashboards. If any of the dashboards already exist this whole command will fail and no dashboards will be created. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.DashboardsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiDashboardList() # ApiDashboardList | The list of dashboards to create. (optional)

try:
    # Creates the list of dashboards.
    api_response = api_instance.create_dashboards(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DashboardsResourceApi->create_dashboards: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiDashboardList**](ApiDashboardList.md)| The list of dashboards to create. | [optional] 

### Return type

[**ApiDashboardList**](ApiDashboardList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_dashboard" id="delete_dashboard"></a>
# **delete_dashboard**
> ApiDashboard delete_dashboard(dashboard_name)

Deletes a dashboard.

Deletes a dashboard.  <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.DashboardsResourceApi(cm_client.ApiClient(configuration))
dashboard_name = 'dashboard_name_example' # str | The name of the dashboard.

try:
    # Deletes a dashboard.
    api_response = api_instance.delete_dashboard(dashboard_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DashboardsResourceApi->delete_dashboard: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboard_name** | **str**| The name of the dashboard. | 

### Return type

[**ApiDashboard**](ApiDashboard.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_dashboard" id="get_dashboard"></a>
# **get_dashboard**
> ApiDashboard get_dashboard(dashboard_name)

Returns a dashboard definition for the specified name.

Returns a dashboard definition for the specified name. This dashboard can be imported with the createDashboards API. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.DashboardsResourceApi(cm_client.ApiClient(configuration))
dashboard_name = 'dashboard_name_example' # str | The name of the dashboard.

try:
    # Returns a dashboard definition for the specified name.
    api_response = api_instance.get_dashboard(dashboard_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DashboardsResourceApi->get_dashboard: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboard_name** | **str**| The name of the dashboard. | 

### Return type

[**ApiDashboard**](ApiDashboard.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_dashboards" id="get_dashboards"></a>
# **get_dashboards**
> ApiDashboardList get_dashboards()

Returns the list of all user-customized dashboards.

Returns the list of all user-customized dashboards. This includes both the new dashboards created by users as well as any user customizations to built-in dashboards. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.DashboardsResourceApi(cm_client.ApiClient(configuration))

try:
    # Returns the list of all user-customized dashboards.
    api_response = api_instance.get_dashboards()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DashboardsResourceApi->get_dashboards: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiDashboardList**](ApiDashboardList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

