# ApiHdfsHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_name** | **str** | Name of the active NameNode. | [optional] 
**active_shared_edits_path** | **str** | Path to the shared edits directory on the active NameNode&#39;s host. Ignored if Quorum-based Storage is being enabled. | [optional] 
**stand_by_name** | **str** | Name of the stand-by Namenode. | [optional] 
**stand_by_shared_edits_path** | **str** | Path to the shared edits directory on the stand-by NameNode&#39;s host. Ignored if Quorum-based Storage is being enabled. | [optional] 
**nameservice** | **str** | Nameservice that identifies the HA pair. | [optional] 
**start_dependent_services** | **bool** | Whether to re-start dependent services. Defaults to true. | [optional] 
**deploy_client_configs** | **bool** | Whether to re-deploy client configurations. Defaults to true. | [optional] 
**enable_quorum_storage** | **bool** | This parameter has been deprecated as of CM 5.0, where HA is only supported using Quorum-based Storage. &lt;p&gt; Whether to enable Quorum-based Storage.  Enabling Quorum-based Storage requires a minimum of three and an odd number of JournalNodes to be created and configured before enabling HDFS HA. &lt;p&gt; Available since API v2. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


