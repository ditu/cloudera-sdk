# ApiScmDbInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scm_db_type** | [**ScmDbType**](ScmDbType.md) | Cloudera Manager server&#39;s db type | [optional] 
**embedded_db_used** | **bool** | Whether Cloudera Manager server is using embedded DB | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


