# ApiParcelUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**racks** | [**list[ApiParcelUsageRack]**](ApiParcelUsageRack.md) | The racks that contain hosts that are part of this cluster. | [optional] 
**parcels** | [**list[ApiParcelUsageParcel]**](ApiParcelUsageParcel.md) | The parcel&#39;s that are activated and/or in-use on this cluster. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


