# cm_client.ExternalAccountsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_account**](ExternalAccountsResourceApi.md#create_account) | **POST** /externalAccounts/create | Create a new external account.
[**delete_account**](ExternalAccountsResourceApi.md#delete_account) | **DELETE** /externalAccounts/delete/{name} | Delete an external account, specifying its name.
[**external_account_command_by_name**](ExternalAccountsResourceApi.md#external_account_command_by_name) | **POST** /externalAccounts/account/{name}/commands/{commandName} | Executes a command on the external account specified by name.
[**get_supported_categories**](ExternalAccountsResourceApi.md#get_supported_categories) | **GET** /externalAccounts/supportedCategories | List of external account categories supported by this Cloudera Manager.
[**get_supported_types**](ExternalAccountsResourceApi.md#get_supported_types) | **GET** /externalAccounts/supportedTypes/{categoryName} | List of external account types supported by this Cloudera Manager by category.
[**list_external_account_commands**](ExternalAccountsResourceApi.md#list_external_account_commands) | **GET** /externalAccounts/typeInfo/{typeName}/commandsByName | Lists all the commands that can be executed by name on the provided external account type.
[**read_account**](ExternalAccountsResourceApi.md#read_account) | **GET** /externalAccounts/account/{name} | Get a single external account by account name.
[**read_account_by_display_name**](ExternalAccountsResourceApi.md#read_account_by_display_name) | **GET** /externalAccounts/accountByDisplayName/{displayName} | Get a single external account by display name.
[**read_accounts**](ExternalAccountsResourceApi.md#read_accounts) | **GET** /externalAccounts/type/{typeName} | Get a list of external accounts for a specific account type.
[**read_config**](ExternalAccountsResourceApi.md#read_config) | **GET** /externalAccounts/account/{name}/config | Get configs of external account for the given account name.
[**update_account**](ExternalAccountsResourceApi.md#update_account) | **PUT** /externalAccounts/update | Update an external account.
[**update_config**](ExternalAccountsResourceApi.md#update_config) | **PUT** /externalAccounts/account/{name}/config | Upadate configs of external account for the given account name.


<a name="create_account" id="create_account"></a>
# **create_account**
> ApiExternalAccount create_account(body=body)

Create a new external account.

Create a new external account. Account names and display names must be unique, i.e. they must not share names or display names with an existing account. Server generates an account ID for the requested account.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiExternalAccount() # ApiExternalAccount |  (optional)

try:
    # Create a new external account.
    api_response = api_instance.create_account(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->create_account: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiExternalAccount**](ApiExternalAccount.md)|  | [optional] 

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_account" id="delete_account"></a>
# **delete_account**
> ApiExternalAccount delete_account(name)

Delete an external account, specifying its name.

Delete an external account, specifying its name.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
name = 'name_example' # str | 

try:
    # Delete an external account, specifying its name.
    api_response = api_instance.delete_account(name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->delete_account: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | 

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="external_account_command_by_name" id="external_account_command_by_name"></a>
# **external_account_command_by_name**
> ApiCommand external_account_command_by_name(command_name, name)

Executes a command on the external account specified by name.

Executes a command on the external account specified by name. <p> Available since API v16.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
command_name = 'command_name_example' # str | The command name.
name = 'name_example' # str | The external account name.

try:
    # Executes a command on the external account specified by name.
    api_response = api_instance.external_account_command_by_name(command_name, name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->external_account_command_by_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_name** | **str**| The command name. | 
 **name** | **str**| The external account name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_supported_categories" id="get_supported_categories"></a>
# **get_supported_categories**
> ApiExternalAccountCategoryList get_supported_categories()

List of external account categories supported by this Cloudera Manager.

List of external account categories supported by this Cloudera Manager.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))

try:
    # List of external account categories supported by this Cloudera Manager.
    api_response = api_instance.get_supported_categories()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->get_supported_categories: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiExternalAccountCategoryList**](ApiExternalAccountCategoryList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_supported_types" id="get_supported_types"></a>
# **get_supported_types**
> ApiExternalAccountTypeList get_supported_types(category_name)

List of external account types supported by this Cloudera Manager by category.

List of external account types supported by this Cloudera Manager by category.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
category_name = 'category_name_example' # str | 

try:
    # List of external account types supported by this Cloudera Manager by category.
    api_response = api_instance.get_supported_types(category_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->get_supported_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_name** | **str**|  | 

### Return type

[**ApiExternalAccountTypeList**](ApiExternalAccountTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_external_account_commands" id="list_external_account_commands"></a>
# **list_external_account_commands**
> ApiCommandMetadataList list_external_account_commands(type_name)

Lists all the commands that can be executed by name on the provided external account type.

Lists all the commands that can be executed by name on the provided external account type. <p> Available since API v16.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
type_name = 'type_name_example' # str | The external account type name

try:
    # Lists all the commands that can be executed by name on the provided external account type.
    api_response = api_instance.list_external_account_commands(type_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->list_external_account_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_name** | **str**| The external account type name | 

### Return type

[**ApiCommandMetadataList**](ApiCommandMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_account" id="read_account"></a>
# **read_account**
> ApiExternalAccount read_account(name, view=view)

Get a single external account by account name.

Get a single external account by account name.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
name = 'name_example' # str | 
view = 'view_example' # str |  (optional)

try:
    # Get a single external account by account name.
    api_response = api_instance.read_account(name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->read_account: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | 
 **view** | **str**|  | [optional] 

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_account_by_display_name" id="read_account_by_display_name"></a>
# **read_account_by_display_name**
> ApiExternalAccount read_account_by_display_name(display_name, view=view)

Get a single external account by display name.

Get a single external account by display name.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
display_name = 'display_name_example' # str | 
view = 'view_example' # str |  (optional)

try:
    # Get a single external account by display name.
    api_response = api_instance.read_account_by_display_name(display_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->read_account_by_display_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **display_name** | **str**|  | 
 **view** | **str**|  | [optional] 

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_accounts" id="read_accounts"></a>
# **read_accounts**
> ApiExternalAccountList read_accounts(type_name, view=view)

Get a list of external accounts for a specific account type.

Get a list of external accounts for a specific account type.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
type_name = 'type_name_example' # str | 
view = 'view_example' # str |  (optional)

try:
    # Get a list of external accounts for a specific account type.
    api_response = api_instance.read_accounts(type_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->read_accounts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_name** | **str**|  | 
 **view** | **str**|  | [optional] 

### Return type

[**ApiExternalAccountList**](ApiExternalAccountList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_config" id="read_config"></a>
# **read_config**
> ApiConfigList read_config(name, view=view)

Get configs of external account for the given account name.

Get configs of external account for the given account name.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
name = 'name_example' # str | The external account name
view = 'summary' # str | The view to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Get configs of external account for the given account name.
    api_response = api_instance.read_config(name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->read_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The external account name | 
 **view** | **str**| The view to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_account" id="update_account"></a>
# **update_account**
> ApiExternalAccount update_account(body=body)

Update an external account.

Update an external account.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiExternalAccount() # ApiExternalAccount |  (optional)

try:
    # Update an external account.
    api_response = api_instance.update_account(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->update_account: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiExternalAccount**](ApiExternalAccount.md)|  | [optional] 

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_config" id="update_config"></a>
# **update_config**
> ApiConfigList update_config(name, message=message, body=body)

Upadate configs of external account for the given account name.

Upadate configs of external account for the given account name.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalAccountsResourceApi(cm_client.ApiClient(configuration))
name = 'name_example' # str | The external account name
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | Settings to update. (optional)

try:
    # Upadate configs of external account for the given account name.
    api_response = api_instance.update_config(name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalAccountsResourceApi->update_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The external account name | 
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| Settings to update. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

