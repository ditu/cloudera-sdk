# cm_client.RoleConfigGroupsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_role_config_groups**](RoleConfigGroupsResourceApi.md#create_role_config_groups) | **POST** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups | Creates new role config groups.
[**delete_role_config_group**](RoleConfigGroupsResourceApi.md#delete_role_config_group) | **DELETE** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Deletes a role config group.
[**move_roles**](RoleConfigGroupsResourceApi.md#move_roles) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/roles | Moves roles to the specified role config group.
[**move_roles_to_base_group**](RoleConfigGroupsResourceApi.md#move_roles_to_base_group) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/roles | Moves roles to the base role config group.
[**read_config**](RoleConfigGroupsResourceApi.md#read_config) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/config | Returns the current revision of the config for the specified role config group.
[**read_role_config_group**](RoleConfigGroupsResourceApi.md#read_role_config_group) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Returns the information for a role config group.
[**read_role_config_groups**](RoleConfigGroupsResourceApi.md#read_role_config_groups) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups | Returns the information for all role config groups for a given cluster and service.
[**read_roles**](RoleConfigGroupsResourceApi.md#read_roles) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/roles | Returns all roles in the given role config group.
[**update_config**](RoleConfigGroupsResourceApi.md#update_config) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/config | Updates the config for the given role config group.
[**update_role_config_group**](RoleConfigGroupsResourceApi.md#update_role_config_group) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Updates an existing role config group.


<a name="create_role_config_groups" id="create_role_config_groups"></a>
# **create_role_config_groups**
> ApiRoleConfigGroupList create_role_config_groups(cluster_name, service_name, body=body)

Creates new role config groups.

Creates new role config groups. It is not allowed to create base groups (base must be set to false.) <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleConfigGroupList() # ApiRoleConfigGroupList | The list of groups to be created. (optional)

try:
    # Creates new role config groups.
    api_response = api_instance.create_role_config_groups(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->create_role_config_groups: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)| The list of groups to be created. | [optional] 

### Return type

[**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_role_config_group" id="delete_role_config_group"></a>
# **delete_role_config_group**
> ApiRoleConfigGroup delete_role_config_group(cluster_name, role_config_group_name, service_name)

Deletes a role config group.

Deletes a role config group. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_config_group_name = 'role_config_group_name_example' # str | The name of the group to delete.
service_name = 'service_name_example' # str | 

try:
    # Deletes a role config group.
    api_response = api_instance.delete_role_config_group(cluster_name, role_config_group_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->delete_role_config_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_config_group_name** | **str**| The name of the group to delete. | 
 **service_name** | **str**|  | 

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="move_roles" id="move_roles"></a>
# **move_roles**
> ApiRoleList move_roles(cluster_name, role_config_group_name, service_name, body=body)

Moves roles to the specified role config group.

Moves roles to the specified role config group.  The roles can be moved from any role config group belonging to the same service. The role type of the destination group must match the role type of the roles. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_config_group_name = 'role_config_group_name_example' # str | The name of the group the roles will be moved to.
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the roles to move. (optional)

try:
    # Moves roles to the specified role config group.
    api_response = api_instance.move_roles(cluster_name, role_config_group_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->move_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_config_group_name** | **str**| The name of the group the roles will be moved to. | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles to move. | [optional] 

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="move_roles_to_base_group" id="move_roles_to_base_group"></a>
# **move_roles_to_base_group**
> ApiRoleList move_roles_to_base_group(cluster_name, service_name, body=body)

Moves roles to the base role config group.

Moves roles to the base role config group.  The roles can be moved from any role config group belonging to the same service. The role type of the roles may vary. Each role will be moved to its corresponding base group depending on its role type. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the roles to move. (optional)

try:
    # Moves roles to the base role config group.
    api_response = api_instance.move_roles_to_base_group(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->move_roles_to_base_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles to move. | [optional] 

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_config" id="read_config"></a>
# **read_config**
> ApiConfigList read_config(cluster_name, role_config_group_name, service_name, view=view)

Returns the current revision of the config for the specified role config group.

Returns the current revision of the config for the specified role config group. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_config_group_name = 'role_config_group_name_example' # str | The name of the role config group.
service_name = 'service_name_example' # str | 
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Returns the current revision of the config for the specified role config group.
    api_response = api_instance.read_config(cluster_name, role_config_group_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->read_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_config_group_name** | **str**| The name of the role config group. | 
 **service_name** | **str**|  | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role_config_group" id="read_role_config_group"></a>
# **read_role_config_group**
> ApiRoleConfigGroup read_role_config_group(cluster_name, role_config_group_name, service_name)

Returns the information for a role config group.

Returns the information for a role config group. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_config_group_name = 'role_config_group_name_example' # str | The name of the requested group.
service_name = 'service_name_example' # str | 

try:
    # Returns the information for a role config group.
    api_response = api_instance.read_role_config_group(cluster_name, role_config_group_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->read_role_config_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_config_group_name** | **str**| The name of the requested group. | 
 **service_name** | **str**|  | 

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role_config_groups" id="read_role_config_groups"></a>
# **read_role_config_groups**
> ApiRoleConfigGroupList read_role_config_groups(cluster_name, service_name)

Returns the information for all role config groups for a given cluster and service.

Returns the information for all role config groups for a given cluster and service. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 

try:
    # Returns the information for all role config groups for a given cluster and service.
    api_response = api_instance.read_role_config_groups(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->read_role_config_groups: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 

### Return type

[**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_roles" id="read_roles"></a>
# **read_roles**
> ApiRoleList read_roles(cluster_name, role_config_group_name, service_name)

Returns all roles in the given role config group.

Returns all roles in the given role config group. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_config_group_name = 'role_config_group_name_example' # str | The name of the role config group.
service_name = 'service_name_example' # str | 

try:
    # Returns all roles in the given role config group.
    api_response = api_instance.read_roles(cluster_name, role_config_group_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->read_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_config_group_name** | **str**| The name of the role config group. | 
 **service_name** | **str**|  | 

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_config" id="update_config"></a>
# **update_config**
> ApiConfigList update_config(cluster_name, role_config_group_name, service_name, message=message, body=body)

Updates the config for the given role config group.

Updates the config for the given role config group.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_config_group_name = 'role_config_group_name_example' # str | The name of the role config group.
service_name = 'service_name_example' # str | 
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | The new config information for the group. (optional)

try:
    # Updates the config for the given role config group.
    api_response = api_instance.update_config(cluster_name, role_config_group_name, service_name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->update_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_config_group_name** | **str**| The name of the role config group. | 
 **service_name** | **str**|  | 
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| The new config information for the group. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_role_config_group" id="update_role_config_group"></a>
# **update_role_config_group**
> ApiRoleConfigGroup update_role_config_group(cluster_name, role_config_group_name, service_name, message=message, body=body)

Updates an existing role config group.

Updates an existing role config group <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_config_group_name = 'role_config_group_name_example' # str | The name of the group to update.
service_name = 'service_name_example' # str | 
message = 'message_example' # str | The optional message describing the changes. (optional)
body = cm_client.ApiRoleConfigGroup() # ApiRoleConfigGroup | The updated role config group. (optional)

try:
    # Updates an existing role config group.
    api_response = api_instance.update_role_config_group(cluster_name, role_config_group_name, service_name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleConfigGroupsResourceApi->update_role_config_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_config_group_name** | **str**| The name of the group to update. | 
 **service_name** | **str**|  | 
 **message** | **str**| The optional message describing the changes. | [optional] 
 **body** | [**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)| The updated role config group. | [optional] 

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

