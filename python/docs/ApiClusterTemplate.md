# ApiClusterTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cdh_version** | **str** |  | [optional] 
**products** | [**list[ApiProductVersion]**](ApiProductVersion.md) |  | [optional] 
**services** | [**list[ApiClusterTemplateService]**](ApiClusterTemplateService.md) |  | [optional] 
**host_templates** | [**list[ApiClusterTemplateHostTemplate]**](ApiClusterTemplateHostTemplate.md) |  | [optional] 
**display_name** | **str** |  | [optional] 
**cm_version** | **str** |  | [optional] 
**instantiator** | [**ApiClusterTemplateInstantiator**](ApiClusterTemplateInstantiator.md) |  | [optional] 
**repositories** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


