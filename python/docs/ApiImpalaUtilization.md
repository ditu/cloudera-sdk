# ApiImpalaUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_queries** | **float** | Total number of queries submitted to Impala. | [optional] 
**successful_queries** | **float** | Number of queries that finished successfully. | [optional] 
**oom_queries** | **float** | Number of queries that failed due to insufficient memory. | [optional] 
**time_out_queries** | **float** | Number of queries that timed out while waiting for resources in a pool. | [optional] 
**rejected_queries** | **float** | Number of queries that were rejected by Impala because the pool was full. | [optional] 
**successful_queries_percentage** | **float** | Percentage of queries that finished successfully. | [optional] 
**oom_queries_percentage** | **float** | Percentage of queries that failed due to insufficient memory. | [optional] 
**time_out_queries_percentage** | **float** | Percentage of queries that timed out while waiting for resources in a pool. | [optional] 
**rejected_queries_percentage** | **float** | Percentage of queries that were rejected by Impala because the pool was full. | [optional] 
**avg_wait_time_in_queue** | **float** | Average time, in milliseconds, spent by a query in an Impala pool while waiting for resources. | [optional] 
**peak_allocation_timestamp_ms** | **float** | The time when Impala reserved the maximum amount of memory for queries. | [optional] 
**max_allocated_memory** | **float** | The maximum memory (in bytes) that was reserved by Impala for executing queries. | [optional] 
**max_allocated_memory_percentage** | **float** | The maximum percentage of memory that was reserved by Impala for executing queries. | [optional] 
**utilized_at_max_allocated** | **float** | The amount of memory (in bytes) used by Impala for running queries at the time when maximum memory was reserved. | [optional] 
**utilized_at_max_allocated_percentage** | **float** | The percentage of memory used by Impala for running queries at the time when maximum memory was reserved. | [optional] 
**peak_usage_timestamp_ms** | **float** | The time when Impala used the maximum amount of memory for queries. | [optional] 
**max_utilized_memory** | **float** | The maximum memory (in bytes) that was used by Impala for executing queries. | [optional] 
**max_utilized_memory_percentage** | **float** | The maximum percentage of memory that was used by Impala for executing queries. | [optional] 
**allocated_at_max_utilized** | **float** | The amount of memory (in bytes) reserved by Impala at the time when it was using the maximum memory for executing queries. | [optional] 
**allocated_at_max_utilized_percentage** | **float** | The percentage of memory reserved by Impala at the time when it was using the maximum memory for executing queries. | [optional] 
**distribution_utilized_by_impala_daemon** | [**ApiImpalaUtilizationHistogram**](ApiImpalaUtilizationHistogram.md) | Distribution of memory used per Impala daemon for executing queries at the time Impala used the maximum memory. | [optional] 
**distribution_allocated_by_impala_daemon** | [**ApiImpalaUtilizationHistogram**](ApiImpalaUtilizationHistogram.md) | Distribution of memory reserved per Impala daemon for executing queries at the time Impala used the maximum memory. | [optional] 
**tenant_utilizations** | [**ApiImpalaTenantUtilizationList**](ApiImpalaTenantUtilizationList.md) | A list of tenant utilization reports. | [optional] 
**error_message** | **str** | error message of utilization report. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


