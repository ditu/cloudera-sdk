# cm_client.ExternalUserMappingsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_external_user_mappings**](ExternalUserMappingsResourceApi.md#create_external_user_mappings) | **POST** /externalUserMappings | Creates a list of external user mappings.
[**delete_external_user_mapping**](ExternalUserMappingsResourceApi.md#delete_external_user_mapping) | **DELETE** /externalUserMappings/{uuid} | Deletes an external user mapping from the system.
[**read_external_user_mapping**](ExternalUserMappingsResourceApi.md#read_external_user_mapping) | **GET** /externalUserMappings/{uuid} | Returns detailed information about an external user mapping.
[**read_external_user_mappings**](ExternalUserMappingsResourceApi.md#read_external_user_mappings) | **GET** /externalUserMappings | Returns a list of the external user mappings configured in the system.
[**update_external_user_mapping**](ExternalUserMappingsResourceApi.md#update_external_user_mapping) | **PUT** /externalUserMappings/{uuid} | Updates the given external user mapping&#39;s information.


<a name="create_external_user_mappings" id="create_external_user_mappings"></a>
# **create_external_user_mappings**
> ApiExternalUserMappingList create_external_user_mappings(body=body)

Creates a list of external user mappings.

Creates a list of external user mappings

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalUserMappingsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiExternalUserMappingList() # ApiExternalUserMappingList | List of external user mappings to create. (optional)

try:
    # Creates a list of external user mappings.
    api_response = api_instance.create_external_user_mappings(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalUserMappingsResourceApi->create_external_user_mappings: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiExternalUserMappingList**](ApiExternalUserMappingList.md)| List of external user mappings to create. | [optional] 

### Return type

[**ApiExternalUserMappingList**](ApiExternalUserMappingList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_external_user_mapping" id="delete_external_user_mapping"></a>
# **delete_external_user_mapping**
> ApiExternalUserMapping delete_external_user_mapping(uuid)

Deletes an external user mapping from the system.

Deletes an external user mapping from the system. <p/>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalUserMappingsResourceApi(cm_client.ApiClient(configuration))
uuid = 'uuid_example' # str | The uuid of the external user mapping to delete.

try:
    # Deletes an external user mapping from the system.
    api_response = api_instance.delete_external_user_mapping(uuid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalUserMappingsResourceApi->delete_external_user_mapping: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| The uuid of the external user mapping to delete. | 

### Return type

[**ApiExternalUserMapping**](ApiExternalUserMapping.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_external_user_mapping" id="read_external_user_mapping"></a>
# **read_external_user_mapping**
> ApiExternalUserMapping read_external_user_mapping(uuid)

Returns detailed information about an external user mapping.

Returns detailed information about an external user mapping.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalUserMappingsResourceApi(cm_client.ApiClient(configuration))
uuid = 'uuid_example' # str | The external user mapping to read.

try:
    # Returns detailed information about an external user mapping.
    api_response = api_instance.read_external_user_mapping(uuid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalUserMappingsResourceApi->read_external_user_mapping: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| The external user mapping to read. | 

### Return type

[**ApiExternalUserMapping**](ApiExternalUserMapping.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_external_user_mappings" id="read_external_user_mappings"></a>
# **read_external_user_mappings**
> ApiExternalUserMappingList read_external_user_mappings(view=view)

Returns a list of the external user mappings configured in the system.

Returns a list of the external user mappings configured in the system.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalUserMappingsResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    # Returns a list of the external user mappings configured in the system.
    api_response = api_instance.read_external_user_mappings(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalUserMappingsResourceApi->read_external_user_mappings: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiExternalUserMappingList**](ApiExternalUserMappingList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_external_user_mapping" id="update_external_user_mapping"></a>
# **update_external_user_mapping**
> ApiExternalUserMapping update_external_user_mapping(uuid, body=body)

Updates the given external user mapping's information.

Updates the given external user mapping's information.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ExternalUserMappingsResourceApi(cm_client.ApiClient(configuration))
uuid = 'uuid_example' # str | Uuid of the external user mapping being updated.
body = cm_client.ApiExternalUserMapping() # ApiExternalUserMapping | The external user mapping information. (optional)

try:
    # Updates the given external user mapping's information.
    api_response = api_instance.update_external_user_mapping(uuid, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExternalUserMappingsResourceApi->update_external_user_mapping: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| Uuid of the external user mapping being updated. | 
 **body** | [**ApiExternalUserMapping**](ApiExternalUserMapping.md)| The external user mapping information. | [optional] 

### Return type

[**ApiExternalUserMapping**](ApiExternalUserMapping.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

