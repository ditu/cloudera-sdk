# ApiHiveReplicationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**database** | **str** | Name of the database. | [optional] 
**table_name** | **str** | Name of the table. | [optional] 
**impala_udf** | **str** | UDF signature, includes the UDF name and parameter types. | [optional] 
**hive_udf** | **str** | Hive UDF signature, includes the UDF name and parameter types. | [optional] 
**error** | **str** | Description of the error. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


