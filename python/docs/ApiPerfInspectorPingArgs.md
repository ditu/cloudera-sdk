# ApiPerfInspectorPingArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ping_timeout_secs** | **float** | Timeout in seconds for the ping request to each target host. If not specified, defaults to 10 seconds. | [optional] 
**ping_count** | **float** | Number of iterations of the ping request to each target host. If not specified, defaults to 10 count. | [optional] 
**ping_packet_size_bytes** | **float** | Packet size in bytes for each ping request. If not specified, defaults to 56 bytes. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


