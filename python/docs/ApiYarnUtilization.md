# ApiYarnUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avg_cpu_utilization** | **float** | Average number of VCores used by YARN applications during the report window. | [optional] 
**max_cpu_utilization** | **float** | Maximum number of VCores used by YARN applications during the report window. | [optional] 
**avg_cpu_daily_peak** | **float** | Average daily peak VCores used by YARN applications during the report window. The number is computed by first finding the maximum resource consumption per day and then taking their mean. | [optional] 
**max_cpu_utilization_timestamp_ms** | **float** | Timestamp corresponds to maximum number of VCores used by YARN applications during the report window. | [optional] 
**avg_cpu_utilization_percentage** | **float** | Average percentage of VCores used by YARN applications during the report window. | [optional] 
**max_cpu_utilization_percentage** | **float** | Maximum percentage of VCores used by YARN applications during the report window. | [optional] 
**avg_cpu_daily_peak_percentage** | **float** | Average daily peak percentage of VCores used by YARN applications during the report window. | [optional] 
**avg_memory_utilization** | **float** | Average memory used by YARN applications during the report window. | [optional] 
**max_memory_utilization** | **float** | Maximum memory used by YARN applications during the report window. | [optional] 
**avg_memory_daily_peak** | **float** | Average daily peak memory used by YARN applications during the report window. The number is computed by first finding the maximum resource consumption per day and then taking their mean. | [optional] 
**max_memory_utilization_timestamp_ms** | **float** | Timestamp corresponds to maximum memory used by YARN applications during the report window. | [optional] 
**avg_memory_utilization_percentage** | **float** | Average percentage memory used by YARN applications during the report window. | [optional] 
**max_memory_utilization_percentage** | **float** | Maximum percentage of memory used by YARN applications during the report window. | [optional] 
**avg_memory_daily_peak_percentage** | **float** | Average daily peak percentage of memory used by YARN applications during the report window. | [optional] 
**tenant_utilizations** | [**ApiYarnTenantUtilizationList**](ApiYarnTenantUtilizationList.md) | A list of tenant utilization reports. | [optional] 
**error_message** | **str** | error message of utilization report. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


