# ApiTimeSeries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**ApiTimeSeriesMetadata**](ApiTimeSeriesMetadata.md) | Metadata for the metric. | [optional] 
**data** | [**list[ApiTimeSeriesData]**](ApiTimeSeriesData.md) | List of metric data points. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


