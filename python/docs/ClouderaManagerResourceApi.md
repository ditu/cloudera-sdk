# cm_client.ClouderaManagerResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**begin_trial**](ClouderaManagerResourceApi.md#begin_trial) | **POST** /cm/trial/begin | Begin trial license.
[**clusters_perf_inspector_command**](ClouderaManagerResourceApi.md#clusters_perf_inspector_command) | **POST** /cm/commands/clustersPerfInspector | Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
[**collect_diagnostic_data_command**](ClouderaManagerResourceApi.md#collect_diagnostic_data_command) | **POST** /cm/commands/collectDiagnosticData | Collect diagnostic data from hosts managed by Cloudera Manager.
[**delete_credentials_command**](ClouderaManagerResourceApi.md#delete_credentials_command) | **POST** /cm/commands/deleteCredentials | Delete existing Kerberos credentials.
[**end_trial**](ClouderaManagerResourceApi.md#end_trial) | **POST** /cm/trial/end | End trial license.
[**generate_credentials_command**](ClouderaManagerResourceApi.md#generate_credentials_command) | **POST** /cm/commands/generateCredentials | Generate missing Kerberos credentials.
[**get_config**](ClouderaManagerResourceApi.md#get_config) | **GET** /cm/config | Retrieve the Cloudera Manager settings.
[**get_deployment2**](ClouderaManagerResourceApi.md#get_deployment2) | **GET** /cm/deployment | Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc.
[**get_kerberos_info**](ClouderaManagerResourceApi.md#get_kerberos_info) | **GET** /cm/kerberosInfo | Provides Cloudera Manager Kerberos information.
[**get_kerberos_principals**](ClouderaManagerResourceApi.md#get_kerberos_principals) | **GET** /cm/kerberosPrincipals | Returns the Kerberos principals needed by the services being managed by Cloudera Manager.
[**get_licensed_feature_usage**](ClouderaManagerResourceApi.md#get_licensed_feature_usage) | **GET** /cm/licensedFeatureUsage | Retrieve a summary of licensed feature usage.
[**get_log**](ClouderaManagerResourceApi.md#get_log) | **GET** /cm/log | Returns the entire contents of the Cloudera Manager log file.
[**get_scm_db_info**](ClouderaManagerResourceApi.md#get_scm_db_info) | **GET** /cm/scmDbInfo | Provides Cloudera Manager server&#39;s database information.
[**get_shutdown_readiness**](ClouderaManagerResourceApi.md#get_shutdown_readiness) | **GET** /cm/shutdownReadiness | Retrieve Cloudera Manager&#39;s readiness for shutdown and destroy.
[**get_version**](ClouderaManagerResourceApi.md#get_version) | **GET** /cm/version | Provides version information of Cloudera Manager itself.
[**host_install_command**](ClouderaManagerResourceApi.md#host_install_command) | **POST** /cm/commands/hostInstall | Perform installation on a set of hosts.
[**hosts_decommission_command**](ClouderaManagerResourceApi.md#hosts_decommission_command) | **POST** /cm/commands/hostsDecommission | Decommission the given hosts.
[**hosts_offline_or_decommission_command**](ClouderaManagerResourceApi.md#hosts_offline_or_decommission_command) | **POST** /cm/commands/hostsOfflineOrDecommission | Decommission the given hosts.
[**hosts_perf_inspector_command**](ClouderaManagerResourceApi.md#hosts_perf_inspector_command) | **POST** /cm/commands/hostsPerfInspector | Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
[**hosts_recommission_and_exit_maintenance_mode_command**](ClouderaManagerResourceApi.md#hosts_recommission_and_exit_maintenance_mode_command) | **POST** /cm/commands/hostsRecommissionAndExitMaintenanceMode | Recommission and exit maintenance on the given hosts.
[**hosts_recommission_command**](ClouderaManagerResourceApi.md#hosts_recommission_command) | **POST** /cm/commands/hostsRecommission | Recommission the given hosts.
[**hosts_recommission_with_start_command**](ClouderaManagerResourceApi.md#hosts_recommission_with_start_command) | **POST** /cm/commands/hostsRecommissionWithStart | Recommission the given hosts.
[**hosts_start_roles_command**](ClouderaManagerResourceApi.md#hosts_start_roles_command) | **POST** /cm/commands/hostsStartRoles | Start all the roles on the given hosts.
[**import_admin_credentials**](ClouderaManagerResourceApi.md#import_admin_credentials) | **POST** /cm/commands/importAdminCredentials | Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.
[**import_cluster_template**](ClouderaManagerResourceApi.md#import_cluster_template) | **POST** /cm/importClusterTemplate | Create cluster as per the given cluster template.
[**import_kerberos_principal**](ClouderaManagerResourceApi.md#import_kerberos_principal) | **POST** /cm/commands/importKerberosPrincipal | Imports the Kerberos credentials for the specified principal which can then be used to add to a role&#39;s keytab by running Generate Credentials command.
[**inspect_hosts_command**](ClouderaManagerResourceApi.md#inspect_hosts_command) | **POST** /cm/commands/inspectHosts | Runs the host inspector on the configured hosts.
[**list_active_commands**](ClouderaManagerResourceApi.md#list_active_commands) | **GET** /cm/commands | List active global commands.
[**read_license**](ClouderaManagerResourceApi.md#read_license) | **GET** /cm/license | Retrieve information about the Cloudera Manager license.
[**refresh_parcel_repos**](ClouderaManagerResourceApi.md#refresh_parcel_repos) | **POST** /cm/commands/refreshParcelRepos | .
[**update_config**](ClouderaManagerResourceApi.md#update_config) | **PUT** /cm/config | Update the Cloudera Manager settings.
[**update_deployment2**](ClouderaManagerResourceApi.md#update_deployment2) | **PUT** /cm/deployment | Apply the supplied deployment description to the system.
[**update_license**](ClouderaManagerResourceApi.md#update_license) | **POST** /cm/license | Updates the Cloudera Manager license.


<a name="begin_trial" id="begin_trial"></a>
# **begin_trial**
> begin_trial()

Begin trial license.

Begin trial license.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Begin trial license.
    api_instance.begin_trial()
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->begin_trial: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="clusters_perf_inspector_command" id="clusters_perf_inspector_command"></a>
# **clusters_perf_inspector_command**
> ApiCommand clusters_perf_inspector_command(body=body)

Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiClustersPerfInspectorArgs() # ApiClustersPerfInspectorArgs | Required arguments for the command. See ApiClustersPerfInspectorArgs. (optional)

try:
    # Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
    api_response = api_instance.clusters_perf_inspector_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->clusters_perf_inspector_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiClustersPerfInspectorArgs**](ApiClustersPerfInspectorArgs.md)| Required arguments for the command. See ApiClustersPerfInspectorArgs. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="collect_diagnostic_data_command" id="collect_diagnostic_data_command"></a>
# **collect_diagnostic_data_command**
> ApiCommand collect_diagnostic_data_command(body=body)

Collect diagnostic data from hosts managed by Cloudera Manager.

Collect diagnostic data from hosts managed by Cloudera Manager. <p> After the command has completed, the ApiCommand will contain a resultDataUrl from where you can download the result. <p/> Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiCollectDiagnosticDataArguments() # ApiCollectDiagnosticDataArguments | The command arguments. (optional)

try:
    # Collect diagnostic data from hosts managed by Cloudera Manager.
    api_response = api_instance.collect_diagnostic_data_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->collect_diagnostic_data_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiCollectDiagnosticDataArguments**](ApiCollectDiagnosticDataArguments.md)| The command arguments. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_credentials_command" id="delete_credentials_command"></a>
# **delete_credentials_command**
> ApiCommand delete_credentials_command(delete_credentials_mode=delete_credentials_mode)

Delete existing Kerberos credentials.

Delete existing Kerberos credentials. <p> This command will affect all services that have been configured to use Kerberos, and have existing credentials. In V18 this takes a new paramater to determine whether it needs to delete all credentials or just unused ones.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
delete_credentials_mode = 'all' # str | this can be set to \"all\" or \"unused\" (optional) (default to all)

try:
    # Delete existing Kerberos credentials.
    api_response = api_instance.delete_credentials_command(delete_credentials_mode=delete_credentials_mode)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->delete_credentials_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_credentials_mode** | **str**| this can be set to \"all\" or \"unused\" | [optional] [default to all]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="end_trial" id="end_trial"></a>
# **end_trial**
> end_trial()

End trial license.

End trial license.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # End trial license.
    api_instance.end_trial()
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->end_trial: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="generate_credentials_command" id="generate_credentials_command"></a>
# **generate_credentials_command**
> ApiCommand generate_credentials_command()

Generate missing Kerberos credentials.

Generate missing Kerberos credentials. <p> This command will affect all services that have been configured to use Kerberos, and haven't had their credentials generated yet.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Generate missing Kerberos credentials.
    api_response = api_instance.generate_credentials_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->generate_credentials_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_config" id="get_config"></a>
# **get_config**
> ApiConfigList get_config(view=view)

Retrieve the Cloudera Manager settings.

Retrieve the Cloudera Manager settings.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str | The view to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Retrieve the Cloudera Manager settings.
    api_response = api_instance.get_config(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| The view to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_deployment2" id="get_deployment2"></a>
# **get_deployment2**
> ApiDeployment2 get_deployment2(view=view)

Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc.

Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc. <p/> This object can be used to reconstruct your entire deployment <p/> Note: Only users with sufficient privileges are allowed to call this. <ul> <li>Full Administrators</li> <li>Cluster Administrators (but Navigator config will be redacted)</li> </ul> <p/> Note: starting with v19, the deployment information contains a newer version of users ApiUser2 that can hold granular permissions.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
view = 'export' # str | May be one of \"export\" (default) or \"export_redacted\".  The latter replaces configurations that are sensitive with the word \"REDACTED\". (optional) (default to export)

try:
    # Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc.
    api_response = api_instance.get_deployment2(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_deployment2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| May be one of \"export\" (default) or \"export_redacted\".  The latter replaces configurations that are sensitive with the word \"REDACTED\". | [optional] [default to export]

### Return type

[**ApiDeployment2**](ApiDeployment2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_kerberos_info" id="get_kerberos_info"></a>
# **get_kerberos_info**
> ApiKerberosInfo get_kerberos_info()

Provides Cloudera Manager Kerberos information.

Provides Cloudera Manager Kerberos information

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Provides Cloudera Manager Kerberos information.
    api_response = api_instance.get_kerberos_info()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_kerberos_info: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiKerberosInfo**](ApiKerberosInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_kerberos_principals" id="get_kerberos_principals"></a>
# **get_kerberos_principals**
> ApiPrincipalList get_kerberos_principals(missing_only=missing_only)

Returns the Kerberos principals needed by the services being managed by Cloudera Manager.

Returns the Kerberos principals needed by the services being managed by Cloudera Manager.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
missing_only = true # bool | Whether to include only those principals which do not already exist in Cloudera Manager's database. (optional)

try:
    # Returns the Kerberos principals needed by the services being managed by Cloudera Manager.
    api_response = api_instance.get_kerberos_principals(missing_only=missing_only)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_kerberos_principals: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **missing_only** | **bool**| Whether to include only those principals which do not already exist in Cloudera Manager's database. | [optional] 

### Return type

[**ApiPrincipalList**](ApiPrincipalList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_licensed_feature_usage" id="get_licensed_feature_usage"></a>
# **get_licensed_feature_usage**
> ApiLicensedFeatureUsage get_licensed_feature_usage()

Retrieve a summary of licensed feature usage.

Retrieve a summary of licensed feature usage. <p/> This command will return information about what Cloudera Enterprise licensed features are in use in the clusters being managed by this Cloudera Manager, as well as totals for usage across all clusters. <p/> The specific features described can vary between different versions of Cloudera Manager. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Retrieve a summary of licensed feature usage.
    api_response = api_instance.get_licensed_feature_usage()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_licensed_feature_usage: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiLicensedFeatureUsage**](ApiLicensedFeatureUsage.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_log" id="get_log"></a>
# **get_log**
> str get_log()

Returns the entire contents of the Cloudera Manager log file.

Returns the entire contents of the Cloudera Manager log file

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Returns the entire contents of the Cloudera Manager log file.
    api_response = api_instance.get_log()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_log: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_scm_db_info" id="get_scm_db_info"></a>
# **get_scm_db_info**
> ApiScmDbInfo get_scm_db_info()

Provides Cloudera Manager server's database information.

Provides Cloudera Manager server's database information

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Provides Cloudera Manager server's database information.
    api_response = api_instance.get_scm_db_info()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_scm_db_info: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiScmDbInfo**](ApiScmDbInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_shutdown_readiness" id="get_shutdown_readiness"></a>
# **get_shutdown_readiness**
> ApiShutdownReadiness get_shutdown_readiness(last_activity_time=last_activity_time)

Retrieve Cloudera Manager's readiness for shutdown and destroy.

Retrieve Cloudera Manager's readiness for shutdown and destroy. Applications that wish to destroy Cloudera Manager and its managed cluster should poll this API, repeatedly if necessary, to respect its readiness.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
last_activity_time = 'last_activity_time_example' # str | End time of the last known activity/workload against the managed clusters, in ISO 8601 format. (optional)

try:
    # Retrieve Cloudera Manager's readiness for shutdown and destroy.
    api_response = api_instance.get_shutdown_readiness(last_activity_time=last_activity_time)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_shutdown_readiness: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **last_activity_time** | **str**| End time of the last known activity/workload against the managed clusters, in ISO 8601 format. | [optional] 

### Return type

[**ApiShutdownReadiness**](ApiShutdownReadiness.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_version" id="get_version"></a>
# **get_version**
> ApiVersionInfo get_version()

Provides version information of Cloudera Manager itself.

Provides version information of Cloudera Manager itself.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Provides version information of Cloudera Manager itself.
    api_response = api_instance.get_version()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->get_version: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiVersionInfo**](ApiVersionInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="host_install_command" id="host_install_command"></a>
# **host_install_command**
> ApiCommand host_install_command(body=body)

Perform installation on a set of hosts.

Perform installation on a set of hosts. <p/> This command installs Cloudera Manager Agent on a set of hosts. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiHostInstallArguments() # ApiHostInstallArguments | Hosts to perform installation on (optional)

try:
    # Perform installation on a set of hosts.
    api_response = api_instance.host_install_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->host_install_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostInstallArguments**](ApiHostInstallArguments.md)| Hosts to perform installation on | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hosts_decommission_command" id="hosts_decommission_command"></a>
# **hosts_decommission_command**
> ApiCommand hosts_decommission_command(body=body)

Decommission the given hosts.

Decommission the given hosts. All slave roles on the hosts will be decommissioned. All other roles will be stopped.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiHostNameList() # ApiHostNameList |  (optional)

try:
    # Decommission the given hosts.
    api_response = api_instance.hosts_decommission_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->hosts_decommission_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hosts_offline_or_decommission_command" id="hosts_offline_or_decommission_command"></a>
# **hosts_offline_or_decommission_command**
> ApiCommand hosts_offline_or_decommission_command(offline_timeout=offline_timeout, body=body)

Decommission the given hosts.

Decommission the given hosts. All slave roles on the hosts will be offlined or decommissioned with preference being offlined if supported by the service. <p> Currently the offline operation is only supported by HDFS, where the offline operation will put DataNodes into <em>HDFS IN MAINTENANCE</em> state which prevents unnecessary re-replication which could occur if decommissioned. <p> All other roles on the hosts will be stopped. <p> The <em>offlineTimeout</em> parameter is used to specify a timeout for offline. For HDFS, when the timeout expires, the DataNode will automatically transition out of <em>HDFS IN MAINTENANCE</em> state, back to <em>HDFS IN SERVICE</em> state. <p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
offline_timeout = 3.4 # float | offline timeout in seconds. Specify as null to get the default timeout (4 hours). Ignored if service does not support he offline operation. (optional)
body = cm_client.ApiHostNameList() # ApiHostNameList | list of host names to decommission. (optional)

try:
    # Decommission the given hosts.
    api_response = api_instance.hosts_offline_or_decommission_command(offline_timeout=offline_timeout, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->hosts_offline_or_decommission_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offline_timeout** | **float**| offline timeout in seconds. Specify as null to get the default timeout (4 hours). Ignored if service does not support he offline operation. | [optional] 
 **body** | [**ApiHostNameList**](ApiHostNameList.md)| list of host names to decommission. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hosts_perf_inspector_command" id="hosts_perf_inspector_command"></a>
# **hosts_perf_inspector_command**
> ApiCommand hosts_perf_inspector_command(body=body)

Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiHostsPerfInspectorArgs() # ApiHostsPerfInspectorArgs | Required arguments for the command. See ApiHostsPerfInspectorArgs. (optional)

try:
    # Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
    api_response = api_instance.hosts_perf_inspector_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->hosts_perf_inspector_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostsPerfInspectorArgs**](ApiHostsPerfInspectorArgs.md)| Required arguments for the command. See ApiHostsPerfInspectorArgs. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hosts_recommission_and_exit_maintenance_mode_command" id="hosts_recommission_and_exit_maintenance_mode_command"></a>
# **hosts_recommission_and_exit_maintenance_mode_command**
> ApiCommand hosts_recommission_and_exit_maintenance_mode_command(recommission_type=recommission_type, body=body)

Recommission and exit maintenance on the given hosts.

Recommission and exit maintenance on the given hosts. The recommission step may optionally start roles as well.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
recommission_type = 'recommission' # str |  (optional) (default to recommission)
body = cm_client.ApiHostNameList() # ApiHostNameList |  (optional)

try:
    # Recommission and exit maintenance on the given hosts.
    api_response = api_instance.hosts_recommission_and_exit_maintenance_mode_command(recommission_type=recommission_type, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->hosts_recommission_and_exit_maintenance_mode_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recommission_type** | **str**|  | [optional] [default to recommission]
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hosts_recommission_command" id="hosts_recommission_command"></a>
# **hosts_recommission_command**
> ApiCommand hosts_recommission_command(body=body)

Recommission the given hosts.

Recommission the given hosts. All slave roles on the hosts will be recommissioned. Roles are not started after this command. Use hostsStartRoles command for that.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiHostNameList() # ApiHostNameList |  (optional)

try:
    # Recommission the given hosts.
    api_response = api_instance.hosts_recommission_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->hosts_recommission_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hosts_recommission_with_start_command" id="hosts_recommission_with_start_command"></a>
# **hosts_recommission_with_start_command**
> ApiCommand hosts_recommission_with_start_command(body=body)

Recommission the given hosts.

Recommission the given hosts. If slave roles support start when decommissioned, start those roles before recommission. All slave roles on the hosts will be recommissioned.  Warning: Evolving. This method may change in the future and does not offer standard compatibility guarantees. Recommission the given hosts. If possible, start those roles before recommission. All slave roles on the hosts will be recommissioned. Do not use without guidance from Cloudera.  Currently, only HDFS DataNodes will be started by this command.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiHostNameList() # ApiHostNameList |  (optional)

try:
    # Recommission the given hosts.
    api_response = api_instance.hosts_recommission_with_start_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->hosts_recommission_with_start_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hosts_start_roles_command" id="hosts_start_roles_command"></a>
# **hosts_start_roles_command**
> ApiCommand hosts_start_roles_command(body=body)

Start all the roles on the given hosts.

Start all the roles on the given hosts.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiHostNameList() # ApiHostNameList |  (optional)

try:
    # Start all the roles on the given hosts.
    api_response = api_instance.hosts_start_roles_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->hosts_start_roles_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="import_admin_credentials" id="import_admin_credentials"></a>
# **import_admin_credentials**
> ApiCommand import_admin_credentials(password=password, username=username)

Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.

Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
password = 'password_example' # str | Password for the Account Manager.  return Information about the submitted command. (optional)
username = 'username_example' # str | Username of the Account Manager. Full name including the Kerberos realm must be specified. (optional)

try:
    # Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.
    api_response = api_instance.import_admin_credentials(password=password, username=username)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->import_admin_credentials: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password** | **str**| Password for the Account Manager.  return Information about the submitted command. | [optional] 
 **username** | **str**| Username of the Account Manager. Full name including the Kerberos realm must be specified. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="import_cluster_template" id="import_cluster_template"></a>
# **import_cluster_template**
> ApiCommand import_cluster_template(add_repositories=add_repositories, body=body)

Create cluster as per the given cluster template.

Create cluster as per the given cluster template

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
add_repositories = false # bool | if true the parcels repositories in the cluster template will be added. (optional) (default to false)
body = cm_client.ApiClusterTemplate() # ApiClusterTemplate | cluster template (optional)

try:
    # Create cluster as per the given cluster template.
    api_response = api_instance.import_cluster_template(add_repositories=add_repositories, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->import_cluster_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **add_repositories** | **bool**| if true the parcels repositories in the cluster template will be added. | [optional] [default to false]
 **body** | [**ApiClusterTemplate**](ApiClusterTemplate.md)| cluster template | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="import_kerberos_principal" id="import_kerberos_principal"></a>
# **import_kerberos_principal**
> ApiCommand import_kerberos_principal(kvno=kvno, password=password, principal=principal)

Imports the Kerberos credentials for the specified principal which can then be used to add to a role's keytab by running Generate Credentials command.

Imports the Kerberos credentials for the specified principal which can then be used to add to a role's keytab by running Generate Credentials command.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
kvno = 3.4 # float | Key-version number of the password.  return Information about the submitted command. (optional)
password = 'password_example' # str | Password for the Kerberos principal. Cloudera Manager will encrypt the principal and password and use it when needed for a daemon. (optional)
principal = 'principal_example' # str | Name of the principal. Full name including the Kerberos realm must be specified. If it already exists, it will be overwritten. (optional)

try:
    # Imports the Kerberos credentials for the specified principal which can then be used to add to a role's keytab by running Generate Credentials command.
    api_response = api_instance.import_kerberos_principal(kvno=kvno, password=password, principal=principal)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->import_kerberos_principal: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kvno** | **float**| Key-version number of the password.  return Information about the submitted command. | [optional] 
 **password** | **str**| Password for the Kerberos principal. Cloudera Manager will encrypt the principal and password and use it when needed for a daemon. | [optional] 
 **principal** | **str**| Name of the principal. Full name including the Kerberos realm must be specified. If it already exists, it will be overwritten. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="inspect_hosts_command" id="inspect_hosts_command"></a>
# **inspect_hosts_command**
> ApiCommand inspect_hosts_command()

Runs the host inspector on the configured hosts.

Runs the host inspector on the configured hosts.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Runs the host inspector on the configured hosts.
    api_response = api_instance.inspect_hosts_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->inspect_hosts_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_active_commands" id="list_active_commands"></a>
# **list_active_commands**
> ApiCommandList list_active_commands(view=view)

List active global commands.

List active global commands.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List active global commands.
    api_response = api_instance.list_active_commands(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->list_active_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_license" id="read_license"></a>
# **read_license**
> ApiLicense read_license()

Retrieve information about the Cloudera Manager license.

Retrieve information about the Cloudera Manager license.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # Retrieve information about the Cloudera Manager license.
    api_response = api_instance.read_license()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->read_license: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiLicense**](ApiLicense.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="refresh_parcel_repos" id="refresh_parcel_repos"></a>
# **refresh_parcel_repos**
> ApiCommand refresh_parcel_repos()

.

<p> Submit a command to refresh parcels information. </p> <p> This API could be used following two scenarios.<br> - User updated Cloudera Manager's local parcel repository. <br> - User updated remote parcel locations. <p> User wants to invoke this API to make sure that Cloudera Manager gets latest parcels information. User can then monitor the returned command before proceeding to the next step. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))

try:
    # .
    api_response = api_instance.refresh_parcel_repos()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->refresh_parcel_repos: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_config" id="update_config"></a>
# **update_config**
> ApiConfigList update_config(message=message, body=body)

Update the Cloudera Manager settings.

Update the Cloudera Manager settings. <p> If a value is set in the given configuration, it will be added to the manager's settings, replacing any existing entry. If a value is unset (its value is null), the existing the setting will be erased. <p> Settings that are not listed in the input will maintain their current values.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | Settings to update. (optional)

try:
    # Update the Cloudera Manager settings.
    api_response = api_instance.update_config(message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->update_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| Settings to update. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_deployment2" id="update_deployment2"></a>
# **update_deployment2**
> ApiDeployment2 update_deployment2(delete_current_deployment=delete_current_deployment, body=body)

Apply the supplied deployment description to the system.

Apply the supplied deployment description to the system. This will create the clusters, services, hosts and other objects specified in the argument. This call does not allow for any merge conflicts. If an entity already exists in the system, this call will fail. You can request, however, that all entities in the system are deleted before instantiating the new ones. <p/> You may specify a complete or partial deployment, e.g. you can provide host info with no clusters.  However, if you request that the current deployment be deleted, you are required to specify at least one admin user or this call will fail. This is to protect you from creating a system that cannot be logged into again. <p/> If there are any errors creating (or optionally deleting) a deployment, all changes will be rolled back leaving the system exactly as it was before calling this method.  The system will never be left in a state where part of the deployment is created and other parts are not. <p/> If the submitted deployment contains entities that require Cloudera Enterprise license, then the license should be provided to Cloudera Manager before making this API call.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
delete_current_deployment = false # bool | If true, the current deployment is deleted before the specified deployment is applied (optional) (default to false)
body = cm_client.ApiDeployment2() # ApiDeployment2 | The deployment to create (optional)

try:
    # Apply the supplied deployment description to the system.
    api_response = api_instance.update_deployment2(delete_current_deployment=delete_current_deployment, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->update_deployment2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_current_deployment** | **bool**| If true, the current deployment is deleted before the specified deployment is applied | [optional] [default to false]
 **body** | [**ApiDeployment2**](ApiDeployment2.md)| The deployment to create | [optional] 

### Return type

[**ApiDeployment2**](ApiDeployment2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_license" id="update_license"></a>
# **update_license**
> ApiLicense update_license(body=body)

Updates the Cloudera Manager license.

Updates the Cloudera Manager license. <p> After a new license is installed, the Cloudera Manager needs to be restarted for the changes to take effect. <p> The license file should be uploaded using a request with content type \"multipart/form-data\", instead of being encoded into a JSON representation.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClouderaManagerResourceApi(cm_client.ApiClient(configuration))
body = '/path/to/file.txt' # file |  (optional)

try:
    # Updates the Cloudera Manager license.
    api_response = api_instance.update_license(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClouderaManagerResourceApi->update_license: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **file**|  | [optional] 

### Return type

[**ApiLicense**](ApiLicense.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

