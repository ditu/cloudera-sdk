# ApiEnableRmHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_rm_host_id** | **str** | Id of host on which second ResourceManager role will be added. | [optional] 
**new_rm_role_name** | **str** | Name of the second ResourceManager role to be created (Optional) | [optional] 
**zk_service_name** | **str** | Name of the ZooKeeper service that will be used for auto-failover. This is an optional parameter if the Yarn to ZooKeeper dependency is already set in CM. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


