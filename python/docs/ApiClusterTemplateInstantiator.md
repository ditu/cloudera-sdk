# ApiClusterTemplateInstantiator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cluster_name** | **str** |  | [optional] 
**hosts** | [**list[ApiClusterTemplateHostInfo]**](ApiClusterTemplateHostInfo.md) |  | [optional] 
**variables** | [**list[ApiClusterTemplateVariable]**](ApiClusterTemplateVariable.md) |  | [optional] 
**role_config_groups** | [**list[ApiClusterTemplateRoleConfigGroupInfo]**](ApiClusterTemplateRoleConfigGroupInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


