# cm_client.AuthServiceRolesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_roles**](AuthServiceRolesResourceApi.md#create_roles) | **POST** /cm/authService/roles | Create new roles in the Authentication Services.
[**delete_role**](AuthServiceRolesResourceApi.md#delete_role) | **DELETE** /cm/authService/roles/{roleName} | Delete a role from the Authentication Services.
[**enter_maintenance_mode**](AuthServiceRolesResourceApi.md#enter_maintenance_mode) | **POST** /cm/authService/roles/{roleName}/commands/enterMaintenanceMode | Put the Authentication Service role into maintenance mode.
[**exit_maintenance_mode**](AuthServiceRolesResourceApi.md#exit_maintenance_mode) | **POST** /cm/authService/roles/{roleName}/commands/exitMaintenanceMode | Take the Authentication Service role out of maintenance mode.
[**get_full_log**](AuthServiceRolesResourceApi.md#get_full_log) | **GET** /cm/authService/roles/{roleName}/logs/full | Retrieves the log file for the role&#39;s main process.
[**get_stacks_log**](AuthServiceRolesResourceApi.md#get_stacks_log) | **GET** /cm/authService/roles/{roleName}/logs/stacks | Retrieves the stacks log file, if any, for the role&#39;s main process.
[**get_stacks_logs_bundle**](AuthServiceRolesResourceApi.md#get_stacks_logs_bundle) | **GET** /cm/authService/roles/{roleName}/logs/stacksBundle | Download a zip-compressed archive of role stacks logs.
[**get_standard_error**](AuthServiceRolesResourceApi.md#get_standard_error) | **GET** /cm/authService/roles/{roleName}/logs/stderr | Retrieves the role&#39;s standard error output.
[**get_standard_output**](AuthServiceRolesResourceApi.md#get_standard_output) | **GET** /cm/authService/roles/{roleName}/logs/stdout | Retrieves the role&#39;s standard output.
[**list_active_commands**](AuthServiceRolesResourceApi.md#list_active_commands) | **GET** /cm/authService/roles/{roleName}/commands | List active role commands.
[**read_role**](AuthServiceRolesResourceApi.md#read_role) | **GET** /cm/authService/roles/{roleName} | Retrieve detailed information about a Authentication Services role.
[**read_role_config**](AuthServiceRolesResourceApi.md#read_role_config) | **GET** /cm/authService/roles/{roleName}/config | Retrieve the configuration of a specific Authentication Services role.
[**read_roles**](AuthServiceRolesResourceApi.md#read_roles) | **GET** /cm/authService/roles | List all roles of the Authentication Services.
[**update_role_config**](AuthServiceRolesResourceApi.md#update_role_config) | **PUT** /cm/authService/roles/{roleName}/config | Update the configuration of a Authentication Services role.


<a name="create_roles" id="create_roles"></a>
# **create_roles**
> ApiRoleList create_roles(body=body)

Create new roles in the Authentication Services.

Create new roles in the Authentication Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleList() # ApiRoleList | Roles to create. (optional)

try:
    # Create new roles in the Authentication Services.
    api_response = api_instance.create_roles(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->create_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleList**](ApiRoleList.md)| Roles to create. | [optional] 

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_role" id="delete_role"></a>
# **delete_role**
> ApiRole delete_role(role_name)

Delete a role from the Authentication Services.

Delete a role from the Authentication Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role name.

try:
    # Delete a role from the Authentication Services.
    api_response = api_instance.delete_role(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->delete_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role name. | 

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enter_maintenance_mode" id="enter_maintenance_mode"></a>
# **enter_maintenance_mode**
> ApiCommand enter_maintenance_mode(role_name)

Put the Authentication Service role into maintenance mode.

Put the Authentication Service role into maintenance mode.This is a synchronous command. The result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role name.

try:
    # Put the Authentication Service role into maintenance mode.
    api_response = api_instance.enter_maintenance_mode(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->enter_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="exit_maintenance_mode" id="exit_maintenance_mode"></a>
# **exit_maintenance_mode**
> ApiCommand exit_maintenance_mode(role_name)

Take the Authentication Service role out of maintenance mode.

Take the Authentication Service role out of maintenance mode. This is a synchronous command. The result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role name.

try:
    # Take the Authentication Service role out of maintenance mode.
    api_response = api_instance.exit_maintenance_mode(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->exit_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_full_log" id="get_full_log"></a>
# **get_full_log**
> str get_full_log(role_name)

Retrieves the log file for the role's main process.

Retrieves the log file for the role's main process. <p> If the role is not started, this will be the log file associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role to fetch logs from.

try:
    # Retrieves the log file for the role's main process.
    api_response = api_instance.get_full_log(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->get_full_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role to fetch logs from. | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_stacks_log" id="get_stacks_log"></a>
# **get_stacks_log**
> str get_stacks_log(role_name)

Retrieves the stacks log file, if any, for the role's main process.

Retrieves the stacks log file, if any, for the role's main process. Note that not all roles support periodic stacks collection.  The log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role to fetch stacks logs from.

try:
    # Retrieves the stacks log file, if any, for the role's main process.
    api_response = api_instance.get_stacks_log(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->get_stacks_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role to fetch stacks logs from. | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_stacks_logs_bundle" id="get_stacks_logs_bundle"></a>
# **get_stacks_logs_bundle**
> get_stacks_logs_bundle(role_name)

Download a zip-compressed archive of role stacks logs.

Download a zip-compressed archive of role stacks logs. Note that not all roles support periodic stacks collection.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role to fetch the stacks logs bundle from.

try:
    # Download a zip-compressed archive of role stacks logs.
    api_instance.get_stacks_logs_bundle(role_name)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->get_stacks_logs_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role to fetch the stacks logs bundle from. | 

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_standard_error" id="get_standard_error"></a>
# **get_standard_error**
> str get_standard_error(role_name)

Retrieves the role's standard error output.

Retrieves the role's standard error output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role to fetch stderr from.

try:
    # Retrieves the role's standard error output.
    api_response = api_instance.get_standard_error(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->get_standard_error: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role to fetch stderr from. | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_standard_output" id="get_standard_output"></a>
# **get_standard_output**
> str get_standard_output(role_name)

Retrieves the role's standard output.

Retrieves the role's standard output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role to fetch stdout from.

try:
    # Retrieves the role's standard output.
    api_response = api_instance.get_standard_output(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->get_standard_output: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role to fetch stdout from. | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_active_commands" id="list_active_commands"></a>
# **list_active_commands**
> ApiCommandList list_active_commands(role_name, view=view)

List active role commands.

List active role commands.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role name.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List active role commands.
    api_response = api_instance.list_active_commands(role_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->list_active_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role name. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role" id="read_role"></a>
# **read_role**
> ApiRole read_role(role_name)

Retrieve detailed information about a Authentication Services role.

Retrieve detailed information about a Authentication Services role.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role name.

try:
    # Retrieve detailed information about a Authentication Services role.
    api_response = api_instance.read_role(role_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->read_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role name. | 

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role_config" id="read_role_config"></a>
# **read_role_config**
> ApiConfigList read_role_config(role_name, view=view)

Retrieve the configuration of a specific Authentication Services role.

Retrieve the configuration of a specific Authentication Services role.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role to look up.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Retrieve the configuration of a specific Authentication Services role.
    api_response = api_instance.read_role_config(role_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->read_role_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role to look up. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_roles" id="read_roles"></a>
# **read_roles**
> ApiRoleList read_roles()

List all roles of the Authentication Services.

List all roles of the Authentication Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))

try:
    # List all roles of the Authentication Services.
    api_response = api_instance.read_roles()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->read_roles: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_role_config" id="update_role_config"></a>
# **update_role_config**
> ApiConfigList update_role_config(role_name, message=message, body=body)

Update the configuration of a Authentication Services role.

Update the configuration of a Authentication Services role. <p> If a value is set in the given configuration, it will be added to the role's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRolesResourceApi(cm_client.ApiClient(configuration))
role_name = 'role_name_example' # str | The role to modify.
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | Configuration changes. (optional)

try:
    # Update the configuration of a Authentication Services role.
    api_response = api_instance.update_role_config(role_name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRolesResourceApi->update_role_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_name** | **str**| The role to modify. | 
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| Configuration changes. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

