# ApiEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | A unique ID for this event. | [optional] 
**content** | **str** | The content payload of this event. | [optional] 
**time_occurred** | **str** | When the event was generated. | [optional] 
**time_received** | **str** | When the event was stored by Cloudera Manager. Events do not arrive in the order that they are generated. If you are writing an event poller, this is a useful field to query. | [optional] 
**category** | [**ApiEventCategory**](ApiEventCategory.md) | The category of this event -- whether it is a health event, an audit event, an activity event, etc. | [optional] 
**severity** | [**ApiEventSeverity**](ApiEventSeverity.md) | The severity of the event. | [optional] 
**alert** | **bool** | Whether the event is promoted to an alert according to configuration. | [optional] 
**attributes** | [**list[ApiEventAttribute]**](ApiEventAttribute.md) | A list of key-value attribute pairs. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


