# ApiHBaseSnapshot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**snapshot_name** | **str** | Snapshot name. | [optional] 
**table_name** | **str** | Name of the table this snapshot is for. | [optional] 
**creation_time** | **str** | Snapshot creation time. | [optional] 
**storage** | [**Storage**](Storage.md) | The location where a snapshot is stored. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


