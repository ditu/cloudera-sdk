# cm_client.ImpalaQueriesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancel_impala_query**](ImpalaQueriesResourceApi.md#cancel_impala_query) | **POST** /clusters/{clusterName}/services/{serviceName}/impalaQueries/{queryId}/cancel | Cancels an Impala Query.
[**get_impala_queries**](ImpalaQueriesResourceApi.md#get_impala_queries) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries | Returns a list of queries that satisfy the filter.
[**get_impala_query_attributes**](ImpalaQueriesResourceApi.md#get_impala_query_attributes) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries/attributes | Returns the list of all attributes that the Service Monitor can associate with Impala queries.
[**get_query_details**](ImpalaQueriesResourceApi.md#get_query_details) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries/{queryId} | Returns details about the query.


<a name="cancel_impala_query" id="cancel_impala_query"></a>
# **cancel_impala_query**
> ApiImpalaCancelResponse cancel_impala_query(cluster_name, query_id, service_name)

Cancels an Impala Query.

Cancels an Impala Query. <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ImpalaQueriesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
query_id = 'query_id_example' # str | The queryId to cancel
service_name = 'service_name_example' # str | The name of the service

try:
    # Cancels an Impala Query.
    api_response = api_instance.cancel_impala_query(cluster_name, query_id, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImpalaQueriesResourceApi->cancel_impala_query: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **query_id** | **str**| The queryId to cancel | 
 **service_name** | **str**| The name of the service | 

### Return type

[**ApiImpalaCancelResponse**](ApiImpalaCancelResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_impala_queries" id="get_impala_queries"></a>
# **get_impala_queries**
> ApiImpalaQueryResponse get_impala_queries(cluster_name, service_name, filter=filter, _from=_from, limit=limit, offset=offset, to=to)

Returns a list of queries that satisfy the filter.

Returns a list of queries that satisfy the filter <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ImpalaQueriesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service
filter = '' # str | A filter to apply to the queries. A basic filter tests the value of an attribute and looks something like 'rowsFetched = 1' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'query_duration > 5s and (user = root or user = myUserName)'. An example of expected full query string in requested URL is: '?filter=(query_duration > 5s and (user = root or user = myUserName))'. (optional) (default to )
_from = '_from_example' # str | Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time). (optional)
limit = 100 # int | The maximum number of queries to return. Queries will be returned in the following order: <ul> <li> All executing queries, ordered from longest to shortest running </li> <li> All completed queries order by end time descending. </li> </ul> (optional) (default to 100)
offset = 0 # int | The offset to start returning queries from. This is useful for paging through lists of queries. Note that this has non-deterministic behavior if executing queries are included in the response because they can disappear from the list while paging. To exclude executing queries from the response and a 'executing = false' clause to your filter. (optional) (default to 0)
to = 'now' # str | End of the period to query in ISO 8601 format (defaults to current time). (optional) (default to now)

try:
    # Returns a list of queries that satisfy the filter.
    api_response = api_instance.get_impala_queries(cluster_name, service_name, filter=filter, _from=_from, limit=limit, offset=offset, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImpalaQueriesResourceApi->get_impala_queries: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 
 **filter** | **str**| A filter to apply to the queries. A basic filter tests the value of an attribute and looks something like 'rowsFetched = 1' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'query_duration > 5s and (user = root or user = myUserName)'. An example of expected full query string in requested URL is: '?filter=(query_duration > 5s and (user = root or user = myUserName))'. | [optional] [default to ]
 **_from** | **str**| Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time). | [optional] 
 **limit** | **int**| The maximum number of queries to return. Queries will be returned in the following order: <ul> <li> All executing queries, ordered from longest to shortest running </li> <li> All completed queries order by end time descending. </li> </ul> | [optional] [default to 100]
 **offset** | **int**| The offset to start returning queries from. This is useful for paging through lists of queries. Note that this has non-deterministic behavior if executing queries are included in the response because they can disappear from the list while paging. To exclude executing queries from the response and a 'executing = false' clause to your filter. | [optional] [default to 0]
 **to** | **str**| End of the period to query in ISO 8601 format (defaults to current time). | [optional] [default to now]

### Return type

[**ApiImpalaQueryResponse**](ApiImpalaQueryResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_impala_query_attributes" id="get_impala_query_attributes"></a>
# **get_impala_query_attributes**
> ApiImpalaQueryAttributeList get_impala_query_attributes(cluster_name, service_name)

Returns the list of all attributes that the Service Monitor can associate with Impala queries.

Returns the list of all attributes that the Service Monitor can associate with Impala queries. <p> Examples of attributes include the user who issued the query and the number of HDFS bytes read by the query. <p> These attributes can be used to search for specific Impala queries through the getImpalaQueries API. For example the 'user' attribute could be used in the search 'user = root'. If the attribute is numeric it can also be used as a metric in a tsquery (ie, 'select hdfs_bytes_read from IMPALA_QUERIES'). <p> Note that this response is identical for all Impala services. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ImpalaQueriesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 

try:
    # Returns the list of all attributes that the Service Monitor can associate with Impala queries.
    api_response = api_instance.get_impala_query_attributes(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImpalaQueriesResourceApi->get_impala_query_attributes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 

### Return type

[**ApiImpalaQueryAttributeList**](ApiImpalaQueryAttributeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_query_details" id="get_query_details"></a>
# **get_query_details**
> ApiImpalaQueryDetailsResponse get_query_details(cluster_name, query_id, service_name, format=format)

Returns details about the query.

Returns details about the query. Not all queries have details, check the detailsAvailable field from the getQueries response. <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ImpalaQueriesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
query_id = 'query_id_example' # str | The queryId to get information about
service_name = 'service_name_example' # str | 
format = 'text' # str | There are two valid format parameters: <ul> <li> 'text': this is a text based, human readable representation of the Impala runtime profile. </li> <li> 'thrift_encoded': this a compact-thrift, base64 encoded representation of the Impala RuntimeProfile.thrift object. See the Impala documentation for more details. </li> </ul> (optional) (default to text)

try:
    # Returns details about the query.
    api_response = api_instance.get_query_details(cluster_name, query_id, service_name, format=format)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImpalaQueriesResourceApi->get_query_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **query_id** | **str**| The queryId to get information about | 
 **service_name** | **str**|  | 
 **format** | **str**| There are two valid format parameters: <ul> <li> 'text': this is a text based, human readable representation of the Impala runtime profile. </li> <li> 'thrift_encoded': this a compact-thrift, base64 encoded representation of the Impala RuntimeProfile.thrift object. See the Impala documentation for more details. </li> </ul> | [optional] [default to text]

### Return type

[**ApiImpalaQueryDetailsResponse**](ApiImpalaQueryDetailsResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

