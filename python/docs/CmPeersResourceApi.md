# cm_client.CmPeersResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_peer**](CmPeersResourceApi.md#create_peer) | **POST** /cm/peers | Create a new Cloudera Manager peer.
[**delete_peer**](CmPeersResourceApi.md#delete_peer) | **DELETE** /cm/peers/{peerName} | Delete Cloudera Manager peer.
[**list_peers**](CmPeersResourceApi.md#list_peers) | **GET** /cm/peers | Retrieves all configured Cloudera Manager peers.
[**read_peer**](CmPeersResourceApi.md#read_peer) | **GET** /cm/peers/{peerName} | Fetch information about an existing Cloudera Manager peer.
[**test_peer**](CmPeersResourceApi.md#test_peer) | **POST** /cm/peers/{peerName}/commands/test | Test the connectivity of a peer.
[**update_peer**](CmPeersResourceApi.md#update_peer) | **PUT** /cm/peers/{peerName} | Update information for a Cloudera Manager peer.


<a name="create_peer" id="create_peer"></a>
# **create_peer**
> ApiCmPeer create_peer(body=body)

Create a new Cloudera Manager peer.

Create a new Cloudera Manager peer. <p> The remote server will be contacted so that a user can be created for use by the new peer. The <i>username</i> and <i>password</i> properties of the provided peer object should contain credentials of a valid admin user on the remote server. A timeout of 10 seconds is enforced when contacting the remote server. <p> It is recommended to run the remote server with TLS enabled, since creating and using peers involve transferring credentials over the network. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> Type field in ApiCmPeer is available since API v11. if not specified when making createPeer() call, 'REPLICATION' type peer will be created.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CmPeersResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiCmPeer() # ApiCmPeer | Peer to create (see above). (optional)

try:
    # Create a new Cloudera Manager peer.
    api_response = api_instance.create_peer(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CmPeersResourceApi->create_peer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiCmPeer**](ApiCmPeer.md)| Peer to create (see above). | [optional] 

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_peer" id="delete_peer"></a>
# **delete_peer**
> ApiCmPeer delete_peer(peer_name, type=type)

Delete Cloudera Manager peer.

Delete Cloudera Manager peer. <p> An attempt will be made to contact the peer server, so that the configured user can be deleted.. Errors while contacting the remote server are non-fatal. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CmPeersResourceApi(cm_client.ApiClient(configuration))
peer_name = 'peer_name_example' # str | Name of peer to delete.
type = 'type_example' # str | Type of peer to delete. If null, REPLICATION peer type will be deleted. (optional)

try:
    # Delete Cloudera Manager peer.
    api_response = api_instance.delete_peer(peer_name, type=type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CmPeersResourceApi->delete_peer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peer_name** | **str**| Name of peer to delete. | 
 **type** | **str**| Type of peer to delete. If null, REPLICATION peer type will be deleted. | [optional] 

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_peers" id="list_peers"></a>
# **list_peers**
> ApiCmPeerList list_peers()

Retrieves all configured Cloudera Manager peers.

Retrieves all configured Cloudera Manager peers. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> When accessed via API version before v11, only REPLICATION type peers will be returned.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CmPeersResourceApi(cm_client.ApiClient(configuration))

try:
    # Retrieves all configured Cloudera Manager peers.
    api_response = api_instance.list_peers()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CmPeersResourceApi->list_peers: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCmPeerList**](ApiCmPeerList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_peer" id="read_peer"></a>
# **read_peer**
> ApiCmPeer read_peer(peer_name, type=type)

Fetch information about an existing Cloudera Manager peer.

Fetch information about an existing Cloudera Manager peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CmPeersResourceApi(cm_client.ApiClient(configuration))
peer_name = 'peer_name_example' # str | Name of peer to retrieve.
type = 'type_example' # str | Type of peer to retrieve. If null, REPLICATION peer type will be returned. (optional)

try:
    # Fetch information about an existing Cloudera Manager peer.
    api_response = api_instance.read_peer(peer_name, type=type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CmPeersResourceApi->read_peer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peer_name** | **str**| Name of peer to retrieve. | 
 **type** | **str**| Type of peer to retrieve. If null, REPLICATION peer type will be returned. | [optional] 

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="test_peer" id="test_peer"></a>
# **test_peer**
> ApiCommand test_peer(peer_name, type=type)

Test the connectivity of a peer.

Test the connectivity of a peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CmPeersResourceApi(cm_client.ApiClient(configuration))
peer_name = 'peer_name_example' # str | Name of peer to test.
type = 'type_example' # str | Type of peer to test. If null, REPLICATION peer type will be tested. (optional)

try:
    # Test the connectivity of a peer.
    api_response = api_instance.test_peer(peer_name, type=type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CmPeersResourceApi->test_peer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peer_name** | **str**| Name of peer to test. | 
 **type** | **str**| Type of peer to test. If null, REPLICATION peer type will be tested. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_peer" id="update_peer"></a>
# **update_peer**
> ApiCmPeer update_peer(peer_name, body=body)

Update information for a Cloudera Manager peer.

Update information for a Cloudera Manager peer. <p> In administrator credentials are provided in the peer information, they will be used to establish new credentials with the remote server. This can be used in case the old credentials are not working anymore. An attempt will be made to delete the old credentials if new ones are successfully created. <p> If changing the peer's URL, an attempt will be made to contact the old Cloudera Manager to delete the existing credentials. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CmPeersResourceApi(cm_client.ApiClient(configuration))
peer_name = 'peer_name_example' # str | Name of peer to update.
body = cm_client.ApiCmPeer() # ApiCmPeer | Updated peer information. (optional)

try:
    # Update information for a Cloudera Manager peer.
    api_response = api_instance.update_peer(peer_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CmPeersResourceApi->update_peer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peer_name** | **str**| Name of peer to update. | 
 **body** | [**ApiCmPeer**](ApiCmPeer.md)| Updated peer information. | [optional] 

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

