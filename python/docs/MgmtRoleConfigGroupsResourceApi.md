# cm_client.MgmtRoleConfigGroupsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**read_config**](MgmtRoleConfigGroupsResourceApi.md#read_config) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName}/config | Returns the current revision of the config for the specified role config group in the Cloudera Management Services.
[**read_role_config_group**](MgmtRoleConfigGroupsResourceApi.md#read_role_config_group) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName} | Returns the information for a given role config group in the Cloudera Management Services.
[**read_role_config_groups**](MgmtRoleConfigGroupsResourceApi.md#read_role_config_groups) | **GET** /cm/service/roleConfigGroups | Returns the information for all role config groups in the Cloudera Management Services.
[**read_roles**](MgmtRoleConfigGroupsResourceApi.md#read_roles) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName}/roles | Returns all roles in the given role config group in the Cloudera Management Services.
[**update_config**](MgmtRoleConfigGroupsResourceApi.md#update_config) | **PUT** /cm/service/roleConfigGroups/{roleConfigGroupName}/config | Updates the config for the given role config group in the Cloudera Management Services.
[**update_role_config_group**](MgmtRoleConfigGroupsResourceApi.md#update_role_config_group) | **PUT** /cm/service/roleConfigGroups/{roleConfigGroupName} | Updates an existing role config group in the Cloudera Management Services.


<a name="read_config" id="read_config"></a>
# **read_config**
> ApiConfigList read_config(role_config_group_name, view=view)

Returns the current revision of the config for the specified role config group in the Cloudera Management Services.

Returns the current revision of the config for the specified role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
role_config_group_name = 'role_config_group_name_example' # str | The name of the role config group.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Returns the current revision of the config for the specified role config group in the Cloudera Management Services.
    api_response = api_instance.read_config(role_config_group_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleConfigGroupsResourceApi->read_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_config_group_name** | **str**| The name of the role config group. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role_config_group" id="read_role_config_group"></a>
# **read_role_config_group**
> ApiRoleConfigGroup read_role_config_group(role_config_group_name)

Returns the information for a given role config group in the Cloudera Management Services.

Returns the information for a given role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
role_config_group_name = 'role_config_group_name_example' # str | The name of the requested group.

try:
    # Returns the information for a given role config group in the Cloudera Management Services.
    api_response = api_instance.read_role_config_group(role_config_group_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleConfigGroupsResourceApi->read_role_config_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_config_group_name** | **str**| The name of the requested group. | 

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role_config_groups" id="read_role_config_groups"></a>
# **read_role_config_groups**
> ApiRoleConfigGroupList read_role_config_groups()

Returns the information for all role config groups in the Cloudera Management Services.

Returns the information for all role config groups in the Cloudera Management Services. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))

try:
    # Returns the information for all role config groups in the Cloudera Management Services.
    api_response = api_instance.read_role_config_groups()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleConfigGroupsResourceApi->read_role_config_groups: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_roles" id="read_roles"></a>
# **read_roles**
> ApiRoleList read_roles(role_config_group_name)

Returns all roles in the given role config group in the Cloudera Management Services.

Returns all roles in the given role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
role_config_group_name = 'role_config_group_name_example' # str | The name of the role config group.

try:
    # Returns all roles in the given role config group in the Cloudera Management Services.
    api_response = api_instance.read_roles(role_config_group_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleConfigGroupsResourceApi->read_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_config_group_name** | **str**| The name of the role config group. | 

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_config" id="update_config"></a>
# **update_config**
> ApiConfigList update_config(role_config_group_name, message=message, body=body)

Updates the config for the given role config group in the Cloudera Management Services.

Updates the config for the given role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
role_config_group_name = 'role_config_group_name_example' # str | The name of the role config group.
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | The new config information for the group. (optional)

try:
    # Updates the config for the given role config group in the Cloudera Management Services.
    api_response = api_instance.update_config(role_config_group_name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleConfigGroupsResourceApi->update_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_config_group_name** | **str**| The name of the role config group. | 
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| The new config information for the group. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_role_config_group" id="update_role_config_group"></a>
# **update_role_config_group**
> ApiRoleConfigGroup update_role_config_group(role_config_group_name, message=message, body=body)

Updates an existing role config group in the Cloudera Management Services.

Updates an existing role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleConfigGroupsResourceApi(cm_client.ApiClient(configuration))
role_config_group_name = 'role_config_group_name_example' # str | The name of the group to update.
message = 'message_example' # str | The optional message describing the changes. (optional)
body = cm_client.ApiRoleConfigGroup() # ApiRoleConfigGroup | The updated role config group. (optional)

try:
    # Updates an existing role config group in the Cloudera Management Services.
    api_response = api_instance.update_role_config_group(role_config_group_name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleConfigGroupsResourceApi->update_role_config_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_config_group_name** | **str**| The name of the group to update. | 
 **message** | **str**| The optional message describing the changes. | [optional] 
 **body** | [**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)| The updated role config group. | [optional] 

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

