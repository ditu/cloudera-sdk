# cm_client.CommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**abort_command**](CommandsResourceApi.md#abort_command) | **POST** /commands/{commandId}/abort | Abort a running command.
[**read_command**](CommandsResourceApi.md#read_command) | **GET** /commands/{commandId} | Retrieve detailed information on an asynchronous command.
[**retry**](CommandsResourceApi.md#retry) | **POST** /commands/{commandId}/retry | Try to rerun a command.


<a name="abort_command" id="abort_command"></a>
# **abort_command**
> ApiCommand abort_command(command_id)

Abort a running command.

Abort a running command.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CommandsResourceApi(cm_client.ApiClient(configuration))
command_id = 3.4 # float | The command id.

try:
    # Abort a running command.
    api_response = api_instance.abort_command(command_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CommandsResourceApi->abort_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **float**| The command id. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_command" id="read_command"></a>
# **read_command**
> ApiCommand read_command(command_id)

Retrieve detailed information on an asynchronous command.

Retrieve detailed information on an asynchronous command.  <p>Cloudera Manager keeps the results and statuses of asynchronous commands, which have non-negative command IDs. On the other hand, synchronous commands complete immediately, and their results are passed back in the return object of the command execution API call. Outside of that return object, there is no way to check the result of a synchronous command.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CommandsResourceApi(cm_client.ApiClient(configuration))
command_id = 3.4 # float | The command id.

try:
    # Retrieve detailed information on an asynchronous command.
    api_response = api_instance.read_command(command_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CommandsResourceApi->read_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **float**| The command id. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="retry" id="retry"></a>
# **retry**
> ApiCommand retry(command_id)

Try to rerun a command.

Try to rerun a command.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.CommandsResourceApi(cm_client.ApiClient(configuration))
command_id = 3.4 # float | ID of the command that needs to be run.

try:
    # Try to rerun a command.
    api_response = api_instance.retry(command_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CommandsResourceApi->retry: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **float**| ID of the command that needs to be run. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

