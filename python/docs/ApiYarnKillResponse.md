# ApiYarnKillResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warning** | **str** | The warning, if any, from the call. We will return a warning if the caller attempts to cancel an application that has already completed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


