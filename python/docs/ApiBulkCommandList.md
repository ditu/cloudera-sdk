# ApiBulkCommandList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[ApiCommand]**](ApiCommand.md) |  | [optional] 
**errors** | **list[str]** | Errors that occurred when issuing individual commands. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


