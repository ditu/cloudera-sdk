# cm_client.EventsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**read_event**](EventsResourceApi.md#read_event) | **GET** /events/{eventId} | Returns a specific event in the system.
[**read_events**](EventsResourceApi.md#read_events) | **GET** /events | Allows you to query events in the system.


<a name="read_event" id="read_event"></a>
# **read_event**
> ApiEvent read_event(event_id)

Returns a specific event in the system.

Returns a specific event in the system

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.EventsResourceApi(cm_client.ApiClient(configuration))
event_id = 'event_id_example' # str | The UUID of the event to read

try:
    # Returns a specific event in the system.
    api_response = api_instance.read_event(event_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventsResourceApi->read_event: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **str**| The UUID of the event to read | 

### Return type

[**ApiEvent**](ApiEvent.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_events" id="read_events"></a>
# **read_events**
> ApiEventQueryResult read_events(max_results=max_results, query=query, result_offset=result_offset)

Allows you to query events in the system.

Allows you to query events in the system.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.EventsResourceApi(cm_client.ApiClient(configuration))
max_results = 100 # int | The maximum number of events to return. (optional) (default to 100)
query = 'query_example' # str | The query to perform to find events in the system. It accepts querying the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>alert==true</dt> <dd>looks for alerts.</dd> <dt>alert==true;attributes.host!=flaky.mysite.com</dt> <dd>looks for alerts, but exclude those with the host attribute of \"flaky.mysite.com\".</dd> <dt>category==log_event;attributes.log_level==ERROR</dt> <dd>looks for error log events. Event attribute matching is case sensitive.</dd> <dt>attributes.service==hbase1;content==hlog</dt> <dd>looks for any events from the \"hbase1\" service that mention \"hlog\".</dd> <dt>attributes.service==hbase1;content!=hlog</dt> <dd>looks for any events from the \"hbase1\" service that do not mention \"hlog\".<br/> A query must not contain only negative constraints (<em>!=</em>). It returns empty results because there is nothing to perform exclusion on.</dd> <dt>attributes.role_type==NAMENODE;severity==critical important</dt> <dd>looks for any important or critical events related to all NameNodes.</dd> <dt>severity==critical;timeReceived=ge=2012-05-04T00:00;timeReceived=lt=2012-05-04T00:10</dt> <dd>looks for critical events received between the given 10 minute range. <br/> When polling for events, use <em>timeReceived</em> instead of <em>timeOccurred</em> because events arrive out of order.</dd> </dl>  You may query any fields present in the ApiEvent object. You can also query by event attribute values using the <em>attributes.*</em> syntax. Values for date time fields (e.g. <em>timeOccurred</em>, <em>timeReceived</em>) should be ISO8601 timestamps. <p> The other valid comparators are <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively. These comparators are only applicable for date time fields. (optional)
result_offset = 0 # int | Specified the offset of events to return. (optional) (default to 0)

try:
    # Allows you to query events in the system.
    api_response = api_instance.read_events(max_results=max_results, query=query, result_offset=result_offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventsResourceApi->read_events: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **max_results** | **int**| The maximum number of events to return. | [optional] [default to 100]
 **query** | **str**| The query to perform to find events in the system. It accepts querying the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>alert==true</dt> <dd>looks for alerts.</dd> <dt>alert==true;attributes.host!=flaky.mysite.com</dt> <dd>looks for alerts, but exclude those with the host attribute of \"flaky.mysite.com\".</dd> <dt>category==log_event;attributes.log_level==ERROR</dt> <dd>looks for error log events. Event attribute matching is case sensitive.</dd> <dt>attributes.service==hbase1;content==hlog</dt> <dd>looks for any events from the \"hbase1\" service that mention \"hlog\".</dd> <dt>attributes.service==hbase1;content!=hlog</dt> <dd>looks for any events from the \"hbase1\" service that do not mention \"hlog\".<br/> A query must not contain only negative constraints (<em>!=</em>). It returns empty results because there is nothing to perform exclusion on.</dd> <dt>attributes.role_type==NAMENODE;severity==critical important</dt> <dd>looks for any important or critical events related to all NameNodes.</dd> <dt>severity==critical;timeReceived=ge=2012-05-04T00:00;timeReceived=lt=2012-05-04T00:10</dt> <dd>looks for critical events received between the given 10 minute range. <br/> When polling for events, use <em>timeReceived</em> instead of <em>timeOccurred</em> because events arrive out of order.</dd> </dl>  You may query any fields present in the ApiEvent object. You can also query by event attribute values using the <em>attributes.*</em> syntax. Values for date time fields (e.g. <em>timeOccurred</em>, <em>timeReceived</em>) should be ISO8601 timestamps. <p> The other valid comparators are <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively. These comparators are only applicable for date time fields. | [optional] 
 **result_offset** | **int**| Specified the offset of events to return. | [optional] [default to 0]

### Return type

[**ApiEventQueryResult**](ApiEventQueryResult.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

