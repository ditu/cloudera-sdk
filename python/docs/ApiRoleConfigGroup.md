# ApiRoleConfigGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Readonly. The unique name of this role config group. | [optional] 
**role_type** | **str** | Readonly. The type of the roles in this group. | [optional] 
**base** | **bool** | Readonly. Indicates whether this is a base group. | [optional] 
**config** | [**ApiConfigList**](ApiConfigList.md) | The configuration for this group. Optional. | [optional] 
**display_name** | **str** | The display name of this group. | [optional] 
**service_ref** | [**ApiServiceRef**](ApiServiceRef.md) | Readonly. The service reference (service name and cluster name) of this group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


