# ApiImpalaUDF

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**database** | **str** | Name of the database to which this UDF belongs. | [optional] 
**signature** | **str** | UDF signature, includes the UDF name and parameter types. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


