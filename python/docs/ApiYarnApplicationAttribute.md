# ApiYarnApplicationAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the attribute. This name can be used in filters, for example &#39;user&#39; could be used in the filter &#39;user &#x3D; root&#39;. | [optional] 
**type** | **str** | The type of the attribute. Valid types are STRING, NUMBER, BOOLEAN, BYTES, MILLISECONDS, BYTES_PER_SECOND, BYTE_SECONDS. | [optional] 
**display_name** | **str** | The display name for the attribute. | [optional] 
**supports_histograms** | **bool** | Whether the Service Monitor can generate a histogram of the distribution of the attribute across applications. | [optional] 
**description** | **str** | The description of the attribute. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


