# cm_client.AuthRolesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_auth_roles**](AuthRolesResourceApi.md#create_auth_roles) | **POST** /authRoles | Creates a list of auth roles.
[**delete_auth_role**](AuthRolesResourceApi.md#delete_auth_role) | **DELETE** /authRoles/{uuid} | Deletes an auth role from the system.
[**read_auth_role**](AuthRolesResourceApi.md#read_auth_role) | **GET** /authRoles/{uuid} | Returns detailed information about an auth role.
[**read_auth_roles**](AuthRolesResourceApi.md#read_auth_roles) | **GET** /authRoles | Returns a list of the auth roles configured in the system.
[**read_auth_roles_metadata**](AuthRolesResourceApi.md#read_auth_roles_metadata) | **GET** /authRoles/metadata | Returns a list of the auth roles&#39; metadata for the built-in roles.
[**update_auth_role**](AuthRolesResourceApi.md#update_auth_role) | **PUT** /authRoles/{uuid} | Updates the given auth role&#39;s information.


<a name="create_auth_roles" id="create_auth_roles"></a>
# **create_auth_roles**
> ApiAuthRoleList create_auth_roles(body=body)

Creates a list of auth roles.

Creates a list of auth roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthRolesResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiAuthRoleList() # ApiAuthRoleList | List of auth roles to create. (optional)

try:
    # Creates a list of auth roles.
    api_response = api_instance.create_auth_roles(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthRolesResourceApi->create_auth_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiAuthRoleList**](ApiAuthRoleList.md)| List of auth roles to create. | [optional] 

### Return type

[**ApiAuthRoleList**](ApiAuthRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_auth_role" id="delete_auth_role"></a>
# **delete_auth_role**
> ApiAuthRole delete_auth_role(uuid)

Deletes an auth role from the system.

Deletes an auth role from the system. <p/>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthRolesResourceApi(cm_client.ApiClient(configuration))
uuid = 'uuid_example' # str | The uuid of the auth role to delete.

try:
    # Deletes an auth role from the system.
    api_response = api_instance.delete_auth_role(uuid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthRolesResourceApi->delete_auth_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| The uuid of the auth role to delete. | 

### Return type

[**ApiAuthRole**](ApiAuthRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_auth_role" id="read_auth_role"></a>
# **read_auth_role**
> ApiAuthRole read_auth_role(uuid)

Returns detailed information about an auth role.

Returns detailed information about an auth role.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthRolesResourceApi(cm_client.ApiClient(configuration))
uuid = 'uuid_example' # str | The auth role to read.

try:
    # Returns detailed information about an auth role.
    api_response = api_instance.read_auth_role(uuid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthRolesResourceApi->read_auth_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| The auth role to read. | 

### Return type

[**ApiAuthRole**](ApiAuthRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_auth_roles" id="read_auth_roles"></a>
# **read_auth_roles**
> ApiAuthRoleList read_auth_roles(view=view)

Returns a list of the auth roles configured in the system.

Returns a list of the auth roles configured in the system.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthRolesResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    # Returns a list of the auth roles configured in the system.
    api_response = api_instance.read_auth_roles(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthRolesResourceApi->read_auth_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiAuthRoleList**](ApiAuthRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_auth_roles_metadata" id="read_auth_roles_metadata"></a>
# **read_auth_roles_metadata**
> ApiAuthRoleMetadataList read_auth_roles_metadata(view=view)

Returns a list of the auth roles' metadata for the built-in roles.

Returns a list of the auth roles' metadata for the built-in roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthRolesResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    # Returns a list of the auth roles' metadata for the built-in roles.
    api_response = api_instance.read_auth_roles_metadata(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthRolesResourceApi->read_auth_roles_metadata: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiAuthRoleMetadataList**](ApiAuthRoleMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_auth_role" id="update_auth_role"></a>
# **update_auth_role**
> ApiAuthRole update_auth_role(uuid, body=body)

Updates the given auth role's information.

Updates the given auth role's information.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthRolesResourceApi(cm_client.ApiClient(configuration))
uuid = 'uuid_example' # str | Uuid of the auth role being updated.
body = cm_client.ApiAuthRole() # ApiAuthRole | The auth role information. (optional)

try:
    # Updates the given auth role's information.
    api_response = api_instance.update_auth_role(uuid, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthRolesResourceApi->update_auth_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| Uuid of the auth role being updated. | 
 **body** | [**ApiAuthRole**](ApiAuthRole.md)| The auth role information. | [optional] 

### Return type

[**ApiAuthRole**](ApiAuthRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

