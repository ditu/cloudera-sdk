# ApiMetric

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the metric. | [optional] 
**context** | **str** | Context the metric is associated with. | [optional] 
**unit** | **str** | Unit of the metric values. | [optional] 
**data** | [**list[ApiMetricData]**](ApiMetricData.md) | List of readings retrieved from the monitors. | [optional] 
**display_name** | **str** | Requires \&quot;full\&quot; view. User-friendly display name for the metric. | [optional] 
**description** | **str** | Requires \&quot;full\&quot; view. Description of the metric. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


