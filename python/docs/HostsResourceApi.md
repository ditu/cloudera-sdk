# cm_client.HostsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_hosts**](HostsResourceApi.md#create_hosts) | **POST** /hosts | .
[**delete_all_hosts**](HostsResourceApi.md#delete_all_hosts) | **DELETE** /hosts | Delete all hosts in the system.
[**delete_host**](HostsResourceApi.md#delete_host) | **DELETE** /hosts/{hostId} | Delete a host from the system.
[**enter_maintenance_mode**](HostsResourceApi.md#enter_maintenance_mode) | **POST** /hosts/{hostId}/commands/enterMaintenanceMode | Put the host into maintenance mode.
[**exit_maintenance_mode**](HostsResourceApi.md#exit_maintenance_mode) | **POST** /hosts/{hostId}/commands/exitMaintenanceMode | Take the host out of maintenance mode.
[**generate_host_certs**](HostsResourceApi.md#generate_host_certs) | **POST** /hosts/{hostId}/commands/generateHostCerts | Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.
[**get_metrics**](HostsResourceApi.md#get_metrics) | **GET** /hosts/{hostId}/metrics | Fetch metric readings for a host.
[**migrate_roles**](HostsResourceApi.md#migrate_roles) | **POST** /hosts/{hostId}/commands/migrateRoles | Migrate roles to a different host.
[**read_host**](HostsResourceApi.md#read_host) | **GET** /hosts/{hostId} | Returns a specific Host in the system.
[**read_host_config**](HostsResourceApi.md#read_host_config) | **GET** /hosts/{hostId}/config | Retrieves the configuration of a specific host.
[**read_hosts**](HostsResourceApi.md#read_hosts) | **GET** /hosts | Returns the hostIds for all hosts in the system.
[**update_host**](HostsResourceApi.md#update_host) | **PUT** /hosts/{hostId} | .
[**update_host_config**](HostsResourceApi.md#update_host_config) | **PUT** /hosts/{hostId}/config | Updates the host configuration with the given values.


<a name="create_hosts" id="create_hosts"></a>
# **create_hosts**
> ApiHostList create_hosts(body=body)

.

<p>Create one or more hosts.</p> <p>You must specify at least the hostname and ipAddress in the request objects. If no hostId is specified, it will be set to the hostname.  It is an error to try and create host with the same hostId as another host.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiHostList() # ApiHostList | The list of hosts to create (optional)

try:
    # .
    api_response = api_instance.create_hosts(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->create_hosts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostList**](ApiHostList.md)| The list of hosts to create | [optional] 

### Return type

[**ApiHostList**](ApiHostList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_all_hosts" id="delete_all_hosts"></a>
# **delete_all_hosts**
> ApiHostList delete_all_hosts()

Delete all hosts in the system.

Delete all hosts in the system

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))

try:
    # Delete all hosts in the system.
    api_response = api_instance.delete_all_hosts()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->delete_all_hosts: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiHostList**](ApiHostList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_host" id="delete_host"></a>
# **delete_host**
> ApiHost delete_host(host_id)

Delete a host from the system.

Delete a host from the system

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host to remove

try:
    # Delete a host from the system.
    api_response = api_instance.delete_host(host_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->delete_host: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host to remove | 

### Return type

[**ApiHost**](ApiHost.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enter_maintenance_mode" id="enter_maintenance_mode"></a>
# **enter_maintenance_mode**
> ApiCommand enter_maintenance_mode(host_id)

Put the host into maintenance mode.

Put the host into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v2.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host

try:
    # Put the host into maintenance mode.
    api_response = api_instance.enter_maintenance_mode(host_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->enter_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="exit_maintenance_mode" id="exit_maintenance_mode"></a>
# **exit_maintenance_mode**
> ApiCommand exit_maintenance_mode(host_id)

Take the host out of maintenance mode.

Take the host out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host

try:
    # Take the host out of maintenance mode.
    api_response = api_instance.exit_maintenance_mode(host_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->exit_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="generate_host_certs" id="generate_host_certs"></a>
# **generate_host_certs**
> ApiCommand generate_host_certs(host_id, body=body)

Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.

Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host to generate a certificate for.
body = cm_client.ApiGenerateHostCertsArguments() # ApiGenerateHostCertsArguments |  (optional)

try:
    # Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.
    api_response = api_instance.generate_host_certs(host_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->generate_host_certs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host to generate a certificate for. | 
 **body** | [**ApiGenerateHostCertsArguments**](ApiGenerateHostCertsArguments.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_metrics" id="get_metrics"></a>
# **get_metrics**
> ApiMetricList get_metrics(host_id, _from=_from, ifs=ifs, metrics=metrics, query_nw=query_nw, query_storage=query_storage, storage_ids=storage_ids, to=to, view=view)

Fetch metric readings for a host.

Fetch metric readings for a host. <p> By default, this call will look up all metrics available for the host. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the role, even if no readings are available in the requested window. <p> Host metrics also include per-network interface and per-storage device metrics. Since collecting this data incurs in more overhead, query parameters can be used to choose which network interfaces and storage devices to query, or to these metrics altogether. <p> Storage metrics are collected at different levels; for example, per-disk and per-partition metrics are available. The \"storageIds\" parameter can be used to filter specific storage IDs. <p> In the returned data, the network interfaces and storage IDs can be identified by looking at the \"context\" property of the metric objects.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The host's ID.
_from = '_from_example' # str | Start of the period to query. (optional)
ifs = ['ifs_example'] # list[str] | Network interfaces to query for metrics (default = all). (optional)
metrics = ['metrics_example'] # list[str] | Filter for which metrics to query. (optional)
query_nw = true # bool | Whether to query for network interface metrics. (optional) (default to true)
query_storage = true # bool | Whether to query for storage metrics. (optional) (default to true)
storage_ids = ['storage_ids_example'] # list[str] | Storage context IDs to query for metrics (default = all). (optional)
to = 'now' # str | End of the period to query. (optional) (default to now)
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Fetch metric readings for a host.
    api_response = api_instance.get_metrics(host_id, _from=_from, ifs=ifs, metrics=metrics, query_nw=query_nw, query_storage=query_storage, storage_ids=storage_ids, to=to, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->get_metrics: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The host's ID. | 
 **_from** | **str**| Start of the period to query. | [optional] 
 **ifs** | [**list[str]**](str.md)| Network interfaces to query for metrics (default = all). | [optional] 
 **metrics** | [**list[str]**](str.md)| Filter for which metrics to query. | [optional] 
 **query_nw** | **bool**| Whether to query for network interface metrics. | [optional] [default to true]
 **query_storage** | **bool**| Whether to query for storage metrics. | [optional] [default to true]
 **storage_ids** | [**list[str]**](str.md)| Storage context IDs to query for metrics (default = all). | [optional] 
 **to** | **str**| End of the period to query. | [optional] [default to now]
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="migrate_roles" id="migrate_roles"></a>
# **migrate_roles**
> ApiCommand migrate_roles(host_id, body=body)

Migrate roles to a different host.

Migrate roles to a different host. <p> This command applies only to HDFS NameNode, JournalNode, and Failover Controller roles. In order to migrate these roles: <ul> <li>HDFS High Availability must be enabled, using quorum-based storage.</li> <li>HDFS must not be configured to use a federated nameservice.</li> </ul> <b>Migrating a NameNode or JournalNode role requires cluster downtime</b>. HDFS, along with all of its dependent services, will be stopped at the beginning of the migration process, and restarted at its conclusion. <p>If the active NameNode is selected for migration, a manual failover will be performed before the role is migrated. The role will remain in standby mode after the migration is complete. <p>When migrating a NameNode role, the co-located Failover Controller role must be migrated as well if automatic failover is enabled. The Failover Controller role name must be included in the list of role names to migrate specified in the arguments to this command (it will not be included implicitly). This command does not allow a Failover Controller role to be moved by itself, although it is possible to move a JournalNode independently. <p> Available since API v10.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host on which the roles to migrate currently reside
body = cm_client.ApiMigrateRolesArguments() # ApiMigrateRolesArguments | Arguments for the command. (optional)

try:
    # Migrate roles to a different host.
    api_response = api_instance.migrate_roles(host_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->migrate_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host on which the roles to migrate currently reside | 
 **body** | [**ApiMigrateRolesArguments**](ApiMigrateRolesArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_host" id="read_host"></a>
# **read_host**
> ApiHost read_host(host_id, view=view)

Returns a specific Host in the system.

Returns a specific Host in the system.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host to read.
view = 'full' # str | The view to materialize. Defaults to 'full'. (optional) (default to full)

try:
    # Returns a specific Host in the system.
    api_response = api_instance.read_host(host_id, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->read_host: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host to read. | 
 **view** | **str**| The view to materialize. Defaults to 'full'. | [optional] [default to full]

### Return type

[**ApiHost**](ApiHost.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_host_config" id="read_host_config"></a>
# **read_host_config**
> ApiConfigList read_host_config(host_id, view=view)

Retrieves the configuration of a specific host.

Retrieves the configuration of a specific host.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Retrieves the configuration of a specific host.
    api_response = api_instance.read_host_config(host_id, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->read_host_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_hosts" id="read_hosts"></a>
# **read_hosts**
> ApiHostList read_hosts(view=view)

Returns the hostIds for all hosts in the system.

Returns the hostIds for all hosts in the system.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str | The view to materialize (optional) (default to summary)

try:
    # Returns the hostIds for all hosts in the system.
    api_response = api_instance.read_hosts(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->read_hosts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| The view to materialize | [optional] [default to summary]

### Return type

[**ApiHostList**](ApiHostList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_host" id="update_host"></a>
# **update_host**
> ApiHost update_host(host_id, body=body)

.

<p>Update an existing host in the system.</p> <p>Currently, only updating the rackId is supported.  All other fields of the host will be ignored.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The hostId to update
body = cm_client.ApiHost() # ApiHost | The updated host object. (optional)

try:
    # .
    api_response = api_instance.update_host(host_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->update_host: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The hostId to update | 
 **body** | [**ApiHost**](ApiHost.md)| The updated host object. | [optional] 

### Return type

[**ApiHost**](ApiHost.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_host_config" id="update_host_config"></a>
# **update_host_config**
> ApiConfigList update_host_config(host_id, message=message, body=body)

Updates the host configuration with the given values.

Updates the host configuration with the given values. <p> If a value is set in the given configuration, it will be added to the host's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostsResourceApi(cm_client.ApiClient(configuration))
host_id = 'host_id_example' # str | The ID of the host.
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | Configuration changes. (optional)

try:
    # Updates the host configuration with the given values.
    api_response = api_instance.update_host_config(host_id, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsResourceApi->update_host_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_id** | **str**| The ID of the host. | 
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| Configuration changes. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

