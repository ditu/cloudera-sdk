# ApiBatchResponseElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_code** | **float** | Read-only. The HTTP status code of the response. | [optional] 
**response** | **object** | Read-only. The (optional) serialized body of the response, in the representation produced by the corresponding API endpoint, such as application/json. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


