# ApiHBaseSnapshotResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processed_table_count** | **float** | Number of processed tables. | [optional] 
**processed_tables** | **list[str]** | The list of processed tables. &lt;p/&gt; This is only available in the full view. | [optional] 
**unprocessed_table_count** | **float** | Number of unprocessed tables. | [optional] 
**unprocessed_tables** | **list[str]** | The list of unprocessed tables. Note that tables that are currently being processed will also be included in this list. &lt;p/&gt; This is only available in the full view. | [optional] 
**created_snapshot_count** | **float** | Number of snapshots created. | [optional] 
**created_snapshots** | [**list[ApiHBaseSnapshot]**](ApiHBaseSnapshot.md) | List of snapshots created. &lt;p/&gt; This is only available in the full view. | [optional] 
**deleted_snapshot_count** | **float** | Number of snapshots deleted. | [optional] 
**deleted_snapshots** | [**list[ApiHBaseSnapshot]**](ApiHBaseSnapshot.md) | List of snapshots deleted. &lt;p/&gt; This is only available in the full view. | [optional] 
**creation_error_count** | **float** | Number of errors detected when creating snapshots. | [optional] 
**creation_errors** | [**list[ApiHBaseSnapshotError]**](ApiHBaseSnapshotError.md) | List of errors encountered when creating snapshots. &lt;p/&gt; This is only available in the full view. | [optional] 
**deletion_error_count** | **float** | Number of errors detected when deleting snapshots. | [optional] 
**deletion_errors** | [**list[ApiHBaseSnapshotError]**](ApiHBaseSnapshotError.md) | List of errors encountered when deleting snapshots. &lt;p/&gt; This is only available in the full view. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


