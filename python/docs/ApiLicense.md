# ApiLicense

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner** | **str** | The owner (organization name) of the license. | [optional] 
**uuid** | **str** | A UUID of this license. | [optional] 
**expiration** | **str** | The expiration date. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


