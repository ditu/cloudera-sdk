# ApiExternalUserMappingRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The uuid of the external user mapping, which uniquely identifies it in a CM installation. | [optional] 
**name** | **str** | The name of the mapping. | [optional] 
**type** | [**ApiExternalUserMappingType**](ApiExternalUserMappingType.md) | The type of the mapping. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


