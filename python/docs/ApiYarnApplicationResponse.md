# ApiYarnApplicationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applications** | [**list[ApiYarnApplication]**](ApiYarnApplication.md) | The list of applications for this response. | [optional] 
**warnings** | **list[str]** | This list of warnings for this response. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


