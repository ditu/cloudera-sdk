# ApiCommandMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of of the command. | [optional] 
**arg_schema** | **str** | The command arguments schema.  This is in the form of json schema and describes the structure of the command arguments. If null, the command does not take arguments. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


