# ApiShutdownReadiness

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | [**ShutdownReadinessState**](ShutdownReadinessState.md) | Shutdown readiness state | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


