# ApiDisableNnHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_nn_name** | **str** | Name of the NamdeNode role that is going to be active after High Availability is disabled. | [optional] 
**snn_host_id** | **str** | Id of the host where the new SecondaryNameNode will be created. | [optional] 
**snn_checkpoint_dir_list** | **list[str]** | List of directories used for checkpointing by the new SecondaryNameNode. | [optional] 
**snn_name** | **str** | Name of the new SecondaryNameNode role (Optional). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


