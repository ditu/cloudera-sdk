# ApiTimeSeriesEntityType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Returns the name of the entity type. This name uniquely identifies this entity type. | [optional] 
**category** | **str** | Returns the category of the entity type. | [optional] 
**name_for_cross_entity_aggregate_metrics** | **str** | Returns the string to use to pluralize the name of the entity for cross entity aggregate metrics. | [optional] 
**display_name** | **str** | Returns the display name of the entity type. | [optional] 
**description** | **str** | Returns the description of the entity type. | [optional] 
**immutable_attribute_names** | **list[str]** | Returns the list of immutable attributes for this entity type. Immutable attributes values for an entity may not change over its lifetime. | [optional] 
**mutable_attribute_names** | **list[str]** | Returns the list of mutable attributes for this entity type. Mutable attributes for an entity may change over its lifetime. | [optional] 
**entity_name_format** | **list[str]** | Returns a list of attribute names that will be used to construct entity names for entities of this type. The attributes named here must be immutable attributes of this type or a parent type. | [optional] 
**entity_display_name_format** | **str** | Returns a format string that will be used to construct the display name of entities of this type. If this returns null the entity name would be used as the display name.  The entity attribute values are used to replace $attribute name portions of this format string. For example, an entity with roleType \&quot;DATANODE\&quot; and hostname \&quot;foo.com\&quot; will have a display name \&quot;DATANODE (foo.com)\&quot; if the format is \&quot;$roleType ($hostname)\&quot;. | [optional] 
**parent_metric_entity_type_names** | **list[str]** | Returns a list of metric entity type names which are parents of this metric entity type. A metric entity type inherits the attributes of its ancestors. For example a role metric entity type has its service as a parent. A service metric entity type has a cluster as a parent. The role type inherits its cluster name attribute through its service parent. Only parent ancestors should be returned here. In the example given, only the service metric entity type should be specified in the parent list. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


