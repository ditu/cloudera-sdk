# ApiYarnApplicationDiagnosticsCollectionArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_ids** | **list[str]** | Id&#39;s of the applications whose diagnostics data has to be collected | [optional] 
**ticket_number** | **str** | Ticket Number of the Cloudera Support Ticket | [optional] 
**comments** | **str** | Comments to add to the support bundle | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


