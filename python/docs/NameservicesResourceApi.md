# cm_client.NameservicesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_metrics**](NameservicesResourceApi.md#get_metrics) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices/{nameservice}/metrics | Fetch metric readings for a particular nameservice.
[**list_nameservices**](NameservicesResourceApi.md#list_nameservices) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices | List the nameservices of an HDFS service.
[**read_nameservice**](NameservicesResourceApi.md#read_nameservice) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices/{nameservice} | Retrieve information about a nameservice.


<a name="get_metrics" id="get_metrics"></a>
# **get_metrics**
> ApiMetricList get_metrics(cluster_name, nameservice, service_name, _from=_from, metrics=metrics, to=to, view=view)

Fetch metric readings for a particular nameservice.

Fetch metric readings for a particular nameservice. <p> By default, this call will look up all metrics available. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available, even if no readings are available in the requested window.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.NameservicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
nameservice = 'nameservice_example' # str | The nameservice.
service_name = 'service_name_example' # str | The service name.
_from = '_from_example' # str | Start of the period to query. (optional)
metrics = ['metrics_example'] # list[str] | Filter for which metrics to query. (optional)
to = 'now' # str | End of the period to query. (optional) (default to now)
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Fetch metric readings for a particular nameservice.
    api_response = api_instance.get_metrics(cluster_name, nameservice, service_name, _from=_from, metrics=metrics, to=to, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NameservicesResourceApi->get_metrics: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **nameservice** | **str**| The nameservice. | 
 **service_name** | **str**| The service name. | 
 **_from** | **str**| Start of the period to query. | [optional] 
 **metrics** | [**list[str]**](str.md)| Filter for which metrics to query. | [optional] 
 **to** | **str**| End of the period to query. | [optional] [default to now]
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_nameservices" id="list_nameservices"></a>
# **list_nameservices**
> ApiNameserviceList list_nameservices(cluster_name, service_name, view=view)

List the nameservices of an HDFS service.

List the nameservices of an HDFS service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.NameservicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List the nameservices of an HDFS service.
    api_response = api_instance.list_nameservices(cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NameservicesResourceApi->list_nameservices: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiNameserviceList**](ApiNameserviceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_nameservice" id="read_nameservice"></a>
# **read_nameservice**
> ApiNameservice read_nameservice(cluster_name, nameservice, service_name, view=view)

Retrieve information about a nameservice.

Retrieve information about a nameservice.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.NameservicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
nameservice = 'nameservice_example' # str | The nameservice to retrieve.
service_name = 'service_name_example' # str | The service name.
view = 'summary' # str | The view to materialize. Defaults to 'full'. (optional) (default to summary)

try:
    # Retrieve information about a nameservice.
    api_response = api_instance.read_nameservice(cluster_name, nameservice, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NameservicesResourceApi->read_nameservice: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **nameservice** | **str**| The nameservice to retrieve. | 
 **service_name** | **str**| The service name. | 
 **view** | **str**| The view to materialize. Defaults to 'full'. | [optional] [default to summary]

### Return type

[**ApiNameservice**](ApiNameservice.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

