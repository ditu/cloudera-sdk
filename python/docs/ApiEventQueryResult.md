# ApiEventQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_results** | **float** | The total number of matched results. Some are possibly not shown due to pagination. | [optional] 
**items** | [**list[ApiEvent]**](ApiEvent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


