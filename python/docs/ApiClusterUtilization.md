# ApiClusterUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_cpu_cores** | **float** | Average number of CPU cores available in the cluster during the report window. | [optional] 
**avg_cpu_utilization** | **float** | Average CPU consumption for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. | [optional] 
**max_cpu_utilization** | **float** | Maximum CPU consumption for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. | [optional] 
**avg_cpu_daily_peak** | **float** | Average daily peak CPU consumption for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. | [optional] 
**avg_workload_cpu** | **float** | Average CPU consumption by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. | [optional] 
**max_workload_cpu** | **float** | Maximum CPU consumption by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. | [optional] 
**avg_workload_cpu_daily_peak** | **float** | Average daily peak CPU consumption by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. | [optional] 
**total_memory** | **float** | Average physical memory (in bytes) available in the cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. | [optional] 
**avg_memory_utilization** | **float** | Average memory consumption (as percentage of total memory) for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. | [optional] 
**max_memory_utilization** | **float** | Maximum memory consumption (as percentage of total memory) for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. | [optional] 
**avg_memory_daily_peak** | **float** | Average daily peak memory consumption (as percentage of total memory) for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. | [optional] 
**avg_workload_memory** | **float** | Average memory consumption (as percentage of total memory) by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. | [optional] 
**max_workload_memory** | **float** | Maximum memory consumption (as percentage of total memory) by workloads that ran on the cluster. This includes consumption by user workloads in YARN and Impala | [optional] 
**avg_workload_memory_daily_peak** | **float** | Average daily peak memory consumption (as percentage of total memory) by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. | [optional] 
**tenant_utilizations** | [**ApiTenantUtilizationList**](ApiTenantUtilizationList.md) | A list of tenant utilization reports. | [optional] 
**max_cpu_utilization_timestamp_ms** | **float** | Timestamp corresponding to maximum CPU utilization for the entire cluster during the report window. | [optional] 
**max_memory_utilization_timestamp_ms** | **float** | Timestamp corresponding to maximum memory utilization for the entire cluster during the report window. | [optional] 
**max_workload_cpu_timestamp_ms** | **float** | Timestamp corresponds to maximum CPU consumption by workloads that ran on the cluster during the report window. | [optional] 
**max_workload_memory_timestamp_ms** | **float** | Timestamp corresponds to maximum memory resource consumption by workloads that ran on the cluster during the report window. | [optional] 
**error_message** | **str** | Error message while generating utilization report. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


