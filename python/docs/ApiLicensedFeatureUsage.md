# ApiLicensedFeatureUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totals** | **dict(str, float)** | Map from named features to the total number of nodes using those features. | [optional] 
**clusters** | **dict(str, object)** | Map from clusters to maps of named features to the number of nodes in the cluster using those features. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


