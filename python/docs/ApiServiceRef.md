# ApiServiceRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peer_name** | **str** | The name of the CM peer corresponding to the remote CM that manages the referenced service. This should only be set when referencing a remote service. | [optional] 
**cluster_name** | **str** | The enclosing cluster for this service. | [optional] 
**service_name** | **str** | The service name. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


