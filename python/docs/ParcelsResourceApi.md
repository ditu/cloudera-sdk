# cm_client.ParcelsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_parcel_usage**](ParcelsResourceApi.md#get_parcel_usage) | **GET** /clusters/{clusterName}/parcels/usage | Retrieve details parcel usage information for the cluster.
[**read_parcels**](ParcelsResourceApi.md#read_parcels) | **GET** /clusters/{clusterName}/parcels | Lists all parcels that the cluster has access to.


<a name="get_parcel_usage" id="get_parcel_usage"></a>
# **get_parcel_usage**
> ApiParcelUsage get_parcel_usage(cluster_name)

Retrieve details parcel usage information for the cluster.

Retrieve details parcel usage information for the cluster. This describes which processes, roles and hosts are using which parcels.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 

try:
    # Retrieve details parcel usage information for the cluster.
    api_response = api_instance.get_parcel_usage(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelsResourceApi->get_parcel_usage: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 

### Return type

[**ApiParcelUsage**](ApiParcelUsage.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_parcels" id="read_parcels"></a>
# **read_parcels**
> ApiParcelList read_parcels(cluster_name, view=view)

Lists all parcels that the cluster has access to.

Lists all parcels that the cluster has access to.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
view = 'summary' # str |  (optional) (default to summary)

try:
    # Lists all parcels that the cluster has access to.
    api_response = api_instance.read_parcels(cluster_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelsResourceApi->read_parcels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiParcelList**](ApiParcelList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

