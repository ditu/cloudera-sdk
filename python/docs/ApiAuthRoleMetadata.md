# ApiAuthRoleMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 
**role** | **str** |  | [optional] 
**authorities** | [**list[ApiAuthRoleAuthority]**](ApiAuthRoleAuthority.md) |  | [optional] 
**allowed_scopes** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


