# cm_client.ClustersResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_hosts**](ClustersResourceApi.md#add_hosts) | **POST** /clusters/{clusterName}/hosts | 
[**auto_assign_roles**](ClustersResourceApi.md#auto_assign_roles) | **PUT** /clusters/{clusterName}/autoAssignRoles | Automatically assign roles to hosts and create the roles for all the services in a cluster.
[**auto_configure**](ClustersResourceApi.md#auto_configure) | **PUT** /clusters/{clusterName}/autoConfigure | Automatically configures roles and services in a cluster.
[**configure_for_kerberos**](ClustersResourceApi.md#configure_for_kerberos) | **POST** /clusters/{clusterName}/commands/configureForKerberos | Command to configure the cluster to use Kerberos for authentication.
[**create_clusters**](ClustersResourceApi.md#create_clusters) | **POST** /clusters | Creates a collection of clusters.
[**delete_cluster**](ClustersResourceApi.md#delete_cluster) | **DELETE** /clusters/{clusterName} | Deletes a cluster.
[**delete_cluster_credentials_command**](ClustersResourceApi.md#delete_cluster_credentials_command) | **POST** /clusters/{clusterName}/commands/deleteCredentials | Delete existing Kerberos credentials for the cluster.
[**deploy_client_config**](ClustersResourceApi.md#deploy_client_config) | **POST** /clusters/{clusterName}/commands/deployClientConfig | Deploy the cluster-wide client configuration.
[**deploy_cluster_client_config**](ClustersResourceApi.md#deploy_cluster_client_config) | **POST** /clusters/{clusterName}/commands/deployClusterClientConfig | Deploy the Cluster&#39;s Kerberos client configuration.
[**enter_maintenance_mode**](ClustersResourceApi.md#enter_maintenance_mode) | **POST** /clusters/{clusterName}/commands/enterMaintenanceMode | Put the cluster into maintenance mode.
[**exit_maintenance_mode**](ClustersResourceApi.md#exit_maintenance_mode) | **POST** /clusters/{clusterName}/commands/exitMaintenanceMode | Take the cluster out of maintenance mode.
[**expire_logs**](ClustersResourceApi.md#expire_logs) | **POST** /clusters/{clusterName}/commands/expireLogs | Remove backup and disaster related log files in hdfs.
[**export**](ClustersResourceApi.md#export) | **GET** /clusters/{clusterName}/export | Export the cluster template for the given cluster.
[**first_run**](ClustersResourceApi.md#first_run) | **POST** /clusters/{clusterName}/commands/firstRun | Prepare and start services in a cluster.
[**get_client_config**](ClustersResourceApi.md#get_client_config) | **GET** /clusters/{clusterName}/clientConfig | Download a zip-compressed archive of the client configuration, of a specific cluster.
[**get_kerberos_info**](ClustersResourceApi.md#get_kerberos_info) | **GET** /clusters/{clusterName}/kerberosInfo | Provides Cluster Kerberos information.
[**get_utilization_report**](ClustersResourceApi.md#get_utilization_report) | **GET** /clusters/{clusterName}/utilization | Provides the resource utilization of the entire cluster as well as the resource utilization per tenant.
[**inspect_hosts_command**](ClustersResourceApi.md#inspect_hosts_command) | **POST** /clusters/{clusterName}/commands/inspectHosts | Runs the host inspector on the configured hosts in the specified cluster.
[**list_active_commands**](ClustersResourceApi.md#list_active_commands) | **GET** /clusters/{clusterName}/commands | List active cluster commands.
[**list_dfs_services**](ClustersResourceApi.md#list_dfs_services) | **GET** /clusters/{clusterName}/dfsServices | List the services that can provide distributed file system (DFS) capabilities in this cluster.
[**list_hosts**](ClustersResourceApi.md#list_hosts) | **GET** /clusters/{clusterName}/hosts | 
[**list_service_types**](ClustersResourceApi.md#list_service_types) | **GET** /clusters/{clusterName}/serviceTypes | List the supported service types for a cluster.
[**perf_inspector_command**](ClustersResourceApi.md#perf_inspector_command) | **POST** /clusters/{clusterName}/commands/perfInspector | Run cluster performance diagnostics test.
[**pools_refresh**](ClustersResourceApi.md#pools_refresh) | **POST** /clusters/{clusterName}/commands/poolsRefresh | Updates all refreshable configuration files for services with Dynamic Resource Pools.
[**pre_upgrade_check_command**](ClustersResourceApi.md#pre_upgrade_check_command) | **POST** /clusters/{clusterName}/commands/preUpgradeCheck | Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other.
[**read_cluster**](ClustersResourceApi.md#read_cluster) | **GET** /clusters/{clusterName} | Reads information about a cluster.
[**read_clusters**](ClustersResourceApi.md#read_clusters) | **GET** /clusters | Lists all known clusters.
[**refresh**](ClustersResourceApi.md#refresh) | **POST** /clusters/{clusterName}/commands/refresh | Updates all refreshable configuration files in the cluster.
[**remove_all_hosts**](ClustersResourceApi.md#remove_all_hosts) | **DELETE** /clusters/{clusterName}/hosts | 
[**remove_host**](ClustersResourceApi.md#remove_host) | **DELETE** /clusters/{clusterName}/hosts/{hostId} | 
[**restart_command**](ClustersResourceApi.md#restart_command) | **POST** /clusters/{clusterName}/commands/restart | Restart all services in the cluster.
[**rolling_restart**](ClustersResourceApi.md#rolling_restart) | **POST** /clusters/{clusterName}/commands/rollingRestart | Command to do a \&quot;best-effort\&quot; rolling restart of the given cluster, i.
[**rolling_upgrade**](ClustersResourceApi.md#rolling_upgrade) | **POST** /clusters/{clusterName}/commands/rollingUpgrade | Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don&#39;t support rolling upgrades.
[**start_command**](ClustersResourceApi.md#start_command) | **POST** /clusters/{clusterName}/commands/start | Start all services in the cluster.
[**stop_command**](ClustersResourceApi.md#stop_command) | **POST** /clusters/{clusterName}/commands/stop | Stop all services in the cluster.
[**update_cluster**](ClustersResourceApi.md#update_cluster) | **PUT** /clusters/{clusterName} | Update an existing cluster.
[**upgrade_cdh_command**](ClustersResourceApi.md#upgrade_cdh_command) | **POST** /clusters/{clusterName}/commands/upgradeCdh | Perform CDH upgrade to the specified version.
[**upgrade_services_command**](ClustersResourceApi.md#upgrade_services_command) | **POST** /clusters/{clusterName}/commands/upgradeServices | Upgrades the services in the cluster to the CDH5 version.


<a name="add_hosts" id="add_hosts"></a>
# **add_hosts**
> ApiHostRefList add_hosts(cluster_name, body=body)





### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
body = cm_client.ApiHostRefList() # ApiHostRefList |  (optional)

try:
    api_response = api_instance.add_hosts(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->add_hosts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **body** | [**ApiHostRefList**](ApiHostRefList.md)|  | [optional] 

### Return type

[**ApiHostRefList**](ApiHostRefList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="auto_assign_roles" id="auto_assign_roles"></a>
# **auto_assign_roles**
> auto_assign_roles(cluster_name)

Automatically assign roles to hosts and create the roles for all the services in a cluster.

Automatically assign roles to hosts and create the roles for all the services in a cluster. <p> Assignments are done based on services and hosts in the cluster, and hardware specifications. If no hosts are added to the cluster, an exception will be thrown preventing any role assignments. Existing roles will be taken into account and their assignments will be not be modified. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.

try:
    # Automatically assign roles to hosts and create the roles for all the services in a cluster.
    api_instance.auto_assign_roles(cluster_name)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->auto_assign_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="auto_configure" id="auto_configure"></a>
# **auto_configure**
> auto_configure(cluster_name)

Automatically configures roles and services in a cluster.

Automatically configures roles and services in a cluster. <p> Overwrites some existing configurations. Might create new role config groups. Only default role config groups must exist before calling this endpoint. Other role config groups must not exist. If they do, an exception will be thrown preventing any configuration. Ignores the Cloudera Management Service even if colocated with roles of this cluster. To avoid over-committing the heap on hosts, assign hosts to this cluster that are not being used by the Cloudera Management Service. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.

try:
    # Automatically configures roles and services in a cluster.
    api_instance.auto_configure(cluster_name)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->auto_configure: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="configure_for_kerberos" id="configure_for_kerberos"></a>
# **configure_for_kerberos**
> ApiCommand configure_for_kerberos(cluster_name, body=body)

Command to configure the cluster to use Kerberos for authentication.

Command to configure the cluster to use Kerberos for authentication.  This command will configure all relevant services on a cluster for Kerberos usage.  This command will trigger a GenerateCredentials command to create Kerberos keytabs for all roles in the cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiConfigureForKerberosArguments() # ApiConfigureForKerberosArguments | Arguments for the configure for kerberos command. (optional)

try:
    # Command to configure the cluster to use Kerberos for authentication.
    api_response = api_instance.configure_for_kerberos(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->configure_for_kerberos: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiConfigureForKerberosArguments**](ApiConfigureForKerberosArguments.md)| Arguments for the configure for kerberos command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_clusters" id="create_clusters"></a>
# **create_clusters**
> ApiClusterList create_clusters(body=body)

Creates a collection of clusters.

Creates a collection of clusters.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiClusterList() # ApiClusterList | List of clusters to created. (optional)

try:
    # Creates a collection of clusters.
    api_response = api_instance.create_clusters(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->create_clusters: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiClusterList**](ApiClusterList.md)| List of clusters to created. | [optional] 

### Return type

[**ApiClusterList**](ApiClusterList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_cluster" id="delete_cluster"></a>
# **delete_cluster**
> ApiCluster delete_cluster(cluster_name)

Deletes a cluster.

Deletes a cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | Name of cluster to delete.

try:
    # Deletes a cluster.
    api_response = api_instance.delete_cluster(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->delete_cluster: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| Name of cluster to delete. | 

### Return type

[**ApiCluster**](ApiCluster.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_cluster_credentials_command" id="delete_cluster_credentials_command"></a>
# **delete_cluster_credentials_command**
> ApiCommand delete_cluster_credentials_command(cluster_name)

Delete existing Kerberos credentials for the cluster.

Delete existing Kerberos credentials for the cluster. <p> This command will affect all services that have been configured to use Kerberos, and have existing credentials.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | cluster name

try:
    # Delete existing Kerberos credentials for the cluster.
    api_response = api_instance.delete_cluster_credentials_command(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->delete_cluster_credentials_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| cluster name | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deploy_client_config" id="deploy_client_config"></a>
# **deploy_client_config**
> ApiCommand deploy_client_config(cluster_name)

Deploy the cluster-wide client configuration.

Deploy the cluster-wide client configuration.  <p>For each service in the cluster, deploy the service's client configuration to all the hosts that the service runs on.</p>  <p>Available since API v2.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster

try:
    # Deploy the cluster-wide client configuration.
    api_response = api_instance.deploy_client_config(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->deploy_client_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deploy_cluster_client_config" id="deploy_cluster_client_config"></a>
# **deploy_cluster_client_config**
> ApiCommand deploy_cluster_client_config(cluster_name, body=body)

Deploy the Cluster's Kerberos client configuration.

Deploy the Cluster's Kerberos client configuration.  <p> Deploy krb5.conf to hosts in a cluster. Does not deploy to decommissioned hosts or hosts with active processes. </p>  <p> Available since API v7. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster
body = cm_client.ApiHostRefList() # ApiHostRefList | Hosts in cluster to deploy to. If empty, will target all eligible hosts in the cluster. (optional)

try:
    # Deploy the Cluster's Kerberos client configuration.
    api_response = api_instance.deploy_cluster_client_config(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->deploy_cluster_client_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 
 **body** | [**ApiHostRefList**](ApiHostRefList.md)| Hosts in cluster to deploy to. If empty, will target all eligible hosts in the cluster. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enter_maintenance_mode" id="enter_maintenance_mode"></a>
# **enter_maintenance_mode**
> ApiCommand enter_maintenance_mode(cluster_name)

Put the cluster into maintenance mode.

Put the cluster into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v2.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster

try:
    # Put the cluster into maintenance mode.
    api_response = api_instance.enter_maintenance_mode(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->enter_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="exit_maintenance_mode" id="exit_maintenance_mode"></a>
# **exit_maintenance_mode**
> ApiCommand exit_maintenance_mode(cluster_name)

Take the cluster out of maintenance mode.

Take the cluster out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v2.</p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster

try:
    # Take the cluster out of maintenance mode.
    api_response = api_instance.exit_maintenance_mode(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->exit_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="expire_logs" id="expire_logs"></a>
# **expire_logs**
> ApiCommand expire_logs(cluster_name, days=days)

Remove backup and disaster related log files in hdfs.

Remove backup and disaster related log files in hdfs. <p> Available since API v31.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster
days = 3.4 # float | logs more than these many days old are purged. -2 to use the existing setting (optional)

try:
    # Remove backup and disaster related log files in hdfs.
    api_response = api_instance.expire_logs(cluster_name, days=days)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->expire_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 
 **days** | **float**| logs more than these many days old are purged. -2 to use the existing setting | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="export" id="export"></a>
# **export**
> ApiClusterTemplate export(cluster_name, export_auto_config=export_auto_config)

Export the cluster template for the given cluster.

Export the cluster template for the given cluster. If cluster does not have host templates defined it will export host templates based on roles assignment.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | cluster name
export_auto_config = false # bool | export configs set by the auto configuration (optional) (default to false)

try:
    # Export the cluster template for the given cluster.
    api_response = api_instance.export(cluster_name, export_auto_config=export_auto_config)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->export: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| cluster name | 
 **export_auto_config** | **bool**| export configs set by the auto configuration | [optional] [default to false]

### Return type

[**ApiClusterTemplate**](ApiClusterTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="first_run" id="first_run"></a>
# **first_run**
> ApiCommand first_run(cluster_name)

Prepare and start services in a cluster.

Prepare and start services in a cluster.  <p> Perform all the steps needed to prepare each service in a cluster and start the services in order. </p>  <p> Available since API v7. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.

try:
    # Prepare and start services in a cluster.
    api_response = api_instance.first_run(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->first_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_client_config" id="get_client_config"></a>
# **get_client_config**
> file get_client_config(cluster_name)

Download a zip-compressed archive of the client configuration, of a specific cluster.

Download a zip-compressed archive of the client configuration, of a specific cluster. Currently, this only includes Kerberos Client Configuration (krb5.conf). For client configuration of services, use the clientConfig endpoint of the services resource. This resource does not require any authentication.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The cluster name.

try:
    # Download a zip-compressed archive of the client configuration, of a specific cluster.
    api_response = api_instance.get_client_config(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->get_client_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The cluster name. | 

### Return type

[**file**](file.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_kerberos_info" id="get_kerberos_info"></a>
# **get_kerberos_info**
> ApiKerberosInfo get_kerberos_info(cluster_name)

Provides Cluster Kerberos information.

Provides Cluster Kerberos information

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The cluster name.

try:
    # Provides Cluster Kerberos information.
    api_response = api_instance.get_kerberos_info(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->get_kerberos_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The cluster name. | 

### Return type

[**ApiKerberosInfo**](ApiKerberosInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_utilization_report" id="get_utilization_report"></a>
# **get_utilization_report**
> ApiClusterUtilization get_utilization_report(cluster_name, days_of_week=days_of_week, end_hour_of_day=end_hour_of_day, _from=_from, start_hour_of_day=start_hour_of_day, tenant_type=tenant_type, to=to)

Provides the resource utilization of the entire cluster as well as the resource utilization per tenant.

Provides the resource utilization of the entire cluster as well as the resource utilization per tenant. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | cluster name
days_of_week = ['days_of_week_example'] # list[str] | The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Monday, and 7 corrensponds to Sunday. All 7 days are included if this is not specified. (optional)
end_hour_of_day = 23 # int | The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. (optional) (default to 23)
_from = '_from_example' # str | Start of the time range to report utilization in ISO 8601 format. (optional)
start_hour_of_day = 0 # int | The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. (optional) (default to 0)
tenant_type = 'POOL' # str | The type of the tenant (POOL or USER). (optional) (default to POOL)
to = 'now' # str | End of the the time range to report utilization in ISO 8601 format (defaults to now). (optional) (default to now)

try:
    # Provides the resource utilization of the entire cluster as well as the resource utilization per tenant.
    api_response = api_instance.get_utilization_report(cluster_name, days_of_week=days_of_week, end_hour_of_day=end_hour_of_day, _from=_from, start_hour_of_day=start_hour_of_day, tenant_type=tenant_type, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->get_utilization_report: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| cluster name | 
 **days_of_week** | [**list[str]**](str.md)| The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Monday, and 7 corrensponds to Sunday. All 7 days are included if this is not specified. | [optional] 
 **end_hour_of_day** | **int**| The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. | [optional] [default to 23]
 **_from** | **str**| Start of the time range to report utilization in ISO 8601 format. | [optional] 
 **start_hour_of_day** | **int**| The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. | [optional] [default to 0]
 **tenant_type** | **str**| The type of the tenant (POOL or USER). | [optional] [default to POOL]
 **to** | **str**| End of the the time range to report utilization in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiClusterUtilization**](ApiClusterUtilization.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="inspect_hosts_command" id="inspect_hosts_command"></a>
# **inspect_hosts_command**
> ApiCommand inspect_hosts_command(cluster_name)

Runs the host inspector on the configured hosts in the specified cluster.

Runs the host inspector on the configured hosts in the specified cluster.  Available since V8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster

try:
    # Runs the host inspector on the configured hosts in the specified cluster.
    api_response = api_instance.inspect_hosts_command(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->inspect_hosts_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_active_commands" id="list_active_commands"></a>
# **list_active_commands**
> ApiCommandList list_active_commands(cluster_name, view=view)

List active cluster commands.

List active cluster commands.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List active cluster commands.
    api_response = api_instance.list_active_commands(cluster_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->list_active_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_dfs_services" id="list_dfs_services"></a>
# **list_dfs_services**
> ApiServiceList list_dfs_services(cluster_name, view=view)

List the services that can provide distributed file system (DFS) capabilities in this cluster.

List the services that can provide distributed file system (DFS) capabilities in this cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | cluster name
view = 'summary' # str | data view required for matching services (optional) (default to summary)

try:
    # List the services that can provide distributed file system (DFS) capabilities in this cluster.
    api_response = api_instance.list_dfs_services(cluster_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->list_dfs_services: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| cluster name | 
 **view** | **str**| data view required for matching services | [optional] [default to summary]

### Return type

[**ApiServiceList**](ApiServiceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_hosts" id="list_hosts"></a>
# **list_hosts**
> ApiHostRefList list_hosts(cluster_name)





### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 

try:
    api_response = api_instance.list_hosts(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->list_hosts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 

### Return type

[**ApiHostRefList**](ApiHostRefList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_service_types" id="list_service_types"></a>
# **list_service_types**
> ApiServiceTypeList list_service_types(cluster_name)

List the supported service types for a cluster.

List the supported service types for a cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The cluster.

try:
    # List the supported service types for a cluster.
    api_response = api_instance.list_service_types(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->list_service_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The cluster. | 

### Return type

[**ApiServiceTypeList**](ApiServiceTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="perf_inspector_command" id="perf_inspector_command"></a>
# **perf_inspector_command**
> ApiCommand perf_inspector_command(cluster_name, body=body)

Run cluster performance diagnostics test.

Run cluster performance diagnostics test.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiClusterPerfInspectorArgs() # ApiClusterPerfInspectorArgs | Optional arguments for the command. See ApiClusterPerfInspectorArgs. (optional)

try:
    # Run cluster performance diagnostics test.
    api_response = api_instance.perf_inspector_command(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->perf_inspector_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiClusterPerfInspectorArgs**](ApiClusterPerfInspectorArgs.md)| Optional arguments for the command. See ApiClusterPerfInspectorArgs. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="pools_refresh" id="pools_refresh"></a>
# **pools_refresh**
> ApiCommand pools_refresh(cluster_name)

Updates all refreshable configuration files for services with Dynamic Resource Pools.

Updates all refreshable configuration files for services with Dynamic Resource Pools. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster

try:
    # Updates all refreshable configuration files for services with Dynamic Resource Pools.
    api_response = api_instance.pools_refresh(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->pools_refresh: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="pre_upgrade_check_command" id="pre_upgrade_check_command"></a>
# **pre_upgrade_check_command**
> ApiCommand pre_upgrade_check_command(cluster_name, body=body)

Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other.

Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other. <p> Allows the following upgrade checks: <ul> <li>Minor upgrades from any CDH 5 to any CDH 5</li> <li>Major upgrades from any CDH 5 to any CDH 6</li> <li>Minor upgrades from any CDH 6 to any CDH 6</li> <li>Maintenance upgrades or downgrades (a.b.x to a.b.y)</li> </ul>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiCdhUpgradeArgs() # ApiCdhUpgradeArgs | Arguments for the command. See ApiCdhUpgradeArgs. (optional)

try:
    # Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other.
    api_response = api_instance.pre_upgrade_check_command(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->pre_upgrade_check_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiCdhUpgradeArgs**](ApiCdhUpgradeArgs.md)| Arguments for the command. See ApiCdhUpgradeArgs. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_cluster" id="read_cluster"></a>
# **read_cluster**
> ApiCluster read_cluster(cluster_name)

Reads information about a cluster.

Reads information about a cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | Name of cluster to look up.

try:
    # Reads information about a cluster.
    api_response = api_instance.read_cluster(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->read_cluster: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| Name of cluster to look up. | 

### Return type

[**ApiCluster**](ApiCluster.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_clusters" id="read_clusters"></a>
# **read_clusters**
> ApiClusterList read_clusters(view=view)

Lists all known clusters.

Lists all known clusters.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    # Lists all known clusters.
    api_response = api_instance.read_clusters(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->read_clusters: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiClusterList**](ApiClusterList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="refresh" id="refresh"></a>
# **refresh**
> ApiCommand refresh(cluster_name)

Updates all refreshable configuration files in the cluster.

Updates all refreshable configuration files in the cluster. Will not restart any roles. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster

try:
    # Updates all refreshable configuration files in the cluster.
    api_response = api_instance.refresh(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->refresh: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="remove_all_hosts" id="remove_all_hosts"></a>
# **remove_all_hosts**
> ApiHostRefList remove_all_hosts(cluster_name)





### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 

try:
    api_response = api_instance.remove_all_hosts(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->remove_all_hosts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 

### Return type

[**ApiHostRefList**](ApiHostRefList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="remove_host" id="remove_host"></a>
# **remove_host**
> ApiHostRef remove_host(cluster_name, host_id)





### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
host_id = 'host_id_example' # str | 

try:
    api_response = api_instance.remove_host(cluster_name, host_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->remove_host: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **host_id** | **str**|  | 

### Return type

[**ApiHostRef**](ApiHostRef.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="restart_command" id="restart_command"></a>
# **restart_command**
> ApiCommand restart_command(cluster_name, body=body)

Restart all services in the cluster.

Restart all services in the cluster. <p> Services are stopped then started in the appropriate order given their dependencies. The command can optionally restart only stale services and their dependencies as well as redeploy client configuration on all services in the cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiRestartClusterArgs() # ApiRestartClusterArgs | arguments for the restart command. (optional)

try:
    # Restart all services in the cluster.
    api_response = api_instance.restart_command(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->restart_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiRestartClusterArgs**](ApiRestartClusterArgs.md)| arguments for the restart command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="rolling_restart" id="rolling_restart"></a>
# **rolling_restart**
> ApiCommand rolling_restart(cluster_name, body=body)

Command to do a \"best-effort\" rolling restart of the given cluster, i.

Command to do a \"best-effort\" rolling restart of the given cluster, i.e. it does plain restart of services that cannot be rolling restarted, followed by first rolling restarting non-slaves and then rolling restarting the slave roles of services that can be rolling restarted. The slave restarts are done host-by-host. <p> Available since API v4. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiRollingRestartClusterArgs() # ApiRollingRestartClusterArgs | Arguments for the rolling restart command. (optional)

try:
    # Command to do a \"best-effort\" rolling restart of the given cluster, i.
    api_response = api_instance.rolling_restart(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->rolling_restart: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiRollingRestartClusterArgs**](ApiRollingRestartClusterArgs.md)| Arguments for the rolling restart command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="rolling_upgrade" id="rolling_upgrade"></a>
# **rolling_upgrade**
> ApiCommand rolling_upgrade(cluster_name, body=body)

Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don't support rolling upgrades.

Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don't support rolling upgrades. The command will throw an error and not start if upgrade of any such service is requested.  This command does not upgrade the full CDH Cluster. You should normally use the upgradeCDH Command for upgrading the cluster. This is primarily helpful if you need to need to recover from an upgrade failure or for advanced users to script an alternative to the upgradeCdhCommand.  This command expects the binaries to be available on hosts and activated. It does not change any binaries on the hosts.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiRollingUpgradeServicesArgs() # ApiRollingUpgradeServicesArgs | Arguments for the rolling upgrade command. (optional)

try:
    # Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don't support rolling upgrades.
    api_response = api_instance.rolling_upgrade(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->rolling_upgrade: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiRollingUpgradeServicesArgs**](ApiRollingUpgradeServicesArgs.md)| Arguments for the rolling upgrade command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_command" id="start_command"></a>
# **start_command**
> ApiCommand start_command(cluster_name)

Start all services in the cluster.

Start all services in the cluster. <p> Services are started in the appropriate order given their dependencies.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.

try:
    # Start all services in the cluster.
    api_response = api_instance.start_command(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->start_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stop_command" id="stop_command"></a>
# **stop_command**
> ApiCommand stop_command(cluster_name)

Stop all services in the cluster.

Stop all services in the cluster. <p> Services are stopped in the appropriate order given their dependencies.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.

try:
    # Stop all services in the cluster.
    api_response = api_instance.stop_command(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->stop_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_cluster" id="update_cluster"></a>
# **update_cluster**
> ApiCluster update_cluster(cluster_name, body=body)

Update an existing cluster.

Update an existing cluster. <p> To update the CDH version, provide the new value in the \"fullVersion\" property. Setting a correct version that matches the actual installed software will ensure the correct version-specific features, such as services, roles, commands, and configurations. This need not be manually set for clusters using parcels. In general this action is only necessary after the CDH packages have been manually updated. Note that a downgrade may be rejected if it would render existing services or roles unusable. For major upgrade, the \"upgradeService\" cluster command should be used instead.</p> <p> To rename the cluster, provide the new name in the \"displayName\" property for API >= 6, or in the \"name\" property for API <=5. <p> Available since API v2.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiCluster() # ApiCluster |  (optional)

try:
    # Update an existing cluster.
    api_response = api_instance.update_cluster(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->update_cluster: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiCluster**](ApiCluster.md)|  | [optional] 

### Return type

[**ApiCluster**](ApiCluster.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="upgrade_cdh_command" id="upgrade_cdh_command"></a>
# **upgrade_cdh_command**
> ApiCommand upgrade_cdh_command(cluster_name, body=body)

Perform CDH upgrade to the specified version.

Perform CDH upgrade to the specified version. <p> Allows the following upgrades: <ul> <li>Major upgrades from any CDH 4 to any CDH 5</li> <li>Minor upgrades from any CDH 5 to any CDH 5</li> <li>Maintenance upgrades or downgrades (a.b.x to a.b.y)</li> </ul> <p> If using packages, CDH packages on all hosts of the cluster must be manually upgraded before this command is issued. <p> The command will upgrade the services and their configuration to the version available in the CDH5 distribution. <p> Unless rolling upgrade options are provided, the entire cluster will experience downtime. If rolling upgrade options are provided, command will do a \"best-effort\" rolling upgrade of the given cluster, i.e. it does plain upgrade of services that cannot be rolling upgraded, followed by first rolling upgrading non-slaves and then rolling restarting the slave roles of services that can be rolling restarted. The slave restarts are done host-by-host. <p> Available since v9. Rolling upgrade is only available with Cloudera Manager Enterprise Edition. A more limited upgrade variant available since v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.
body = cm_client.ApiCdhUpgradeArgs() # ApiCdhUpgradeArgs | Arguments for the command. See ApiCdhUpgradeArgs. (optional)

try:
    # Perform CDH upgrade to the specified version.
    api_response = api_instance.upgrade_cdh_command(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->upgrade_cdh_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 
 **body** | [**ApiCdhUpgradeArgs**](ApiCdhUpgradeArgs.md)| Arguments for the command. See ApiCdhUpgradeArgs. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="upgrade_services_command" id="upgrade_services_command"></a>
# **upgrade_services_command**
> ApiCommand upgrade_services_command(cluster_name)

Upgrades the services in the cluster to the CDH5 version.

Upgrades the services in the cluster to the CDH5 version. <p> This command requires that the CDH packages in the hosts used by the cluster be upgraded to CDH5 before this command is issued. Once issued, this command will stop all running services before proceeding. <p> If parcels are used instead of CDH system packages then the following steps need to happen in order: <ol> <li>Stop all services manually</li> <li>Activate parcel</li> <li>Run this upgrade command</li> </ol> The command will upgrade the services and their configuration to the version available in the CDH5 distribution.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ClustersResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster.

try:
    # Upgrades the services in the cluster to the CDH5 version.
    api_response = api_instance.upgrade_services_command(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClustersResourceApi->upgrade_services_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

