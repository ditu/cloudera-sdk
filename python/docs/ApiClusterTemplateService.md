# ApiClusterTemplateService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref_name** | **str** |  | [optional] 
**service_type** | **str** |  | [optional] 
**service_configs** | [**list[ApiClusterTemplateConfig]**](ApiClusterTemplateConfig.md) |  | [optional] 
**role_config_groups** | [**list[ApiClusterTemplateRoleConfigGroup]**](ApiClusterTemplateRoleConfigGroup.md) |  | [optional] 
**roles** | [**list[ApiClusterTemplateRole]**](ApiClusterTemplateRole.md) |  | [optional] 
**display_name** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


