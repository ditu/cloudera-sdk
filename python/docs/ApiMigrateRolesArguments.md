# ApiMigrateRolesArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role_names_to_migrate** | **list[str]** | The list of role names to migrate. | [optional] 
**destination_host_id** | **str** | The ID of the host to which the roles should be migrated. | [optional] 
**clear_stale_role_data** | **bool** | Delete existing stale role data, if any. For example, when migrating a NameNode, if the destination host has stale data in the NameNode data directories (possibly because a NameNode role was previously located there), this stale data will be deleted before migrating the role. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


