# ApiAudit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **str** | When the audit event was captured. | [optional] 
**service** | **str** | Service name associated with this audit. | [optional] 
**username** | **str** | The user who performed this operation. | [optional] 
**impersonator** | **str** | The impersonating user (or the proxy user) who submitted this operation. This is usually applicable when using services like Oozie or Hue, who can be configured to impersonate other users and submit jobs. | [optional] 
**ip_address** | **str** | The IP address that the client connected from. | [optional] 
**command** | **str** | The command/operation that was requested. | [optional] 
**resource** | **str** | The resource that the operation was performed on. | [optional] 
**operation_text** | **str** | The full text of the requested operation. E.g. the full Hive query. &lt;p&gt; Available since API v5. | [optional] 
**allowed** | **bool** | Whether the operation was allowed or denied by the authorization system. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


