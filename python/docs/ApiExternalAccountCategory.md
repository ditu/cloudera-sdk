# ApiExternalAccountCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Represents an identifier for a category. | [optional] 
**display_name** | **str** | Represents a localized display name for a category. | [optional] 
**description** | **str** | Represents a localized description for a category. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


