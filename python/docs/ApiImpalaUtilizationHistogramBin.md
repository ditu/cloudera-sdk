# ApiImpalaUtilizationHistogramBin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_point_inclusive** | **float** | start point (inclusive) of the histogram bin. | [optional] 
**end_point_exclusive** | **float** | end point (exclusive) of the histogram bin. | [optional] 
**number_of_impala_daemons** | **float** | Number of Impala daemons. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


