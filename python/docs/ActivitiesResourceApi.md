# cm_client.ActivitiesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_metrics**](ActivitiesResourceApi.md#get_metrics) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/metrics | Fetch metric readings for a particular activity.
[**read_activities**](ActivitiesResourceApi.md#read_activities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities | Read all activities in the system.
[**read_activity**](ActivitiesResourceApi.md#read_activity) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId} | Returns a specific activity in the system.
[**read_child_activities**](ActivitiesResourceApi.md#read_child_activities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/children | Returns the child activities.
[**read_similar_activities**](ActivitiesResourceApi.md#read_similar_activities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/similar | Returns a list of similar activities.


<a name="get_metrics" id="get_metrics"></a>
# **get_metrics**
> ApiMetricList get_metrics(activity_id, cluster_name, service_name, _from=_from, metrics=metrics, to=to, view=view)

Fetch metric readings for a particular activity.

Fetch metric readings for a particular activity. <p> By default, this call will look up all metrics available for the activity. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the activity, even if no readings are available in the requested window.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ActivitiesResourceApi(cm_client.ApiClient(configuration))
activity_id = 'activity_id_example' # str | The name of the activity.
cluster_name = 'cluster_name_example' # str | The name of the cluster.
service_name = 'service_name_example' # str | The name of the service.
_from = '_from_example' # str | Start of the period to query. (optional)
metrics = ['metrics_example'] # list[str] | Filter for which metrics to query. (optional)
to = 'now' # str | End of the period to query. (optional) (default to now)
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Fetch metric readings for a particular activity.
    api_response = api_instance.get_metrics(activity_id, cluster_name, service_name, _from=_from, metrics=metrics, to=to, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesResourceApi->get_metrics: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activity_id** | **str**| The name of the activity. | 
 **cluster_name** | **str**| The name of the cluster. | 
 **service_name** | **str**| The name of the service. | 
 **_from** | **str**| Start of the period to query. | [optional] 
 **metrics** | [**list[str]**](str.md)| Filter for which metrics to query. | [optional] 
 **to** | **str**| End of the period to query. | [optional] [default to now]
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_activities" id="read_activities"></a>
# **read_activities**
> ApiActivityList read_activities(cluster_name, service_name, max_results=max_results, query=query, result_offset=result_offset, view=view)

Read all activities in the system.

Read all activities in the system

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ActivitiesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | The name of the cluster
service_name = 'service_name_example' # str | The name of the service
max_results = 100 # int | The maximum number of activities to return. (optional) (default to 100)
query = 'status==started;parent==' # str | The query to perform to find activities in the system. By default, this call returns top level (i.e. root) activities that have currently started. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>status==started;parent==</dt> <dd>looks for running root activities. This is also the default query.</dd> <dt>status==failed;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for failed activities after the given date time.</dd> <dt>name==Pi Estimator;startTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for activities started after the given date time, with the name of \"Pi Estimator\".</dd> <dt>startTime=lt=2012-01-02T00:00:00.000Z;finishTime=ge=2012-01-01T00:00:00.000Z</dt> <dd>looks for activities that are active on 2012 New Year's Day. Note that they may start before or finish after that day.</dd> <dt>status==failed;parent==000014-20120425161321-oozie-joe</dt> <dd>looks for failed child activities of the given parent activity id.</dd> <dt>status==started;metrics.cpu_user=gt=10</dt> <dd>looks for started activities that are using more than 10 cores per second.</dd> <dt>type==hive;metrics.user==bc;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for all hive queries submitted by user bc after the given date time.</dd> </dl>  You may query any fields present in the ApiActivity object. You can also query by activity metric values using the <em>metrics.*</em> syntax. Values for date time fields should be ISO8601 timestamps. <p> The valid comparators are <em>==</em>, <em>!=</em>, <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"==\", \"!=\", \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively. (optional) (default to status==started;parent==)
result_offset = 0 # int | Specified the offset of activities to return. (optional) (default to 0)
view = 'summary' # str | The view of the activities to materialize (optional) (default to summary)

try:
    # Read all activities in the system.
    api_response = api_instance.read_activities(cluster_name, service_name, max_results=max_results, query=query, result_offset=result_offset, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesResourceApi->read_activities: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**| The name of the cluster | 
 **service_name** | **str**| The name of the service | 
 **max_results** | **int**| The maximum number of activities to return. | [optional] [default to 100]
 **query** | **str**| The query to perform to find activities in the system. By default, this call returns top level (i.e. root) activities that have currently started. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>status==started;parent==</dt> <dd>looks for running root activities. This is also the default query.</dd> <dt>status==failed;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for failed activities after the given date time.</dd> <dt>name==Pi Estimator;startTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for activities started after the given date time, with the name of \"Pi Estimator\".</dd> <dt>startTime=lt=2012-01-02T00:00:00.000Z;finishTime=ge=2012-01-01T00:00:00.000Z</dt> <dd>looks for activities that are active on 2012 New Year's Day. Note that they may start before or finish after that day.</dd> <dt>status==failed;parent==000014-20120425161321-oozie-joe</dt> <dd>looks for failed child activities of the given parent activity id.</dd> <dt>status==started;metrics.cpu_user=gt=10</dt> <dd>looks for started activities that are using more than 10 cores per second.</dd> <dt>type==hive;metrics.user==bc;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for all hive queries submitted by user bc after the given date time.</dd> </dl>  You may query any fields present in the ApiActivity object. You can also query by activity metric values using the <em>metrics.*</em> syntax. Values for date time fields should be ISO8601 timestamps. <p> The valid comparators are <em>==</em>, <em>!=</em>, <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"==\", \"!=\", \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively. | [optional] [default to status&#x3D;&#x3D;started;parent&#x3D;&#x3D;]
 **result_offset** | **int**| Specified the offset of activities to return. | [optional] [default to 0]
 **view** | **str**| The view of the activities to materialize | [optional] [default to summary]

### Return type

[**ApiActivityList**](ApiActivityList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_activity" id="read_activity"></a>
# **read_activity**
> ApiActivity read_activity(activity_id, cluster_name, service_name, view=view)

Returns a specific activity in the system.

Returns a specific activity in the system

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ActivitiesResourceApi(cm_client.ApiClient(configuration))
activity_id = 'activity_id_example' # str | The id of the activity to retrieve
cluster_name = 'cluster_name_example' # str | The name of the cluster
service_name = 'service_name_example' # str | The name of the service
view = 'summary' # str | The view of the activity to materialize (optional) (default to summary)

try:
    # Returns a specific activity in the system.
    api_response = api_instance.read_activity(activity_id, cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesResourceApi->read_activity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activity_id** | **str**| The id of the activity to retrieve | 
 **cluster_name** | **str**| The name of the cluster | 
 **service_name** | **str**| The name of the service | 
 **view** | **str**| The view of the activity to materialize | [optional] [default to summary]

### Return type

[**ApiActivity**](ApiActivity.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_child_activities" id="read_child_activities"></a>
# **read_child_activities**
> ApiActivityList read_child_activities(activity_id, cluster_name, service_name, max_results=max_results, result_offset=result_offset, view=view)

Returns the child activities.

Returns the child activities

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ActivitiesResourceApi(cm_client.ApiClient(configuration))
activity_id = 'activity_id_example' # str | The id of the activity
cluster_name = 'cluster_name_example' # str | The name of the cluster
service_name = 'service_name_example' # str | The name of the service
max_results = 100 # int | The maximum number of activities to return. (optional) (default to 100)
result_offset = 0 # int | Specified the offset of activities to return. (optional) (default to 0)
view = 'summary' # str | The view of the children to materialize (optional) (default to summary)

try:
    # Returns the child activities.
    api_response = api_instance.read_child_activities(activity_id, cluster_name, service_name, max_results=max_results, result_offset=result_offset, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesResourceApi->read_child_activities: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activity_id** | **str**| The id of the activity | 
 **cluster_name** | **str**| The name of the cluster | 
 **service_name** | **str**| The name of the service | 
 **max_results** | **int**| The maximum number of activities to return. | [optional] [default to 100]
 **result_offset** | **int**| Specified the offset of activities to return. | [optional] [default to 0]
 **view** | **str**| The view of the children to materialize | [optional] [default to summary]

### Return type

[**ApiActivityList**](ApiActivityList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_similar_activities" id="read_similar_activities"></a>
# **read_similar_activities**
> ApiActivityList read_similar_activities(activity_id, cluster_name, service_name, view=view)

Returns a list of similar activities.

Returns a list of similar activities

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ActivitiesResourceApi(cm_client.ApiClient(configuration))
activity_id = 'activity_id_example' # str | The id of the activity
cluster_name = 'cluster_name_example' # str | The name of the cluster
service_name = 'service_name_example' # str | The name of the service
view = 'summary' # str | The view of the activities to materialize (optional) (default to summary)

try:
    # Returns a list of similar activities.
    api_response = api_instance.read_similar_activities(activity_id, cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesResourceApi->read_similar_activities: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activity_id** | **str**| The id of the activity | 
 **cluster_name** | **str**| The name of the cluster | 
 **service_name** | **str**| The name of the service | 
 **view** | **str**| The view of the activities to materialize | [optional] [default to summary]

### Return type

[**ApiActivityList**](ApiActivityList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

