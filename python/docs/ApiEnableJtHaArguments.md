# ApiEnableJtHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_jt_host_id** | **str** | Id of host on which second JobTracker role will be added. | [optional] 
**force_init_z_node** | **bool** | Initialize the ZNode even if it already exists. This can happen if JobTracker HA was enabled before and then disabled. Disable operation doesn&#39;t delete this ZNode. Defaults to true. | [optional] 
**zk_service_name** | **str** | Name of the ZooKeeper service that will be used for auto-failover. This is an optional parameter if the MapReduce to ZooKeeper dependency is already set in CM. | [optional] 
**new_jt_role_name** | **str** | Name of the second JobTracker role to be created (Optional) | [optional] 
**fc1_role_name** | **str** | Name of first Failover Controller role to be created. This is the Failover Controller co-located with the current JobTracker (Optional) | [optional] 
**fc2_role_name** | **str** | Name of second Failover Controller role to be created. This is the Failover Controller co-located with the new JobTracker (Optional) | [optional] 
**logical_name** | **str** | Logical name of the JobTracker pair. If value is not provided, \&quot;logicaljt\&quot; is used as the default. The name can contain only alphanumeric characters and \&quot;-\&quot;. &lt;p&gt; Available since API v8. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


