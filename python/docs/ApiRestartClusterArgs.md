# ApiRestartClusterArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**restart_only_stale_services** | **bool** | Only restart services that have stale configuration and their dependent services. | [optional] 
**redeploy_client_configuration** | **bool** | Re-deploy client configuration for all services in the cluster. | [optional] 
**restart_service_names** | **list[str]** | Only restart services that are specified and their dependent services. Available since V11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


