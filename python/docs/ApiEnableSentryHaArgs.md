# ApiEnableSentryHaArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_sentry_host_id** | **str** | Id of host on which new Sentry Server role will be added. | [optional] 
**new_sentry_role_name** | **str** | Name of the new Sentry Server role to be created. This is an optional argument. | [optional] 
**zk_service_name** | **str** | Name of the ZooKeeper service that will be used for Sentry HA. This is an optional parameter if the Sentry to ZooKeeper dependency is already set in CM. | [optional] 
**rrc_args** | [**ApiSimpleRollingRestartClusterArgs**](ApiSimpleRollingRestartClusterArgs.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


