# cm_client.ToolsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**echo**](ToolsResourceApi.md#echo) | **GET** /tools/echo | Echoes the provided message back to the caller.
[**echo_error**](ToolsResourceApi.md#echo_error) | **GET** /tools/echoError | Throws an error containing the given input message.


<a name="echo" id="echo"></a>
# **echo**
> ApiEcho echo(message=message)

Echoes the provided message back to the caller.

Echoes the provided message back to the caller.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ToolsResourceApi(cm_client.ApiClient(configuration))
message = 'Hello, World!' # str | The message to echo back (optional) (default to Hello, World!)

try:
    # Echoes the provided message back to the caller.
    api_response = api_instance.echo(message=message)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ToolsResourceApi->echo: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **str**| The message to echo back | [optional] [default to Hello, World!]

### Return type

[**ApiEcho**](ApiEcho.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="echo_error" id="echo_error"></a>
# **echo_error**
> ApiEcho echo_error(message=message)

Throws an error containing the given input message.

Throws an error containing the given input message. This is what an error response looks like.  <pre>    {      \"message\": \"An error message\",      \"causes\": [ \"A list of causes\", \"Potentially null\" ]    }  </pre>  <p>The <em>message</em> field contains a description of the error. The <em>causes</em> field, if not null, contains a list of causes for the error. </p>  <p>Note that this <strong>never</strong> returns an echoMessage. Instead, the result (and all error results) has the above structure. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ToolsResourceApi(cm_client.ApiClient(configuration))
message = 'Default error message' # str | The error message to echo (optional) (default to Default error message)

try:
    # Throws an error containing the given input message.
    api_response = api_instance.echo_error(message=message)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ToolsResourceApi->echo_error: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **str**| The error message to echo | [optional] [default to Default error message]

### Return type

[**ApiEcho**](ApiEcho.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

