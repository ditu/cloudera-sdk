# ApiImpalaRoleDiagnosticsArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticket_number** | **str** | The support ticket number to attach to this data collection. | [optional] 
**comments** | **str** | Comments to include with this data collection. | [optional] 
**stacks_count** | **float** |  | [optional] 
**stacks_interval_seconds** | **float** | Interval between stack collections. | [optional] 
**jmap** | **bool** |  | [optional] 
**gcore** | **bool** |  | [optional] 
**minidumps_count** | **float** |  | [optional] 
**minidumps_interval_seconds** | **float** |  | [optional] 
**phone_home** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


