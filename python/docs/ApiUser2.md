# ApiUser2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The username, which is unique within a Cloudera Manager installation. | [optional] 
**password** | **str** | Returns the user password. &lt;p&gt; Passwords are not returned when querying user information, so this property will always be empty when reading information from a server. | [optional] 
**auth_roles** | [**list[ApiAuthRoleRef]**](ApiAuthRoleRef.md) | A list of ApiAuthRole that this user possesses. | [optional] 
**pw_hash** | **str** | NOTE: Only available in the \&quot;export\&quot; view | [optional] 
**pw_salt** | **float** | NOTE: Only available in the \&quot;export\&quot; view | [optional] 
**pw_login** | **bool** | NOTE: Only available in the \&quot;export\&quot; view | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


