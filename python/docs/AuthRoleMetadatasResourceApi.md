# cm_client.AuthRoleMetadatasResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**read_auth_roles_metadata**](AuthRoleMetadatasResourceApi.md#read_auth_roles_metadata) | **GET** /authRoleMetadatas | Returns a list of the auth roles&#39; metadata for the built-in roles.


<a name="read_auth_roles_metadata" id="read_auth_roles_metadata"></a>
# **read_auth_roles_metadata**
> ApiAuthRoleMetadataList read_auth_roles_metadata(view=view)

Returns a list of the auth roles' metadata for the built-in roles.

Returns a list of the auth roles' metadata for the built-in roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthRoleMetadatasResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    # Returns a list of the auth roles' metadata for the built-in roles.
    api_response = api_instance.read_auth_roles_metadata(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthRoleMetadatasResourceApi->read_auth_roles_metadata: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiAuthRoleMetadataList**](ApiAuthRoleMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

