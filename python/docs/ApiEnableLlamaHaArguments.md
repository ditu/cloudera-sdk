# ApiEnableLlamaHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_llama_host_id** | **str** | HostId of the host on which the second Llama role will be created. | [optional] 
**new_llama_role_name** | **str** | Name of the second Llama role to be created (optional). | [optional] 
**zk_service_name** | **str** | Name of the ZooKeeper service that will be used for auto-failover. This argument may be omitted if the ZooKeeper dependency for Impala is already configured. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


