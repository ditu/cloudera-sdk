# ApiClustersPerfInspectorArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_cluster** | **str** | Required name of the source cluster to run network diagnostics test. | [optional] 
**target_cluster** | **str** | Required name of the target cluster to run network diagnostics test. | [optional] 
**ping_args** | [**ApiPerfInspectorPingArgs**](ApiPerfInspectorPingArgs.md) | Optional ping request arguments. If not specified, default arguments will be used for ping test. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


