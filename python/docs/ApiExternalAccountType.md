# ApiExternalAccountType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Represents the immutable name for this account. | [optional] 
**category_name** | **str** | Represents the category of this account. | [optional] 
**type** | **str** | Represents the type for this account. | [optional] 
**display_name** | **str** | Represents the localized display name for this account. | [optional] 
**description** | **str** | Represents the localized description for this account type. | [optional] 
**allowed_account_configs** | [**ApiConfigList**](ApiConfigList.md) | Represents the list of allowed account configs. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


