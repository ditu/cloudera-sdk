# ApiYarnApplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allocated_mb** | **float** | The sum of memory in MB allocated to the application&#39;s running containers Available since v12. | [optional] 
**allocated_v_cores** | **float** | The sum of virtual cores allocated to the application&#39;s running containers Available since v12. | [optional] 
**running_containers** | **float** | The number of containers currently running for the application Available since v12. | [optional] 
**application_tags** | **list[str]** | List of YARN application tags. Available since v12. | [optional] 
**allocated_memory_seconds** | **float** | Allocated memory to the application in units of mb-secs. Available since v12. | [optional] 
**allocated_vcore_seconds** | **float** | Allocated vcore-secs to the application. Available since v12. | [optional] 
**application_id** | **str** | The application id. | [optional] 
**name** | **str** | The name of the application. | [optional] 
**start_time** | **str** | The time the application was submitted. | [optional] 
**end_time** | **str** | The time the application finished. If the application hasn&#39;t finished this will return null. | [optional] 
**user** | **str** | The user who submitted the application. | [optional] 
**pool** | **str** | The pool the application was submitted to. | [optional] 
**progress** | **float** | The progress, as a percentage, the application has made. This is only set if the application is currently executing. | [optional] 
**attributes** | **dict(str, str)** | A map of additional application attributes which is generated by Cloudera Manager. For example MR2 job counters are exposed as key/value pairs here. For more details see the Cloudera Manager documentation. | [optional] 
**mr2_app_information** | [**ApiMr2AppInformation**](ApiMr2AppInformation.md) |  | [optional] 
**state** | **str** |  | [optional] 
**container_used_memory_seconds** | **float** | Actual memory (in MB-secs) used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12. | [optional] 
**container_used_memory_max** | **float** | Maximum memory used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics Available since v16 | [optional] 
**container_used_cpu_seconds** | **float** | Actual CPU (in percent-secs) used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12. | [optional] 
**container_used_vcore_seconds** | **float** | Actual VCore-secs used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12. | [optional] 
**container_allocated_memory_seconds** | **float** | Total memory (in mb-secs) allocated to containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12. | [optional] 
**container_allocated_vcore_seconds** | **float** | Total vcore-secs allocated to containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


