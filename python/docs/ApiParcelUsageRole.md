# ApiParcelUsageRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role_ref** | [**ApiRoleRef**](ApiRoleRef.md) | A reference to the corresponding Role object. | [optional] 
**parcel_refs** | [**list[ApiParcelRef]**](ApiParcelRef.md) | A collection of references to the parcels being used by the role. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


