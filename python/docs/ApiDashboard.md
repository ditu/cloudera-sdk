# ApiDashboard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Returns the dashboard name. | [optional] 
**json** | **str** | Returns the json structure for the dashboard. This should be treated as an opaque blob. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


