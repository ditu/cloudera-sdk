# ApiServiceConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[ApiConfig]**](ApiConfig.md) |  | [optional] 
**role_type_configs** | [**list[ApiRoleTypeConfig]**](ApiRoleTypeConfig.md) | List of role type configurations. Only available up to API v2. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


