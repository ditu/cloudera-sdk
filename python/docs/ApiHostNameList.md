# ApiHostNameList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | **list[str]** | List of host names. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


