# ApiImpalaQueryDetailsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**details** | **str** | The details for this query. Two formats are supported: &lt;ul&gt; &lt;li&gt; &#39;text&#39;: this is a text based, human readable representation of the Impala runtime profile. &lt;/li&gt; &lt;li&gt; &#39;thrift_encoded&#39;: this a compact-thrift, base64 encoded representation of the impala RuntimeProfile.thrift object. See the Impala documentation for more details. &lt;/li&gt; &lt;/ul&gt; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


