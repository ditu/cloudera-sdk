# ApiClusterTemplateHostInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host_name** | **str** |  | [optional] 
**host_name_range** | **str** |  | [optional] 
**rack_id** | **str** |  | [optional] 
**host_template_ref_name** | **str** |  | [optional] 
**role_ref_names** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


