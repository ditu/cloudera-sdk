# ApiMrUsageReportRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time_period** | **str** | The time period over which this report is generated. | [optional] 
**user** | **str** | The user being reported. | [optional] 
**group** | **str** | The group this user belongs to. | [optional] 
**cpu_sec** | **float** | Amount of CPU time (in seconds) taken up this user&#39;s MapReduce jobs. | [optional] 
**memory_bytes** | **float** | The sum of physical memory used (collected as a snapshot) by this user&#39;s MapReduce jobs. | [optional] 
**job_count** | **float** | Number of jobs. | [optional] 
**task_count** | **float** | Number of tasks. | [optional] 
**duration_sec** | **float** | Total duration of this user&#39;s MapReduce jobs. | [optional] 
**failed_maps** | **float** | Failed maps of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**total_maps** | **float** | Total maps of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**failed_reduces** | **float** | Failed reduces of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**total_reduces** | **float** | Total reduces of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**map_input_bytes** | **float** | Map input bytes of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**map_output_bytes** | **float** | Map output bytes of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**hdfs_bytes_read** | **float** | HDFS bytes read of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**hdfs_bytes_written** | **float** | HDFS bytes written of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**local_bytes_read** | **float** | Local bytes read of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**local_bytes_written** | **float** | Local bytes written of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**data_local_maps** | **float** | Data local maps of this user&#39;s MapReduce jobs. Available since v11. | [optional] 
**rack_local_maps** | **float** | Rack local maps of this user&#39;s MapReduce jobs. Available since v11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


