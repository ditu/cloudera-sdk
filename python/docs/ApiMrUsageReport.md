# ApiMrUsageReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[ApiMrUsageReportRow]**](ApiMrUsageReportRow.md) | A list of per-user usage information at the requested time granularity. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


