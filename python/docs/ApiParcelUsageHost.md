# ApiParcelUsageHost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host_ref** | [**ApiHostRef**](ApiHostRef.md) | A reference to the corresponding Host object. | [optional] 
**roles** | [**list[ApiParcelUsageRole]**](ApiParcelUsageRole.md) | A collection of the roles present on the host. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


