# ApiHiveTable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**database** | **str** | Name of the database to which this table belongs. | [optional] 
**table_name** | **str** | Name of the table. When used as input for a replication job, this can be a regular expression that matches several table names. Refer to the Hive documentation for the syntax of regular expressions. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


