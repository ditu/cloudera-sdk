# ApiHost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host_id** | **str** | A unique host identifier. This is not the same as the hostname (FQDN). It is a distinct value that remains the same even if the hostname changes. | [optional] 
**ip_address** | **str** | The host IP address. This field is not mutable after the initial creation. | [optional] 
**hostname** | **str** | The hostname. This field is not mutable after the initial creation. | [optional] 
**rack_id** | **str** | The rack ID for this host. | [optional] 
**last_heartbeat** | **str** | Readonly. Requires \&quot;full\&quot; view. When the host agent sent the last heartbeat. | [optional] 
**role_refs** | [**list[ApiRoleRef]**](ApiRoleRef.md) | Readonly. Requires \&quot;full\&quot; view. The list of roles assigned to this host. | [optional] 
**health_summary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Readonly. Requires \&quot;full\&quot; view. The high-level health status of this host. | [optional] 
**health_checks** | [**list[ApiHealthCheck]**](ApiHealthCheck.md) | Readonly. Requires \&quot;full\&quot; view. The list of health checks performed on the host, with their results. | [optional] 
**host_url** | **str** | Readonly. A URL into the Cloudera Manager web UI for this specific host. | [optional] 
**maintenance_mode** | **bool** | Readonly. Whether the host is in maintenance mode. Available since API v2. | [optional] 
**commission_state** | [**ApiCommissionState**](ApiCommissionState.md) | Readonly. The commission state of this role. Available since API v2. | [optional] 
**maintenance_owners** | [**list[ApiEntityType]**](ApiEntityType.md) | Readonly. The list of objects that trigger this host to be in maintenance mode. Available since API v2. | [optional] 
**config** | [**ApiConfigList**](ApiConfigList.md) |  | [optional] 
**num_cores** | **float** | Readonly. The number of logical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v4. | [optional] 
**num_physical_cores** | **float** | Readonly. The number of physical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v9. | [optional] 
**total_phys_mem_bytes** | **float** | Readonly. The amount of physical RAM on this host, in bytes. Only populated after the host has heartbeated to the server. Available since API v4. | [optional] 
**entity_status** | [**ApiEntityStatus**](ApiEntityStatus.md) | Readonly. The entity status for this host. Available since API v11. | [optional] 
**cluster_ref** | [**ApiClusterRef**](ApiClusterRef.md) | Readonly. A reference to the enclosing cluster. This might be null if the host is not yet assigned to a cluster. Available since API v11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


