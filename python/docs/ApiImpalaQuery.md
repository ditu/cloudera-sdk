# ApiImpalaQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query_id** | **str** | The query id. | [optional] 
**statement** | **str** | The SQL statement for the query. | [optional] 
**query_type** | **str** | The query type. The possible values are: DML, DDL, QUERY and UNKNOWN. See the Impala documentation for more details. | [optional] 
**query_state** | **str** | The query state. The possible values are: CREATED, INITIALIZED, COMPILED, RUNNING, FINISHED, EXCEPTION, and UNKNOWN. See the Impala documentation for more details. | [optional] 
**start_time** | **str** | The time the query was issued. | [optional] 
**end_time** | **str** | The time the query finished. If the query hasn&#39;t finished then this will return null. | [optional] 
**rows_produced** | **float** | The number of rows produced by the query. If the query hasn&#39;t completed this will return null. | [optional] 
**attributes** | **dict(str, str)** | A map of additional query attributes which is generated by Cloudera Manager. | [optional] 
**user** | **str** | The user who issued this query. | [optional] 
**coordinator** | [**ApiHostRef**](ApiHostRef.md) | The host of the Impala Daemon coordinating the query | [optional] 
**details_available** | **bool** | Whether we have a detailed runtime profile available for the query. This profile is available at the endpoint /queries/{QUERY_ID}. | [optional] 
**database** | **str** | The database on which this query was issued. | [optional] 
**duration_millis** | **float** | The duration of the query in milliseconds. If the query hasn&#39;t completed then this will return null. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


