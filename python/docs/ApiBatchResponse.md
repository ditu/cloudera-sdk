# ApiBatchResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[ApiBatchResponseElement]**](ApiBatchResponseElement.md) |  | [optional] 
**success** | **bool** | Read-only. True if every response element&#39;s ApiBatchResponseElement#getStatusCode() is in the range [200, 300), false otherwise. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


