# cm_client.YarnApplicationsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_yarn_application_attributes**](YarnApplicationsResourceApi.md#get_yarn_application_attributes) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnApplications/attributes | Returns the list of all attributes that the Service Monitor can associate with YARN applications.
[**get_yarn_applications**](YarnApplicationsResourceApi.md#get_yarn_applications) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnApplications | Returns a list of applications that satisfy the filter.
[**kill_yarn_application**](YarnApplicationsResourceApi.md#kill_yarn_application) | **POST** /clusters/{clusterName}/services/{serviceName}/yarnApplications/{applicationId}/kill | Kills an YARN Application.


<a name="get_yarn_application_attributes" id="get_yarn_application_attributes"></a>
# **get_yarn_application_attributes**
> ApiYarnApplicationAttributeList get_yarn_application_attributes(cluster_name, service_name)

Returns the list of all attributes that the Service Monitor can associate with YARN applications.

Returns the list of all attributes that the Service Monitor can associate with YARN applications. <p> Examples of attributes include the user who ran the application and the number of maps completed by the application. <p> These attributes can be used to search for specific YARN applications through the getYarnApplications API. For example the 'user' attribute could be used in the search 'user = root'. If the attribute is numeric it can also be used as a metric in a tsquery (ie, 'select maps_completed from YARN_APPLICATIONS'). <p> Note that this response is identical for all YARN services. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.YarnApplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 

try:
    # Returns the list of all attributes that the Service Monitor can associate with YARN applications.
    api_response = api_instance.get_yarn_application_attributes(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling YarnApplicationsResourceApi->get_yarn_application_attributes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 

### Return type

[**ApiYarnApplicationAttributeList**](ApiYarnApplicationAttributeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_yarn_applications" id="get_yarn_applications"></a>
# **get_yarn_applications**
> ApiYarnApplicationResponse get_yarn_applications(cluster_name, service_name, filter=filter, _from=_from, limit=limit, offset=offset, to=to)

Returns a list of applications that satisfy the filter.

Returns a list of applications that satisfy the filter <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.YarnApplicationsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service
filter = '' # str | A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like 'executing = true' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'application_duration > 5s and (user = root or user = myUserName'). (optional) (default to )
_from = '_from_example' # str | Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time). (optional)
limit = 100 # int | The maximum number of applications to return. Applications will be returned in the following order: <ul> <li> All executing applications, ordered from longest to shortest running </li> <li> All completed applications order by end time descending. </li> </ul> (optional) (default to 100)
offset = 0 # int | The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a 'executing = false' clause to your filter. (optional) (default to 0)
to = 'now' # str | End of the period to query in ISO 8601 format (defaults to now). (optional) (default to now)

try:
    # Returns a list of applications that satisfy the filter.
    api_response = api_instance.get_yarn_applications(cluster_name, service_name, filter=filter, _from=_from, limit=limit, offset=offset, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling YarnApplicationsResourceApi->get_yarn_applications: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 
 **filter** | **str**| A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like 'executing = true' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'application_duration > 5s and (user = root or user = myUserName'). | [optional] [default to ]
 **_from** | **str**| Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time). | [optional] 
 **limit** | **int**| The maximum number of applications to return. Applications will be returned in the following order: <ul> <li> All executing applications, ordered from longest to shortest running </li> <li> All completed applications order by end time descending. </li> </ul> | [optional] [default to 100]
 **offset** | **int**| The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a 'executing = false' clause to your filter. | [optional] [default to 0]
 **to** | **str**| End of the period to query in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiYarnApplicationResponse**](ApiYarnApplicationResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="kill_yarn_application" id="kill_yarn_application"></a>
# **kill_yarn_application**
> ApiYarnKillResponse kill_yarn_application(application_id, cluster_name, service_name)

Kills an YARN Application.

Kills an YARN Application <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.YarnApplicationsResourceApi(cm_client.ApiClient(configuration))
application_id = 'application_id_example' # str | The applicationId to kill
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service

try:
    # Kills an YARN Application.
    api_response = api_instance.kill_yarn_application(application_id, cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling YarnApplicationsResourceApi->kill_yarn_application: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **application_id** | **str**| The applicationId to kill | 
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 

### Return type

[**ApiYarnKillResponse**](ApiYarnKillResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

