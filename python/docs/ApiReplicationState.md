# ApiReplicationState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incremental_export_enabled** | **bool** | returns if incremental export is enabled for the given Hive service. Not applicable for HDFS service. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


