# ApiHiveReplicationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phase** | **str** | Phase the replication is in. &lt;p/&gt; If the replication job is still active, this will contain a string describing the current phase. This will be one of: EXPORT, DATA or IMPORT, for, respectively, exporting the source metastore information, replicating table data (if configured), and importing metastore information in the target. &lt;p/&gt; This value will not be present if the replication is not active. &lt;p/&gt; Available since API v4. | [optional] 
**table_count** | **float** | Number of tables that were successfully replicated. Available since API v4. | [optional] 
**tables** | [**list[ApiHiveTable]**](ApiHiveTable.md) | The list of tables successfully replicated. &lt;p/&gt; Since API v4, this is only available in the full view. | [optional] 
**impala_udf_count** | **float** | Number of impala UDFs that were successfully replicated. Available since API v6. | [optional] 
**hive_udf_count** | **float** | Number of hive UDFs that were successfully replicated. Available since API v14. | [optional] 
**impala_ud_fs** | [**list[ApiImpalaUDF]**](ApiImpalaUDF.md) | The list of Impala UDFs successfully replicated. Available since API v6 in the full view. | [optional] 
**hive_ud_fs** | [**list[ApiHiveUDF]**](ApiHiveUDF.md) | The list of Impala UDFs successfully replicated. Available since API v6 in the full view. | [optional] 
**error_count** | **float** | Number of errors detected during replication job. Available since API v4. | [optional] 
**errors** | [**list[ApiHiveReplicationError]**](ApiHiveReplicationError.md) | List of errors encountered during replication. &lt;p/&gt; Since API v4, this is only available in the full view. | [optional] 
**data_replication_result** | [**ApiHdfsReplicationResult**](ApiHdfsReplicationResult.md) | Result of table data replication, if performed. | [optional] 
**dry_run** | **bool** | Whether this was a dry run. | [optional] 
**run_as_user** | **str** | Name of the of proxy user, if any. Available since API v11. | [optional] 
**run_on_source_as_user** | **str** | Name of the source proxy user, if any. Available since API v18. | [optional] 
**stats_available** | **bool** | Whether stats are available to display or not. Available since API v19. | [optional] 
**db_processed** | **float** | Number of Db&#39;s Imported/Exported. Available since API v19. | [optional] 
**table_processed** | **float** | Number of Tables Imported/Exported. Available since API v19. | [optional] 
**partition_processed** | **float** | Number of Partitions Imported/Exported. Available since API v19. | [optional] 
**function_processed** | **float** | Number of Functions Imported/Exported. Available since API v19. | [optional] 
**index_processed** | **float** | Number of Indexes Imported/Exported. Available since API v19. | [optional] 
**stats_processed** | **float** | Number of Table and Partitions Statistics Imported/Exported. Available since API v19. | [optional] 
**db_expected** | **float** | Number of Db&#39;s Expected. Available since API v19. | [optional] 
**table_expected** | **float** | Number of Tables Expected. Available since API v19. | [optional] 
**partition_expected** | **float** | Number of Partitions Expected. Available since API v19. | [optional] 
**function_expected** | **float** | Number of Functions Expected. Available since API v19. | [optional] 
**index_expected** | **float** | Number of Indexes Expected. Available since API v19. | [optional] 
**stats_expected** | **float** | Number of Table and Partition Statistics Expected. Available since API v19. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


