# cm_client.ServicesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**collect_yarn_application_diagnostics**](ServicesResourceApi.md#collect_yarn_application_diagnostics) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnApplicationDiagnosticsCollection | Collect the Diagnostics data for Yarn applications.
[**create_beeswax_warehouse_command**](ServicesResourceApi.md#create_beeswax_warehouse_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueCreateHiveWarehouse | Create the Beeswax role&#39;s Hive warehouse directory, on Hue services.
[**create_h_base_root_command**](ServicesResourceApi.md#create_h_base_root_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hbaseCreateRoot | Creates the root directory of an HBase service.
[**create_hive_user_dir_command**](ServicesResourceApi.md#create_hive_user_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateHiveUserDir | Create the Hive user directory.
[**create_hive_warehouse_command**](ServicesResourceApi.md#create_hive_warehouse_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateHiveWarehouse | Create the Hive warehouse directory, on Hive services.
[**create_impala_user_dir_command**](ServicesResourceApi.md#create_impala_user_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateUserDir | Create the Impala user directory.
[**create_oozie_db**](ServicesResourceApi.md#create_oozie_db) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createOozieDb | Creates the Oozie Database Schema in the configured database.
[**create_services**](ServicesResourceApi.md#create_services) | **POST** /clusters/{clusterName}/services | Creates a list of services.
[**create_solr_hdfs_home_dir_command**](ServicesResourceApi.md#create_solr_hdfs_home_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createSolrHdfsHomeDir | Creates the home directory of a Solr service in HDFS.
[**create_sqoop_user_dir_command**](ServicesResourceApi.md#create_sqoop_user_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createSqoopUserDir | Creates the user directory of a Sqoop service in HDFS.
[**create_yarn_cm_container_usage_input_dir_command**](ServicesResourceApi.md#create_yarn_cm_container_usage_input_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnCreateCmContainerUsageInputDirCommand | Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics.
[**create_yarn_job_history_dir_command**](ServicesResourceApi.md#create_yarn_job_history_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnCreateJobHistoryDirCommand | Create the Yarn job history directory.
[**create_yarn_node_manager_remote_app_log_dir_command**](ServicesResourceApi.md#create_yarn_node_manager_remote_app_log_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnNodeManagerRemoteAppLogDirCommand | Create the Yarn NodeManager remote application log directory.
[**decommission_command**](ServicesResourceApi.md#decommission_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/decommission | Decommission roles of a service.
[**delete_service**](ServicesResourceApi.md#delete_service) | **DELETE** /clusters/{clusterName}/services/{serviceName} | Deletes a service from the system.
[**deploy_client_config_command**](ServicesResourceApi.md#deploy_client_config_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/deployClientConfig | Deploy a service&#39;s client configuration.
[**disable_jt_ha_command**](ServicesResourceApi.md#disable_jt_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableJtHa | Disable high availability (HA) for JobTracker.
[**disable_llama_ha_command**](ServicesResourceApi.md#disable_llama_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaDisableLlamaHa | Not Supported.
[**disable_llama_rm_command**](ServicesResourceApi.md#disable_llama_rm_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaDisableLlamaRm | Not Supported.
[**disable_oozie_ha_command**](ServicesResourceApi.md#disable_oozie_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieDisableHa | Disable high availability (HA) for Oozie.
[**disable_rm_ha_command**](ServicesResourceApi.md#disable_rm_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableRmHa | Disable high availability (HA) for ResourceManager.
[**disable_sentry_ha_command**](ServicesResourceApi.md#disable_sentry_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableSentryHa | Disable high availability (HA) for Sentry service.
[**enable_jt_ha_command**](ServicesResourceApi.md#enable_jt_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableJtHa | Enable high availability (HA) for a JobTracker.
[**enable_llama_ha_command**](ServicesResourceApi.md#enable_llama_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaEnableLlamaHa | Not Supported.
[**enable_llama_rm_command**](ServicesResourceApi.md#enable_llama_rm_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaEnableLlamaRm | Not Supported.
[**enable_oozie_ha_command**](ServicesResourceApi.md#enable_oozie_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieEnableHa | Enable high availability (HA) for Oozie service.
[**enable_rm_ha_command**](ServicesResourceApi.md#enable_rm_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableRmHa | Enable high availability (HA) for a YARN ResourceManager.
[**enable_sentry_ha_command**](ServicesResourceApi.md#enable_sentry_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableSentryHa | Enable high availability (HA) for Sentry service.
[**enter_maintenance_mode**](ServicesResourceApi.md#enter_maintenance_mode) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enterMaintenanceMode | Put the service into maintenance mode.
[**exit_maintenance_mode**](ServicesResourceApi.md#exit_maintenance_mode) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/exitMaintenanceMode | Take the service out of maintenance mode.
[**first_run**](ServicesResourceApi.md#first_run) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/firstRun | Prepare and start a service.
[**get_client_config**](ServicesResourceApi.md#get_client_config) | **GET** /clusters/{clusterName}/services/{serviceName}/clientConfig | Download a zip-compressed archive of the client configuration, of a specific service.
[**get_hdfs_usage_report**](ServicesResourceApi.md#get_hdfs_usage_report) | **GET** /clusters/{clusterName}/services/{serviceName}/reports/hdfsUsageReport | Fetch the HDFS usage report.
[**get_impala_utilization**](ServicesResourceApi.md#get_impala_utilization) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaUtilization | Provides the resource utilization of the Impala service as well as the resource utilization per tenant.
[**get_metrics**](ServicesResourceApi.md#get_metrics) | **GET** /clusters/{clusterName}/services/{serviceName}/metrics | Fetch metric readings for a particular service.
[**get_mr_usage_report**](ServicesResourceApi.md#get_mr_usage_report) | **GET** /clusters/{clusterName}/services/{serviceName}/reports/mrUsageReport | Fetch the MR usage report.
[**get_yarn_utilization**](ServicesResourceApi.md#get_yarn_utilization) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnUtilization | Provides the resource utilization of the yarn service as well as the resource utilization per tenant.
[**hbase_upgrade_command**](ServicesResourceApi.md#hbase_upgrade_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hbaseUpgrade | Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5.
[**hdfs_create_tmp_dir**](ServicesResourceApi.md#hdfs_create_tmp_dir) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsCreateTmpDir | Creates a tmp directory on the HDFS filesystem.
[**hdfs_disable_auto_failover_command**](ServicesResourceApi.md#hdfs_disable_auto_failover_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableAutoFailover | Disable auto-failover for a highly available HDFS nameservice.
[**hdfs_disable_ha_command**](ServicesResourceApi.md#hdfs_disable_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableHa | Disable high availability (HA) for an HDFS NameNode.
[**hdfs_disable_nn_ha_command**](ServicesResourceApi.md#hdfs_disable_nn_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableNnHa | Disable High Availability (HA) with Automatic Failover for an HDFS NameNode.
[**hdfs_enable_auto_failover_command**](ServicesResourceApi.md#hdfs_enable_auto_failover_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableAutoFailover | Enable auto-failover for an HDFS nameservice.
[**hdfs_enable_ha_command**](ServicesResourceApi.md#hdfs_enable_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableHa | Enable high availability (HA) for an HDFS NameNode.
[**hdfs_enable_nn_ha_command**](ServicesResourceApi.md#hdfs_enable_nn_ha_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableNnHa | Enable High Availability (HA) with Automatic Failover for an HDFS NameNode.
[**hdfs_failover_command**](ServicesResourceApi.md#hdfs_failover_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsFailover | Initiate a failover in an HDFS HA NameNode pair.
[**hdfs_finalize_rolling_upgrade**](ServicesResourceApi.md#hdfs_finalize_rolling_upgrade) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsFinalizeRollingUpgrade | Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version.
[**hdfs_roll_edits_command**](ServicesResourceApi.md#hdfs_roll_edits_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsRollEdits | Roll the edits of an HDFS NameNode or Nameservice.
[**hdfs_upgrade_metadata_command**](ServicesResourceApi.md#hdfs_upgrade_metadata_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsUpgradeMetadata | Upgrade HDFS Metadata as part of a major version upgrade.
[**hive_create_metastore_database_command**](ServicesResourceApi.md#hive_create_metastore_database_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateMetastoreDatabase | Create the Hive Metastore Database.
[**hive_create_metastore_database_tables_command**](ServicesResourceApi.md#hive_create_metastore_database_tables_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateMetastoreDatabaseTables | Create the Hive Metastore Database tables.
[**hive_update_metastore_namenodes_command**](ServicesResourceApi.md#hive_update_metastore_namenodes_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveUpdateMetastoreNamenodes | Update Hive Metastore to point to a NameNode&#39;s Nameservice name instead of hostname.
[**hive_upgrade_metastore_command**](ServicesResourceApi.md#hive_upgrade_metastore_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveUpgradeMetastore | Upgrade Hive Metastore as part of a major version upgrade.
[**hive_validate_metastore_schema_command**](ServicesResourceApi.md#hive_validate_metastore_schema_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveValidateMetastoreSchema | Validate the Hive Metastore Schema.
[**hue_dump_db_command**](ServicesResourceApi.md#hue_dump_db_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueDumpDb | Runs Hue&#39;s dumpdata command.
[**hue_load_db_command**](ServicesResourceApi.md#hue_load_db_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueLoadDb | Runs Hue&#39;s loaddata command.
[**hue_sync_db_command**](ServicesResourceApi.md#hue_sync_db_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueSyncDb | Runs Hue&#39;s syncdb command.
[**impala_create_catalog_database_command**](ServicesResourceApi.md#impala_create_catalog_database_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateCatalogDatabase | .
[**impala_create_catalog_database_tables_command**](ServicesResourceApi.md#impala_create_catalog_database_tables_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateCatalogDatabaseTables | .
[**import_mr_configs_into_yarn**](ServicesResourceApi.md#import_mr_configs_into_yarn) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/importMrConfigsIntoYarn | Import MapReduce configuration into Yarn, overwriting Yarn configuration.
[**init_solr_command**](ServicesResourceApi.md#init_solr_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/initSolr | Initializes the Solr service in Zookeeper.
[**install_mr_framework_jars**](ServicesResourceApi.md#install_mr_framework_jars) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/installMrFrameworkJars | Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it.
[**install_oozie_share_lib**](ServicesResourceApi.md#install_oozie_share_lib) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/installOozieShareLib | Creates directory for Oozie user in HDFS and installs the ShareLib in it.
[**ks_migrate_to_sentry**](ServicesResourceApi.md#ks_migrate_to_sentry) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/migrateToSentry | Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer.
[**list_active_commands**](ServicesResourceApi.md#list_active_commands) | **GET** /clusters/{clusterName}/services/{serviceName}/commands | List active service commands.
[**list_role_types**](ServicesResourceApi.md#list_role_types) | **GET** /clusters/{clusterName}/services/{serviceName}/roleTypes | List the supported role types for a service.
[**list_service_commands**](ServicesResourceApi.md#list_service_commands) | **GET** /clusters/{clusterName}/services/{serviceName}/commandsByName | Lists all the commands that can be executed by name on the provided service.
[**offline_command**](ServicesResourceApi.md#offline_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/offline | Offline roles of a service.
[**oozie_create_embedded_database_command**](ServicesResourceApi.md#oozie_create_embedded_database_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieCreateEmbeddedDatabase | Create the Oozie Server Database.
[**oozie_dump_database_command**](ServicesResourceApi.md#oozie_dump_database_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieDumpDatabase | Dump the Oozie Server Database.
[**oozie_load_database_command**](ServicesResourceApi.md#oozie_load_database_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieLoadDatabase | Load the Oozie Server Database from dump.
[**oozie_upgrade_db_command**](ServicesResourceApi.md#oozie_upgrade_db_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieUpgradeDb | Upgrade Oozie Database schema as part of a major version upgrade.
[**read_service**](ServicesResourceApi.md#read_service) | **GET** /clusters/{clusterName}/services/{serviceName} | Retrieves details information about a service.
[**read_service_config**](ServicesResourceApi.md#read_service_config) | **GET** /clusters/{clusterName}/services/{serviceName}/config | Retrieves the configuration of a specific service.
[**read_services**](ServicesResourceApi.md#read_services) | **GET** /clusters/{clusterName}/services | Lists all services registered in the cluster.
[**recommission_command**](ServicesResourceApi.md#recommission_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/recommission | Recommission roles of a service.
[**recommission_with_start_command**](ServicesResourceApi.md#recommission_with_start_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/recommissionWithStart | Start and recommission roles of a service.
[**restart_command**](ServicesResourceApi.md#restart_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/restart | Restart the service.
[**rolling_restart**](ServicesResourceApi.md#rolling_restart) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/rollingRestart | Command to run rolling restart of roles in a service.
[**sentry_create_database_command**](ServicesResourceApi.md#sentry_create_database_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryCreateDatabase | Create the Sentry Server Database.
[**sentry_create_database_tables_command**](ServicesResourceApi.md#sentry_create_database_tables_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryCreateDatabaseTables | Create the Sentry Server Database tables.
[**sentry_upgrade_database_tables_command**](ServicesResourceApi.md#sentry_upgrade_database_tables_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryUpgradeDatabaseTables | Upgrade the Sentry Server Database tables.
[**service_command_by_name**](ServicesResourceApi.md#service_command_by_name) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/{commandName} | Executes a command on the service specified by name.
[**solr_bootstrap_collections_command**](ServicesResourceApi.md#solr_bootstrap_collections_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrBootstrapCollections | Bootstraps Solr Collections after the CDH upgrade.
[**solr_bootstrap_config_command**](ServicesResourceApi.md#solr_bootstrap_config_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrBootstrapConfig | Bootstraps Solr config during the CDH upgrade.
[**solr_config_backup_command**](ServicesResourceApi.md#solr_config_backup_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrConfigBackup | Backs up Solr configuration metadata before CDH upgrade.
[**solr_migrate_sentry_privileges_command**](ServicesResourceApi.md#solr_migrate_sentry_privileges_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrMigrateSentryPrivilegesCommand | Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service.
[**solr_reinitialize_state_for_upgrade_command**](ServicesResourceApi.md#solr_reinitialize_state_for_upgrade_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrReinitializeStateForUpgrade | Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state.
[**solr_validate_metadata_command**](ServicesResourceApi.md#solr_validate_metadata_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrValidateMetadata | Validates Solr metadata and configurations.
[**sqoop_create_database_tables_command**](ServicesResourceApi.md#sqoop_create_database_tables_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sqoopCreateDatabaseTables | Create the Sqoop2 Server Database tables.
[**sqoop_upgrade_db_command**](ServicesResourceApi.md#sqoop_upgrade_db_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sqoopUpgradeDb | Upgrade Sqoop Database schema as part of a major version upgrade.
[**start_command**](ServicesResourceApi.md#start_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/start | Start the service.
[**stop_command**](ServicesResourceApi.md#stop_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/stop | Stop the service.
[**switch_to_mr2**](ServicesResourceApi.md#switch_to_mr2) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/switchToMr2 | Change the cluster to use MR2 instead of MR1.
[**update_service**](ServicesResourceApi.md#update_service) | **PUT** /clusters/{clusterName}/services/{serviceName} | Updates service information.
[**update_service_config**](ServicesResourceApi.md#update_service_config) | **PUT** /clusters/{clusterName}/services/{serviceName}/config | Updates the service configuration with the given values.
[**yarn_format_state_store**](ServicesResourceApi.md#yarn_format_state_store) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnFormatStateStore | Formats the state store in ZooKeeper used for Resource Manager High Availability.
[**zoo_keeper_cleanup_command**](ServicesResourceApi.md#zoo_keeper_cleanup_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/zooKeeperCleanup | Clean up all running server instances of a ZooKeeper service.
[**zoo_keeper_init_command**](ServicesResourceApi.md#zoo_keeper_init_command) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/zooKeeperInit | Initializes all the server instances of a ZooKeeper service.


<a name="collect_yarn_application_diagnostics" id="collect_yarn_application_diagnostics"></a>
# **collect_yarn_application_diagnostics**
> ApiCommand collect_yarn_application_diagnostics(cluster_name, service_name, body=body)

Collect the Diagnostics data for Yarn applications.

Collect the Diagnostics data for Yarn applications <p> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the YARN service on which to run the command.
body = cm_client.ApiYarnApplicationDiagnosticsCollectionArgs() # ApiYarnApplicationDiagnosticsCollectionArgs | Arguments used for collecting diagnostics data for Yarn applications (optional)

try:
    # Collect the Diagnostics data for Yarn applications.
    api_response = api_instance.collect_yarn_application_diagnostics(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->collect_yarn_application_diagnostics: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the YARN service on which to run the command. | 
 **body** | [**ApiYarnApplicationDiagnosticsCollectionArgs**](ApiYarnApplicationDiagnosticsCollectionArgs.md)| Arguments used for collecting diagnostics data for Yarn applications | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_beeswax_warehouse_command" id="create_beeswax_warehouse_command"></a>
# **create_beeswax_warehouse_command**
> ApiCommand create_beeswax_warehouse_command(cluster_name, service_name)

Create the Beeswax role's Hive warehouse directory, on Hue services.

Create the Beeswax role's Hive warehouse directory, on Hue services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Hue service name.

try:
    # Create the Beeswax role's Hive warehouse directory, on Hue services.
    api_response = api_instance.create_beeswax_warehouse_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_beeswax_warehouse_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Hue service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_h_base_root_command" id="create_h_base_root_command"></a>
# **create_h_base_root_command**
> ApiCommand create_h_base_root_command(cluster_name, service_name)

Creates the root directory of an HBase service.

Creates the root directory of an HBase service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HBase service name.

try:
    # Creates the root directory of an HBase service.
    api_response = api_instance.create_h_base_root_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_h_base_root_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HBase service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_hive_user_dir_command" id="create_hive_user_dir_command"></a>
# **create_hive_user_dir_command**
> ApiCommand create_hive_user_dir_command(cluster_name, service_name)

Create the Hive user directory.

Create the Hive user directory <p> Available since API v4. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Hive service name.

try:
    # Create the Hive user directory.
    api_response = api_instance.create_hive_user_dir_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_hive_user_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Hive service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_hive_warehouse_command" id="create_hive_warehouse_command"></a>
# **create_hive_warehouse_command**
> ApiCommand create_hive_warehouse_command(cluster_name, service_name)

Create the Hive warehouse directory, on Hive services.

Create the Hive warehouse directory, on Hive services. <p> Available since API v3. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Hive service name.

try:
    # Create the Hive warehouse directory, on Hive services.
    api_response = api_instance.create_hive_warehouse_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_hive_warehouse_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Hive service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_impala_user_dir_command" id="create_impala_user_dir_command"></a>
# **create_impala_user_dir_command**
> ApiCommand create_impala_user_dir_command(cluster_name, service_name)

Create the Impala user directory.

Create the Impala user directory <p> Available since API v6. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Impala service name.

try:
    # Create the Impala user directory.
    api_response = api_instance.create_impala_user_dir_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_impala_user_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Impala service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_oozie_db" id="create_oozie_db"></a>
# **create_oozie_db**
> ApiCommand create_oozie_db(cluster_name, service_name)

Creates the Oozie Database Schema in the configured database.

Creates the Oozie Database Schema in the configured database. This command does not create database. This command creates only tables required by Oozie. To create database, please refer to oozieCreateEmbeddedDatabase()  <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Oozie service on which to run the command.

try:
    # Creates the Oozie Database Schema in the configured database.
    api_response = api_instance.create_oozie_db(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_oozie_db: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Oozie service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_services" id="create_services"></a>
# **create_services**
> ApiServiceList create_services(cluster_name, body=body)

Creates a list of services.

Creates a list of services. <p> There are typically two service creation strategies: <ol> <li> The caller may choose to set up a new service piecemeal, by first creating the service itself (without any roles or configuration), and then create the roles, and then specify configuration. </li> <li> Alternatively, the caller can pack all the information in one call, by fully specifying the fields in the com.cloudera.api.model.ApiService object, with <ul> <li>service config and role type config, and</li> <li>role to host assignment.</li> </ul> </li> </ol>  <table> <thead> <tr> <th>Cluster Version</th> <th>Available Service Types</th> </tr> </thead> <tbody> <tr> <td>CDH4</td> <td>HDFS, MAPREDUCE, HBASE, OOZIE, ZOOKEEPER, HUE, YARN, IMPALA, FLUME, HIVE, SOLR, SQOOP, KS_INDEXER</td> </tr> <tr> <td>CDH5</td> <td>HDFS, MAPREDUCE, HBASE, OOZIE, ZOOKEEPER, HUE, YARN, IMPALA, FLUME, HIVE, SOLR, SQOOP, KS_INDEXER, SQOOP_CLIENT, SENTRY, ACCUMULO16, KMS, SPARK_ON_YARN, KAFKA </td> </tr> </tbody> </table>  As of V6, GET /{clusterName}/serviceTypes should be used to get the service types available to the cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
body = cm_client.ApiServiceList() # ApiServiceList | Details of the services to create. (optional)

try:
    # Creates a list of services.
    api_response = api_instance.create_services(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_services: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **body** | [**ApiServiceList**](ApiServiceList.md)| Details of the services to create. | [optional] 

### Return type

[**ApiServiceList**](ApiServiceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_solr_hdfs_home_dir_command" id="create_solr_hdfs_home_dir_command"></a>
# **create_solr_hdfs_home_dir_command**
> ApiCommand create_solr_hdfs_home_dir_command(cluster_name, service_name)

Creates the home directory of a Solr service in HDFS.

Creates the home directory of a Solr service in HDFS.  <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Solr service name.

try:
    # Creates the home directory of a Solr service in HDFS.
    api_response = api_instance.create_solr_hdfs_home_dir_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_solr_hdfs_home_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_sqoop_user_dir_command" id="create_sqoop_user_dir_command"></a>
# **create_sqoop_user_dir_command**
> ApiCommand create_sqoop_user_dir_command(cluster_name, service_name)

Creates the user directory of a Sqoop service in HDFS.

Creates the user directory of a Sqoop service in HDFS.  <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Sqoop service name.

try:
    # Creates the user directory of a Sqoop service in HDFS.
    api_response = api_instance.create_sqoop_user_dir_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_sqoop_user_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Sqoop service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_yarn_cm_container_usage_input_dir_command" id="create_yarn_cm_container_usage_input_dir_command"></a>
# **create_yarn_cm_container_usage_input_dir_command**
> ApiCommand create_yarn_cm_container_usage_input_dir_command(cluster_name, service_name)

Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics.

Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics. <p> Available since API v13. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The YARN service name.

try:
    # Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics.
    api_response = api_instance.create_yarn_cm_container_usage_input_dir_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_yarn_cm_container_usage_input_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The YARN service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_yarn_job_history_dir_command" id="create_yarn_job_history_dir_command"></a>
# **create_yarn_job_history_dir_command**
> ApiCommand create_yarn_job_history_dir_command(cluster_name, service_name)

Create the Yarn job history directory.

Create the Yarn job history directory <p> Available since API v6. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The YARN service name.

try:
    # Create the Yarn job history directory.
    api_response = api_instance.create_yarn_job_history_dir_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_yarn_job_history_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The YARN service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_yarn_node_manager_remote_app_log_dir_command" id="create_yarn_node_manager_remote_app_log_dir_command"></a>
# **create_yarn_node_manager_remote_app_log_dir_command**
> ApiCommand create_yarn_node_manager_remote_app_log_dir_command(cluster_name, service_name)

Create the Yarn NodeManager remote application log directory.

Create the Yarn NodeManager remote application log directory <p> Available since API v6. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The YARN service name.

try:
    # Create the Yarn NodeManager remote application log directory.
    api_response = api_instance.create_yarn_node_manager_remote_app_log_dir_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->create_yarn_node_manager_remote_app_log_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The YARN service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="decommission_command" id="decommission_command"></a>
# **decommission_command**
> ApiCommand decommission_command(cluster_name, service_name, body=body)

Decommission roles of a service.

Decommission roles of a service. <p> For HBase services, the list should contain names of RegionServers to decommission. <p> For HDFS services, the list should contain names of DataNodes to decommission.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HBase service name.
body = cm_client.ApiRoleNameList() # ApiRoleNameList | List of role names to decommision. (optional)

try:
    # Decommission roles of a service.
    api_response = api_instance.decommission_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->decommission_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HBase service name. | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to decommision. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_service" id="delete_service"></a>
# **delete_service**
> ApiService delete_service(cluster_name, service_name)

Deletes a service from the system.

Deletes a service from the system.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service to delete.

try:
    # Deletes a service from the system.
    api_response = api_instance.delete_service(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->delete_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service to delete. | 

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deploy_client_config_command" id="deploy_client_config_command"></a>
# **deploy_client_config_command**
> ApiCommand deploy_client_config_command(cluster_name, service_name, body=body)

Deploy a service's client configuration.

Deploy a service's client configuration. <p> The client configuration is deployed to the hosts where the given roles are running. <p/> Added in v3: passing null for the role name list will deploy client configs to all known service roles. Added in v6: passing an empty role name list will deploy client configs to all known service roles. <p/> In Cloudera Manager 5.3 and newer, client configurations are fully managed, meaning that the server maintains state about which client configurations should exist and be managed by alternatives, and the agents actively rectify their hosts with this state. Consequently, if this API call is made with a specific set of roles, Cloudera Manager will deactivate, from alternatives, any deployed client configs from any non-gateway roles that are <em>not</em> specified as arguments. Gateway roles are always preserved, and calling this API with an empty or null argument continues to deploy to all roles. <p/>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
body = cm_client.ApiRoleNameList() # ApiRoleNameList | List of role names. (optional)

try:
    # Deploy a service's client configuration.
    api_response = api_instance.deploy_client_config_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->deploy_client_config_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="disable_jt_ha_command" id="disable_jt_ha_command"></a>
# **disable_jt_ha_command**
> ApiCommand disable_jt_ha_command(cluster_name, service_name, body=body)

Disable high availability (HA) for JobTracker.

Disable high availability (HA) for JobTracker.  As part of disabling HA, any services that depend on the MapReduce service being modified will be stopped. The command arguments provide options to specify name of JobTracker that will be preserved. The Command will redeploy the client configurations for services of the cluster after HA has been disabled.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The MapReduce service name.
body = cm_client.ApiDisableJtHaArguments() # ApiDisableJtHaArguments | Arguments for the command. (optional)

try:
    # Disable high availability (HA) for JobTracker.
    api_response = api_instance.disable_jt_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->disable_jt_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The MapReduce service name. | 
 **body** | [**ApiDisableJtHaArguments**](ApiDisableJtHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="disable_llama_ha_command" id="disable_llama_ha_command"></a>
# **disable_llama_ha_command**
> ApiCommand disable_llama_ha_command(cluster_name, service_name, body=body)

Not Supported.

Not Supported. Llama was removed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiDisableLlamaHaArguments() # ApiDisableLlamaHaArguments |  (optional)

try:
    # Not Supported.
    api_response = api_instance.disable_llama_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->disable_llama_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiDisableLlamaHaArguments**](ApiDisableLlamaHaArguments.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="disable_llama_rm_command" id="disable_llama_rm_command"></a>
# **disable_llama_rm_command**
> ApiCommand disable_llama_rm_command(cluster_name, service_name)

Not Supported.

Not Supported. Llama was removed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 

try:
    # Not Supported.
    api_response = api_instance.disable_llama_rm_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->disable_llama_rm_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="disable_oozie_ha_command" id="disable_oozie_ha_command"></a>
# **disable_oozie_ha_command**
> ApiCommand disable_oozie_ha_command(cluster_name, service_name, body=body)

Disable high availability (HA) for Oozie.

Disable high availability (HA) for Oozie.  As part of disabling HA, any services that depend on the Oozie service being modified will be stopped. The command arguments provide options to specify name of Oozie Server that will be preserved. After deleting, other Oozie servers, all the services that were stopped are restarted.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Oozie service name.
body = cm_client.ApiDisableOozieHaArguments() # ApiDisableOozieHaArguments | Arguments for the command. (optional)

try:
    # Disable high availability (HA) for Oozie.
    api_response = api_instance.disable_oozie_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->disable_oozie_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Oozie service name. | 
 **body** | [**ApiDisableOozieHaArguments**](ApiDisableOozieHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="disable_rm_ha_command" id="disable_rm_ha_command"></a>
# **disable_rm_ha_command**
> ApiCommand disable_rm_ha_command(cluster_name, service_name, body=body)

Disable high availability (HA) for ResourceManager.

Disable high availability (HA) for ResourceManager.  As part of disabling HA, any services that depend on the YARN service being modified will be stopped. The command arguments provide options to specify name of ResourceManager that will be preserved. The command will redeploy the client configurations for services of the cluster after HA has been disabled.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The YARN service name.
body = cm_client.ApiDisableRmHaArguments() # ApiDisableRmHaArguments | Arguments for the command. (optional)

try:
    # Disable high availability (HA) for ResourceManager.
    api_response = api_instance.disable_rm_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->disable_rm_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The YARN service name. | 
 **body** | [**ApiDisableRmHaArguments**](ApiDisableRmHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="disable_sentry_ha_command" id="disable_sentry_ha_command"></a>
# **disable_sentry_ha_command**
> ApiCommand disable_sentry_ha_command(cluster_name, service_name, body=body)

Disable high availability (HA) for Sentry service.

Disable high availability (HA) for Sentry service. <p> This command only applies to CDH 5.13+ Sentry services. <p> The command will keep exactly one Sentry server, on the specified host, and update the ZooKeeper configs needed for Sentry. <p> All services that depend on HDFS will be restarted after enabling Sentry HA. <p> Note: Sentry doesn't support Rolling Restart.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Sentry service name.
body = cm_client.ApiDisableSentryHaArgs() # ApiDisableSentryHaArgs | An instance of ApiDisableSentryHaArgs representing the arguments to the command. (optional)

try:
    # Disable high availability (HA) for Sentry service.
    api_response = api_instance.disable_sentry_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->disable_sentry_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Sentry service name. | 
 **body** | [**ApiDisableSentryHaArgs**](ApiDisableSentryHaArgs.md)| An instance of ApiDisableSentryHaArgs representing the arguments to the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enable_jt_ha_command" id="enable_jt_ha_command"></a>
# **enable_jt_ha_command**
> ApiCommand enable_jt_ha_command(cluster_name, service_name, body=body)

Enable high availability (HA) for a JobTracker.

Enable high availability (HA) for a JobTracker. <p> This command only applies to CDH4 MapReduce services. <p> The command will create a new JobTracker on the specified host and then create an active/standby pair with the existing JobTracker. Autofailover will be enabled using ZooKeeper. A ZNode will be created for this purpose. Command arguments provide option to forcefully create this ZNode if one already exists. A node may already exists if JobTracker was previously enabled in HA mode but HA mode was disabled later on. The ZNode is not deleted when HA is disabled. <p> As part of enabling HA, any services that depends on the MapReduce service being modified will be stopped. Command will redeploy the client configurations for services of the cluster after HA has been enabled.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The MapReduce service name.
body = cm_client.ApiEnableJtHaArguments() # ApiEnableJtHaArguments | Arguments for the command. (optional)

try:
    # Enable high availability (HA) for a JobTracker.
    api_response = api_instance.enable_jt_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->enable_jt_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The MapReduce service name. | 
 **body** | [**ApiEnableJtHaArguments**](ApiEnableJtHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enable_llama_ha_command" id="enable_llama_ha_command"></a>
# **enable_llama_ha_command**
> ApiCommand enable_llama_ha_command(cluster_name, service_name, body=body)

Not Supported.

Not Supported. Llama was removed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiEnableLlamaHaArguments() # ApiEnableLlamaHaArguments |  (optional)

try:
    # Not Supported.
    api_response = api_instance.enable_llama_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->enable_llama_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiEnableLlamaHaArguments**](ApiEnableLlamaHaArguments.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enable_llama_rm_command" id="enable_llama_rm_command"></a>
# **enable_llama_rm_command**
> ApiCommand enable_llama_rm_command(cluster_name, service_name, body=body)

Not Supported.

Not Supported. Llama was removed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiEnableLlamaRmArguments() # ApiEnableLlamaRmArguments |  (optional)

try:
    # Not Supported.
    api_response = api_instance.enable_llama_rm_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->enable_llama_rm_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiEnableLlamaRmArguments**](ApiEnableLlamaRmArguments.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enable_oozie_ha_command" id="enable_oozie_ha_command"></a>
# **enable_oozie_ha_command**
> ApiCommand enable_oozie_ha_command(cluster_name, service_name, body=body)

Enable high availability (HA) for Oozie service.

Enable high availability (HA) for Oozie service. <p> This command only applies to CDH5+ Oozie services. <p> The command will create new Oozie Servers on the specified hosts and set the ZooKeeper and Load Balancer configs needed for Oozie HA. <p> As part of enabling HA, any services that depends on the Oozie service being modified will be stopped and restarted after enabling Oozie HA.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Oozie service name.
body = cm_client.ApiEnableOozieHaArguments() # ApiEnableOozieHaArguments | Arguments for the command. (optional)

try:
    # Enable high availability (HA) for Oozie service.
    api_response = api_instance.enable_oozie_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->enable_oozie_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Oozie service name. | 
 **body** | [**ApiEnableOozieHaArguments**](ApiEnableOozieHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enable_rm_ha_command" id="enable_rm_ha_command"></a>
# **enable_rm_ha_command**
> ApiCommand enable_rm_ha_command(cluster_name, service_name, body=body)

Enable high availability (HA) for a YARN ResourceManager.

Enable high availability (HA) for a YARN ResourceManager. <p> This command only applies to CDH5+ YARN services. <p> The command will create a new ResourceManager on the specified host and then create an active/standby pair with the existing ResourceManager. Autofailover will be enabled using ZooKeeper. <p> As part of enabling HA, any services that depends on the YARN service being modified will be stopped. Command will redeploy the client configurations for services of the cluster after HA has been enabled.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The YARN service name.
body = cm_client.ApiEnableRmHaArguments() # ApiEnableRmHaArguments | Arguments for the command. (optional)

try:
    # Enable high availability (HA) for a YARN ResourceManager.
    api_response = api_instance.enable_rm_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->enable_rm_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The YARN service name. | 
 **body** | [**ApiEnableRmHaArguments**](ApiEnableRmHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enable_sentry_ha_command" id="enable_sentry_ha_command"></a>
# **enable_sentry_ha_command**
> ApiCommand enable_sentry_ha_command(cluster_name, service_name, body=body)

Enable high availability (HA) for Sentry service.

Enable high availability (HA) for Sentry service. <p> This command only applies to CDH 5.13+ Sentry services. <p> The command will create a new Sentry server on the specified host and set the ZooKeeper configs needed for Sentry HA. <p> As part of enabling HA, all services that depend on HDFS will be restarted after enabling Sentry HA. <p> Note: Sentry doesn't support Rolling Restart.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Sentry service name.
body = cm_client.ApiEnableSentryHaArgs() # ApiEnableSentryHaArgs | An instance of ApiEnableSentryHaArgs representing the arguments to the command. (optional)

try:
    # Enable high availability (HA) for Sentry service.
    api_response = api_instance.enable_sentry_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->enable_sentry_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Sentry service name. | 
 **body** | [**ApiEnableSentryHaArgs**](ApiEnableSentryHaArgs.md)| An instance of ApiEnableSentryHaArgs representing the arguments to the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enter_maintenance_mode" id="enter_maintenance_mode"></a>
# **enter_maintenance_mode**
> ApiCommand enter_maintenance_mode(cluster_name, service_name)

Put the service into maintenance mode.

Put the service into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.

try:
    # Put the service into maintenance mode.
    api_response = api_instance.enter_maintenance_mode(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->enter_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="exit_maintenance_mode" id="exit_maintenance_mode"></a>
# **exit_maintenance_mode**
> ApiCommand exit_maintenance_mode(cluster_name, service_name)

Take the service out of maintenance mode.

Take the service out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.

try:
    # Take the service out of maintenance mode.
    api_response = api_instance.exit_maintenance_mode(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->exit_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="first_run" id="first_run"></a>
# **first_run**
> ApiCommand first_run(cluster_name, service_name)

Prepare and start a service.

Prepare and start a service.  <p> Perform all the steps needed to prepare the service and start it. </p>  <p> Available since API v7. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the cluster.

try:
    # Prepare and start a service.
    api_response = api_instance.first_run(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->first_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the cluster. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_client_config" id="get_client_config"></a>
# **get_client_config**
> file get_client_config(cluster_name, service_name)

Download a zip-compressed archive of the client configuration, of a specific service.

Download a zip-compressed archive of the client configuration, of a specific service. This resource does not require any authentication.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.

try:
    # Download a zip-compressed archive of the client configuration, of a specific service.
    api_response = api_instance.get_client_config(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->get_client_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 

### Return type

[**file**](file.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_hdfs_usage_report" id="get_hdfs_usage_report"></a>
# **get_hdfs_usage_report**
> ApiHdfsUsageReport get_hdfs_usage_report(cluster_name, service_name, aggregation=aggregation, _from=_from, nameservice=nameservice, to=to)

Fetch the HDFS usage report.

Fetch the HDFS usage report. For the requested time range, at the specified aggregation intervals, the report shows HDFS disk usages per user. <p> This call supports returning JSON or CSV, as determined by the \"Accept\" header of application/json or text/csv. <p> Available since API v4. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
aggregation = 'daily' # str | The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\". (optional) (default to daily)
_from = '_from_example' # str | The (optional) start time of the report in ISO 8601 format ( defaults to 24 hours before \"to\" time). (optional)
nameservice = 'nameservice_example' # str | The (optional) HDFS nameservice. Required for HA setup. (optional)
to = 'now' # str | The (optional) end time of the report in ISO 8601 format ( defaults to now). (optional) (default to now)

try:
    # Fetch the HDFS usage report.
    api_response = api_instance.get_hdfs_usage_report(cluster_name, service_name, aggregation=aggregation, _from=_from, nameservice=nameservice, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->get_hdfs_usage_report: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **aggregation** | **str**| The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\". | [optional] [default to daily]
 **_from** | **str**| The (optional) start time of the report in ISO 8601 format ( defaults to 24 hours before \"to\" time). | [optional] 
 **nameservice** | **str**| The (optional) HDFS nameservice. Required for HA setup. | [optional] 
 **to** | **str**| The (optional) end time of the report in ISO 8601 format ( defaults to now). | [optional] [default to now]

### Return type

[**ApiHdfsUsageReport**](ApiHdfsUsageReport.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_impala_utilization" id="get_impala_utilization"></a>
# **get_impala_utilization**
> ApiImpalaUtilization get_impala_utilization(cluster_name, service_name, days_of_week=days_of_week, end_hour_of_day=end_hour_of_day, _from=_from, start_hour_of_day=start_hour_of_day, tenant_type=tenant_type, to=to)

Provides the resource utilization of the Impala service as well as the resource utilization per tenant.

Provides the resource utilization of the Impala service as well as the resource utilization per tenant. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | service name
days_of_week = ['days_of_week_example'] # list[str] | The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified. (optional)
end_hour_of_day = 23 # int | The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. (optional) (default to 23)
_from = '_from_example' # str | Start of the time range to report utilization in ISO 8601 format. (optional)
start_hour_of_day = 0 # int | The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. (optional) (default to 0)
tenant_type = 'POOL' # str | The type of the tenant (POOL or USER). (optional) (default to POOL)
to = 'now' # str | End of the the time range to report utilization in ISO 8601 format (defaults to now). (optional) (default to now)

try:
    # Provides the resource utilization of the Impala service as well as the resource utilization per tenant.
    api_response = api_instance.get_impala_utilization(cluster_name, service_name, days_of_week=days_of_week, end_hour_of_day=end_hour_of_day, _from=_from, start_hour_of_day=start_hour_of_day, tenant_type=tenant_type, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->get_impala_utilization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| service name | 
 **days_of_week** | [**list[str]**](str.md)| The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified. | [optional] 
 **end_hour_of_day** | **int**| The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. | [optional] [default to 23]
 **_from** | **str**| Start of the time range to report utilization in ISO 8601 format. | [optional] 
 **start_hour_of_day** | **int**| The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. | [optional] [default to 0]
 **tenant_type** | **str**| The type of the tenant (POOL or USER). | [optional] [default to POOL]
 **to** | **str**| End of the the time range to report utilization in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiImpalaUtilization**](ApiImpalaUtilization.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_metrics" id="get_metrics"></a>
# **get_metrics**
> ApiMetricList get_metrics(cluster_name, service_name, _from=_from, metrics=metrics, to=to, view=view)

Fetch metric readings for a particular service.

Fetch metric readings for a particular service. <p> By default, this call will look up all metrics available for the service. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the service, even if no readings are available in the requested window. <p> HDFS services that have more than one nameservice will not expose any metrics. Instead, the nameservices should be queried separately. <p/>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service.
_from = '_from_example' # str | Start of the period to query. (optional)
metrics = ['metrics_example'] # list[str] | Filter for which metrics to query. (optional)
to = 'now' # str | End of the period to query. (optional) (default to now)
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Fetch metric readings for a particular service.
    api_response = api_instance.get_metrics(cluster_name, service_name, _from=_from, metrics=metrics, to=to, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->get_metrics: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service. | 
 **_from** | **str**| Start of the period to query. | [optional] 
 **metrics** | [**list[str]**](str.md)| Filter for which metrics to query. | [optional] 
 **to** | **str**| End of the period to query. | [optional] [default to now]
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_mr_usage_report" id="get_mr_usage_report"></a>
# **get_mr_usage_report**
> ApiMrUsageReport get_mr_usage_report(cluster_name, service_name, aggregation=aggregation, _from=_from, to=to)

Fetch the MR usage report.

Fetch the MR usage report. For the requested time range, at the specified aggregation intervals, the report shows job CPU usages (and other metrics) per user. <p> This call supports returning JSON or CSV, as determined by the \"Accept\" header of application/json or text/csv. <p> Available since API v4. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The MR service name.
aggregation = 'daily' # str | The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\". (optional) (default to daily)
_from = '_from_example' # str | The (optional) start time of the report in ISO 8601 format (defaults to 24 hours before \"to\" time). (optional)
to = 'now' # str | The (optional) end time of the report in ISO 8601 format (defaults to now). (optional) (default to now)

try:
    # Fetch the MR usage report.
    api_response = api_instance.get_mr_usage_report(cluster_name, service_name, aggregation=aggregation, _from=_from, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->get_mr_usage_report: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The MR service name. | 
 **aggregation** | **str**| The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\". | [optional] [default to daily]
 **_from** | **str**| The (optional) start time of the report in ISO 8601 format (defaults to 24 hours before \"to\" time). | [optional] 
 **to** | **str**| The (optional) end time of the report in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiMrUsageReport**](ApiMrUsageReport.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_yarn_utilization" id="get_yarn_utilization"></a>
# **get_yarn_utilization**
> ApiYarnUtilization get_yarn_utilization(cluster_name, service_name, days_of_week=days_of_week, end_hour_of_day=end_hour_of_day, _from=_from, start_hour_of_day=start_hour_of_day, tenant_type=tenant_type, to=to)

Provides the resource utilization of the yarn service as well as the resource utilization per tenant.

Provides the resource utilization of the yarn service as well as the resource utilization per tenant. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | service name
days_of_week = ['days_of_week_example'] # list[str] | The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified. (optional)
end_hour_of_day = 23 # int | The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. (optional) (default to 23)
_from = '_from_example' # str | Start of the time range to report utilization in ISO 8601 format. (optional)
start_hour_of_day = 0 # int | The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. (optional) (default to 0)
tenant_type = 'POOL' # str | The type of the tenant (POOL or USER). (optional) (default to POOL)
to = 'now' # str | End of the the time range to report utilization in ISO 8601 format (defaults to now). (optional) (default to now)

try:
    # Provides the resource utilization of the yarn service as well as the resource utilization per tenant.
    api_response = api_instance.get_yarn_utilization(cluster_name, service_name, days_of_week=days_of_week, end_hour_of_day=end_hour_of_day, _from=_from, start_hour_of_day=start_hour_of_day, tenant_type=tenant_type, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->get_yarn_utilization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| service name | 
 **days_of_week** | [**list[str]**](str.md)| The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified. | [optional] 
 **end_hour_of_day** | **int**| The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. | [optional] [default to 23]
 **_from** | **str**| Start of the time range to report utilization in ISO 8601 format. | [optional] 
 **start_hour_of_day** | **int**| The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. | [optional] [default to 0]
 **tenant_type** | **str**| The type of the tenant (POOL or USER). | [optional] [default to POOL]
 **to** | **str**| End of the the time range to report utilization in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiYarnUtilization**](ApiYarnUtilization.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hbase_upgrade_command" id="hbase_upgrade_command"></a>
# **hbase_upgrade_command**
> ApiCommand hbase_upgrade_command(cluster_name, service_name)

Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5.

Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5. <p/> This is required in order to run HBase after upgrade. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HBase service name.

try:
    # Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5.
    api_response = api_instance.hbase_upgrade_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hbase_upgrade_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HBase service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_create_tmp_dir" id="hdfs_create_tmp_dir"></a>
# **hdfs_create_tmp_dir**
> ApiCommand hdfs_create_tmp_dir(cluster_name, service_name)

Creates a tmp directory on the HDFS filesystem.

Creates a tmp directory on the HDFS filesystem. <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the HDFS service on which to run the command.

try:
    # Creates a tmp directory on the HDFS filesystem.
    api_response = api_instance.hdfs_create_tmp_dir(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_create_tmp_dir: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the HDFS service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_disable_auto_failover_command" id="hdfs_disable_auto_failover_command"></a>
# **hdfs_disable_auto_failover_command**
> ApiCommand hdfs_disable_auto_failover_command(cluster_name, service_name, body=body)

Disable auto-failover for a highly available HDFS nameservice.

Disable auto-failover for a highly available HDFS nameservice. <p> The command will modify the nameservice's NameNodes configuration to disable automatic failover, and delete the existing failover controllers. <p> The ZooKeeper dependency of the service will not be removed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
body = 'body_example' # str | The nameservice name. (optional)

try:
    # Disable auto-failover for a highly available HDFS nameservice.
    api_response = api_instance.hdfs_disable_auto_failover_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_disable_auto_failover_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **body** | **str**| The nameservice name. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_disable_ha_command" id="hdfs_disable_ha_command"></a>
# **hdfs_disable_ha_command**
> ApiCommand hdfs_disable_ha_command(cluster_name, service_name, body=body)

Disable high availability (HA) for an HDFS NameNode.

Disable high availability (HA) for an HDFS NameNode. <p> The NameNode to be kept must be running before HA can be disabled. <p> As part of disabling HA, any services that depend on the HDFS service being modified will be stopped. The command arguments provide options to re-start these services and to re-deploy the client configurations for services of the cluster after HA has been disabled.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
body = cm_client.ApiHdfsDisableHaArguments() # ApiHdfsDisableHaArguments | Arguments for the command. (optional)

try:
    # Disable high availability (HA) for an HDFS NameNode.
    api_response = api_instance.hdfs_disable_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_disable_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **body** | [**ApiHdfsDisableHaArguments**](ApiHdfsDisableHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_disable_nn_ha_command" id="hdfs_disable_nn_ha_command"></a>
# **hdfs_disable_nn_ha_command**
> ApiCommand hdfs_disable_nn_ha_command(cluster_name, service_name, body=body)

Disable High Availability (HA) with Automatic Failover for an HDFS NameNode.

Disable High Availability (HA) with Automatic Failover for an HDFS NameNode. <p> As part of disabling HA, any services that depend on the HDFS service being modified will be stopped. The command will delete the Standby NameNode associated with the specified NameNode. Any FailoverControllers associated with the NameNode's nameservice are also deleted. A SecondaryNameNode is created on the host specified by the arugments. <p> If no nameservices uses Quorum Journal after HA is disabled for the specified nameservice, then all JournalNodes are also deleted. <p> Then, HDFS service is restarted and all services that were stopped are started again afterwards. Finally, client configs for HDFS and its depedents will be re-deployed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
body = cm_client.ApiDisableNnHaArguments() # ApiDisableNnHaArguments | Arguments for the command. (optional)

try:
    # Disable High Availability (HA) with Automatic Failover for an HDFS NameNode.
    api_response = api_instance.hdfs_disable_nn_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_disable_nn_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **body** | [**ApiDisableNnHaArguments**](ApiDisableNnHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_enable_auto_failover_command" id="hdfs_enable_auto_failover_command"></a>
# **hdfs_enable_auto_failover_command**
> ApiCommand hdfs_enable_auto_failover_command(cluster_name, service_name, body=body)

Enable auto-failover for an HDFS nameservice.

Enable auto-failover for an HDFS nameservice. <p> This command requires that the nameservice exists, and HA has been configured for that nameservice. <p> The command will create the needed failover controllers, perform the needed initialization and configuration, and will start the new roles. The existing NameNodes which are part of the nameservice will be re-started in the process. <p> This process may require changing the service's configuration, to add a dependency on the provided ZooKeeper service. This will be done if such a dependency has not been configured yet, and will cause roles that are not affected by this command to show an \"outdated configuration\" status. <p> If a ZooKeeper dependency has already been set up by some other means, it does not need to be provided in the command arguments.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
body = cm_client.ApiHdfsFailoverArguments() # ApiHdfsFailoverArguments | Arguments for the command. (optional)

try:
    # Enable auto-failover for an HDFS nameservice.
    api_response = api_instance.hdfs_enable_auto_failover_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_enable_auto_failover_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **body** | [**ApiHdfsFailoverArguments**](ApiHdfsFailoverArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_enable_ha_command" id="hdfs_enable_ha_command"></a>
# **hdfs_enable_ha_command**
> ApiCommand hdfs_enable_ha_command(cluster_name, service_name, body=body)

Enable high availability (HA) for an HDFS NameNode.

Enable high availability (HA) for an HDFS NameNode. <p> The command will set up the given \"active\" and \"stand-by\" NameNodes as an HA pair. Both nodes need to already exist. <p> If there is a SecondaryNameNode associated with either given NameNode instance, it will be deleted. <p> Note that while the shared edits path may be different for both nodes, they need to point to the same underlying storage (e.g., an NFS share). <p> As part of enabling HA, any services that depend on the HDFS service being modified will be stopped. The command arguments provide options to re-start these services and to re-deploy the client configurations for services of the cluster after HA has been enabled.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
body = cm_client.ApiHdfsHaArguments() # ApiHdfsHaArguments | Arguments for the command. (optional)

try:
    # Enable high availability (HA) for an HDFS NameNode.
    api_response = api_instance.hdfs_enable_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_enable_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **body** | [**ApiHdfsHaArguments**](ApiHdfsHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_enable_nn_ha_command" id="hdfs_enable_nn_ha_command"></a>
# **hdfs_enable_nn_ha_command**
> ApiCommand hdfs_enable_nn_ha_command(cluster_name, service_name, body=body)

Enable High Availability (HA) with Automatic Failover for an HDFS NameNode.

Enable High Availability (HA) with Automatic Failover for an HDFS NameNode. <p> The command will create a Standby NameNode for the given nameservice and create FailoverControllers for both Active and Standby NameNodes. The SecondaryNameNode associated with the Active NameNode will be deleted. <p> The command will also create JournalNodes needed for HDFS HA if they do not already exist. <p> As part of enabling HA, any services that depend on the HDFS service being modified will be stopped. They will be restarted after HA has been enabled. Finally, client configs for HDFS and its depedents will be re-deployed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
body = cm_client.ApiEnableNnHaArguments() # ApiEnableNnHaArguments | Arguments for the command. (optional)

try:
    # Enable High Availability (HA) with Automatic Failover for an HDFS NameNode.
    api_response = api_instance.hdfs_enable_nn_ha_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_enable_nn_ha_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **body** | [**ApiEnableNnHaArguments**](ApiEnableNnHaArguments.md)| Arguments for the command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_failover_command" id="hdfs_failover_command"></a>
# **hdfs_failover_command**
> ApiCommand hdfs_failover_command(cluster_name, service_name, force=force, body=body)

Initiate a failover in an HDFS HA NameNode pair.

Initiate a failover in an HDFS HA NameNode pair. <p> The arguments should contain the names of the two NameNodes in the HA pair. The first one should be the currently active NameNode, the second one the NameNode to be made active.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
force = false # bool | Whether to force failover. (optional) (default to false)
body = cm_client.ApiRoleNameList() # ApiRoleNameList | Names of the NameNodes in the HA pair. (optional)

try:
    # Initiate a failover in an HDFS HA NameNode pair.
    api_response = api_instance.hdfs_failover_command(cluster_name, service_name, force=force, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_failover_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **force** | **bool**| Whether to force failover. | [optional] [default to false]
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| Names of the NameNodes in the HA pair. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_finalize_rolling_upgrade" id="hdfs_finalize_rolling_upgrade"></a>
# **hdfs_finalize_rolling_upgrade**
> ApiCommand hdfs_finalize_rolling_upgrade(cluster_name, service_name)

Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version.

Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version. Should be done after doing a rolling upgrade to a CDH version >= 5.2.0. <p> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.

try:
    # Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version.
    api_response = api_instance.hdfs_finalize_rolling_upgrade(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_finalize_rolling_upgrade: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_roll_edits_command" id="hdfs_roll_edits_command"></a>
# **hdfs_roll_edits_command**
> ApiCommand hdfs_roll_edits_command(cluster_name, service_name, body=body)

Roll the edits of an HDFS NameNode or Nameservice.

Roll the edits of an HDFS NameNode or Nameservice. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.
body = cm_client.ApiRollEditsArgs() # ApiRollEditsArgs | Arguments to the Roll Edits command. (optional)

try:
    # Roll the edits of an HDFS NameNode or Nameservice.
    api_response = api_instance.hdfs_roll_edits_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_roll_edits_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 
 **body** | [**ApiRollEditsArgs**](ApiRollEditsArgs.md)| Arguments to the Roll Edits command. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_upgrade_metadata_command" id="hdfs_upgrade_metadata_command"></a>
# **hdfs_upgrade_metadata_command**
> ApiCommand hdfs_upgrade_metadata_command(cluster_name, service_name)

Upgrade HDFS Metadata as part of a major version upgrade.

Upgrade HDFS Metadata as part of a major version upgrade. <p/> When doing a major version upgrade for HDFS, it is necessary to start HDFS in a special mode where it will do any necessary upgrades of stored metadata. Trying to start HDFS normally will result in an error message and the NameNode(s) failing to start. <p/> The metadata upgrade must eventually be finalized, using the hdfsFinalizeMetadataUpgrade command on the NameNode. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The HDFS service name.

try:
    # Upgrade HDFS Metadata as part of a major version upgrade.
    api_response = api_instance.hdfs_upgrade_metadata_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hdfs_upgrade_metadata_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The HDFS service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hive_create_metastore_database_command" id="hive_create_metastore_database_command"></a>
# **hive_create_metastore_database_command**
> ApiCommand hive_create_metastore_database_command(cluster_name, service_name)

Create the Hive Metastore Database.

Create the Hive Metastore Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database needs to be created in the embedded postgresql database for a Hive service. This command should usually be followed by a call to hiveCreateMetastoreDatabaseTables. <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Hive service on which to run the command.

try:
    # Create the Hive Metastore Database.
    api_response = api_instance.hive_create_metastore_database_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hive_create_metastore_database_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Hive service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hive_create_metastore_database_tables_command" id="hive_create_metastore_database_tables_command"></a>
# **hive_create_metastore_database_tables_command**
> ApiCommand hive_create_metastore_database_tables_command(cluster_name, service_name)

Create the Hive Metastore Database tables.

Create the Hive Metastore Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only Available when all Hive Metastore Servers are stopped. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Hive service on which to run the command.

try:
    # Create the Hive Metastore Database tables.
    api_response = api_instance.hive_create_metastore_database_tables_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hive_create_metastore_database_tables_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Hive service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hive_update_metastore_namenodes_command" id="hive_update_metastore_namenodes_command"></a>
# **hive_update_metastore_namenodes_command**
> ApiCommand hive_update_metastore_namenodes_command(cluster_name, service_name)

Update Hive Metastore to point to a NameNode's Nameservice name instead of hostname.

Update Hive Metastore to point to a NameNode's Nameservice name instead of hostname. <p> <strong>Back up the Hive Metastore Database before running this command.</strong> <p> This command is to be run after enabling HDFS High Availability. Only available when all Hive Metastore Servers are stopped. <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Hive service on which to run the command.

try:
    # Update Hive Metastore to point to a NameNode's Nameservice name instead of hostname.
    api_response = api_instance.hive_update_metastore_namenodes_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hive_update_metastore_namenodes_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Hive service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hive_upgrade_metastore_command" id="hive_upgrade_metastore_command"></a>
# **hive_upgrade_metastore_command**
> ApiCommand hive_upgrade_metastore_command(cluster_name, service_name)

Upgrade Hive Metastore as part of a major version upgrade.

Upgrade Hive Metastore as part of a major version upgrade. <p/> When doing a major version upgrade for Hive, it is necessary to upgrade data in the metastore database. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Hive service name.

try:
    # Upgrade Hive Metastore as part of a major version upgrade.
    api_response = api_instance.hive_upgrade_metastore_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hive_upgrade_metastore_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Hive service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hive_validate_metastore_schema_command" id="hive_validate_metastore_schema_command"></a>
# **hive_validate_metastore_schema_command**
> ApiCommand hive_validate_metastore_schema_command(cluster_name, service_name)

Validate the Hive Metastore Schema.

Validate the Hive Metastore Schema. <p> This command checks the Hive metastore schema for any errors and corruptions. This command is to be run on two instances: <li>After the Hive Metastore database tables are created.</li> <li>Both before and after upgrading the Hive metastore database schema./li> * <p> Available since API v17.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Hive service on which to run the command.

try:
    # Validate the Hive Metastore Schema.
    api_response = api_instance.hive_validate_metastore_schema_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hive_validate_metastore_schema_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Hive service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hue_dump_db_command" id="hue_dump_db_command"></a>
# **hue_dump_db_command**
> ApiCommand hue_dump_db_command(cluster_name, service_name)

Runs Hue's dumpdata command.

Runs Hue's dumpdata command.  Available since API v10.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service

try:
    # Runs Hue's dumpdata command.
    api_response = api_instance.hue_dump_db_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hue_dump_db_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hue_load_db_command" id="hue_load_db_command"></a>
# **hue_load_db_command**
> ApiCommand hue_load_db_command(cluster_name, service_name)

Runs Hue's loaddata command.

Runs Hue's loaddata command.  Available since API v10.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service

try:
    # Runs Hue's loaddata command.
    api_response = api_instance.hue_load_db_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hue_load_db_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hue_sync_db_command" id="hue_sync_db_command"></a>
# **hue_sync_db_command**
> ApiCommand hue_sync_db_command(cluster_name, service_name)

Runs Hue's syncdb command.

Runs Hue's syncdb command.  Available since API v10.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service

try:
    # Runs Hue's syncdb command.
    api_response = api_instance.hue_sync_db_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->hue_sync_db_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="impala_create_catalog_database_command" id="impala_create_catalog_database_command"></a>
# **impala_create_catalog_database_command**
> ApiCommand impala_create_catalog_database_command(cluster_name, service_name)

.

<strong>Not needed in CM 5.0.0 Release, since Impala Catalog Database is not yet available in CDH as of this release.</strong> Create the Impala Catalog Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database needs to be created in the embedded postgresql database for the Impala Catalog Server. This command should usually be followed by a call to impalaCreateCatalogDatabaseTables. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Impala service on which to run the command.

try:
    # .
    api_response = api_instance.impala_create_catalog_database_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->impala_create_catalog_database_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Impala service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="impala_create_catalog_database_tables_command" id="impala_create_catalog_database_tables_command"></a>
# **impala_create_catalog_database_tables_command**
> ApiCommand impala_create_catalog_database_tables_command(cluster_name, service_name)

.

<strong>Not needed in CM 5.0.0 Release, since Impala Catalog Database is not yet available in CDH as of this release.</strong> Create the Impala Catalog Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only available when all Impala Catalog Servers are stopped. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Impala service on which to run the command.

try:
    # .
    api_response = api_instance.impala_create_catalog_database_tables_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->impala_create_catalog_database_tables_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Impala service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="import_mr_configs_into_yarn" id="import_mr_configs_into_yarn"></a>
# **import_mr_configs_into_yarn**
> ApiCommand import_mr_configs_into_yarn(cluster_name, service_name)

Import MapReduce configuration into Yarn, overwriting Yarn configuration.

Import MapReduce configuration into Yarn, overwriting Yarn configuration. <p> You will lose existing Yarn configuration. Read all MapReduce configuration, role assignments, and role configuration groups and update Yarn with corresponding values. MR1 configuration will be converted into the equivalent MR2 configuration. <p> Before running this command, Yarn must be stopped and MapReduce must exist with valid configuration. <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Yarn service on which to run the command.

try:
    # Import MapReduce configuration into Yarn, overwriting Yarn configuration.
    api_response = api_instance.import_mr_configs_into_yarn(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->import_mr_configs_into_yarn: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Yarn service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="init_solr_command" id="init_solr_command"></a>
# **init_solr_command**
> ApiCommand init_solr_command(cluster_name, service_name)

Initializes the Solr service in Zookeeper.

Initializes the Solr service in Zookeeper.  <p> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Solr service name.

try:
    # Initializes the Solr service in Zookeeper.
    api_response = api_instance.init_solr_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->init_solr_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="install_mr_framework_jars" id="install_mr_framework_jars"></a>
# **install_mr_framework_jars**
> ApiCommand install_mr_framework_jars(cluster_name, service_name)

Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it.

Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it. <p> This command is run automatically when starting a YARN service for the first time, or when upgrading an existing YARN service. It can also be run manually to ensure that the latest version of the framework JARS is installed. <p> Available since API v30. <p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the YARN service on which to run the command.

try:
    # Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it.
    api_response = api_instance.install_mr_framework_jars(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->install_mr_framework_jars: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the YARN service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="install_oozie_share_lib" id="install_oozie_share_lib"></a>
# **install_oozie_share_lib**
> ApiCommand install_oozie_share_lib(cluster_name, service_name)

Creates directory for Oozie user in HDFS and installs the ShareLib in it.

Creates directory for Oozie user in HDFS and installs the ShareLib in it. <p/> This command should be re-run after a major version upgrade to refresh the ShareLib to the latest version. <p/> Available since API v3. <p/>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Oozie service on which to run the command.

try:
    # Creates directory for Oozie user in HDFS and installs the ShareLib in it.
    api_response = api_instance.install_oozie_share_lib(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->install_oozie_share_lib: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Oozie service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="ks_migrate_to_sentry" id="ks_migrate_to_sentry"></a>
# **ks_migrate_to_sentry**
> ApiCommand ks_migrate_to_sentry(cluster_name, service_name)

Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer.

Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer. <p> Note: <li> <ul>KeyStore Indexer service should be in Stopped state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> <p> Available since API v30.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the KeyStore Indexer service name.

try:
    # Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer.
    api_response = api_instance.ks_migrate_to_sentry(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->ks_migrate_to_sentry: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the KeyStore Indexer service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_active_commands" id="list_active_commands"></a>
# **list_active_commands**
> ApiCommandList list_active_commands(cluster_name, service_name, view=view)

List active service commands.

List active service commands.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to which the role belongs.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List active service commands.
    api_response = api_instance.list_active_commands(cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->list_active_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to which the role belongs. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_role_types" id="list_role_types"></a>
# **list_role_types**
> ApiRoleTypeList list_role_types(cluster_name, service_name)

List the supported role types for a service.

List the supported role types for a service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to modify.

try:
    # List the supported role types for a service.
    api_response = api_instance.list_role_types(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->list_role_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to modify. | 

### Return type

[**ApiRoleTypeList**](ApiRoleTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_service_commands" id="list_service_commands"></a>
# **list_service_commands**
> ApiCommandMetadataList list_service_commands(cluster_name, service_name)

Lists all the commands that can be executed by name on the provided service.

Lists all the commands that can be executed by name on the provided service.  <p> Available since API v6. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.

try:
    # Lists all the commands that can be executed by name on the provided service.
    api_response = api_instance.list_service_commands(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->list_service_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiCommandMetadataList**](ApiCommandMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="offline_command" id="offline_command"></a>
# **offline_command**
> ApiCommand offline_command(cluster_name, service_name, timeout=timeout, body=body)

Offline roles of a service.

Offline roles of a service. <p> Currently the offline operation is only supported by HDFS. <p> For HDFS, the offline operation will put DataNodes into <em>HDFS IN MAINTENANCE</em> state which prevents unnecessary re-replication which could occur if decommissioned. <p> The <em>timeout</em> parameter is used to specify a timeout for offline. For HDFS, when the timeout expires, the DataNode will automatically transition out of <em>HDFS IN MAINTENANCE</em> state, back to <em>HDFS IN SERVICE</em> state. <p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
timeout = 3.4 # float | Offline timeout in seconds. Offlined roles will automatically transition from offline state to normal state after timeout. Specify as null to get the default timeout (4 hours). (optional)
body = cm_client.ApiRoleNameList() # ApiRoleNameList | List of role names to offline. (optional)

try:
    # Offline roles of a service.
    api_response = api_instance.offline_command(cluster_name, service_name, timeout=timeout, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->offline_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **timeout** | **float**| Offline timeout in seconds. Offlined roles will automatically transition from offline state to normal state after timeout. Specify as null to get the default timeout (4 hours). | [optional] 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to offline. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="oozie_create_embedded_database_command" id="oozie_create_embedded_database_command"></a>
# **oozie_create_embedded_database_command**
> ApiCommand oozie_create_embedded_database_command(cluster_name, service_name)

Create the Oozie Server Database.

Create the Oozie Server Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database need to be created in the embedded postgresql database for an Oozie service. This command should usually be followed by a call to createOozieDb. <p> Available since API v10.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Oozie service on which to run the command.

try:
    # Create the Oozie Server Database.
    api_response = api_instance.oozie_create_embedded_database_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->oozie_create_embedded_database_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Oozie service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="oozie_dump_database_command" id="oozie_dump_database_command"></a>
# **oozie_dump_database_command**
> ApiCommand oozie_dump_database_command(cluster_name, service_name)

Dump the Oozie Server Database.

Dump the Oozie Server Database.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service

try:
    # Dump the Oozie Server Database.
    api_response = api_instance.oozie_dump_database_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->oozie_dump_database_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="oozie_load_database_command" id="oozie_load_database_command"></a>
# **oozie_load_database_command**
> ApiCommand oozie_load_database_command(cluster_name, service_name)

Load the Oozie Server Database from dump.

Load the Oozie Server Database from dump.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The name of the service

try:
    # Load the Oozie Server Database from dump.
    api_response = api_instance.oozie_load_database_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->oozie_load_database_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The name of the service | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="oozie_upgrade_db_command" id="oozie_upgrade_db_command"></a>
# **oozie_upgrade_db_command**
> ApiCommand oozie_upgrade_db_command(cluster_name, service_name)

Upgrade Oozie Database schema as part of a major version upgrade.

Upgrade Oozie Database schema as part of a major version upgrade. <p/> When doing a major version upgrade for Oozie, it is necessary to upgrade the schema of its database before Oozie can run successfully. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Oozie service name.

try:
    # Upgrade Oozie Database schema as part of a major version upgrade.
    api_response = api_instance.oozie_upgrade_db_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->oozie_upgrade_db_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Oozie service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_service" id="read_service"></a>
# **read_service**
> ApiService read_service(cluster_name, service_name, view=view)

Retrieves details information about a service.

Retrieves details information about a service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
view = 'full' # str | DataView to materialize. Defaults to 'full'. (optional) (default to full)

try:
    # Retrieves details information about a service.
    api_response = api_instance.read_service(cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->read_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **view** | **str**| DataView to materialize. Defaults to 'full'. | [optional] [default to full]

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_service_config" id="read_service_config"></a>
# **read_service_config**
> ApiServiceConfig read_service_config(cluster_name, service_name, view=view)

Retrieves the configuration of a specific service.

Retrieves the configuration of a specific service. <p> The \"summary\" view contains only the configured parameters, and configuration for role types that contain configured parameters. <p> The \"full\" view contains all available configuration parameters for the service and its role types. This mode performs validation on the configuration, which could take a few seconds on a large cluster (around 500 nodes or more).

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to query.
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Retrieves the configuration of a specific service.
    api_response = api_instance.read_service_config(cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->read_service_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to query. | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_services" id="read_services"></a>
# **read_services**
> ApiServiceList read_services(cluster_name, view=view)

Lists all services registered in the cluster.

Lists all services registered in the cluster.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
view = 'summary' # str |  (optional) (default to summary)

try:
    # Lists all services registered in the cluster.
    api_response = api_instance.read_services(cluster_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->read_services: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiServiceList**](ApiServiceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="recommission_command" id="recommission_command"></a>
# **recommission_command**
> ApiCommand recommission_command(cluster_name, service_name, body=body)

Recommission roles of a service.

Recommission roles of a service. <p> The list should contain names of slave roles to recommission. </p>  <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the service on which to run the command.
body = cm_client.ApiRoleNameList() # ApiRoleNameList | List of role names to recommision. (optional)

try:
    # Recommission roles of a service.
    api_response = api_instance.recommission_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->recommission_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the service on which to run the command. | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to recommision. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="recommission_with_start_command" id="recommission_with_start_command"></a>
# **recommission_with_start_command**
> ApiCommand recommission_with_start_command(cluster_name, service_name, body=body)

Start and recommission roles of a service.

Start and recommission roles of a service. <p> The list should contain names of slave roles to start and recommission. </p>  <p> Warning: Evolving. This method may change in the future and does not offer standard compatibility guarantees. Only support by HDFS. Do not use without guidance from Cloudera. </p>  <p> Available since API v15. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the service on which to run the command.
body = cm_client.ApiRoleNameList() # ApiRoleNameList | List of role names to recommision. (optional)

try:
    # Start and recommission roles of a service.
    api_response = api_instance.recommission_with_start_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->recommission_with_start_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the service on which to run the command. | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to recommision. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="restart_command" id="restart_command"></a>
# **restart_command**
> ApiCommand restart_command(cluster_name, service_name)

Restart the service.

Restart the service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to start.

try:
    # Restart the service.
    api_response = api_instance.restart_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->restart_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to start. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="rolling_restart" id="rolling_restart"></a>
# **rolling_restart**
> ApiCommand rolling_restart(cluster_name, service_name, body=body)

Command to run rolling restart of roles in a service.

Command to run rolling restart of roles in a service. The sequence is: <ol> <li>Restart all the non-slave roles <li>If slaves are present restart them in batches of size specified in RollingRestartCmdArgs <li>Perform any post-command needed after rolling restart </ol> <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRollingRestartArgs() # ApiRollingRestartArgs |  (optional)

try:
    # Command to run rolling restart of roles in a service.
    api_response = api_instance.rolling_restart(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->rolling_restart: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRollingRestartArgs**](ApiRollingRestartArgs.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="sentry_create_database_command" id="sentry_create_database_command"></a>
# **sentry_create_database_command**
> ApiCommand sentry_create_database_command(cluster_name, service_name)

Create the Sentry Server Database.

Create the Sentry Server Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database need to be created in the embedded postgresql database for a Sentry service. This command should usually be followed by a call to sentryCreateDatabaseTables. <p> Available since API v7.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Sentry service on which to run the command.

try:
    # Create the Sentry Server Database.
    api_response = api_instance.sentry_create_database_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->sentry_create_database_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Sentry service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="sentry_create_database_tables_command" id="sentry_create_database_tables_command"></a>
# **sentry_create_database_tables_command**
> ApiCommand sentry_create_database_tables_command(cluster_name, service_name)

Create the Sentry Server Database tables.

Create the Sentry Server Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only Available when Sentry Server is stopped. <p> Available since API v7.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Sentry service on which to run the command.

try:
    # Create the Sentry Server Database tables.
    api_response = api_instance.sentry_create_database_tables_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->sentry_create_database_tables_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Sentry service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="sentry_upgrade_database_tables_command" id="sentry_upgrade_database_tables_command"></a>
# **sentry_upgrade_database_tables_command**
> ApiCommand sentry_upgrade_database_tables_command(cluster_name, service_name)

Upgrade the Sentry Server Database tables.

Upgrade the Sentry Server Database tables. <p> This command is to be run whenever Sentry requires an upgrade to its database tables. <p> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Sentry service on which to run the command.

try:
    # Upgrade the Sentry Server Database tables.
    api_response = api_instance.sentry_upgrade_database_tables_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->sentry_upgrade_database_tables_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Sentry service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="service_command_by_name" id="service_command_by_name"></a>
# **service_command_by_name**
> ApiCommand service_command_by_name(cluster_name, command_name, service_name)

Executes a command on the service specified by name.

Executes a command on the service specified by name. <p> Available since API v6. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
command_name = 'command_name_example' # str | The command name.
service_name = 'service_name_example' # str | The service name.

try:
    # Executes a command on the service specified by name.
    api_response = api_instance.service_command_by_name(cluster_name, command_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->service_command_by_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **command_name** | **str**| The command name. | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="solr_bootstrap_collections_command" id="solr_bootstrap_collections_command"></a>
# **solr_bootstrap_collections_command**
> ApiCommand solr_bootstrap_collections_command(cluster_name, service_name)

Bootstraps Solr Collections after the CDH upgrade.

Bootstraps Solr Collections after the CDH upgrade. <p> Note: This is only needed for upgrading to CDH6.0. <p> Available since API v30.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Solr service name.

try:
    # Bootstraps Solr Collections after the CDH upgrade.
    api_response = api_instance.solr_bootstrap_collections_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->solr_bootstrap_collections_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="solr_bootstrap_config_command" id="solr_bootstrap_config_command"></a>
# **solr_bootstrap_config_command**
> ApiCommand solr_bootstrap_config_command(cluster_name, service_name)

Bootstraps Solr config during the CDH upgrade.

Bootstraps Solr config during the CDH upgrade. <p> Note: This is only needed for upgrading to CDH6.0. <p> Available since API v30.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Solr service name.

try:
    # Bootstraps Solr config during the CDH upgrade.
    api_response = api_instance.solr_bootstrap_config_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->solr_bootstrap_config_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="solr_config_backup_command" id="solr_config_backup_command"></a>
# **solr_config_backup_command**
> ApiCommand solr_config_backup_command(cluster_name, service_name)

Backs up Solr configuration metadata before CDH upgrade.

Backs up Solr configuration metadata before CDH upgrade. <p> Note: <li> <ul>Solr service should be in Stopped state.</ul> <ul>HDFS and Zookeeper services should in Running state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> </p> Available since API v30.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Solr service name.

try:
    # Backs up Solr configuration metadata before CDH upgrade.
    api_response = api_instance.solr_config_backup_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->solr_config_backup_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="solr_migrate_sentry_privileges_command" id="solr_migrate_sentry_privileges_command"></a>
# **solr_migrate_sentry_privileges_command**
> ApiCommand solr_migrate_sentry_privileges_command(cluster_name, service_name)

Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service.

Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service. <p> Note: <li> <ul>Solr service should be in Stopped state.</ul> <ul>HDFS, Zookeeper, and Sentry services should in Running state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> <p> Available since API v30.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Solr service name.

try:
    # Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service.
    api_response = api_instance.solr_migrate_sentry_privileges_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->solr_migrate_sentry_privileges_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="solr_reinitialize_state_for_upgrade_command" id="solr_reinitialize_state_for_upgrade_command"></a>
# **solr_reinitialize_state_for_upgrade_command**
> ApiCommand solr_reinitialize_state_for_upgrade_command(cluster_name, service_name)

Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state.

Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state. <p> Note: <li> <ul>Solr service should be in Stopped state.</ul> <ul>HDFS and Zookeeper services should in Running state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> <p> Available since API v30.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Solr service name.

try:
    # Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state.
    api_response = api_instance.solr_reinitialize_state_for_upgrade_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->solr_reinitialize_state_for_upgrade_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="solr_validate_metadata_command" id="solr_validate_metadata_command"></a>
# **solr_validate_metadata_command**
> ApiCommand solr_validate_metadata_command(cluster_name, service_name)

Validates Solr metadata and configurations.

Validates Solr metadata and configurations. <p> Note: This is only needed for upgrading to CDH6.0. <p> Available since API v30.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | A String representing the Solr service name.

try:
    # Validates Solr metadata and configurations.
    api_response = api_instance.solr_validate_metadata_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->solr_validate_metadata_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| A String representing the Solr service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="sqoop_create_database_tables_command" id="sqoop_create_database_tables_command"></a>
# **sqoop_create_database_tables_command**
> ApiCommand sqoop_create_database_tables_command(cluster_name, service_name)

Create the Sqoop2 Server Database tables.

Create the Sqoop2 Server Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only available when Sqoop2 Server is stopped. <p> Available since API v10.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Sentry service on which to run the command.

try:
    # Create the Sqoop2 Server Database tables.
    api_response = api_instance.sqoop_create_database_tables_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->sqoop_create_database_tables_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Sentry service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="sqoop_upgrade_db_command" id="sqoop_upgrade_db_command"></a>
# **sqoop_upgrade_db_command**
> ApiCommand sqoop_upgrade_db_command(cluster_name, service_name)

Upgrade Sqoop Database schema as part of a major version upgrade.

Upgrade Sqoop Database schema as part of a major version upgrade. <p/> When doing a major version upgrade for Sqoop, it is necessary to upgrade the schema of its database before Sqoop can run successfully. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The Sqoop service name.

try:
    # Upgrade Sqoop Database schema as part of a major version upgrade.
    api_response = api_instance.sqoop_upgrade_db_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->sqoop_upgrade_db_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The Sqoop service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_command" id="start_command"></a>
# **start_command**
> ApiCommand start_command(cluster_name, service_name)

Start the service.

Start the service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to start.

try:
    # Start the service.
    api_response = api_instance.start_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->start_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to start. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stop_command" id="stop_command"></a>
# **stop_command**
> ApiCommand stop_command(cluster_name, service_name)

Stop the service.

Stop the service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to stop.

try:
    # Stop the service.
    api_response = api_instance.stop_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->stop_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to stop. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="switch_to_mr2" id="switch_to_mr2"></a>
# **switch_to_mr2**
> ApiCommand switch_to_mr2(cluster_name, service_name)

Change the cluster to use MR2 instead of MR1.

Change the cluster to use MR2 instead of MR1. Services will be restarted. <p> Will perform the following steps: <ul> <li>Update all services that depend on MapReduce to instead depend on Yarn. </li> <li>Stop MapReduce</li> <li>Start Yarn (MR2 Included)</li> <li>Deploy Yarn (MR2) Client Configuration</li> </ul> <p> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | Name of the Yarn service on which to run the command.

try:
    # Change the cluster to use MR2 instead of MR1.
    api_response = api_instance.switch_to_mr2(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->switch_to_mr2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| Name of the Yarn service on which to run the command. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_service" id="update_service"></a>
# **update_service**
> ApiService update_service(cluster_name, service_name, body=body)

Updates service information.

Updates service information. <p/> This method will update only writable fields of the service information. Currently this only includes the service display name. <p/> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
body = cm_client.ApiService() # ApiService | Updated service information. (optional)

try:
    # Updates service information.
    api_response = api_instance.update_service(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->update_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **body** | [**ApiService**](ApiService.md)| Updated service information. | [optional] 

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_service_config" id="update_service_config"></a>
# **update_service_config**
> ApiServiceConfig update_service_config(cluster_name, service_name, message=message, body=body)

Updates the service configuration with the given values.

Updates the service configuration with the given values. <p> If a value is set in the given configuration, it will be added to the service's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to modify.
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiServiceConfig() # ApiServiceConfig | Configuration changes. (optional)

try:
    # Updates the service configuration with the given values.
    api_response = api_instance.update_service_config(cluster_name, service_name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->update_service_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to modify. | 
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiServiceConfig**](ApiServiceConfig.md)| Configuration changes. | [optional] 

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="yarn_format_state_store" id="yarn_format_state_store"></a>
# **yarn_format_state_store**
> ApiCommand yarn_format_state_store(cluster_name, service_name)

Formats the state store in ZooKeeper used for Resource Manager High Availability.

Formats the state store in ZooKeeper used for Resource Manager High Availability. Typically used while moving from non-secure to secure cluster or vice-versa. <p> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The YARN service name.

try:
    # Formats the state store in ZooKeeper used for Resource Manager High Availability.
    api_response = api_instance.yarn_format_state_store(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->yarn_format_state_store: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The YARN service name. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="zoo_keeper_cleanup_command" id="zoo_keeper_cleanup_command"></a>
# **zoo_keeper_cleanup_command**
> ApiCommand zoo_keeper_cleanup_command(cluster_name, service_name)

Clean up all running server instances of a ZooKeeper service.

Clean up all running server instances of a ZooKeeper service. <p> This command removes snapshots and transaction log files kept by ZooKeeper for backup purposes. Refer to the ZooKeeper documentation for more details.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to start.

try:
    # Clean up all running server instances of a ZooKeeper service.
    api_response = api_instance.zoo_keeper_cleanup_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->zoo_keeper_cleanup_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to start. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="zoo_keeper_init_command" id="zoo_keeper_init_command"></a>
# **zoo_keeper_init_command**
> ApiCommand zoo_keeper_init_command(cluster_name, service_name)

Initializes all the server instances of a ZooKeeper service.

Initializes all the server instances of a ZooKeeper service. <p> ZooKeeper server roles need to be initialized before they can be used.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ServicesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service to start.

try:
    # Initializes all the server instances of a ZooKeeper service.
    api_response = api_instance.zoo_keeper_init_command(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServicesResourceApi->zoo_keeper_init_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service to start. | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

