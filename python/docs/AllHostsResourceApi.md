# cm_client.AllHostsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**read_config**](AllHostsResourceApi.md#read_config) | **GET** /cm/allHosts/config | Retrieve the default configuration for all hosts.
[**update_config**](AllHostsResourceApi.md#update_config) | **PUT** /cm/allHosts/config | Update the default configuration values for all hosts.


<a name="read_config" id="read_config"></a>
# **read_config**
> ApiConfigList read_config(view=view)

Retrieve the default configuration for all hosts.

Retrieve the default configuration for all hosts. <p/> These values will apply to all hosts managed by CM unless overridden at the host level.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AllHostsResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Retrieve the default configuration for all hosts.
    api_response = api_instance.read_config(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AllHostsResourceApi->read_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_config" id="update_config"></a>
# **update_config**
> ApiConfigList update_config(message=message, body=body)

Update the default configuration values for all hosts.

Update the default configuration values for all hosts. <p/> Note that this does not override values set at the host level. It just updates the default values that will be inherited by each host's configuration.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AllHostsResourceApi(cm_client.ApiClient(configuration))
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | The config values to update. (optional)

try:
    # Update the default configuration values for all hosts.
    api_response = api_instance.update_config(message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AllHostsResourceApi->update_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| The config values to update. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

