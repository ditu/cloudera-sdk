# ApiParcelRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cluster_name** | **str** | The name of the cluster that the parcel is used by. | [optional] 
**parcel_name** | **str** | The name of the parcel. | [optional] 
**parcel_version** | **str** | The version of the parcel. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


