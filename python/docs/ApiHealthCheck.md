# ApiHealthCheck

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Unique name of this health check. | [optional] 
**summary** | [**ApiHealthSummary**](ApiHealthSummary.md) | The summary status of this check. | [optional] 
**explanation** | **str** | The explanation of this health check. Available since v11. | [optional] 
**suppressed** | **bool** | Whether this health test is suppressed. A suppressed health test is not considered when computing an entity&#39;s overall health. Available since v11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


