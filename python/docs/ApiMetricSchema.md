# ApiMetricSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the metric. This name is guaranteed to be unique among the metrics. | [optional] 
**display_name** | **str** | Display name of the metric. | [optional] 
**description** | **str** | Description of the metric. | [optional] 
**is_counter** | **bool** | Is the metric a counter. A counter tracks the total count since a process / host started. The rate of change of a counter may often be more interesting than the raw value of a counter. | [optional] 
**unit_numerator** | **str** | Numerator for the unit of the metric. | [optional] 
**unit_denominator** | **str** | Denominator for the unit of the metric. | [optional] 
**aliases** | **list[str]** | Aliases for the metric. An alias is unique per metric (per source and version) but is not globally unique. Aliases usually refer to previous names for the metric as metrics are renamed or replaced. | [optional] 
**sources** | [**list[dict(str, str)]**](dict.md) | Sources for the metric. Each source entry contains the name of the source and a list of versions for which this source is valid | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


