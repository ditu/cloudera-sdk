# cm_client.ParcelResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activate_command**](ParcelResourceApi.md#activate_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/activate | A synchronous command that activates the parcel on the cluster.
[**cancel_distribution_command**](ParcelResourceApi.md#cancel_distribution_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/cancelDistribution | A synchronous command that cancels the parcel distribution.
[**cancel_download_command**](ParcelResourceApi.md#cancel_download_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/cancelDownload | A synchronous command that cancels the parcel download.
[**deactivate_command**](ParcelResourceApi.md#deactivate_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/deactivate | A synchronous command that deactivates the parcel on the cluster.
[**read_parcel**](ParcelResourceApi.md#read_parcel) | **GET** /clusters/{clusterName}/parcels/products/{product}/versions/{version} | Retrieves detailed information about a parcel.
[**remove_download_command**](ParcelResourceApi.md#remove_download_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/removeDownload | A synchronous command that removes the downloaded parcel.
[**start_distribution_command**](ParcelResourceApi.md#start_distribution_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startDistribution | A synchronous command that starts the distribution of the parcel to the cluster.
[**start_download_command**](ParcelResourceApi.md#start_download_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startDownload | A synchronous command that starts the parcel download.
[**start_removal_of_distribution_command**](ParcelResourceApi.md#start_removal_of_distribution_command) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startRemovalOfDistribution | A synchronous command that removes the distribution from the hosts in the cluster.


<a name="activate_command" id="activate_command"></a>
# **activate_command**
> ApiCommand activate_command(cluster_name, product, version)

A synchronous command that activates the parcel on the cluster.

A synchronous command that activates the parcel on the cluster. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that activates the parcel on the cluster.
    api_response = api_instance.activate_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->activate_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="cancel_distribution_command" id="cancel_distribution_command"></a>
# **cancel_distribution_command**
> ApiCommand cancel_distribution_command(cluster_name, product, version)

A synchronous command that cancels the parcel distribution.

A synchronous command that cancels the parcel distribution. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that cancels the parcel distribution.
    api_response = api_instance.cancel_distribution_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->cancel_distribution_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="cancel_download_command" id="cancel_download_command"></a>
# **cancel_download_command**
> ApiCommand cancel_download_command(cluster_name, product, version)

A synchronous command that cancels the parcel download.

A synchronous command that cancels the parcel download. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that cancels the parcel download.
    api_response = api_instance.cancel_download_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->cancel_download_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deactivate_command" id="deactivate_command"></a>
# **deactivate_command**
> ApiCommand deactivate_command(cluster_name, product, version)

A synchronous command that deactivates the parcel on the cluster.

A synchronous command that deactivates the parcel on the cluster. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that deactivates the parcel on the cluster.
    api_response = api_instance.deactivate_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->deactivate_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_parcel" id="read_parcel"></a>
# **read_parcel**
> ApiParcel read_parcel(cluster_name, product, version)

Retrieves detailed information about a parcel.

Retrieves detailed information about a parcel.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # Retrieves detailed information about a parcel.
    api_response = api_instance.read_parcel(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->read_parcel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiParcel**](ApiParcel.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="remove_download_command" id="remove_download_command"></a>
# **remove_download_command**
> ApiCommand remove_download_command(cluster_name, product, version)

A synchronous command that removes the downloaded parcel.

A synchronous command that removes the downloaded parcel. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that removes the downloaded parcel.
    api_response = api_instance.remove_download_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->remove_download_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_distribution_command" id="start_distribution_command"></a>
# **start_distribution_command**
> ApiCommand start_distribution_command(cluster_name, product, version)

A synchronous command that starts the distribution of the parcel to the cluster.

A synchronous command that starts the distribution of the parcel to the cluster. <p> Since it is synchronous, the result is known immediately upon return. In order to see the progress of the distribution, a call to ParcelResource#readParcel() needs to be made.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that starts the distribution of the parcel to the cluster.
    api_response = api_instance.start_distribution_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->start_distribution_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_download_command" id="start_download_command"></a>
# **start_download_command**
> ApiCommand start_download_command(cluster_name, product, version)

A synchronous command that starts the parcel download.

A synchronous command that starts the parcel download. <p> Since it is synchronous, the result is known immediately upon return. In order to see the progress of the download, a call to ParcelResource#readParcel() needs to be made.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that starts the parcel download.
    api_response = api_instance.start_download_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->start_download_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_removal_of_distribution_command" id="start_removal_of_distribution_command"></a>
# **start_removal_of_distribution_command**
> ApiCommand start_removal_of_distribution_command(cluster_name, product, version)

A synchronous command that removes the distribution from the hosts in the cluster.

A synchronous command that removes the distribution from the hosts in the cluster. <p> Since it is synchronous, the result is known immediately upon return. In order to see the progress of the removal, a call to ParcelResource#readParcel() needs to be made.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ParcelResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
product = 'product_example' # str | the product
version = 'version_example' # str | the version

try:
    # A synchronous command that removes the distribution from the hosts in the cluster.
    api_response = api_instance.start_removal_of_distribution_command(cluster_name, product, version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParcelResourceApi->start_removal_of_distribution_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **product** | **str**| the product | 
 **version** | **str**| the version | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

