# cm_client.WatchedDirResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_watched_directory**](WatchedDirResourceApi.md#add_watched_directory) | **POST** /clusters/{clusterName}/services/{serviceName}/watcheddir | Adds a directory to the watching list.
[**list_watched_directories**](WatchedDirResourceApi.md#list_watched_directories) | **GET** /clusters/{clusterName}/services/{serviceName}/watcheddir | Lists all the watched directories.
[**remove_watched_directory**](WatchedDirResourceApi.md#remove_watched_directory) | **DELETE** /clusters/{clusterName}/services/{serviceName}/watcheddir/{directoryPath} | Removes a directory from the watching list.


<a name="add_watched_directory" id="add_watched_directory"></a>
# **add_watched_directory**
> ApiWatchedDir add_watched_directory(cluster_name, service_name, body=body)

Adds a directory to the watching list.

Adds a directory to the watching list. <p> Available since API v14. Only available with Cloudera Manager Enterprise Edition. <p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.WatchedDirResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.
body = cm_client.ApiWatchedDir() # ApiWatchedDir | The directory to be added. (optional)

try:
    # Adds a directory to the watching list.
    api_response = api_instance.add_watched_directory(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WatchedDirResourceApi->add_watched_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 
 **body** | [**ApiWatchedDir**](ApiWatchedDir.md)| The directory to be added. | [optional] 

### Return type

[**ApiWatchedDir**](ApiWatchedDir.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_watched_directories" id="list_watched_directories"></a>
# **list_watched_directories**
> ApiWatchedDirList list_watched_directories(cluster_name, service_name)

Lists all the watched directories.

Lists all the watched directories. <p> Available since API v14. Only available with Cloudera Manager Enterprise Edition. <p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.WatchedDirResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | The service name.

try:
    # Lists all the watched directories.
    api_response = api_instance.list_watched_directories(cluster_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WatchedDirResourceApi->list_watched_directories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiWatchedDirList**](ApiWatchedDirList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="remove_watched_directory" id="remove_watched_directory"></a>
# **remove_watched_directory**
> ApiWatchedDir remove_watched_directory(cluster_name, directory_path, service_name)

Removes a directory from the watching list.

Removes a directory from the watching list. <p> Available since API v14. Only available with Cloudera Manager Enterprise Edition. <p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.WatchedDirResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
directory_path = 'directory_path_example' # str | The directory path to be removed.
service_name = 'service_name_example' # str | The service name.

try:
    # Removes a directory from the watching list.
    api_response = api_instance.remove_watched_directory(cluster_name, directory_path, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WatchedDirResourceApi->remove_watched_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **directory_path** | **str**| The directory path to be removed. | 
 **service_name** | **str**| The service name. | 

### Return type

[**ApiWatchedDir**](ApiWatchedDir.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

