# ApiSnapshotCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | The command ID. | [optional] 
**name** | **str** | The command name. | [optional] 
**start_time** | **str** | The start time. | [optional] 
**end_time** | **str** | The end time, if the command is finished. | [optional] 
**active** | **bool** | Whether the command is currently active. | [optional] 
**success** | **bool** | If the command is finished, whether it was successful. | [optional] 
**result_message** | **str** | If the command is finished, the result message. | [optional] 
**result_data_url** | **str** | URL to the command&#39;s downloadable result data, if any exists. | [optional] 
**cluster_ref** | [**ApiClusterRef**](ApiClusterRef.md) | Reference to the cluster (for cluster commands only). | [optional] 
**service_ref** | [**ApiServiceRef**](ApiServiceRef.md) | Reference to the service (for service commands only). | [optional] 
**role_ref** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the role (for role commands only). | [optional] 
**host_ref** | [**ApiHostRef**](ApiHostRef.md) | Reference to the host (for host commands only). | [optional] 
**parent** | [**ApiCommand**](ApiCommand.md) | Reference to the parent command, if any. | [optional] 
**children** | [**ApiCommandList**](ApiCommandList.md) | List of child commands. Only available in the full view. &lt;p&gt; The list contains only the summary view of the children. | [optional] 
**can_retry** | **bool** | If the command can be retried. Available since V11 | [optional] 
**hbase_result** | [**ApiHBaseSnapshotResult**](ApiHBaseSnapshotResult.md) | Results for snapshot commands on HBase services. | [optional] 
**hdfs_result** | [**ApiHdfsSnapshotResult**](ApiHdfsSnapshotResult.md) | Results for snapshot commands on Hdfs services. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


