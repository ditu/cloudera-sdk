# cm_client.TimeSeriesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_entity_type_attributes**](TimeSeriesResourceApi.md#get_entity_type_attributes) | **GET** /timeseries/entityTypeAttributes | Retrieve all metric entity type attributes monitored by Cloudera Manager.
[**get_entity_types**](TimeSeriesResourceApi.md#get_entity_types) | **GET** /timeseries/entityTypes | Retrieve all metric entity types monitored by Cloudera Manager.
[**get_metric_schema**](TimeSeriesResourceApi.md#get_metric_schema) | **GET** /timeseries/schema | Retrieve schema for all metrics.
[**query_time_series**](TimeSeriesResourceApi.md#query_time_series) | **GET** /timeseries | Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.
[**query_time_series_0**](TimeSeriesResourceApi.md#query_time_series_0) | **POST** /timeseries | Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.


<a name="get_entity_type_attributes" id="get_entity_type_attributes"></a>
# **get_entity_type_attributes**
> ApiTimeSeriesEntityAttributeList get_entity_type_attributes()

Retrieve all metric entity type attributes monitored by Cloudera Manager.

Retrieve all metric entity type attributes monitored by Cloudera Manager. <p/> Available since API v11.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.TimeSeriesResourceApi(cm_client.ApiClient(configuration))

try:
    # Retrieve all metric entity type attributes monitored by Cloudera Manager.
    api_response = api_instance.get_entity_type_attributes()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TimeSeriesResourceApi->get_entity_type_attributes: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiTimeSeriesEntityAttributeList**](ApiTimeSeriesEntityAttributeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_entity_types" id="get_entity_types"></a>
# **get_entity_types**
> ApiTimeSeriesEntityTypeList get_entity_types()

Retrieve all metric entity types monitored by Cloudera Manager.

Retrieve all metric entity types monitored by Cloudera Manager. It is guaranteed that parent types appear before their children. <p/> Available since API v11.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.TimeSeriesResourceApi(cm_client.ApiClient(configuration))

try:
    # Retrieve all metric entity types monitored by Cloudera Manager.
    api_response = api_instance.get_entity_types()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TimeSeriesResourceApi->get_entity_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiTimeSeriesEntityTypeList**](ApiTimeSeriesEntityTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_metric_schema" id="get_metric_schema"></a>
# **get_metric_schema**
> ApiMetricSchemaList get_metric_schema()

Retrieve schema for all metrics.

Retrieve schema for all metrics <p/> The schema is fixed for a product version. The schema may change for an API versions <p/> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.TimeSeriesResourceApi(cm_client.ApiClient(configuration))

try:
    # Retrieve schema for all metrics.
    api_response = api_instance.get_metric_schema()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TimeSeriesResourceApi->get_metric_schema: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiMetricSchemaList**](ApiMetricSchemaList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="query_time_series" id="query_time_series"></a>
# **query_time_series**
> ApiTimeSeriesResponseList query_time_series(content_type=content_type, desired_rollup=desired_rollup, _from=_from, must_use_desired_rollup=must_use_desired_rollup, query=query, to=to)

Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.

Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.  Please see the <a href=\"http://tiny.cloudera.com/cm_tsquery\"> tsquery language documentation</a>. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.TimeSeriesResourceApi(cm_client.ApiClient(configuration))
content_type = 'application/json' # str | to return the response in. The content types \"application/json\" and \"text/csv\" are supported. This defaults to \"application/json\". If \"text/csv\" is specified then we return one row per time series data point, and we don't return any of the metadata. (optional) (default to application/json)
desired_rollup = 'RAW' # str | Aggregate rollup level desired for the response data. Valid values are RAW, TEN_MINUTELY, HOURLY, SIX_HOURLY, DAILY, and WEEKLY. Note that if the mustUseDesiredRollup parameter is not set, then the monitoring server can decide to return a different rollup level. (optional) (default to RAW)
_from = '_from_example' # str | Start of the period to query in ISO 8601 format (defaults to 5 minutes before the end of the period). (optional)
must_use_desired_rollup = false # bool | If set then the tsquery will return data with the desired aggregate rollup level. (optional) (default to false)
query = 'query_example' # str | Tsquery to run against the CM time-series data store. (optional)
to = 'now' # str | End of the period to query in ISO 8601 format (defaults to current time). (optional) (default to now)

try:
    # Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.
    api_response = api_instance.query_time_series(content_type=content_type, desired_rollup=desired_rollup, _from=_from, must_use_desired_rollup=must_use_desired_rollup, query=query, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TimeSeriesResourceApi->query_time_series: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_type** | **str**| to return the response in. The content types \"application/json\" and \"text/csv\" are supported. This defaults to \"application/json\". If \"text/csv\" is specified then we return one row per time series data point, and we don't return any of the metadata. | [optional] [default to application/json]
 **desired_rollup** | **str**| Aggregate rollup level desired for the response data. Valid values are RAW, TEN_MINUTELY, HOURLY, SIX_HOURLY, DAILY, and WEEKLY. Note that if the mustUseDesiredRollup parameter is not set, then the monitoring server can decide to return a different rollup level. | [optional] [default to RAW]
 **_from** | **str**| Start of the period to query in ISO 8601 format (defaults to 5 minutes before the end of the period). | [optional] 
 **must_use_desired_rollup** | **bool**| If set then the tsquery will return data with the desired aggregate rollup level. | [optional] [default to false]
 **query** | **str**| Tsquery to run against the CM time-series data store. | [optional] 
 **to** | **str**| End of the period to query in ISO 8601 format (defaults to current time). | [optional] [default to now]

### Return type

[**ApiTimeSeriesResponseList**](ApiTimeSeriesResponseList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="query_time_series_0" id="query_time_series_0"></a>
# **query_time_series_0**
> ApiTimeSeriesResponseList query_time_series_0(body=body)

Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.

Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request. This method differs from queryTimeSeries() in v6 that this could accept query strings that are longer than HTTP GET request limit.  Available since API v11.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.TimeSeriesResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiTimeSeriesRequest() # ApiTimeSeriesRequest | Request object containing information used when retrieving timeseries data. (optional)

try:
    # Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.
    api_response = api_instance.query_time_series_0(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TimeSeriesResourceApi->query_time_series_0: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiTimeSeriesRequest**](ApiTimeSeriesRequest.md)| Request object containing information used when retrieving timeseries data. | [optional] 

### Return type

[**ApiTimeSeriesResponseList**](ApiTimeSeriesResponseList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

