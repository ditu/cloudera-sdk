# ApiParcelUsageParcel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parcel_ref** | [**ApiParcelRef**](ApiParcelRef.md) | Reference to the corresponding Parcel object. | [optional] 
**process_count** | **float** | How many running processes on the cluster are using the parcel. | [optional] 
**activated** | **bool** | Is this parcel currently activated on the cluster. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


