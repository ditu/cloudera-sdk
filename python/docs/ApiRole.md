# ApiRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the role. Optional when creating a role since API v6. If not specified, a name will be automatically generated for the role. | [optional] 
**type** | **str** | The type of the role, e.g. NAMENODE, DATANODE, TASKTRACKER. | [optional] 
**host_ref** | [**ApiHostRef**](ApiHostRef.md) | A reference to the host where this role runs. | [optional] 
**service_ref** | [**ApiServiceRef**](ApiServiceRef.md) | Readonly. A reference to the parent service. | [optional] 
**role_state** | [**ApiRoleState**](ApiRoleState.md) | Readonly. The configured run state of this role. Whether it&#39;s running, etc. | [optional] 
**commission_state** | [**ApiCommissionState**](ApiCommissionState.md) | Readonly. The commission state of this role. Available since API v2. | [optional] 
**health_summary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Readonly. The high-level health status of this role. | [optional] 
**config_stale** | **bool** | Readonly. Expresses whether the role configuration is stale. | [optional] 
**config_staleness_status** | [**ApiConfigStalenessStatus**](ApiConfigStalenessStatus.md) | Readonly. Expresses the role&#39;s configuration staleness status. Available since API v6. | [optional] 
**health_checks** | [**list[ApiHealthCheck]**](ApiHealthCheck.md) | Readonly. The list of health checks of this service. | [optional] 
**ha_status** | [**HaStatus**](HaStatus.md) | Readonly. The HA status of this role. | [optional] 
**role_url** | **str** | Readonly. Link into the Cloudera Manager web UI for this specific role. | [optional] 
**maintenance_mode** | **bool** | Readonly. Whether the role is in maintenance mode. Available since API v2. | [optional] 
**maintenance_owners** | [**list[ApiEntityType]**](ApiEntityType.md) | Readonly. The list of objects that trigger this role to be in maintenance mode. Available since API v2. | [optional] 
**config** | [**ApiConfigList**](ApiConfigList.md) | The role configuration. Optional. | [optional] 
**role_config_group_ref** | [**ApiRoleConfigGroupRef**](ApiRoleConfigGroupRef.md) | Readonly. The reference to the role configuration group of this role. Available since API v3. | [optional] 
**zoo_keeper_server_mode** | [**ZooKeeperServerMode**](ZooKeeperServerMode.md) | Readonly. The ZooKeeper server mode for this role. Note that for non-ZooKeeper Server roles this will be null. Available since API v6. | [optional] 
**entity_status** | [**ApiEntityStatus**](ApiEntityStatus.md) | Readonly. The entity status for this role. Available since API v11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


