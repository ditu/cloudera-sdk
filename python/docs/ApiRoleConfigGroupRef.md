# ApiRoleConfigGroupRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role_config_group_name** | **str** | The name of the role config group, which uniquely identifies it in a CM installation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


