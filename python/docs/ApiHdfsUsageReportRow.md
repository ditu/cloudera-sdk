# ApiHdfsUsageReportRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **str** | The date of the report row data. | [optional] 
**user** | **str** | The user being reported. | [optional] 
**size** | **float** | Total size (in bytes) of the files owned by this user. This does not include replication in HDFS. | [optional] 
**raw_size** | **float** | Total size (in bytes) of all the replicas of all the files owned by this user. | [optional] 
**num_files** | **float** | Number of files owned by this user. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


