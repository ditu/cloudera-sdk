# ApiHdfsSnapshotError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **str** | Path for which the snapshot error occurred. | [optional] 
**snapshot_name** | **str** | Name of snapshot for which error occurred. | [optional] 
**error** | **str** | Description of the error. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


