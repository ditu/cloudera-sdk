# ApiMr2AppInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_state** | **str** | The state of the job. This is only set on completed jobs. This can take on the following values: \&quot;NEW\&quot;, \&quot;INITED\&quot;, \&quot;RUNNING\&quot;, \&quot;SUCCEEDED\&quot;, \&quot;FAILED\&quot;, \&quot;KILLED\&quot;, \&quot;ERROR\&quot;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


