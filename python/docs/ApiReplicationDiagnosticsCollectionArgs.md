# ApiReplicationDiagnosticsCollectionArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commands** | [**ApiCommandList**](ApiCommandList.md) | Commands to limit diagnostics to. By default, the most recent 10 commands on the schedule will be used. | [optional] 
**ticket_number** | **str** | Ticket number to which this bundle must be associated with. | [optional] 
**comments** | **str** | Additional comments for the bundle. | [optional] 
**phone_home** | **bool** | Whether the diagnostics bundle must be uploaded to Cloudera. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


