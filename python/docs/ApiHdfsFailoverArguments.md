# ApiHdfsFailoverArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nameservice** | **str** | Nameservice for which to enable automatic failover. | [optional] 
**zoo_keeper_service** | [**ApiServiceRef**](ApiServiceRef.md) | The ZooKeeper service to use. | [optional] 
**active_fc_name** | **str** | Name of the failover controller to create for the active NameNode. | [optional] 
**stand_by_fc_name** | **str** | Name of the failover controller to create for the stand-by NameNode. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


