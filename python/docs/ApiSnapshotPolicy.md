# ApiSnapshotPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the snapshot policy. | [optional] 
**description** | **str** | Description of the snapshot policy. | [optional] 
**hourly_snapshots** | **float** | Number of hourly snapshots to be retained. | [optional] 
**daily_snapshots** | **float** | Number of daily snapshots to be retained. | [optional] 
**weekly_snapshots** | **float** | Number of weekly snapshots to be retained. | [optional] 
**monthly_snapshots** | **float** | Number of monthly snapshots to be retained. | [optional] 
**yearly_snapshots** | **float** | Number of yearly snapshots to be retained. | [optional] 
**minute_of_hour** | **float** | Minute in the hour that hourly, daily, weekly, monthly and yearly snapshots should be created. Valid values are 0 to 59. Default value is 0. | [optional] 
**hours_for_hourly_snapshots** | **list[float]** | Hours of the day that hourly snapshots should be created. Valid values are 0 to 23. If this list is null or empty, then hourly snapshots are created for every hour. | [optional] 
**hour_of_day** | **float** | Hour in the day that daily, weekly, monthly and yearly snapshots should be created. Valid values are 0 to 23. Default value is 0. | [optional] 
**day_of_week** | **float** | Day of the week that weekly snapshots should be created. Valid values are 1 to 7, 1 representing Sunday. Default value is 1. | [optional] 
**day_of_month** | **float** | Day of the month that monthly and yearly snapshots should be created. Values from 1 to 31 are allowed. Additionally 0 to -30 can be used to specify offsets from the last day of the month. Default value is 1. &lt;p/&gt; If this value is invalid for any month for which snapshots are required, the backend will throw an exception. | [optional] 
**month_of_year** | **float** | Month of the year that yearly snapshots should be created. Valid values are 1 to 12, 1 representing January. Default value is 1. | [optional] 
**alert_on_start** | **bool** | Whether to alert on start of snapshot creation/deletion activity. | [optional] 
**alert_on_success** | **bool** | Whether to alert on successful completion of snapshot creation/deletion activity. | [optional] 
**alert_on_fail** | **bool** | Whether to alert on failure of snapshot creation/deletion activity. | [optional] 
**alert_on_abort** | **bool** | Whether to alert on abort of snapshot creation/deletion activity. | [optional] 
**hbase_arguments** | [**ApiHBaseSnapshotPolicyArguments**](ApiHBaseSnapshotPolicyArguments.md) | Arguments specific to HBase snapshot policies. | [optional] 
**hdfs_arguments** | [**ApiHdfsSnapshotPolicyArguments**](ApiHdfsSnapshotPolicyArguments.md) | Arguments specific to Hdfs snapshot policies. | [optional] 
**last_command** | [**ApiSnapshotCommand**](ApiSnapshotCommand.md) | Latest command of this policy. The command might still be active. | [optional] 
**last_successful_command** | [**ApiSnapshotCommand**](ApiSnapshotCommand.md) | Last successful command of this policy. Returns null if there has been no successful command. | [optional] 
**paused** | **bool** | Whether to pause a snapshot policy, available since V11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


