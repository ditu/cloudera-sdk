# ApiHBaseSnapshotPolicyArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**table_reg_exps** | **list[str]** | The regular expressions specifying the tables. Tables matching any of them will be eligible for snapshot creation. | [optional] 
**storage** | [**Storage**](Storage.md) | The location where the snapshots should be stored. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


