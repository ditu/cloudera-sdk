# ApiHostsPerfInspectorArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_host_list** | [**ApiHostNameList**](ApiHostNameList.md) | Required list of host names which&#39;ll act as source for running network diagnostics test. | [optional] 
**target_host_list** | [**ApiHostNameList**](ApiHostNameList.md) | Required list of host names which&#39;ll act as target for running network diagnostics test. | [optional] 
**ping_args** | [**ApiPerfInspectorPingArgs**](ApiPerfInspectorPingArgs.md) | Optional ping request arguments. If not specified, default arguments will be used for ping test. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


