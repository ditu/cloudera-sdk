# cm_client.HostTemplatesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apply_host_template**](HostTemplatesResourceApi.md#apply_host_template) | **POST** /clusters/{clusterName}/hostTemplates/{hostTemplateName}/commands/applyHostTemplate | Applies a host template to a collection of hosts.
[**create_host_templates**](HostTemplatesResourceApi.md#create_host_templates) | **POST** /clusters/{clusterName}/hostTemplates | Creates new host templates.
[**delete_host_template**](HostTemplatesResourceApi.md#delete_host_template) | **DELETE** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Deletes a host template.
[**read_host_template**](HostTemplatesResourceApi.md#read_host_template) | **GET** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Retrieves information about a host template.
[**read_host_templates**](HostTemplatesResourceApi.md#read_host_templates) | **GET** /clusters/{clusterName}/hostTemplates | Lists all host templates in a cluster.
[**update_host_template**](HostTemplatesResourceApi.md#update_host_template) | **PUT** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Updates an existing host template.


<a name="apply_host_template" id="apply_host_template"></a>
# **apply_host_template**
> ApiCommand apply_host_template(cluster_name, host_template_name, start_roles=start_roles, body=body)

Applies a host template to a collection of hosts.

Applies a host template to a collection of hosts. This will create a role for each role config group on each of the hosts. <p> The provided hosts must not have any existing roles on them and if the cluster is not using parcels, the hosts must have a CDH version matching that of the cluster version. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostTemplatesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
host_template_name = 'host_template_name_example' # str | Host template to apply.
start_roles = true # bool | Whether to start the newly created roles or not. (optional)
body = cm_client.ApiHostRefList() # ApiHostRefList | List of hosts to apply the host template to. (optional)

try:
    # Applies a host template to a collection of hosts.
    api_response = api_instance.apply_host_template(cluster_name, host_template_name, start_roles=start_roles, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostTemplatesResourceApi->apply_host_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **host_template_name** | **str**| Host template to apply. | 
 **start_roles** | **bool**| Whether to start the newly created roles or not. | [optional] 
 **body** | [**ApiHostRefList**](ApiHostRefList.md)| List of hosts to apply the host template to. | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_host_templates" id="create_host_templates"></a>
# **create_host_templates**
> ApiHostTemplateList create_host_templates(cluster_name, body=body)

Creates new host templates.

Creates new host templates. <p> Host template names must be unique across clusters. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostTemplatesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
body = cm_client.ApiHostTemplateList() # ApiHostTemplateList | The list of host templates to create. (optional)

try:
    # Creates new host templates.
    api_response = api_instance.create_host_templates(cluster_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostTemplatesResourceApi->create_host_templates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **body** | [**ApiHostTemplateList**](ApiHostTemplateList.md)| The list of host templates to create. | [optional] 

### Return type

[**ApiHostTemplateList**](ApiHostTemplateList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_host_template" id="delete_host_template"></a>
# **delete_host_template**
> ApiHostTemplate delete_host_template(cluster_name, host_template_name)

Deletes a host template.

Deletes a host template. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostTemplatesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
host_template_name = 'host_template_name_example' # str | Host template to delete.

try:
    # Deletes a host template.
    api_response = api_instance.delete_host_template(cluster_name, host_template_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostTemplatesResourceApi->delete_host_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **host_template_name** | **str**| Host template to delete. | 

### Return type

[**ApiHostTemplate**](ApiHostTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_host_template" id="read_host_template"></a>
# **read_host_template**
> ApiHostTemplate read_host_template(cluster_name, host_template_name)

Retrieves information about a host template.

Retrieves information about a host template. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostTemplatesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
host_template_name = 'host_template_name_example' # str | 

try:
    # Retrieves information about a host template.
    api_response = api_instance.read_host_template(cluster_name, host_template_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostTemplatesResourceApi->read_host_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **host_template_name** | **str**|  | 

### Return type

[**ApiHostTemplate**](ApiHostTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_host_templates" id="read_host_templates"></a>
# **read_host_templates**
> ApiHostTemplateList read_host_templates(cluster_name)

Lists all host templates in a cluster.

Lists all host templates in a cluster. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostTemplatesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 

try:
    # Lists all host templates in a cluster.
    api_response = api_instance.read_host_templates(cluster_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostTemplatesResourceApi->read_host_templates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 

### Return type

[**ApiHostTemplateList**](ApiHostTemplateList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_host_template" id="update_host_template"></a>
# **update_host_template**
> ApiHostTemplate update_host_template(cluster_name, host_template_name, body=body)

Updates an existing host template.

Updates an existing host template. <p> Can be used to update the role config groups in a host template or rename it. <p> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.HostTemplatesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
host_template_name = 'host_template_name_example' # str | Host template with updated fields.
body = cm_client.ApiHostTemplate() # ApiHostTemplate |  (optional)

try:
    # Updates an existing host template.
    api_response = api_instance.update_host_template(cluster_name, host_template_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostTemplatesResourceApi->update_host_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **host_template_name** | **str**| Host template with updated fields. | 
 **body** | [**ApiHostTemplate**](ApiHostTemplate.md)|  | [optional] 

### Return type

[**ApiHostTemplate**](ApiHostTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

