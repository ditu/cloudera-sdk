# cm_client.MgmtRoleCommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jmap_dump**](MgmtRoleCommandsResourceApi.md#jmap_dump) | **POST** /cm/service/roleCommands/jmapDump | Run the jmapDump diagnostic command.
[**jmap_histo**](MgmtRoleCommandsResourceApi.md#jmap_histo) | **POST** /cm/service/roleCommands/jmapHisto | Run the jmapHisto diagnostic command.
[**jstack**](MgmtRoleCommandsResourceApi.md#jstack) | **POST** /cm/service/roleCommands/jstack | Run the jstack diagnostic command.
[**lsof**](MgmtRoleCommandsResourceApi.md#lsof) | **POST** /cm/service/roleCommands/lsof | Run the lsof diagnostic command.
[**restart_command**](MgmtRoleCommandsResourceApi.md#restart_command) | **POST** /cm/service/roleCommands/restart | Restart a set of Cloudera Management Services roles.
[**start_command**](MgmtRoleCommandsResourceApi.md#start_command) | **POST** /cm/service/roleCommands/start | Start a set of Cloudera Management Services roles.
[**stop_command**](MgmtRoleCommandsResourceApi.md#stop_command) | **POST** /cm/service/roleCommands/stop | Stop a set of Cloudera Management Services roles.


<a name="jmap_dump" id="jmap_dump"></a>
# **jmap_dump**
> ApiBulkCommandList jmap_dump(body=body)

Run the jmapDump diagnostic command.

Run the jmapDump diagnostic command. The command runs the jmap utility to capture a dump of the role's java heap. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to jmap. (optional)

try:
    # Run the jmapDump diagnostic command.
    api_response = api_instance.jmap_dump(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleCommandsResourceApi->jmap_dump: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="jmap_histo" id="jmap_histo"></a>
# **jmap_histo**
> ApiBulkCommandList jmap_histo(body=body)

Run the jmapHisto diagnostic command.

Run the jmapHisto diagnostic command. The command runs the jmap utility to capture a histogram of the objects on the role's java heap. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to jmap. (optional)

try:
    # Run the jmapHisto diagnostic command.
    api_response = api_instance.jmap_histo(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleCommandsResourceApi->jmap_histo: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="jstack" id="jstack"></a>
# **jstack**
> ApiBulkCommandList jstack(body=body)

Run the jstack diagnostic command.

Run the jstack diagnostic command. The command runs the jstack utility to capture a role's java thread stacks. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to jstack. (optional)

try:
    # Run the jstack diagnostic command.
    api_response = api_instance.jstack(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleCommandsResourceApi->jstack: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jstack. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="lsof" id="lsof"></a>
# **lsof**
> ApiBulkCommandList lsof(body=body)

Run the lsof diagnostic command.

Run the lsof diagnostic command. This command runs the lsof utility to list a role's open files. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to lsof. (optional)

try:
    # Run the lsof diagnostic command.
    api_response = api_instance.lsof(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleCommandsResourceApi->lsof: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to lsof. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="restart_command" id="restart_command"></a>
# **restart_command**
> ApiBulkCommandList restart_command(body=body)

Restart a set of Cloudera Management Services roles.

Restart a set of Cloudera Management Services roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The roles to restart. (optional)

try:
    # Restart a set of Cloudera Management Services roles.
    api_response = api_instance.restart_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleCommandsResourceApi->restart_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to restart. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_command" id="start_command"></a>
# **start_command**
> ApiBulkCommandList start_command(body=body)

Start a set of Cloudera Management Services roles.

Start a set of Cloudera Management Services roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The roles to start. (optional)

try:
    # Start a set of Cloudera Management Services roles.
    api_response = api_instance.start_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleCommandsResourceApi->start_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to start. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stop_command" id="stop_command"></a>
# **stop_command**
> ApiBulkCommandList stop_command(body=body)

Stop a set of Cloudera Management Services roles.

Stop a set of Cloudera Management Services roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.MgmtRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The roles to stop. (optional)

try:
    # Stop a set of Cloudera Management Services roles.
    api_response = api_instance.stop_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MgmtRoleCommandsResourceApi->stop_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to stop. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

