# ApiRollEditsArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nameservice** | **str** | Nameservice whose edits need to be rolled. Required only if HDFS service is federated. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


