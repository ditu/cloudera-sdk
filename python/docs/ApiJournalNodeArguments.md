# ApiJournalNodeArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jn_name** | **str** | Name of new JournalNode to be created. (Optional) | [optional] 
**jn_host_id** | **str** | ID of the host where the new JournalNode will be created. | [optional] 
**jn_edits_dir** | **str** | Path to the JournalNode edits directory. Need not be specified if it is already set at RoleConfigGroup level. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


