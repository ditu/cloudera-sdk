# cm_client.SnapshotsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_policies**](SnapshotsResourceApi.md#create_policies) | **POST** /clusters/{clusterName}/services/{serviceName}/snapshots/policies | Creates one or more snapshot policies.
[**delete_policy**](SnapshotsResourceApi.md#delete_policy) | **DELETE** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Deletes an existing snapshot policy.
[**read_history**](SnapshotsResourceApi.md#read_history) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName}/history | Returns a list of commands triggered by a snapshot policy.
[**read_policies**](SnapshotsResourceApi.md#read_policies) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies | Returns information for all snapshot policies.
[**read_policy**](SnapshotsResourceApi.md#read_policy) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Returns information for a specific snapshot policy.
[**update_policy**](SnapshotsResourceApi.md#update_policy) | **PUT** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Updates an existing snapshot policy.


<a name="create_policies" id="create_policies"></a>
# **create_policies**
> ApiSnapshotPolicyList create_policies(cluster_name, service_name, body=body)

Creates one or more snapshot policies.

Creates one or more snapshot policies.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.SnapshotsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiSnapshotPolicyList() # ApiSnapshotPolicyList | List of the snapshot policies to create. (optional)

try:
    # Creates one or more snapshot policies.
    api_response = api_instance.create_policies(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SnapshotsResourceApi->create_policies: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiSnapshotPolicyList**](ApiSnapshotPolicyList.md)| List of the snapshot policies to create. | [optional] 

### Return type

[**ApiSnapshotPolicyList**](ApiSnapshotPolicyList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_policy" id="delete_policy"></a>
# **delete_policy**
> ApiSnapshotPolicy delete_policy(cluster_name, policy_name, service_name)

Deletes an existing snapshot policy.

Deletes an existing snapshot policy.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.SnapshotsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
policy_name = 'policy_name_example' # str | Name of an existing snapshot policy.
service_name = 'service_name_example' # str | 

try:
    # Deletes an existing snapshot policy.
    api_response = api_instance.delete_policy(cluster_name, policy_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SnapshotsResourceApi->delete_policy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **policy_name** | **str**| Name of an existing snapshot policy. | 
 **service_name** | **str**|  | 

### Return type

[**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_history" id="read_history"></a>
# **read_history**
> ApiSnapshotCommandList read_history(cluster_name, policy_name, service_name, limit=limit, offset=offset, view=view)

Returns a list of commands triggered by a snapshot policy.

Returns a list of commands triggered by a snapshot policy.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.SnapshotsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
policy_name = 'policy_name_example' # str | Name of an existing snapshot policy.
service_name = 'service_name_example' # str | 
limit = 20 # int | Maximum number of commands to retrieve. (optional) (default to 20)
offset = 0 # int | Index of first command to retrieve. (optional) (default to 0)
view = 'summary' # str | The view to materialize. (optional) (default to summary)

try:
    # Returns a list of commands triggered by a snapshot policy.
    api_response = api_instance.read_history(cluster_name, policy_name, service_name, limit=limit, offset=offset, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SnapshotsResourceApi->read_history: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **policy_name** | **str**| Name of an existing snapshot policy. | 
 **service_name** | **str**|  | 
 **limit** | **int**| Maximum number of commands to retrieve. | [optional] [default to 20]
 **offset** | **int**| Index of first command to retrieve. | [optional] [default to 0]
 **view** | **str**| The view to materialize. | [optional] [default to summary]

### Return type

[**ApiSnapshotCommandList**](ApiSnapshotCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_policies" id="read_policies"></a>
# **read_policies**
> ApiSnapshotPolicyList read_policies(cluster_name, service_name, view=view)

Returns information for all snapshot policies.

Returns information for all snapshot policies.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.SnapshotsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
view = 'summary' # str | The view to materialize. (optional) (default to summary)

try:
    # Returns information for all snapshot policies.
    api_response = api_instance.read_policies(cluster_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SnapshotsResourceApi->read_policies: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **view** | **str**| The view to materialize. | [optional] [default to summary]

### Return type

[**ApiSnapshotPolicyList**](ApiSnapshotPolicyList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_policy" id="read_policy"></a>
# **read_policy**
> ApiSnapshotPolicy read_policy(cluster_name, policy_name, service_name, view=view)

Returns information for a specific snapshot policy.

Returns information for a specific snapshot policy.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.SnapshotsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
policy_name = 'policy_name_example' # str | Name of an existing snapshot policy.
service_name = 'service_name_example' # str | 
view = 'summary' # str | The view to materialize. (optional) (default to summary)

try:
    # Returns information for a specific snapshot policy.
    api_response = api_instance.read_policy(cluster_name, policy_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SnapshotsResourceApi->read_policy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **policy_name** | **str**| Name of an existing snapshot policy. | 
 **service_name** | **str**|  | 
 **view** | **str**| The view to materialize. | [optional] [default to summary]

### Return type

[**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_policy" id="update_policy"></a>
# **update_policy**
> ApiSnapshotPolicy update_policy(cluster_name, policy_name, service_name, body=body)

Updates an existing snapshot policy.

Updates an existing snapshot policy.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.SnapshotsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
policy_name = 'policy_name_example' # str | Name of an existing snapshot policy.
service_name = 'service_name_example' # str | 
body = cm_client.ApiSnapshotPolicy() # ApiSnapshotPolicy | Modified policy. (optional)

try:
    # Updates an existing snapshot policy.
    api_response = api_instance.update_policy(cluster_name, policy_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SnapshotsResourceApi->update_policy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **policy_name** | **str**| Name of an existing snapshot policy. | 
 **service_name** | **str**|  | 
 **body** | [**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)| Modified policy. | [optional] 

### Return type

[**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

