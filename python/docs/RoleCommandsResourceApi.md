# cm_client.RoleCommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**format_command**](RoleCommandsResourceApi.md#format_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsFormat | Format HDFS NameNodes.
[**hdfs_bootstrap_stand_by_command**](RoleCommandsResourceApi.md#hdfs_bootstrap_stand_by_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsBootstrapStandBy | Bootstrap HDFS stand-by NameNodes.
[**hdfs_enter_safemode**](RoleCommandsResourceApi.md#hdfs_enter_safemode) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsEnterSafemode | Enter safemode for namenodes.
[**hdfs_finalize_metadata_upgrade**](RoleCommandsResourceApi.md#hdfs_finalize_metadata_upgrade) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsFinalizeMetadataUpgrade | Finalize HDFS NameNode metadata upgrade.
[**hdfs_initialize_auto_failover_command**](RoleCommandsResourceApi.md#hdfs_initialize_auto_failover_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsInitializeAutoFailover | Initialize HDFS HA failover controller metadata.
[**hdfs_initialize_shared_dir_command**](RoleCommandsResourceApi.md#hdfs_initialize_shared_dir_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsInitializeSharedDir | Initialize HDFS NameNodes&#39; shared edit directory.
[**hdfs_leave_safemode**](RoleCommandsResourceApi.md#hdfs_leave_safemode) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsLeaveSafemode | Leave safemode for namenodes.
[**hdfs_save_namespace**](RoleCommandsResourceApi.md#hdfs_save_namespace) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsSaveNamespace | Save namespace for namenodes.
[**jmap_dump**](RoleCommandsResourceApi.md#jmap_dump) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jmapDump | Run the jmapDump diagnostic command.
[**jmap_histo**](RoleCommandsResourceApi.md#jmap_histo) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jmapHisto | Run the jmapHisto diagnostic command.
[**jstack**](RoleCommandsResourceApi.md#jstack) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jstack | Run the jstack diagnostic command.
[**lsof**](RoleCommandsResourceApi.md#lsof) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/lsof | Run the lsof diagnostic command.
[**refresh_command**](RoleCommandsResourceApi.md#refresh_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/refresh | Refresh a role&#39;s data.
[**restart_command**](RoleCommandsResourceApi.md#restart_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/restart | Restart a set of role instances.
[**role_command_by_name**](RoleCommandsResourceApi.md#role_command_by_name) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/{commandName} | Execute a role command by name.
[**start_command**](RoleCommandsResourceApi.md#start_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/start | Start a set of role instances.
[**stop_command**](RoleCommandsResourceApi.md#stop_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/stop | Stop a set of role instances.
[**sync_hue_db_command**](RoleCommandsResourceApi.md#sync_hue_db_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hueSyncDb | Create / update the Hue database schema.
[**zoo_keeper_cleanup_command**](RoleCommandsResourceApi.md#zoo_keeper_cleanup_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/zooKeeperCleanup | Cleanup a list of ZooKeeper server roles.
[**zoo_keeper_init_command**](RoleCommandsResourceApi.md#zoo_keeper_init_command) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/zooKeeperInit | Initialize a list of ZooKeeper server roles.


<a name="format_command" id="format_command"></a>
# **format_command**
> ApiBulkCommandList format_command(cluster_name, service_name, body=body)

Format HDFS NameNodes.

Format HDFS NameNodes. <p> Submit a format request to a list of NameNodes on a service. Note that trying to format a previously formatted NameNode will fail. <p> Note about high availability: when two NameNodes are working in an HA pair, only one of them should be formatted. <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the NameNodes to format. (optional)

try:
    # Format HDFS NameNodes.
    api_response = api_instance.format_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->format_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the NameNodes to format. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_bootstrap_stand_by_command" id="hdfs_bootstrap_stand_by_command"></a>
# **hdfs_bootstrap_stand_by_command**
> ApiBulkCommandList hdfs_bootstrap_stand_by_command(cluster_name, service_name, body=body)

Bootstrap HDFS stand-by NameNodes.

Bootstrap HDFS stand-by NameNodes. <p> Submit a request to synchronize HDFS NameNodes with their assigned HA partners. The command requires that the target NameNodes are part of existing HA pairs, which can be accomplished by setting the nameservice configuration parameter in the NameNode's configuration. <p> The HA partner must already be formatted and running for this command to run.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the stand-by NameNodes to bootstrap. (optional)

try:
    # Bootstrap HDFS stand-by NameNodes.
    api_response = api_instance.hdfs_bootstrap_stand_by_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->hdfs_bootstrap_stand_by_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the stand-by NameNodes to bootstrap. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_enter_safemode" id="hdfs_enter_safemode"></a>
# **hdfs_enter_safemode**
> ApiBulkCommandList hdfs_enter_safemode(cluster_name, service_name, body=body)

Enter safemode for namenodes.

Enter safemode for namenodes <p/> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | NameNodes for which to enter safemode. (optional)

try:
    # Enter safemode for namenodes.
    api_response = api_instance.hdfs_enter_safemode(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->hdfs_enter_safemode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to enter safemode. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_finalize_metadata_upgrade" id="hdfs_finalize_metadata_upgrade"></a>
# **hdfs_finalize_metadata_upgrade**
> ApiBulkCommandList hdfs_finalize_metadata_upgrade(cluster_name, service_name, body=body)

Finalize HDFS NameNode metadata upgrade.

Finalize HDFS NameNode metadata upgrade. <p/> Available since API v3.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | NameNodes for which to finalize the upgrade. (optional)

try:
    # Finalize HDFS NameNode metadata upgrade.
    api_response = api_instance.hdfs_finalize_metadata_upgrade(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->hdfs_finalize_metadata_upgrade: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to finalize the upgrade. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_initialize_auto_failover_command" id="hdfs_initialize_auto_failover_command"></a>
# **hdfs_initialize_auto_failover_command**
> ApiBulkCommandList hdfs_initialize_auto_failover_command(cluster_name, service_name, body=body)

Initialize HDFS HA failover controller metadata.

Initialize HDFS HA failover controller metadata. <p> The controllers being initialized must already exist and be properly configured. The command will make sure the needed data is initialized for the controller to work. <p> Only one controller per nameservice needs to be initialized.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the controllers to initialize. (optional)

try:
    # Initialize HDFS HA failover controller metadata.
    api_response = api_instance.hdfs_initialize_auto_failover_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->hdfs_initialize_auto_failover_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the controllers to initialize. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_initialize_shared_dir_command" id="hdfs_initialize_shared_dir_command"></a>
# **hdfs_initialize_shared_dir_command**
> ApiBulkCommandList hdfs_initialize_shared_dir_command(cluster_name, service_name, body=body)

Initialize HDFS NameNodes' shared edit directory.

Initialize HDFS NameNodes' shared edit directory. <p> Shared edit directories are used when two HDFS NameNodes are operating as a high-availability pair. This command initializes the shared directory to include the necessary metadata. <p> The provided role names should reflect one of the NameNodes in the respective HA pair; the role must be stopped and its data directory must already have been formatted. The shared edits directory must be empty for this command to succeed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the NameNodes. (optional)

try:
    # Initialize HDFS NameNodes' shared edit directory.
    api_response = api_instance.hdfs_initialize_shared_dir_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->hdfs_initialize_shared_dir_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the NameNodes. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_leave_safemode" id="hdfs_leave_safemode"></a>
# **hdfs_leave_safemode**
> ApiBulkCommandList hdfs_leave_safemode(cluster_name, service_name, body=body)

Leave safemode for namenodes.

Leave safemode for namenodes <p/> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | NameNodes for which to leave safemode. (optional)

try:
    # Leave safemode for namenodes.
    api_response = api_instance.hdfs_leave_safemode(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->hdfs_leave_safemode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to leave safemode. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="hdfs_save_namespace" id="hdfs_save_namespace"></a>
# **hdfs_save_namespace**
> ApiBulkCommandList hdfs_save_namespace(cluster_name, service_name, body=body)

Save namespace for namenodes.

Save namespace for namenodes <p/> Available since API v4.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | NameNodes for which to save namespace. (optional)

try:
    # Save namespace for namenodes.
    api_response = api_instance.hdfs_save_namespace(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->hdfs_save_namespace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to save namespace. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="jmap_dump" id="jmap_dump"></a>
# **jmap_dump**
> ApiBulkCommandList jmap_dump(cluster_name, service_name, body=body)

Run the jmapDump diagnostic command.

Run the jmapDump diagnostic command. The command runs the jmap utility to capture a dump of the role's java heap. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to jmap. (optional)

try:
    # Run the jmapDump diagnostic command.
    api_response = api_instance.jmap_dump(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->jmap_dump: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="jmap_histo" id="jmap_histo"></a>
# **jmap_histo**
> ApiBulkCommandList jmap_histo(cluster_name, service_name, body=body)

Run the jmapHisto diagnostic command.

Run the jmapHisto diagnostic command. The command runs the jmap utility to capture a histogram of the objects on the role's java heap. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to jmap. (optional)

try:
    # Run the jmapHisto diagnostic command.
    api_response = api_instance.jmap_histo(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->jmap_histo: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="jstack" id="jstack"></a>
# **jstack**
> ApiBulkCommandList jstack(cluster_name, service_name, body=body)

Run the jstack diagnostic command.

Run the jstack diagnostic command. The command runs the jstack utility to capture a role's java thread stacks. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to jstack. (optional)

try:
    # Run the jstack diagnostic command.
    api_response = api_instance.jstack(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->jstack: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jstack. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="lsof" id="lsof"></a>
# **lsof**
> ApiBulkCommandList lsof(cluster_name, service_name, body=body)

Run the lsof diagnostic command.

Run the lsof diagnostic command. This command runs the lsof utility to list a role's open files. <p/> Available since API v8.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the names of the roles to lsof. (optional)

try:
    # Run the lsof diagnostic command.
    api_response = api_instance.lsof(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->lsof: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to lsof. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="refresh_command" id="refresh_command"></a>
# **refresh_command**
> ApiBulkCommandList refresh_command(cluster_name, service_name, body=body)

Refresh a role's data.

Refresh a role's data. <p> For MapReduce services, this command should be executed on JobTracker roles. It refreshes the role's queue and node information. <p> For HDFS services, this command should be executed on NameNode or DataNode roles. For NameNodes, it refreshes the role's node list. For DataNodes, it refreshes the role's data directory list and other configuration. <p> For YARN services, this command should be executed on ResourceManager roles. It refreshes the role's queue and node information. <p> Available since API v1. DataNode data directories refresh available since API v10.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the roles. (optional)

try:
    # Refresh a role's data.
    api_response = api_instance.refresh_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->refresh_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="restart_command" id="restart_command"></a>
# **restart_command**
> ApiBulkCommandList restart_command(cluster_name, service_name, body=body)

Restart a set of role instances.

Restart a set of role instances <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The name of the roles to restart. (optional)

try:
    # Restart a set of role instances.
    api_response = api_instance.restart_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->restart_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The name of the roles to restart. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="role_command_by_name" id="role_command_by_name"></a>
# **role_command_by_name**
> ApiBulkCommandList role_command_by_name(cluster_name, command_name, service_name, body=body)

Execute a role command by name.

Execute a role command by name. <p/> Available since API v6.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
command_name = 'command_name_example' # str | the name of command to execute.
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | the roles to run this command on. (optional)

try:
    # Execute a role command by name.
    api_response = api_instance.role_command_by_name(cluster_name, command_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->role_command_by_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **command_name** | **str**| the name of command to execute. | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the roles to run this command on. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_command" id="start_command"></a>
# **start_command**
> ApiBulkCommandList start_command(cluster_name, service_name, body=body)

Start a set of role instances.

Start a set of role instances. <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the roles to start. (optional)

try:
    # Start a set of role instances.
    api_response = api_instance.start_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->start_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles to start. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stop_command" id="stop_command"></a>
# **stop_command**
> ApiBulkCommandList stop_command(cluster_name, service_name, body=body)

Stop a set of role instances.

Stop a set of role instances. <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The role type. (optional)

try:
    # Stop a set of role instances.
    api_response = api_instance.stop_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->stop_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The role type. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="sync_hue_db_command" id="sync_hue_db_command"></a>
# **sync_hue_db_command**
> ApiBulkCommandList sync_hue_db_command(cluster_name, service_name, body=body)

Create / update the Hue database schema.

Create / update the Hue database schema. <p> This command is to be run whenever a new database has been specified or, as necessary, after an upgrade. <p> This request should be sent to Hue servers only.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the Hue server roles. (optional)

try:
    # Create / update the Hue database schema.
    api_response = api_instance.sync_hue_db_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->sync_hue_db_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the Hue server roles. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="zoo_keeper_cleanup_command" id="zoo_keeper_cleanup_command"></a>
# **zoo_keeper_cleanup_command**
> ApiBulkCommandList zoo_keeper_cleanup_command(cluster_name, service_name, body=body)

Cleanup a list of ZooKeeper server roles.

Cleanup a list of ZooKeeper server roles. <p> This command removes snapshots and transaction log files kept by ZooKeeper for backup purposes. Refer to the ZooKeeper documentation for more details.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the roles. (optional)

try:
    # Cleanup a list of ZooKeeper server roles.
    api_response = api_instance.zoo_keeper_cleanup_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->zoo_keeper_cleanup_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="zoo_keeper_init_command" id="zoo_keeper_init_command"></a>
# **zoo_keeper_init_command**
> ApiBulkCommandList zoo_keeper_init_command(cluster_name, service_name, body=body)

Initialize a list of ZooKeeper server roles.

Initialize a list of ZooKeeper server roles. <p> This applies to ZooKeeper services from CDH4. Before ZooKeeper server roles can be used, they need to be initialized.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RoleCommandsResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The names of the roles. (optional)

try:
    # Initialize a list of ZooKeeper server roles.
    api_response = api_instance.zoo_keeper_init_command(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleCommandsResourceApi->zoo_keeper_init_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

