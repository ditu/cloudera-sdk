# ApiHdfsDisableHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_name** | **str** | Name of the the NameNode to be kept. | [optional] 
**secondary_name** | **str** | Name of the SecondaryNamenode to associate with the active NameNode. | [optional] 
**start_dependent_services** | **bool** | Whether to re-start dependent services. Defaults to true. | [optional] 
**deploy_client_configs** | **bool** | Whether to re-deploy client configurations. Defaults to true. | [optional] 
**disable_quorum_storage** | **bool** | Whether to disable Quorum-based Storage. Defaults to false.  Available since API v2. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


