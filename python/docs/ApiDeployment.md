# ApiDeployment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **str** | Readonly. This timestamp is provided when you request a deployment and is not required (or even read) when creating a deployment. This timestamp is useful if you have multiple deployments saved and want to determine which one to use as a restore point. | [optional] 
**clusters** | [**list[ApiCluster]**](ApiCluster.md) | List of clusters in the system including their services, roles and complete config values. | [optional] 
**hosts** | [**list[ApiHost]**](ApiHost.md) | List of hosts in the system | [optional] 
**users** | [**list[ApiUser]**](ApiUser.md) | List of all users in the system | [optional] 
**version_info** | [**ApiVersionInfo**](ApiVersionInfo.md) | Full version information about the running Cloudera Manager instance | [optional] 
**management_service** | [**ApiService**](ApiService.md) | The full configuration of the Cloudera Manager management service including all the management roles and their config values | [optional] 
**manager_settings** | [**ApiConfigList**](ApiConfigList.md) | The full configuration of Cloudera Manager itself including licensing info | [optional] 
**all_hosts_config** | [**ApiConfigList**](ApiConfigList.md) | Configuration parameters that apply to all hosts, unless overridden at the host level. Available since API v3. | [optional] 
**peers** | [**list[ApiCmPeer]**](ApiCmPeer.md) | The list of peers configured in Cloudera Manager. Available since API v3. | [optional] 
**host_templates** | [**ApiHostTemplateList**](ApiHostTemplateList.md) | The list of all host templates in Cloudera Manager. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


