# ApiHostTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the host template. Unique across clusters. | [optional] 
**cluster_ref** | [**ApiClusterRef**](ApiClusterRef.md) | Readonly. A reference to the cluster the host template belongs to. | [optional] 
**role_config_group_refs** | [**list[ApiRoleConfigGroupRef]**](ApiRoleConfigGroupRef.md) | The role config groups belonging to this host tempalte. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


