# cm_client.AuthServiceResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auto_assign_roles**](AuthServiceResourceApi.md#auto_assign_roles) | **PUT** /cm/authService/autoAssignRoles | Automatically assign roles to hosts and create the roles for the Authentication Service.
[**auto_configure**](AuthServiceResourceApi.md#auto_configure) | **PUT** /cm/authService/autoConfigure | Automatically configures roles of the Authentication Service.
[**delete**](AuthServiceResourceApi.md#delete) | **DELETE** /cm/authService | Delete the Authentication Service.
[**enter_maintenance_mode**](AuthServiceResourceApi.md#enter_maintenance_mode) | **POST** /cm/authService/commands/enterMaintenanceMode | Put the Authentication Service into maintenance mode.
[**exit_maintenance_mode**](AuthServiceResourceApi.md#exit_maintenance_mode) | **POST** /cm/authService/commands/exitMaintenanceMode | Take the Authentication Service out of maintenance mode.
[**list_active_commands**](AuthServiceResourceApi.md#list_active_commands) | **GET** /cm/authService/commands | List active Authentication Service commands.
[**list_role_types**](AuthServiceResourceApi.md#list_role_types) | **GET** /cm/authService/roleTypes | List the supported role types for the Authentication Service.
[**read_service**](AuthServiceResourceApi.md#read_service) | **GET** /cm/authService | Retrieve information about the Authentication Services.
[**read_service_config**](AuthServiceResourceApi.md#read_service_config) | **GET** /cm/authService/config | 
[**restart_command**](AuthServiceResourceApi.md#restart_command) | **POST** /cm/authService/commands/restart | Restart the Authentication Service.
[**setup**](AuthServiceResourceApi.md#setup) | **PUT** /cm/authService | Setup the Authentication Service.
[**start_command**](AuthServiceResourceApi.md#start_command) | **POST** /cm/authService/commands/start | Start the Authentication Service.
[**stop_command**](AuthServiceResourceApi.md#stop_command) | **POST** /cm/authService/commands/stop | Stop the Authentication Service.
[**update_service_config**](AuthServiceResourceApi.md#update_service_config) | **PUT** /cm/authService/config | 


<a name="auto_assign_roles" id="auto_assign_roles"></a>
# **auto_assign_roles**
> auto_assign_roles()

Automatically assign roles to hosts and create the roles for the Authentication Service.

Automatically assign roles to hosts and create the roles for the Authentication Service. <p> Assignments are done based on number of hosts in the deployment and hardware specifications. If no hosts are part of the deployment, an exception will be thrown preventing any role assignments. Existing roles will be taken into account and their assignments will be not be modified. The deployment should not have any clusters when calling this endpoint. If it does, an exception will be thrown preventing any role assignments.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Automatically assign roles to hosts and create the roles for the Authentication Service.
    api_instance.auto_assign_roles()
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->auto_assign_roles: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="auto_configure" id="auto_configure"></a>
# **auto_configure**
> auto_configure()

Automatically configures roles of the Authentication Service.

Automatically configures roles of the Authentication Service. <p> Overwrites some existing configurations. Only default role config groups must exist before calling this endpoint. Other role config groups must not exist. If they do, an exception will be thrown preventing any configuration. Ignores any clusters (and their services and roles) colocated with the Authentication Service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Automatically configures roles of the Authentication Service.
    api_instance.auto_configure()
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->auto_configure: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete" id="delete"></a>
# **delete**
> ApiService delete()

Delete the Authentication Service.

Delete the Authentication Service. <p> This method will fail if a CMS instance doesn't already exist.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Delete the Authentication Service.
    api_response = api_instance.delete()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->delete: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enter_maintenance_mode" id="enter_maintenance_mode"></a>
# **enter_maintenance_mode**
> ApiCommand enter_maintenance_mode()

Put the Authentication Service into maintenance mode.

Put the Authentication Service into maintenance mode. This is a synchronous command. The result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Put the Authentication Service into maintenance mode.
    api_response = api_instance.enter_maintenance_mode()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->enter_maintenance_mode: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="exit_maintenance_mode" id="exit_maintenance_mode"></a>
# **exit_maintenance_mode**
> ApiCommand exit_maintenance_mode()

Take the Authentication Service out of maintenance mode.

Take the Authentication Service out of maintenance mode. This is a synchronous command. The result is known immediately upon return.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Take the Authentication Service out of maintenance mode.
    api_response = api_instance.exit_maintenance_mode()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->exit_maintenance_mode: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_active_commands" id="list_active_commands"></a>
# **list_active_commands**
> ApiCommandList list_active_commands(view=view)

List active Authentication Service commands.

List active Authentication Service commands.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List active Authentication Service commands.
    api_response = api_instance.list_active_commands(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->list_active_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_role_types" id="list_role_types"></a>
# **list_role_types**
> ApiRoleTypeList list_role_types()

List the supported role types for the Authentication Service.

List the supported role types for the Authentication Service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # List the supported role types for the Authentication Service.
    api_response = api_instance.list_role_types()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->list_role_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleTypeList**](ApiRoleTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_service" id="read_service"></a>
# **read_service**
> ApiService read_service()

Retrieve information about the Authentication Services.

Retrieve information about the Authentication Services.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Retrieve information about the Authentication Services.
    api_response = api_instance.read_service()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->read_service: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_service_config" id="read_service_config"></a>
# **read_service_config**
> ApiServiceConfig read_service_config(view=view)





### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))
view = 'summary' # str |  (optional) (default to summary)

try:
    api_response = api_instance.read_service_config(view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->read_service_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **str**|  | [optional] [default to summary]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="restart_command" id="restart_command"></a>
# **restart_command**
> ApiCommand restart_command()

Restart the Authentication Service.

Restart the Authentication Service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Restart the Authentication Service.
    api_response = api_instance.restart_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->restart_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="setup" id="setup"></a>
# **setup**
> ApiService setup(body=body)

Setup the Authentication Service.

Setup the Authentication Service. <p> Configure the Auth Service instance with the information given in the ApiService. The provided configuration data can be used to set up host mappings for each role, and required configuration such as database connection information for specific roles. <p> This method needs a valid CM license to be installed beforehand. <p> This method does not start any services or roles. <p> This method will fail if a Auth Service instance already exists. <p> Available role types: <ul> <li>AUTHSRV</li> </ul>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiService() # ApiService | Role configuration overrides. (optional)

try:
    # Setup the Authentication Service.
    api_response = api_instance.setup(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->setup: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiService**](ApiService.md)| Role configuration overrides. | [optional] 

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_command" id="start_command"></a>
# **start_command**
> ApiCommand start_command()

Start the Authentication Service.

Start the Authentication Service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Start the Authentication Service.
    api_response = api_instance.start_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->start_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stop_command" id="stop_command"></a>
# **stop_command**
> ApiCommand stop_command()

Stop the Authentication Service.

Stop the Authentication Service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))

try:
    # Stop the Authentication Service.
    api_response = api_instance.stop_command()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->stop_command: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_service_config" id="update_service_config"></a>
# **update_service_config**
> ApiServiceConfig update_service_config(message=message, body=body)





### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceResourceApi(cm_client.ApiClient(configuration))
message = 'message_example' # str |  (optional)
body = cm_client.ApiServiceConfig() # ApiServiceConfig |  (optional)

try:
    api_response = api_instance.update_service_config(message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceResourceApi->update_service_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **str**|  | [optional] 
 **body** | [**ApiServiceConfig**](ApiServiceConfig.md)|  | [optional] 

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

