# cm_client.ProcessResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_config_file**](ProcessResourceApi.md#get_config_file) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/process/configFiles/{configFileName} | Returns the contents of the specified config file.
[**get_process**](ProcessResourceApi.md#get_process) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/process | 


<a name="get_config_file" id="get_config_file"></a>
# **get_config_file**
> file get_config_file(cluster_name, config_file_name, role_name, service_name)

Returns the contents of the specified config file.

Returns the contents of the specified config file. A multi-level file name (e.g. hadoop-conf/hdfs-site.xml) is acceptable here.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ProcessResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
config_file_name = 'config_file_name_example' # str | Name of the config file to get.
role_name = 'role_name_example' # str | 
service_name = 'service_name_example' # str | 

try:
    # Returns the contents of the specified config file.
    api_response = api_instance.get_config_file(cluster_name, config_file_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProcessResourceApi->get_config_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **config_file_name** | **str**| Name of the config file to get. | 
 **role_name** | **str**|  | 
 **service_name** | **str**|  | 

### Return type

[**file**](file.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_process" id="get_process"></a>
# **get_process**
> ApiProcess get_process(cluster_name, role_name, service_name)





### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.ProcessResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | 
service_name = 'service_name_example' # str | 

try:
    api_response = api_instance.get_process(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProcessResourceApi->get_process: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**|  | 
 **service_name** | **str**|  | 

### Return type

[**ApiProcess**](ApiProcess.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

