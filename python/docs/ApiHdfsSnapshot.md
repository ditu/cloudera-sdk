# ApiHdfsSnapshot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **str** | Snapshotted path. | [optional] 
**snapshot_name** | **str** | Snapshot name. | [optional] 
**snapshot_path** | **str** | Read-only. Fully qualified path for the snapshot version of \&quot;path\&quot;. &lt;p/&gt; For example, if a snapshot \&quot;s1\&quot; is present at \&quot;/a/.snapshot/s1, then the snapshot path corresponding to \&quot;s1\&quot; for path \&quot;/a/b\&quot; will be \&quot;/a/.snapshot/s1/b\&quot;. | [optional] 
**creation_time** | **str** | Snapshot creation time. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


