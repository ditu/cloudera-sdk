# ApiImpalaUtilizationHistogram

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bins** | [**ApiImpalaUtilizationHistogramBinList**](ApiImpalaUtilizationHistogramBinList.md) | Bins of the histogram. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


