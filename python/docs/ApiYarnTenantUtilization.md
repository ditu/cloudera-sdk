# ApiYarnTenantUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenant_name** | **str** | Name of the tenant. | [optional] 
**avg_yarn_cpu_allocation** | **float** | Average number of VCores allocated to YARN applications of the tenant. | [optional] 
**avg_yarn_cpu_utilization** | **float** | Average number of VCores used by YARN applications of the tenant. | [optional] 
**avg_yarn_cpu_unused_capacity** | **float** | Average unused VCores of the tenant. | [optional] 
**avg_yarn_cpu_steady_fair_share** | **float** | Average steady fair share VCores. | [optional] 
**avg_yarn_pool_allocated_cpu_during_contention** | **float** | Average allocated Vcores with pending containers. | [optional] 
**avg_yarn_pool_fair_share_cpu_during_contention** | **float** | Average fair share VCores with pending containers. | [optional] 
**avg_yarn_pool_steady_fair_share_cpu_during_contention** | **float** | Average steady fair share VCores with pending containers. | [optional] 
**avg_yarn_container_wait_ratio** | **float** | Average percentage of pending containers for the pool during periods of contention. | [optional] 
**avg_yarn_memory_allocation** | **float** | Average memory allocated to YARN applications of the tenant. | [optional] 
**avg_yarn_memory_utilization** | **float** | Average memory used by YARN applications of the tenant. | [optional] 
**avg_yarn_memory_unused_capacity** | **float** | Average unused memory of the tenant. | [optional] 
**avg_yarn_memory_steady_fair_share** | **float** | Average steady fair share memory. | [optional] 
**avg_yarn_pool_allocated_memory_during_contention** | **float** | Average allocated memory with pending containers. | [optional] 
**avg_yarn_pool_fair_share_memory_during_contention** | **float** | Average fair share memory with pending containers. | [optional] 
**avg_yarn_pool_steady_fair_share_memory_during_contention** | **float** | Average steady fair share memory with pending containers. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


