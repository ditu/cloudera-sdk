# ApiNameservice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the nameservice. | [optional] 
**active** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the active NameNode. | [optional] 
**active_failover_controller** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the active NameNode&#39;s failover controller, if configured. | [optional] 
**stand_by** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the stand-by NameNode. | [optional] 
**stand_by_failover_controller** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the stand-by NameNode&#39;s failover controller, if configured. | [optional] 
**secondary** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the SecondaryNameNode. | [optional] 
**mount_points** | **list[str]** | Mount points assigned to this nameservice in a federation. | [optional] 
**health_summary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Requires \&quot;full\&quot; view. The high-level health status of this nameservice. | [optional] 
**health_checks** | [**list[ApiHealthCheck]**](ApiHealthCheck.md) | Requires \&quot;full\&quot; view. List of health checks performed on the nameservice. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


