# ApiRollingRestartClusterArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slave_batch_size** | **float** | Number of hosts with slave roles to restart at a time. Must be greater than zero. Default is 1. | [optional] 
**sleep_seconds** | **float** | Number of seconds to sleep between restarts of slave host batches. &lt;p&gt; Must be greater than or equal to 0. Default is 0. | [optional] 
**slave_fail_count_threshold** | **float** | The threshold for number of slave host batches that are allowed to fail to restart before the entire command is considered failed. &lt;p&gt; Must be greater than or equal to 0. Default is 0. &lt;p&gt; This argument is for ADVANCED users only. &lt;/p&gt; | [optional] 
**stale_configs_only** | **bool** | Restart roles with stale configs only. | [optional] 
**un_upgraded_only** | **bool** | Restart roles that haven&#39;t been upgraded yet. | [optional] 
**redeploy_client_configuration** | **bool** | Re-deploy client configuration. Available since API v6. | [optional] 
**roles_to_include** | [**ApiRolesToInclude**](ApiRolesToInclude.md) | Role types to restart. Default is slave roles only. | [optional] 
**restart_service_names** | **list[str]** | List of services to restart. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


