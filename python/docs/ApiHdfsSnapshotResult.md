# ApiHdfsSnapshotResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processed_path_count** | **float** | Number of processed paths. | [optional] 
**processed_paths** | **list[str]** | The list of processed paths. &lt;p/&gt; This is only available in the full view. | [optional] 
**unprocessed_path_count** | **float** | Number of unprocessed paths. | [optional] 
**unprocessed_paths** | **list[str]** | The list of unprocessed paths. Note that paths that are currently being processed will also be included in this list. &lt;p/&gt; This is only available in the full view. | [optional] 
**created_snapshot_count** | **float** | Number of snapshots created. | [optional] 
**created_snapshots** | [**list[ApiHdfsSnapshot]**](ApiHdfsSnapshot.md) | List of snapshots created. &lt;p/&gt; This is only available in the full view. | [optional] 
**deleted_snapshot_count** | **float** | Number of snapshots deleted. | [optional] 
**deleted_snapshots** | [**list[ApiHdfsSnapshot]**](ApiHdfsSnapshot.md) | List of snapshots deleted. &lt;p/&gt; This is only available in the full view. | [optional] 
**creation_error_count** | **float** | Number of errors detected when creating snapshots. | [optional] 
**creation_errors** | [**list[ApiHdfsSnapshotError]**](ApiHdfsSnapshotError.md) | List of errors encountered when creating snapshots. &lt;p/&gt; This is only available in the full view. | [optional] 
**deletion_error_count** | **float** | Number of errors detected when deleting snapshots. | [optional] 
**deletion_errors** | [**list[ApiHdfsSnapshotError]**](ApiHdfsSnapshotError.md) | List of errors encountered when deleting snapshots. &lt;p/&gt; This is only available in the full view. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


