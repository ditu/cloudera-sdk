# cm_client.AuthServiceRoleCommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restart_command**](AuthServiceRoleCommandsResourceApi.md#restart_command) | **POST** /cm/authService/roleCommands/restart | Restart a set of Authentication Service roles.
[**start_command**](AuthServiceRoleCommandsResourceApi.md#start_command) | **POST** /cm/authService/roleCommands/start | Start a set of Authentication Service roles.
[**stop_command**](AuthServiceRoleCommandsResourceApi.md#stop_command) | **POST** /cm/authService/roleCommands/stop | Stop a set of Authentication Service roles.


<a name="restart_command" id="restart_command"></a>
# **restart_command**
> ApiBulkCommandList restart_command(body=body)

Restart a set of Authentication Service roles.

Restart a set of Authentication Service roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The roles to restart. (optional)

try:
    # Restart a set of Authentication Service roles.
    api_response = api_instance.restart_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRoleCommandsResourceApi->restart_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to restart. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="start_command" id="start_command"></a>
# **start_command**
> ApiBulkCommandList start_command(body=body)

Start a set of Authentication Service roles.

Start a set of Authentication Service roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The roles to start. (optional)

try:
    # Start a set of Authentication Service roles.
    api_response = api_instance.start_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRoleCommandsResourceApi->start_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to start. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stop_command" id="stop_command"></a>
# **stop_command**
> ApiBulkCommandList stop_command(body=body)

Stop a set of Authentication Service roles.

Stop a set of Authentication Service roles.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.AuthServiceRoleCommandsResourceApi(cm_client.ApiClient(configuration))
body = cm_client.ApiRoleNameList() # ApiRoleNameList | The roles to stop. (optional)

try:
    # Stop a set of Authentication Service roles.
    api_response = api_instance.stop_command(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthServiceRoleCommandsResourceApi->stop_command: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to stop. | [optional] 

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

