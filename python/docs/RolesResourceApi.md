# cm_client.RolesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bulk_delete_roles**](RolesResourceApi.md#bulk_delete_roles) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/bulkDelete | Bulk delete roles in a particular service by name.
[**create_roles**](RolesResourceApi.md#create_roles) | **POST** /clusters/{clusterName}/services/{serviceName}/roles | Create new roles in a given service.
[**delete_role**](RolesResourceApi.md#delete_role) | **DELETE** /clusters/{clusterName}/services/{serviceName}/roles/{roleName} | Deletes a role from a given service.
[**enter_maintenance_mode**](RolesResourceApi.md#enter_maintenance_mode) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/enterMaintenanceMode | Put the role into maintenance mode.
[**exit_maintenance_mode**](RolesResourceApi.md#exit_maintenance_mode) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/exitMaintenanceMode | Take the role out of maintenance mode.
[**get_full_log**](RolesResourceApi.md#get_full_log) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/full | Retrieves the log file for the role&#39;s main process.
[**get_metrics**](RolesResourceApi.md#get_metrics) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/metrics | Fetch metric readings for a particular role.
[**get_stacks_log**](RolesResourceApi.md#get_stacks_log) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stacks | Retrieves the stacks log file, if any, for the role&#39;s main process.
[**get_stacks_logs_bundle**](RolesResourceApi.md#get_stacks_logs_bundle) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stacksBundle | Download a zip-compressed archive of role stacks logs.
[**get_standard_error**](RolesResourceApi.md#get_standard_error) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stderr | Retrieves the role&#39;s standard error output.
[**get_standard_output**](RolesResourceApi.md#get_standard_output) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stdout | Retrieves the role&#39;s standard output.
[**impala_diagnostics**](RolesResourceApi.md#impala_diagnostics) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/impalaDiagnostics | Collects diagnostics data for an Impala role.
[**list_active_commands**](RolesResourceApi.md#list_active_commands) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands | List active role commands.
[**list_commands**](RolesResourceApi.md#list_commands) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commandsByName | Lists all the commands that can be executed by name on the provided role.
[**read_role**](RolesResourceApi.md#read_role) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName} | Retrieves detailed information about a role.
[**read_role_config**](RolesResourceApi.md#read_role_config) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/config | Retrieves the configuration of a specific role.
[**read_roles**](RolesResourceApi.md#read_roles) | **GET** /clusters/{clusterName}/services/{serviceName}/roles | Lists all roles of a given service.
[**update_role_config**](RolesResourceApi.md#update_role_config) | **PUT** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/config | Updates the role configuration with the given values.


<a name="bulk_delete_roles" id="bulk_delete_roles"></a>
# **bulk_delete_roles**
> ApiRoleList bulk_delete_roles(cluster_name, service_name, body=body)

Bulk delete roles in a particular service by name.

Bulk delete roles in a particular service by name. Fails if any role cannot be found.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleNameList() # ApiRoleNameList | list of role names to be deleted (optional)

try:
    # Bulk delete roles in a particular service by name.
    api_response = api_instance.bulk_delete_roles(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->bulk_delete_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| list of role names to be deleted | [optional] 

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create_roles" id="create_roles"></a>
# **create_roles**
> ApiRoleList create_roles(cluster_name, service_name, body=body)

Create new roles in a given service.

Create new roles in a given service.  <table> <thead> <tr> <th>Service Type</th> <th>Available Role Types</th> </tr> </thead> <tbody> <tr> <td>HDFS (CDH3)</td> <td>NAMENODE, DATANODE, SECONDARYNAMENODE, BALANCER, GATEWAY</td> </tr> <tr> <td>HDFS (CDH4)</td> <td>NAMENODE, DATANODE, SECONDARYNAMENODE, BALANCER, HTTPFS, FAILOVERCONTROLLER, GATEWAY, JOURNALNODE</td> </tr> <tr> <td>HDFS (CDH5)</td> <td>NAMENODE, DATANODE, SECONDARYNAMENODE, BALANCER, HTTPFS, FAILOVERCONTROLLER, GATEWAY, JOURNALNODE, NFSGATEWAY</td> </tr> <td>MAPREDUCE</td> <td>JOBTRACKER, TASKTRACKER, GATEWAY, FAILOVERCONTROLLER,</td> </tr> <td>HBASE</td> <td>MASTER, REGIONSERVER, GATEWAY, HBASETHRIFTSERVER, HBASERESTSERVER</td> </tr> <tr> <td>YARN</td> <td>RESOURCEMANAGER, NODEMANAGER, JOBHISTORY, GATEWAY</td> </tr> <tr> <td>OOZIE</td> <td>OOZIE_SERVER</td> </tr> <tr> <td>ZOOKEEPER</td> <td>SERVER</td> </tr> <tr> <td>HUE (CDH3)</td> <td>HUE_SERVER, BEESWAX_SERVER, KT_RENEWER, JOBSUBD</td> </tr> <tr> <td>HUE (CDH4)</td> <td>HUE_SERVER, BEESWAX_SERVER, KT_RENEWER</td> </tr> <tr> <td>HUE (CDH5)</td> <td>HUE_SERVER, KT_RENEWER</td> </tr> <tr> <td>HUE (CDH5 5.5+)</td> <td>HUE_SERVER, KT_RENEWER, HUE_LOAD_BALANCER</td> </tr> <tr> <td>FLUME</td> <td>AGENT</td> </tr> <tr> <td>IMPALA (CDH4)</td> <td>IMPALAD, STATESTORE, CATALOGSERVER</td> </tr> <tr> <td>IMPALA (CDH5)</td> <td>IMPALAD, STATESTORE, CATALOGSERVER</td> </tr> <tr> <td>HIVE</td> <td>HIVESERVER2, HIVEMETASTORE, WEBHCAT, GATEWAY</td> </tr> <tr> <td>SOLR</td> <td>SOLR_SERVER, GATEWAY</td> </tr> <tr> <td>SQOOP</td> <td>SQOOP_SERVER</td> </tr> <tr> <td>SQOOP_CLIENT</td> <td>GATEWAY</td> </tr> <tr> <td>SENTRY</td> <td>SENTRY_SERVER</td> </tr> <tr> <td>ACCUMULO16</td> <td>GARBAGE_COLLECTOR, GATEWAY, ACCUMULO16_MASTER, MONITOR, ACCUMULO16_TSERVER, TRACER</td> </tr> <tr> <td>KMS</td> <td>KMS</td> </tr> <tr> <td>KS_INDEXER</td> <td>HBASE_INDEXER</td> </tr> <tr> <td>SPARK_ON_YARN</td> <td>GATEWAY, SPARK_YARN_HISTORY_SERVER</td> </tr> </tbody>  </table>  When specifying roles to be created, the names provided for each role must not conflict with the names that CM auto-generates for roles. Specifically, names of the form \"<service name>-<role type>-<arbitrary value>\" cannot be used unless the <arbitrary value> is the same one CM would use. If CM detects such a conflict, the error message will indicate what <arbitrary value> is safe to use. Alternately, a differently formatted name should be used.  Since API v6: The role name can be left blank to allow CM to generate the name.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
body = cm_client.ApiRoleList() # ApiRoleList | Roles to create. (optional)

try:
    # Create new roles in a given service.
    api_response = api_instance.create_roles(cluster_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->create_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **body** | [**ApiRoleList**](ApiRoleList.md)| Roles to create. | [optional] 

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="delete_role" id="delete_role"></a>
# **delete_role**
> ApiRole delete_role(cluster_name, role_name, service_name)

Deletes a role from a given service.

Deletes a role from a given service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role name.
service_name = 'service_name_example' # str | 

try:
    # Deletes a role from a given service.
    api_response = api_instance.delete_role(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->delete_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role name. | 
 **service_name** | **str**|  | 

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="enter_maintenance_mode" id="enter_maintenance_mode"></a>
# **enter_maintenance_mode**
> ApiCommand enter_maintenance_mode(cluster_name, role_name, service_name)

Put the role into maintenance mode.

Put the role into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role name.
service_name = 'service_name_example' # str | 

try:
    # Put the role into maintenance mode.
    api_response = api_instance.enter_maintenance_mode(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->enter_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role name. | 
 **service_name** | **str**|  | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="exit_maintenance_mode" id="exit_maintenance_mode"></a>
# **exit_maintenance_mode**
> ApiCommand exit_maintenance_mode(cluster_name, role_name, service_name)

Take the role out of maintenance mode.

Take the role out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role name.
service_name = 'service_name_example' # str | 

try:
    # Take the role out of maintenance mode.
    api_response = api_instance.exit_maintenance_mode(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->exit_maintenance_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role name. | 
 **service_name** | **str**|  | 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_full_log" id="get_full_log"></a>
# **get_full_log**
> str get_full_log(cluster_name, role_name, service_name)

Retrieves the log file for the role's main process.

Retrieves the log file for the role's main process. <p> If the role is not started, this will be the log file associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to fetch logs from.
service_name = 'service_name_example' # str | 

try:
    # Retrieves the log file for the role's main process.
    api_response = api_instance.get_full_log(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->get_full_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to fetch logs from. | 
 **service_name** | **str**|  | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_metrics" id="get_metrics"></a>
# **get_metrics**
> ApiMetricList get_metrics(cluster_name, role_name, service_name, _from=_from, metrics=metrics, to=to, view=view)

Fetch metric readings for a particular role.

Fetch metric readings for a particular role. <p> By default, this call will look up all metrics available for the role. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the role, even if no readings are available in the requested window.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The name of the role.
service_name = 'service_name_example' # str | 
_from = '_from_example' # str | Start of the period to query. (optional)
metrics = ['metrics_example'] # list[str] | Filter for which metrics to query. (optional)
to = 'now' # str | End of the period to query. (optional) (default to now)
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Fetch metric readings for a particular role.
    api_response = api_instance.get_metrics(cluster_name, role_name, service_name, _from=_from, metrics=metrics, to=to, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->get_metrics: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The name of the role. | 
 **service_name** | **str**|  | 
 **_from** | **str**| Start of the period to query. | [optional] 
 **metrics** | [**list[str]**](str.md)| Filter for which metrics to query. | [optional] 
 **to** | **str**| End of the period to query. | [optional] [default to now]
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_stacks_log" id="get_stacks_log"></a>
# **get_stacks_log**
> str get_stacks_log(cluster_name, role_name, service_name)

Retrieves the stacks log file, if any, for the role's main process.

Retrieves the stacks log file, if any, for the role's main process. Note that not all roles support periodic stacks collection.  The log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to fetch stacks logs from.
service_name = 'service_name_example' # str | 

try:
    # Retrieves the stacks log file, if any, for the role's main process.
    api_response = api_instance.get_stacks_log(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->get_stacks_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to fetch stacks logs from. | 
 **service_name** | **str**|  | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_stacks_logs_bundle" id="get_stacks_logs_bundle"></a>
# **get_stacks_logs_bundle**
> get_stacks_logs_bundle(cluster_name, role_name, service_name)

Download a zip-compressed archive of role stacks logs.

Download a zip-compressed archive of role stacks logs. Note that not all roles support periodic stacks collection.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to fetch the stacks logs bundle from.
service_name = 'service_name_example' # str | 

try:
    # Download a zip-compressed archive of role stacks logs.
    api_instance.get_stacks_logs_bundle(cluster_name, role_name, service_name)
except ApiException as e:
    print("Exception when calling RolesResourceApi->get_stacks_logs_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to fetch the stacks logs bundle from. | 
 **service_name** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_standard_error" id="get_standard_error"></a>
# **get_standard_error**
> str get_standard_error(cluster_name, role_name, service_name)

Retrieves the role's standard error output.

Retrieves the role's standard error output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to fetch stderr from.
service_name = 'service_name_example' # str | 

try:
    # Retrieves the role's standard error output.
    api_response = api_instance.get_standard_error(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->get_standard_error: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to fetch stderr from. | 
 **service_name** | **str**|  | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="get_standard_output" id="get_standard_output"></a>
# **get_standard_output**
> str get_standard_output(cluster_name, role_name, service_name)

Retrieves the role's standard output.

Retrieves the role's standard output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to fetch stdout from.
service_name = 'service_name_example' # str | 

try:
    # Retrieves the role's standard output.
    api_response = api_instance.get_standard_output(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->get_standard_output: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to fetch stdout from. | 
 **service_name** | **str**|  | 

### Return type

**str**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="impala_diagnostics" id="impala_diagnostics"></a>
# **impala_diagnostics**
> ApiCommand impala_diagnostics(cluster_name, role_name, service_name, body=body)

Collects diagnostics data for an Impala role.

Collects diagnostics data for an Impala role.  <p> Available since API v31. </p>

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role name.
service_name = 'service_name_example' # str | 
body = cm_client.ApiImpalaRoleDiagnosticsArgs() # ApiImpalaRoleDiagnosticsArgs |  (optional)

try:
    # Collects diagnostics data for an Impala role.
    api_response = api_instance.impala_diagnostics(cluster_name, role_name, service_name, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->impala_diagnostics: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role name. | 
 **service_name** | **str**|  | 
 **body** | [**ApiImpalaRoleDiagnosticsArgs**](ApiImpalaRoleDiagnosticsArgs.md)|  | [optional] 

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_active_commands" id="list_active_commands"></a>
# **list_active_commands**
> ApiCommandList list_active_commands(cluster_name, role_name, service_name, view=view)

List active role commands.

List active role commands.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to start.
service_name = 'service_name_example' # str | 
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # List active role commands.
    api_response = api_instance.list_active_commands(cluster_name, role_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->list_active_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to start. | 
 **service_name** | **str**|  | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list_commands" id="list_commands"></a>
# **list_commands**
> ApiCommandMetadataList list_commands(cluster_name, role_name, service_name)

Lists all the commands that can be executed by name on the provided role.

Lists all the commands that can be executed by name on the provided role.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | the role name.
service_name = 'service_name_example' # str | 

try:
    # Lists all the commands that can be executed by name on the provided role.
    api_response = api_instance.list_commands(cluster_name, role_name, service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->list_commands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| the role name. | 
 **service_name** | **str**|  | 

### Return type

[**ApiCommandMetadataList**](ApiCommandMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role" id="read_role"></a>
# **read_role**
> ApiRole read_role(cluster_name, role_name, service_name, view=view)

Retrieves detailed information about a role.

Retrieves detailed information about a role.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role name.
service_name = 'service_name_example' # str | 
view = 'full' # str | The view to materialize. Defaults to 'full'. (optional) (default to full)

try:
    # Retrieves detailed information about a role.
    api_response = api_instance.read_role(cluster_name, role_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->read_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role name. | 
 **service_name** | **str**|  | 
 **view** | **str**| The view to materialize. Defaults to 'full'. | [optional] [default to full]

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_role_config" id="read_role_config"></a>
# **read_role_config**
> ApiConfigList read_role_config(cluster_name, role_name, service_name, view=view)

Retrieves the configuration of a specific role.

Retrieves the configuration of a specific role. Note that the \"full\" view performs validation on the configuration, which could take a few seconds on a large cluster (around 500 nodes or more).

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to look up.
service_name = 'service_name_example' # str | 
view = 'summary' # str | The view of the data to materialize, either \"summary\" or \"full\". (optional) (default to summary)

try:
    # Retrieves the configuration of a specific role.
    api_response = api_instance.read_role_config(cluster_name, role_name, service_name, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->read_role_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to look up. | 
 **service_name** | **str**|  | 
 **view** | **str**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read_roles" id="read_roles"></a>
# **read_roles**
> ApiRoleList read_roles(cluster_name, service_name, filter=filter, view=view)

Lists all roles of a given service.

Lists all roles of a given service.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
service_name = 'service_name_example' # str | 
filter = '' # str | Optional query to filter the roles by. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: hostname==host1.abc.com;type==DATANODE </p>  Currently supports filtering by: <ul> <li>hostname: The hostname of the host the role is running on.</li> <li>hostId: The unique identifier of the host the role is running on.</li> <li>type: The role's type.</li> </ul> (optional) (default to )
view = 'summary' # str | DataView for getting roles. Defaults to 'summary'. (optional) (default to summary)

try:
    # Lists all roles of a given service.
    api_response = api_instance.read_roles(cluster_name, service_name, filter=filter, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->read_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **service_name** | **str**|  | 
 **filter** | **str**| Optional query to filter the roles by. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: hostname==host1.abc.com;type==DATANODE </p>  Currently supports filtering by: <ul> <li>hostname: The hostname of the host the role is running on.</li> <li>hostId: The unique identifier of the host the role is running on.</li> <li>type: The role's type.</li> </ul> | [optional] [default to ]
 **view** | **str**| DataView for getting roles. Defaults to 'summary'. | [optional] [default to summary]

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update_role_config" id="update_role_config"></a>
# **update_role_config**
> ApiConfigList update_role_config(cluster_name, role_name, service_name, message=message, body=body)

Updates the role configuration with the given values.

Updates the role configuration with the given values. <p> If a value is set in the given configuration, it will be added to the role's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```python
from __future__ import print_function
import time
import cm_client
from cm_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = cm_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = cm_client.RolesResourceApi(cm_client.ApiClient(configuration))
cluster_name = 'cluster_name_example' # str | 
role_name = 'role_name_example' # str | The role to modify.
service_name = 'service_name_example' # str | 
message = 'message_example' # str | Optional message describing the changes. (optional)
body = cm_client.ApiConfigList() # ApiConfigList | Configuration changes. (optional)

try:
    # Updates the role configuration with the given values.
    api_response = api_instance.update_role_config(cluster_name, role_name, service_name, message=message, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesResourceApi->update_role_config: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cluster_name** | **str**|  | 
 **role_name** | **str**| The role to modify. | 
 **service_name** | **str**|  | 
 **message** | **str**| Optional message describing the changes. | [optional] 
 **body** | [**ApiConfigList**](ApiConfigList.md)| Configuration changes. | [optional] 

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

