# ApiParcelState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**progress** | **float** | The progress of the state transition. | [optional] 
**total_progress** | **float** | The total amount that #getProgress() needs to get to. | [optional] 
**count** | **float** | The current hosts that have completed. | [optional] 
**total_count** | **float** | The total amount that #getCount() needs to get to. | [optional] 
**errors** | **list[str]** | The errors that exist for this parcel. | [optional] 
**warnings** | **list[str]** | The warnings that exist for this parcel. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


