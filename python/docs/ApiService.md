# ApiService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the service. | [optional] 
**type** | **str** | The type of the service, e.g. HDFS, MAPREDUCE, HBASE. | [optional] 
**cluster_ref** | [**ApiClusterRef**](ApiClusterRef.md) | Readonly. A reference to the enclosing cluster. | [optional] 
**service_state** | [**ApiServiceState**](ApiServiceState.md) | Readonly. The configured run state of this service. Whether it&#39;s running, etc. | [optional] 
**health_summary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Readonly. The high-level health status of this service. | [optional] 
**config_stale** | **bool** | Readonly. Expresses whether the service configuration is stale. | [optional] 
**config_staleness_status** | [**ApiConfigStalenessStatus**](ApiConfigStalenessStatus.md) | Readonly. Expresses the service&#39;s configuration staleness status which is based on the staleness status of its roles. Available since API v6. | [optional] 
**client_config_staleness_status** | [**ApiConfigStalenessStatus**](ApiConfigStalenessStatus.md) | Readonly. Expresses the service&#39;s client configuration staleness status which is marked as stale if any of the service&#39;s hosts have missing client configurations or if any of the deployed client configurations are stale. Available since API v6. | [optional] 
**health_checks** | [**list[ApiHealthCheck]**](ApiHealthCheck.md) | Readonly. The list of health checks of this service. | [optional] 
**service_url** | **str** | Readonly. Link into the Cloudera Manager web UI for this specific service. | [optional] 
**role_instances_url** | **str** | Readonly. Link into the Cloudera Manager web UI for role instances table for this specific service. Available since API v11. | [optional] 
**maintenance_mode** | **bool** | Readonly. Whether the service is in maintenance mode. Available since API v2. | [optional] 
**maintenance_owners** | [**list[ApiEntityType]**](ApiEntityType.md) | Readonly. The list of objects that trigger this service to be in maintenance mode. Available since API v2. | [optional] 
**config** | [**ApiServiceConfig**](ApiServiceConfig.md) | Configuration of the service being created. Optional. | [optional] 
**roles** | [**list[ApiRole]**](ApiRole.md) | The list of service roles. Optional. | [optional] 
**display_name** | **str** | The display name for the service that is shown in the UI. Available since API v2. | [optional] 
**role_config_groups** | [**list[ApiRoleConfigGroup]**](ApiRoleConfigGroup.md) | The list of role configuration groups in this service. Optional. Available since API v3. | [optional] 
**replication_schedules** | [**list[ApiReplicationSchedule]**](ApiReplicationSchedule.md) | The list of replication schedules for this service. Optional. Available since API v6. | [optional] 
**snapshot_policies** | [**list[ApiSnapshotPolicy]**](ApiSnapshotPolicy.md) | The list of snapshot policies for this service. Optional. Available since API v6. | [optional] 
**entity_status** | [**ApiEntityStatus**](ApiEntityStatus.md) | Readonly. The entity status for this service. Available since API v11. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


