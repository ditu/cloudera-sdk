# ApiDisableLlamaHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_name** | **str** | Name of the Llama role that will be active after HA is disabled. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


