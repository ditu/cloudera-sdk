# ApiExternalUserMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the external mapping | [optional] 
**type** | [**ApiExternalUserMappingType**](ApiExternalUserMappingType.md) | The type of the external mapping | [optional] 
**uuid** | **str** | Readonly. The UUID of the authRole. &lt;p&gt; | [optional] 
**auth_roles** | [**list[ApiAuthRoleRef]**](ApiAuthRoleRef.md) | A list of ApiAuthRole that this user possesses. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


