# ApiAuthRoleRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **str** | The name of the authRole. | [optional] 
**uuid** | **str** | The uuid of the authRole, which uniquely identifies it in a CM installation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


