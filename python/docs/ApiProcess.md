# ApiProcess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config_files** | **list[str]** | List of config files supplied to the process. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


