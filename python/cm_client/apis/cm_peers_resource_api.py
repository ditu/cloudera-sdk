# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..configuration import Configuration
from ..api_client import ApiClient


class CmPeersResourceApi(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client

    def create_peer(self, **kwargs):
        """
        Create a new Cloudera Manager peer.
        Create a new Cloudera Manager peer. <p> The remote server will be contacted so that a user can be created for use by the new peer. The <i>username</i> and <i>password</i> properties of the provided peer object should contain credentials of a valid admin user on the remote server. A timeout of 10 seconds is enforced when contacting the remote server. <p> It is recommended to run the remote server with TLS enabled, since creating and using peers involve transferring credentials over the network. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> Type field in ApiCmPeer is available since API v11. if not specified when making createPeer() call, 'REPLICATION' type peer will be created.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.create_peer(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param ApiCmPeer body: Peer to create (see above).
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.create_peer_with_http_info(**kwargs)
        else:
            (data) = self.create_peer_with_http_info(**kwargs)
            return data

    def create_peer_with_http_info(self, **kwargs):
        """
        Create a new Cloudera Manager peer.
        Create a new Cloudera Manager peer. <p> The remote server will be contacted so that a user can be created for use by the new peer. The <i>username</i> and <i>password</i> properties of the provided peer object should contain credentials of a valid admin user on the remote server. A timeout of 10 seconds is enforced when contacting the remote server. <p> It is recommended to run the remote server with TLS enabled, since creating and using peers involve transferring credentials over the network. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> Type field in ApiCmPeer is available since API v11. if not specified when making createPeer() call, 'REPLICATION' type peer will be created.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.create_peer_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param ApiCmPeer body: Peer to create (see above).
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_peer" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/cm/peers', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiCmPeer',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_peer(self, peer_name, **kwargs):
        """
        Delete Cloudera Manager peer.
        Delete Cloudera Manager peer. <p> An attempt will be made to contact the peer server, so that the configured user can be deleted.. Errors while contacting the remote server are non-fatal. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.delete_peer(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to delete. (required)
        :param str type: Type of peer to delete. If null, REPLICATION peer type will be deleted.
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.delete_peer_with_http_info(peer_name, **kwargs)
        else:
            (data) = self.delete_peer_with_http_info(peer_name, **kwargs)
            return data

    def delete_peer_with_http_info(self, peer_name, **kwargs):
        """
        Delete Cloudera Manager peer.
        Delete Cloudera Manager peer. <p> An attempt will be made to contact the peer server, so that the configured user can be deleted.. Errors while contacting the remote server are non-fatal. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.delete_peer_with_http_info(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to delete. (required)
        :param str type: Type of peer to delete. If null, REPLICATION peer type will be deleted.
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['peer_name', 'type']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_peer" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'peer_name' is set
        if ('peer_name' not in params) or (params['peer_name'] is None):
            raise ValueError("Missing the required parameter `peer_name` when calling `delete_peer`")


        collection_formats = {}

        path_params = {}
        if 'peer_name' in params:
            path_params['peerName'] = params['peer_name']

        query_params = []
        if 'type' in params:
            query_params.append(('type', params['type']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/cm/peers/{peerName}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiCmPeer',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def list_peers(self, **kwargs):
        """
        Retrieves all configured Cloudera Manager peers.
        Retrieves all configured Cloudera Manager peers. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> When accessed via API version before v11, only REPLICATION type peers will be returned.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.list_peers(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiCmPeerList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.list_peers_with_http_info(**kwargs)
        else:
            (data) = self.list_peers_with_http_info(**kwargs)
            return data

    def list_peers_with_http_info(self, **kwargs):
        """
        Retrieves all configured Cloudera Manager peers.
        Retrieves all configured Cloudera Manager peers. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> When accessed via API version before v11, only REPLICATION type peers will be returned.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.list_peers_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiCmPeerList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_peers" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/cm/peers', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiCmPeerList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def read_peer(self, peer_name, **kwargs):
        """
        Fetch information about an existing Cloudera Manager peer.
        Fetch information about an existing Cloudera Manager peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_peer(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to retrieve. (required)
        :param str type: Type of peer to retrieve. If null, REPLICATION peer type will be returned.
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.read_peer_with_http_info(peer_name, **kwargs)
        else:
            (data) = self.read_peer_with_http_info(peer_name, **kwargs)
            return data

    def read_peer_with_http_info(self, peer_name, **kwargs):
        """
        Fetch information about an existing Cloudera Manager peer.
        Fetch information about an existing Cloudera Manager peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_peer_with_http_info(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to retrieve. (required)
        :param str type: Type of peer to retrieve. If null, REPLICATION peer type will be returned.
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['peer_name', 'type']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method read_peer" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'peer_name' is set
        if ('peer_name' not in params) or (params['peer_name'] is None):
            raise ValueError("Missing the required parameter `peer_name` when calling `read_peer`")


        collection_formats = {}

        path_params = {}
        if 'peer_name' in params:
            path_params['peerName'] = params['peer_name']

        query_params = []
        if 'type' in params:
            query_params.append(('type', params['type']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/cm/peers/{peerName}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiCmPeer',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def test_peer(self, peer_name, **kwargs):
        """
        Test the connectivity of a peer.
        Test the connectivity of a peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.test_peer(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to test. (required)
        :param str type: Type of peer to test. If null, REPLICATION peer type will be tested.
        :return: ApiCommand
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.test_peer_with_http_info(peer_name, **kwargs)
        else:
            (data) = self.test_peer_with_http_info(peer_name, **kwargs)
            return data

    def test_peer_with_http_info(self, peer_name, **kwargs):
        """
        Test the connectivity of a peer.
        Test the connectivity of a peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.test_peer_with_http_info(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to test. (required)
        :param str type: Type of peer to test. If null, REPLICATION peer type will be tested.
        :return: ApiCommand
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['peer_name', 'type']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method test_peer" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'peer_name' is set
        if ('peer_name' not in params) or (params['peer_name'] is None):
            raise ValueError("Missing the required parameter `peer_name` when calling `test_peer`")


        collection_formats = {}

        path_params = {}
        if 'peer_name' in params:
            path_params['peerName'] = params['peer_name']

        query_params = []
        if 'type' in params:
            query_params.append(('type', params['type']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/cm/peers/{peerName}/commands/test', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiCommand',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def update_peer(self, peer_name, **kwargs):
        """
        Update information for a Cloudera Manager peer.
        Update information for a Cloudera Manager peer. <p> In administrator credentials are provided in the peer information, they will be used to establish new credentials with the remote server. This can be used in case the old credentials are not working anymore. An attempt will be made to delete the old credentials if new ones are successfully created. <p> If changing the peer's URL, an attempt will be made to contact the old Cloudera Manager to delete the existing credentials. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.update_peer(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to update. (required)
        :param ApiCmPeer body: Updated peer information.
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.update_peer_with_http_info(peer_name, **kwargs)
        else:
            (data) = self.update_peer_with_http_info(peer_name, **kwargs)
            return data

    def update_peer_with_http_info(self, peer_name, **kwargs):
        """
        Update information for a Cloudera Manager peer.
        Update information for a Cloudera Manager peer. <p> In administrator credentials are provided in the peer information, they will be used to establish new credentials with the remote server. This can be used in case the old credentials are not working anymore. An attempt will be made to delete the old credentials if new ones are successfully created. <p> If changing the peer's URL, an attempt will be made to contact the old Cloudera Manager to delete the existing credentials. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.update_peer_with_http_info(peer_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str peer_name: Name of peer to update. (required)
        :param ApiCmPeer body: Updated peer information.
        :return: ApiCmPeer
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['peer_name', 'body']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method update_peer" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'peer_name' is set
        if ('peer_name' not in params) or (params['peer_name'] is None):
            raise ValueError("Missing the required parameter `peer_name` when calling `update_peer`")


        collection_formats = {}

        path_params = {}
        if 'peer_name' in params:
            path_params['peerName'] = params['peer_name']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/cm/peers/{peerName}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiCmPeer',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)
