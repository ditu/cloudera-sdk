# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..configuration import Configuration
from ..api_client import ApiClient


class TimeSeriesResourceApi(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client

    def get_entity_type_attributes(self, **kwargs):
        """
        Retrieve all metric entity type attributes monitored by Cloudera Manager.
        Retrieve all metric entity type attributes monitored by Cloudera Manager. <p/> Available since API v11.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_entity_type_attributes(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiTimeSeriesEntityAttributeList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.get_entity_type_attributes_with_http_info(**kwargs)
        else:
            (data) = self.get_entity_type_attributes_with_http_info(**kwargs)
            return data

    def get_entity_type_attributes_with_http_info(self, **kwargs):
        """
        Retrieve all metric entity type attributes monitored by Cloudera Manager.
        Retrieve all metric entity type attributes monitored by Cloudera Manager. <p/> Available since API v11.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_entity_type_attributes_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiTimeSeriesEntityAttributeList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_entity_type_attributes" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/timeseries/entityTypeAttributes', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiTimeSeriesEntityAttributeList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_entity_types(self, **kwargs):
        """
        Retrieve all metric entity types monitored by Cloudera Manager.
        Retrieve all metric entity types monitored by Cloudera Manager. It is guaranteed that parent types appear before their children. <p/> Available since API v11.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_entity_types(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiTimeSeriesEntityTypeList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.get_entity_types_with_http_info(**kwargs)
        else:
            (data) = self.get_entity_types_with_http_info(**kwargs)
            return data

    def get_entity_types_with_http_info(self, **kwargs):
        """
        Retrieve all metric entity types monitored by Cloudera Manager.
        Retrieve all metric entity types monitored by Cloudera Manager. It is guaranteed that parent types appear before their children. <p/> Available since API v11.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_entity_types_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiTimeSeriesEntityTypeList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_entity_types" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/timeseries/entityTypes', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiTimeSeriesEntityTypeList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_metric_schema(self, **kwargs):
        """
        Retrieve schema for all metrics.
        Retrieve schema for all metrics <p/> The schema is fixed for a product version. The schema may change for an API versions <p/> Available since API v4.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_metric_schema(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiMetricSchemaList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.get_metric_schema_with_http_info(**kwargs)
        else:
            (data) = self.get_metric_schema_with_http_info(**kwargs)
            return data

    def get_metric_schema_with_http_info(self, **kwargs):
        """
        Retrieve schema for all metrics.
        Retrieve schema for all metrics <p/> The schema is fixed for a product version. The schema may change for an API versions <p/> Available since API v4.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_metric_schema_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :return: ApiMetricSchemaList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_metric_schema" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/timeseries/schema', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiMetricSchemaList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def query_time_series(self, **kwargs):
        """
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.  Please see the <a href=\"http://tiny.cloudera.com/cm_tsquery\"> tsquery language documentation</a>. <p/> Available since API v6.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.query_time_series(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str content_type: to return the response in. The content types \"application/json\" and \"text/csv\" are supported. This defaults to \"application/json\". If \"text/csv\" is specified then we return one row per time series data point, and we don't return any of the metadata.
        :param str desired_rollup: Aggregate rollup level desired for the response data. Valid values are RAW, TEN_MINUTELY, HOURLY, SIX_HOURLY, DAILY, and WEEKLY. Note that if the mustUseDesiredRollup parameter is not set, then the monitoring server can decide to return a different rollup level.
        :param str _from: Start of the period to query in ISO 8601 format (defaults to 5 minutes before the end of the period).
        :param bool must_use_desired_rollup: If set then the tsquery will return data with the desired aggregate rollup level.
        :param str query: Tsquery to run against the CM time-series data store.
        :param str to: End of the period to query in ISO 8601 format (defaults to current time).
        :return: ApiTimeSeriesResponseList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.query_time_series_with_http_info(**kwargs)
        else:
            (data) = self.query_time_series_with_http_info(**kwargs)
            return data

    def query_time_series_with_http_info(self, **kwargs):
        """
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.  Please see the <a href=\"http://tiny.cloudera.com/cm_tsquery\"> tsquery language documentation</a>. <p/> Available since API v6.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.query_time_series_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str content_type: to return the response in. The content types \"application/json\" and \"text/csv\" are supported. This defaults to \"application/json\". If \"text/csv\" is specified then we return one row per time series data point, and we don't return any of the metadata.
        :param str desired_rollup: Aggregate rollup level desired for the response data. Valid values are RAW, TEN_MINUTELY, HOURLY, SIX_HOURLY, DAILY, and WEEKLY. Note that if the mustUseDesiredRollup parameter is not set, then the monitoring server can decide to return a different rollup level.
        :param str _from: Start of the period to query in ISO 8601 format (defaults to 5 minutes before the end of the period).
        :param bool must_use_desired_rollup: If set then the tsquery will return data with the desired aggregate rollup level.
        :param str query: Tsquery to run against the CM time-series data store.
        :param str to: End of the period to query in ISO 8601 format (defaults to current time).
        :return: ApiTimeSeriesResponseList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['content_type', 'desired_rollup', '_from', 'must_use_desired_rollup', 'query', 'to']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method query_time_series" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'content_type' in params:
            query_params.append(('contentType', params['content_type']))
        if 'desired_rollup' in params:
            query_params.append(('desiredRollup', params['desired_rollup']))
        if '_from' in params:
            query_params.append(('from', params['_from']))
        if 'must_use_desired_rollup' in params:
            query_params.append(('mustUseDesiredRollup', params['must_use_desired_rollup']))
        if 'query' in params:
            query_params.append(('query', params['query']))
        if 'to' in params:
            query_params.append(('to', params['to']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/timeseries', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiTimeSeriesResponseList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def query_time_series_0(self, **kwargs):
        """
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request. This method differs from queryTimeSeries() in v6 that this could accept query strings that are longer than HTTP GET request limit.  Available since API v11.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.query_time_series_0(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param ApiTimeSeriesRequest body: Request object containing information used when retrieving timeseries data.
        :return: ApiTimeSeriesResponseList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.query_time_series_0_with_http_info(**kwargs)
        else:
            (data) = self.query_time_series_0_with_http_info(**kwargs)
            return data

    def query_time_series_0_with_http_info(self, **kwargs):
        """
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.
        Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request. This method differs from queryTimeSeries() in v6 that this could accept query strings that are longer than HTTP GET request limit.  Available since API v11.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.query_time_series_0_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param ApiTimeSeriesRequest body: Request object containing information used when retrieving timeseries data.
        :return: ApiTimeSeriesResponseList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method query_time_series_0" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/timeseries', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiTimeSeriesResponseList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)
