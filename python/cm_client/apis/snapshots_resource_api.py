# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..configuration import Configuration
from ..api_client import ApiClient


class SnapshotsResourceApi(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client

    def create_policies(self, cluster_name, service_name, **kwargs):
        """
        Creates one or more snapshot policies.
        Creates one or more snapshot policies.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.create_policies(cluster_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str service_name:  (required)
        :param ApiSnapshotPolicyList body: List of the snapshot policies to create.
        :return: ApiSnapshotPolicyList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.create_policies_with_http_info(cluster_name, service_name, **kwargs)
        else:
            (data) = self.create_policies_with_http_info(cluster_name, service_name, **kwargs)
            return data

    def create_policies_with_http_info(self, cluster_name, service_name, **kwargs):
        """
        Creates one or more snapshot policies.
        Creates one or more snapshot policies.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.create_policies_with_http_info(cluster_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str service_name:  (required)
        :param ApiSnapshotPolicyList body: List of the snapshot policies to create.
        :return: ApiSnapshotPolicyList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_name', 'service_name', 'body']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_policies" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_name' is set
        if ('cluster_name' not in params) or (params['cluster_name'] is None):
            raise ValueError("Missing the required parameter `cluster_name` when calling `create_policies`")
        # verify the required parameter 'service_name' is set
        if ('service_name' not in params) or (params['service_name'] is None):
            raise ValueError("Missing the required parameter `service_name` when calling `create_policies`")


        collection_formats = {}

        path_params = {}
        if 'cluster_name' in params:
            path_params['clusterName'] = params['cluster_name']
        if 'service_name' in params:
            path_params['serviceName'] = params['service_name']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/clusters/{clusterName}/services/{serviceName}/snapshots/policies', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiSnapshotPolicyList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_policy(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Deletes an existing snapshot policy.
        Deletes an existing snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.delete_policy(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :return: ApiSnapshotPolicy
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.delete_policy_with_http_info(cluster_name, policy_name, service_name, **kwargs)
        else:
            (data) = self.delete_policy_with_http_info(cluster_name, policy_name, service_name, **kwargs)
            return data

    def delete_policy_with_http_info(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Deletes an existing snapshot policy.
        Deletes an existing snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.delete_policy_with_http_info(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :return: ApiSnapshotPolicy
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_name', 'policy_name', 'service_name']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_policy" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_name' is set
        if ('cluster_name' not in params) or (params['cluster_name'] is None):
            raise ValueError("Missing the required parameter `cluster_name` when calling `delete_policy`")
        # verify the required parameter 'policy_name' is set
        if ('policy_name' not in params) or (params['policy_name'] is None):
            raise ValueError("Missing the required parameter `policy_name` when calling `delete_policy`")
        # verify the required parameter 'service_name' is set
        if ('service_name' not in params) or (params['service_name'] is None):
            raise ValueError("Missing the required parameter `service_name` when calling `delete_policy`")


        collection_formats = {}

        path_params = {}
        if 'cluster_name' in params:
            path_params['clusterName'] = params['cluster_name']
        if 'policy_name' in params:
            path_params['policyName'] = params['policy_name']
        if 'service_name' in params:
            path_params['serviceName'] = params['service_name']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiSnapshotPolicy',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def read_history(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Returns a list of commands triggered by a snapshot policy.
        Returns a list of commands triggered by a snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_history(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :param int limit: Maximum number of commands to retrieve.
        :param int offset: Index of first command to retrieve.
        :param str view: The view to materialize.
        :return: ApiSnapshotCommandList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.read_history_with_http_info(cluster_name, policy_name, service_name, **kwargs)
        else:
            (data) = self.read_history_with_http_info(cluster_name, policy_name, service_name, **kwargs)
            return data

    def read_history_with_http_info(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Returns a list of commands triggered by a snapshot policy.
        Returns a list of commands triggered by a snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_history_with_http_info(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :param int limit: Maximum number of commands to retrieve.
        :param int offset: Index of first command to retrieve.
        :param str view: The view to materialize.
        :return: ApiSnapshotCommandList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_name', 'policy_name', 'service_name', 'limit', 'offset', 'view']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method read_history" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_name' is set
        if ('cluster_name' not in params) or (params['cluster_name'] is None):
            raise ValueError("Missing the required parameter `cluster_name` when calling `read_history`")
        # verify the required parameter 'policy_name' is set
        if ('policy_name' not in params) or (params['policy_name'] is None):
            raise ValueError("Missing the required parameter `policy_name` when calling `read_history`")
        # verify the required parameter 'service_name' is set
        if ('service_name' not in params) or (params['service_name'] is None):
            raise ValueError("Missing the required parameter `service_name` when calling `read_history`")


        collection_formats = {}

        path_params = {}
        if 'cluster_name' in params:
            path_params['clusterName'] = params['cluster_name']
        if 'policy_name' in params:
            path_params['policyName'] = params['policy_name']
        if 'service_name' in params:
            path_params['serviceName'] = params['service_name']

        query_params = []
        if 'limit' in params:
            query_params.append(('limit', params['limit']))
        if 'offset' in params:
            query_params.append(('offset', params['offset']))
        if 'view' in params:
            query_params.append(('view', params['view']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName}/history', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiSnapshotCommandList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def read_policies(self, cluster_name, service_name, **kwargs):
        """
        Returns information for all snapshot policies.
        Returns information for all snapshot policies.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_policies(cluster_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str service_name:  (required)
        :param str view: The view to materialize.
        :return: ApiSnapshotPolicyList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.read_policies_with_http_info(cluster_name, service_name, **kwargs)
        else:
            (data) = self.read_policies_with_http_info(cluster_name, service_name, **kwargs)
            return data

    def read_policies_with_http_info(self, cluster_name, service_name, **kwargs):
        """
        Returns information for all snapshot policies.
        Returns information for all snapshot policies.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_policies_with_http_info(cluster_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str service_name:  (required)
        :param str view: The view to materialize.
        :return: ApiSnapshotPolicyList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_name', 'service_name', 'view']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method read_policies" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_name' is set
        if ('cluster_name' not in params) or (params['cluster_name'] is None):
            raise ValueError("Missing the required parameter `cluster_name` when calling `read_policies`")
        # verify the required parameter 'service_name' is set
        if ('service_name' not in params) or (params['service_name'] is None):
            raise ValueError("Missing the required parameter `service_name` when calling `read_policies`")


        collection_formats = {}

        path_params = {}
        if 'cluster_name' in params:
            path_params['clusterName'] = params['cluster_name']
        if 'service_name' in params:
            path_params['serviceName'] = params['service_name']

        query_params = []
        if 'view' in params:
            query_params.append(('view', params['view']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/clusters/{clusterName}/services/{serviceName}/snapshots/policies', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiSnapshotPolicyList',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def read_policy(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Returns information for a specific snapshot policy.
        Returns information for a specific snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_policy(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :param str view: The view to materialize.
        :return: ApiSnapshotPolicy
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.read_policy_with_http_info(cluster_name, policy_name, service_name, **kwargs)
        else:
            (data) = self.read_policy_with_http_info(cluster_name, policy_name, service_name, **kwargs)
            return data

    def read_policy_with_http_info(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Returns information for a specific snapshot policy.
        Returns information for a specific snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.read_policy_with_http_info(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :param str view: The view to materialize.
        :return: ApiSnapshotPolicy
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_name', 'policy_name', 'service_name', 'view']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method read_policy" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_name' is set
        if ('cluster_name' not in params) or (params['cluster_name'] is None):
            raise ValueError("Missing the required parameter `cluster_name` when calling `read_policy`")
        # verify the required parameter 'policy_name' is set
        if ('policy_name' not in params) or (params['policy_name'] is None):
            raise ValueError("Missing the required parameter `policy_name` when calling `read_policy`")
        # verify the required parameter 'service_name' is set
        if ('service_name' not in params) or (params['service_name'] is None):
            raise ValueError("Missing the required parameter `service_name` when calling `read_policy`")


        collection_formats = {}

        path_params = {}
        if 'cluster_name' in params:
            path_params['clusterName'] = params['cluster_name']
        if 'policy_name' in params:
            path_params['policyName'] = params['policy_name']
        if 'service_name' in params:
            path_params['serviceName'] = params['service_name']

        query_params = []
        if 'view' in params:
            query_params.append(('view', params['view']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiSnapshotPolicy',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def update_policy(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Updates an existing snapshot policy.
        Updates an existing snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.update_policy(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :param ApiSnapshotPolicy body: Modified policy.
        :return: ApiSnapshotPolicy
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.update_policy_with_http_info(cluster_name, policy_name, service_name, **kwargs)
        else:
            (data) = self.update_policy_with_http_info(cluster_name, policy_name, service_name, **kwargs)
            return data

    def update_policy_with_http_info(self, cluster_name, policy_name, service_name, **kwargs):
        """
        Updates an existing snapshot policy.
        Updates an existing snapshot policy.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.update_policy_with_http_info(cluster_name, policy_name, service_name, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str cluster_name:  (required)
        :param str policy_name: Name of an existing snapshot policy. (required)
        :param str service_name:  (required)
        :param ApiSnapshotPolicy body: Modified policy.
        :return: ApiSnapshotPolicy
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_name', 'policy_name', 'service_name', 'body']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method update_policy" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_name' is set
        if ('cluster_name' not in params) or (params['cluster_name'] is None):
            raise ValueError("Missing the required parameter `cluster_name` when calling `update_policy`")
        # verify the required parameter 'policy_name' is set
        if ('policy_name' not in params) or (params['policy_name'] is None):
            raise ValueError("Missing the required parameter `policy_name` when calling `update_policy`")
        # verify the required parameter 'service_name' is set
        if ('service_name' not in params) or (params['service_name'] is None):
            raise ValueError("Missing the required parameter `service_name` when calling `update_policy`")


        collection_formats = {}

        path_params = {}
        if 'cluster_name' in params:
            path_params['clusterName'] = params['cluster_name']
        if 'policy_name' in params:
            path_params['policyName'] = params['policy_name']
        if 'service_name' in params:
            path_params['serviceName'] = params['service_name']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['basic']

        return self.api_client.call_api('/clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ApiSnapshotPolicy',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)
