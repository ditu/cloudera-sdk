# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiSimpleRollingRestartClusterArgs(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'slave_batch_size': 'float',
        'sleep_seconds': 'float',
        'slave_fail_count_threshold': 'float'
    }

    attribute_map = {
        'slave_batch_size': 'slaveBatchSize',
        'sleep_seconds': 'sleepSeconds',
        'slave_fail_count_threshold': 'slaveFailCountThreshold'
    }

    def __init__(self, slave_batch_size=None, sleep_seconds=None, slave_fail_count_threshold=None):
        """
        ApiSimpleRollingRestartClusterArgs - a model defined in Swagger
        """

        self._slave_batch_size = None
        self._sleep_seconds = None
        self._slave_fail_count_threshold = None

        if slave_batch_size is not None:
          self.slave_batch_size = slave_batch_size
        if sleep_seconds is not None:
          self.sleep_seconds = sleep_seconds
        if slave_fail_count_threshold is not None:
          self.slave_fail_count_threshold = slave_fail_count_threshold

    @property
    def slave_batch_size(self):
        """
        Gets the slave_batch_size of this ApiSimpleRollingRestartClusterArgs.
        Number of hosts with slave roles to restart at a time. Must be greater than zero. Default is 1.

        :return: The slave_batch_size of this ApiSimpleRollingRestartClusterArgs.
        :rtype: float
        """
        return self._slave_batch_size

    @slave_batch_size.setter
    def slave_batch_size(self, slave_batch_size):
        """
        Sets the slave_batch_size of this ApiSimpleRollingRestartClusterArgs.
        Number of hosts with slave roles to restart at a time. Must be greater than zero. Default is 1.

        :param slave_batch_size: The slave_batch_size of this ApiSimpleRollingRestartClusterArgs.
        :type: float
        """

        self._slave_batch_size = slave_batch_size

    @property
    def sleep_seconds(self):
        """
        Gets the sleep_seconds of this ApiSimpleRollingRestartClusterArgs.
        Number of seconds to sleep between restarts of slave host batches. <p> Must be greater than or equal to 0. Default is 0.

        :return: The sleep_seconds of this ApiSimpleRollingRestartClusterArgs.
        :rtype: float
        """
        return self._sleep_seconds

    @sleep_seconds.setter
    def sleep_seconds(self, sleep_seconds):
        """
        Sets the sleep_seconds of this ApiSimpleRollingRestartClusterArgs.
        Number of seconds to sleep between restarts of slave host batches. <p> Must be greater than or equal to 0. Default is 0.

        :param sleep_seconds: The sleep_seconds of this ApiSimpleRollingRestartClusterArgs.
        :type: float
        """

        self._sleep_seconds = sleep_seconds

    @property
    def slave_fail_count_threshold(self):
        """
        Gets the slave_fail_count_threshold of this ApiSimpleRollingRestartClusterArgs.
        The threshold for number of slave host batches that are allowed to fail to restart before the entire command is considered failed. <p> Must be greater than or equal to 0. Default is 0. <p> This argument is for ADVANCED users only. </p>

        :return: The slave_fail_count_threshold of this ApiSimpleRollingRestartClusterArgs.
        :rtype: float
        """
        return self._slave_fail_count_threshold

    @slave_fail_count_threshold.setter
    def slave_fail_count_threshold(self, slave_fail_count_threshold):
        """
        Sets the slave_fail_count_threshold of this ApiSimpleRollingRestartClusterArgs.
        The threshold for number of slave host batches that are allowed to fail to restart before the entire command is considered failed. <p> Must be greater than or equal to 0. Default is 0. <p> This argument is for ADVANCED users only. </p>

        :param slave_fail_count_threshold: The slave_fail_count_threshold of this ApiSimpleRollingRestartClusterArgs.
        :type: float
        """

        self._slave_fail_count_threshold = slave_fail_count_threshold

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiSimpleRollingRestartClusterArgs):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
