# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiHostInstallArguments(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'host_names': 'list[str]',
        'ssh_port': 'float',
        'user_name': 'str',
        'password': 'str',
        'private_key': 'str',
        'passphrase': 'str',
        'parallel_install_count': 'float',
        'cm_repo_url': 'str',
        'gpg_key_custom_url': 'str',
        'java_install_strategy': 'str',
        'unlimited_jce': 'bool',
        'gpg_key_override_bundle': 'str'
    }

    attribute_map = {
        'host_names': 'hostNames',
        'ssh_port': 'sshPort',
        'user_name': 'userName',
        'password': 'password',
        'private_key': 'privateKey',
        'passphrase': 'passphrase',
        'parallel_install_count': 'parallelInstallCount',
        'cm_repo_url': 'cmRepoUrl',
        'gpg_key_custom_url': 'gpgKeyCustomUrl',
        'java_install_strategy': 'javaInstallStrategy',
        'unlimited_jce': 'unlimitedJCE',
        'gpg_key_override_bundle': 'gpgKeyOverrideBundle'
    }

    def __init__(self, host_names=None, ssh_port=None, user_name=None, password=None, private_key=None, passphrase=None, parallel_install_count=None, cm_repo_url=None, gpg_key_custom_url=None, java_install_strategy=None, unlimited_jce=None, gpg_key_override_bundle=None):
        """
        ApiHostInstallArguments - a model defined in Swagger
        """

        self._host_names = None
        self._ssh_port = None
        self._user_name = None
        self._password = None
        self._private_key = None
        self._passphrase = None
        self._parallel_install_count = None
        self._cm_repo_url = None
        self._gpg_key_custom_url = None
        self._java_install_strategy = None
        self._unlimited_jce = None
        self._gpg_key_override_bundle = None

        if host_names is not None:
          self.host_names = host_names
        if ssh_port is not None:
          self.ssh_port = ssh_port
        if user_name is not None:
          self.user_name = user_name
        if password is not None:
          self.password = password
        if private_key is not None:
          self.private_key = private_key
        if passphrase is not None:
          self.passphrase = passphrase
        if parallel_install_count is not None:
          self.parallel_install_count = parallel_install_count
        if cm_repo_url is not None:
          self.cm_repo_url = cm_repo_url
        if gpg_key_custom_url is not None:
          self.gpg_key_custom_url = gpg_key_custom_url
        if java_install_strategy is not None:
          self.java_install_strategy = java_install_strategy
        if unlimited_jce is not None:
          self.unlimited_jce = unlimited_jce
        if gpg_key_override_bundle is not None:
          self.gpg_key_override_bundle = gpg_key_override_bundle

    @property
    def host_names(self):
        """
        Gets the host_names of this ApiHostInstallArguments.
        List of hosts to configure for use with Cloudera Manager. A host may be specified by a hostname (FQDN) or an IP address.

        :return: The host_names of this ApiHostInstallArguments.
        :rtype: list[str]
        """
        return self._host_names

    @host_names.setter
    def host_names(self, host_names):
        """
        Sets the host_names of this ApiHostInstallArguments.
        List of hosts to configure for use with Cloudera Manager. A host may be specified by a hostname (FQDN) or an IP address.

        :param host_names: The host_names of this ApiHostInstallArguments.
        :type: list[str]
        """

        self._host_names = host_names

    @property
    def ssh_port(self):
        """
        Gets the ssh_port of this ApiHostInstallArguments.
        SSH port. If unset, defaults to 22.

        :return: The ssh_port of this ApiHostInstallArguments.
        :rtype: float
        """
        return self._ssh_port

    @ssh_port.setter
    def ssh_port(self, ssh_port):
        """
        Sets the ssh_port of this ApiHostInstallArguments.
        SSH port. If unset, defaults to 22.

        :param ssh_port: The ssh_port of this ApiHostInstallArguments.
        :type: float
        """

        self._ssh_port = ssh_port

    @property
    def user_name(self):
        """
        Gets the user_name of this ApiHostInstallArguments.
        The username used to authenticate with the hosts. Root access to your hosts is required to install Cloudera packages. The installer will connect to your hosts via SSH and log in either directly as root or as another user with password-less sudo privileges to become root.

        :return: The user_name of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._user_name

    @user_name.setter
    def user_name(self, user_name):
        """
        Sets the user_name of this ApiHostInstallArguments.
        The username used to authenticate with the hosts. Root access to your hosts is required to install Cloudera packages. The installer will connect to your hosts via SSH and log in either directly as root or as another user with password-less sudo privileges to become root.

        :param user_name: The user_name of this ApiHostInstallArguments.
        :type: str
        """

        self._user_name = user_name

    @property
    def password(self):
        """
        Gets the password of this ApiHostInstallArguments.
        The password used to authenticate with the hosts. Specify either this or a private key. For password-less login, use an empty string as password.

        :return: The password of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._password

    @password.setter
    def password(self, password):
        """
        Sets the password of this ApiHostInstallArguments.
        The password used to authenticate with the hosts. Specify either this or a private key. For password-less login, use an empty string as password.

        :param password: The password of this ApiHostInstallArguments.
        :type: str
        """

        self._password = password

    @property
    def private_key(self):
        """
        Gets the private_key of this ApiHostInstallArguments.
        The private key to authenticate with the hosts. Specify either this or a password. <br> The private key, if specified, needs to be a standard PEM-encoded key as a single string, with all line breaks replaced with the line-feed control character '\\n'. <br> A value will typically look like the following string: <br> -----BEGIN RSA PRIVATE KEY-----\\n[base-64 encoded key]\\n-----END RSA PRIVATE KEY----- <br>

        :return: The private_key of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._private_key

    @private_key.setter
    def private_key(self, private_key):
        """
        Sets the private_key of this ApiHostInstallArguments.
        The private key to authenticate with the hosts. Specify either this or a password. <br> The private key, if specified, needs to be a standard PEM-encoded key as a single string, with all line breaks replaced with the line-feed control character '\\n'. <br> A value will typically look like the following string: <br> -----BEGIN RSA PRIVATE KEY-----\\n[base-64 encoded key]\\n-----END RSA PRIVATE KEY----- <br>

        :param private_key: The private_key of this ApiHostInstallArguments.
        :type: str
        """

        self._private_key = private_key

    @property
    def passphrase(self):
        """
        Gets the passphrase of this ApiHostInstallArguments.
        The passphrase associated with the private key used to authenticate with the hosts (optional).

        :return: The passphrase of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._passphrase

    @passphrase.setter
    def passphrase(self, passphrase):
        """
        Sets the passphrase of this ApiHostInstallArguments.
        The passphrase associated with the private key used to authenticate with the hosts (optional).

        :param passphrase: The passphrase of this ApiHostInstallArguments.
        :type: str
        """

        self._passphrase = passphrase

    @property
    def parallel_install_count(self):
        """
        Gets the parallel_install_count of this ApiHostInstallArguments.
        Number of simultaneous installations. Defaults to 10. Running a large number of installations at once can consume large amounts of network bandwidth and other system resources.

        :return: The parallel_install_count of this ApiHostInstallArguments.
        :rtype: float
        """
        return self._parallel_install_count

    @parallel_install_count.setter
    def parallel_install_count(self, parallel_install_count):
        """
        Sets the parallel_install_count of this ApiHostInstallArguments.
        Number of simultaneous installations. Defaults to 10. Running a large number of installations at once can consume large amounts of network bandwidth and other system resources.

        :param parallel_install_count: The parallel_install_count of this ApiHostInstallArguments.
        :type: float
        """

        self._parallel_install_count = parallel_install_count

    @property
    def cm_repo_url(self):
        """
        Gets the cm_repo_url of this ApiHostInstallArguments.
        The Cloudera Manager repository URL to use (optional). Example for SLES, Redhat or Debian based distributions: https://archive.cloudera.com/cm6/6.0.0

        :return: The cm_repo_url of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._cm_repo_url

    @cm_repo_url.setter
    def cm_repo_url(self, cm_repo_url):
        """
        Sets the cm_repo_url of this ApiHostInstallArguments.
        The Cloudera Manager repository URL to use (optional). Example for SLES, Redhat or Debian based distributions: https://archive.cloudera.com/cm6/6.0.0

        :param cm_repo_url: The cm_repo_url of this ApiHostInstallArguments.
        :type: str
        """

        self._cm_repo_url = cm_repo_url

    @property
    def gpg_key_custom_url(self):
        """
        Gets the gpg_key_custom_url of this ApiHostInstallArguments.
        The Cloudera Manager public GPG key (optional). Example for SLES, Redhat or other RPM based distributions: https://archive.cloudera.com/cm5/redhat/5/x86_64/cm/RPM-GPG-KEY-cloudera Example for Ubuntu or other Debian based distributions: https://archive.cloudera.com/cm5/ubuntu/lucid/amd64/cm/archive.key

        :return: The gpg_key_custom_url of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._gpg_key_custom_url

    @gpg_key_custom_url.setter
    def gpg_key_custom_url(self, gpg_key_custom_url):
        """
        Sets the gpg_key_custom_url of this ApiHostInstallArguments.
        The Cloudera Manager public GPG key (optional). Example for SLES, Redhat or other RPM based distributions: https://archive.cloudera.com/cm5/redhat/5/x86_64/cm/RPM-GPG-KEY-cloudera Example for Ubuntu or other Debian based distributions: https://archive.cloudera.com/cm5/ubuntu/lucid/amd64/cm/archive.key

        :param gpg_key_custom_url: The gpg_key_custom_url of this ApiHostInstallArguments.
        :type: str
        """

        self._gpg_key_custom_url = gpg_key_custom_url

    @property
    def java_install_strategy(self):
        """
        Gets the java_install_strategy of this ApiHostInstallArguments.
        Added in v8: Strategy to use for JDK installation. Valid values are 1. AUTO (default): Cloudera Manager will install the JDK versions that are required when the \"AUTO\" option is selected. Cloudera Manager may overwrite any of the existing JDK installations. 2. NONE: Cloudera Manager will not install any JDK when \"NONE\" option is selected. It should be used if an existing JDK installation has to be used.

        :return: The java_install_strategy of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._java_install_strategy

    @java_install_strategy.setter
    def java_install_strategy(self, java_install_strategy):
        """
        Sets the java_install_strategy of this ApiHostInstallArguments.
        Added in v8: Strategy to use for JDK installation. Valid values are 1. AUTO (default): Cloudera Manager will install the JDK versions that are required when the \"AUTO\" option is selected. Cloudera Manager may overwrite any of the existing JDK installations. 2. NONE: Cloudera Manager will not install any JDK when \"NONE\" option is selected. It should be used if an existing JDK installation has to be used.

        :param java_install_strategy: The java_install_strategy of this ApiHostInstallArguments.
        :type: str
        """

        self._java_install_strategy = java_install_strategy

    @property
    def unlimited_jce(self):
        """
        Gets the unlimited_jce of this ApiHostInstallArguments.
        Added in v8: Flag for unlimited strength JCE policy files installation If unset, defaults to false

        :return: The unlimited_jce of this ApiHostInstallArguments.
        :rtype: bool
        """
        return self._unlimited_jce

    @unlimited_jce.setter
    def unlimited_jce(self, unlimited_jce):
        """
        Sets the unlimited_jce of this ApiHostInstallArguments.
        Added in v8: Flag for unlimited strength JCE policy files installation If unset, defaults to false

        :param unlimited_jce: The unlimited_jce of this ApiHostInstallArguments.
        :type: bool
        """

        self._unlimited_jce = unlimited_jce

    @property
    def gpg_key_override_bundle(self):
        """
        Gets the gpg_key_override_bundle of this ApiHostInstallArguments.
        The Cloudera Manager public GPG key (optional). This points to the actual bundle contents and not a URL.

        :return: The gpg_key_override_bundle of this ApiHostInstallArguments.
        :rtype: str
        """
        return self._gpg_key_override_bundle

    @gpg_key_override_bundle.setter
    def gpg_key_override_bundle(self, gpg_key_override_bundle):
        """
        Sets the gpg_key_override_bundle of this ApiHostInstallArguments.
        The Cloudera Manager public GPG key (optional). This points to the actual bundle contents and not a URL.

        :param gpg_key_override_bundle: The gpg_key_override_bundle of this ApiHostInstallArguments.
        :type: str
        """

        self._gpg_key_override_bundle = gpg_key_override_bundle

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiHostInstallArguments):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
