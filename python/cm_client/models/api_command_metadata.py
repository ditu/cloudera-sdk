# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiCommandMetadata(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'arg_schema': 'str'
    }

    attribute_map = {
        'name': 'name',
        'arg_schema': 'argSchema'
    }

    def __init__(self, name=None, arg_schema=None):
        """
        ApiCommandMetadata - a model defined in Swagger
        """

        self._name = None
        self._arg_schema = None

        if name is not None:
          self.name = name
        if arg_schema is not None:
          self.arg_schema = arg_schema

    @property
    def name(self):
        """
        Gets the name of this ApiCommandMetadata.
        The name of of the command.

        :return: The name of this ApiCommandMetadata.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ApiCommandMetadata.
        The name of of the command.

        :param name: The name of this ApiCommandMetadata.
        :type: str
        """

        self._name = name

    @property
    def arg_schema(self):
        """
        Gets the arg_schema of this ApiCommandMetadata.
        The command arguments schema.  This is in the form of json schema and describes the structure of the command arguments. If null, the command does not take arguments.

        :return: The arg_schema of this ApiCommandMetadata.
        :rtype: str
        """
        return self._arg_schema

    @arg_schema.setter
    def arg_schema(self, arg_schema):
        """
        Sets the arg_schema of this ApiCommandMetadata.
        The command arguments schema.  This is in the form of json schema and describes the structure of the command arguments. If null, the command does not take arguments.

        :param arg_schema: The arg_schema of this ApiCommandMetadata.
        :type: str
        """

        self._arg_schema = arg_schema

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiCommandMetadata):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
