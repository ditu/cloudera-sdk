# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiNameservice(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'active': 'ApiRoleRef',
        'active_failover_controller': 'ApiRoleRef',
        'stand_by': 'ApiRoleRef',
        'stand_by_failover_controller': 'ApiRoleRef',
        'secondary': 'ApiRoleRef',
        'mount_points': 'list[str]',
        'health_summary': 'ApiHealthSummary',
        'health_checks': 'list[ApiHealthCheck]'
    }

    attribute_map = {
        'name': 'name',
        'active': 'active',
        'active_failover_controller': 'activeFailoverController',
        'stand_by': 'standBy',
        'stand_by_failover_controller': 'standByFailoverController',
        'secondary': 'secondary',
        'mount_points': 'mountPoints',
        'health_summary': 'healthSummary',
        'health_checks': 'healthChecks'
    }

    def __init__(self, name=None, active=None, active_failover_controller=None, stand_by=None, stand_by_failover_controller=None, secondary=None, mount_points=None, health_summary=None, health_checks=None):
        """
        ApiNameservice - a model defined in Swagger
        """

        self._name = None
        self._active = None
        self._active_failover_controller = None
        self._stand_by = None
        self._stand_by_failover_controller = None
        self._secondary = None
        self._mount_points = None
        self._health_summary = None
        self._health_checks = None

        if name is not None:
          self.name = name
        if active is not None:
          self.active = active
        if active_failover_controller is not None:
          self.active_failover_controller = active_failover_controller
        if stand_by is not None:
          self.stand_by = stand_by
        if stand_by_failover_controller is not None:
          self.stand_by_failover_controller = stand_by_failover_controller
        if secondary is not None:
          self.secondary = secondary
        if mount_points is not None:
          self.mount_points = mount_points
        if health_summary is not None:
          self.health_summary = health_summary
        if health_checks is not None:
          self.health_checks = health_checks

    @property
    def name(self):
        """
        Gets the name of this ApiNameservice.
        Name of the nameservice.

        :return: The name of this ApiNameservice.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ApiNameservice.
        Name of the nameservice.

        :param name: The name of this ApiNameservice.
        :type: str
        """

        self._name = name

    @property
    def active(self):
        """
        Gets the active of this ApiNameservice.
        Reference to the active NameNode.

        :return: The active of this ApiNameservice.
        :rtype: ApiRoleRef
        """
        return self._active

    @active.setter
    def active(self, active):
        """
        Sets the active of this ApiNameservice.
        Reference to the active NameNode.

        :param active: The active of this ApiNameservice.
        :type: ApiRoleRef
        """

        self._active = active

    @property
    def active_failover_controller(self):
        """
        Gets the active_failover_controller of this ApiNameservice.
        Reference to the active NameNode's failover controller, if configured.

        :return: The active_failover_controller of this ApiNameservice.
        :rtype: ApiRoleRef
        """
        return self._active_failover_controller

    @active_failover_controller.setter
    def active_failover_controller(self, active_failover_controller):
        """
        Sets the active_failover_controller of this ApiNameservice.
        Reference to the active NameNode's failover controller, if configured.

        :param active_failover_controller: The active_failover_controller of this ApiNameservice.
        :type: ApiRoleRef
        """

        self._active_failover_controller = active_failover_controller

    @property
    def stand_by(self):
        """
        Gets the stand_by of this ApiNameservice.
        Reference to the stand-by NameNode.

        :return: The stand_by of this ApiNameservice.
        :rtype: ApiRoleRef
        """
        return self._stand_by

    @stand_by.setter
    def stand_by(self, stand_by):
        """
        Sets the stand_by of this ApiNameservice.
        Reference to the stand-by NameNode.

        :param stand_by: The stand_by of this ApiNameservice.
        :type: ApiRoleRef
        """

        self._stand_by = stand_by

    @property
    def stand_by_failover_controller(self):
        """
        Gets the stand_by_failover_controller of this ApiNameservice.
        Reference to the stand-by NameNode's failover controller, if configured.

        :return: The stand_by_failover_controller of this ApiNameservice.
        :rtype: ApiRoleRef
        """
        return self._stand_by_failover_controller

    @stand_by_failover_controller.setter
    def stand_by_failover_controller(self, stand_by_failover_controller):
        """
        Sets the stand_by_failover_controller of this ApiNameservice.
        Reference to the stand-by NameNode's failover controller, if configured.

        :param stand_by_failover_controller: The stand_by_failover_controller of this ApiNameservice.
        :type: ApiRoleRef
        """

        self._stand_by_failover_controller = stand_by_failover_controller

    @property
    def secondary(self):
        """
        Gets the secondary of this ApiNameservice.
        Reference to the SecondaryNameNode.

        :return: The secondary of this ApiNameservice.
        :rtype: ApiRoleRef
        """
        return self._secondary

    @secondary.setter
    def secondary(self, secondary):
        """
        Sets the secondary of this ApiNameservice.
        Reference to the SecondaryNameNode.

        :param secondary: The secondary of this ApiNameservice.
        :type: ApiRoleRef
        """

        self._secondary = secondary

    @property
    def mount_points(self):
        """
        Gets the mount_points of this ApiNameservice.
        Mount points assigned to this nameservice in a federation.

        :return: The mount_points of this ApiNameservice.
        :rtype: list[str]
        """
        return self._mount_points

    @mount_points.setter
    def mount_points(self, mount_points):
        """
        Sets the mount_points of this ApiNameservice.
        Mount points assigned to this nameservice in a federation.

        :param mount_points: The mount_points of this ApiNameservice.
        :type: list[str]
        """

        self._mount_points = mount_points

    @property
    def health_summary(self):
        """
        Gets the health_summary of this ApiNameservice.
        Requires \"full\" view. The high-level health status of this nameservice.

        :return: The health_summary of this ApiNameservice.
        :rtype: ApiHealthSummary
        """
        return self._health_summary

    @health_summary.setter
    def health_summary(self, health_summary):
        """
        Sets the health_summary of this ApiNameservice.
        Requires \"full\" view. The high-level health status of this nameservice.

        :param health_summary: The health_summary of this ApiNameservice.
        :type: ApiHealthSummary
        """

        self._health_summary = health_summary

    @property
    def health_checks(self):
        """
        Gets the health_checks of this ApiNameservice.
        Requires \"full\" view. List of health checks performed on the nameservice.

        :return: The health_checks of this ApiNameservice.
        :rtype: list[ApiHealthCheck]
        """
        return self._health_checks

    @health_checks.setter
    def health_checks(self, health_checks):
        """
        Sets the health_checks of this ApiNameservice.
        Requires \"full\" view. List of health checks performed on the nameservice.

        :param health_checks: The health_checks of this ApiNameservice.
        :type: list[ApiHealthCheck]
        """

        self._health_checks = health_checks

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiNameservice):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
