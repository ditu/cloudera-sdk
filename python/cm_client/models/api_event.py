# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiEvent(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'content': 'str',
        'time_occurred': 'str',
        'time_received': 'str',
        'category': 'ApiEventCategory',
        'severity': 'ApiEventSeverity',
        'alert': 'bool',
        'attributes': 'list[ApiEventAttribute]'
    }

    attribute_map = {
        'id': 'id',
        'content': 'content',
        'time_occurred': 'timeOccurred',
        'time_received': 'timeReceived',
        'category': 'category',
        'severity': 'severity',
        'alert': 'alert',
        'attributes': 'attributes'
    }

    def __init__(self, id=None, content=None, time_occurred=None, time_received=None, category=None, severity=None, alert=None, attributes=None):
        """
        ApiEvent - a model defined in Swagger
        """

        self._id = None
        self._content = None
        self._time_occurred = None
        self._time_received = None
        self._category = None
        self._severity = None
        self._alert = None
        self._attributes = None

        if id is not None:
          self.id = id
        if content is not None:
          self.content = content
        if time_occurred is not None:
          self.time_occurred = time_occurred
        if time_received is not None:
          self.time_received = time_received
        if category is not None:
          self.category = category
        if severity is not None:
          self.severity = severity
        if alert is not None:
          self.alert = alert
        if attributes is not None:
          self.attributes = attributes

    @property
    def id(self):
        """
        Gets the id of this ApiEvent.
        A unique ID for this event.

        :return: The id of this ApiEvent.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this ApiEvent.
        A unique ID for this event.

        :param id: The id of this ApiEvent.
        :type: str
        """

        self._id = id

    @property
    def content(self):
        """
        Gets the content of this ApiEvent.
        The content payload of this event.

        :return: The content of this ApiEvent.
        :rtype: str
        """
        return self._content

    @content.setter
    def content(self, content):
        """
        Sets the content of this ApiEvent.
        The content payload of this event.

        :param content: The content of this ApiEvent.
        :type: str
        """

        self._content = content

    @property
    def time_occurred(self):
        """
        Gets the time_occurred of this ApiEvent.
        When the event was generated.

        :return: The time_occurred of this ApiEvent.
        :rtype: str
        """
        return self._time_occurred

    @time_occurred.setter
    def time_occurred(self, time_occurred):
        """
        Sets the time_occurred of this ApiEvent.
        When the event was generated.

        :param time_occurred: The time_occurred of this ApiEvent.
        :type: str
        """

        self._time_occurred = time_occurred

    @property
    def time_received(self):
        """
        Gets the time_received of this ApiEvent.
        When the event was stored by Cloudera Manager. Events do not arrive in the order that they are generated. If you are writing an event poller, this is a useful field to query.

        :return: The time_received of this ApiEvent.
        :rtype: str
        """
        return self._time_received

    @time_received.setter
    def time_received(self, time_received):
        """
        Sets the time_received of this ApiEvent.
        When the event was stored by Cloudera Manager. Events do not arrive in the order that they are generated. If you are writing an event poller, this is a useful field to query.

        :param time_received: The time_received of this ApiEvent.
        :type: str
        """

        self._time_received = time_received

    @property
    def category(self):
        """
        Gets the category of this ApiEvent.
        The category of this event -- whether it is a health event, an audit event, an activity event, etc.

        :return: The category of this ApiEvent.
        :rtype: ApiEventCategory
        """
        return self._category

    @category.setter
    def category(self, category):
        """
        Sets the category of this ApiEvent.
        The category of this event -- whether it is a health event, an audit event, an activity event, etc.

        :param category: The category of this ApiEvent.
        :type: ApiEventCategory
        """

        self._category = category

    @property
    def severity(self):
        """
        Gets the severity of this ApiEvent.
        The severity of the event.

        :return: The severity of this ApiEvent.
        :rtype: ApiEventSeverity
        """
        return self._severity

    @severity.setter
    def severity(self, severity):
        """
        Sets the severity of this ApiEvent.
        The severity of the event.

        :param severity: The severity of this ApiEvent.
        :type: ApiEventSeverity
        """

        self._severity = severity

    @property
    def alert(self):
        """
        Gets the alert of this ApiEvent.
        Whether the event is promoted to an alert according to configuration.

        :return: The alert of this ApiEvent.
        :rtype: bool
        """
        return self._alert

    @alert.setter
    def alert(self, alert):
        """
        Sets the alert of this ApiEvent.
        Whether the event is promoted to an alert according to configuration.

        :param alert: The alert of this ApiEvent.
        :type: bool
        """

        self._alert = alert

    @property
    def attributes(self):
        """
        Gets the attributes of this ApiEvent.
        A list of key-value attribute pairs.

        :return: The attributes of this ApiEvent.
        :rtype: list[ApiEventAttribute]
        """
        return self._attributes

    @attributes.setter
    def attributes(self, attributes):
        """
        Sets the attributes of this ApiEvent.
        A list of key-value attribute pairs.

        :param attributes: The attributes of this ApiEvent.
        :type: list[ApiEventAttribute]
        """

        self._attributes = attributes

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiEvent):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
