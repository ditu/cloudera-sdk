# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiClusterTemplateHostInfo(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'host_name': 'str',
        'host_name_range': 'str',
        'rack_id': 'str',
        'host_template_ref_name': 'str',
        'role_ref_names': 'list[str]'
    }

    attribute_map = {
        'host_name': 'hostName',
        'host_name_range': 'hostNameRange',
        'rack_id': 'rackId',
        'host_template_ref_name': 'hostTemplateRefName',
        'role_ref_names': 'roleRefNames'
    }

    def __init__(self, host_name=None, host_name_range=None, rack_id=None, host_template_ref_name=None, role_ref_names=None):
        """
        ApiClusterTemplateHostInfo - a model defined in Swagger
        """

        self._host_name = None
        self._host_name_range = None
        self._rack_id = None
        self._host_template_ref_name = None
        self._role_ref_names = None

        if host_name is not None:
          self.host_name = host_name
        if host_name_range is not None:
          self.host_name_range = host_name_range
        if rack_id is not None:
          self.rack_id = rack_id
        if host_template_ref_name is not None:
          self.host_template_ref_name = host_template_ref_name
        if role_ref_names is not None:
          self.role_ref_names = role_ref_names

    @property
    def host_name(self):
        """
        Gets the host_name of this ApiClusterTemplateHostInfo.
        

        :return: The host_name of this ApiClusterTemplateHostInfo.
        :rtype: str
        """
        return self._host_name

    @host_name.setter
    def host_name(self, host_name):
        """
        Sets the host_name of this ApiClusterTemplateHostInfo.
        

        :param host_name: The host_name of this ApiClusterTemplateHostInfo.
        :type: str
        """

        self._host_name = host_name

    @property
    def host_name_range(self):
        """
        Gets the host_name_range of this ApiClusterTemplateHostInfo.
        

        :return: The host_name_range of this ApiClusterTemplateHostInfo.
        :rtype: str
        """
        return self._host_name_range

    @host_name_range.setter
    def host_name_range(self, host_name_range):
        """
        Sets the host_name_range of this ApiClusterTemplateHostInfo.
        

        :param host_name_range: The host_name_range of this ApiClusterTemplateHostInfo.
        :type: str
        """

        self._host_name_range = host_name_range

    @property
    def rack_id(self):
        """
        Gets the rack_id of this ApiClusterTemplateHostInfo.
        

        :return: The rack_id of this ApiClusterTemplateHostInfo.
        :rtype: str
        """
        return self._rack_id

    @rack_id.setter
    def rack_id(self, rack_id):
        """
        Sets the rack_id of this ApiClusterTemplateHostInfo.
        

        :param rack_id: The rack_id of this ApiClusterTemplateHostInfo.
        :type: str
        """

        self._rack_id = rack_id

    @property
    def host_template_ref_name(self):
        """
        Gets the host_template_ref_name of this ApiClusterTemplateHostInfo.
        

        :return: The host_template_ref_name of this ApiClusterTemplateHostInfo.
        :rtype: str
        """
        return self._host_template_ref_name

    @host_template_ref_name.setter
    def host_template_ref_name(self, host_template_ref_name):
        """
        Sets the host_template_ref_name of this ApiClusterTemplateHostInfo.
        

        :param host_template_ref_name: The host_template_ref_name of this ApiClusterTemplateHostInfo.
        :type: str
        """

        self._host_template_ref_name = host_template_ref_name

    @property
    def role_ref_names(self):
        """
        Gets the role_ref_names of this ApiClusterTemplateHostInfo.
        

        :return: The role_ref_names of this ApiClusterTemplateHostInfo.
        :rtype: list[str]
        """
        return self._role_ref_names

    @role_ref_names.setter
    def role_ref_names(self, role_ref_names):
        """
        Sets the role_ref_names of this ApiClusterTemplateHostInfo.
        

        :param role_ref_names: The role_ref_names of this ApiClusterTemplateHostInfo.
        :type: list[str]
        """

        self._role_ref_names = role_ref_names

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiClusterTemplateHostInfo):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
