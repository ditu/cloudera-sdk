# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiHiveReplicationError(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'database': 'str',
        'table_name': 'str',
        'impala_udf': 'str',
        'hive_udf': 'str',
        'error': 'str'
    }

    attribute_map = {
        'database': 'database',
        'table_name': 'tableName',
        'impala_udf': 'impalaUDF',
        'hive_udf': 'hiveUDF',
        'error': 'error'
    }

    def __init__(self, database=None, table_name=None, impala_udf=None, hive_udf=None, error=None):
        """
        ApiHiveReplicationError - a model defined in Swagger
        """

        self._database = None
        self._table_name = None
        self._impala_udf = None
        self._hive_udf = None
        self._error = None

        if database is not None:
          self.database = database
        if table_name is not None:
          self.table_name = table_name
        if impala_udf is not None:
          self.impala_udf = impala_udf
        if hive_udf is not None:
          self.hive_udf = hive_udf
        if error is not None:
          self.error = error

    @property
    def database(self):
        """
        Gets the database of this ApiHiveReplicationError.
        Name of the database.

        :return: The database of this ApiHiveReplicationError.
        :rtype: str
        """
        return self._database

    @database.setter
    def database(self, database):
        """
        Sets the database of this ApiHiveReplicationError.
        Name of the database.

        :param database: The database of this ApiHiveReplicationError.
        :type: str
        """

        self._database = database

    @property
    def table_name(self):
        """
        Gets the table_name of this ApiHiveReplicationError.
        Name of the table.

        :return: The table_name of this ApiHiveReplicationError.
        :rtype: str
        """
        return self._table_name

    @table_name.setter
    def table_name(self, table_name):
        """
        Sets the table_name of this ApiHiveReplicationError.
        Name of the table.

        :param table_name: The table_name of this ApiHiveReplicationError.
        :type: str
        """

        self._table_name = table_name

    @property
    def impala_udf(self):
        """
        Gets the impala_udf of this ApiHiveReplicationError.
        UDF signature, includes the UDF name and parameter types.

        :return: The impala_udf of this ApiHiveReplicationError.
        :rtype: str
        """
        return self._impala_udf

    @impala_udf.setter
    def impala_udf(self, impala_udf):
        """
        Sets the impala_udf of this ApiHiveReplicationError.
        UDF signature, includes the UDF name and parameter types.

        :param impala_udf: The impala_udf of this ApiHiveReplicationError.
        :type: str
        """

        self._impala_udf = impala_udf

    @property
    def hive_udf(self):
        """
        Gets the hive_udf of this ApiHiveReplicationError.
        Hive UDF signature, includes the UDF name and parameter types.

        :return: The hive_udf of this ApiHiveReplicationError.
        :rtype: str
        """
        return self._hive_udf

    @hive_udf.setter
    def hive_udf(self, hive_udf):
        """
        Sets the hive_udf of this ApiHiveReplicationError.
        Hive UDF signature, includes the UDF name and parameter types.

        :param hive_udf: The hive_udf of this ApiHiveReplicationError.
        :type: str
        """

        self._hive_udf = hive_udf

    @property
    def error(self):
        """
        Gets the error of this ApiHiveReplicationError.
        Description of the error.

        :return: The error of this ApiHiveReplicationError.
        :rtype: str
        """
        return self._error

    @error.setter
    def error(self, error):
        """
        Sets the error of this ApiHiveReplicationError.
        Description of the error.

        :param error: The error of this ApiHiveReplicationError.
        :type: str
        """

        self._error = error

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiHiveReplicationError):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
