# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiYarnApplication(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'allocated_mb': 'float',
        'allocated_v_cores': 'float',
        'running_containers': 'float',
        'application_tags': 'list[str]',
        'allocated_memory_seconds': 'float',
        'allocated_vcore_seconds': 'float',
        'application_id': 'str',
        'name': 'str',
        'start_time': 'str',
        'end_time': 'str',
        'user': 'str',
        'pool': 'str',
        'progress': 'float',
        'attributes': 'dict(str, str)',
        'mr2_app_information': 'ApiMr2AppInformation',
        'state': 'str',
        'container_used_memory_seconds': 'float',
        'container_used_memory_max': 'float',
        'container_used_cpu_seconds': 'float',
        'container_used_vcore_seconds': 'float',
        'container_allocated_memory_seconds': 'float',
        'container_allocated_vcore_seconds': 'float'
    }

    attribute_map = {
        'allocated_mb': 'allocatedMB',
        'allocated_v_cores': 'allocatedVCores',
        'running_containers': 'runningContainers',
        'application_tags': 'applicationTags',
        'allocated_memory_seconds': 'allocatedMemorySeconds',
        'allocated_vcore_seconds': 'allocatedVcoreSeconds',
        'application_id': 'applicationId',
        'name': 'name',
        'start_time': 'startTime',
        'end_time': 'endTime',
        'user': 'user',
        'pool': 'pool',
        'progress': 'progress',
        'attributes': 'attributes',
        'mr2_app_information': 'mr2AppInformation',
        'state': 'state',
        'container_used_memory_seconds': 'containerUsedMemorySeconds',
        'container_used_memory_max': 'containerUsedMemoryMax',
        'container_used_cpu_seconds': 'containerUsedCpuSeconds',
        'container_used_vcore_seconds': 'containerUsedVcoreSeconds',
        'container_allocated_memory_seconds': 'containerAllocatedMemorySeconds',
        'container_allocated_vcore_seconds': 'containerAllocatedVcoreSeconds'
    }

    def __init__(self, allocated_mb=None, allocated_v_cores=None, running_containers=None, application_tags=None, allocated_memory_seconds=None, allocated_vcore_seconds=None, application_id=None, name=None, start_time=None, end_time=None, user=None, pool=None, progress=None, attributes=None, mr2_app_information=None, state=None, container_used_memory_seconds=None, container_used_memory_max=None, container_used_cpu_seconds=None, container_used_vcore_seconds=None, container_allocated_memory_seconds=None, container_allocated_vcore_seconds=None):
        """
        ApiYarnApplication - a model defined in Swagger
        """

        self._allocated_mb = None
        self._allocated_v_cores = None
        self._running_containers = None
        self._application_tags = None
        self._allocated_memory_seconds = None
        self._allocated_vcore_seconds = None
        self._application_id = None
        self._name = None
        self._start_time = None
        self._end_time = None
        self._user = None
        self._pool = None
        self._progress = None
        self._attributes = None
        self._mr2_app_information = None
        self._state = None
        self._container_used_memory_seconds = None
        self._container_used_memory_max = None
        self._container_used_cpu_seconds = None
        self._container_used_vcore_seconds = None
        self._container_allocated_memory_seconds = None
        self._container_allocated_vcore_seconds = None

        if allocated_mb is not None:
          self.allocated_mb = allocated_mb
        if allocated_v_cores is not None:
          self.allocated_v_cores = allocated_v_cores
        if running_containers is not None:
          self.running_containers = running_containers
        if application_tags is not None:
          self.application_tags = application_tags
        if allocated_memory_seconds is not None:
          self.allocated_memory_seconds = allocated_memory_seconds
        if allocated_vcore_seconds is not None:
          self.allocated_vcore_seconds = allocated_vcore_seconds
        if application_id is not None:
          self.application_id = application_id
        if name is not None:
          self.name = name
        if start_time is not None:
          self.start_time = start_time
        if end_time is not None:
          self.end_time = end_time
        if user is not None:
          self.user = user
        if pool is not None:
          self.pool = pool
        if progress is not None:
          self.progress = progress
        if attributes is not None:
          self.attributes = attributes
        if mr2_app_information is not None:
          self.mr2_app_information = mr2_app_information
        if state is not None:
          self.state = state
        if container_used_memory_seconds is not None:
          self.container_used_memory_seconds = container_used_memory_seconds
        if container_used_memory_max is not None:
          self.container_used_memory_max = container_used_memory_max
        if container_used_cpu_seconds is not None:
          self.container_used_cpu_seconds = container_used_cpu_seconds
        if container_used_vcore_seconds is not None:
          self.container_used_vcore_seconds = container_used_vcore_seconds
        if container_allocated_memory_seconds is not None:
          self.container_allocated_memory_seconds = container_allocated_memory_seconds
        if container_allocated_vcore_seconds is not None:
          self.container_allocated_vcore_seconds = container_allocated_vcore_seconds

    @property
    def allocated_mb(self):
        """
        Gets the allocated_mb of this ApiYarnApplication.
        The sum of memory in MB allocated to the application's running containers Available since v12.

        :return: The allocated_mb of this ApiYarnApplication.
        :rtype: float
        """
        return self._allocated_mb

    @allocated_mb.setter
    def allocated_mb(self, allocated_mb):
        """
        Sets the allocated_mb of this ApiYarnApplication.
        The sum of memory in MB allocated to the application's running containers Available since v12.

        :param allocated_mb: The allocated_mb of this ApiYarnApplication.
        :type: float
        """

        self._allocated_mb = allocated_mb

    @property
    def allocated_v_cores(self):
        """
        Gets the allocated_v_cores of this ApiYarnApplication.
        The sum of virtual cores allocated to the application's running containers Available since v12.

        :return: The allocated_v_cores of this ApiYarnApplication.
        :rtype: float
        """
        return self._allocated_v_cores

    @allocated_v_cores.setter
    def allocated_v_cores(self, allocated_v_cores):
        """
        Sets the allocated_v_cores of this ApiYarnApplication.
        The sum of virtual cores allocated to the application's running containers Available since v12.

        :param allocated_v_cores: The allocated_v_cores of this ApiYarnApplication.
        :type: float
        """

        self._allocated_v_cores = allocated_v_cores

    @property
    def running_containers(self):
        """
        Gets the running_containers of this ApiYarnApplication.
        The number of containers currently running for the application Available since v12.

        :return: The running_containers of this ApiYarnApplication.
        :rtype: float
        """
        return self._running_containers

    @running_containers.setter
    def running_containers(self, running_containers):
        """
        Sets the running_containers of this ApiYarnApplication.
        The number of containers currently running for the application Available since v12.

        :param running_containers: The running_containers of this ApiYarnApplication.
        :type: float
        """

        self._running_containers = running_containers

    @property
    def application_tags(self):
        """
        Gets the application_tags of this ApiYarnApplication.
        List of YARN application tags. Available since v12.

        :return: The application_tags of this ApiYarnApplication.
        :rtype: list[str]
        """
        return self._application_tags

    @application_tags.setter
    def application_tags(self, application_tags):
        """
        Sets the application_tags of this ApiYarnApplication.
        List of YARN application tags. Available since v12.

        :param application_tags: The application_tags of this ApiYarnApplication.
        :type: list[str]
        """

        self._application_tags = application_tags

    @property
    def allocated_memory_seconds(self):
        """
        Gets the allocated_memory_seconds of this ApiYarnApplication.
        Allocated memory to the application in units of mb-secs. Available since v12.

        :return: The allocated_memory_seconds of this ApiYarnApplication.
        :rtype: float
        """
        return self._allocated_memory_seconds

    @allocated_memory_seconds.setter
    def allocated_memory_seconds(self, allocated_memory_seconds):
        """
        Sets the allocated_memory_seconds of this ApiYarnApplication.
        Allocated memory to the application in units of mb-secs. Available since v12.

        :param allocated_memory_seconds: The allocated_memory_seconds of this ApiYarnApplication.
        :type: float
        """

        self._allocated_memory_seconds = allocated_memory_seconds

    @property
    def allocated_vcore_seconds(self):
        """
        Gets the allocated_vcore_seconds of this ApiYarnApplication.
        Allocated vcore-secs to the application. Available since v12.

        :return: The allocated_vcore_seconds of this ApiYarnApplication.
        :rtype: float
        """
        return self._allocated_vcore_seconds

    @allocated_vcore_seconds.setter
    def allocated_vcore_seconds(self, allocated_vcore_seconds):
        """
        Sets the allocated_vcore_seconds of this ApiYarnApplication.
        Allocated vcore-secs to the application. Available since v12.

        :param allocated_vcore_seconds: The allocated_vcore_seconds of this ApiYarnApplication.
        :type: float
        """

        self._allocated_vcore_seconds = allocated_vcore_seconds

    @property
    def application_id(self):
        """
        Gets the application_id of this ApiYarnApplication.
        The application id.

        :return: The application_id of this ApiYarnApplication.
        :rtype: str
        """
        return self._application_id

    @application_id.setter
    def application_id(self, application_id):
        """
        Sets the application_id of this ApiYarnApplication.
        The application id.

        :param application_id: The application_id of this ApiYarnApplication.
        :type: str
        """

        self._application_id = application_id

    @property
    def name(self):
        """
        Gets the name of this ApiYarnApplication.
        The name of the application.

        :return: The name of this ApiYarnApplication.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ApiYarnApplication.
        The name of the application.

        :param name: The name of this ApiYarnApplication.
        :type: str
        """

        self._name = name

    @property
    def start_time(self):
        """
        Gets the start_time of this ApiYarnApplication.
        The time the application was submitted.

        :return: The start_time of this ApiYarnApplication.
        :rtype: str
        """
        return self._start_time

    @start_time.setter
    def start_time(self, start_time):
        """
        Sets the start_time of this ApiYarnApplication.
        The time the application was submitted.

        :param start_time: The start_time of this ApiYarnApplication.
        :type: str
        """

        self._start_time = start_time

    @property
    def end_time(self):
        """
        Gets the end_time of this ApiYarnApplication.
        The time the application finished. If the application hasn't finished this will return null.

        :return: The end_time of this ApiYarnApplication.
        :rtype: str
        """
        return self._end_time

    @end_time.setter
    def end_time(self, end_time):
        """
        Sets the end_time of this ApiYarnApplication.
        The time the application finished. If the application hasn't finished this will return null.

        :param end_time: The end_time of this ApiYarnApplication.
        :type: str
        """

        self._end_time = end_time

    @property
    def user(self):
        """
        Gets the user of this ApiYarnApplication.
        The user who submitted the application.

        :return: The user of this ApiYarnApplication.
        :rtype: str
        """
        return self._user

    @user.setter
    def user(self, user):
        """
        Sets the user of this ApiYarnApplication.
        The user who submitted the application.

        :param user: The user of this ApiYarnApplication.
        :type: str
        """

        self._user = user

    @property
    def pool(self):
        """
        Gets the pool of this ApiYarnApplication.
        The pool the application was submitted to.

        :return: The pool of this ApiYarnApplication.
        :rtype: str
        """
        return self._pool

    @pool.setter
    def pool(self, pool):
        """
        Sets the pool of this ApiYarnApplication.
        The pool the application was submitted to.

        :param pool: The pool of this ApiYarnApplication.
        :type: str
        """

        self._pool = pool

    @property
    def progress(self):
        """
        Gets the progress of this ApiYarnApplication.
        The progress, as a percentage, the application has made. This is only set if the application is currently executing.

        :return: The progress of this ApiYarnApplication.
        :rtype: float
        """
        return self._progress

    @progress.setter
    def progress(self, progress):
        """
        Sets the progress of this ApiYarnApplication.
        The progress, as a percentage, the application has made. This is only set if the application is currently executing.

        :param progress: The progress of this ApiYarnApplication.
        :type: float
        """

        self._progress = progress

    @property
    def attributes(self):
        """
        Gets the attributes of this ApiYarnApplication.
        A map of additional application attributes which is generated by Cloudera Manager. For example MR2 job counters are exposed as key/value pairs here. For more details see the Cloudera Manager documentation.

        :return: The attributes of this ApiYarnApplication.
        :rtype: dict(str, str)
        """
        return self._attributes

    @attributes.setter
    def attributes(self, attributes):
        """
        Sets the attributes of this ApiYarnApplication.
        A map of additional application attributes which is generated by Cloudera Manager. For example MR2 job counters are exposed as key/value pairs here. For more details see the Cloudera Manager documentation.

        :param attributes: The attributes of this ApiYarnApplication.
        :type: dict(str, str)
        """

        self._attributes = attributes

    @property
    def mr2_app_information(self):
        """
        Gets the mr2_app_information of this ApiYarnApplication.
        

        :return: The mr2_app_information of this ApiYarnApplication.
        :rtype: ApiMr2AppInformation
        """
        return self._mr2_app_information

    @mr2_app_information.setter
    def mr2_app_information(self, mr2_app_information):
        """
        Sets the mr2_app_information of this ApiYarnApplication.
        

        :param mr2_app_information: The mr2_app_information of this ApiYarnApplication.
        :type: ApiMr2AppInformation
        """

        self._mr2_app_information = mr2_app_information

    @property
    def state(self):
        """
        Gets the state of this ApiYarnApplication.
        

        :return: The state of this ApiYarnApplication.
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """
        Sets the state of this ApiYarnApplication.
        

        :param state: The state of this ApiYarnApplication.
        :type: str
        """

        self._state = state

    @property
    def container_used_memory_seconds(self):
        """
        Gets the container_used_memory_seconds of this ApiYarnApplication.
        Actual memory (in MB-secs) used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :return: The container_used_memory_seconds of this ApiYarnApplication.
        :rtype: float
        """
        return self._container_used_memory_seconds

    @container_used_memory_seconds.setter
    def container_used_memory_seconds(self, container_used_memory_seconds):
        """
        Sets the container_used_memory_seconds of this ApiYarnApplication.
        Actual memory (in MB-secs) used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :param container_used_memory_seconds: The container_used_memory_seconds of this ApiYarnApplication.
        :type: float
        """

        self._container_used_memory_seconds = container_used_memory_seconds

    @property
    def container_used_memory_max(self):
        """
        Gets the container_used_memory_max of this ApiYarnApplication.
        Maximum memory used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics Available since v16

        :return: The container_used_memory_max of this ApiYarnApplication.
        :rtype: float
        """
        return self._container_used_memory_max

    @container_used_memory_max.setter
    def container_used_memory_max(self, container_used_memory_max):
        """
        Sets the container_used_memory_max of this ApiYarnApplication.
        Maximum memory used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics Available since v16

        :param container_used_memory_max: The container_used_memory_max of this ApiYarnApplication.
        :type: float
        """

        self._container_used_memory_max = container_used_memory_max

    @property
    def container_used_cpu_seconds(self):
        """
        Gets the container_used_cpu_seconds of this ApiYarnApplication.
        Actual CPU (in percent-secs) used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :return: The container_used_cpu_seconds of this ApiYarnApplication.
        :rtype: float
        """
        return self._container_used_cpu_seconds

    @container_used_cpu_seconds.setter
    def container_used_cpu_seconds(self, container_used_cpu_seconds):
        """
        Sets the container_used_cpu_seconds of this ApiYarnApplication.
        Actual CPU (in percent-secs) used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :param container_used_cpu_seconds: The container_used_cpu_seconds of this ApiYarnApplication.
        :type: float
        """

        self._container_used_cpu_seconds = container_used_cpu_seconds

    @property
    def container_used_vcore_seconds(self):
        """
        Gets the container_used_vcore_seconds of this ApiYarnApplication.
        Actual VCore-secs used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :return: The container_used_vcore_seconds of this ApiYarnApplication.
        :rtype: float
        """
        return self._container_used_vcore_seconds

    @container_used_vcore_seconds.setter
    def container_used_vcore_seconds(self, container_used_vcore_seconds):
        """
        Sets the container_used_vcore_seconds of this ApiYarnApplication.
        Actual VCore-secs used by containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :param container_used_vcore_seconds: The container_used_vcore_seconds of this ApiYarnApplication.
        :type: float
        """

        self._container_used_vcore_seconds = container_used_vcore_seconds

    @property
    def container_allocated_memory_seconds(self):
        """
        Gets the container_allocated_memory_seconds of this ApiYarnApplication.
        Total memory (in mb-secs) allocated to containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :return: The container_allocated_memory_seconds of this ApiYarnApplication.
        :rtype: float
        """
        return self._container_allocated_memory_seconds

    @container_allocated_memory_seconds.setter
    def container_allocated_memory_seconds(self, container_allocated_memory_seconds):
        """
        Sets the container_allocated_memory_seconds of this ApiYarnApplication.
        Total memory (in mb-secs) allocated to containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :param container_allocated_memory_seconds: The container_allocated_memory_seconds of this ApiYarnApplication.
        :type: float
        """

        self._container_allocated_memory_seconds = container_allocated_memory_seconds

    @property
    def container_allocated_vcore_seconds(self):
        """
        Gets the container_allocated_vcore_seconds of this ApiYarnApplication.
        Total vcore-secs allocated to containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :return: The container_allocated_vcore_seconds of this ApiYarnApplication.
        :rtype: float
        """
        return self._container_allocated_vcore_seconds

    @container_allocated_vcore_seconds.setter
    def container_allocated_vcore_seconds(self, container_allocated_vcore_seconds):
        """
        Sets the container_allocated_vcore_seconds of this ApiYarnApplication.
        Total vcore-secs allocated to containers launched by the YARN application. Computed by running a MapReduce job from Cloudera Service Monitor to aggregate YARN usage metrics. Available since v12.

        :param container_allocated_vcore_seconds: The container_allocated_vcore_seconds of this ApiYarnApplication.
        :type: float
        """

        self._container_allocated_vcore_seconds = container_allocated_vcore_seconds

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiYarnApplication):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
