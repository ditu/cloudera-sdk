# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiHostRef(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'host_id': 'str',
        'hostname': 'str'
    }

    attribute_map = {
        'host_id': 'hostId',
        'hostname': 'hostname'
    }

    def __init__(self, host_id=None, hostname=None):
        """
        ApiHostRef - a model defined in Swagger
        """

        self._host_id = None
        self._hostname = None

        if host_id is not None:
          self.host_id = host_id
        if hostname is not None:
          self.hostname = hostname

    @property
    def host_id(self):
        """
        Gets the host_id of this ApiHostRef.
        The unique host ID.

        :return: The host_id of this ApiHostRef.
        :rtype: str
        """
        return self._host_id

    @host_id.setter
    def host_id(self, host_id):
        """
        Sets the host_id of this ApiHostRef.
        The unique host ID.

        :param host_id: The host_id of this ApiHostRef.
        :type: str
        """

        self._host_id = host_id

    @property
    def hostname(self):
        """
        Gets the hostname of this ApiHostRef.
        The hostname. Available since v31.

        :return: The hostname of this ApiHostRef.
        :rtype: str
        """
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        """
        Sets the hostname of this ApiHostRef.
        The hostname. Available since v31.

        :param hostname: The hostname of this ApiHostRef.
        :type: str
        """

        self._hostname = hostname

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiHostRef):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
