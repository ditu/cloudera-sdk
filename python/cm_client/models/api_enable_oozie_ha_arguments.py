# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiEnableOozieHaArguments(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'new_oozie_server_host_ids': 'list[str]',
        'new_oozie_server_role_names': 'list[str]',
        'zk_service_name': 'str',
        'load_balancer_hostname': 'str',
        'load_balancer_port': 'float',
        'load_balancer_ssl_port': 'float',
        'load_balancer_host_port': 'str'
    }

    attribute_map = {
        'new_oozie_server_host_ids': 'newOozieServerHostIds',
        'new_oozie_server_role_names': 'newOozieServerRoleNames',
        'zk_service_name': 'zkServiceName',
        'load_balancer_hostname': 'loadBalancerHostname',
        'load_balancer_port': 'loadBalancerPort',
        'load_balancer_ssl_port': 'loadBalancerSslPort',
        'load_balancer_host_port': 'loadBalancerHostPort'
    }

    def __init__(self, new_oozie_server_host_ids=None, new_oozie_server_role_names=None, zk_service_name=None, load_balancer_hostname=None, load_balancer_port=None, load_balancer_ssl_port=None, load_balancer_host_port=None):
        """
        ApiEnableOozieHaArguments - a model defined in Swagger
        """

        self._new_oozie_server_host_ids = None
        self._new_oozie_server_role_names = None
        self._zk_service_name = None
        self._load_balancer_hostname = None
        self._load_balancer_port = None
        self._load_balancer_ssl_port = None
        self._load_balancer_host_port = None

        if new_oozie_server_host_ids is not None:
          self.new_oozie_server_host_ids = new_oozie_server_host_ids
        if new_oozie_server_role_names is not None:
          self.new_oozie_server_role_names = new_oozie_server_role_names
        if zk_service_name is not None:
          self.zk_service_name = zk_service_name
        if load_balancer_hostname is not None:
          self.load_balancer_hostname = load_balancer_hostname
        if load_balancer_port is not None:
          self.load_balancer_port = load_balancer_port
        if load_balancer_ssl_port is not None:
          self.load_balancer_ssl_port = load_balancer_ssl_port
        if load_balancer_host_port is not None:
          self.load_balancer_host_port = load_balancer_host_port

    @property
    def new_oozie_server_host_ids(self):
        """
        Gets the new_oozie_server_host_ids of this ApiEnableOozieHaArguments.
        IDs of the hosts on which new Oozie Servers will be added.

        :return: The new_oozie_server_host_ids of this ApiEnableOozieHaArguments.
        :rtype: list[str]
        """
        return self._new_oozie_server_host_ids

    @new_oozie_server_host_ids.setter
    def new_oozie_server_host_ids(self, new_oozie_server_host_ids):
        """
        Sets the new_oozie_server_host_ids of this ApiEnableOozieHaArguments.
        IDs of the hosts on which new Oozie Servers will be added.

        :param new_oozie_server_host_ids: The new_oozie_server_host_ids of this ApiEnableOozieHaArguments.
        :type: list[str]
        """

        self._new_oozie_server_host_ids = new_oozie_server_host_ids

    @property
    def new_oozie_server_role_names(self):
        """
        Gets the new_oozie_server_role_names of this ApiEnableOozieHaArguments.
        Names of the new Oozie Servers. This is an optional argument, but if provided, it should match the length of host IDs provided.

        :return: The new_oozie_server_role_names of this ApiEnableOozieHaArguments.
        :rtype: list[str]
        """
        return self._new_oozie_server_role_names

    @new_oozie_server_role_names.setter
    def new_oozie_server_role_names(self, new_oozie_server_role_names):
        """
        Sets the new_oozie_server_role_names of this ApiEnableOozieHaArguments.
        Names of the new Oozie Servers. This is an optional argument, but if provided, it should match the length of host IDs provided.

        :param new_oozie_server_role_names: The new_oozie_server_role_names of this ApiEnableOozieHaArguments.
        :type: list[str]
        """

        self._new_oozie_server_role_names = new_oozie_server_role_names

    @property
    def zk_service_name(self):
        """
        Gets the zk_service_name of this ApiEnableOozieHaArguments.
        Name of the ZooKeeper service that will be used for Oozie HA. This is an optional parameter if the Oozie to ZooKeeper dependency is already set in CM.

        :return: The zk_service_name of this ApiEnableOozieHaArguments.
        :rtype: str
        """
        return self._zk_service_name

    @zk_service_name.setter
    def zk_service_name(self, zk_service_name):
        """
        Sets the zk_service_name of this ApiEnableOozieHaArguments.
        Name of the ZooKeeper service that will be used for Oozie HA. This is an optional parameter if the Oozie to ZooKeeper dependency is already set in CM.

        :param zk_service_name: The zk_service_name of this ApiEnableOozieHaArguments.
        :type: str
        """

        self._zk_service_name = zk_service_name

    @property
    def load_balancer_hostname(self):
        """
        Gets the load_balancer_hostname of this ApiEnableOozieHaArguments.
        Hostname of the load balancer used for Oozie HA. Optional if load balancer host and ports are already set in CM.

        :return: The load_balancer_hostname of this ApiEnableOozieHaArguments.
        :rtype: str
        """
        return self._load_balancer_hostname

    @load_balancer_hostname.setter
    def load_balancer_hostname(self, load_balancer_hostname):
        """
        Sets the load_balancer_hostname of this ApiEnableOozieHaArguments.
        Hostname of the load balancer used for Oozie HA. Optional if load balancer host and ports are already set in CM.

        :param load_balancer_hostname: The load_balancer_hostname of this ApiEnableOozieHaArguments.
        :type: str
        """

        self._load_balancer_hostname = load_balancer_hostname

    @property
    def load_balancer_port(self):
        """
        Gets the load_balancer_port of this ApiEnableOozieHaArguments.
        HTTP port of the load balancer used for Oozie HA. Optional if load balancer host and ports are already set in CM.

        :return: The load_balancer_port of this ApiEnableOozieHaArguments.
        :rtype: float
        """
        return self._load_balancer_port

    @load_balancer_port.setter
    def load_balancer_port(self, load_balancer_port):
        """
        Sets the load_balancer_port of this ApiEnableOozieHaArguments.
        HTTP port of the load balancer used for Oozie HA. Optional if load balancer host and ports are already set in CM.

        :param load_balancer_port: The load_balancer_port of this ApiEnableOozieHaArguments.
        :type: float
        """

        self._load_balancer_port = load_balancer_port

    @property
    def load_balancer_ssl_port(self):
        """
        Gets the load_balancer_ssl_port of this ApiEnableOozieHaArguments.
        HTTPS port of the load balancer used for Oozie HA when SSL is enabled. This port is only used for oozie.base.url -- the callback is always on HTTP. Optional if load balancer host and ports are already set in CM.

        :return: The load_balancer_ssl_port of this ApiEnableOozieHaArguments.
        :rtype: float
        """
        return self._load_balancer_ssl_port

    @load_balancer_ssl_port.setter
    def load_balancer_ssl_port(self, load_balancer_ssl_port):
        """
        Sets the load_balancer_ssl_port of this ApiEnableOozieHaArguments.
        HTTPS port of the load balancer used for Oozie HA when SSL is enabled. This port is only used for oozie.base.url -- the callback is always on HTTP. Optional if load balancer host and ports are already set in CM.

        :param load_balancer_ssl_port: The load_balancer_ssl_port of this ApiEnableOozieHaArguments.
        :type: float
        """

        self._load_balancer_ssl_port = load_balancer_ssl_port

    @property
    def load_balancer_host_port(self):
        """
        Gets the load_balancer_host_port of this ApiEnableOozieHaArguments.
        Address of the load balancer used for Oozie HA. This is an optional parameter if this config is already set in CM.

        :return: The load_balancer_host_port of this ApiEnableOozieHaArguments.
        :rtype: str
        """
        return self._load_balancer_host_port

    @load_balancer_host_port.setter
    def load_balancer_host_port(self, load_balancer_host_port):
        """
        Sets the load_balancer_host_port of this ApiEnableOozieHaArguments.
        Address of the load balancer used for Oozie HA. This is an optional parameter if this config is already set in CM.

        :param load_balancer_host_port: The load_balancer_host_port of this ApiEnableOozieHaArguments.
        :type: str
        """

        self._load_balancer_host_port = load_balancer_host_port

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiEnableOozieHaArguments):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
