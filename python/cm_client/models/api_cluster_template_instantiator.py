# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiClusterTemplateInstantiator(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'cluster_name': 'str',
        'hosts': 'list[ApiClusterTemplateHostInfo]',
        'variables': 'list[ApiClusterTemplateVariable]',
        'role_config_groups': 'list[ApiClusterTemplateRoleConfigGroupInfo]'
    }

    attribute_map = {
        'cluster_name': 'clusterName',
        'hosts': 'hosts',
        'variables': 'variables',
        'role_config_groups': 'roleConfigGroups'
    }

    def __init__(self, cluster_name=None, hosts=None, variables=None, role_config_groups=None):
        """
        ApiClusterTemplateInstantiator - a model defined in Swagger
        """

        self._cluster_name = None
        self._hosts = None
        self._variables = None
        self._role_config_groups = None

        if cluster_name is not None:
          self.cluster_name = cluster_name
        if hosts is not None:
          self.hosts = hosts
        if variables is not None:
          self.variables = variables
        if role_config_groups is not None:
          self.role_config_groups = role_config_groups

    @property
    def cluster_name(self):
        """
        Gets the cluster_name of this ApiClusterTemplateInstantiator.
        

        :return: The cluster_name of this ApiClusterTemplateInstantiator.
        :rtype: str
        """
        return self._cluster_name

    @cluster_name.setter
    def cluster_name(self, cluster_name):
        """
        Sets the cluster_name of this ApiClusterTemplateInstantiator.
        

        :param cluster_name: The cluster_name of this ApiClusterTemplateInstantiator.
        :type: str
        """

        self._cluster_name = cluster_name

    @property
    def hosts(self):
        """
        Gets the hosts of this ApiClusterTemplateInstantiator.
        

        :return: The hosts of this ApiClusterTemplateInstantiator.
        :rtype: list[ApiClusterTemplateHostInfo]
        """
        return self._hosts

    @hosts.setter
    def hosts(self, hosts):
        """
        Sets the hosts of this ApiClusterTemplateInstantiator.
        

        :param hosts: The hosts of this ApiClusterTemplateInstantiator.
        :type: list[ApiClusterTemplateHostInfo]
        """

        self._hosts = hosts

    @property
    def variables(self):
        """
        Gets the variables of this ApiClusterTemplateInstantiator.
        

        :return: The variables of this ApiClusterTemplateInstantiator.
        :rtype: list[ApiClusterTemplateVariable]
        """
        return self._variables

    @variables.setter
    def variables(self, variables):
        """
        Sets the variables of this ApiClusterTemplateInstantiator.
        

        :param variables: The variables of this ApiClusterTemplateInstantiator.
        :type: list[ApiClusterTemplateVariable]
        """

        self._variables = variables

    @property
    def role_config_groups(self):
        """
        Gets the role_config_groups of this ApiClusterTemplateInstantiator.
        

        :return: The role_config_groups of this ApiClusterTemplateInstantiator.
        :rtype: list[ApiClusterTemplateRoleConfigGroupInfo]
        """
        return self._role_config_groups

    @role_config_groups.setter
    def role_config_groups(self, role_config_groups):
        """
        Sets the role_config_groups of this ApiClusterTemplateInstantiator.
        

        :param role_config_groups: The role_config_groups of this ApiClusterTemplateInstantiator.
        :type: list[ApiClusterTemplateRoleConfigGroupInfo]
        """

        self._role_config_groups = role_config_groups

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiClusterTemplateInstantiator):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
