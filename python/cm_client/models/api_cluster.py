# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiCluster(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'display_name': 'str',
        'version': 'ApiClusterVersion',
        'full_version': 'str',
        'maintenance_mode': 'bool',
        'maintenance_owners': 'list[ApiEntityType]',
        'services': 'list[ApiService]',
        'parcels': 'list[ApiParcel]',
        'cluster_url': 'str',
        'hosts_url': 'str',
        'entity_status': 'ApiEntityStatus',
        'uuid': 'str'
    }

    attribute_map = {
        'name': 'name',
        'display_name': 'displayName',
        'version': 'version',
        'full_version': 'fullVersion',
        'maintenance_mode': 'maintenanceMode',
        'maintenance_owners': 'maintenanceOwners',
        'services': 'services',
        'parcels': 'parcels',
        'cluster_url': 'clusterUrl',
        'hosts_url': 'hostsUrl',
        'entity_status': 'entityStatus',
        'uuid': 'uuid'
    }

    def __init__(self, name=None, display_name=None, version=None, full_version=None, maintenance_mode=None, maintenance_owners=None, services=None, parcels=None, cluster_url=None, hosts_url=None, entity_status=None, uuid=None):
        """
        ApiCluster - a model defined in Swagger
        """

        self._name = None
        self._display_name = None
        self._version = None
        self._full_version = None
        self._maintenance_mode = None
        self._maintenance_owners = None
        self._services = None
        self._parcels = None
        self._cluster_url = None
        self._hosts_url = None
        self._entity_status = None
        self._uuid = None

        if name is not None:
          self.name = name
        if display_name is not None:
          self.display_name = display_name
        if version is not None:
          self.version = version
        if full_version is not None:
          self.full_version = full_version
        if maintenance_mode is not None:
          self.maintenance_mode = maintenance_mode
        if maintenance_owners is not None:
          self.maintenance_owners = maintenance_owners
        if services is not None:
          self.services = services
        if parcels is not None:
          self.parcels = parcels
        if cluster_url is not None:
          self.cluster_url = cluster_url
        if hosts_url is not None:
          self.hosts_url = hosts_url
        if entity_status is not None:
          self.entity_status = entity_status
        if uuid is not None:
          self.uuid = uuid

    @property
    def name(self):
        """
        Gets the name of this ApiCluster.
        The name of the cluster. <p> Immutable since API v6. <p> Prior to API v6, will contain the display name of the cluster.

        :return: The name of this ApiCluster.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ApiCluster.
        The name of the cluster. <p> Immutable since API v6. <p> Prior to API v6, will contain the display name of the cluster.

        :param name: The name of this ApiCluster.
        :type: str
        """

        self._name = name

    @property
    def display_name(self):
        """
        Gets the display_name of this ApiCluster.
        The display name of the cluster that is shown in the UI. <p> Available since API v6.

        :return: The display_name of this ApiCluster.
        :rtype: str
        """
        return self._display_name

    @display_name.setter
    def display_name(self, display_name):
        """
        Sets the display_name of this ApiCluster.
        The display name of the cluster that is shown in the UI. <p> Available since API v6.

        :param display_name: The display_name of this ApiCluster.
        :type: str
        """

        self._display_name = display_name

    @property
    def version(self):
        """
        Gets the version of this ApiCluster.
        The CDH version of the cluster.

        :return: The version of this ApiCluster.
        :rtype: ApiClusterVersion
        """
        return self._version

    @version.setter
    def version(self, version):
        """
        Sets the version of this ApiCluster.
        The CDH version of the cluster.

        :param version: The version of this ApiCluster.
        :type: ApiClusterVersion
        """

        self._version = version

    @property
    def full_version(self):
        """
        Gets the full_version of this ApiCluster.
        The full CDH version of the cluster. The expected format is three dot separated version numbers, e.g. \"4.2.1\" or \"5.0.0\". The full version takes precedence over the version field during cluster creation. <p> Available since API v6.

        :return: The full_version of this ApiCluster.
        :rtype: str
        """
        return self._full_version

    @full_version.setter
    def full_version(self, full_version):
        """
        Sets the full_version of this ApiCluster.
        The full CDH version of the cluster. The expected format is three dot separated version numbers, e.g. \"4.2.1\" or \"5.0.0\". The full version takes precedence over the version field during cluster creation. <p> Available since API v6.

        :param full_version: The full_version of this ApiCluster.
        :type: str
        """

        self._full_version = full_version

    @property
    def maintenance_mode(self):
        """
        Gets the maintenance_mode of this ApiCluster.
        Readonly. Whether the cluster is in maintenance mode. Available since API v2.

        :return: The maintenance_mode of this ApiCluster.
        :rtype: bool
        """
        return self._maintenance_mode

    @maintenance_mode.setter
    def maintenance_mode(self, maintenance_mode):
        """
        Sets the maintenance_mode of this ApiCluster.
        Readonly. Whether the cluster is in maintenance mode. Available since API v2.

        :param maintenance_mode: The maintenance_mode of this ApiCluster.
        :type: bool
        """

        self._maintenance_mode = maintenance_mode

    @property
    def maintenance_owners(self):
        """
        Gets the maintenance_owners of this ApiCluster.
        Readonly. The list of objects that trigger this cluster to be in maintenance mode. Available since API v2.

        :return: The maintenance_owners of this ApiCluster.
        :rtype: list[ApiEntityType]
        """
        return self._maintenance_owners

    @maintenance_owners.setter
    def maintenance_owners(self, maintenance_owners):
        """
        Sets the maintenance_owners of this ApiCluster.
        Readonly. The list of objects that trigger this cluster to be in maintenance mode. Available since API v2.

        :param maintenance_owners: The maintenance_owners of this ApiCluster.
        :type: list[ApiEntityType]
        """

        self._maintenance_owners = maintenance_owners

    @property
    def services(self):
        """
        Gets the services of this ApiCluster.
        Optional. Used during import/export of settings.

        :return: The services of this ApiCluster.
        :rtype: list[ApiService]
        """
        return self._services

    @services.setter
    def services(self, services):
        """
        Sets the services of this ApiCluster.
        Optional. Used during import/export of settings.

        :param services: The services of this ApiCluster.
        :type: list[ApiService]
        """

        self._services = services

    @property
    def parcels(self):
        """
        Gets the parcels of this ApiCluster.
        Optional. Used during import/export of settings. Available since API v4.

        :return: The parcels of this ApiCluster.
        :rtype: list[ApiParcel]
        """
        return self._parcels

    @parcels.setter
    def parcels(self, parcels):
        """
        Sets the parcels of this ApiCluster.
        Optional. Used during import/export of settings. Available since API v4.

        :param parcels: The parcels of this ApiCluster.
        :type: list[ApiParcel]
        """

        self._parcels = parcels

    @property
    def cluster_url(self):
        """
        Gets the cluster_url of this ApiCluster.
        Readonly. Link into the Cloudera Manager web UI for this specific cluster. <p> Available since API v10.

        :return: The cluster_url of this ApiCluster.
        :rtype: str
        """
        return self._cluster_url

    @cluster_url.setter
    def cluster_url(self, cluster_url):
        """
        Sets the cluster_url of this ApiCluster.
        Readonly. Link into the Cloudera Manager web UI for this specific cluster. <p> Available since API v10.

        :param cluster_url: The cluster_url of this ApiCluster.
        :type: str
        """

        self._cluster_url = cluster_url

    @property
    def hosts_url(self):
        """
        Gets the hosts_url of this ApiCluster.
        Readonly. Link into the Cloudera Manager web UI for host table for this cluster. <p> Available since API v11.

        :return: The hosts_url of this ApiCluster.
        :rtype: str
        """
        return self._hosts_url

    @hosts_url.setter
    def hosts_url(self, hosts_url):
        """
        Sets the hosts_url of this ApiCluster.
        Readonly. Link into the Cloudera Manager web UI for host table for this cluster. <p> Available since API v11.

        :param hosts_url: The hosts_url of this ApiCluster.
        :type: str
        """

        self._hosts_url = hosts_url

    @property
    def entity_status(self):
        """
        Gets the entity_status of this ApiCluster.
        Readonly. The entity status for this cluster. Available since API v11.

        :return: The entity_status of this ApiCluster.
        :rtype: ApiEntityStatus
        """
        return self._entity_status

    @entity_status.setter
    def entity_status(self, entity_status):
        """
        Sets the entity_status of this ApiCluster.
        Readonly. The entity status for this cluster. Available since API v11.

        :param entity_status: The entity_status of this ApiCluster.
        :type: ApiEntityStatus
        """

        self._entity_status = entity_status

    @property
    def uuid(self):
        """
        Gets the uuid of this ApiCluster.
        Readonly. The UUID of the cluster. <p> Available since API v15.

        :return: The uuid of this ApiCluster.
        :rtype: str
        """
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        """
        Sets the uuid of this ApiCluster.
        Readonly. The UUID of the cluster. <p> Available since API v15.

        :param uuid: The uuid of this ApiCluster.
        :type: str
        """

        self._uuid = uuid

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiCluster):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
