# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiUser(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'password': 'str',
        'roles': 'list[str]',
        'pw_hash': 'str',
        'pw_salt': 'float',
        'pw_login': 'bool'
    }

    attribute_map = {
        'name': 'name',
        'password': 'password',
        'roles': 'roles',
        'pw_hash': 'pwHash',
        'pw_salt': 'pwSalt',
        'pw_login': 'pwLogin'
    }

    def __init__(self, name=None, password=None, roles=None, pw_hash=None, pw_salt=None, pw_login=None):
        """
        ApiUser - a model defined in Swagger
        """

        self._name = None
        self._password = None
        self._roles = None
        self._pw_hash = None
        self._pw_salt = None
        self._pw_login = None

        if name is not None:
          self.name = name
        if password is not None:
          self.password = password
        if roles is not None:
          self.roles = roles
        if pw_hash is not None:
          self.pw_hash = pw_hash
        if pw_salt is not None:
          self.pw_salt = pw_salt
        if pw_login is not None:
          self.pw_login = pw_login

    @property
    def name(self):
        """
        Gets the name of this ApiUser.
        The username, which is unique within a Cloudera Manager installation.

        :return: The name of this ApiUser.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ApiUser.
        The username, which is unique within a Cloudera Manager installation.

        :param name: The name of this ApiUser.
        :type: str
        """

        self._name = name

    @property
    def password(self):
        """
        Gets the password of this ApiUser.
        Returns the user password. <p> Passwords are not returned when querying user information, so this property will always be empty when reading information from a server.

        :return: The password of this ApiUser.
        :rtype: str
        """
        return self._password

    @password.setter
    def password(self, password):
        """
        Sets the password of this ApiUser.
        Returns the user password. <p> Passwords are not returned when querying user information, so this property will always be empty when reading information from a server.

        :param password: The password of this ApiUser.
        :type: str
        """

        self._password = password

    @property
    def roles(self):
        """
        Gets the roles of this ApiUser.
        A list of roles this user belongs to. <p> In Cloudera Express, possible values are: <ul> <li><b>ROLE_ADMIN</b></li> <li><b>ROLE_USER</b></li> </ul> In Cloudera Enterprise Datahub Edition, additional possible values are: <ul> <li><b>ROLE_LIMITED</b>: Added in Cloudera Manager 5.0</li> <li><b>ROLE_OPERATOR</b>: Added in Cloudera Manager 5.1</li> <li><b>ROLE_CONFIGURATOR</b>: Added in Cloudera Manager 5.1</li> <li><b>ROLE_CLUSTER_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_BDR_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_NAVIGATOR_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_USER_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_KEY_ADMIN</b>: Added in Cloudera Manager 5.5</li> </ul> An empty list implies ROLE_USER. <p> Note that although this interface provides a list of roles, a user should only be assigned a single role at a time.

        :return: The roles of this ApiUser.
        :rtype: list[str]
        """
        return self._roles

    @roles.setter
    def roles(self, roles):
        """
        Sets the roles of this ApiUser.
        A list of roles this user belongs to. <p> In Cloudera Express, possible values are: <ul> <li><b>ROLE_ADMIN</b></li> <li><b>ROLE_USER</b></li> </ul> In Cloudera Enterprise Datahub Edition, additional possible values are: <ul> <li><b>ROLE_LIMITED</b>: Added in Cloudera Manager 5.0</li> <li><b>ROLE_OPERATOR</b>: Added in Cloudera Manager 5.1</li> <li><b>ROLE_CONFIGURATOR</b>: Added in Cloudera Manager 5.1</li> <li><b>ROLE_CLUSTER_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_BDR_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_NAVIGATOR_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_USER_ADMIN</b>: Added in Cloudera Manager 5.2</li> <li><b>ROLE_KEY_ADMIN</b>: Added in Cloudera Manager 5.5</li> </ul> An empty list implies ROLE_USER. <p> Note that although this interface provides a list of roles, a user should only be assigned a single role at a time.

        :param roles: The roles of this ApiUser.
        :type: list[str]
        """

        self._roles = roles

    @property
    def pw_hash(self):
        """
        Gets the pw_hash of this ApiUser.
        NOTE: Only available in the \"export\" view

        :return: The pw_hash of this ApiUser.
        :rtype: str
        """
        return self._pw_hash

    @pw_hash.setter
    def pw_hash(self, pw_hash):
        """
        Sets the pw_hash of this ApiUser.
        NOTE: Only available in the \"export\" view

        :param pw_hash: The pw_hash of this ApiUser.
        :type: str
        """

        self._pw_hash = pw_hash

    @property
    def pw_salt(self):
        """
        Gets the pw_salt of this ApiUser.
        NOTE: Only available in the \"export\" view

        :return: The pw_salt of this ApiUser.
        :rtype: float
        """
        return self._pw_salt

    @pw_salt.setter
    def pw_salt(self, pw_salt):
        """
        Sets the pw_salt of this ApiUser.
        NOTE: Only available in the \"export\" view

        :param pw_salt: The pw_salt of this ApiUser.
        :type: float
        """

        self._pw_salt = pw_salt

    @property
    def pw_login(self):
        """
        Gets the pw_login of this ApiUser.
        NOTE: Only available in the \"export\" view

        :return: The pw_login of this ApiUser.
        :rtype: bool
        """
        return self._pw_login

    @pw_login.setter
    def pw_login(self, pw_login):
        """
        Sets the pw_login of this ApiUser.
        NOTE: Only available in the \"export\" view

        :param pw_login: The pw_login of this ApiUser.
        :type: bool
        """

        self._pw_login = pw_login

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiUser):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
