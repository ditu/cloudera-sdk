# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiImpalaUtilizationHistogram(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'bins': 'ApiImpalaUtilizationHistogramBinList'
    }

    attribute_map = {
        'bins': 'bins'
    }

    def __init__(self, bins=None):
        """
        ApiImpalaUtilizationHistogram - a model defined in Swagger
        """

        self._bins = None

        if bins is not None:
          self.bins = bins

    @property
    def bins(self):
        """
        Gets the bins of this ApiImpalaUtilizationHistogram.
        Bins of the histogram.

        :return: The bins of this ApiImpalaUtilizationHistogram.
        :rtype: ApiImpalaUtilizationHistogramBinList
        """
        return self._bins

    @bins.setter
    def bins(self, bins):
        """
        Sets the bins of this ApiImpalaUtilizationHistogram.
        Bins of the histogram.

        :param bins: The bins of this ApiImpalaUtilizationHistogram.
        :type: ApiImpalaUtilizationHistogramBinList
        """

        self._bins = bins

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiImpalaUtilizationHistogram):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
