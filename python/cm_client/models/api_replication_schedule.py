# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiReplicationSchedule(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'float',
        'display_name': 'str',
        'description': 'str',
        'start_time': 'str',
        'end_time': 'str',
        'interval': 'float',
        'interval_unit': 'ApiScheduleInterval',
        'next_run': 'str',
        'paused': 'bool',
        'alert_on_start': 'bool',
        'alert_on_success': 'bool',
        'alert_on_fail': 'bool',
        'alert_on_abort': 'bool',
        'hdfs_arguments': 'ApiHdfsReplicationArguments',
        'hive_arguments': 'ApiHiveReplicationArguments',
        'hdfs_cloud_arguments': 'ApiHdfsCloudReplicationArguments',
        'history': 'list[ApiReplicationCommand]',
        'active': 'bool',
        'hive_cloud_arguments': 'ApiHiveCloudReplicationArguments'
    }

    attribute_map = {
        'id': 'id',
        'display_name': 'displayName',
        'description': 'description',
        'start_time': 'startTime',
        'end_time': 'endTime',
        'interval': 'interval',
        'interval_unit': 'intervalUnit',
        'next_run': 'nextRun',
        'paused': 'paused',
        'alert_on_start': 'alertOnStart',
        'alert_on_success': 'alertOnSuccess',
        'alert_on_fail': 'alertOnFail',
        'alert_on_abort': 'alertOnAbort',
        'hdfs_arguments': 'hdfsArguments',
        'hive_arguments': 'hiveArguments',
        'hdfs_cloud_arguments': 'hdfsCloudArguments',
        'history': 'history',
        'active': 'active',
        'hive_cloud_arguments': 'hiveCloudArguments'
    }

    def __init__(self, id=None, display_name=None, description=None, start_time=None, end_time=None, interval=None, interval_unit=None, next_run=None, paused=None, alert_on_start=None, alert_on_success=None, alert_on_fail=None, alert_on_abort=None, hdfs_arguments=None, hive_arguments=None, hdfs_cloud_arguments=None, history=None, active=None, hive_cloud_arguments=None):
        """
        ApiReplicationSchedule - a model defined in Swagger
        """

        self._id = None
        self._display_name = None
        self._description = None
        self._start_time = None
        self._end_time = None
        self._interval = None
        self._interval_unit = None
        self._next_run = None
        self._paused = None
        self._alert_on_start = None
        self._alert_on_success = None
        self._alert_on_fail = None
        self._alert_on_abort = None
        self._hdfs_arguments = None
        self._hive_arguments = None
        self._hdfs_cloud_arguments = None
        self._history = None
        self._active = None
        self._hive_cloud_arguments = None

        if id is not None:
          self.id = id
        if display_name is not None:
          self.display_name = display_name
        if description is not None:
          self.description = description
        if start_time is not None:
          self.start_time = start_time
        if end_time is not None:
          self.end_time = end_time
        if interval is not None:
          self.interval = interval
        if interval_unit is not None:
          self.interval_unit = interval_unit
        if next_run is not None:
          self.next_run = next_run
        if paused is not None:
          self.paused = paused
        if alert_on_start is not None:
          self.alert_on_start = alert_on_start
        if alert_on_success is not None:
          self.alert_on_success = alert_on_success
        if alert_on_fail is not None:
          self.alert_on_fail = alert_on_fail
        if alert_on_abort is not None:
          self.alert_on_abort = alert_on_abort
        if hdfs_arguments is not None:
          self.hdfs_arguments = hdfs_arguments
        if hive_arguments is not None:
          self.hive_arguments = hive_arguments
        if hdfs_cloud_arguments is not None:
          self.hdfs_cloud_arguments = hdfs_cloud_arguments
        if history is not None:
          self.history = history
        if active is not None:
          self.active = active
        if hive_cloud_arguments is not None:
          self.hive_cloud_arguments = hive_cloud_arguments

    @property
    def id(self):
        """
        Gets the id of this ApiReplicationSchedule.
        The schedule id.

        :return: The id of this ApiReplicationSchedule.
        :rtype: float
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this ApiReplicationSchedule.
        The schedule id.

        :param id: The id of this ApiReplicationSchedule.
        :type: float
        """

        self._id = id

    @property
    def display_name(self):
        """
        Gets the display_name of this ApiReplicationSchedule.
        The schedule display name.

        :return: The display_name of this ApiReplicationSchedule.
        :rtype: str
        """
        return self._display_name

    @display_name.setter
    def display_name(self, display_name):
        """
        Sets the display_name of this ApiReplicationSchedule.
        The schedule display name.

        :param display_name: The display_name of this ApiReplicationSchedule.
        :type: str
        """

        self._display_name = display_name

    @property
    def description(self):
        """
        Gets the description of this ApiReplicationSchedule.
        The schedule description.

        :return: The description of this ApiReplicationSchedule.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """
        Sets the description of this ApiReplicationSchedule.
        The schedule description.

        :param description: The description of this ApiReplicationSchedule.
        :type: str
        """

        self._description = description

    @property
    def start_time(self):
        """
        Gets the start_time of this ApiReplicationSchedule.
        The time at which the scheduled activity is triggered for the first time.

        :return: The start_time of this ApiReplicationSchedule.
        :rtype: str
        """
        return self._start_time

    @start_time.setter
    def start_time(self, start_time):
        """
        Sets the start_time of this ApiReplicationSchedule.
        The time at which the scheduled activity is triggered for the first time.

        :param start_time: The start_time of this ApiReplicationSchedule.
        :type: str
        """

        self._start_time = start_time

    @property
    def end_time(self):
        """
        Gets the end_time of this ApiReplicationSchedule.
        The time after which the scheduled activity will no longer be triggered.

        :return: The end_time of this ApiReplicationSchedule.
        :rtype: str
        """
        return self._end_time

    @end_time.setter
    def end_time(self, end_time):
        """
        Sets the end_time of this ApiReplicationSchedule.
        The time after which the scheduled activity will no longer be triggered.

        :param end_time: The end_time of this ApiReplicationSchedule.
        :type: str
        """

        self._end_time = end_time

    @property
    def interval(self):
        """
        Gets the interval of this ApiReplicationSchedule.
        The duration between consecutive triggers of a scheduled activity.

        :return: The interval of this ApiReplicationSchedule.
        :rtype: float
        """
        return self._interval

    @interval.setter
    def interval(self, interval):
        """
        Sets the interval of this ApiReplicationSchedule.
        The duration between consecutive triggers of a scheduled activity.

        :param interval: The interval of this ApiReplicationSchedule.
        :type: float
        """

        self._interval = interval

    @property
    def interval_unit(self):
        """
        Gets the interval_unit of this ApiReplicationSchedule.
        The unit for the repeat interval.

        :return: The interval_unit of this ApiReplicationSchedule.
        :rtype: ApiScheduleInterval
        """
        return self._interval_unit

    @interval_unit.setter
    def interval_unit(self, interval_unit):
        """
        Sets the interval_unit of this ApiReplicationSchedule.
        The unit for the repeat interval.

        :param interval_unit: The interval_unit of this ApiReplicationSchedule.
        :type: ApiScheduleInterval
        """

        self._interval_unit = interval_unit

    @property
    def next_run(self):
        """
        Gets the next_run of this ApiReplicationSchedule.
        Readonly. The time the scheduled command will run next.

        :return: The next_run of this ApiReplicationSchedule.
        :rtype: str
        """
        return self._next_run

    @next_run.setter
    def next_run(self, next_run):
        """
        Sets the next_run of this ApiReplicationSchedule.
        Readonly. The time the scheduled command will run next.

        :param next_run: The next_run of this ApiReplicationSchedule.
        :type: str
        """

        self._next_run = next_run

    @property
    def paused(self):
        """
        Gets the paused of this ApiReplicationSchedule.
        The paused state for the schedule. The scheduled activity will not be triggered as long as the scheduled is paused.

        :return: The paused of this ApiReplicationSchedule.
        :rtype: bool
        """
        return self._paused

    @paused.setter
    def paused(self, paused):
        """
        Sets the paused of this ApiReplicationSchedule.
        The paused state for the schedule. The scheduled activity will not be triggered as long as the scheduled is paused.

        :param paused: The paused of this ApiReplicationSchedule.
        :type: bool
        """

        self._paused = paused

    @property
    def alert_on_start(self):
        """
        Gets the alert_on_start of this ApiReplicationSchedule.
        Whether to alert on start of the scheduled activity.

        :return: The alert_on_start of this ApiReplicationSchedule.
        :rtype: bool
        """
        return self._alert_on_start

    @alert_on_start.setter
    def alert_on_start(self, alert_on_start):
        """
        Sets the alert_on_start of this ApiReplicationSchedule.
        Whether to alert on start of the scheduled activity.

        :param alert_on_start: The alert_on_start of this ApiReplicationSchedule.
        :type: bool
        """

        self._alert_on_start = alert_on_start

    @property
    def alert_on_success(self):
        """
        Gets the alert_on_success of this ApiReplicationSchedule.
        Whether to alert on successful completion of the scheduled activity.

        :return: The alert_on_success of this ApiReplicationSchedule.
        :rtype: bool
        """
        return self._alert_on_success

    @alert_on_success.setter
    def alert_on_success(self, alert_on_success):
        """
        Sets the alert_on_success of this ApiReplicationSchedule.
        Whether to alert on successful completion of the scheduled activity.

        :param alert_on_success: The alert_on_success of this ApiReplicationSchedule.
        :type: bool
        """

        self._alert_on_success = alert_on_success

    @property
    def alert_on_fail(self):
        """
        Gets the alert_on_fail of this ApiReplicationSchedule.
        Whether to alert on failure of the scheduled activity.

        :return: The alert_on_fail of this ApiReplicationSchedule.
        :rtype: bool
        """
        return self._alert_on_fail

    @alert_on_fail.setter
    def alert_on_fail(self, alert_on_fail):
        """
        Sets the alert_on_fail of this ApiReplicationSchedule.
        Whether to alert on failure of the scheduled activity.

        :param alert_on_fail: The alert_on_fail of this ApiReplicationSchedule.
        :type: bool
        """

        self._alert_on_fail = alert_on_fail

    @property
    def alert_on_abort(self):
        """
        Gets the alert_on_abort of this ApiReplicationSchedule.
        Whether to alert on abort of the scheduled activity.

        :return: The alert_on_abort of this ApiReplicationSchedule.
        :rtype: bool
        """
        return self._alert_on_abort

    @alert_on_abort.setter
    def alert_on_abort(self, alert_on_abort):
        """
        Sets the alert_on_abort of this ApiReplicationSchedule.
        Whether to alert on abort of the scheduled activity.

        :param alert_on_abort: The alert_on_abort of this ApiReplicationSchedule.
        :type: bool
        """

        self._alert_on_abort = alert_on_abort

    @property
    def hdfs_arguments(self):
        """
        Gets the hdfs_arguments of this ApiReplicationSchedule.
        Arguments for HDFS replication commands.

        :return: The hdfs_arguments of this ApiReplicationSchedule.
        :rtype: ApiHdfsReplicationArguments
        """
        return self._hdfs_arguments

    @hdfs_arguments.setter
    def hdfs_arguments(self, hdfs_arguments):
        """
        Sets the hdfs_arguments of this ApiReplicationSchedule.
        Arguments for HDFS replication commands.

        :param hdfs_arguments: The hdfs_arguments of this ApiReplicationSchedule.
        :type: ApiHdfsReplicationArguments
        """

        self._hdfs_arguments = hdfs_arguments

    @property
    def hive_arguments(self):
        """
        Gets the hive_arguments of this ApiReplicationSchedule.
        Arguments for Hive replication commands.

        :return: The hive_arguments of this ApiReplicationSchedule.
        :rtype: ApiHiveReplicationArguments
        """
        return self._hive_arguments

    @hive_arguments.setter
    def hive_arguments(self, hive_arguments):
        """
        Sets the hive_arguments of this ApiReplicationSchedule.
        Arguments for Hive replication commands.

        :param hive_arguments: The hive_arguments of this ApiReplicationSchedule.
        :type: ApiHiveReplicationArguments
        """

        self._hive_arguments = hive_arguments

    @property
    def hdfs_cloud_arguments(self):
        """
        Gets the hdfs_cloud_arguments of this ApiReplicationSchedule.
        Arguments for HDFS cloud replication commands.

        :return: The hdfs_cloud_arguments of this ApiReplicationSchedule.
        :rtype: ApiHdfsCloudReplicationArguments
        """
        return self._hdfs_cloud_arguments

    @hdfs_cloud_arguments.setter
    def hdfs_cloud_arguments(self, hdfs_cloud_arguments):
        """
        Sets the hdfs_cloud_arguments of this ApiReplicationSchedule.
        Arguments for HDFS cloud replication commands.

        :param hdfs_cloud_arguments: The hdfs_cloud_arguments of this ApiReplicationSchedule.
        :type: ApiHdfsCloudReplicationArguments
        """

        self._hdfs_cloud_arguments = hdfs_cloud_arguments

    @property
    def history(self):
        """
        Gets the history of this ApiReplicationSchedule.
        List of active and/or finished commands for this schedule.

        :return: The history of this ApiReplicationSchedule.
        :rtype: list[ApiReplicationCommand]
        """
        return self._history

    @history.setter
    def history(self, history):
        """
        Sets the history of this ApiReplicationSchedule.
        List of active and/or finished commands for this schedule.

        :param history: The history of this ApiReplicationSchedule.
        :type: list[ApiReplicationCommand]
        """

        self._history = history

    @property
    def active(self):
        """
        Gets the active of this ApiReplicationSchedule.
        Read-only field that is true if this schedule is currently active, false if not. Available since API v11.

        :return: The active of this ApiReplicationSchedule.
        :rtype: bool
        """
        return self._active

    @active.setter
    def active(self, active):
        """
        Sets the active of this ApiReplicationSchedule.
        Read-only field that is true if this schedule is currently active, false if not. Available since API v11.

        :param active: The active of this ApiReplicationSchedule.
        :type: bool
        """

        self._active = active

    @property
    def hive_cloud_arguments(self):
        """
        Gets the hive_cloud_arguments of this ApiReplicationSchedule.
        Arguments for Hive cloud replication commands.

        :return: The hive_cloud_arguments of this ApiReplicationSchedule.
        :rtype: ApiHiveCloudReplicationArguments
        """
        return self._hive_cloud_arguments

    @hive_cloud_arguments.setter
    def hive_cloud_arguments(self, hive_cloud_arguments):
        """
        Sets the hive_cloud_arguments of this ApiReplicationSchedule.
        Arguments for Hive cloud replication commands.

        :param hive_cloud_arguments: The hive_cloud_arguments of this ApiReplicationSchedule.
        :type: ApiHiveCloudReplicationArguments
        """

        self._hive_cloud_arguments = hive_cloud_arguments

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiReplicationSchedule):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
