# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiDeployment(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'timestamp': 'str',
        'clusters': 'list[ApiCluster]',
        'hosts': 'list[ApiHost]',
        'users': 'list[ApiUser]',
        'version_info': 'ApiVersionInfo',
        'management_service': 'ApiService',
        'manager_settings': 'ApiConfigList',
        'all_hosts_config': 'ApiConfigList',
        'peers': 'list[ApiCmPeer]',
        'host_templates': 'ApiHostTemplateList'
    }

    attribute_map = {
        'timestamp': 'timestamp',
        'clusters': 'clusters',
        'hosts': 'hosts',
        'users': 'users',
        'version_info': 'versionInfo',
        'management_service': 'managementService',
        'manager_settings': 'managerSettings',
        'all_hosts_config': 'allHostsConfig',
        'peers': 'peers',
        'host_templates': 'hostTemplates'
    }

    def __init__(self, timestamp=None, clusters=None, hosts=None, users=None, version_info=None, management_service=None, manager_settings=None, all_hosts_config=None, peers=None, host_templates=None):
        """
        ApiDeployment - a model defined in Swagger
        """

        self._timestamp = None
        self._clusters = None
        self._hosts = None
        self._users = None
        self._version_info = None
        self._management_service = None
        self._manager_settings = None
        self._all_hosts_config = None
        self._peers = None
        self._host_templates = None

        if timestamp is not None:
          self.timestamp = timestamp
        if clusters is not None:
          self.clusters = clusters
        if hosts is not None:
          self.hosts = hosts
        if users is not None:
          self.users = users
        if version_info is not None:
          self.version_info = version_info
        if management_service is not None:
          self.management_service = management_service
        if manager_settings is not None:
          self.manager_settings = manager_settings
        if all_hosts_config is not None:
          self.all_hosts_config = all_hosts_config
        if peers is not None:
          self.peers = peers
        if host_templates is not None:
          self.host_templates = host_templates

    @property
    def timestamp(self):
        """
        Gets the timestamp of this ApiDeployment.
        Readonly. This timestamp is provided when you request a deployment and is not required (or even read) when creating a deployment. This timestamp is useful if you have multiple deployments saved and want to determine which one to use as a restore point.

        :return: The timestamp of this ApiDeployment.
        :rtype: str
        """
        return self._timestamp

    @timestamp.setter
    def timestamp(self, timestamp):
        """
        Sets the timestamp of this ApiDeployment.
        Readonly. This timestamp is provided when you request a deployment and is not required (or even read) when creating a deployment. This timestamp is useful if you have multiple deployments saved and want to determine which one to use as a restore point.

        :param timestamp: The timestamp of this ApiDeployment.
        :type: str
        """

        self._timestamp = timestamp

    @property
    def clusters(self):
        """
        Gets the clusters of this ApiDeployment.
        List of clusters in the system including their services, roles and complete config values.

        :return: The clusters of this ApiDeployment.
        :rtype: list[ApiCluster]
        """
        return self._clusters

    @clusters.setter
    def clusters(self, clusters):
        """
        Sets the clusters of this ApiDeployment.
        List of clusters in the system including their services, roles and complete config values.

        :param clusters: The clusters of this ApiDeployment.
        :type: list[ApiCluster]
        """

        self._clusters = clusters

    @property
    def hosts(self):
        """
        Gets the hosts of this ApiDeployment.
        List of hosts in the system

        :return: The hosts of this ApiDeployment.
        :rtype: list[ApiHost]
        """
        return self._hosts

    @hosts.setter
    def hosts(self, hosts):
        """
        Sets the hosts of this ApiDeployment.
        List of hosts in the system

        :param hosts: The hosts of this ApiDeployment.
        :type: list[ApiHost]
        """

        self._hosts = hosts

    @property
    def users(self):
        """
        Gets the users of this ApiDeployment.
        List of all users in the system

        :return: The users of this ApiDeployment.
        :rtype: list[ApiUser]
        """
        return self._users

    @users.setter
    def users(self, users):
        """
        Sets the users of this ApiDeployment.
        List of all users in the system

        :param users: The users of this ApiDeployment.
        :type: list[ApiUser]
        """

        self._users = users

    @property
    def version_info(self):
        """
        Gets the version_info of this ApiDeployment.
        Full version information about the running Cloudera Manager instance

        :return: The version_info of this ApiDeployment.
        :rtype: ApiVersionInfo
        """
        return self._version_info

    @version_info.setter
    def version_info(self, version_info):
        """
        Sets the version_info of this ApiDeployment.
        Full version information about the running Cloudera Manager instance

        :param version_info: The version_info of this ApiDeployment.
        :type: ApiVersionInfo
        """

        self._version_info = version_info

    @property
    def management_service(self):
        """
        Gets the management_service of this ApiDeployment.
        The full configuration of the Cloudera Manager management service including all the management roles and their config values

        :return: The management_service of this ApiDeployment.
        :rtype: ApiService
        """
        return self._management_service

    @management_service.setter
    def management_service(self, management_service):
        """
        Sets the management_service of this ApiDeployment.
        The full configuration of the Cloudera Manager management service including all the management roles and their config values

        :param management_service: The management_service of this ApiDeployment.
        :type: ApiService
        """

        self._management_service = management_service

    @property
    def manager_settings(self):
        """
        Gets the manager_settings of this ApiDeployment.
        The full configuration of Cloudera Manager itself including licensing info

        :return: The manager_settings of this ApiDeployment.
        :rtype: ApiConfigList
        """
        return self._manager_settings

    @manager_settings.setter
    def manager_settings(self, manager_settings):
        """
        Sets the manager_settings of this ApiDeployment.
        The full configuration of Cloudera Manager itself including licensing info

        :param manager_settings: The manager_settings of this ApiDeployment.
        :type: ApiConfigList
        """

        self._manager_settings = manager_settings

    @property
    def all_hosts_config(self):
        """
        Gets the all_hosts_config of this ApiDeployment.
        Configuration parameters that apply to all hosts, unless overridden at the host level. Available since API v3.

        :return: The all_hosts_config of this ApiDeployment.
        :rtype: ApiConfigList
        """
        return self._all_hosts_config

    @all_hosts_config.setter
    def all_hosts_config(self, all_hosts_config):
        """
        Sets the all_hosts_config of this ApiDeployment.
        Configuration parameters that apply to all hosts, unless overridden at the host level. Available since API v3.

        :param all_hosts_config: The all_hosts_config of this ApiDeployment.
        :type: ApiConfigList
        """

        self._all_hosts_config = all_hosts_config

    @property
    def peers(self):
        """
        Gets the peers of this ApiDeployment.
        The list of peers configured in Cloudera Manager. Available since API v3.

        :return: The peers of this ApiDeployment.
        :rtype: list[ApiCmPeer]
        """
        return self._peers

    @peers.setter
    def peers(self, peers):
        """
        Sets the peers of this ApiDeployment.
        The list of peers configured in Cloudera Manager. Available since API v3.

        :param peers: The peers of this ApiDeployment.
        :type: list[ApiCmPeer]
        """

        self._peers = peers

    @property
    def host_templates(self):
        """
        Gets the host_templates of this ApiDeployment.
        The list of all host templates in Cloudera Manager.

        :return: The host_templates of this ApiDeployment.
        :rtype: ApiHostTemplateList
        """
        return self._host_templates

    @host_templates.setter
    def host_templates(self, host_templates):
        """
        Sets the host_templates of this ApiDeployment.
        The list of all host templates in Cloudera Manager.

        :param host_templates: The host_templates of this ApiDeployment.
        :type: ApiHostTemplateList
        """

        self._host_templates = host_templates

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiDeployment):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
