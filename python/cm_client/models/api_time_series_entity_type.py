# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ApiTimeSeriesEntityType(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'category': 'str',
        'name_for_cross_entity_aggregate_metrics': 'str',
        'display_name': 'str',
        'description': 'str',
        'immutable_attribute_names': 'list[str]',
        'mutable_attribute_names': 'list[str]',
        'entity_name_format': 'list[str]',
        'entity_display_name_format': 'str',
        'parent_metric_entity_type_names': 'list[str]'
    }

    attribute_map = {
        'name': 'name',
        'category': 'category',
        'name_for_cross_entity_aggregate_metrics': 'nameForCrossEntityAggregateMetrics',
        'display_name': 'displayName',
        'description': 'description',
        'immutable_attribute_names': 'immutableAttributeNames',
        'mutable_attribute_names': 'mutableAttributeNames',
        'entity_name_format': 'entityNameFormat',
        'entity_display_name_format': 'entityDisplayNameFormat',
        'parent_metric_entity_type_names': 'parentMetricEntityTypeNames'
    }

    def __init__(self, name=None, category=None, name_for_cross_entity_aggregate_metrics=None, display_name=None, description=None, immutable_attribute_names=None, mutable_attribute_names=None, entity_name_format=None, entity_display_name_format=None, parent_metric_entity_type_names=None):
        """
        ApiTimeSeriesEntityType - a model defined in Swagger
        """

        self._name = None
        self._category = None
        self._name_for_cross_entity_aggregate_metrics = None
        self._display_name = None
        self._description = None
        self._immutable_attribute_names = None
        self._mutable_attribute_names = None
        self._entity_name_format = None
        self._entity_display_name_format = None
        self._parent_metric_entity_type_names = None

        if name is not None:
          self.name = name
        if category is not None:
          self.category = category
        if name_for_cross_entity_aggregate_metrics is not None:
          self.name_for_cross_entity_aggregate_metrics = name_for_cross_entity_aggregate_metrics
        if display_name is not None:
          self.display_name = display_name
        if description is not None:
          self.description = description
        if immutable_attribute_names is not None:
          self.immutable_attribute_names = immutable_attribute_names
        if mutable_attribute_names is not None:
          self.mutable_attribute_names = mutable_attribute_names
        if entity_name_format is not None:
          self.entity_name_format = entity_name_format
        if entity_display_name_format is not None:
          self.entity_display_name_format = entity_display_name_format
        if parent_metric_entity_type_names is not None:
          self.parent_metric_entity_type_names = parent_metric_entity_type_names

    @property
    def name(self):
        """
        Gets the name of this ApiTimeSeriesEntityType.
        Returns the name of the entity type. This name uniquely identifies this entity type.

        :return: The name of this ApiTimeSeriesEntityType.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ApiTimeSeriesEntityType.
        Returns the name of the entity type. This name uniquely identifies this entity type.

        :param name: The name of this ApiTimeSeriesEntityType.
        :type: str
        """

        self._name = name

    @property
    def category(self):
        """
        Gets the category of this ApiTimeSeriesEntityType.
        Returns the category of the entity type.

        :return: The category of this ApiTimeSeriesEntityType.
        :rtype: str
        """
        return self._category

    @category.setter
    def category(self, category):
        """
        Sets the category of this ApiTimeSeriesEntityType.
        Returns the category of the entity type.

        :param category: The category of this ApiTimeSeriesEntityType.
        :type: str
        """

        self._category = category

    @property
    def name_for_cross_entity_aggregate_metrics(self):
        """
        Gets the name_for_cross_entity_aggregate_metrics of this ApiTimeSeriesEntityType.
        Returns the string to use to pluralize the name of the entity for cross entity aggregate metrics.

        :return: The name_for_cross_entity_aggregate_metrics of this ApiTimeSeriesEntityType.
        :rtype: str
        """
        return self._name_for_cross_entity_aggregate_metrics

    @name_for_cross_entity_aggregate_metrics.setter
    def name_for_cross_entity_aggregate_metrics(self, name_for_cross_entity_aggregate_metrics):
        """
        Sets the name_for_cross_entity_aggregate_metrics of this ApiTimeSeriesEntityType.
        Returns the string to use to pluralize the name of the entity for cross entity aggregate metrics.

        :param name_for_cross_entity_aggregate_metrics: The name_for_cross_entity_aggregate_metrics of this ApiTimeSeriesEntityType.
        :type: str
        """

        self._name_for_cross_entity_aggregate_metrics = name_for_cross_entity_aggregate_metrics

    @property
    def display_name(self):
        """
        Gets the display_name of this ApiTimeSeriesEntityType.
        Returns the display name of the entity type.

        :return: The display_name of this ApiTimeSeriesEntityType.
        :rtype: str
        """
        return self._display_name

    @display_name.setter
    def display_name(self, display_name):
        """
        Sets the display_name of this ApiTimeSeriesEntityType.
        Returns the display name of the entity type.

        :param display_name: The display_name of this ApiTimeSeriesEntityType.
        :type: str
        """

        self._display_name = display_name

    @property
    def description(self):
        """
        Gets the description of this ApiTimeSeriesEntityType.
        Returns the description of the entity type.

        :return: The description of this ApiTimeSeriesEntityType.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """
        Sets the description of this ApiTimeSeriesEntityType.
        Returns the description of the entity type.

        :param description: The description of this ApiTimeSeriesEntityType.
        :type: str
        """

        self._description = description

    @property
    def immutable_attribute_names(self):
        """
        Gets the immutable_attribute_names of this ApiTimeSeriesEntityType.
        Returns the list of immutable attributes for this entity type. Immutable attributes values for an entity may not change over its lifetime.

        :return: The immutable_attribute_names of this ApiTimeSeriesEntityType.
        :rtype: list[str]
        """
        return self._immutable_attribute_names

    @immutable_attribute_names.setter
    def immutable_attribute_names(self, immutable_attribute_names):
        """
        Sets the immutable_attribute_names of this ApiTimeSeriesEntityType.
        Returns the list of immutable attributes for this entity type. Immutable attributes values for an entity may not change over its lifetime.

        :param immutable_attribute_names: The immutable_attribute_names of this ApiTimeSeriesEntityType.
        :type: list[str]
        """

        self._immutable_attribute_names = immutable_attribute_names

    @property
    def mutable_attribute_names(self):
        """
        Gets the mutable_attribute_names of this ApiTimeSeriesEntityType.
        Returns the list of mutable attributes for this entity type. Mutable attributes for an entity may change over its lifetime.

        :return: The mutable_attribute_names of this ApiTimeSeriesEntityType.
        :rtype: list[str]
        """
        return self._mutable_attribute_names

    @mutable_attribute_names.setter
    def mutable_attribute_names(self, mutable_attribute_names):
        """
        Sets the mutable_attribute_names of this ApiTimeSeriesEntityType.
        Returns the list of mutable attributes for this entity type. Mutable attributes for an entity may change over its lifetime.

        :param mutable_attribute_names: The mutable_attribute_names of this ApiTimeSeriesEntityType.
        :type: list[str]
        """

        self._mutable_attribute_names = mutable_attribute_names

    @property
    def entity_name_format(self):
        """
        Gets the entity_name_format of this ApiTimeSeriesEntityType.
        Returns a list of attribute names that will be used to construct entity names for entities of this type. The attributes named here must be immutable attributes of this type or a parent type.

        :return: The entity_name_format of this ApiTimeSeriesEntityType.
        :rtype: list[str]
        """
        return self._entity_name_format

    @entity_name_format.setter
    def entity_name_format(self, entity_name_format):
        """
        Sets the entity_name_format of this ApiTimeSeriesEntityType.
        Returns a list of attribute names that will be used to construct entity names for entities of this type. The attributes named here must be immutable attributes of this type or a parent type.

        :param entity_name_format: The entity_name_format of this ApiTimeSeriesEntityType.
        :type: list[str]
        """

        self._entity_name_format = entity_name_format

    @property
    def entity_display_name_format(self):
        """
        Gets the entity_display_name_format of this ApiTimeSeriesEntityType.
        Returns a format string that will be used to construct the display name of entities of this type. If this returns null the entity name would be used as the display name.  The entity attribute values are used to replace $attribute name portions of this format string. For example, an entity with roleType \"DATANODE\" and hostname \"foo.com\" will have a display name \"DATANODE (foo.com)\" if the format is \"$roleType ($hostname)\".

        :return: The entity_display_name_format of this ApiTimeSeriesEntityType.
        :rtype: str
        """
        return self._entity_display_name_format

    @entity_display_name_format.setter
    def entity_display_name_format(self, entity_display_name_format):
        """
        Sets the entity_display_name_format of this ApiTimeSeriesEntityType.
        Returns a format string that will be used to construct the display name of entities of this type. If this returns null the entity name would be used as the display name.  The entity attribute values are used to replace $attribute name portions of this format string. For example, an entity with roleType \"DATANODE\" and hostname \"foo.com\" will have a display name \"DATANODE (foo.com)\" if the format is \"$roleType ($hostname)\".

        :param entity_display_name_format: The entity_display_name_format of this ApiTimeSeriesEntityType.
        :type: str
        """

        self._entity_display_name_format = entity_display_name_format

    @property
    def parent_metric_entity_type_names(self):
        """
        Gets the parent_metric_entity_type_names of this ApiTimeSeriesEntityType.
        Returns a list of metric entity type names which are parents of this metric entity type. A metric entity type inherits the attributes of its ancestors. For example a role metric entity type has its service as a parent. A service metric entity type has a cluster as a parent. The role type inherits its cluster name attribute through its service parent. Only parent ancestors should be returned here. In the example given, only the service metric entity type should be specified in the parent list.

        :return: The parent_metric_entity_type_names of this ApiTimeSeriesEntityType.
        :rtype: list[str]
        """
        return self._parent_metric_entity_type_names

    @parent_metric_entity_type_names.setter
    def parent_metric_entity_type_names(self, parent_metric_entity_type_names):
        """
        Sets the parent_metric_entity_type_names of this ApiTimeSeriesEntityType.
        Returns a list of metric entity type names which are parents of this metric entity type. A metric entity type inherits the attributes of its ancestors. For example a role metric entity type has its service as a parent. A service metric entity type has a cluster as a parent. The role type inherits its cluster name attribute through its service parent. Only parent ancestors should be returned here. In the example given, only the service metric entity type should be specified in the parent list.

        :param parent_metric_entity_type_names: The parent_metric_entity_type_names of this ApiTimeSeriesEntityType.
        :type: list[str]
        """

        self._parent_metric_entity_type_names = parent_metric_entity_type_names

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ApiTimeSeriesEntityType):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
