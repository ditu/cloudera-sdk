# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import cm_client
from cm_client.rest import ApiException
from cm_client.models.api_parcel_ref import ApiParcelRef


class TestApiParcelRef(unittest.TestCase):
    """ ApiParcelRef unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testApiParcelRef(self):
        """
        Test ApiParcelRef
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = cm_client.models.api_parcel_ref.ApiParcelRef()
        pass


if __name__ == '__main__':
    unittest.main()
