# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import cm_client
from cm_client.rest import ApiException
from cm_client.apis.replications_resource_api import ReplicationsResourceApi


class TestReplicationsResourceApi(unittest.TestCase):
    """ ReplicationsResourceApi unit test stubs """

    def setUp(self):
        self.api = cm_client.apis.replications_resource_api.ReplicationsResourceApi()

    def tearDown(self):
        pass

    def test_collect_diagnostic_data(self):
        """
        Test case for collect_diagnostic_data

        Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.
        """
        pass

    def test_create_schedules(self):
        """
        Test case for create_schedules

        Creates one or more replication schedules.
        """
        pass

    def test_delete_all_schedules(self):
        """
        Test case for delete_all_schedules

        Deletes all existing replication schedules.
        """
        pass

    def test_delete_schedule(self):
        """
        Test case for delete_schedule

        Deletes an existing replication schedule.
        """
        pass

    def test_get_replication_state(self):
        """
        Test case for get_replication_state

        returns the replication state.
        """
        pass

    def test_read_history(self):
        """
        Test case for read_history

        Returns a list of commands triggered by a schedule.
        """
        pass

    def test_read_schedule(self):
        """
        Test case for read_schedule

        Returns information for a specific replication schedule.
        """
        pass

    def test_read_schedules(self):
        """
        Test case for read_schedules

        Returns information for all replication schedules.
        """
        pass

    def test_run_copy_listing(self):
        """
        Test case for run_copy_listing

        Run the hdfs copy listing command.
        """
        pass

    def test_run_schedule(self):
        """
        Test case for run_schedule

        Run the schedule immediately.
        """
        pass

    def test_update_schedule(self):
        """
        Test case for update_schedule

        Updates an existing replication schedule.
        """
        pass


if __name__ == '__main__':
    unittest.main()
