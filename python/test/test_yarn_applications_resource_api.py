# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import cm_client
from cm_client.rest import ApiException
from cm_client.apis.yarn_applications_resource_api import YarnApplicationsResourceApi


class TestYarnApplicationsResourceApi(unittest.TestCase):
    """ YarnApplicationsResourceApi unit test stubs """

    def setUp(self):
        self.api = cm_client.apis.yarn_applications_resource_api.YarnApplicationsResourceApi()

    def tearDown(self):
        pass

    def test_get_yarn_application_attributes(self):
        """
        Test case for get_yarn_application_attributes

        Returns the list of all attributes that the Service Monitor can associate with YARN applications.
        """
        pass

    def test_get_yarn_applications(self):
        """
        Test case for get_yarn_applications

        Returns a list of applications that satisfy the filter.
        """
        pass

    def test_kill_yarn_application(self):
        """
        Test case for kill_yarn_application

        Kills an YARN Application.
        """
        pass


if __name__ == '__main__':
    unittest.main()
