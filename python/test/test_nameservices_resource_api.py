# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import cm_client
from cm_client.rest import ApiException
from cm_client.apis.nameservices_resource_api import NameservicesResourceApi


class TestNameservicesResourceApi(unittest.TestCase):
    """ NameservicesResourceApi unit test stubs """

    def setUp(self):
        self.api = cm_client.apis.nameservices_resource_api.NameservicesResourceApi()

    def tearDown(self):
        pass

    def test_get_metrics(self):
        """
        Test case for get_metrics

        Fetch metric readings for a particular nameservice.
        """
        pass

    def test_list_nameservices(self):
        """
        Test case for list_nameservices

        List the nameservices of an HDFS service.
        """
        pass

    def test_read_nameservice(self):
        """
        Test case for read_nameservice

        Retrieve information about a nameservice.
        """
        pass


if __name__ == '__main__':
    unittest.main()
