# coding: utf-8

"""
    Cloudera Manager API

    <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>

    OpenAPI spec version: 6.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import cm_client
from cm_client.rest import ApiException
from cm_client.apis.tools_resource_api import ToolsResourceApi


class TestToolsResourceApi(unittest.TestCase):
    """ ToolsResourceApi unit test stubs """

    def setUp(self):
        self.api = cm_client.apis.tools_resource_api.ToolsResourceApi()

    def tearDown(self):
        pass

    def test_echo(self):
        """
        Test case for echo

        Echoes the provided message back to the caller.
        """
        pass

    def test_echo_error(self):
        """
        Test case for echo_error

        Throws an error containing the given input message.
        """
        pass


if __name__ == '__main__':
    unittest.main()
