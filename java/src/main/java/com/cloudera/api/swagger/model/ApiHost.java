/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiClusterRef;
import com.cloudera.api.swagger.model.ApiCommissionState;
import com.cloudera.api.swagger.model.ApiConfigList;
import com.cloudera.api.swagger.model.ApiEntityStatus;
import com.cloudera.api.swagger.model.ApiEntityType;
import com.cloudera.api.swagger.model.ApiHealthCheck;
import com.cloudera.api.swagger.model.ApiHealthSummary;
import com.cloudera.api.swagger.model.ApiRoleRef;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the model for a host in the system.
 */
@ApiModel(description = "This is the model for a host in the system.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiHost {
  @SerializedName("hostId")
  private String hostId = null;

  @SerializedName("ipAddress")
  private String ipAddress = null;

  @SerializedName("hostname")
  private String hostname = null;

  @SerializedName("rackId")
  private String rackId = null;

  @SerializedName("lastHeartbeat")
  private String lastHeartbeat = null;

  @SerializedName("roleRefs")
  private List<ApiRoleRef> roleRefs = null;

  @SerializedName("healthSummary")
  private ApiHealthSummary healthSummary = null;

  @SerializedName("healthChecks")
  private List<ApiHealthCheck> healthChecks = null;

  @SerializedName("hostUrl")
  private String hostUrl = null;

  @SerializedName("maintenanceMode")
  private Boolean maintenanceMode = null;

  @SerializedName("commissionState")
  private ApiCommissionState commissionState = null;

  @SerializedName("maintenanceOwners")
  private List<ApiEntityType> maintenanceOwners = null;

  @SerializedName("config")
  private ApiConfigList config = null;

  @SerializedName("numCores")
  private BigDecimal numCores = null;

  @SerializedName("numPhysicalCores")
  private BigDecimal numPhysicalCores = null;

  @SerializedName("totalPhysMemBytes")
  private BigDecimal totalPhysMemBytes = null;

  @SerializedName("entityStatus")
  private ApiEntityStatus entityStatus = null;

  @SerializedName("clusterRef")
  private ApiClusterRef clusterRef = null;

  public ApiHost hostId(String hostId) {
    this.hostId = hostId;
    return this;
  }

   /**
   * A unique host identifier. This is not the same as the hostname (FQDN). It is a distinct value that remains the same even if the hostname changes.
   * @return hostId
  **/
  @ApiModelProperty(value = "A unique host identifier. This is not the same as the hostname (FQDN). It is a distinct value that remains the same even if the hostname changes.")
  public String getHostId() {
    return hostId;
  }

  public void setHostId(String hostId) {
    this.hostId = hostId;
  }

  public ApiHost ipAddress(String ipAddress) {
    this.ipAddress = ipAddress;
    return this;
  }

   /**
   * The host IP address. This field is not mutable after the initial creation.
   * @return ipAddress
  **/
  @ApiModelProperty(value = "The host IP address. This field is not mutable after the initial creation.")
  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public ApiHost hostname(String hostname) {
    this.hostname = hostname;
    return this;
  }

   /**
   * The hostname. This field is not mutable after the initial creation.
   * @return hostname
  **/
  @ApiModelProperty(value = "The hostname. This field is not mutable after the initial creation.")
  public String getHostname() {
    return hostname;
  }

  public void setHostname(String hostname) {
    this.hostname = hostname;
  }

  public ApiHost rackId(String rackId) {
    this.rackId = rackId;
    return this;
  }

   /**
   * The rack ID for this host.
   * @return rackId
  **/
  @ApiModelProperty(value = "The rack ID for this host.")
  public String getRackId() {
    return rackId;
  }

  public void setRackId(String rackId) {
    this.rackId = rackId;
  }

  public ApiHost lastHeartbeat(String lastHeartbeat) {
    this.lastHeartbeat = lastHeartbeat;
    return this;
  }

   /**
   * Readonly. Requires \&quot;full\&quot; view. When the host agent sent the last heartbeat.
   * @return lastHeartbeat
  **/
  @ApiModelProperty(value = "Readonly. Requires \"full\" view. When the host agent sent the last heartbeat.")
  public String getLastHeartbeat() {
    return lastHeartbeat;
  }

  public void setLastHeartbeat(String lastHeartbeat) {
    this.lastHeartbeat = lastHeartbeat;
  }

  public ApiHost roleRefs(List<ApiRoleRef> roleRefs) {
    this.roleRefs = roleRefs;
    return this;
  }

  public ApiHost addRoleRefsItem(ApiRoleRef roleRefsItem) {
    if (this.roleRefs == null) {
      this.roleRefs = new ArrayList<ApiRoleRef>();
    }
    this.roleRefs.add(roleRefsItem);
    return this;
  }

   /**
   * Readonly. Requires \&quot;full\&quot; view. The list of roles assigned to this host.
   * @return roleRefs
  **/
  @ApiModelProperty(value = "Readonly. Requires \"full\" view. The list of roles assigned to this host.")
  public List<ApiRoleRef> getRoleRefs() {
    return roleRefs;
  }

  public void setRoleRefs(List<ApiRoleRef> roleRefs) {
    this.roleRefs = roleRefs;
  }

  public ApiHost healthSummary(ApiHealthSummary healthSummary) {
    this.healthSummary = healthSummary;
    return this;
  }

   /**
   * Readonly. Requires \&quot;full\&quot; view. The high-level health status of this host.
   * @return healthSummary
  **/
  @ApiModelProperty(value = "Readonly. Requires \"full\" view. The high-level health status of this host.")
  public ApiHealthSummary getHealthSummary() {
    return healthSummary;
  }

  public void setHealthSummary(ApiHealthSummary healthSummary) {
    this.healthSummary = healthSummary;
  }

  public ApiHost healthChecks(List<ApiHealthCheck> healthChecks) {
    this.healthChecks = healthChecks;
    return this;
  }

  public ApiHost addHealthChecksItem(ApiHealthCheck healthChecksItem) {
    if (this.healthChecks == null) {
      this.healthChecks = new ArrayList<ApiHealthCheck>();
    }
    this.healthChecks.add(healthChecksItem);
    return this;
  }

   /**
   * Readonly. Requires \&quot;full\&quot; view. The list of health checks performed on the host, with their results.
   * @return healthChecks
  **/
  @ApiModelProperty(value = "Readonly. Requires \"full\" view. The list of health checks performed on the host, with their results.")
  public List<ApiHealthCheck> getHealthChecks() {
    return healthChecks;
  }

  public void setHealthChecks(List<ApiHealthCheck> healthChecks) {
    this.healthChecks = healthChecks;
  }

  public ApiHost hostUrl(String hostUrl) {
    this.hostUrl = hostUrl;
    return this;
  }

   /**
   * Readonly. A URL into the Cloudera Manager web UI for this specific host.
   * @return hostUrl
  **/
  @ApiModelProperty(value = "Readonly. A URL into the Cloudera Manager web UI for this specific host.")
  public String getHostUrl() {
    return hostUrl;
  }

  public void setHostUrl(String hostUrl) {
    this.hostUrl = hostUrl;
  }

  public ApiHost maintenanceMode(Boolean maintenanceMode) {
    this.maintenanceMode = maintenanceMode;
    return this;
  }

   /**
   * Readonly. Whether the host is in maintenance mode. Available since API v2.
   * @return maintenanceMode
  **/
  @ApiModelProperty(value = "Readonly. Whether the host is in maintenance mode. Available since API v2.")
  public Boolean getMaintenanceMode() {
    return maintenanceMode;
  }

  public void setMaintenanceMode(Boolean maintenanceMode) {
    this.maintenanceMode = maintenanceMode;
  }

  public ApiHost commissionState(ApiCommissionState commissionState) {
    this.commissionState = commissionState;
    return this;
  }

   /**
   * Readonly. The commission state of this role. Available since API v2.
   * @return commissionState
  **/
  @ApiModelProperty(value = "Readonly. The commission state of this role. Available since API v2.")
  public ApiCommissionState getCommissionState() {
    return commissionState;
  }

  public void setCommissionState(ApiCommissionState commissionState) {
    this.commissionState = commissionState;
  }

  public ApiHost maintenanceOwners(List<ApiEntityType> maintenanceOwners) {
    this.maintenanceOwners = maintenanceOwners;
    return this;
  }

  public ApiHost addMaintenanceOwnersItem(ApiEntityType maintenanceOwnersItem) {
    if (this.maintenanceOwners == null) {
      this.maintenanceOwners = new ArrayList<ApiEntityType>();
    }
    this.maintenanceOwners.add(maintenanceOwnersItem);
    return this;
  }

   /**
   * Readonly. The list of objects that trigger this host to be in maintenance mode. Available since API v2.
   * @return maintenanceOwners
  **/
  @ApiModelProperty(example = "\"null\"", value = "Readonly. The list of objects that trigger this host to be in maintenance mode. Available since API v2.")
  public List<ApiEntityType> getMaintenanceOwners() {
    return maintenanceOwners;
  }

  public void setMaintenanceOwners(List<ApiEntityType> maintenanceOwners) {
    this.maintenanceOwners = maintenanceOwners;
  }

  public ApiHost config(ApiConfigList config) {
    this.config = config;
    return this;
  }

   /**
   * 
   * @return config
  **/
  @ApiModelProperty(value = "")
  public ApiConfigList getConfig() {
    return config;
  }

  public void setConfig(ApiConfigList config) {
    this.config = config;
  }

  public ApiHost numCores(BigDecimal numCores) {
    this.numCores = numCores;
    return this;
  }

   /**
   * Readonly. The number of logical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v4.
   * @return numCores
  **/
  @ApiModelProperty(value = "Readonly. The number of logical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v4.")
  public BigDecimal getNumCores() {
    return numCores;
  }

  public void setNumCores(BigDecimal numCores) {
    this.numCores = numCores;
  }

  public ApiHost numPhysicalCores(BigDecimal numPhysicalCores) {
    this.numPhysicalCores = numPhysicalCores;
    return this;
  }

   /**
   * Readonly. The number of physical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v9.
   * @return numPhysicalCores
  **/
  @ApiModelProperty(value = "Readonly. The number of physical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v9.")
  public BigDecimal getNumPhysicalCores() {
    return numPhysicalCores;
  }

  public void setNumPhysicalCores(BigDecimal numPhysicalCores) {
    this.numPhysicalCores = numPhysicalCores;
  }

  public ApiHost totalPhysMemBytes(BigDecimal totalPhysMemBytes) {
    this.totalPhysMemBytes = totalPhysMemBytes;
    return this;
  }

   /**
   * Readonly. The amount of physical RAM on this host, in bytes. Only populated after the host has heartbeated to the server. Available since API v4.
   * @return totalPhysMemBytes
  **/
  @ApiModelProperty(value = "Readonly. The amount of physical RAM on this host, in bytes. Only populated after the host has heartbeated to the server. Available since API v4.")
  public BigDecimal getTotalPhysMemBytes() {
    return totalPhysMemBytes;
  }

  public void setTotalPhysMemBytes(BigDecimal totalPhysMemBytes) {
    this.totalPhysMemBytes = totalPhysMemBytes;
  }

  public ApiHost entityStatus(ApiEntityStatus entityStatus) {
    this.entityStatus = entityStatus;
    return this;
  }

   /**
   * Readonly. The entity status for this host. Available since API v11.
   * @return entityStatus
  **/
  @ApiModelProperty(value = "Readonly. The entity status for this host. Available since API v11.")
  public ApiEntityStatus getEntityStatus() {
    return entityStatus;
  }

  public void setEntityStatus(ApiEntityStatus entityStatus) {
    this.entityStatus = entityStatus;
  }

  public ApiHost clusterRef(ApiClusterRef clusterRef) {
    this.clusterRef = clusterRef;
    return this;
  }

   /**
   * Readonly. A reference to the enclosing cluster. This might be null if the host is not yet assigned to a cluster. Available since API v11.
   * @return clusterRef
  **/
  @ApiModelProperty(value = "Readonly. A reference to the enclosing cluster. This might be null if the host is not yet assigned to a cluster. Available since API v11.")
  public ApiClusterRef getClusterRef() {
    return clusterRef;
  }

  public void setClusterRef(ApiClusterRef clusterRef) {
    this.clusterRef = clusterRef;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiHost apiHost = (ApiHost) o;
    return Objects.equals(this.hostId, apiHost.hostId) &&
        Objects.equals(this.ipAddress, apiHost.ipAddress) &&
        Objects.equals(this.hostname, apiHost.hostname) &&
        Objects.equals(this.rackId, apiHost.rackId) &&
        Objects.equals(this.lastHeartbeat, apiHost.lastHeartbeat) &&
        Objects.equals(this.roleRefs, apiHost.roleRefs) &&
        Objects.equals(this.healthSummary, apiHost.healthSummary) &&
        Objects.equals(this.healthChecks, apiHost.healthChecks) &&
        Objects.equals(this.hostUrl, apiHost.hostUrl) &&
        Objects.equals(this.maintenanceMode, apiHost.maintenanceMode) &&
        Objects.equals(this.commissionState, apiHost.commissionState) &&
        Objects.equals(this.maintenanceOwners, apiHost.maintenanceOwners) &&
        Objects.equals(this.config, apiHost.config) &&
        Objects.equals(this.numCores, apiHost.numCores) &&
        Objects.equals(this.numPhysicalCores, apiHost.numPhysicalCores) &&
        Objects.equals(this.totalPhysMemBytes, apiHost.totalPhysMemBytes) &&
        Objects.equals(this.entityStatus, apiHost.entityStatus) &&
        Objects.equals(this.clusterRef, apiHost.clusterRef);
  }

  @Override
  public int hashCode() {
    return Objects.hash(hostId, ipAddress, hostname, rackId, lastHeartbeat, roleRefs, healthSummary, healthChecks, hostUrl, maintenanceMode, commissionState, maintenanceOwners, config, numCores, numPhysicalCores, totalPhysMemBytes, entityStatus, clusterRef);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiHost {\n");
    
    sb.append("    hostId: ").append(toIndentedString(hostId)).append("\n");
    sb.append("    ipAddress: ").append(toIndentedString(ipAddress)).append("\n");
    sb.append("    hostname: ").append(toIndentedString(hostname)).append("\n");
    sb.append("    rackId: ").append(toIndentedString(rackId)).append("\n");
    sb.append("    lastHeartbeat: ").append(toIndentedString(lastHeartbeat)).append("\n");
    sb.append("    roleRefs: ").append(toIndentedString(roleRefs)).append("\n");
    sb.append("    healthSummary: ").append(toIndentedString(healthSummary)).append("\n");
    sb.append("    healthChecks: ").append(toIndentedString(healthChecks)).append("\n");
    sb.append("    hostUrl: ").append(toIndentedString(hostUrl)).append("\n");
    sb.append("    maintenanceMode: ").append(toIndentedString(maintenanceMode)).append("\n");
    sb.append("    commissionState: ").append(toIndentedString(commissionState)).append("\n");
    sb.append("    maintenanceOwners: ").append(toIndentedString(maintenanceOwners)).append("\n");
    sb.append("    config: ").append(toIndentedString(config)).append("\n");
    sb.append("    numCores: ").append(toIndentedString(numCores)).append("\n");
    sb.append("    numPhysicalCores: ").append(toIndentedString(numPhysicalCores)).append("\n");
    sb.append("    totalPhysMemBytes: ").append(toIndentedString(totalPhysMemBytes)).append("\n");
    sb.append("    entityStatus: ").append(toIndentedString(entityStatus)).append("\n");
    sb.append("    clusterRef: ").append(toIndentedString(clusterRef)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

