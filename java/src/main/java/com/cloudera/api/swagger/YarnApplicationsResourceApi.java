/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger;

import com.cloudera.api.swagger.client.ApiCallback;
import com.cloudera.api.swagger.client.ApiClient;
import com.cloudera.api.swagger.client.ApiException;
import com.cloudera.api.swagger.client.ApiResponse;
import com.cloudera.api.swagger.client.Configuration;
import com.cloudera.api.swagger.client.Pair;
import com.cloudera.api.swagger.client.ProgressRequestBody;
import com.cloudera.api.swagger.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;


import com.cloudera.api.swagger.model.ApiYarnApplicationAttributeList;
import com.cloudera.api.swagger.model.ApiYarnApplicationResponse;
import com.cloudera.api.swagger.model.ApiYarnKillResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YarnApplicationsResourceApi {
    private ApiClient apiClient;

    public YarnApplicationsResourceApi() {
        this(Configuration.getDefaultApiClient());
    }

    public YarnApplicationsResourceApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for getYarnApplicationAttributes
     * @param clusterName  (required)
     * @param serviceName  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getYarnApplicationAttributesCall(String clusterName, String serviceName, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/clusters/{clusterName}/services/{serviceName}/yarnApplications/attributes"
            .replaceAll("\\{" + "clusterName" + "\\}", apiClient.escapeString(clusterName.toString()))
            .replaceAll("\\{" + "serviceName" + "\\}", apiClient.escapeString(serviceName.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "basic" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getYarnApplicationAttributesValidateBeforeCall(String clusterName, String serviceName, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'clusterName' is set
        if (clusterName == null) {
            throw new ApiException("Missing the required parameter 'clusterName' when calling getYarnApplicationAttributes(Async)");
        }
        
        // verify the required parameter 'serviceName' is set
        if (serviceName == null) {
            throw new ApiException("Missing the required parameter 'serviceName' when calling getYarnApplicationAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getYarnApplicationAttributesCall(clusterName, serviceName, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Returns the list of all attributes that the Service Monitor can associate with YARN applications.
     * Returns the list of all attributes that the Service Monitor can associate with YARN applications. &lt;p&gt; Examples of attributes include the user who ran the application and the number of maps completed by the application. &lt;p&gt; These attributes can be used to search for specific YARN applications through the getYarnApplications API. For example the &#39;user&#39; attribute could be used in the search &#39;user &#x3D; root&#39;. If the attribute is numeric it can also be used as a metric in a tsquery (ie, &#39;select maps_completed from YARN_APPLICATIONS&#39;). &lt;p&gt; Note that this response is identical for all YARN services. &lt;p&gt; Available since API v6.
     * @param clusterName  (required)
     * @param serviceName  (required)
     * @return ApiYarnApplicationAttributeList
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiYarnApplicationAttributeList getYarnApplicationAttributes(String clusterName, String serviceName) throws ApiException {
        ApiResponse<ApiYarnApplicationAttributeList> resp = getYarnApplicationAttributesWithHttpInfo(clusterName, serviceName);
        return resp.getData();
    }

    /**
     * Returns the list of all attributes that the Service Monitor can associate with YARN applications.
     * Returns the list of all attributes that the Service Monitor can associate with YARN applications. &lt;p&gt; Examples of attributes include the user who ran the application and the number of maps completed by the application. &lt;p&gt; These attributes can be used to search for specific YARN applications through the getYarnApplications API. For example the &#39;user&#39; attribute could be used in the search &#39;user &#x3D; root&#39;. If the attribute is numeric it can also be used as a metric in a tsquery (ie, &#39;select maps_completed from YARN_APPLICATIONS&#39;). &lt;p&gt; Note that this response is identical for all YARN services. &lt;p&gt; Available since API v6.
     * @param clusterName  (required)
     * @param serviceName  (required)
     * @return ApiResponse&lt;ApiYarnApplicationAttributeList&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ApiYarnApplicationAttributeList> getYarnApplicationAttributesWithHttpInfo(String clusterName, String serviceName) throws ApiException {
        com.squareup.okhttp.Call call = getYarnApplicationAttributesValidateBeforeCall(clusterName, serviceName, null, null);
        Type localVarReturnType = new TypeToken<ApiYarnApplicationAttributeList>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Returns the list of all attributes that the Service Monitor can associate with YARN applications. (asynchronously)
     * Returns the list of all attributes that the Service Monitor can associate with YARN applications. &lt;p&gt; Examples of attributes include the user who ran the application and the number of maps completed by the application. &lt;p&gt; These attributes can be used to search for specific YARN applications through the getYarnApplications API. For example the &#39;user&#39; attribute could be used in the search &#39;user &#x3D; root&#39;. If the attribute is numeric it can also be used as a metric in a tsquery (ie, &#39;select maps_completed from YARN_APPLICATIONS&#39;). &lt;p&gt; Note that this response is identical for all YARN services. &lt;p&gt; Available since API v6.
     * @param clusterName  (required)
     * @param serviceName  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getYarnApplicationAttributesAsync(String clusterName, String serviceName, final ApiCallback<ApiYarnApplicationAttributeList> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getYarnApplicationAttributesValidateBeforeCall(clusterName, serviceName, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ApiYarnApplicationAttributeList>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getYarnApplications
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @param filter A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like &#39;executing &#x3D; true&#39; or &#39;user &#x3D; root&#39;. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: &#39;application_duration &gt; 5s and (user &#x3D; root or user &#x3D; myUserName&#39;). (optional, default to )
     * @param from Start of the period to query in ISO 8601 format (defaults to 5 minutes before the &#39;to&#39; time). (optional)
     * @param limit The maximum number of applications to return. Applications will be returned in the following order: &lt;ul&gt; &lt;li&gt; All executing applications, ordered from longest to shortest running &lt;/li&gt; &lt;li&gt; All completed applications order by end time descending. &lt;/li&gt; &lt;/ul&gt; (optional, default to 100)
     * @param offset The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a &#39;executing &#x3D; false&#39; clause to your filter. (optional, default to 0)
     * @param to End of the period to query in ISO 8601 format (defaults to now). (optional, default to now)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getYarnApplicationsCall(String clusterName, String serviceName, String filter, String from, Integer limit, Integer offset, String to, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/clusters/{clusterName}/services/{serviceName}/yarnApplications"
            .replaceAll("\\{" + "clusterName" + "\\}", apiClient.escapeString(clusterName.toString()))
            .replaceAll("\\{" + "serviceName" + "\\}", apiClient.escapeString(serviceName.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (filter != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "filter", filter));
        if (from != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "from", from));
        if (limit != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "limit", limit));
        if (offset != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "offset", offset));
        if (to != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "to", to));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "basic" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getYarnApplicationsValidateBeforeCall(String clusterName, String serviceName, String filter, String from, Integer limit, Integer offset, String to, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'clusterName' is set
        if (clusterName == null) {
            throw new ApiException("Missing the required parameter 'clusterName' when calling getYarnApplications(Async)");
        }
        
        // verify the required parameter 'serviceName' is set
        if (serviceName == null) {
            throw new ApiException("Missing the required parameter 'serviceName' when calling getYarnApplications(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getYarnApplicationsCall(clusterName, serviceName, filter, from, limit, offset, to, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Returns a list of applications that satisfy the filter.
     * Returns a list of applications that satisfy the filter &lt;p&gt; Available since API v6.
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @param filter A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like &#39;executing &#x3D; true&#39; or &#39;user &#x3D; root&#39;. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: &#39;application_duration &gt; 5s and (user &#x3D; root or user &#x3D; myUserName&#39;). (optional, default to )
     * @param from Start of the period to query in ISO 8601 format (defaults to 5 minutes before the &#39;to&#39; time). (optional)
     * @param limit The maximum number of applications to return. Applications will be returned in the following order: &lt;ul&gt; &lt;li&gt; All executing applications, ordered from longest to shortest running &lt;/li&gt; &lt;li&gt; All completed applications order by end time descending. &lt;/li&gt; &lt;/ul&gt; (optional, default to 100)
     * @param offset The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a &#39;executing &#x3D; false&#39; clause to your filter. (optional, default to 0)
     * @param to End of the period to query in ISO 8601 format (defaults to now). (optional, default to now)
     * @return ApiYarnApplicationResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiYarnApplicationResponse getYarnApplications(String clusterName, String serviceName, String filter, String from, Integer limit, Integer offset, String to) throws ApiException {
        ApiResponse<ApiYarnApplicationResponse> resp = getYarnApplicationsWithHttpInfo(clusterName, serviceName, filter, from, limit, offset, to);
        return resp.getData();
    }

    /**
     * Returns a list of applications that satisfy the filter.
     * Returns a list of applications that satisfy the filter &lt;p&gt; Available since API v6.
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @param filter A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like &#39;executing &#x3D; true&#39; or &#39;user &#x3D; root&#39;. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: &#39;application_duration &gt; 5s and (user &#x3D; root or user &#x3D; myUserName&#39;). (optional, default to )
     * @param from Start of the period to query in ISO 8601 format (defaults to 5 minutes before the &#39;to&#39; time). (optional)
     * @param limit The maximum number of applications to return. Applications will be returned in the following order: &lt;ul&gt; &lt;li&gt; All executing applications, ordered from longest to shortest running &lt;/li&gt; &lt;li&gt; All completed applications order by end time descending. &lt;/li&gt; &lt;/ul&gt; (optional, default to 100)
     * @param offset The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a &#39;executing &#x3D; false&#39; clause to your filter. (optional, default to 0)
     * @param to End of the period to query in ISO 8601 format (defaults to now). (optional, default to now)
     * @return ApiResponse&lt;ApiYarnApplicationResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ApiYarnApplicationResponse> getYarnApplicationsWithHttpInfo(String clusterName, String serviceName, String filter, String from, Integer limit, Integer offset, String to) throws ApiException {
        com.squareup.okhttp.Call call = getYarnApplicationsValidateBeforeCall(clusterName, serviceName, filter, from, limit, offset, to, null, null);
        Type localVarReturnType = new TypeToken<ApiYarnApplicationResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Returns a list of applications that satisfy the filter. (asynchronously)
     * Returns a list of applications that satisfy the filter &lt;p&gt; Available since API v6.
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @param filter A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like &#39;executing &#x3D; true&#39; or &#39;user &#x3D; root&#39;. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: &#39;application_duration &gt; 5s and (user &#x3D; root or user &#x3D; myUserName&#39;). (optional, default to )
     * @param from Start of the period to query in ISO 8601 format (defaults to 5 minutes before the &#39;to&#39; time). (optional)
     * @param limit The maximum number of applications to return. Applications will be returned in the following order: &lt;ul&gt; &lt;li&gt; All executing applications, ordered from longest to shortest running &lt;/li&gt; &lt;li&gt; All completed applications order by end time descending. &lt;/li&gt; &lt;/ul&gt; (optional, default to 100)
     * @param offset The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a &#39;executing &#x3D; false&#39; clause to your filter. (optional, default to 0)
     * @param to End of the period to query in ISO 8601 format (defaults to now). (optional, default to now)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getYarnApplicationsAsync(String clusterName, String serviceName, String filter, String from, Integer limit, Integer offset, String to, final ApiCallback<ApiYarnApplicationResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getYarnApplicationsValidateBeforeCall(clusterName, serviceName, filter, from, limit, offset, to, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ApiYarnApplicationResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for killYarnApplication
     * @param applicationId The applicationId to kill (required)
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call killYarnApplicationCall(String applicationId, String clusterName, String serviceName, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/clusters/{clusterName}/services/{serviceName}/yarnApplications/{applicationId}/kill"
            .replaceAll("\\{" + "applicationId" + "\\}", apiClient.escapeString(applicationId.toString()))
            .replaceAll("\\{" + "clusterName" + "\\}", apiClient.escapeString(clusterName.toString()))
            .replaceAll("\\{" + "serviceName" + "\\}", apiClient.escapeString(serviceName.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "basic" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call killYarnApplicationValidateBeforeCall(String applicationId, String clusterName, String serviceName, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'applicationId' is set
        if (applicationId == null) {
            throw new ApiException("Missing the required parameter 'applicationId' when calling killYarnApplication(Async)");
        }
        
        // verify the required parameter 'clusterName' is set
        if (clusterName == null) {
            throw new ApiException("Missing the required parameter 'clusterName' when calling killYarnApplication(Async)");
        }
        
        // verify the required parameter 'serviceName' is set
        if (serviceName == null) {
            throw new ApiException("Missing the required parameter 'serviceName' when calling killYarnApplication(Async)");
        }
        
        
        com.squareup.okhttp.Call call = killYarnApplicationCall(applicationId, clusterName, serviceName, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Kills an YARN Application.
     * Kills an YARN Application &lt;p&gt; Available since API v6.
     * @param applicationId The applicationId to kill (required)
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @return ApiYarnKillResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiYarnKillResponse killYarnApplication(String applicationId, String clusterName, String serviceName) throws ApiException {
        ApiResponse<ApiYarnKillResponse> resp = killYarnApplicationWithHttpInfo(applicationId, clusterName, serviceName);
        return resp.getData();
    }

    /**
     * Kills an YARN Application.
     * Kills an YARN Application &lt;p&gt; Available since API v6.
     * @param applicationId The applicationId to kill (required)
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @return ApiResponse&lt;ApiYarnKillResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ApiYarnKillResponse> killYarnApplicationWithHttpInfo(String applicationId, String clusterName, String serviceName) throws ApiException {
        com.squareup.okhttp.Call call = killYarnApplicationValidateBeforeCall(applicationId, clusterName, serviceName, null, null);
        Type localVarReturnType = new TypeToken<ApiYarnKillResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Kills an YARN Application. (asynchronously)
     * Kills an YARN Application &lt;p&gt; Available since API v6.
     * @param applicationId The applicationId to kill (required)
     * @param clusterName  (required)
     * @param serviceName The name of the service (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call killYarnApplicationAsync(String applicationId, String clusterName, String serviceName, final ApiCallback<ApiYarnKillResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = killYarnApplicationValidateBeforeCall(applicationId, clusterName, serviceName, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ApiYarnKillResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
