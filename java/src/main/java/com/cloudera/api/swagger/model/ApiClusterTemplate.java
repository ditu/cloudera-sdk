/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiClusterTemplateHostTemplate;
import com.cloudera.api.swagger.model.ApiClusterTemplateInstantiator;
import com.cloudera.api.swagger.model.ApiClusterTemplateService;
import com.cloudera.api.swagger.model.ApiProductVersion;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Details of cluster template
 */
@ApiModel(description = "Details of cluster template")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiClusterTemplate {
  @SerializedName("cdhVersion")
  private String cdhVersion = null;

  @SerializedName("products")
  private List<ApiProductVersion> products = null;

  @SerializedName("services")
  private List<ApiClusterTemplateService> services = null;

  @SerializedName("hostTemplates")
  private List<ApiClusterTemplateHostTemplate> hostTemplates = null;

  @SerializedName("displayName")
  private String displayName = null;

  @SerializedName("cmVersion")
  private String cmVersion = null;

  @SerializedName("instantiator")
  private ApiClusterTemplateInstantiator instantiator = null;

  @SerializedName("repositories")
  private List<String> repositories = null;

  public ApiClusterTemplate cdhVersion(String cdhVersion) {
    this.cdhVersion = cdhVersion;
    return this;
  }

   /**
   * 
   * @return cdhVersion
  **/
  @ApiModelProperty(value = "")
  public String getCdhVersion() {
    return cdhVersion;
  }

  public void setCdhVersion(String cdhVersion) {
    this.cdhVersion = cdhVersion;
  }

  public ApiClusterTemplate products(List<ApiProductVersion> products) {
    this.products = products;
    return this;
  }

  public ApiClusterTemplate addProductsItem(ApiProductVersion productsItem) {
    if (this.products == null) {
      this.products = new ArrayList<ApiProductVersion>();
    }
    this.products.add(productsItem);
    return this;
  }

   /**
   * 
   * @return products
  **/
  @ApiModelProperty(value = "")
  public List<ApiProductVersion> getProducts() {
    return products;
  }

  public void setProducts(List<ApiProductVersion> products) {
    this.products = products;
  }

  public ApiClusterTemplate services(List<ApiClusterTemplateService> services) {
    this.services = services;
    return this;
  }

  public ApiClusterTemplate addServicesItem(ApiClusterTemplateService servicesItem) {
    if (this.services == null) {
      this.services = new ArrayList<ApiClusterTemplateService>();
    }
    this.services.add(servicesItem);
    return this;
  }

   /**
   * 
   * @return services
  **/
  @ApiModelProperty(value = "")
  public List<ApiClusterTemplateService> getServices() {
    return services;
  }

  public void setServices(List<ApiClusterTemplateService> services) {
    this.services = services;
  }

  public ApiClusterTemplate hostTemplates(List<ApiClusterTemplateHostTemplate> hostTemplates) {
    this.hostTemplates = hostTemplates;
    return this;
  }

  public ApiClusterTemplate addHostTemplatesItem(ApiClusterTemplateHostTemplate hostTemplatesItem) {
    if (this.hostTemplates == null) {
      this.hostTemplates = new ArrayList<ApiClusterTemplateHostTemplate>();
    }
    this.hostTemplates.add(hostTemplatesItem);
    return this;
  }

   /**
   * 
   * @return hostTemplates
  **/
  @ApiModelProperty(value = "")
  public List<ApiClusterTemplateHostTemplate> getHostTemplates() {
    return hostTemplates;
  }

  public void setHostTemplates(List<ApiClusterTemplateHostTemplate> hostTemplates) {
    this.hostTemplates = hostTemplates;
  }

  public ApiClusterTemplate displayName(String displayName) {
    this.displayName = displayName;
    return this;
  }

   /**
   * 
   * @return displayName
  **/
  @ApiModelProperty(value = "")
  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public ApiClusterTemplate cmVersion(String cmVersion) {
    this.cmVersion = cmVersion;
    return this;
  }

   /**
   * 
   * @return cmVersion
  **/
  @ApiModelProperty(value = "")
  public String getCmVersion() {
    return cmVersion;
  }

  public void setCmVersion(String cmVersion) {
    this.cmVersion = cmVersion;
  }

  public ApiClusterTemplate instantiator(ApiClusterTemplateInstantiator instantiator) {
    this.instantiator = instantiator;
    return this;
  }

   /**
   * 
   * @return instantiator
  **/
  @ApiModelProperty(value = "")
  public ApiClusterTemplateInstantiator getInstantiator() {
    return instantiator;
  }

  public void setInstantiator(ApiClusterTemplateInstantiator instantiator) {
    this.instantiator = instantiator;
  }

  public ApiClusterTemplate repositories(List<String> repositories) {
    this.repositories = repositories;
    return this;
  }

  public ApiClusterTemplate addRepositoriesItem(String repositoriesItem) {
    if (this.repositories == null) {
      this.repositories = new ArrayList<String>();
    }
    this.repositories.add(repositoriesItem);
    return this;
  }

   /**
   * 
   * @return repositories
  **/
  @ApiModelProperty(example = "\"null\"", value = "")
  public List<String> getRepositories() {
    return repositories;
  }

  public void setRepositories(List<String> repositories) {
    this.repositories = repositories;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiClusterTemplate apiClusterTemplate = (ApiClusterTemplate) o;
    return Objects.equals(this.cdhVersion, apiClusterTemplate.cdhVersion) &&
        Objects.equals(this.products, apiClusterTemplate.products) &&
        Objects.equals(this.services, apiClusterTemplate.services) &&
        Objects.equals(this.hostTemplates, apiClusterTemplate.hostTemplates) &&
        Objects.equals(this.displayName, apiClusterTemplate.displayName) &&
        Objects.equals(this.cmVersion, apiClusterTemplate.cmVersion) &&
        Objects.equals(this.instantiator, apiClusterTemplate.instantiator) &&
        Objects.equals(this.repositories, apiClusterTemplate.repositories);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cdhVersion, products, services, hostTemplates, displayName, cmVersion, instantiator, repositories);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiClusterTemplate {\n");
    
    sb.append("    cdhVersion: ").append(toIndentedString(cdhVersion)).append("\n");
    sb.append("    products: ").append(toIndentedString(products)).append("\n");
    sb.append("    services: ").append(toIndentedString(services)).append("\n");
    sb.append("    hostTemplates: ").append(toIndentedString(hostTemplates)).append("\n");
    sb.append("    displayName: ").append(toIndentedString(displayName)).append("\n");
    sb.append("    cmVersion: ").append(toIndentedString(cmVersion)).append("\n");
    sb.append("    instantiator: ").append(toIndentedString(instantiator)).append("\n");
    sb.append("    repositories: ").append(toIndentedString(repositories)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

