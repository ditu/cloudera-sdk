/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Arguments to run ping test.
 */
@ApiModel(description = "Arguments to run ping test.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiPerfInspectorPingArgs {
  @SerializedName("pingTimeoutSecs")
  private BigDecimal pingTimeoutSecs = null;

  @SerializedName("pingCount")
  private BigDecimal pingCount = null;

  @SerializedName("pingPacketSizeBytes")
  private BigDecimal pingPacketSizeBytes = null;

  public ApiPerfInspectorPingArgs pingTimeoutSecs(BigDecimal pingTimeoutSecs) {
    this.pingTimeoutSecs = pingTimeoutSecs;
    return this;
  }

   /**
   * Timeout in seconds for the ping request to each target host. If not specified, defaults to 10 seconds.
   * @return pingTimeoutSecs
  **/
  @ApiModelProperty(example = "10.0", value = "Timeout in seconds for the ping request to each target host. If not specified, defaults to 10 seconds.")
  public BigDecimal getPingTimeoutSecs() {
    return pingTimeoutSecs;
  }

  public void setPingTimeoutSecs(BigDecimal pingTimeoutSecs) {
    this.pingTimeoutSecs = pingTimeoutSecs;
  }

  public ApiPerfInspectorPingArgs pingCount(BigDecimal pingCount) {
    this.pingCount = pingCount;
    return this;
  }

   /**
   * Number of iterations of the ping request to each target host. If not specified, defaults to 10 count.
   * @return pingCount
  **/
  @ApiModelProperty(example = "10.0", value = "Number of iterations of the ping request to each target host. If not specified, defaults to 10 count.")
  public BigDecimal getPingCount() {
    return pingCount;
  }

  public void setPingCount(BigDecimal pingCount) {
    this.pingCount = pingCount;
  }

  public ApiPerfInspectorPingArgs pingPacketSizeBytes(BigDecimal pingPacketSizeBytes) {
    this.pingPacketSizeBytes = pingPacketSizeBytes;
    return this;
  }

   /**
   * Packet size in bytes for each ping request. If not specified, defaults to 56 bytes.
   * @return pingPacketSizeBytes
  **/
  @ApiModelProperty(example = "56.0", value = "Packet size in bytes for each ping request. If not specified, defaults to 56 bytes.")
  public BigDecimal getPingPacketSizeBytes() {
    return pingPacketSizeBytes;
  }

  public void setPingPacketSizeBytes(BigDecimal pingPacketSizeBytes) {
    this.pingPacketSizeBytes = pingPacketSizeBytes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiPerfInspectorPingArgs apiPerfInspectorPingArgs = (ApiPerfInspectorPingArgs) o;
    return Objects.equals(this.pingTimeoutSecs, apiPerfInspectorPingArgs.pingTimeoutSecs) &&
        Objects.equals(this.pingCount, apiPerfInspectorPingArgs.pingCount) &&
        Objects.equals(this.pingPacketSizeBytes, apiPerfInspectorPingArgs.pingPacketSizeBytes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pingTimeoutSecs, pingCount, pingPacketSizeBytes);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiPerfInspectorPingArgs {\n");
    
    sb.append("    pingTimeoutSecs: ").append(toIndentedString(pingTimeoutSecs)).append("\n");
    sb.append("    pingCount: ").append(toIndentedString(pingCount)).append("\n");
    sb.append("    pingPacketSizeBytes: ").append(toIndentedString(pingPacketSizeBytes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

