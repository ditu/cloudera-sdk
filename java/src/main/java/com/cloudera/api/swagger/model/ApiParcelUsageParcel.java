/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiParcelRef;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * This object is used to represent a parcel within an ApiParcelUsage.
 */
@ApiModel(description = "This object is used to represent a parcel within an ApiParcelUsage.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiParcelUsageParcel {
  @SerializedName("parcelRef")
  private ApiParcelRef parcelRef = null;

  @SerializedName("processCount")
  private BigDecimal processCount = null;

  @SerializedName("activated")
  private Boolean activated = null;

  public ApiParcelUsageParcel parcelRef(ApiParcelRef parcelRef) {
    this.parcelRef = parcelRef;
    return this;
  }

   /**
   * Reference to the corresponding Parcel object.
   * @return parcelRef
  **/
  @ApiModelProperty(value = "Reference to the corresponding Parcel object.")
  public ApiParcelRef getParcelRef() {
    return parcelRef;
  }

  public void setParcelRef(ApiParcelRef parcelRef) {
    this.parcelRef = parcelRef;
  }

  public ApiParcelUsageParcel processCount(BigDecimal processCount) {
    this.processCount = processCount;
    return this;
  }

   /**
   * How many running processes on the cluster are using the parcel.
   * @return processCount
  **/
  @ApiModelProperty(value = "How many running processes on the cluster are using the parcel.")
  public BigDecimal getProcessCount() {
    return processCount;
  }

  public void setProcessCount(BigDecimal processCount) {
    this.processCount = processCount;
  }

  public ApiParcelUsageParcel activated(Boolean activated) {
    this.activated = activated;
    return this;
  }

   /**
   * Is this parcel currently activated on the cluster.
   * @return activated
  **/
  @ApiModelProperty(value = "Is this parcel currently activated on the cluster.")
  public Boolean getActivated() {
    return activated;
  }

  public void setActivated(Boolean activated) {
    this.activated = activated;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiParcelUsageParcel apiParcelUsageParcel = (ApiParcelUsageParcel) o;
    return Objects.equals(this.parcelRef, apiParcelUsageParcel.parcelRef) &&
        Objects.equals(this.processCount, apiParcelUsageParcel.processCount) &&
        Objects.equals(this.activated, apiParcelUsageParcel.activated);
  }

  @Override
  public int hashCode() {
    return Objects.hash(parcelRef, processCount, activated);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiParcelUsageParcel {\n");
    
    sb.append("    parcelRef: ").append(toIndentedString(parcelRef)).append("\n");
    sb.append("    processCount: ").append(toIndentedString(processCount)).append("\n");
    sb.append("    activated: ").append(toIndentedString(activated)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

