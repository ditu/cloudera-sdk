/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiEvent;
import com.cloudera.api.swagger.model.ApiListBase;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * A generic list.
 */
@ApiModel(description = "A generic list.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiEventQueryResult {
  @SerializedName("totalResults")
  private BigDecimal totalResults = null;

  @SerializedName("items")
  private List<ApiEvent> items = null;

  public ApiEventQueryResult totalResults(BigDecimal totalResults) {
    this.totalResults = totalResults;
    return this;
  }

   /**
   * The total number of matched results. Some are possibly not shown due to pagination.
   * @return totalResults
  **/
  @ApiModelProperty(value = "The total number of matched results. Some are possibly not shown due to pagination.")
  public BigDecimal getTotalResults() {
    return totalResults;
  }

  public void setTotalResults(BigDecimal totalResults) {
    this.totalResults = totalResults;
  }

  public ApiEventQueryResult items(List<ApiEvent> items) {
    this.items = items;
    return this;
  }

  public ApiEventQueryResult addItemsItem(ApiEvent itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<ApiEvent>();
    }
    this.items.add(itemsItem);
    return this;
  }

   /**
   * 
   * @return items
  **/
  @ApiModelProperty(value = "")
  public List<ApiEvent> getItems() {
    return items;
  }

  public void setItems(List<ApiEvent> items) {
    this.items = items;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiEventQueryResult apiEventQueryResult = (ApiEventQueryResult) o;
    return Objects.equals(this.totalResults, apiEventQueryResult.totalResults) &&
        Objects.equals(this.items, apiEventQueryResult.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalResults, items);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiEventQueryResult {\n");
    
    sb.append("    totalResults: ").append(toIndentedString(totalResults)).append("\n");
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

