/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiListBase;
import com.cloudera.api.swagger.model.ApiMrUsageReportRow;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A generic list.
 */
@ApiModel(description = "A generic list.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiMrUsageReport {
  @SerializedName("items")
  private List<ApiMrUsageReportRow> items = null;

  public ApiMrUsageReport items(List<ApiMrUsageReportRow> items) {
    this.items = items;
    return this;
  }

  public ApiMrUsageReport addItemsItem(ApiMrUsageReportRow itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<ApiMrUsageReportRow>();
    }
    this.items.add(itemsItem);
    return this;
  }

   /**
   * A list of per-user usage information at the requested time granularity.
   * @return items
  **/
  @ApiModelProperty(value = "A list of per-user usage information at the requested time granularity.")
  public List<ApiMrUsageReportRow> getItems() {
    return items;
  }

  public void setItems(List<ApiMrUsageReportRow> items) {
    this.items = items;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiMrUsageReport apiMrUsageReport = (ApiMrUsageReport) o;
    return Objects.equals(this.items, apiMrUsageReport.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(items);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiMrUsageReport {\n");
    
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

