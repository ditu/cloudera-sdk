/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiClusterRef;
import com.cloudera.api.swagger.model.ApiCommand;
import com.cloudera.api.swagger.model.ApiCommandList;
import com.cloudera.api.swagger.model.ApiHostRef;
import com.cloudera.api.swagger.model.ApiRoleRef;
import com.cloudera.api.swagger.model.ApiServiceRef;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Provides detailed information about a submitted command.  &lt;p&gt;There are two types of commands: synchronous and asynchronous. Synchronous commands complete immediately, and their results are passed back in the returned command object after the execution of an API call. Outside of that returned object, there is no way to check the result of a synchronous command.&lt;/p&gt;  &lt;p&gt;Asynchronous commands have unique non-negative IDs. They may still be running when the API call returns. Clients can check the status of such commands using the API.&lt;/p&gt;
 */
@ApiModel(description = "Provides detailed information about a submitted command.  <p>There are two types of commands: synchronous and asynchronous. Synchronous commands complete immediately, and their results are passed back in the returned command object after the execution of an API call. Outside of that returned object, there is no way to check the result of a synchronous command.</p>  <p>Asynchronous commands have unique non-negative IDs. They may still be running when the API call returns. Clients can check the status of such commands using the API.</p>")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiCommand {
  @SerializedName("id")
  private BigDecimal id = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("startTime")
  private String startTime = null;

  @SerializedName("endTime")
  private String endTime = null;

  @SerializedName("active")
  private Boolean active = null;

  @SerializedName("success")
  private Boolean success = null;

  @SerializedName("resultMessage")
  private String resultMessage = null;

  @SerializedName("resultDataUrl")
  private String resultDataUrl = null;

  @SerializedName("clusterRef")
  private ApiClusterRef clusterRef = null;

  @SerializedName("serviceRef")
  private ApiServiceRef serviceRef = null;

  @SerializedName("roleRef")
  private ApiRoleRef roleRef = null;

  @SerializedName("hostRef")
  private ApiHostRef hostRef = null;

  @SerializedName("parent")
  private ApiCommand parent = null;

  @SerializedName("children")
  private ApiCommandList children = null;

  @SerializedName("canRetry")
  private Boolean canRetry = null;

  public ApiCommand id(BigDecimal id) {
    this.id = id;
    return this;
  }

   /**
   * The command ID.
   * @return id
  **/
  @ApiModelProperty(value = "The command ID.")
  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public ApiCommand name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The command name.
   * @return name
  **/
  @ApiModelProperty(value = "The command name.")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ApiCommand startTime(String startTime) {
    this.startTime = startTime;
    return this;
  }

   /**
   * The start time.
   * @return startTime
  **/
  @ApiModelProperty(value = "The start time.")
  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public ApiCommand endTime(String endTime) {
    this.endTime = endTime;
    return this;
  }

   /**
   * The end time, if the command is finished.
   * @return endTime
  **/
  @ApiModelProperty(value = "The end time, if the command is finished.")
  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public ApiCommand active(Boolean active) {
    this.active = active;
    return this;
  }

   /**
   * Whether the command is currently active.
   * @return active
  **/
  @ApiModelProperty(value = "Whether the command is currently active.")
  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public ApiCommand success(Boolean success) {
    this.success = success;
    return this;
  }

   /**
   * If the command is finished, whether it was successful.
   * @return success
  **/
  @ApiModelProperty(value = "If the command is finished, whether it was successful.")
  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public ApiCommand resultMessage(String resultMessage) {
    this.resultMessage = resultMessage;
    return this;
  }

   /**
   * If the command is finished, the result message.
   * @return resultMessage
  **/
  @ApiModelProperty(value = "If the command is finished, the result message.")
  public String getResultMessage() {
    return resultMessage;
  }

  public void setResultMessage(String resultMessage) {
    this.resultMessage = resultMessage;
  }

  public ApiCommand resultDataUrl(String resultDataUrl) {
    this.resultDataUrl = resultDataUrl;
    return this;
  }

   /**
   * URL to the command&#39;s downloadable result data, if any exists.
   * @return resultDataUrl
  **/
  @ApiModelProperty(value = "URL to the command's downloadable result data, if any exists.")
  public String getResultDataUrl() {
    return resultDataUrl;
  }

  public void setResultDataUrl(String resultDataUrl) {
    this.resultDataUrl = resultDataUrl;
  }

  public ApiCommand clusterRef(ApiClusterRef clusterRef) {
    this.clusterRef = clusterRef;
    return this;
  }

   /**
   * Reference to the cluster (for cluster commands only).
   * @return clusterRef
  **/
  @ApiModelProperty(value = "Reference to the cluster (for cluster commands only).")
  public ApiClusterRef getClusterRef() {
    return clusterRef;
  }

  public void setClusterRef(ApiClusterRef clusterRef) {
    this.clusterRef = clusterRef;
  }

  public ApiCommand serviceRef(ApiServiceRef serviceRef) {
    this.serviceRef = serviceRef;
    return this;
  }

   /**
   * Reference to the service (for service commands only).
   * @return serviceRef
  **/
  @ApiModelProperty(value = "Reference to the service (for service commands only).")
  public ApiServiceRef getServiceRef() {
    return serviceRef;
  }

  public void setServiceRef(ApiServiceRef serviceRef) {
    this.serviceRef = serviceRef;
  }

  public ApiCommand roleRef(ApiRoleRef roleRef) {
    this.roleRef = roleRef;
    return this;
  }

   /**
   * Reference to the role (for role commands only).
   * @return roleRef
  **/
  @ApiModelProperty(value = "Reference to the role (for role commands only).")
  public ApiRoleRef getRoleRef() {
    return roleRef;
  }

  public void setRoleRef(ApiRoleRef roleRef) {
    this.roleRef = roleRef;
  }

  public ApiCommand hostRef(ApiHostRef hostRef) {
    this.hostRef = hostRef;
    return this;
  }

   /**
   * Reference to the host (for host commands only).
   * @return hostRef
  **/
  @ApiModelProperty(value = "Reference to the host (for host commands only).")
  public ApiHostRef getHostRef() {
    return hostRef;
  }

  public void setHostRef(ApiHostRef hostRef) {
    this.hostRef = hostRef;
  }

  public ApiCommand parent(ApiCommand parent) {
    this.parent = parent;
    return this;
  }

   /**
   * Reference to the parent command, if any.
   * @return parent
  **/
  @ApiModelProperty(value = "Reference to the parent command, if any.")
  public ApiCommand getParent() {
    return parent;
  }

  public void setParent(ApiCommand parent) {
    this.parent = parent;
  }

  public ApiCommand children(ApiCommandList children) {
    this.children = children;
    return this;
  }

   /**
   * List of child commands. Only available in the full view. &lt;p&gt; The list contains only the summary view of the children.
   * @return children
  **/
  @ApiModelProperty(value = "List of child commands. Only available in the full view. <p> The list contains only the summary view of the children.")
  public ApiCommandList getChildren() {
    return children;
  }

  public void setChildren(ApiCommandList children) {
    this.children = children;
  }

  public ApiCommand canRetry(Boolean canRetry) {
    this.canRetry = canRetry;
    return this;
  }

   /**
   * If the command can be retried. Available since V11
   * @return canRetry
  **/
  @ApiModelProperty(value = "If the command can be retried. Available since V11")
  public Boolean getCanRetry() {
    return canRetry;
  }

  public void setCanRetry(Boolean canRetry) {
    this.canRetry = canRetry;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiCommand apiCommand = (ApiCommand) o;
    return Objects.equals(this.id, apiCommand.id) &&
        Objects.equals(this.name, apiCommand.name) &&
        Objects.equals(this.startTime, apiCommand.startTime) &&
        Objects.equals(this.endTime, apiCommand.endTime) &&
        Objects.equals(this.active, apiCommand.active) &&
        Objects.equals(this.success, apiCommand.success) &&
        Objects.equals(this.resultMessage, apiCommand.resultMessage) &&
        Objects.equals(this.resultDataUrl, apiCommand.resultDataUrl) &&
        Objects.equals(this.clusterRef, apiCommand.clusterRef) &&
        Objects.equals(this.serviceRef, apiCommand.serviceRef) &&
        Objects.equals(this.roleRef, apiCommand.roleRef) &&
        Objects.equals(this.hostRef, apiCommand.hostRef) &&
        Objects.equals(this.parent, apiCommand.parent) &&
        Objects.equals(this.children, apiCommand.children) &&
        Objects.equals(this.canRetry, apiCommand.canRetry);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, startTime, endTime, active, success, resultMessage, resultDataUrl, clusterRef, serviceRef, roleRef, hostRef, parent, children, canRetry);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiCommand {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    endTime: ").append(toIndentedString(endTime)).append("\n");
    sb.append("    active: ").append(toIndentedString(active)).append("\n");
    sb.append("    success: ").append(toIndentedString(success)).append("\n");
    sb.append("    resultMessage: ").append(toIndentedString(resultMessage)).append("\n");
    sb.append("    resultDataUrl: ").append(toIndentedString(resultDataUrl)).append("\n");
    sb.append("    clusterRef: ").append(toIndentedString(clusterRef)).append("\n");
    sb.append("    serviceRef: ").append(toIndentedString(serviceRef)).append("\n");
    sb.append("    roleRef: ").append(toIndentedString(roleRef)).append("\n");
    sb.append("    hostRef: ").append(toIndentedString(hostRef)).append("\n");
    sb.append("    parent: ").append(toIndentedString(parent)).append("\n");
    sb.append("    children: ").append(toIndentedString(children)).append("\n");
    sb.append("    canRetry: ").append(toIndentedString(canRetry)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

