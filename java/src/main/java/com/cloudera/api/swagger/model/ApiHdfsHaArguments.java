/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Arguments used for HDFS HA commands.
 */
@ApiModel(description = "Arguments used for HDFS HA commands.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiHdfsHaArguments {
  @SerializedName("activeName")
  private String activeName = null;

  @SerializedName("activeSharedEditsPath")
  private String activeSharedEditsPath = null;

  @SerializedName("standByName")
  private String standByName = null;

  @SerializedName("standBySharedEditsPath")
  private String standBySharedEditsPath = null;

  @SerializedName("nameservice")
  private String nameservice = null;

  @SerializedName("startDependentServices")
  private Boolean startDependentServices = null;

  @SerializedName("deployClientConfigs")
  private Boolean deployClientConfigs = null;

  @SerializedName("enableQuorumStorage")
  private Boolean enableQuorumStorage = null;

  public ApiHdfsHaArguments activeName(String activeName) {
    this.activeName = activeName;
    return this;
  }

   /**
   * Name of the active NameNode.
   * @return activeName
  **/
  @ApiModelProperty(value = "Name of the active NameNode.")
  public String getActiveName() {
    return activeName;
  }

  public void setActiveName(String activeName) {
    this.activeName = activeName;
  }

  public ApiHdfsHaArguments activeSharedEditsPath(String activeSharedEditsPath) {
    this.activeSharedEditsPath = activeSharedEditsPath;
    return this;
  }

   /**
   * Path to the shared edits directory on the active NameNode&#39;s host. Ignored if Quorum-based Storage is being enabled.
   * @return activeSharedEditsPath
  **/
  @ApiModelProperty(value = "Path to the shared edits directory on the active NameNode's host. Ignored if Quorum-based Storage is being enabled.")
  public String getActiveSharedEditsPath() {
    return activeSharedEditsPath;
  }

  public void setActiveSharedEditsPath(String activeSharedEditsPath) {
    this.activeSharedEditsPath = activeSharedEditsPath;
  }

  public ApiHdfsHaArguments standByName(String standByName) {
    this.standByName = standByName;
    return this;
  }

   /**
   * Name of the stand-by Namenode.
   * @return standByName
  **/
  @ApiModelProperty(value = "Name of the stand-by Namenode.")
  public String getStandByName() {
    return standByName;
  }

  public void setStandByName(String standByName) {
    this.standByName = standByName;
  }

  public ApiHdfsHaArguments standBySharedEditsPath(String standBySharedEditsPath) {
    this.standBySharedEditsPath = standBySharedEditsPath;
    return this;
  }

   /**
   * Path to the shared edits directory on the stand-by NameNode&#39;s host. Ignored if Quorum-based Storage is being enabled.
   * @return standBySharedEditsPath
  **/
  @ApiModelProperty(value = "Path to the shared edits directory on the stand-by NameNode's host. Ignored if Quorum-based Storage is being enabled.")
  public String getStandBySharedEditsPath() {
    return standBySharedEditsPath;
  }

  public void setStandBySharedEditsPath(String standBySharedEditsPath) {
    this.standBySharedEditsPath = standBySharedEditsPath;
  }

  public ApiHdfsHaArguments nameservice(String nameservice) {
    this.nameservice = nameservice;
    return this;
  }

   /**
   * Nameservice that identifies the HA pair.
   * @return nameservice
  **/
  @ApiModelProperty(value = "Nameservice that identifies the HA pair.")
  public String getNameservice() {
    return nameservice;
  }

  public void setNameservice(String nameservice) {
    this.nameservice = nameservice;
  }

  public ApiHdfsHaArguments startDependentServices(Boolean startDependentServices) {
    this.startDependentServices = startDependentServices;
    return this;
  }

   /**
   * Whether to re-start dependent services. Defaults to true.
   * @return startDependentServices
  **/
  @ApiModelProperty(value = "Whether to re-start dependent services. Defaults to true.")
  public Boolean getStartDependentServices() {
    return startDependentServices;
  }

  public void setStartDependentServices(Boolean startDependentServices) {
    this.startDependentServices = startDependentServices;
  }

  public ApiHdfsHaArguments deployClientConfigs(Boolean deployClientConfigs) {
    this.deployClientConfigs = deployClientConfigs;
    return this;
  }

   /**
   * Whether to re-deploy client configurations. Defaults to true.
   * @return deployClientConfigs
  **/
  @ApiModelProperty(value = "Whether to re-deploy client configurations. Defaults to true.")
  public Boolean getDeployClientConfigs() {
    return deployClientConfigs;
  }

  public void setDeployClientConfigs(Boolean deployClientConfigs) {
    this.deployClientConfigs = deployClientConfigs;
  }

  public ApiHdfsHaArguments enableQuorumStorage(Boolean enableQuorumStorage) {
    this.enableQuorumStorage = enableQuorumStorage;
    return this;
  }

   /**
   * This parameter has been deprecated as of CM 5.0, where HA is only supported using Quorum-based Storage. &lt;p&gt; Whether to enable Quorum-based Storage.  Enabling Quorum-based Storage requires a minimum of three and an odd number of JournalNodes to be created and configured before enabling HDFS HA. &lt;p&gt; Available since API v2.
   * @return enableQuorumStorage
  **/
  @ApiModelProperty(value = "This parameter has been deprecated as of CM 5.0, where HA is only supported using Quorum-based Storage. <p> Whether to enable Quorum-based Storage.  Enabling Quorum-based Storage requires a minimum of three and an odd number of JournalNodes to be created and configured before enabling HDFS HA. <p> Available since API v2.")
  public Boolean getEnableQuorumStorage() {
    return enableQuorumStorage;
  }

  public void setEnableQuorumStorage(Boolean enableQuorumStorage) {
    this.enableQuorumStorage = enableQuorumStorage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiHdfsHaArguments apiHdfsHaArguments = (ApiHdfsHaArguments) o;
    return Objects.equals(this.activeName, apiHdfsHaArguments.activeName) &&
        Objects.equals(this.activeSharedEditsPath, apiHdfsHaArguments.activeSharedEditsPath) &&
        Objects.equals(this.standByName, apiHdfsHaArguments.standByName) &&
        Objects.equals(this.standBySharedEditsPath, apiHdfsHaArguments.standBySharedEditsPath) &&
        Objects.equals(this.nameservice, apiHdfsHaArguments.nameservice) &&
        Objects.equals(this.startDependentServices, apiHdfsHaArguments.startDependentServices) &&
        Objects.equals(this.deployClientConfigs, apiHdfsHaArguments.deployClientConfigs) &&
        Objects.equals(this.enableQuorumStorage, apiHdfsHaArguments.enableQuorumStorage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(activeName, activeSharedEditsPath, standByName, standBySharedEditsPath, nameservice, startDependentServices, deployClientConfigs, enableQuorumStorage);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiHdfsHaArguments {\n");
    
    sb.append("    activeName: ").append(toIndentedString(activeName)).append("\n");
    sb.append("    activeSharedEditsPath: ").append(toIndentedString(activeSharedEditsPath)).append("\n");
    sb.append("    standByName: ").append(toIndentedString(standByName)).append("\n");
    sb.append("    standBySharedEditsPath: ").append(toIndentedString(standBySharedEditsPath)).append("\n");
    sb.append("    nameservice: ").append(toIndentedString(nameservice)).append("\n");
    sb.append("    startDependentServices: ").append(toIndentedString(startDependentServices)).append("\n");
    sb.append("    deployClientConfigs: ").append(toIndentedString(deployClientConfigs)).append("\n");
    sb.append("    enableQuorumStorage: ").append(toIndentedString(enableQuorumStorage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

