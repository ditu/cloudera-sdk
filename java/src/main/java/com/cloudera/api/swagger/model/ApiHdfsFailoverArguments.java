/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiServiceRef;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Arguments used when enabling HDFS automatic failover.
 */
@ApiModel(description = "Arguments used when enabling HDFS automatic failover.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiHdfsFailoverArguments {
  @SerializedName("nameservice")
  private String nameservice = null;

  @SerializedName("zooKeeperService")
  private ApiServiceRef zooKeeperService = null;

  @SerializedName("activeFCName")
  private String activeFCName = null;

  @SerializedName("standByFCName")
  private String standByFCName = null;

  public ApiHdfsFailoverArguments nameservice(String nameservice) {
    this.nameservice = nameservice;
    return this;
  }

   /**
   * Nameservice for which to enable automatic failover.
   * @return nameservice
  **/
  @ApiModelProperty(value = "Nameservice for which to enable automatic failover.")
  public String getNameservice() {
    return nameservice;
  }

  public void setNameservice(String nameservice) {
    this.nameservice = nameservice;
  }

  public ApiHdfsFailoverArguments zooKeeperService(ApiServiceRef zooKeeperService) {
    this.zooKeeperService = zooKeeperService;
    return this;
  }

   /**
   * The ZooKeeper service to use.
   * @return zooKeeperService
  **/
  @ApiModelProperty(value = "The ZooKeeper service to use.")
  public ApiServiceRef getZooKeeperService() {
    return zooKeeperService;
  }

  public void setZooKeeperService(ApiServiceRef zooKeeperService) {
    this.zooKeeperService = zooKeeperService;
  }

  public ApiHdfsFailoverArguments activeFCName(String activeFCName) {
    this.activeFCName = activeFCName;
    return this;
  }

   /**
   * Name of the failover controller to create for the active NameNode.
   * @return activeFCName
  **/
  @ApiModelProperty(value = "Name of the failover controller to create for the active NameNode.")
  public String getActiveFCName() {
    return activeFCName;
  }

  public void setActiveFCName(String activeFCName) {
    this.activeFCName = activeFCName;
  }

  public ApiHdfsFailoverArguments standByFCName(String standByFCName) {
    this.standByFCName = standByFCName;
    return this;
  }

   /**
   * Name of the failover controller to create for the stand-by NameNode.
   * @return standByFCName
  **/
  @ApiModelProperty(value = "Name of the failover controller to create for the stand-by NameNode.")
  public String getStandByFCName() {
    return standByFCName;
  }

  public void setStandByFCName(String standByFCName) {
    this.standByFCName = standByFCName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiHdfsFailoverArguments apiHdfsFailoverArguments = (ApiHdfsFailoverArguments) o;
    return Objects.equals(this.nameservice, apiHdfsFailoverArguments.nameservice) &&
        Objects.equals(this.zooKeeperService, apiHdfsFailoverArguments.zooKeeperService) &&
        Objects.equals(this.activeFCName, apiHdfsFailoverArguments.activeFCName) &&
        Objects.equals(this.standByFCName, apiHdfsFailoverArguments.standByFCName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nameservice, zooKeeperService, activeFCName, standByFCName);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiHdfsFailoverArguments {\n");
    
    sb.append("    nameservice: ").append(toIndentedString(nameservice)).append("\n");
    sb.append("    zooKeeperService: ").append(toIndentedString(zooKeeperService)).append("\n");
    sb.append("    activeFCName: ").append(toIndentedString(activeFCName)).append("\n");
    sb.append("    standByFCName: ").append(toIndentedString(standByFCName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

