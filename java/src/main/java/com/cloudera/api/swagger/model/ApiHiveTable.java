/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * A Hive table identifier.
 */
@ApiModel(description = "A Hive table identifier.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiHiveTable {
  @SerializedName("database")
  private String database = null;

  @SerializedName("tableName")
  private String tableName = null;

  public ApiHiveTable database(String database) {
    this.database = database;
    return this;
  }

   /**
   * Name of the database to which this table belongs.
   * @return database
  **/
  @ApiModelProperty(value = "Name of the database to which this table belongs.")
  public String getDatabase() {
    return database;
  }

  public void setDatabase(String database) {
    this.database = database;
  }

  public ApiHiveTable tableName(String tableName) {
    this.tableName = tableName;
    return this;
  }

   /**
   * Name of the table. When used as input for a replication job, this can be a regular expression that matches several table names. Refer to the Hive documentation for the syntax of regular expressions.
   * @return tableName
  **/
  @ApiModelProperty(value = "Name of the table. When used as input for a replication job, this can be a regular expression that matches several table names. Refer to the Hive documentation for the syntax of regular expressions.")
  public String getTableName() {
    return tableName;
  }

  public void setTableName(String tableName) {
    this.tableName = tableName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiHiveTable apiHiveTable = (ApiHiveTable) o;
    return Objects.equals(this.database, apiHiveTable.database) &&
        Objects.equals(this.tableName, apiHiveTable.tableName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(database, tableName);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiHiveTable {\n");
    
    sb.append("    database: ").append(toIndentedString(database)).append("\n");
    sb.append("    tableName: ").append(toIndentedString(tableName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

