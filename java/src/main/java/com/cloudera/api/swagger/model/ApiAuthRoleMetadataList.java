/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiAuthRoleMetadata;
import com.cloudera.api.swagger.model.ApiListBase;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A list of auth roles metadata.
 */
@ApiModel(description = "A list of auth roles metadata.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiAuthRoleMetadataList {
  @SerializedName("items")
  private List<ApiAuthRoleMetadata> items = null;

  public ApiAuthRoleMetadataList items(List<ApiAuthRoleMetadata> items) {
    this.items = items;
    return this;
  }

  public ApiAuthRoleMetadataList addItemsItem(ApiAuthRoleMetadata itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<ApiAuthRoleMetadata>();
    }
    this.items.add(itemsItem);
    return this;
  }

   /**
   * 
   * @return items
  **/
  @ApiModelProperty(value = "")
  public List<ApiAuthRoleMetadata> getItems() {
    return items;
  }

  public void setItems(List<ApiAuthRoleMetadata> items) {
    this.items = items;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiAuthRoleMetadataList apiAuthRoleMetadataList = (ApiAuthRoleMetadataList) o;
    return Objects.equals(this.items, apiAuthRoleMetadataList.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(items);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiAuthRoleMetadataList {\n");
    
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

