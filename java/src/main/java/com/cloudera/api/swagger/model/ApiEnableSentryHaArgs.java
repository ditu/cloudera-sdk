/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.cloudera.api.swagger.model.ApiSimpleRollingRestartClusterArgs;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Arguments used for enable Sentry HA command.
 */
@ApiModel(description = "Arguments used for enable Sentry HA command.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiEnableSentryHaArgs {
  @SerializedName("newSentryHostId")
  private String newSentryHostId = null;

  @SerializedName("newSentryRoleName")
  private String newSentryRoleName = null;

  @SerializedName("zkServiceName")
  private String zkServiceName = null;

  @SerializedName("rrcArgs")
  private ApiSimpleRollingRestartClusterArgs rrcArgs = null;

  public ApiEnableSentryHaArgs newSentryHostId(String newSentryHostId) {
    this.newSentryHostId = newSentryHostId;
    return this;
  }

   /**
   * Id of host on which new Sentry Server role will be added.
   * @return newSentryHostId
  **/
  @ApiModelProperty(value = "Id of host on which new Sentry Server role will be added.")
  public String getNewSentryHostId() {
    return newSentryHostId;
  }

  public void setNewSentryHostId(String newSentryHostId) {
    this.newSentryHostId = newSentryHostId;
  }

  public ApiEnableSentryHaArgs newSentryRoleName(String newSentryRoleName) {
    this.newSentryRoleName = newSentryRoleName;
    return this;
  }

   /**
   * Name of the new Sentry Server role to be created. This is an optional argument.
   * @return newSentryRoleName
  **/
  @ApiModelProperty(value = "Name of the new Sentry Server role to be created. This is an optional argument.")
  public String getNewSentryRoleName() {
    return newSentryRoleName;
  }

  public void setNewSentryRoleName(String newSentryRoleName) {
    this.newSentryRoleName = newSentryRoleName;
  }

  public ApiEnableSentryHaArgs zkServiceName(String zkServiceName) {
    this.zkServiceName = zkServiceName;
    return this;
  }

   /**
   * Name of the ZooKeeper service that will be used for Sentry HA. This is an optional parameter if the Sentry to ZooKeeper dependency is already set in CM.
   * @return zkServiceName
  **/
  @ApiModelProperty(value = "Name of the ZooKeeper service that will be used for Sentry HA. This is an optional parameter if the Sentry to ZooKeeper dependency is already set in CM.")
  public String getZkServiceName() {
    return zkServiceName;
  }

  public void setZkServiceName(String zkServiceName) {
    this.zkServiceName = zkServiceName;
  }

  public ApiEnableSentryHaArgs rrcArgs(ApiSimpleRollingRestartClusterArgs rrcArgs) {
    this.rrcArgs = rrcArgs;
    return this;
  }

   /**
   * 
   * @return rrcArgs
  **/
  @ApiModelProperty(value = "")
  public ApiSimpleRollingRestartClusterArgs getRrcArgs() {
    return rrcArgs;
  }

  public void setRrcArgs(ApiSimpleRollingRestartClusterArgs rrcArgs) {
    this.rrcArgs = rrcArgs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiEnableSentryHaArgs apiEnableSentryHaArgs = (ApiEnableSentryHaArgs) o;
    return Objects.equals(this.newSentryHostId, apiEnableSentryHaArgs.newSentryHostId) &&
        Objects.equals(this.newSentryRoleName, apiEnableSentryHaArgs.newSentryRoleName) &&
        Objects.equals(this.zkServiceName, apiEnableSentryHaArgs.zkServiceName) &&
        Objects.equals(this.rrcArgs, apiEnableSentryHaArgs.rrcArgs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(newSentryHostId, newSentryRoleName, zkServiceName, rrcArgs);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiEnableSentryHaArgs {\n");
    
    sb.append("    newSentryHostId: ").append(toIndentedString(newSentryHostId)).append("\n");
    sb.append("    newSentryRoleName: ").append(toIndentedString(newSentryRoleName)).append("\n");
    sb.append("    zkServiceName: ").append(toIndentedString(zkServiceName)).append("\n");
    sb.append("    rrcArgs: ").append(toIndentedString(rrcArgs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

