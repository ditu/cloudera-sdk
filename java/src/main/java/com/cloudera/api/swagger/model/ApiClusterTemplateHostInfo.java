/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This contains information about the host or host range on which provided host template will be applied.
 */
@ApiModel(description = "This contains information about the host or host range on which provided host template will be applied.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-06T17:03:25.100Z")
public class ApiClusterTemplateHostInfo {
  @SerializedName("hostName")
  private String hostName = null;

  @SerializedName("hostNameRange")
  private String hostNameRange = null;

  @SerializedName("rackId")
  private String rackId = null;

  @SerializedName("hostTemplateRefName")
  private String hostTemplateRefName = null;

  @SerializedName("roleRefNames")
  private List<String> roleRefNames = null;

  public ApiClusterTemplateHostInfo hostName(String hostName) {
    this.hostName = hostName;
    return this;
  }

   /**
   * 
   * @return hostName
  **/
  @ApiModelProperty(value = "")
  public String getHostName() {
    return hostName;
  }

  public void setHostName(String hostName) {
    this.hostName = hostName;
  }

  public ApiClusterTemplateHostInfo hostNameRange(String hostNameRange) {
    this.hostNameRange = hostNameRange;
    return this;
  }

   /**
   * 
   * @return hostNameRange
  **/
  @ApiModelProperty(value = "")
  public String getHostNameRange() {
    return hostNameRange;
  }

  public void setHostNameRange(String hostNameRange) {
    this.hostNameRange = hostNameRange;
  }

  public ApiClusterTemplateHostInfo rackId(String rackId) {
    this.rackId = rackId;
    return this;
  }

   /**
   * 
   * @return rackId
  **/
  @ApiModelProperty(value = "")
  public String getRackId() {
    return rackId;
  }

  public void setRackId(String rackId) {
    this.rackId = rackId;
  }

  public ApiClusterTemplateHostInfo hostTemplateRefName(String hostTemplateRefName) {
    this.hostTemplateRefName = hostTemplateRefName;
    return this;
  }

   /**
   * 
   * @return hostTemplateRefName
  **/
  @ApiModelProperty(value = "")
  public String getHostTemplateRefName() {
    return hostTemplateRefName;
  }

  public void setHostTemplateRefName(String hostTemplateRefName) {
    this.hostTemplateRefName = hostTemplateRefName;
  }

  public ApiClusterTemplateHostInfo roleRefNames(List<String> roleRefNames) {
    this.roleRefNames = roleRefNames;
    return this;
  }

  public ApiClusterTemplateHostInfo addRoleRefNamesItem(String roleRefNamesItem) {
    if (this.roleRefNames == null) {
      this.roleRefNames = new ArrayList<String>();
    }
    this.roleRefNames.add(roleRefNamesItem);
    return this;
  }

   /**
   * 
   * @return roleRefNames
  **/
  @ApiModelProperty(example = "\"null\"", value = "")
  public List<String> getRoleRefNames() {
    return roleRefNames;
  }

  public void setRoleRefNames(List<String> roleRefNames) {
    this.roleRefNames = roleRefNames;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiClusterTemplateHostInfo apiClusterTemplateHostInfo = (ApiClusterTemplateHostInfo) o;
    return Objects.equals(this.hostName, apiClusterTemplateHostInfo.hostName) &&
        Objects.equals(this.hostNameRange, apiClusterTemplateHostInfo.hostNameRange) &&
        Objects.equals(this.rackId, apiClusterTemplateHostInfo.rackId) &&
        Objects.equals(this.hostTemplateRefName, apiClusterTemplateHostInfo.hostTemplateRefName) &&
        Objects.equals(this.roleRefNames, apiClusterTemplateHostInfo.roleRefNames);
  }

  @Override
  public int hashCode() {
    return Objects.hash(hostName, hostNameRange, rackId, hostTemplateRefName, roleRefNames);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiClusterTemplateHostInfo {\n");
    
    sb.append("    hostName: ").append(toIndentedString(hostName)).append("\n");
    sb.append("    hostNameRange: ").append(toIndentedString(hostNameRange)).append("\n");
    sb.append("    rackId: ").append(toIndentedString(rackId)).append("\n");
    sb.append("    hostTemplateRefName: ").append(toIndentedString(hostTemplateRefName)).append("\n");
    sb.append("    roleRefNames: ").append(toIndentedString(roleRefNames)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

