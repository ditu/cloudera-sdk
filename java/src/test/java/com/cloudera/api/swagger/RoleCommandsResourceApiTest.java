/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger;

import com.cloudera.api.swagger.client.ApiException;
import com.cloudera.api.swagger.model.ApiBulkCommandList;
import com.cloudera.api.swagger.model.ApiRoleNameList;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for RoleCommandsResourceApi
 */
@Ignore
public class RoleCommandsResourceApiTest {

    private final RoleCommandsResourceApi api = new RoleCommandsResourceApi();

    
    /**
     * Format HDFS NameNodes.
     *
     * Format HDFS NameNodes. &lt;p&gt; Submit a format request to a list of NameNodes on a service. Note that trying to format a previously formatted NameNode will fail. &lt;p&gt; Note about high availability: when two NameNodes are working in an HA pair, only one of them should be formatted. &lt;p&gt; Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void formatCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.formatCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Bootstrap HDFS stand-by NameNodes.
     *
     * Bootstrap HDFS stand-by NameNodes. &lt;p&gt; Submit a request to synchronize HDFS NameNodes with their assigned HA partners. The command requires that the target NameNodes are part of existing HA pairs, which can be accomplished by setting the nameservice configuration parameter in the NameNode&#39;s configuration. &lt;p&gt; The HA partner must already be formatted and running for this command to run.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void hdfsBootstrapStandByCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.hdfsBootstrapStandByCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Enter safemode for namenodes.
     *
     * Enter safemode for namenodes &lt;p/&gt; Available since API v4.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void hdfsEnterSafemodeTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.hdfsEnterSafemode(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Finalize HDFS NameNode metadata upgrade.
     *
     * Finalize HDFS NameNode metadata upgrade. &lt;p/&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void hdfsFinalizeMetadataUpgradeTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.hdfsFinalizeMetadataUpgrade(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Initialize HDFS HA failover controller metadata.
     *
     * Initialize HDFS HA failover controller metadata. &lt;p&gt; The controllers being initialized must already exist and be properly configured. The command will make sure the needed data is initialized for the controller to work. &lt;p&gt; Only one controller per nameservice needs to be initialized.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void hdfsInitializeAutoFailoverCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.hdfsInitializeAutoFailoverCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Initialize HDFS NameNodes&#39; shared edit directory.
     *
     * Initialize HDFS NameNodes&#39; shared edit directory. &lt;p&gt; Shared edit directories are used when two HDFS NameNodes are operating as a high-availability pair. This command initializes the shared directory to include the necessary metadata. &lt;p&gt; The provided role names should reflect one of the NameNodes in the respective HA pair; the role must be stopped and its data directory must already have been formatted. The shared edits directory must be empty for this command to succeed.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void hdfsInitializeSharedDirCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.hdfsInitializeSharedDirCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Leave safemode for namenodes.
     *
     * Leave safemode for namenodes &lt;p/&gt; Available since API v4.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void hdfsLeaveSafemodeTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.hdfsLeaveSafemode(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Save namespace for namenodes.
     *
     * Save namespace for namenodes &lt;p/&gt; Available since API v4.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void hdfsSaveNamespaceTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.hdfsSaveNamespace(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Run the jmapDump diagnostic command.
     *
     * Run the jmapDump diagnostic command. The command runs the jmap utility to capture a dump of the role&#39;s java heap. &lt;p/&gt; Available since API v8.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void jmapDumpTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.jmapDump(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Run the jmapHisto diagnostic command.
     *
     * Run the jmapHisto diagnostic command. The command runs the jmap utility to capture a histogram of the objects on the role&#39;s java heap. &lt;p/&gt; Available since API v8.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void jmapHistoTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.jmapHisto(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Run the jstack diagnostic command.
     *
     * Run the jstack diagnostic command. The command runs the jstack utility to capture a role&#39;s java thread stacks. &lt;p/&gt; Available since API v8.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void jstackTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.jstack(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Run the lsof diagnostic command.
     *
     * Run the lsof diagnostic command. This command runs the lsof utility to list a role&#39;s open files. &lt;p/&gt; Available since API v8.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void lsofTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.lsof(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Refresh a role&#39;s data.
     *
     * Refresh a role&#39;s data. &lt;p&gt; For MapReduce services, this command should be executed on JobTracker roles. It refreshes the role&#39;s queue and node information. &lt;p&gt; For HDFS services, this command should be executed on NameNode or DataNode roles. For NameNodes, it refreshes the role&#39;s node list. For DataNodes, it refreshes the role&#39;s data directory list and other configuration. &lt;p&gt; For YARN services, this command should be executed on ResourceManager roles. It refreshes the role&#39;s queue and node information. &lt;p&gt; Available since API v1. DataNode data directories refresh available since API v10.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void refreshCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.refreshCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Restart a set of role instances.
     *
     * Restart a set of role instances &lt;p&gt; Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void restartCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.restartCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Execute a role command by name.
     *
     * Execute a role command by name. &lt;p/&gt; Available since API v6.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void roleCommandByNameTest() throws ApiException {
        String clusterName = null;
        String commandName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.roleCommandByName(clusterName, commandName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Start a set of role instances.
     *
     * Start a set of role instances. &lt;p&gt; Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void startCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.startCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Stop a set of role instances.
     *
     * Stop a set of role instances. &lt;p&gt; Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void stopCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.stopCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Create / update the Hue database schema.
     *
     * Create / update the Hue database schema. &lt;p&gt; This command is to be run whenever a new database has been specified or, as necessary, after an upgrade. &lt;p&gt; This request should be sent to Hue servers only.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void syncHueDbCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.syncHueDbCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Cleanup a list of ZooKeeper server roles.
     *
     * Cleanup a list of ZooKeeper server roles. &lt;p&gt; This command removes snapshots and transaction log files kept by ZooKeeper for backup purposes. Refer to the ZooKeeper documentation for more details.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void zooKeeperCleanupCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.zooKeeperCleanupCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Initialize a list of ZooKeeper server roles.
     *
     * Initialize a list of ZooKeeper server roles. &lt;p&gt; This applies to ZooKeeper services from CDH4. Before ZooKeeper server roles can be used, they need to be initialized.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void zooKeeperInitCommandTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiBulkCommandList response = api.zooKeeperInitCommand(clusterName, serviceName, body);

        // TODO: test validations
    }
    
}
