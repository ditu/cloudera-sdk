/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger;

import com.cloudera.api.swagger.client.ApiException;
import com.cloudera.api.swagger.model.ApiCommand;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for CommandsResourceApi
 */
@Ignore
public class CommandsResourceApiTest {

    private final CommandsResourceApi api = new CommandsResourceApi();

    
    /**
     * Abort a running command.
     *
     * Abort a running command.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void abortCommandTest() throws ApiException {
        BigDecimal commandId = null;
        ApiCommand response = api.abortCommand(commandId);

        // TODO: test validations
    }
    
    /**
     * Retrieve detailed information on an asynchronous command.
     *
     * Retrieve detailed information on an asynchronous command.  &lt;p&gt;Cloudera Manager keeps the results and statuses of asynchronous commands, which have non-negative command IDs. On the other hand, synchronous commands complete immediately, and their results are passed back in the return object of the command execution API call. Outside of that return object, there is no way to check the result of a synchronous command.&lt;/p&gt;
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readCommandTest() throws ApiException {
        BigDecimal commandId = null;
        ApiCommand response = api.readCommand(commandId);

        // TODO: test validations
    }
    
    /**
     * Try to rerun a command.
     *
     * Try to rerun a command.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void retryTest() throws ApiException {
        BigDecimal commandId = null;
        ApiCommand response = api.retry(commandId);

        // TODO: test validations
    }
    
}
