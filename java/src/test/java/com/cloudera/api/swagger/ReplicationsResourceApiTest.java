/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger;

import com.cloudera.api.swagger.client.ApiException;
import com.cloudera.api.swagger.model.ApiCommand;
import com.cloudera.api.swagger.model.ApiReplicationCommandList;
import com.cloudera.api.swagger.model.ApiReplicationDiagnosticsCollectionArgs;
import com.cloudera.api.swagger.model.ApiReplicationSchedule;
import com.cloudera.api.swagger.model.ApiReplicationScheduleList;
import com.cloudera.api.swagger.model.ApiReplicationState;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for ReplicationsResourceApi
 */
@Ignore
public class ReplicationsResourceApiTest {

    private final ReplicationsResourceApi api = new ReplicationsResourceApi();

    
    /**
     * Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.
     *
     * Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.  The returned command&#39;s resultDataUrl property, upon the commands completion, will refer to the generated diagnostic data. Available since API v11.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void collectDiagnosticDataTest() throws ApiException {
        String clusterName = null;
        BigDecimal scheduleId = null;
        String serviceName = null;
        String view = null;
        ApiReplicationDiagnosticsCollectionArgs body = null;
        ApiCommand response = api.collectDiagnosticData(clusterName, scheduleId, serviceName, view, body);

        // TODO: test validations
    }
    
    /**
     * Creates one or more replication schedules.
     *
     * Creates one or more replication schedules. &lt;p&gt; Available since API v3. Only available with Cloudera Manager Enterprise Edition.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void createSchedulesTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiReplicationScheduleList body = null;
        ApiReplicationScheduleList response = api.createSchedules(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Deletes all existing replication schedules.
     *
     * Deletes all existing replication schedules. &lt;p&gt; Available since API v3. Only available with Cloudera Manager Enterprise Edition.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteAllSchedulesTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiReplicationScheduleList response = api.deleteAllSchedules(clusterName, serviceName);

        // TODO: test validations
    }
    
    /**
     * Deletes an existing replication schedule.
     *
     * Deletes an existing replication schedule. &lt;p&gt; Available since API v3. Only available with Cloudera Manager Enterprise Edition.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteScheduleTest() throws ApiException {
        String clusterName = null;
        BigDecimal scheduleId = null;
        String serviceName = null;
        ApiReplicationSchedule response = api.deleteSchedule(clusterName, scheduleId, serviceName);

        // TODO: test validations
    }
    
    /**
     * returns the replication state.
     *
     * returns the replication state. for example if incremental export is enabled, etc
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getReplicationStateTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        String view = null;
        ApiReplicationState response = api.getReplicationState(clusterName, serviceName, view);

        // TODO: test validations
    }
    
    /**
     * Returns a list of commands triggered by a schedule.
     *
     * Returns a list of commands triggered by a schedule.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readHistoryTest() throws ApiException {
        String clusterName = null;
        BigDecimal scheduleId = null;
        String serviceName = null;
        Integer limit = null;
        Integer offset = null;
        String view = null;
        ApiReplicationCommandList response = api.readHistory(clusterName, scheduleId, serviceName, limit, offset, view);

        // TODO: test validations
    }
    
    /**
     * Returns information for a specific replication schedule.
     *
     * Returns information for a specific replication schedule. &lt;p&gt; Available since API v3. Only available with Cloudera Manager Enterprise Edition.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readScheduleTest() throws ApiException {
        String clusterName = null;
        BigDecimal scheduleId = null;
        String serviceName = null;
        String view = null;
        ApiReplicationSchedule response = api.readSchedule(clusterName, scheduleId, serviceName, view);

        // TODO: test validations
    }
    
    /**
     * Returns information for all replication schedules.
     *
     * Returns information for all replication schedules. &lt;p&gt; Available since API v11.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readSchedulesTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        String limits = null;
        ApiReplicationScheduleList response = api.readSchedules(clusterName, serviceName, limits);

        // TODO: test validations
    }
    
    /**
     * Run the hdfs copy listing command.
     *
     * Run the hdfs copy listing command &lt;p&gt; The copy listing command will be triggered with the provided arguments &lt;p&gt; Available since API v18. Only available with Cloudera Manager Enterprise Edition.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void runCopyListingTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        String body = null;
        ApiCommand response = api.runCopyListing(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Run the schedule immediately.
     *
     * Run the schedule immediately. &lt;p&gt; The replication command will be triggered with the configured arguments, and will be recorded in the schedule&#39;s history. &lt;p&gt; Available since API v3. Only available with Cloudera Manager Enterprise Edition.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void runScheduleTest() throws ApiException {
        String clusterName = null;
        BigDecimal scheduleId = null;
        String serviceName = null;
        Boolean dryRun = null;
        ApiCommand response = api.runSchedule(clusterName, scheduleId, serviceName, dryRun);

        // TODO: test validations
    }
    
    /**
     * Updates an existing replication schedule.
     *
     * Updates an existing replication schedule. &lt;p&gt; Available since API v3. Only available with Cloudera Manager Enterprise Edition.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateScheduleTest() throws ApiException {
        String clusterName = null;
        BigDecimal scheduleId = null;
        String serviceName = null;
        ApiReplicationSchedule body = null;
        ApiReplicationSchedule response = api.updateSchedule(clusterName, scheduleId, serviceName, body);

        // TODO: test validations
    }
    
}
