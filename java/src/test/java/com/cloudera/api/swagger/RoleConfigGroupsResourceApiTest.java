/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger;

import com.cloudera.api.swagger.client.ApiException;
import com.cloudera.api.swagger.model.ApiConfigList;
import com.cloudera.api.swagger.model.ApiRoleConfigGroup;
import com.cloudera.api.swagger.model.ApiRoleConfigGroupList;
import com.cloudera.api.swagger.model.ApiRoleList;
import com.cloudera.api.swagger.model.ApiRoleNameList;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for RoleConfigGroupsResourceApi
 */
@Ignore
public class RoleConfigGroupsResourceApiTest {

    private final RoleConfigGroupsResourceApi api = new RoleConfigGroupsResourceApi();

    
    /**
     * Creates new role config groups.
     *
     * Creates new role config groups. It is not allowed to create base groups (base must be set to false.) &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void createRoleConfigGroupsTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleConfigGroupList body = null;
        ApiRoleConfigGroupList response = api.createRoleConfigGroups(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Deletes a role config group.
     *
     * Deletes a role config group. &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteRoleConfigGroupTest() throws ApiException {
        String clusterName = null;
        String roleConfigGroupName = null;
        String serviceName = null;
        ApiRoleConfigGroup response = api.deleteRoleConfigGroup(clusterName, roleConfigGroupName, serviceName);

        // TODO: test validations
    }
    
    /**
     * Moves roles to the specified role config group.
     *
     * Moves roles to the specified role config group.  The roles can be moved from any role config group belonging to the same service. The role type of the destination group must match the role type of the roles. &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void moveRolesTest() throws ApiException {
        String clusterName = null;
        String roleConfigGroupName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiRoleList response = api.moveRoles(clusterName, roleConfigGroupName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Moves roles to the base role config group.
     *
     * Moves roles to the base role config group.  The roles can be moved from any role config group belonging to the same service. The role type of the roles may vary. Each role will be moved to its corresponding base group depending on its role type. &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void moveRolesToBaseGroupTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleNameList body = null;
        ApiRoleList response = api.moveRolesToBaseGroup(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Returns the current revision of the config for the specified role config group.
     *
     * Returns the current revision of the config for the specified role config group. &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readConfigTest() throws ApiException {
        String clusterName = null;
        String roleConfigGroupName = null;
        String serviceName = null;
        String view = null;
        ApiConfigList response = api.readConfig(clusterName, roleConfigGroupName, serviceName, view);

        // TODO: test validations
    }
    
    /**
     * Returns the information for a role config group.
     *
     * Returns the information for a role config group. &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readRoleConfigGroupTest() throws ApiException {
        String clusterName = null;
        String roleConfigGroupName = null;
        String serviceName = null;
        ApiRoleConfigGroup response = api.readRoleConfigGroup(clusterName, roleConfigGroupName, serviceName);

        // TODO: test validations
    }
    
    /**
     * Returns the information for all role config groups for a given cluster and service.
     *
     * Returns the information for all role config groups for a given cluster and service. &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readRoleConfigGroupsTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiRoleConfigGroupList response = api.readRoleConfigGroups(clusterName, serviceName);

        // TODO: test validations
    }
    
    /**
     * Returns all roles in the given role config group.
     *
     * Returns all roles in the given role config group. &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readRolesTest() throws ApiException {
        String clusterName = null;
        String roleConfigGroupName = null;
        String serviceName = null;
        ApiRoleList response = api.readRoles(clusterName, roleConfigGroupName, serviceName);

        // TODO: test validations
    }
    
    /**
     * Updates the config for the given role config group.
     *
     * Updates the config for the given role config group.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateConfigTest() throws ApiException {
        String clusterName = null;
        String roleConfigGroupName = null;
        String serviceName = null;
        String message = null;
        ApiConfigList body = null;
        ApiConfigList response = api.updateConfig(clusterName, roleConfigGroupName, serviceName, message, body);

        // TODO: test validations
    }
    
    /**
     * Updates an existing role config group.
     *
     * Updates an existing role config group &lt;p&gt; Available since API v3.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateRoleConfigGroupTest() throws ApiException {
        String clusterName = null;
        String roleConfigGroupName = null;
        String serviceName = null;
        String message = null;
        ApiRoleConfigGroup body = null;
        ApiRoleConfigGroup response = api.updateRoleConfigGroup(clusterName, roleConfigGroupName, serviceName, message, body);

        // TODO: test validations
    }
    
}
