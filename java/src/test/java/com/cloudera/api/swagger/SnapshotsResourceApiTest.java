/*
 * Cloudera Manager API
 * <h1>Cloudera Manager API v31</h1>       <p>Introduced in Cloudera Manager 6.1.0</p>       <p><a href=\"http://www.cloudera.com/documentation.html\">Cloudera Product Documentation</a></p>
 *
 * OpenAPI spec version: 6.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudera.api.swagger;

import com.cloudera.api.swagger.client.ApiException;
import com.cloudera.api.swagger.model.ApiSnapshotCommandList;
import com.cloudera.api.swagger.model.ApiSnapshotPolicy;
import com.cloudera.api.swagger.model.ApiSnapshotPolicyList;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for SnapshotsResourceApi
 */
@Ignore
public class SnapshotsResourceApiTest {

    private final SnapshotsResourceApi api = new SnapshotsResourceApi();

    
    /**
     * Creates one or more snapshot policies.
     *
     * Creates one or more snapshot policies.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void createPoliciesTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        ApiSnapshotPolicyList body = null;
        ApiSnapshotPolicyList response = api.createPolicies(clusterName, serviceName, body);

        // TODO: test validations
    }
    
    /**
     * Deletes an existing snapshot policy.
     *
     * Deletes an existing snapshot policy.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deletePolicyTest() throws ApiException {
        String clusterName = null;
        String policyName = null;
        String serviceName = null;
        ApiSnapshotPolicy response = api.deletePolicy(clusterName, policyName, serviceName);

        // TODO: test validations
    }
    
    /**
     * Returns a list of commands triggered by a snapshot policy.
     *
     * Returns a list of commands triggered by a snapshot policy.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readHistoryTest() throws ApiException {
        String clusterName = null;
        String policyName = null;
        String serviceName = null;
        Integer limit = null;
        Integer offset = null;
        String view = null;
        ApiSnapshotCommandList response = api.readHistory(clusterName, policyName, serviceName, limit, offset, view);

        // TODO: test validations
    }
    
    /**
     * Returns information for all snapshot policies.
     *
     * Returns information for all snapshot policies.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readPoliciesTest() throws ApiException {
        String clusterName = null;
        String serviceName = null;
        String view = null;
        ApiSnapshotPolicyList response = api.readPolicies(clusterName, serviceName, view);

        // TODO: test validations
    }
    
    /**
     * Returns information for a specific snapshot policy.
     *
     * Returns information for a specific snapshot policy.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void readPolicyTest() throws ApiException {
        String clusterName = null;
        String policyName = null;
        String serviceName = null;
        String view = null;
        ApiSnapshotPolicy response = api.readPolicy(clusterName, policyName, serviceName, view);

        // TODO: test validations
    }
    
    /**
     * Updates an existing snapshot policy.
     *
     * Updates an existing snapshot policy.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updatePolicyTest() throws ApiException {
        String clusterName = null;
        String policyName = null;
        String serviceName = null;
        ApiSnapshotPolicy body = null;
        ApiSnapshotPolicy response = api.updatePolicy(clusterName, policyName, serviceName, body);

        // TODO: test validations
    }
    
}
