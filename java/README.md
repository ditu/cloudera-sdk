# cloudera-manager-api-swagger

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.cloudera.api.swagger</groupId>
    <artifactId>cloudera-manager-api-swagger</artifactId>
    <version>6.1.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.cloudera.api.swagger:cloudera-manager-api-swagger:6.1.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/cloudera-manager-api-swagger-6.1.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.cloudera.api.swagger.client.*;
import com.cloudera.api.swagger.client.auth.*;
import com.cloudera.api.swagger.model.*;
import com.cloudera.api.swagger.ActivitiesResourceApi;

import java.io.File;
import java.util.*;

public class ActivitiesResourceApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure HTTP basic authorization: basic
        HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
        basic.setUsername("YOUR USERNAME");
        basic.setPassword("YOUR PASSWORD");

        ActivitiesResourceApi apiInstance = new ActivitiesResourceApi();
        String activityId = "activityId_example"; // String | The name of the activity.
        String clusterName = "clusterName_example"; // String | The name of the cluster.
        String serviceName = "serviceName_example"; // String | The name of the service.
        String from = "from_example"; // String | Start of the period to query.
        List<String> metrics = Arrays.asList("metrics_example"); // List<String> | Filter for which metrics to query.
        String to = "now"; // String | End of the period to query.
        String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
        try {
            ApiMetricList result = apiInstance.getMetrics(activityId, clusterName, serviceName, from, metrics, to, view);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ActivitiesResourceApi#getMetrics");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost/api/v31*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ActivitiesResourceApi* | [**getMetrics**](docs/ActivitiesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/metrics | Fetch metric readings for a particular activity.
*ActivitiesResourceApi* | [**readActivities**](docs/ActivitiesResourceApi.md#readActivities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities | Read all activities in the system.
*ActivitiesResourceApi* | [**readActivity**](docs/ActivitiesResourceApi.md#readActivity) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId} | Returns a specific activity in the system.
*ActivitiesResourceApi* | [**readChildActivities**](docs/ActivitiesResourceApi.md#readChildActivities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/children | Returns the child activities.
*ActivitiesResourceApi* | [**readSimilarActivities**](docs/ActivitiesResourceApi.md#readSimilarActivities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/similar | Returns a list of similar activities.
*AllHostsResourceApi* | [**readConfig**](docs/AllHostsResourceApi.md#readConfig) | **GET** /cm/allHosts/config | Retrieve the default configuration for all hosts.
*AllHostsResourceApi* | [**updateConfig**](docs/AllHostsResourceApi.md#updateConfig) | **PUT** /cm/allHosts/config | Update the default configuration values for all hosts.
*AuditsResourceApi* | [**readAudits**](docs/AuditsResourceApi.md#readAudits) | **GET** /audits | Fetch audit events from Cloudera Manager (CM) and CM managed services like HDFS, HBase, Impala, Hive, and Sentry.
*AuditsResourceApi* | [**streamAudits**](docs/AuditsResourceApi.md#streamAudits) | **GET** /audits/stream | 
*AuthRoleMetadatasResourceApi* | [**readAuthRolesMetadata**](docs/AuthRoleMetadatasResourceApi.md#readAuthRolesMetadata) | **GET** /authRoleMetadatas | Returns a list of the auth roles&#39; metadata for the built-in roles.
*AuthRolesResourceApi* | [**createAuthRoles**](docs/AuthRolesResourceApi.md#createAuthRoles) | **POST** /authRoles | Creates a list of auth roles.
*AuthRolesResourceApi* | [**deleteAuthRole**](docs/AuthRolesResourceApi.md#deleteAuthRole) | **DELETE** /authRoles/{uuid} | Deletes an auth role from the system.
*AuthRolesResourceApi* | [**readAuthRole**](docs/AuthRolesResourceApi.md#readAuthRole) | **GET** /authRoles/{uuid} | Returns detailed information about an auth role.
*AuthRolesResourceApi* | [**readAuthRoles**](docs/AuthRolesResourceApi.md#readAuthRoles) | **GET** /authRoles | Returns a list of the auth roles configured in the system.
*AuthRolesResourceApi* | [**readAuthRolesMetadata**](docs/AuthRolesResourceApi.md#readAuthRolesMetadata) | **GET** /authRoles/metadata | Returns a list of the auth roles&#39; metadata for the built-in roles.
*AuthRolesResourceApi* | [**updateAuthRole**](docs/AuthRolesResourceApi.md#updateAuthRole) | **PUT** /authRoles/{uuid} | Updates the given auth role&#39;s information.
*AuthServiceResourceApi* | [**autoAssignRoles**](docs/AuthServiceResourceApi.md#autoAssignRoles) | **PUT** /cm/authService/autoAssignRoles | Automatically assign roles to hosts and create the roles for the Authentication Service.
*AuthServiceResourceApi* | [**autoConfigure**](docs/AuthServiceResourceApi.md#autoConfigure) | **PUT** /cm/authService/autoConfigure | Automatically configures roles of the Authentication Service.
*AuthServiceResourceApi* | [**delete**](docs/AuthServiceResourceApi.md#delete) | **DELETE** /cm/authService | Delete the Authentication Service.
*AuthServiceResourceApi* | [**enterMaintenanceMode**](docs/AuthServiceResourceApi.md#enterMaintenanceMode) | **POST** /cm/authService/commands/enterMaintenanceMode | Put the Authentication Service into maintenance mode.
*AuthServiceResourceApi* | [**exitMaintenanceMode**](docs/AuthServiceResourceApi.md#exitMaintenanceMode) | **POST** /cm/authService/commands/exitMaintenanceMode | Take the Authentication Service out of maintenance mode.
*AuthServiceResourceApi* | [**listActiveCommands**](docs/AuthServiceResourceApi.md#listActiveCommands) | **GET** /cm/authService/commands | List active Authentication Service commands.
*AuthServiceResourceApi* | [**listRoleTypes**](docs/AuthServiceResourceApi.md#listRoleTypes) | **GET** /cm/authService/roleTypes | List the supported role types for the Authentication Service.
*AuthServiceResourceApi* | [**readService**](docs/AuthServiceResourceApi.md#readService) | **GET** /cm/authService | Retrieve information about the Authentication Services.
*AuthServiceResourceApi* | [**readServiceConfig**](docs/AuthServiceResourceApi.md#readServiceConfig) | **GET** /cm/authService/config | 
*AuthServiceResourceApi* | [**restartCommand**](docs/AuthServiceResourceApi.md#restartCommand) | **POST** /cm/authService/commands/restart | Restart the Authentication Service.
*AuthServiceResourceApi* | [**setup**](docs/AuthServiceResourceApi.md#setup) | **PUT** /cm/authService | Setup the Authentication Service.
*AuthServiceResourceApi* | [**startCommand**](docs/AuthServiceResourceApi.md#startCommand) | **POST** /cm/authService/commands/start | Start the Authentication Service.
*AuthServiceResourceApi* | [**stopCommand**](docs/AuthServiceResourceApi.md#stopCommand) | **POST** /cm/authService/commands/stop | Stop the Authentication Service.
*AuthServiceResourceApi* | [**updateServiceConfig**](docs/AuthServiceResourceApi.md#updateServiceConfig) | **PUT** /cm/authService/config | 
*AuthServiceRoleCommandsResourceApi* | [**restartCommand**](docs/AuthServiceRoleCommandsResourceApi.md#restartCommand) | **POST** /cm/authService/roleCommands/restart | Restart a set of Authentication Service roles.
*AuthServiceRoleCommandsResourceApi* | [**startCommand**](docs/AuthServiceRoleCommandsResourceApi.md#startCommand) | **POST** /cm/authService/roleCommands/start | Start a set of Authentication Service roles.
*AuthServiceRoleCommandsResourceApi* | [**stopCommand**](docs/AuthServiceRoleCommandsResourceApi.md#stopCommand) | **POST** /cm/authService/roleCommands/stop | Stop a set of Authentication Service roles.
*AuthServiceRoleConfigGroupsResourceApi* | [**readConfig**](docs/AuthServiceRoleConfigGroupsResourceApi.md#readConfig) | **GET** /cm/authService/roleConfigGroups/{roleConfigGroupName}/config | Returns the current revision of the config for the specified role config group in the Authentication Service.
*AuthServiceRoleConfigGroupsResourceApi* | [**readRoleConfigGroup**](docs/AuthServiceRoleConfigGroupsResourceApi.md#readRoleConfigGroup) | **GET** /cm/authService/roleConfigGroups/{roleConfigGroupName} | Returns the information for a given role config group in the Authentication Service.
*AuthServiceRoleConfigGroupsResourceApi* | [**readRoleConfigGroups**](docs/AuthServiceRoleConfigGroupsResourceApi.md#readRoleConfigGroups) | **GET** /cm/authService/roleConfigGroups | Returns the information for all role config groups in the Authentication Service.
*AuthServiceRoleConfigGroupsResourceApi* | [**readRoles**](docs/AuthServiceRoleConfigGroupsResourceApi.md#readRoles) | **GET** /cm/authService/roleConfigGroups/{roleConfigGroupName}/roles | Returns all roles in the given role config group in the Authentication Service.
*AuthServiceRoleConfigGroupsResourceApi* | [**updateConfig**](docs/AuthServiceRoleConfigGroupsResourceApi.md#updateConfig) | **PUT** /cm/authService/roleConfigGroups/{roleConfigGroupName}/config | Updates the config for the given role config group in the Authentication Service.
*AuthServiceRoleConfigGroupsResourceApi* | [**updateRoleConfigGroup**](docs/AuthServiceRoleConfigGroupsResourceApi.md#updateRoleConfigGroup) | **PUT** /cm/authService/roleConfigGroups/{roleConfigGroupName} | Updates an existing role config group in the Authentication Service.
*AuthServiceRolesResourceApi* | [**createRoles**](docs/AuthServiceRolesResourceApi.md#createRoles) | **POST** /cm/authService/roles | Create new roles in the Authentication Services.
*AuthServiceRolesResourceApi* | [**deleteRole**](docs/AuthServiceRolesResourceApi.md#deleteRole) | **DELETE** /cm/authService/roles/{roleName} | Delete a role from the Authentication Services.
*AuthServiceRolesResourceApi* | [**enterMaintenanceMode**](docs/AuthServiceRolesResourceApi.md#enterMaintenanceMode) | **POST** /cm/authService/roles/{roleName}/commands/enterMaintenanceMode | Put the Authentication Service role into maintenance mode.
*AuthServiceRolesResourceApi* | [**exitMaintenanceMode**](docs/AuthServiceRolesResourceApi.md#exitMaintenanceMode) | **POST** /cm/authService/roles/{roleName}/commands/exitMaintenanceMode | Take the Authentication Service role out of maintenance mode.
*AuthServiceRolesResourceApi* | [**getFullLog**](docs/AuthServiceRolesResourceApi.md#getFullLog) | **GET** /cm/authService/roles/{roleName}/logs/full | Retrieves the log file for the role&#39;s main process.
*AuthServiceRolesResourceApi* | [**getStacksLog**](docs/AuthServiceRolesResourceApi.md#getStacksLog) | **GET** /cm/authService/roles/{roleName}/logs/stacks | Retrieves the stacks log file, if any, for the role&#39;s main process.
*AuthServiceRolesResourceApi* | [**getStacksLogsBundle**](docs/AuthServiceRolesResourceApi.md#getStacksLogsBundle) | **GET** /cm/authService/roles/{roleName}/logs/stacksBundle | Download a zip-compressed archive of role stacks logs.
*AuthServiceRolesResourceApi* | [**getStandardError**](docs/AuthServiceRolesResourceApi.md#getStandardError) | **GET** /cm/authService/roles/{roleName}/logs/stderr | Retrieves the role&#39;s standard error output.
*AuthServiceRolesResourceApi* | [**getStandardOutput**](docs/AuthServiceRolesResourceApi.md#getStandardOutput) | **GET** /cm/authService/roles/{roleName}/logs/stdout | Retrieves the role&#39;s standard output.
*AuthServiceRolesResourceApi* | [**listActiveCommands**](docs/AuthServiceRolesResourceApi.md#listActiveCommands) | **GET** /cm/authService/roles/{roleName}/commands | List active role commands.
*AuthServiceRolesResourceApi* | [**readRole**](docs/AuthServiceRolesResourceApi.md#readRole) | **GET** /cm/authService/roles/{roleName} | Retrieve detailed information about a Authentication Services role.
*AuthServiceRolesResourceApi* | [**readRoleConfig**](docs/AuthServiceRolesResourceApi.md#readRoleConfig) | **GET** /cm/authService/roles/{roleName}/config | Retrieve the configuration of a specific Authentication Services role.
*AuthServiceRolesResourceApi* | [**readRoles**](docs/AuthServiceRolesResourceApi.md#readRoles) | **GET** /cm/authService/roles | List all roles of the Authentication Services.
*AuthServiceRolesResourceApi* | [**updateRoleConfig**](docs/AuthServiceRolesResourceApi.md#updateRoleConfig) | **PUT** /cm/authService/roles/{roleName}/config | Update the configuration of a Authentication Services role.
*BatchResourceApi* | [**execute**](docs/BatchResourceApi.md#execute) | **POST** /batch | Executes a batch of API requests in one database transaction.
*ClouderaManagerResourceApi* | [**beginTrial**](docs/ClouderaManagerResourceApi.md#beginTrial) | **POST** /cm/trial/begin | Begin trial license.
*ClouderaManagerResourceApi* | [**clustersPerfInspectorCommand**](docs/ClouderaManagerResourceApi.md#clustersPerfInspectorCommand) | **POST** /cm/commands/clustersPerfInspector | Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
*ClouderaManagerResourceApi* | [**collectDiagnosticDataCommand**](docs/ClouderaManagerResourceApi.md#collectDiagnosticDataCommand) | **POST** /cm/commands/collectDiagnosticData | Collect diagnostic data from hosts managed by Cloudera Manager.
*ClouderaManagerResourceApi* | [**deleteCredentialsCommand**](docs/ClouderaManagerResourceApi.md#deleteCredentialsCommand) | **POST** /cm/commands/deleteCredentials | Delete existing Kerberos credentials.
*ClouderaManagerResourceApi* | [**endTrial**](docs/ClouderaManagerResourceApi.md#endTrial) | **POST** /cm/trial/end | End trial license.
*ClouderaManagerResourceApi* | [**generateCredentialsCommand**](docs/ClouderaManagerResourceApi.md#generateCredentialsCommand) | **POST** /cm/commands/generateCredentials | Generate missing Kerberos credentials.
*ClouderaManagerResourceApi* | [**getConfig**](docs/ClouderaManagerResourceApi.md#getConfig) | **GET** /cm/config | Retrieve the Cloudera Manager settings.
*ClouderaManagerResourceApi* | [**getDeployment2**](docs/ClouderaManagerResourceApi.md#getDeployment2) | **GET** /cm/deployment | Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc.
*ClouderaManagerResourceApi* | [**getKerberosInfo**](docs/ClouderaManagerResourceApi.md#getKerberosInfo) | **GET** /cm/kerberosInfo | Provides Cloudera Manager Kerberos information.
*ClouderaManagerResourceApi* | [**getKerberosPrincipals**](docs/ClouderaManagerResourceApi.md#getKerberosPrincipals) | **GET** /cm/kerberosPrincipals | Returns the Kerberos principals needed by the services being managed by Cloudera Manager.
*ClouderaManagerResourceApi* | [**getLicensedFeatureUsage**](docs/ClouderaManagerResourceApi.md#getLicensedFeatureUsage) | **GET** /cm/licensedFeatureUsage | Retrieve a summary of licensed feature usage.
*ClouderaManagerResourceApi* | [**getLog**](docs/ClouderaManagerResourceApi.md#getLog) | **GET** /cm/log | Returns the entire contents of the Cloudera Manager log file.
*ClouderaManagerResourceApi* | [**getScmDbInfo**](docs/ClouderaManagerResourceApi.md#getScmDbInfo) | **GET** /cm/scmDbInfo | Provides Cloudera Manager server&#39;s database information.
*ClouderaManagerResourceApi* | [**getShutdownReadiness**](docs/ClouderaManagerResourceApi.md#getShutdownReadiness) | **GET** /cm/shutdownReadiness | Retrieve Cloudera Manager&#39;s readiness for shutdown and destroy.
*ClouderaManagerResourceApi* | [**getVersion**](docs/ClouderaManagerResourceApi.md#getVersion) | **GET** /cm/version | Provides version information of Cloudera Manager itself.
*ClouderaManagerResourceApi* | [**hostInstallCommand**](docs/ClouderaManagerResourceApi.md#hostInstallCommand) | **POST** /cm/commands/hostInstall | Perform installation on a set of hosts.
*ClouderaManagerResourceApi* | [**hostsDecommissionCommand**](docs/ClouderaManagerResourceApi.md#hostsDecommissionCommand) | **POST** /cm/commands/hostsDecommission | Decommission the given hosts.
*ClouderaManagerResourceApi* | [**hostsOfflineOrDecommissionCommand**](docs/ClouderaManagerResourceApi.md#hostsOfflineOrDecommissionCommand) | **POST** /cm/commands/hostsOfflineOrDecommission | Decommission the given hosts.
*ClouderaManagerResourceApi* | [**hostsPerfInspectorCommand**](docs/ClouderaManagerResourceApi.md#hostsPerfInspectorCommand) | **POST** /cm/commands/hostsPerfInspector | Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
*ClouderaManagerResourceApi* | [**hostsRecommissionAndExitMaintenanceModeCommand**](docs/ClouderaManagerResourceApi.md#hostsRecommissionAndExitMaintenanceModeCommand) | **POST** /cm/commands/hostsRecommissionAndExitMaintenanceMode | Recommission and exit maintenance on the given hosts.
*ClouderaManagerResourceApi* | [**hostsRecommissionCommand**](docs/ClouderaManagerResourceApi.md#hostsRecommissionCommand) | **POST** /cm/commands/hostsRecommission | Recommission the given hosts.
*ClouderaManagerResourceApi* | [**hostsRecommissionWithStartCommand**](docs/ClouderaManagerResourceApi.md#hostsRecommissionWithStartCommand) | **POST** /cm/commands/hostsRecommissionWithStart | Recommission the given hosts.
*ClouderaManagerResourceApi* | [**hostsStartRolesCommand**](docs/ClouderaManagerResourceApi.md#hostsStartRolesCommand) | **POST** /cm/commands/hostsStartRoles | Start all the roles on the given hosts.
*ClouderaManagerResourceApi* | [**importAdminCredentials**](docs/ClouderaManagerResourceApi.md#importAdminCredentials) | **POST** /cm/commands/importAdminCredentials | Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.
*ClouderaManagerResourceApi* | [**importClusterTemplate**](docs/ClouderaManagerResourceApi.md#importClusterTemplate) | **POST** /cm/importClusterTemplate | Create cluster as per the given cluster template.
*ClouderaManagerResourceApi* | [**importKerberosPrincipal**](docs/ClouderaManagerResourceApi.md#importKerberosPrincipal) | **POST** /cm/commands/importKerberosPrincipal | Imports the Kerberos credentials for the specified principal which can then be used to add to a role&#39;s keytab by running Generate Credentials command.
*ClouderaManagerResourceApi* | [**inspectHostsCommand**](docs/ClouderaManagerResourceApi.md#inspectHostsCommand) | **POST** /cm/commands/inspectHosts | Runs the host inspector on the configured hosts.
*ClouderaManagerResourceApi* | [**listActiveCommands**](docs/ClouderaManagerResourceApi.md#listActiveCommands) | **GET** /cm/commands | List active global commands.
*ClouderaManagerResourceApi* | [**readLicense**](docs/ClouderaManagerResourceApi.md#readLicense) | **GET** /cm/license | Retrieve information about the Cloudera Manager license.
*ClouderaManagerResourceApi* | [**refreshParcelRepos**](docs/ClouderaManagerResourceApi.md#refreshParcelRepos) | **POST** /cm/commands/refreshParcelRepos | .
*ClouderaManagerResourceApi* | [**updateConfig**](docs/ClouderaManagerResourceApi.md#updateConfig) | **PUT** /cm/config | Update the Cloudera Manager settings.
*ClouderaManagerResourceApi* | [**updateDeployment2**](docs/ClouderaManagerResourceApi.md#updateDeployment2) | **PUT** /cm/deployment | Apply the supplied deployment description to the system.
*ClouderaManagerResourceApi* | [**updateLicense**](docs/ClouderaManagerResourceApi.md#updateLicense) | **POST** /cm/license | Updates the Cloudera Manager license.
*ClustersResourceApi* | [**addHosts**](docs/ClustersResourceApi.md#addHosts) | **POST** /clusters/{clusterName}/hosts | 
*ClustersResourceApi* | [**autoAssignRoles**](docs/ClustersResourceApi.md#autoAssignRoles) | **PUT** /clusters/{clusterName}/autoAssignRoles | Automatically assign roles to hosts and create the roles for all the services in a cluster.
*ClustersResourceApi* | [**autoConfigure**](docs/ClustersResourceApi.md#autoConfigure) | **PUT** /clusters/{clusterName}/autoConfigure | Automatically configures roles and services in a cluster.
*ClustersResourceApi* | [**configureForKerberos**](docs/ClustersResourceApi.md#configureForKerberos) | **POST** /clusters/{clusterName}/commands/configureForKerberos | Command to configure the cluster to use Kerberos for authentication.
*ClustersResourceApi* | [**createClusters**](docs/ClustersResourceApi.md#createClusters) | **POST** /clusters | Creates a collection of clusters.
*ClustersResourceApi* | [**deleteCluster**](docs/ClustersResourceApi.md#deleteCluster) | **DELETE** /clusters/{clusterName} | Deletes a cluster.
*ClustersResourceApi* | [**deleteClusterCredentialsCommand**](docs/ClustersResourceApi.md#deleteClusterCredentialsCommand) | **POST** /clusters/{clusterName}/commands/deleteCredentials | Delete existing Kerberos credentials for the cluster.
*ClustersResourceApi* | [**deployClientConfig**](docs/ClustersResourceApi.md#deployClientConfig) | **POST** /clusters/{clusterName}/commands/deployClientConfig | Deploy the cluster-wide client configuration.
*ClustersResourceApi* | [**deployClusterClientConfig**](docs/ClustersResourceApi.md#deployClusterClientConfig) | **POST** /clusters/{clusterName}/commands/deployClusterClientConfig | Deploy the Cluster&#39;s Kerberos client configuration.
*ClustersResourceApi* | [**enterMaintenanceMode**](docs/ClustersResourceApi.md#enterMaintenanceMode) | **POST** /clusters/{clusterName}/commands/enterMaintenanceMode | Put the cluster into maintenance mode.
*ClustersResourceApi* | [**exitMaintenanceMode**](docs/ClustersResourceApi.md#exitMaintenanceMode) | **POST** /clusters/{clusterName}/commands/exitMaintenanceMode | Take the cluster out of maintenance mode.
*ClustersResourceApi* | [**expireLogs**](docs/ClustersResourceApi.md#expireLogs) | **POST** /clusters/{clusterName}/commands/expireLogs | Remove backup and disaster related log files in hdfs.
*ClustersResourceApi* | [**export**](docs/ClustersResourceApi.md#export) | **GET** /clusters/{clusterName}/export | Export the cluster template for the given cluster.
*ClustersResourceApi* | [**firstRun**](docs/ClustersResourceApi.md#firstRun) | **POST** /clusters/{clusterName}/commands/firstRun | Prepare and start services in a cluster.
*ClustersResourceApi* | [**getClientConfig**](docs/ClustersResourceApi.md#getClientConfig) | **GET** /clusters/{clusterName}/clientConfig | Download a zip-compressed archive of the client configuration, of a specific cluster.
*ClustersResourceApi* | [**getKerberosInfo**](docs/ClustersResourceApi.md#getKerberosInfo) | **GET** /clusters/{clusterName}/kerberosInfo | Provides Cluster Kerberos information.
*ClustersResourceApi* | [**getUtilizationReport**](docs/ClustersResourceApi.md#getUtilizationReport) | **GET** /clusters/{clusterName}/utilization | Provides the resource utilization of the entire cluster as well as the resource utilization per tenant.
*ClustersResourceApi* | [**inspectHostsCommand**](docs/ClustersResourceApi.md#inspectHostsCommand) | **POST** /clusters/{clusterName}/commands/inspectHosts | Runs the host inspector on the configured hosts in the specified cluster.
*ClustersResourceApi* | [**listActiveCommands**](docs/ClustersResourceApi.md#listActiveCommands) | **GET** /clusters/{clusterName}/commands | List active cluster commands.
*ClustersResourceApi* | [**listDfsServices**](docs/ClustersResourceApi.md#listDfsServices) | **GET** /clusters/{clusterName}/dfsServices | List the services that can provide distributed file system (DFS) capabilities in this cluster.
*ClustersResourceApi* | [**listHosts**](docs/ClustersResourceApi.md#listHosts) | **GET** /clusters/{clusterName}/hosts | 
*ClustersResourceApi* | [**listServiceTypes**](docs/ClustersResourceApi.md#listServiceTypes) | **GET** /clusters/{clusterName}/serviceTypes | List the supported service types for a cluster.
*ClustersResourceApi* | [**perfInspectorCommand**](docs/ClustersResourceApi.md#perfInspectorCommand) | **POST** /clusters/{clusterName}/commands/perfInspector | Run cluster performance diagnostics test.
*ClustersResourceApi* | [**poolsRefresh**](docs/ClustersResourceApi.md#poolsRefresh) | **POST** /clusters/{clusterName}/commands/poolsRefresh | Updates all refreshable configuration files for services with Dynamic Resource Pools.
*ClustersResourceApi* | [**preUpgradeCheckCommand**](docs/ClustersResourceApi.md#preUpgradeCheckCommand) | **POST** /clusters/{clusterName}/commands/preUpgradeCheck | Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other.
*ClustersResourceApi* | [**readCluster**](docs/ClustersResourceApi.md#readCluster) | **GET** /clusters/{clusterName} | Reads information about a cluster.
*ClustersResourceApi* | [**readClusters**](docs/ClustersResourceApi.md#readClusters) | **GET** /clusters | Lists all known clusters.
*ClustersResourceApi* | [**refresh**](docs/ClustersResourceApi.md#refresh) | **POST** /clusters/{clusterName}/commands/refresh | Updates all refreshable configuration files in the cluster.
*ClustersResourceApi* | [**removeAllHosts**](docs/ClustersResourceApi.md#removeAllHosts) | **DELETE** /clusters/{clusterName}/hosts | 
*ClustersResourceApi* | [**removeHost**](docs/ClustersResourceApi.md#removeHost) | **DELETE** /clusters/{clusterName}/hosts/{hostId} | 
*ClustersResourceApi* | [**restartCommand**](docs/ClustersResourceApi.md#restartCommand) | **POST** /clusters/{clusterName}/commands/restart | Restart all services in the cluster.
*ClustersResourceApi* | [**rollingRestart**](docs/ClustersResourceApi.md#rollingRestart) | **POST** /clusters/{clusterName}/commands/rollingRestart | Command to do a \&quot;best-effort\&quot; rolling restart of the given cluster, i.
*ClustersResourceApi* | [**rollingUpgrade**](docs/ClustersResourceApi.md#rollingUpgrade) | **POST** /clusters/{clusterName}/commands/rollingUpgrade | Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don&#39;t support rolling upgrades.
*ClustersResourceApi* | [**startCommand**](docs/ClustersResourceApi.md#startCommand) | **POST** /clusters/{clusterName}/commands/start | Start all services in the cluster.
*ClustersResourceApi* | [**stopCommand**](docs/ClustersResourceApi.md#stopCommand) | **POST** /clusters/{clusterName}/commands/stop | Stop all services in the cluster.
*ClustersResourceApi* | [**updateCluster**](docs/ClustersResourceApi.md#updateCluster) | **PUT** /clusters/{clusterName} | Update an existing cluster.
*ClustersResourceApi* | [**upgradeCdhCommand**](docs/ClustersResourceApi.md#upgradeCdhCommand) | **POST** /clusters/{clusterName}/commands/upgradeCdh | Perform CDH upgrade to the specified version.
*ClustersResourceApi* | [**upgradeServicesCommand**](docs/ClustersResourceApi.md#upgradeServicesCommand) | **POST** /clusters/{clusterName}/commands/upgradeServices | Upgrades the services in the cluster to the CDH5 version.
*CmPeersResourceApi* | [**createPeer**](docs/CmPeersResourceApi.md#createPeer) | **POST** /cm/peers | Create a new Cloudera Manager peer.
*CmPeersResourceApi* | [**deletePeer**](docs/CmPeersResourceApi.md#deletePeer) | **DELETE** /cm/peers/{peerName} | Delete Cloudera Manager peer.
*CmPeersResourceApi* | [**listPeers**](docs/CmPeersResourceApi.md#listPeers) | **GET** /cm/peers | Retrieves all configured Cloudera Manager peers.
*CmPeersResourceApi* | [**readPeer**](docs/CmPeersResourceApi.md#readPeer) | **GET** /cm/peers/{peerName} | Fetch information about an existing Cloudera Manager peer.
*CmPeersResourceApi* | [**testPeer**](docs/CmPeersResourceApi.md#testPeer) | **POST** /cm/peers/{peerName}/commands/test | Test the connectivity of a peer.
*CmPeersResourceApi* | [**updatePeer**](docs/CmPeersResourceApi.md#updatePeer) | **PUT** /cm/peers/{peerName} | Update information for a Cloudera Manager peer.
*CommandsResourceApi* | [**abortCommand**](docs/CommandsResourceApi.md#abortCommand) | **POST** /commands/{commandId}/abort | Abort a running command.
*CommandsResourceApi* | [**readCommand**](docs/CommandsResourceApi.md#readCommand) | **GET** /commands/{commandId} | Retrieve detailed information on an asynchronous command.
*CommandsResourceApi* | [**retry**](docs/CommandsResourceApi.md#retry) | **POST** /commands/{commandId}/retry | Try to rerun a command.
*DashboardsResourceApi* | [**createDashboards**](docs/DashboardsResourceApi.md#createDashboards) | **POST** /timeseries/dashboards | Creates the list of dashboards.
*DashboardsResourceApi* | [**deleteDashboard**](docs/DashboardsResourceApi.md#deleteDashboard) | **DELETE** /timeseries/dashboards/{dashboardName} | Deletes a dashboard.
*DashboardsResourceApi* | [**getDashboard**](docs/DashboardsResourceApi.md#getDashboard) | **GET** /timeseries/dashboards/{dashboardName} | Returns a dashboard definition for the specified name.
*DashboardsResourceApi* | [**getDashboards**](docs/DashboardsResourceApi.md#getDashboards) | **GET** /timeseries/dashboards | Returns the list of all user-customized dashboards.
*EventsResourceApi* | [**readEvent**](docs/EventsResourceApi.md#readEvent) | **GET** /events/{eventId} | Returns a specific event in the system.
*EventsResourceApi* | [**readEvents**](docs/EventsResourceApi.md#readEvents) | **GET** /events | Allows you to query events in the system.
*ExternalAccountsResourceApi* | [**createAccount**](docs/ExternalAccountsResourceApi.md#createAccount) | **POST** /externalAccounts/create | Create a new external account.
*ExternalAccountsResourceApi* | [**deleteAccount**](docs/ExternalAccountsResourceApi.md#deleteAccount) | **DELETE** /externalAccounts/delete/{name} | Delete an external account, specifying its name.
*ExternalAccountsResourceApi* | [**externalAccountCommandByName**](docs/ExternalAccountsResourceApi.md#externalAccountCommandByName) | **POST** /externalAccounts/account/{name}/commands/{commandName} | Executes a command on the external account specified by name.
*ExternalAccountsResourceApi* | [**getSupportedCategories**](docs/ExternalAccountsResourceApi.md#getSupportedCategories) | **GET** /externalAccounts/supportedCategories | List of external account categories supported by this Cloudera Manager.
*ExternalAccountsResourceApi* | [**getSupportedTypes**](docs/ExternalAccountsResourceApi.md#getSupportedTypes) | **GET** /externalAccounts/supportedTypes/{categoryName} | List of external account types supported by this Cloudera Manager by category.
*ExternalAccountsResourceApi* | [**listExternalAccountCommands**](docs/ExternalAccountsResourceApi.md#listExternalAccountCommands) | **GET** /externalAccounts/typeInfo/{typeName}/commandsByName | Lists all the commands that can be executed by name on the provided external account type.
*ExternalAccountsResourceApi* | [**readAccount**](docs/ExternalAccountsResourceApi.md#readAccount) | **GET** /externalAccounts/account/{name} | Get a single external account by account name.
*ExternalAccountsResourceApi* | [**readAccountByDisplayName**](docs/ExternalAccountsResourceApi.md#readAccountByDisplayName) | **GET** /externalAccounts/accountByDisplayName/{displayName} | Get a single external account by display name.
*ExternalAccountsResourceApi* | [**readAccounts**](docs/ExternalAccountsResourceApi.md#readAccounts) | **GET** /externalAccounts/type/{typeName} | Get a list of external accounts for a specific account type.
*ExternalAccountsResourceApi* | [**readConfig**](docs/ExternalAccountsResourceApi.md#readConfig) | **GET** /externalAccounts/account/{name}/config | Get configs of external account for the given account name.
*ExternalAccountsResourceApi* | [**updateAccount**](docs/ExternalAccountsResourceApi.md#updateAccount) | **PUT** /externalAccounts/update | Update an external account.
*ExternalAccountsResourceApi* | [**updateConfig**](docs/ExternalAccountsResourceApi.md#updateConfig) | **PUT** /externalAccounts/account/{name}/config | Upadate configs of external account for the given account name.
*ExternalUserMappingsResourceApi* | [**createExternalUserMappings**](docs/ExternalUserMappingsResourceApi.md#createExternalUserMappings) | **POST** /externalUserMappings | Creates a list of external user mappings.
*ExternalUserMappingsResourceApi* | [**deleteExternalUserMapping**](docs/ExternalUserMappingsResourceApi.md#deleteExternalUserMapping) | **DELETE** /externalUserMappings/{uuid} | Deletes an external user mapping from the system.
*ExternalUserMappingsResourceApi* | [**readExternalUserMapping**](docs/ExternalUserMappingsResourceApi.md#readExternalUserMapping) | **GET** /externalUserMappings/{uuid} | Returns detailed information about an external user mapping.
*ExternalUserMappingsResourceApi* | [**readExternalUserMappings**](docs/ExternalUserMappingsResourceApi.md#readExternalUserMappings) | **GET** /externalUserMappings | Returns a list of the external user mappings configured in the system.
*ExternalUserMappingsResourceApi* | [**updateExternalUserMapping**](docs/ExternalUserMappingsResourceApi.md#updateExternalUserMapping) | **PUT** /externalUserMappings/{uuid} | Updates the given external user mapping&#39;s information.
*HostTemplatesResourceApi* | [**applyHostTemplate**](docs/HostTemplatesResourceApi.md#applyHostTemplate) | **POST** /clusters/{clusterName}/hostTemplates/{hostTemplateName}/commands/applyHostTemplate | Applies a host template to a collection of hosts.
*HostTemplatesResourceApi* | [**createHostTemplates**](docs/HostTemplatesResourceApi.md#createHostTemplates) | **POST** /clusters/{clusterName}/hostTemplates | Creates new host templates.
*HostTemplatesResourceApi* | [**deleteHostTemplate**](docs/HostTemplatesResourceApi.md#deleteHostTemplate) | **DELETE** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Deletes a host template.
*HostTemplatesResourceApi* | [**readHostTemplate**](docs/HostTemplatesResourceApi.md#readHostTemplate) | **GET** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Retrieves information about a host template.
*HostTemplatesResourceApi* | [**readHostTemplates**](docs/HostTemplatesResourceApi.md#readHostTemplates) | **GET** /clusters/{clusterName}/hostTemplates | Lists all host templates in a cluster.
*HostTemplatesResourceApi* | [**updateHostTemplate**](docs/HostTemplatesResourceApi.md#updateHostTemplate) | **PUT** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Updates an existing host template.
*HostsResourceApi* | [**createHosts**](docs/HostsResourceApi.md#createHosts) | **POST** /hosts | .
*HostsResourceApi* | [**deleteAllHosts**](docs/HostsResourceApi.md#deleteAllHosts) | **DELETE** /hosts | Delete all hosts in the system.
*HostsResourceApi* | [**deleteHost**](docs/HostsResourceApi.md#deleteHost) | **DELETE** /hosts/{hostId} | Delete a host from the system.
*HostsResourceApi* | [**enterMaintenanceMode**](docs/HostsResourceApi.md#enterMaintenanceMode) | **POST** /hosts/{hostId}/commands/enterMaintenanceMode | Put the host into maintenance mode.
*HostsResourceApi* | [**exitMaintenanceMode**](docs/HostsResourceApi.md#exitMaintenanceMode) | **POST** /hosts/{hostId}/commands/exitMaintenanceMode | Take the host out of maintenance mode.
*HostsResourceApi* | [**generateHostCerts**](docs/HostsResourceApi.md#generateHostCerts) | **POST** /hosts/{hostId}/commands/generateHostCerts | Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.
*HostsResourceApi* | [**getMetrics**](docs/HostsResourceApi.md#getMetrics) | **GET** /hosts/{hostId}/metrics | Fetch metric readings for a host.
*HostsResourceApi* | [**migrateRoles**](docs/HostsResourceApi.md#migrateRoles) | **POST** /hosts/{hostId}/commands/migrateRoles | Migrate roles to a different host.
*HostsResourceApi* | [**readHost**](docs/HostsResourceApi.md#readHost) | **GET** /hosts/{hostId} | Returns a specific Host in the system.
*HostsResourceApi* | [**readHostConfig**](docs/HostsResourceApi.md#readHostConfig) | **GET** /hosts/{hostId}/config | Retrieves the configuration of a specific host.
*HostsResourceApi* | [**readHosts**](docs/HostsResourceApi.md#readHosts) | **GET** /hosts | Returns the hostIds for all hosts in the system.
*HostsResourceApi* | [**updateHost**](docs/HostsResourceApi.md#updateHost) | **PUT** /hosts/{hostId} | .
*HostsResourceApi* | [**updateHostConfig**](docs/HostsResourceApi.md#updateHostConfig) | **PUT** /hosts/{hostId}/config | Updates the host configuration with the given values.
*ImpalaQueriesResourceApi* | [**cancelImpalaQuery**](docs/ImpalaQueriesResourceApi.md#cancelImpalaQuery) | **POST** /clusters/{clusterName}/services/{serviceName}/impalaQueries/{queryId}/cancel | Cancels an Impala Query.
*ImpalaQueriesResourceApi* | [**getImpalaQueries**](docs/ImpalaQueriesResourceApi.md#getImpalaQueries) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries | Returns a list of queries that satisfy the filter.
*ImpalaQueriesResourceApi* | [**getImpalaQueryAttributes**](docs/ImpalaQueriesResourceApi.md#getImpalaQueryAttributes) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries/attributes | Returns the list of all attributes that the Service Monitor can associate with Impala queries.
*ImpalaQueriesResourceApi* | [**getQueryDetails**](docs/ImpalaQueriesResourceApi.md#getQueryDetails) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries/{queryId} | Returns details about the query.
*MgmtRoleCommandsResourceApi* | [**jmapDump**](docs/MgmtRoleCommandsResourceApi.md#jmapDump) | **POST** /cm/service/roleCommands/jmapDump | Run the jmapDump diagnostic command.
*MgmtRoleCommandsResourceApi* | [**jmapHisto**](docs/MgmtRoleCommandsResourceApi.md#jmapHisto) | **POST** /cm/service/roleCommands/jmapHisto | Run the jmapHisto diagnostic command.
*MgmtRoleCommandsResourceApi* | [**jstack**](docs/MgmtRoleCommandsResourceApi.md#jstack) | **POST** /cm/service/roleCommands/jstack | Run the jstack diagnostic command.
*MgmtRoleCommandsResourceApi* | [**lsof**](docs/MgmtRoleCommandsResourceApi.md#lsof) | **POST** /cm/service/roleCommands/lsof | Run the lsof diagnostic command.
*MgmtRoleCommandsResourceApi* | [**restartCommand**](docs/MgmtRoleCommandsResourceApi.md#restartCommand) | **POST** /cm/service/roleCommands/restart | Restart a set of Cloudera Management Services roles.
*MgmtRoleCommandsResourceApi* | [**startCommand**](docs/MgmtRoleCommandsResourceApi.md#startCommand) | **POST** /cm/service/roleCommands/start | Start a set of Cloudera Management Services roles.
*MgmtRoleCommandsResourceApi* | [**stopCommand**](docs/MgmtRoleCommandsResourceApi.md#stopCommand) | **POST** /cm/service/roleCommands/stop | Stop a set of Cloudera Management Services roles.
*MgmtRoleConfigGroupsResourceApi* | [**readConfig**](docs/MgmtRoleConfigGroupsResourceApi.md#readConfig) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName}/config | Returns the current revision of the config for the specified role config group in the Cloudera Management Services.
*MgmtRoleConfigGroupsResourceApi* | [**readRoleConfigGroup**](docs/MgmtRoleConfigGroupsResourceApi.md#readRoleConfigGroup) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName} | Returns the information for a given role config group in the Cloudera Management Services.
*MgmtRoleConfigGroupsResourceApi* | [**readRoleConfigGroups**](docs/MgmtRoleConfigGroupsResourceApi.md#readRoleConfigGroups) | **GET** /cm/service/roleConfigGroups | Returns the information for all role config groups in the Cloudera Management Services.
*MgmtRoleConfigGroupsResourceApi* | [**readRoles**](docs/MgmtRoleConfigGroupsResourceApi.md#readRoles) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName}/roles | Returns all roles in the given role config group in the Cloudera Management Services.
*MgmtRoleConfigGroupsResourceApi* | [**updateConfig**](docs/MgmtRoleConfigGroupsResourceApi.md#updateConfig) | **PUT** /cm/service/roleConfigGroups/{roleConfigGroupName}/config | Updates the config for the given role config group in the Cloudera Management Services.
*MgmtRoleConfigGroupsResourceApi* | [**updateRoleConfigGroup**](docs/MgmtRoleConfigGroupsResourceApi.md#updateRoleConfigGroup) | **PUT** /cm/service/roleConfigGroups/{roleConfigGroupName} | Updates an existing role config group in the Cloudera Management Services.
*MgmtRolesResourceApi* | [**createRoles**](docs/MgmtRolesResourceApi.md#createRoles) | **POST** /cm/service/roles | Create new roles in the Cloudera Management Services.
*MgmtRolesResourceApi* | [**deleteRole**](docs/MgmtRolesResourceApi.md#deleteRole) | **DELETE** /cm/service/roles/{roleName} | Delete a role from the Cloudera Management Services.
*MgmtRolesResourceApi* | [**enterMaintenanceMode**](docs/MgmtRolesResourceApi.md#enterMaintenanceMode) | **POST** /cm/service/roles/{roleName}/commands/enterMaintenanceMode | Put the Cloudera Management Service role into maintenance mode.
*MgmtRolesResourceApi* | [**exitMaintenanceMode**](docs/MgmtRolesResourceApi.md#exitMaintenanceMode) | **POST** /cm/service/roles/{roleName}/commands/exitMaintenanceMode | Take the Cloudera Management Service role out of maintenance mode.
*MgmtRolesResourceApi* | [**getFullLog**](docs/MgmtRolesResourceApi.md#getFullLog) | **GET** /cm/service/roles/{roleName}/logs/full | Retrieves the log file for the role&#39;s main process.
*MgmtRolesResourceApi* | [**getStacksLog**](docs/MgmtRolesResourceApi.md#getStacksLog) | **GET** /cm/service/roles/{roleName}/logs/stacks | Retrieves the stacks log file, if any, for the role&#39;s main process.
*MgmtRolesResourceApi* | [**getStacksLogsBundle**](docs/MgmtRolesResourceApi.md#getStacksLogsBundle) | **GET** /cm/service/roles/{roleName}/logs/stacksBundle | Download a zip-compressed archive of role stacks logs.
*MgmtRolesResourceApi* | [**getStandardError**](docs/MgmtRolesResourceApi.md#getStandardError) | **GET** /cm/service/roles/{roleName}/logs/stderr | Retrieves the role&#39;s standard error output.
*MgmtRolesResourceApi* | [**getStandardOutput**](docs/MgmtRolesResourceApi.md#getStandardOutput) | **GET** /cm/service/roles/{roleName}/logs/stdout | Retrieves the role&#39;s standard output.
*MgmtRolesResourceApi* | [**listActiveCommands**](docs/MgmtRolesResourceApi.md#listActiveCommands) | **GET** /cm/service/roles/{roleName}/commands | List active role commands.
*MgmtRolesResourceApi* | [**readRole**](docs/MgmtRolesResourceApi.md#readRole) | **GET** /cm/service/roles/{roleName} | Retrieve detailed information about a Cloudera Management Services role.
*MgmtRolesResourceApi* | [**readRoleConfig**](docs/MgmtRolesResourceApi.md#readRoleConfig) | **GET** /cm/service/roles/{roleName}/config | Retrieve the configuration of a specific Cloudera Management Services role.
*MgmtRolesResourceApi* | [**readRoles**](docs/MgmtRolesResourceApi.md#readRoles) | **GET** /cm/service/roles | List all roles of the Cloudera Management Services.
*MgmtRolesResourceApi* | [**updateRoleConfig**](docs/MgmtRolesResourceApi.md#updateRoleConfig) | **PUT** /cm/service/roles/{roleName}/config | Update the configuration of a Cloudera Management Services role.
*MgmtServiceResourceApi* | [**autoAssignRoles**](docs/MgmtServiceResourceApi.md#autoAssignRoles) | **PUT** /cm/service/autoAssignRoles | Automatically assign roles to hosts and create the roles for the Cloudera Management Service.
*MgmtServiceResourceApi* | [**autoConfigure**](docs/MgmtServiceResourceApi.md#autoConfigure) | **PUT** /cm/service/autoConfigure | Automatically configures roles of the Cloudera Management Service.
*MgmtServiceResourceApi* | [**deleteCMS**](docs/MgmtServiceResourceApi.md#deleteCMS) | **DELETE** /cm/service | Delete the Cloudera Management Services.
*MgmtServiceResourceApi* | [**enterMaintenanceMode**](docs/MgmtServiceResourceApi.md#enterMaintenanceMode) | **POST** /cm/service/commands/enterMaintenanceMode | Put Cloudera Management Service into maintenance mode.
*MgmtServiceResourceApi* | [**exitMaintenanceMode**](docs/MgmtServiceResourceApi.md#exitMaintenanceMode) | **POST** /cm/service/commands/exitMaintenanceMode | Take Cloudera Management Service out of maintenance mode.
*MgmtServiceResourceApi* | [**listActiveCommands**](docs/MgmtServiceResourceApi.md#listActiveCommands) | **GET** /cm/service/commands | List active Cloudera Management Services commands.
*MgmtServiceResourceApi* | [**listRoleTypes**](docs/MgmtServiceResourceApi.md#listRoleTypes) | **GET** /cm/service/roleTypes | List the supported role types for the Cloudera Management Services.
*MgmtServiceResourceApi* | [**readService**](docs/MgmtServiceResourceApi.md#readService) | **GET** /cm/service | Retrieve information about the Cloudera Management Services.
*MgmtServiceResourceApi* | [**readServiceConfig**](docs/MgmtServiceResourceApi.md#readServiceConfig) | **GET** /cm/service/config | Retrieve the configuration of the Cloudera Management Services.
*MgmtServiceResourceApi* | [**restartCommand**](docs/MgmtServiceResourceApi.md#restartCommand) | **POST** /cm/service/commands/restart | Restart the Cloudera Management Services.
*MgmtServiceResourceApi* | [**setupCMS**](docs/MgmtServiceResourceApi.md#setupCMS) | **PUT** /cm/service | Setup the Cloudera Management Services.
*MgmtServiceResourceApi* | [**startCommand**](docs/MgmtServiceResourceApi.md#startCommand) | **POST** /cm/service/commands/start | Start the Cloudera Management Services.
*MgmtServiceResourceApi* | [**stopCommand**](docs/MgmtServiceResourceApi.md#stopCommand) | **POST** /cm/service/commands/stop | Stop the Cloudera Management Services.
*MgmtServiceResourceApi* | [**updateServiceConfig**](docs/MgmtServiceResourceApi.md#updateServiceConfig) | **PUT** /cm/service/config | Update the Cloudera Management Services configuration.
*NameservicesResourceApi* | [**getMetrics**](docs/NameservicesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices/{nameservice}/metrics | Fetch metric readings for a particular nameservice.
*NameservicesResourceApi* | [**listNameservices**](docs/NameservicesResourceApi.md#listNameservices) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices | List the nameservices of an HDFS service.
*NameservicesResourceApi* | [**readNameservice**](docs/NameservicesResourceApi.md#readNameservice) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices/{nameservice} | Retrieve information about a nameservice.
*ParcelResourceApi* | [**activateCommand**](docs/ParcelResourceApi.md#activateCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/activate | A synchronous command that activates the parcel on the cluster.
*ParcelResourceApi* | [**cancelDistributionCommand**](docs/ParcelResourceApi.md#cancelDistributionCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/cancelDistribution | A synchronous command that cancels the parcel distribution.
*ParcelResourceApi* | [**cancelDownloadCommand**](docs/ParcelResourceApi.md#cancelDownloadCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/cancelDownload | A synchronous command that cancels the parcel download.
*ParcelResourceApi* | [**deactivateCommand**](docs/ParcelResourceApi.md#deactivateCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/deactivate | A synchronous command that deactivates the parcel on the cluster.
*ParcelResourceApi* | [**readParcel**](docs/ParcelResourceApi.md#readParcel) | **GET** /clusters/{clusterName}/parcels/products/{product}/versions/{version} | Retrieves detailed information about a parcel.
*ParcelResourceApi* | [**removeDownloadCommand**](docs/ParcelResourceApi.md#removeDownloadCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/removeDownload | A synchronous command that removes the downloaded parcel.
*ParcelResourceApi* | [**startDistributionCommand**](docs/ParcelResourceApi.md#startDistributionCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startDistribution | A synchronous command that starts the distribution of the parcel to the cluster.
*ParcelResourceApi* | [**startDownloadCommand**](docs/ParcelResourceApi.md#startDownloadCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startDownload | A synchronous command that starts the parcel download.
*ParcelResourceApi* | [**startRemovalOfDistributionCommand**](docs/ParcelResourceApi.md#startRemovalOfDistributionCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startRemovalOfDistribution | A synchronous command that removes the distribution from the hosts in the cluster.
*ParcelsResourceApi* | [**getParcelUsage**](docs/ParcelsResourceApi.md#getParcelUsage) | **GET** /clusters/{clusterName}/parcels/usage | Retrieve details parcel usage information for the cluster.
*ParcelsResourceApi* | [**readParcels**](docs/ParcelsResourceApi.md#readParcels) | **GET** /clusters/{clusterName}/parcels | Lists all parcels that the cluster has access to.
*ProcessResourceApi* | [**getConfigFile**](docs/ProcessResourceApi.md#getConfigFile) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/process/configFiles/{configFileName} | Returns the contents of the specified config file.
*ProcessResourceApi* | [**getProcess**](docs/ProcessResourceApi.md#getProcess) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/process | 
*ReplicationsResourceApi* | [**collectDiagnosticData**](docs/ReplicationsResourceApi.md#collectDiagnosticData) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/collectDiagnosticData | Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.
*ReplicationsResourceApi* | [**createSchedules**](docs/ReplicationsResourceApi.md#createSchedules) | **POST** /clusters/{clusterName}/services/{serviceName}/replications | Creates one or more replication schedules.
*ReplicationsResourceApi* | [**deleteAllSchedules**](docs/ReplicationsResourceApi.md#deleteAllSchedules) | **DELETE** /clusters/{clusterName}/services/{serviceName}/replications | Deletes all existing replication schedules.
*ReplicationsResourceApi* | [**deleteSchedule**](docs/ReplicationsResourceApi.md#deleteSchedule) | **DELETE** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Deletes an existing replication schedule.
*ReplicationsResourceApi* | [**getReplicationState**](docs/ReplicationsResourceApi.md#getReplicationState) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/replicationState | returns the replication state.
*ReplicationsResourceApi* | [**readHistory**](docs/ReplicationsResourceApi.md#readHistory) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/history | Returns a list of commands triggered by a schedule.
*ReplicationsResourceApi* | [**readSchedule**](docs/ReplicationsResourceApi.md#readSchedule) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Returns information for a specific replication schedule.
*ReplicationsResourceApi* | [**readSchedules**](docs/ReplicationsResourceApi.md#readSchedules) | **GET** /clusters/{clusterName}/services/{serviceName}/replications | Returns information for all replication schedules.
*ReplicationsResourceApi* | [**runCopyListing**](docs/ReplicationsResourceApi.md#runCopyListing) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/hdfsCopyListing | Run the hdfs copy listing command.
*ReplicationsResourceApi* | [**runSchedule**](docs/ReplicationsResourceApi.md#runSchedule) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/run | Run the schedule immediately.
*ReplicationsResourceApi* | [**updateSchedule**](docs/ReplicationsResourceApi.md#updateSchedule) | **PUT** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Updates an existing replication schedule.
*RoleCommandsResourceApi* | [**formatCommand**](docs/RoleCommandsResourceApi.md#formatCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsFormat | Format HDFS NameNodes.
*RoleCommandsResourceApi* | [**hdfsBootstrapStandByCommand**](docs/RoleCommandsResourceApi.md#hdfsBootstrapStandByCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsBootstrapStandBy | Bootstrap HDFS stand-by NameNodes.
*RoleCommandsResourceApi* | [**hdfsEnterSafemode**](docs/RoleCommandsResourceApi.md#hdfsEnterSafemode) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsEnterSafemode | Enter safemode for namenodes.
*RoleCommandsResourceApi* | [**hdfsFinalizeMetadataUpgrade**](docs/RoleCommandsResourceApi.md#hdfsFinalizeMetadataUpgrade) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsFinalizeMetadataUpgrade | Finalize HDFS NameNode metadata upgrade.
*RoleCommandsResourceApi* | [**hdfsInitializeAutoFailoverCommand**](docs/RoleCommandsResourceApi.md#hdfsInitializeAutoFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsInitializeAutoFailover | Initialize HDFS HA failover controller metadata.
*RoleCommandsResourceApi* | [**hdfsInitializeSharedDirCommand**](docs/RoleCommandsResourceApi.md#hdfsInitializeSharedDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsInitializeSharedDir | Initialize HDFS NameNodes&#39; shared edit directory.
*RoleCommandsResourceApi* | [**hdfsLeaveSafemode**](docs/RoleCommandsResourceApi.md#hdfsLeaveSafemode) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsLeaveSafemode | Leave safemode for namenodes.
*RoleCommandsResourceApi* | [**hdfsSaveNamespace**](docs/RoleCommandsResourceApi.md#hdfsSaveNamespace) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsSaveNamespace | Save namespace for namenodes.
*RoleCommandsResourceApi* | [**jmapDump**](docs/RoleCommandsResourceApi.md#jmapDump) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jmapDump | Run the jmapDump diagnostic command.
*RoleCommandsResourceApi* | [**jmapHisto**](docs/RoleCommandsResourceApi.md#jmapHisto) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jmapHisto | Run the jmapHisto diagnostic command.
*RoleCommandsResourceApi* | [**jstack**](docs/RoleCommandsResourceApi.md#jstack) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jstack | Run the jstack diagnostic command.
*RoleCommandsResourceApi* | [**lsof**](docs/RoleCommandsResourceApi.md#lsof) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/lsof | Run the lsof diagnostic command.
*RoleCommandsResourceApi* | [**refreshCommand**](docs/RoleCommandsResourceApi.md#refreshCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/refresh | Refresh a role&#39;s data.
*RoleCommandsResourceApi* | [**restartCommand**](docs/RoleCommandsResourceApi.md#restartCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/restart | Restart a set of role instances.
*RoleCommandsResourceApi* | [**roleCommandByName**](docs/RoleCommandsResourceApi.md#roleCommandByName) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/{commandName} | Execute a role command by name.
*RoleCommandsResourceApi* | [**startCommand**](docs/RoleCommandsResourceApi.md#startCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/start | Start a set of role instances.
*RoleCommandsResourceApi* | [**stopCommand**](docs/RoleCommandsResourceApi.md#stopCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/stop | Stop a set of role instances.
*RoleCommandsResourceApi* | [**syncHueDbCommand**](docs/RoleCommandsResourceApi.md#syncHueDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hueSyncDb | Create / update the Hue database schema.
*RoleCommandsResourceApi* | [**zooKeeperCleanupCommand**](docs/RoleCommandsResourceApi.md#zooKeeperCleanupCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/zooKeeperCleanup | Cleanup a list of ZooKeeper server roles.
*RoleCommandsResourceApi* | [**zooKeeperInitCommand**](docs/RoleCommandsResourceApi.md#zooKeeperInitCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/zooKeeperInit | Initialize a list of ZooKeeper server roles.
*RoleConfigGroupsResourceApi* | [**createRoleConfigGroups**](docs/RoleConfigGroupsResourceApi.md#createRoleConfigGroups) | **POST** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups | Creates new role config groups.
*RoleConfigGroupsResourceApi* | [**deleteRoleConfigGroup**](docs/RoleConfigGroupsResourceApi.md#deleteRoleConfigGroup) | **DELETE** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Deletes a role config group.
*RoleConfigGroupsResourceApi* | [**moveRoles**](docs/RoleConfigGroupsResourceApi.md#moveRoles) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/roles | Moves roles to the specified role config group.
*RoleConfigGroupsResourceApi* | [**moveRolesToBaseGroup**](docs/RoleConfigGroupsResourceApi.md#moveRolesToBaseGroup) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/roles | Moves roles to the base role config group.
*RoleConfigGroupsResourceApi* | [**readConfig**](docs/RoleConfigGroupsResourceApi.md#readConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/config | Returns the current revision of the config for the specified role config group.
*RoleConfigGroupsResourceApi* | [**readRoleConfigGroup**](docs/RoleConfigGroupsResourceApi.md#readRoleConfigGroup) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Returns the information for a role config group.
*RoleConfigGroupsResourceApi* | [**readRoleConfigGroups**](docs/RoleConfigGroupsResourceApi.md#readRoleConfigGroups) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups | Returns the information for all role config groups for a given cluster and service.
*RoleConfigGroupsResourceApi* | [**readRoles**](docs/RoleConfigGroupsResourceApi.md#readRoles) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/roles | Returns all roles in the given role config group.
*RoleConfigGroupsResourceApi* | [**updateConfig**](docs/RoleConfigGroupsResourceApi.md#updateConfig) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/config | Updates the config for the given role config group.
*RoleConfigGroupsResourceApi* | [**updateRoleConfigGroup**](docs/RoleConfigGroupsResourceApi.md#updateRoleConfigGroup) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Updates an existing role config group.
*RolesResourceApi* | [**bulkDeleteRoles**](docs/RolesResourceApi.md#bulkDeleteRoles) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/bulkDelete | Bulk delete roles in a particular service by name.
*RolesResourceApi* | [**createRoles**](docs/RolesResourceApi.md#createRoles) | **POST** /clusters/{clusterName}/services/{serviceName}/roles | Create new roles in a given service.
*RolesResourceApi* | [**deleteRole**](docs/RolesResourceApi.md#deleteRole) | **DELETE** /clusters/{clusterName}/services/{serviceName}/roles/{roleName} | Deletes a role from a given service.
*RolesResourceApi* | [**enterMaintenanceMode**](docs/RolesResourceApi.md#enterMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/enterMaintenanceMode | Put the role into maintenance mode.
*RolesResourceApi* | [**exitMaintenanceMode**](docs/RolesResourceApi.md#exitMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/exitMaintenanceMode | Take the role out of maintenance mode.
*RolesResourceApi* | [**getFullLog**](docs/RolesResourceApi.md#getFullLog) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/full | Retrieves the log file for the role&#39;s main process.
*RolesResourceApi* | [**getMetrics**](docs/RolesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/metrics | Fetch metric readings for a particular role.
*RolesResourceApi* | [**getStacksLog**](docs/RolesResourceApi.md#getStacksLog) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stacks | Retrieves the stacks log file, if any, for the role&#39;s main process.
*RolesResourceApi* | [**getStacksLogsBundle**](docs/RolesResourceApi.md#getStacksLogsBundle) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stacksBundle | Download a zip-compressed archive of role stacks logs.
*RolesResourceApi* | [**getStandardError**](docs/RolesResourceApi.md#getStandardError) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stderr | Retrieves the role&#39;s standard error output.
*RolesResourceApi* | [**getStandardOutput**](docs/RolesResourceApi.md#getStandardOutput) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stdout | Retrieves the role&#39;s standard output.
*RolesResourceApi* | [**impalaDiagnostics**](docs/RolesResourceApi.md#impalaDiagnostics) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/impalaDiagnostics | Collects diagnostics data for an Impala role.
*RolesResourceApi* | [**listActiveCommands**](docs/RolesResourceApi.md#listActiveCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands | List active role commands.
*RolesResourceApi* | [**listCommands**](docs/RolesResourceApi.md#listCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commandsByName | Lists all the commands that can be executed by name on the provided role.
*RolesResourceApi* | [**readRole**](docs/RolesResourceApi.md#readRole) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName} | Retrieves detailed information about a role.
*RolesResourceApi* | [**readRoleConfig**](docs/RolesResourceApi.md#readRoleConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/config | Retrieves the configuration of a specific role.
*RolesResourceApi* | [**readRoles**](docs/RolesResourceApi.md#readRoles) | **GET** /clusters/{clusterName}/services/{serviceName}/roles | Lists all roles of a given service.
*RolesResourceApi* | [**updateRoleConfig**](docs/RolesResourceApi.md#updateRoleConfig) | **PUT** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/config | Updates the role configuration with the given values.
*ServicesResourceApi* | [**collectYarnApplicationDiagnostics**](docs/ServicesResourceApi.md#collectYarnApplicationDiagnostics) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnApplicationDiagnosticsCollection | Collect the Diagnostics data for Yarn applications.
*ServicesResourceApi* | [**createBeeswaxWarehouseCommand**](docs/ServicesResourceApi.md#createBeeswaxWarehouseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueCreateHiveWarehouse | Create the Beeswax role&#39;s Hive warehouse directory, on Hue services.
*ServicesResourceApi* | [**createHBaseRootCommand**](docs/ServicesResourceApi.md#createHBaseRootCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hbaseCreateRoot | Creates the root directory of an HBase service.
*ServicesResourceApi* | [**createHiveUserDirCommand**](docs/ServicesResourceApi.md#createHiveUserDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateHiveUserDir | Create the Hive user directory.
*ServicesResourceApi* | [**createHiveWarehouseCommand**](docs/ServicesResourceApi.md#createHiveWarehouseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateHiveWarehouse | Create the Hive warehouse directory, on Hive services.
*ServicesResourceApi* | [**createImpalaUserDirCommand**](docs/ServicesResourceApi.md#createImpalaUserDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateUserDir | Create the Impala user directory.
*ServicesResourceApi* | [**createOozieDb**](docs/ServicesResourceApi.md#createOozieDb) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createOozieDb | Creates the Oozie Database Schema in the configured database.
*ServicesResourceApi* | [**createServices**](docs/ServicesResourceApi.md#createServices) | **POST** /clusters/{clusterName}/services | Creates a list of services.
*ServicesResourceApi* | [**createSolrHdfsHomeDirCommand**](docs/ServicesResourceApi.md#createSolrHdfsHomeDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createSolrHdfsHomeDir | Creates the home directory of a Solr service in HDFS.
*ServicesResourceApi* | [**createSqoopUserDirCommand**](docs/ServicesResourceApi.md#createSqoopUserDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createSqoopUserDir | Creates the user directory of a Sqoop service in HDFS.
*ServicesResourceApi* | [**createYarnCmContainerUsageInputDirCommand**](docs/ServicesResourceApi.md#createYarnCmContainerUsageInputDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnCreateCmContainerUsageInputDirCommand | Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics.
*ServicesResourceApi* | [**createYarnJobHistoryDirCommand**](docs/ServicesResourceApi.md#createYarnJobHistoryDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnCreateJobHistoryDirCommand | Create the Yarn job history directory.
*ServicesResourceApi* | [**createYarnNodeManagerRemoteAppLogDirCommand**](docs/ServicesResourceApi.md#createYarnNodeManagerRemoteAppLogDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnNodeManagerRemoteAppLogDirCommand | Create the Yarn NodeManager remote application log directory.
*ServicesResourceApi* | [**decommissionCommand**](docs/ServicesResourceApi.md#decommissionCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/decommission | Decommission roles of a service.
*ServicesResourceApi* | [**deleteService**](docs/ServicesResourceApi.md#deleteService) | **DELETE** /clusters/{clusterName}/services/{serviceName} | Deletes a service from the system.
*ServicesResourceApi* | [**deployClientConfigCommand**](docs/ServicesResourceApi.md#deployClientConfigCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/deployClientConfig | Deploy a service&#39;s client configuration.
*ServicesResourceApi* | [**disableJtHaCommand**](docs/ServicesResourceApi.md#disableJtHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableJtHa | Disable high availability (HA) for JobTracker.
*ServicesResourceApi* | [**disableLlamaHaCommand**](docs/ServicesResourceApi.md#disableLlamaHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaDisableLlamaHa | Not Supported.
*ServicesResourceApi* | [**disableLlamaRmCommand**](docs/ServicesResourceApi.md#disableLlamaRmCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaDisableLlamaRm | Not Supported.
*ServicesResourceApi* | [**disableOozieHaCommand**](docs/ServicesResourceApi.md#disableOozieHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieDisableHa | Disable high availability (HA) for Oozie.
*ServicesResourceApi* | [**disableRmHaCommand**](docs/ServicesResourceApi.md#disableRmHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableRmHa | Disable high availability (HA) for ResourceManager.
*ServicesResourceApi* | [**disableSentryHaCommand**](docs/ServicesResourceApi.md#disableSentryHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableSentryHa | Disable high availability (HA) for Sentry service.
*ServicesResourceApi* | [**enableJtHaCommand**](docs/ServicesResourceApi.md#enableJtHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableJtHa | Enable high availability (HA) for a JobTracker.
*ServicesResourceApi* | [**enableLlamaHaCommand**](docs/ServicesResourceApi.md#enableLlamaHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaEnableLlamaHa | Not Supported.
*ServicesResourceApi* | [**enableLlamaRmCommand**](docs/ServicesResourceApi.md#enableLlamaRmCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaEnableLlamaRm | Not Supported.
*ServicesResourceApi* | [**enableOozieHaCommand**](docs/ServicesResourceApi.md#enableOozieHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieEnableHa | Enable high availability (HA) for Oozie service.
*ServicesResourceApi* | [**enableRmHaCommand**](docs/ServicesResourceApi.md#enableRmHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableRmHa | Enable high availability (HA) for a YARN ResourceManager.
*ServicesResourceApi* | [**enableSentryHaCommand**](docs/ServicesResourceApi.md#enableSentryHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableSentryHa | Enable high availability (HA) for Sentry service.
*ServicesResourceApi* | [**enterMaintenanceMode**](docs/ServicesResourceApi.md#enterMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enterMaintenanceMode | Put the service into maintenance mode.
*ServicesResourceApi* | [**exitMaintenanceMode**](docs/ServicesResourceApi.md#exitMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/exitMaintenanceMode | Take the service out of maintenance mode.
*ServicesResourceApi* | [**firstRun**](docs/ServicesResourceApi.md#firstRun) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/firstRun | Prepare and start a service.
*ServicesResourceApi* | [**getClientConfig**](docs/ServicesResourceApi.md#getClientConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/clientConfig | Download a zip-compressed archive of the client configuration, of a specific service.
*ServicesResourceApi* | [**getHdfsUsageReport**](docs/ServicesResourceApi.md#getHdfsUsageReport) | **GET** /clusters/{clusterName}/services/{serviceName}/reports/hdfsUsageReport | Fetch the HDFS usage report.
*ServicesResourceApi* | [**getImpalaUtilization**](docs/ServicesResourceApi.md#getImpalaUtilization) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaUtilization | Provides the resource utilization of the Impala service as well as the resource utilization per tenant.
*ServicesResourceApi* | [**getMetrics**](docs/ServicesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/metrics | Fetch metric readings for a particular service.
*ServicesResourceApi* | [**getMrUsageReport**](docs/ServicesResourceApi.md#getMrUsageReport) | **GET** /clusters/{clusterName}/services/{serviceName}/reports/mrUsageReport | Fetch the MR usage report.
*ServicesResourceApi* | [**getYarnUtilization**](docs/ServicesResourceApi.md#getYarnUtilization) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnUtilization | Provides the resource utilization of the yarn service as well as the resource utilization per tenant.
*ServicesResourceApi* | [**hbaseUpgradeCommand**](docs/ServicesResourceApi.md#hbaseUpgradeCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hbaseUpgrade | Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5.
*ServicesResourceApi* | [**hdfsCreateTmpDir**](docs/ServicesResourceApi.md#hdfsCreateTmpDir) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsCreateTmpDir | Creates a tmp directory on the HDFS filesystem.
*ServicesResourceApi* | [**hdfsDisableAutoFailoverCommand**](docs/ServicesResourceApi.md#hdfsDisableAutoFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableAutoFailover | Disable auto-failover for a highly available HDFS nameservice.
*ServicesResourceApi* | [**hdfsDisableHaCommand**](docs/ServicesResourceApi.md#hdfsDisableHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableHa | Disable high availability (HA) for an HDFS NameNode.
*ServicesResourceApi* | [**hdfsDisableNnHaCommand**](docs/ServicesResourceApi.md#hdfsDisableNnHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableNnHa | Disable High Availability (HA) with Automatic Failover for an HDFS NameNode.
*ServicesResourceApi* | [**hdfsEnableAutoFailoverCommand**](docs/ServicesResourceApi.md#hdfsEnableAutoFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableAutoFailover | Enable auto-failover for an HDFS nameservice.
*ServicesResourceApi* | [**hdfsEnableHaCommand**](docs/ServicesResourceApi.md#hdfsEnableHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableHa | Enable high availability (HA) for an HDFS NameNode.
*ServicesResourceApi* | [**hdfsEnableNnHaCommand**](docs/ServicesResourceApi.md#hdfsEnableNnHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableNnHa | Enable High Availability (HA) with Automatic Failover for an HDFS NameNode.
*ServicesResourceApi* | [**hdfsFailoverCommand**](docs/ServicesResourceApi.md#hdfsFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsFailover | Initiate a failover in an HDFS HA NameNode pair.
*ServicesResourceApi* | [**hdfsFinalizeRollingUpgrade**](docs/ServicesResourceApi.md#hdfsFinalizeRollingUpgrade) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsFinalizeRollingUpgrade | Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version.
*ServicesResourceApi* | [**hdfsRollEditsCommand**](docs/ServicesResourceApi.md#hdfsRollEditsCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsRollEdits | Roll the edits of an HDFS NameNode or Nameservice.
*ServicesResourceApi* | [**hdfsUpgradeMetadataCommand**](docs/ServicesResourceApi.md#hdfsUpgradeMetadataCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsUpgradeMetadata | Upgrade HDFS Metadata as part of a major version upgrade.
*ServicesResourceApi* | [**hiveCreateMetastoreDatabaseCommand**](docs/ServicesResourceApi.md#hiveCreateMetastoreDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateMetastoreDatabase | Create the Hive Metastore Database.
*ServicesResourceApi* | [**hiveCreateMetastoreDatabaseTablesCommand**](docs/ServicesResourceApi.md#hiveCreateMetastoreDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateMetastoreDatabaseTables | Create the Hive Metastore Database tables.
*ServicesResourceApi* | [**hiveUpdateMetastoreNamenodesCommand**](docs/ServicesResourceApi.md#hiveUpdateMetastoreNamenodesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveUpdateMetastoreNamenodes | Update Hive Metastore to point to a NameNode&#39;s Nameservice name instead of hostname.
*ServicesResourceApi* | [**hiveUpgradeMetastoreCommand**](docs/ServicesResourceApi.md#hiveUpgradeMetastoreCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveUpgradeMetastore | Upgrade Hive Metastore as part of a major version upgrade.
*ServicesResourceApi* | [**hiveValidateMetastoreSchemaCommand**](docs/ServicesResourceApi.md#hiveValidateMetastoreSchemaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveValidateMetastoreSchema | Validate the Hive Metastore Schema.
*ServicesResourceApi* | [**hueDumpDbCommand**](docs/ServicesResourceApi.md#hueDumpDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueDumpDb | Runs Hue&#39;s dumpdata command.
*ServicesResourceApi* | [**hueLoadDbCommand**](docs/ServicesResourceApi.md#hueLoadDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueLoadDb | Runs Hue&#39;s loaddata command.
*ServicesResourceApi* | [**hueSyncDbCommand**](docs/ServicesResourceApi.md#hueSyncDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueSyncDb | Runs Hue&#39;s syncdb command.
*ServicesResourceApi* | [**impalaCreateCatalogDatabaseCommand**](docs/ServicesResourceApi.md#impalaCreateCatalogDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateCatalogDatabase | .
*ServicesResourceApi* | [**impalaCreateCatalogDatabaseTablesCommand**](docs/ServicesResourceApi.md#impalaCreateCatalogDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateCatalogDatabaseTables | .
*ServicesResourceApi* | [**importMrConfigsIntoYarn**](docs/ServicesResourceApi.md#importMrConfigsIntoYarn) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/importMrConfigsIntoYarn | Import MapReduce configuration into Yarn, overwriting Yarn configuration.
*ServicesResourceApi* | [**initSolrCommand**](docs/ServicesResourceApi.md#initSolrCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/initSolr | Initializes the Solr service in Zookeeper.
*ServicesResourceApi* | [**installMrFrameworkJars**](docs/ServicesResourceApi.md#installMrFrameworkJars) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/installMrFrameworkJars | Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it.
*ServicesResourceApi* | [**installOozieShareLib**](docs/ServicesResourceApi.md#installOozieShareLib) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/installOozieShareLib | Creates directory for Oozie user in HDFS and installs the ShareLib in it.
*ServicesResourceApi* | [**ksMigrateToSentry**](docs/ServicesResourceApi.md#ksMigrateToSentry) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/migrateToSentry | Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer.
*ServicesResourceApi* | [**listActiveCommands**](docs/ServicesResourceApi.md#listActiveCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/commands | List active service commands.
*ServicesResourceApi* | [**listRoleTypes**](docs/ServicesResourceApi.md#listRoleTypes) | **GET** /clusters/{clusterName}/services/{serviceName}/roleTypes | List the supported role types for a service.
*ServicesResourceApi* | [**listServiceCommands**](docs/ServicesResourceApi.md#listServiceCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/commandsByName | Lists all the commands that can be executed by name on the provided service.
*ServicesResourceApi* | [**offlineCommand**](docs/ServicesResourceApi.md#offlineCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/offline | Offline roles of a service.
*ServicesResourceApi* | [**oozieCreateEmbeddedDatabaseCommand**](docs/ServicesResourceApi.md#oozieCreateEmbeddedDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieCreateEmbeddedDatabase | Create the Oozie Server Database.
*ServicesResourceApi* | [**oozieDumpDatabaseCommand**](docs/ServicesResourceApi.md#oozieDumpDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieDumpDatabase | Dump the Oozie Server Database.
*ServicesResourceApi* | [**oozieLoadDatabaseCommand**](docs/ServicesResourceApi.md#oozieLoadDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieLoadDatabase | Load the Oozie Server Database from dump.
*ServicesResourceApi* | [**oozieUpgradeDbCommand**](docs/ServicesResourceApi.md#oozieUpgradeDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieUpgradeDb | Upgrade Oozie Database schema as part of a major version upgrade.
*ServicesResourceApi* | [**readService**](docs/ServicesResourceApi.md#readService) | **GET** /clusters/{clusterName}/services/{serviceName} | Retrieves details information about a service.
*ServicesResourceApi* | [**readServiceConfig**](docs/ServicesResourceApi.md#readServiceConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/config | Retrieves the configuration of a specific service.
*ServicesResourceApi* | [**readServices**](docs/ServicesResourceApi.md#readServices) | **GET** /clusters/{clusterName}/services | Lists all services registered in the cluster.
*ServicesResourceApi* | [**recommissionCommand**](docs/ServicesResourceApi.md#recommissionCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/recommission | Recommission roles of a service.
*ServicesResourceApi* | [**recommissionWithStartCommand**](docs/ServicesResourceApi.md#recommissionWithStartCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/recommissionWithStart | Start and recommission roles of a service.
*ServicesResourceApi* | [**restartCommand**](docs/ServicesResourceApi.md#restartCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/restart | Restart the service.
*ServicesResourceApi* | [**rollingRestart**](docs/ServicesResourceApi.md#rollingRestart) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/rollingRestart | Command to run rolling restart of roles in a service.
*ServicesResourceApi* | [**sentryCreateDatabaseCommand**](docs/ServicesResourceApi.md#sentryCreateDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryCreateDatabase | Create the Sentry Server Database.
*ServicesResourceApi* | [**sentryCreateDatabaseTablesCommand**](docs/ServicesResourceApi.md#sentryCreateDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryCreateDatabaseTables | Create the Sentry Server Database tables.
*ServicesResourceApi* | [**sentryUpgradeDatabaseTablesCommand**](docs/ServicesResourceApi.md#sentryUpgradeDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryUpgradeDatabaseTables | Upgrade the Sentry Server Database tables.
*ServicesResourceApi* | [**serviceCommandByName**](docs/ServicesResourceApi.md#serviceCommandByName) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/{commandName} | Executes a command on the service specified by name.
*ServicesResourceApi* | [**solrBootstrapCollectionsCommand**](docs/ServicesResourceApi.md#solrBootstrapCollectionsCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrBootstrapCollections | Bootstraps Solr Collections after the CDH upgrade.
*ServicesResourceApi* | [**solrBootstrapConfigCommand**](docs/ServicesResourceApi.md#solrBootstrapConfigCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrBootstrapConfig | Bootstraps Solr config during the CDH upgrade.
*ServicesResourceApi* | [**solrConfigBackupCommand**](docs/ServicesResourceApi.md#solrConfigBackupCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrConfigBackup | Backs up Solr configuration metadata before CDH upgrade.
*ServicesResourceApi* | [**solrMigrateSentryPrivilegesCommand**](docs/ServicesResourceApi.md#solrMigrateSentryPrivilegesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrMigrateSentryPrivilegesCommand | Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service.
*ServicesResourceApi* | [**solrReinitializeStateForUpgradeCommand**](docs/ServicesResourceApi.md#solrReinitializeStateForUpgradeCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrReinitializeStateForUpgrade | Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state.
*ServicesResourceApi* | [**solrValidateMetadataCommand**](docs/ServicesResourceApi.md#solrValidateMetadataCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrValidateMetadata | Validates Solr metadata and configurations.
*ServicesResourceApi* | [**sqoopCreateDatabaseTablesCommand**](docs/ServicesResourceApi.md#sqoopCreateDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sqoopCreateDatabaseTables | Create the Sqoop2 Server Database tables.
*ServicesResourceApi* | [**sqoopUpgradeDbCommand**](docs/ServicesResourceApi.md#sqoopUpgradeDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sqoopUpgradeDb | Upgrade Sqoop Database schema as part of a major version upgrade.
*ServicesResourceApi* | [**startCommand**](docs/ServicesResourceApi.md#startCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/start | Start the service.
*ServicesResourceApi* | [**stopCommand**](docs/ServicesResourceApi.md#stopCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/stop | Stop the service.
*ServicesResourceApi* | [**switchToMr2**](docs/ServicesResourceApi.md#switchToMr2) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/switchToMr2 | Change the cluster to use MR2 instead of MR1.
*ServicesResourceApi* | [**updateService**](docs/ServicesResourceApi.md#updateService) | **PUT** /clusters/{clusterName}/services/{serviceName} | Updates service information.
*ServicesResourceApi* | [**updateServiceConfig**](docs/ServicesResourceApi.md#updateServiceConfig) | **PUT** /clusters/{clusterName}/services/{serviceName}/config | Updates the service configuration with the given values.
*ServicesResourceApi* | [**yarnFormatStateStore**](docs/ServicesResourceApi.md#yarnFormatStateStore) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnFormatStateStore | Formats the state store in ZooKeeper used for Resource Manager High Availability.
*ServicesResourceApi* | [**zooKeeperCleanupCommand**](docs/ServicesResourceApi.md#zooKeeperCleanupCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/zooKeeperCleanup | Clean up all running server instances of a ZooKeeper service.
*ServicesResourceApi* | [**zooKeeperInitCommand**](docs/ServicesResourceApi.md#zooKeeperInitCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/zooKeeperInit | Initializes all the server instances of a ZooKeeper service.
*SnapshotsResourceApi* | [**createPolicies**](docs/SnapshotsResourceApi.md#createPolicies) | **POST** /clusters/{clusterName}/services/{serviceName}/snapshots/policies | Creates one or more snapshot policies.
*SnapshotsResourceApi* | [**deletePolicy**](docs/SnapshotsResourceApi.md#deletePolicy) | **DELETE** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Deletes an existing snapshot policy.
*SnapshotsResourceApi* | [**readHistory**](docs/SnapshotsResourceApi.md#readHistory) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName}/history | Returns a list of commands triggered by a snapshot policy.
*SnapshotsResourceApi* | [**readPolicies**](docs/SnapshotsResourceApi.md#readPolicies) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies | Returns information for all snapshot policies.
*SnapshotsResourceApi* | [**readPolicy**](docs/SnapshotsResourceApi.md#readPolicy) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Returns information for a specific snapshot policy.
*SnapshotsResourceApi* | [**updatePolicy**](docs/SnapshotsResourceApi.md#updatePolicy) | **PUT** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Updates an existing snapshot policy.
*TimeSeriesResourceApi* | [**getEntityTypeAttributes**](docs/TimeSeriesResourceApi.md#getEntityTypeAttributes) | **GET** /timeseries/entityTypeAttributes | Retrieve all metric entity type attributes monitored by Cloudera Manager.
*TimeSeriesResourceApi* | [**getEntityTypes**](docs/TimeSeriesResourceApi.md#getEntityTypes) | **GET** /timeseries/entityTypes | Retrieve all metric entity types monitored by Cloudera Manager.
*TimeSeriesResourceApi* | [**getMetricSchema**](docs/TimeSeriesResourceApi.md#getMetricSchema) | **GET** /timeseries/schema | Retrieve schema for all metrics.
*TimeSeriesResourceApi* | [**queryTimeSeries**](docs/TimeSeriesResourceApi.md#queryTimeSeries) | **GET** /timeseries | Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.
*TimeSeriesResourceApi* | [**queryTimeSeries_0**](docs/TimeSeriesResourceApi.md#queryTimeSeries_0) | **POST** /timeseries | Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.
*ToolsResourceApi* | [**echo**](docs/ToolsResourceApi.md#echo) | **GET** /tools/echo | Echoes the provided message back to the caller.
*ToolsResourceApi* | [**echoError**](docs/ToolsResourceApi.md#echoError) | **GET** /tools/echoError | Throws an error containing the given input message.
*UsersResourceApi* | [**createUsers2**](docs/UsersResourceApi.md#createUsers2) | **POST** /users | Creates a list of users.
*UsersResourceApi* | [**deleteUser2**](docs/UsersResourceApi.md#deleteUser2) | **DELETE** /users/{userName} | Deletes a user from the system.
*UsersResourceApi* | [**getSessions**](docs/UsersResourceApi.md#getSessions) | **GET** /users/sessions | Return a list of the sessions associated with interactive authenticated users in Cloudera Manager.
*UsersResourceApi* | [**readUser2**](docs/UsersResourceApi.md#readUser2) | **GET** /users/{userName} | Returns detailed information about a user.
*UsersResourceApi* | [**readUsers2**](docs/UsersResourceApi.md#readUsers2) | **GET** /users | Returns a list of the user names configured in the system.
*UsersResourceApi* | [**updateUser2**](docs/UsersResourceApi.md#updateUser2) | **PUT** /users/{userName} | Updates the given user&#39;s information.
*WatchedDirResourceApi* | [**addWatchedDirectory**](docs/WatchedDirResourceApi.md#addWatchedDirectory) | **POST** /clusters/{clusterName}/services/{serviceName}/watcheddir | Adds a directory to the watching list.
*WatchedDirResourceApi* | [**listWatchedDirectories**](docs/WatchedDirResourceApi.md#listWatchedDirectories) | **GET** /clusters/{clusterName}/services/{serviceName}/watcheddir | Lists all the watched directories.
*WatchedDirResourceApi* | [**removeWatchedDirectory**](docs/WatchedDirResourceApi.md#removeWatchedDirectory) | **DELETE** /clusters/{clusterName}/services/{serviceName}/watcheddir/{directoryPath} | Removes a directory from the watching list.
*YarnApplicationsResourceApi* | [**getYarnApplicationAttributes**](docs/YarnApplicationsResourceApi.md#getYarnApplicationAttributes) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnApplications/attributes | Returns the list of all attributes that the Service Monitor can associate with YARN applications.
*YarnApplicationsResourceApi* | [**getYarnApplications**](docs/YarnApplicationsResourceApi.md#getYarnApplications) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnApplications | Returns a list of applications that satisfy the filter.
*YarnApplicationsResourceApi* | [**killYarnApplication**](docs/YarnApplicationsResourceApi.md#killYarnApplication) | **POST** /clusters/{clusterName}/services/{serviceName}/yarnApplications/{applicationId}/kill | Kills an YARN Application.


## Documentation for Models

 - [ApiActivity](docs/ApiActivity.md)
 - [ApiActivityStatus](docs/ApiActivityStatus.md)
 - [ApiActivityType](docs/ApiActivityType.md)
 - [ApiAudit](docs/ApiAudit.md)
 - [ApiAuthRole](docs/ApiAuthRole.md)
 - [ApiAuthRoleAuthority](docs/ApiAuthRoleAuthority.md)
 - [ApiAuthRoleMetadata](docs/ApiAuthRoleMetadata.md)
 - [ApiAuthRoleRef](docs/ApiAuthRoleRef.md)
 - [ApiBatchRequestElement](docs/ApiBatchRequestElement.md)
 - [ApiBatchResponseElement](docs/ApiBatchResponseElement.md)
 - [ApiCdhUpgradeArgs](docs/ApiCdhUpgradeArgs.md)
 - [ApiCluster](docs/ApiCluster.md)
 - [ApiClusterPerfInspectorArgs](docs/ApiClusterPerfInspectorArgs.md)
 - [ApiClusterRef](docs/ApiClusterRef.md)
 - [ApiClusterTemplate](docs/ApiClusterTemplate.md)
 - [ApiClusterTemplateConfig](docs/ApiClusterTemplateConfig.md)
 - [ApiClusterTemplateHostInfo](docs/ApiClusterTemplateHostInfo.md)
 - [ApiClusterTemplateHostTemplate](docs/ApiClusterTemplateHostTemplate.md)
 - [ApiClusterTemplateInstantiator](docs/ApiClusterTemplateInstantiator.md)
 - [ApiClusterTemplateRole](docs/ApiClusterTemplateRole.md)
 - [ApiClusterTemplateRoleConfigGroup](docs/ApiClusterTemplateRoleConfigGroup.md)
 - [ApiClusterTemplateRoleConfigGroupInfo](docs/ApiClusterTemplateRoleConfigGroupInfo.md)
 - [ApiClusterTemplateService](docs/ApiClusterTemplateService.md)
 - [ApiClusterTemplateVariable](docs/ApiClusterTemplateVariable.md)
 - [ApiClusterUtilization](docs/ApiClusterUtilization.md)
 - [ApiClusterVersion](docs/ApiClusterVersion.md)
 - [ApiClustersPerfInspectorArgs](docs/ApiClustersPerfInspectorArgs.md)
 - [ApiCmPeer](docs/ApiCmPeer.md)
 - [ApiCmPeerType](docs/ApiCmPeerType.md)
 - [ApiCollectDiagnosticDataArguments](docs/ApiCollectDiagnosticDataArguments.md)
 - [ApiCommand](docs/ApiCommand.md)
 - [ApiCommandMetadata](docs/ApiCommandMetadata.md)
 - [ApiCommissionState](docs/ApiCommissionState.md)
 - [ApiConfig](docs/ApiConfig.md)
 - [ApiConfigStalenessStatus](docs/ApiConfigStalenessStatus.md)
 - [ApiConfigureForKerberosArguments](docs/ApiConfigureForKerberosArguments.md)
 - [ApiDashboard](docs/ApiDashboard.md)
 - [ApiDeployment](docs/ApiDeployment.md)
 - [ApiDeployment2](docs/ApiDeployment2.md)
 - [ApiDisableJtHaArguments](docs/ApiDisableJtHaArguments.md)
 - [ApiDisableLlamaHaArguments](docs/ApiDisableLlamaHaArguments.md)
 - [ApiDisableNnHaArguments](docs/ApiDisableNnHaArguments.md)
 - [ApiDisableOozieHaArguments](docs/ApiDisableOozieHaArguments.md)
 - [ApiDisableRmHaArguments](docs/ApiDisableRmHaArguments.md)
 - [ApiDisableSentryHaArgs](docs/ApiDisableSentryHaArgs.md)
 - [ApiEcho](docs/ApiEcho.md)
 - [ApiEnableJtHaArguments](docs/ApiEnableJtHaArguments.md)
 - [ApiEnableLlamaHaArguments](docs/ApiEnableLlamaHaArguments.md)
 - [ApiEnableLlamaRmArguments](docs/ApiEnableLlamaRmArguments.md)
 - [ApiEnableNnHaArguments](docs/ApiEnableNnHaArguments.md)
 - [ApiEnableOozieHaArguments](docs/ApiEnableOozieHaArguments.md)
 - [ApiEnableRmHaArguments](docs/ApiEnableRmHaArguments.md)
 - [ApiEnableSentryHaArgs](docs/ApiEnableSentryHaArgs.md)
 - [ApiEntityStatus](docs/ApiEntityStatus.md)
 - [ApiEntityType](docs/ApiEntityType.md)
 - [ApiEvent](docs/ApiEvent.md)
 - [ApiEventAttribute](docs/ApiEventAttribute.md)
 - [ApiEventCategory](docs/ApiEventCategory.md)
 - [ApiEventSeverity](docs/ApiEventSeverity.md)
 - [ApiExternalAccount](docs/ApiExternalAccount.md)
 - [ApiExternalAccountCategory](docs/ApiExternalAccountCategory.md)
 - [ApiExternalAccountType](docs/ApiExternalAccountType.md)
 - [ApiExternalUserMapping](docs/ApiExternalUserMapping.md)
 - [ApiExternalUserMappingRef](docs/ApiExternalUserMappingRef.md)
 - [ApiExternalUserMappingType](docs/ApiExternalUserMappingType.md)
 - [ApiGenerateHostCertsArguments](docs/ApiGenerateHostCertsArguments.md)
 - [ApiHBaseSnapshot](docs/ApiHBaseSnapshot.md)
 - [ApiHBaseSnapshotError](docs/ApiHBaseSnapshotError.md)
 - [ApiHBaseSnapshotPolicyArguments](docs/ApiHBaseSnapshotPolicyArguments.md)
 - [ApiHBaseSnapshotResult](docs/ApiHBaseSnapshotResult.md)
 - [ApiHdfsDisableHaArguments](docs/ApiHdfsDisableHaArguments.md)
 - [ApiHdfsFailoverArguments](docs/ApiHdfsFailoverArguments.md)
 - [ApiHdfsHaArguments](docs/ApiHdfsHaArguments.md)
 - [ApiHdfsReplicationArguments](docs/ApiHdfsReplicationArguments.md)
 - [ApiHdfsReplicationCounter](docs/ApiHdfsReplicationCounter.md)
 - [ApiHdfsReplicationResult](docs/ApiHdfsReplicationResult.md)
 - [ApiHdfsSnapshot](docs/ApiHdfsSnapshot.md)
 - [ApiHdfsSnapshotError](docs/ApiHdfsSnapshotError.md)
 - [ApiHdfsSnapshotPolicyArguments](docs/ApiHdfsSnapshotPolicyArguments.md)
 - [ApiHdfsSnapshotResult](docs/ApiHdfsSnapshotResult.md)
 - [ApiHdfsUsageReportRow](docs/ApiHdfsUsageReportRow.md)
 - [ApiHealthCheck](docs/ApiHealthCheck.md)
 - [ApiHealthSummary](docs/ApiHealthSummary.md)
 - [ApiHiveReplicationArguments](docs/ApiHiveReplicationArguments.md)
 - [ApiHiveReplicationError](docs/ApiHiveReplicationError.md)
 - [ApiHiveReplicationResult](docs/ApiHiveReplicationResult.md)
 - [ApiHiveTable](docs/ApiHiveTable.md)
 - [ApiHiveUDF](docs/ApiHiveUDF.md)
 - [ApiHost](docs/ApiHost.md)
 - [ApiHostInstallArguments](docs/ApiHostInstallArguments.md)
 - [ApiHostRef](docs/ApiHostRef.md)
 - [ApiHostTemplate](docs/ApiHostTemplate.md)
 - [ApiHostsPerfInspectorArgs](docs/ApiHostsPerfInspectorArgs.md)
 - [ApiImpalaCancelResponse](docs/ApiImpalaCancelResponse.md)
 - [ApiImpalaQuery](docs/ApiImpalaQuery.md)
 - [ApiImpalaQueryAttribute](docs/ApiImpalaQueryAttribute.md)
 - [ApiImpalaQueryDetailsResponse](docs/ApiImpalaQueryDetailsResponse.md)
 - [ApiImpalaQueryResponse](docs/ApiImpalaQueryResponse.md)
 - [ApiImpalaRoleDiagnosticsArgs](docs/ApiImpalaRoleDiagnosticsArgs.md)
 - [ApiImpalaTenantUtilization](docs/ApiImpalaTenantUtilization.md)
 - [ApiImpalaUDF](docs/ApiImpalaUDF.md)
 - [ApiImpalaUtilization](docs/ApiImpalaUtilization.md)
 - [ApiImpalaUtilizationHistogram](docs/ApiImpalaUtilizationHistogram.md)
 - [ApiImpalaUtilizationHistogramBin](docs/ApiImpalaUtilizationHistogramBin.md)
 - [ApiJournalNodeArguments](docs/ApiJournalNodeArguments.md)
 - [ApiKerberosInfo](docs/ApiKerberosInfo.md)
 - [ApiLicense](docs/ApiLicense.md)
 - [ApiLicensedFeatureUsage](docs/ApiLicensedFeatureUsage.md)
 - [ApiListBase](docs/ApiListBase.md)
 - [ApiMapEntry](docs/ApiMapEntry.md)
 - [ApiMetric](docs/ApiMetric.md)
 - [ApiMetricData](docs/ApiMetricData.md)
 - [ApiMetricSchema](docs/ApiMetricSchema.md)
 - [ApiMigrateRolesArguments](docs/ApiMigrateRolesArguments.md)
 - [ApiMr2AppInformation](docs/ApiMr2AppInformation.md)
 - [ApiMrUsageReportRow](docs/ApiMrUsageReportRow.md)
 - [ApiNameservice](docs/ApiNameservice.md)
 - [ApiParcel](docs/ApiParcel.md)
 - [ApiParcelRef](docs/ApiParcelRef.md)
 - [ApiParcelState](docs/ApiParcelState.md)
 - [ApiParcelUsage](docs/ApiParcelUsage.md)
 - [ApiParcelUsageHost](docs/ApiParcelUsageHost.md)
 - [ApiParcelUsageParcel](docs/ApiParcelUsageParcel.md)
 - [ApiParcelUsageRack](docs/ApiParcelUsageRack.md)
 - [ApiParcelUsageRole](docs/ApiParcelUsageRole.md)
 - [ApiPerfInspectorPingArgs](docs/ApiPerfInspectorPingArgs.md)
 - [ApiProcess](docs/ApiProcess.md)
 - [ApiProductVersion](docs/ApiProductVersion.md)
 - [ApiReplicationDiagnosticsCollectionArgs](docs/ApiReplicationDiagnosticsCollectionArgs.md)
 - [ApiReplicationState](docs/ApiReplicationState.md)
 - [ApiRestartClusterArgs](docs/ApiRestartClusterArgs.md)
 - [ApiRole](docs/ApiRole.md)
 - [ApiRoleConfigGroup](docs/ApiRoleConfigGroup.md)
 - [ApiRoleConfigGroupRef](docs/ApiRoleConfigGroupRef.md)
 - [ApiRoleRef](docs/ApiRoleRef.md)
 - [ApiRoleState](docs/ApiRoleState.md)
 - [ApiRolesToInclude](docs/ApiRolesToInclude.md)
 - [ApiRollEditsArgs](docs/ApiRollEditsArgs.md)
 - [ApiRollingRestartArgs](docs/ApiRollingRestartArgs.md)
 - [ApiRollingRestartClusterArgs](docs/ApiRollingRestartClusterArgs.md)
 - [ApiRollingUpgradeClusterArgs](docs/ApiRollingUpgradeClusterArgs.md)
 - [ApiRollingUpgradeServicesArgs](docs/ApiRollingUpgradeServicesArgs.md)
 - [ApiSchedule](docs/ApiSchedule.md)
 - [ApiScheduleInterval](docs/ApiScheduleInterval.md)
 - [ApiScmDbInfo](docs/ApiScmDbInfo.md)
 - [ApiService](docs/ApiService.md)
 - [ApiServiceRef](docs/ApiServiceRef.md)
 - [ApiServiceState](docs/ApiServiceState.md)
 - [ApiShutdownReadiness](docs/ApiShutdownReadiness.md)
 - [ApiSimpleRollingRestartClusterArgs](docs/ApiSimpleRollingRestartClusterArgs.md)
 - [ApiSnapshotPolicy](docs/ApiSnapshotPolicy.md)
 - [ApiTenantUtilization](docs/ApiTenantUtilization.md)
 - [ApiTimeSeries](docs/ApiTimeSeries.md)
 - [ApiTimeSeriesAggregateStatistics](docs/ApiTimeSeriesAggregateStatistics.md)
 - [ApiTimeSeriesCrossEntityMetadata](docs/ApiTimeSeriesCrossEntityMetadata.md)
 - [ApiTimeSeriesData](docs/ApiTimeSeriesData.md)
 - [ApiTimeSeriesEntityAttribute](docs/ApiTimeSeriesEntityAttribute.md)
 - [ApiTimeSeriesEntityType](docs/ApiTimeSeriesEntityType.md)
 - [ApiTimeSeriesMetadata](docs/ApiTimeSeriesMetadata.md)
 - [ApiTimeSeriesRequest](docs/ApiTimeSeriesRequest.md)
 - [ApiTimeSeriesResponse](docs/ApiTimeSeriesResponse.md)
 - [ApiUser](docs/ApiUser.md)
 - [ApiUser2](docs/ApiUser2.md)
 - [ApiUser2Ref](docs/ApiUser2Ref.md)
 - [ApiUserSession](docs/ApiUserSession.md)
 - [ApiVersionInfo](docs/ApiVersionInfo.md)
 - [ApiWatchedDir](docs/ApiWatchedDir.md)
 - [ApiYarnApplication](docs/ApiYarnApplication.md)
 - [ApiYarnApplicationAttribute](docs/ApiYarnApplicationAttribute.md)
 - [ApiYarnApplicationDiagnosticsCollectionArgs](docs/ApiYarnApplicationDiagnosticsCollectionArgs.md)
 - [ApiYarnApplicationResponse](docs/ApiYarnApplicationResponse.md)
 - [ApiYarnKillResponse](docs/ApiYarnKillResponse.md)
 - [ApiYarnTenantUtilization](docs/ApiYarnTenantUtilization.md)
 - [ApiYarnUtilization](docs/ApiYarnUtilization.md)
 - [HTTPMethod](docs/HTTPMethod.md)
 - [HaStatus](docs/HaStatus.md)
 - [ReplicationOption](docs/ReplicationOption.md)
 - [ReplicationStrategy](docs/ReplicationStrategy.md)
 - [ScmDbType](docs/ScmDbType.md)
 - [ShutdownReadinessState](docs/ShutdownReadinessState.md)
 - [Storage](docs/Storage.md)
 - [ValidationState](docs/ValidationState.md)
 - [ZooKeeperServerMode](docs/ZooKeeperServerMode.md)
 - [ApiActivityList](docs/ApiActivityList.md)
 - [ApiAuditList](docs/ApiAuditList.md)
 - [ApiAuthRoleList](docs/ApiAuthRoleList.md)
 - [ApiAuthRoleMetadataList](docs/ApiAuthRoleMetadataList.md)
 - [ApiBatchRequest](docs/ApiBatchRequest.md)
 - [ApiBatchResponse](docs/ApiBatchResponse.md)
 - [ApiClusterList](docs/ApiClusterList.md)
 - [ApiClusterNameList](docs/ApiClusterNameList.md)
 - [ApiCmPeerList](docs/ApiCmPeerList.md)
 - [ApiCommandList](docs/ApiCommandList.md)
 - [ApiCommandMetadataList](docs/ApiCommandMetadataList.md)
 - [ApiConfigList](docs/ApiConfigList.md)
 - [ApiDashboardList](docs/ApiDashboardList.md)
 - [ApiEventQueryResult](docs/ApiEventQueryResult.md)
 - [ApiExternalAccountCategoryList](docs/ApiExternalAccountCategoryList.md)
 - [ApiExternalAccountList](docs/ApiExternalAccountList.md)
 - [ApiExternalAccountTypeList](docs/ApiExternalAccountTypeList.md)
 - [ApiExternalUserMappingList](docs/ApiExternalUserMappingList.md)
 - [ApiHdfsCloudReplicationArguments](docs/ApiHdfsCloudReplicationArguments.md)
 - [ApiHdfsUsageReport](docs/ApiHdfsUsageReport.md)
 - [ApiHiveCloudReplicationArguments](docs/ApiHiveCloudReplicationArguments.md)
 - [ApiHostList](docs/ApiHostList.md)
 - [ApiHostNameList](docs/ApiHostNameList.md)
 - [ApiHostRefList](docs/ApiHostRefList.md)
 - [ApiHostTemplateList](docs/ApiHostTemplateList.md)
 - [ApiImpalaQueryAttributeList](docs/ApiImpalaQueryAttributeList.md)
 - [ApiImpalaTenantUtilizationList](docs/ApiImpalaTenantUtilizationList.md)
 - [ApiImpalaUtilizationHistogramBinList](docs/ApiImpalaUtilizationHistogramBinList.md)
 - [ApiMetricList](docs/ApiMetricList.md)
 - [ApiMetricSchemaList](docs/ApiMetricSchemaList.md)
 - [ApiMrUsageReport](docs/ApiMrUsageReport.md)
 - [ApiNameserviceList](docs/ApiNameserviceList.md)
 - [ApiParcelList](docs/ApiParcelList.md)
 - [ApiPrincipalList](docs/ApiPrincipalList.md)
 - [ApiReplicationCommand](docs/ApiReplicationCommand.md)
 - [ApiReplicationCommandList](docs/ApiReplicationCommandList.md)
 - [ApiReplicationSchedule](docs/ApiReplicationSchedule.md)
 - [ApiReplicationScheduleList](docs/ApiReplicationScheduleList.md)
 - [ApiRoleConfigGroupList](docs/ApiRoleConfigGroupList.md)
 - [ApiRoleList](docs/ApiRoleList.md)
 - [ApiRoleNameList](docs/ApiRoleNameList.md)
 - [ApiRoleTypeList](docs/ApiRoleTypeList.md)
 - [ApiServiceList](docs/ApiServiceList.md)
 - [ApiServiceTypeList](docs/ApiServiceTypeList.md)
 - [ApiSnapshotCommand](docs/ApiSnapshotCommand.md)
 - [ApiSnapshotCommandList](docs/ApiSnapshotCommandList.md)
 - [ApiSnapshotPolicyList](docs/ApiSnapshotPolicyList.md)
 - [ApiTenantUtilizationList](docs/ApiTenantUtilizationList.md)
 - [ApiTimeSeriesEntityAttributeList](docs/ApiTimeSeriesEntityAttributeList.md)
 - [ApiTimeSeriesEntityTypeList](docs/ApiTimeSeriesEntityTypeList.md)
 - [ApiTimeSeriesResponseList](docs/ApiTimeSeriesResponseList.md)
 - [ApiUser2List](docs/ApiUser2List.md)
 - [ApiUserList](docs/ApiUserList.md)
 - [ApiUserSessionList](docs/ApiUserSessionList.md)
 - [ApiWatchedDirList](docs/ApiWatchedDirList.md)
 - [ApiYarnApplicationAttributeList](docs/ApiYarnApplicationAttributeList.md)
 - [ApiYarnTenantUtilizationList](docs/ApiYarnTenantUtilizationList.md)
 - [ApiBulkCommandList](docs/ApiBulkCommandList.md)
 - [ApiRoleTypeConfig](docs/ApiRoleTypeConfig.md)
 - [ApiServiceConfig](docs/ApiServiceConfig.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### basic

- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



