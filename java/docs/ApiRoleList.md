
# ApiRoleList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiRole&gt;**](ApiRole.md) |  |  [optional]



