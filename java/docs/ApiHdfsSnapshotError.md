
# ApiHdfsSnapshotError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** | Path for which the snapshot error occurred. |  [optional]
**snapshotName** | **String** | Name of snapshot for which error occurred. |  [optional]
**error** | **String** | Description of the error. |  [optional]



