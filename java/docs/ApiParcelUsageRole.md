
# ApiParcelUsageRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roleRef** | [**ApiRoleRef**](ApiRoleRef.md) | A reference to the corresponding Role object. |  [optional]
**parcelRefs** | [**List&lt;ApiParcelRef&gt;**](ApiParcelRef.md) | A collection of references to the parcels being used by the role. |  [optional]



