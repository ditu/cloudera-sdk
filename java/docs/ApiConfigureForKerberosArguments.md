
# ApiConfigureForKerberosArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**datanodeTransceiverPort** | [**BigDecimal**](BigDecimal.md) | The HDFS DataNode transceiver port to use. This will be applied to all DataNode role configuration groups. If not specified, this will default to 1004. |  [optional]
**datanodeWebPort** | [**BigDecimal**](BigDecimal.md) | The HDFS DataNode web port to use.  This will be applied to all DataNode role configuration groups. If not specified, this will default to 1006. |  [optional]



