
# ApiHdfsSnapshot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** | Snapshotted path. |  [optional]
**snapshotName** | **String** | Snapshot name. |  [optional]
**snapshotPath** | **String** | Read-only. Fully qualified path for the snapshot version of \&quot;path\&quot;. &lt;p/&gt; For example, if a snapshot \&quot;s1\&quot; is present at \&quot;/a/.snapshot/s1, then the snapshot path corresponding to \&quot;s1\&quot; for path \&quot;/a/b\&quot; will be \&quot;/a/.snapshot/s1/b\&quot;. |  [optional]
**creationTime** | **String** | Snapshot creation time. |  [optional]



