
# ApiClustersPerfInspectorArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceCluster** | **String** | Required name of the source cluster to run network diagnostics test. |  [optional]
**targetCluster** | **String** | Required name of the target cluster to run network diagnostics test. |  [optional]
**pingArgs** | [**ApiPerfInspectorPingArgs**](ApiPerfInspectorPingArgs.md) | Optional ping request arguments. If not specified, default arguments will be used for ping test. |  [optional]



