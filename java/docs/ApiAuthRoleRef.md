
# ApiAuthRoleRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** | The name of the authRole. |  [optional]
**uuid** | **String** | The uuid of the authRole, which uniquely identifies it in a CM installation. |  [optional]



