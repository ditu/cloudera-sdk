
# ApiMr2AppInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jobState** | **String** | The state of the job. This is only set on completed jobs. This can take on the following values: \&quot;NEW\&quot;, \&quot;INITED\&quot;, \&quot;RUNNING\&quot;, \&quot;SUCCEEDED\&quot;, \&quot;FAILED\&quot;, \&quot;KILLED\&quot;, \&quot;ERROR\&quot;. |  [optional]



