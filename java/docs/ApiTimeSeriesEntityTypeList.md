
# ApiTimeSeriesEntityTypeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiTimeSeriesEntityType&gt;**](ApiTimeSeriesEntityType.md) |  |  [optional]



