
# ApiLicense

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner** | **String** | The owner (organization name) of the license. |  [optional]
**uuid** | **String** | A UUID of this license. |  [optional]
**expiration** | **String** | The expiration date. |  [optional]



