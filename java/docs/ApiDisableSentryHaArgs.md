
# ApiDisableSentryHaArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeName** | **String** | Name of the single role that will remain active after HA is disabled. |  [optional]



