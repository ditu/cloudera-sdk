
# ApiParcelList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiParcel&gt;**](ApiParcel.md) |  |  [optional]



