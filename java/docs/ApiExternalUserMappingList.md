
# ApiExternalUserMappingList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiExternalUserMapping&gt;**](ApiExternalUserMapping.md) |  |  [optional]



