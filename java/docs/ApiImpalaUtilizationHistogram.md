
# ApiImpalaUtilizationHistogram

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bins** | [**ApiImpalaUtilizationHistogramBinList**](ApiImpalaUtilizationHistogramBinList.md) | Bins of the histogram. |  [optional]



