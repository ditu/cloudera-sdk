
# ApiHBaseSnapshotError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tableName** | **String** | Name of the table. |  [optional]
**snapshotName** | **String** | Name of the snapshot. |  [optional]
**storage** | [**Storage**](Storage.md) | The location of the snapshot. |  [optional]
**error** | **String** | Description of the error. |  [optional]



