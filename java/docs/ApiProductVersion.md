
# ApiProductVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** |  |  [optional]
**product** | **String** |  |  [optional]



