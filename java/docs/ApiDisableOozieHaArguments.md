
# ApiDisableOozieHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeName** | **String** | Name of the Oozie Server that will be active after HA is disabled. |  [optional]



