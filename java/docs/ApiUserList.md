
# ApiUserList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiUser&gt;**](ApiUser.md) |  |  [optional]



