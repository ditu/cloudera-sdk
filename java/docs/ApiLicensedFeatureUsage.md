
# ApiLicensedFeatureUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totals** | [**Map&lt;String, BigDecimal&gt;**](BigDecimal.md) | Map from named features to the total number of nodes using those features. |  [optional]
**clusters** | **Map&lt;String, Object&gt;** | Map from clusters to maps of named features to the number of nodes in the cluster using those features. |  [optional]



