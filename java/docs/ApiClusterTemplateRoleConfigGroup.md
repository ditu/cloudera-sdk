
# ApiClusterTemplateRoleConfigGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refName** | **String** |  |  [optional]
**roleType** | **String** |  |  [optional]
**base** | **Boolean** |  |  [optional]
**displayName** | **String** |  |  [optional]
**configs** | [**List&lt;ApiClusterTemplateConfig&gt;**](ApiClusterTemplateConfig.md) |  |  [optional]



