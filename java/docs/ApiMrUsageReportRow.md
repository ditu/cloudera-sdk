
# ApiMrUsageReportRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timePeriod** | **String** | The time period over which this report is generated. |  [optional]
**user** | **String** | The user being reported. |  [optional]
**group** | **String** | The group this user belongs to. |  [optional]
**cpuSec** | [**BigDecimal**](BigDecimal.md) | Amount of CPU time (in seconds) taken up this user&#39;s MapReduce jobs. |  [optional]
**memoryBytes** | [**BigDecimal**](BigDecimal.md) | The sum of physical memory used (collected as a snapshot) by this user&#39;s MapReduce jobs. |  [optional]
**jobCount** | [**BigDecimal**](BigDecimal.md) | Number of jobs. |  [optional]
**taskCount** | [**BigDecimal**](BigDecimal.md) | Number of tasks. |  [optional]
**durationSec** | [**BigDecimal**](BigDecimal.md) | Total duration of this user&#39;s MapReduce jobs. |  [optional]
**failedMaps** | [**BigDecimal**](BigDecimal.md) | Failed maps of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**totalMaps** | [**BigDecimal**](BigDecimal.md) | Total maps of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**failedReduces** | [**BigDecimal**](BigDecimal.md) | Failed reduces of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**totalReduces** | [**BigDecimal**](BigDecimal.md) | Total reduces of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**mapInputBytes** | [**BigDecimal**](BigDecimal.md) | Map input bytes of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**mapOutputBytes** | [**BigDecimal**](BigDecimal.md) | Map output bytes of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**hdfsBytesRead** | [**BigDecimal**](BigDecimal.md) | HDFS bytes read of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**hdfsBytesWritten** | [**BigDecimal**](BigDecimal.md) | HDFS bytes written of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**localBytesRead** | [**BigDecimal**](BigDecimal.md) | Local bytes read of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**localBytesWritten** | [**BigDecimal**](BigDecimal.md) | Local bytes written of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**dataLocalMaps** | [**BigDecimal**](BigDecimal.md) | Data local maps of this user&#39;s MapReduce jobs. Available since v11. |  [optional]
**rackLocalMaps** | [**BigDecimal**](BigDecimal.md) | Rack local maps of this user&#39;s MapReduce jobs. Available since v11. |  [optional]



