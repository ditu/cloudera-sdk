
# ApiRoleRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clusterName** | **String** |  |  [optional]
**serviceName** | **String** |  |  [optional]
**roleName** | **String** |  |  [optional]



