
# ApiYarnApplicationDiagnosticsCollectionArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicationIds** | **List&lt;String&gt;** | Id&#39;s of the applications whose diagnostics data has to be collected |  [optional]
**ticketNumber** | **String** | Ticket Number of the Cloudera Support Ticket |  [optional]
**comments** | **String** | Comments to add to the support bundle |  [optional]



