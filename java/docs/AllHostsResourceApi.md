# AllHostsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**readConfig**](AllHostsResourceApi.md#readConfig) | **GET** /cm/allHosts/config | Retrieve the default configuration for all hosts.
[**updateConfig**](AllHostsResourceApi.md#updateConfig) | **PUT** /cm/allHosts/config | Update the default configuration values for all hosts.


<a name="readConfig"></a>
# **readConfig**
> ApiConfigList readConfig(view)

Retrieve the default configuration for all hosts.

Retrieve the default configuration for all hosts. <p/> These values will apply to all hosts managed by CM unless overridden at the host level.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AllHostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AllHostsResourceApi apiInstance = new AllHostsResourceApi();
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.readConfig(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllHostsResourceApi#readConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateConfig"></a>
# **updateConfig**
> ApiConfigList updateConfig(message, body)

Update the default configuration values for all hosts.

Update the default configuration values for all hosts. <p/> Note that this does not override values set at the host level. It just updates the default values that will be inherited by each host's configuration.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AllHostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AllHostsResourceApi apiInstance = new AllHostsResourceApi();
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | The config values to update.
try {
    ApiConfigList result = apiInstance.updateConfig(message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllHostsResourceApi#updateConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| The config values to update. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

