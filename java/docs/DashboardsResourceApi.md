# DashboardsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDashboards**](DashboardsResourceApi.md#createDashboards) | **POST** /timeseries/dashboards | Creates the list of dashboards.
[**deleteDashboard**](DashboardsResourceApi.md#deleteDashboard) | **DELETE** /timeseries/dashboards/{dashboardName} | Deletes a dashboard.
[**getDashboard**](DashboardsResourceApi.md#getDashboard) | **GET** /timeseries/dashboards/{dashboardName} | Returns a dashboard definition for the specified name.
[**getDashboards**](DashboardsResourceApi.md#getDashboards) | **GET** /timeseries/dashboards | Returns the list of all user-customized dashboards.


<a name="createDashboards"></a>
# **createDashboards**
> ApiDashboardList createDashboards(body)

Creates the list of dashboards.

Creates the list of dashboards. If any of the dashboards already exist this whole command will fail and no dashboards will be created. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.DashboardsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

DashboardsResourceApi apiInstance = new DashboardsResourceApi();
ApiDashboardList body = new ApiDashboardList(); // ApiDashboardList | The list of dashboards to create.
try {
    ApiDashboardList result = apiInstance.createDashboards(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DashboardsResourceApi#createDashboards");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiDashboardList**](ApiDashboardList.md)| The list of dashboards to create. | [optional]

### Return type

[**ApiDashboardList**](ApiDashboardList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteDashboard"></a>
# **deleteDashboard**
> ApiDashboard deleteDashboard(dashboardName)

Deletes a dashboard.

Deletes a dashboard.  <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.DashboardsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

DashboardsResourceApi apiInstance = new DashboardsResourceApi();
String dashboardName = "dashboardName_example"; // String | The name of the dashboard.
try {
    ApiDashboard result = apiInstance.deleteDashboard(dashboardName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DashboardsResourceApi#deleteDashboard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboardName** | **String**| The name of the dashboard. |

### Return type

[**ApiDashboard**](ApiDashboard.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDashboard"></a>
# **getDashboard**
> ApiDashboard getDashboard(dashboardName)

Returns a dashboard definition for the specified name.

Returns a dashboard definition for the specified name. This dashboard can be imported with the createDashboards API. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.DashboardsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

DashboardsResourceApi apiInstance = new DashboardsResourceApi();
String dashboardName = "dashboardName_example"; // String | The name of the dashboard.
try {
    ApiDashboard result = apiInstance.getDashboard(dashboardName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DashboardsResourceApi#getDashboard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboardName** | **String**| The name of the dashboard. |

### Return type

[**ApiDashboard**](ApiDashboard.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDashboards"></a>
# **getDashboards**
> ApiDashboardList getDashboards()

Returns the list of all user-customized dashboards.

Returns the list of all user-customized dashboards. This includes both the new dashboards created by users as well as any user customizations to built-in dashboards. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.DashboardsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

DashboardsResourceApi apiInstance = new DashboardsResourceApi();
try {
    ApiDashboardList result = apiInstance.getDashboards();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DashboardsResourceApi#getDashboards");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiDashboardList**](ApiDashboardList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

