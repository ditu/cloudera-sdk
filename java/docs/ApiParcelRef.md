
# ApiParcelRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clusterName** | **String** | The name of the cluster that the parcel is used by. |  [optional]
**parcelName** | **String** | The name of the parcel. |  [optional]
**parcelVersion** | **String** | The version of the parcel. |  [optional]



