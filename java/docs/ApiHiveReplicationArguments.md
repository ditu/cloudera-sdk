
# ApiHiveReplicationArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceService** | [**ApiServiceRef**](ApiServiceRef.md) | The service to replicate from. |  [optional]
**tableFilters** | [**List&lt;ApiHiveTable&gt;**](ApiHiveTable.md) | Filters for tables to include in the replication. Optional. If not provided, include all tables in all databases. |  [optional]
**exportDir** | **String** | Directory, in the HDFS service where the target Hive service&#39;s data is stored, where the export file will be saved. Optional. If not provided, Cloudera Manager will pick a directory for storing the data. |  [optional]
**force** | **Boolean** | Whether to force overwriting of mismatched tables. |  [optional]
**replicateData** | **Boolean** | Whether to replicate table data stored in HDFS. &lt;p/&gt; If set, the \&quot;hdfsArguments\&quot; property must be set to configure the HDFS replication job. |  [optional]
**hdfsArguments** | [**ApiHdfsReplicationArguments**](ApiHdfsReplicationArguments.md) | Arguments for the HDFS replication job. &lt;p/&gt; This must be provided when choosing to replicate table data stored in HDFS. The \&quot;sourceService\&quot;, \&quot;sourcePath\&quot; and \&quot;dryRun\&quot; properties of the HDFS arguments are ignored; their values are derived from the Hive replication&#39;s information. &lt;p/&gt; The \&quot;destinationPath\&quot; property is used slightly differently from the usual HDFS replication jobs. It is used to map the root path of the source service into the target service. It may be omitted, in which case the source and target paths will match. &lt;p/&gt; Example: if the destination path is set to \&quot;/new_root\&quot;, a \&quot;/foo/bar\&quot; path in the source will be stored in \&quot;/new_root/foo/bar\&quot; in the target. |  [optional]
**replicateImpalaMetadata** | **Boolean** | Whether to replicate the impala metadata. (i.e. the metadata for impala UDFs and their corresponding binaries in HDFS). |  [optional]
**runInvalidateMetadata** | **Boolean** | Whether to run invalidate metadata query or not |  [optional]
**dryRun** | **Boolean** | Whether to perform a dry run. Defaults to false. |  [optional]
**numThreads** | [**BigDecimal**](BigDecimal.md) | Number of threads to use in multi-threaded export/import phase |  [optional]



