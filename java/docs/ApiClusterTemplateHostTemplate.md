
# ApiClusterTemplateHostTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refName** | **String** |  |  [optional]
**roleConfigGroupsRefNames** | **List&lt;String&gt;** |  |  [optional]
**cardinality** | [**BigDecimal**](BigDecimal.md) |  |  [optional]



