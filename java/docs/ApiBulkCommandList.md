
# ApiBulkCommandList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiCommand&gt;**](ApiCommand.md) |  |  [optional]
**errors** | **List&lt;String&gt;** | Errors that occurred when issuing individual commands. |  [optional]



