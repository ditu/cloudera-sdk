
# ApiExternalAccountCategoryList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiExternalAccountCategory&gt;**](ApiExternalAccountCategory.md) |  |  [optional]



