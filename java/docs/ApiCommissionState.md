
# ApiCommissionState

## Enum


* `COMMISSIONED` (value: `"COMMISSIONED"`)

* `DECOMMISSIONING` (value: `"DECOMMISSIONING"`)

* `DECOMMISSIONED` (value: `"DECOMMISSIONED"`)

* `UNKNOWN` (value: `"UNKNOWN"`)

* `OFFLINING` (value: `"OFFLINING"`)

* `OFFLINED` (value: `"OFFLINED"`)



