
# ApiHiveReplicationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**database** | **String** | Name of the database. |  [optional]
**tableName** | **String** | Name of the table. |  [optional]
**impalaUDF** | **String** | UDF signature, includes the UDF name and parameter types. |  [optional]
**hiveUDF** | **String** | Hive UDF signature, includes the UDF name and parameter types. |  [optional]
**error** | **String** | Description of the error. |  [optional]



