
# ApiEventCategory

## Enum


* `UNKNOWN` (value: `"UNKNOWN"`)

* `HEALTH_EVENT` (value: `"HEALTH_EVENT"`)

* `LOG_EVENT` (value: `"LOG_EVENT"`)

* `AUDIT_EVENT` (value: `"AUDIT_EVENT"`)

* `ACTIVITY_EVENT` (value: `"ACTIVITY_EVENT"`)

* `HBASE` (value: `"HBASE"`)

* `SYSTEM` (value: `"SYSTEM"`)



