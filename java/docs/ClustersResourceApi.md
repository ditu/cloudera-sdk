# ClustersResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addHosts**](ClustersResourceApi.md#addHosts) | **POST** /clusters/{clusterName}/hosts | 
[**autoAssignRoles**](ClustersResourceApi.md#autoAssignRoles) | **PUT** /clusters/{clusterName}/autoAssignRoles | Automatically assign roles to hosts and create the roles for all the services in a cluster.
[**autoConfigure**](ClustersResourceApi.md#autoConfigure) | **PUT** /clusters/{clusterName}/autoConfigure | Automatically configures roles and services in a cluster.
[**configureForKerberos**](ClustersResourceApi.md#configureForKerberos) | **POST** /clusters/{clusterName}/commands/configureForKerberos | Command to configure the cluster to use Kerberos for authentication.
[**createClusters**](ClustersResourceApi.md#createClusters) | **POST** /clusters | Creates a collection of clusters.
[**deleteCluster**](ClustersResourceApi.md#deleteCluster) | **DELETE** /clusters/{clusterName} | Deletes a cluster.
[**deleteClusterCredentialsCommand**](ClustersResourceApi.md#deleteClusterCredentialsCommand) | **POST** /clusters/{clusterName}/commands/deleteCredentials | Delete existing Kerberos credentials for the cluster.
[**deployClientConfig**](ClustersResourceApi.md#deployClientConfig) | **POST** /clusters/{clusterName}/commands/deployClientConfig | Deploy the cluster-wide client configuration.
[**deployClusterClientConfig**](ClustersResourceApi.md#deployClusterClientConfig) | **POST** /clusters/{clusterName}/commands/deployClusterClientConfig | Deploy the Cluster&#39;s Kerberos client configuration.
[**enterMaintenanceMode**](ClustersResourceApi.md#enterMaintenanceMode) | **POST** /clusters/{clusterName}/commands/enterMaintenanceMode | Put the cluster into maintenance mode.
[**exitMaintenanceMode**](ClustersResourceApi.md#exitMaintenanceMode) | **POST** /clusters/{clusterName}/commands/exitMaintenanceMode | Take the cluster out of maintenance mode.
[**expireLogs**](ClustersResourceApi.md#expireLogs) | **POST** /clusters/{clusterName}/commands/expireLogs | Remove backup and disaster related log files in hdfs.
[**export**](ClustersResourceApi.md#export) | **GET** /clusters/{clusterName}/export | Export the cluster template for the given cluster.
[**firstRun**](ClustersResourceApi.md#firstRun) | **POST** /clusters/{clusterName}/commands/firstRun | Prepare and start services in a cluster.
[**getClientConfig**](ClustersResourceApi.md#getClientConfig) | **GET** /clusters/{clusterName}/clientConfig | Download a zip-compressed archive of the client configuration, of a specific cluster.
[**getKerberosInfo**](ClustersResourceApi.md#getKerberosInfo) | **GET** /clusters/{clusterName}/kerberosInfo | Provides Cluster Kerberos information.
[**getUtilizationReport**](ClustersResourceApi.md#getUtilizationReport) | **GET** /clusters/{clusterName}/utilization | Provides the resource utilization of the entire cluster as well as the resource utilization per tenant.
[**inspectHostsCommand**](ClustersResourceApi.md#inspectHostsCommand) | **POST** /clusters/{clusterName}/commands/inspectHosts | Runs the host inspector on the configured hosts in the specified cluster.
[**listActiveCommands**](ClustersResourceApi.md#listActiveCommands) | **GET** /clusters/{clusterName}/commands | List active cluster commands.
[**listDfsServices**](ClustersResourceApi.md#listDfsServices) | **GET** /clusters/{clusterName}/dfsServices | List the services that can provide distributed file system (DFS) capabilities in this cluster.
[**listHosts**](ClustersResourceApi.md#listHosts) | **GET** /clusters/{clusterName}/hosts | 
[**listServiceTypes**](ClustersResourceApi.md#listServiceTypes) | **GET** /clusters/{clusterName}/serviceTypes | List the supported service types for a cluster.
[**perfInspectorCommand**](ClustersResourceApi.md#perfInspectorCommand) | **POST** /clusters/{clusterName}/commands/perfInspector | Run cluster performance diagnostics test.
[**poolsRefresh**](ClustersResourceApi.md#poolsRefresh) | **POST** /clusters/{clusterName}/commands/poolsRefresh | Updates all refreshable configuration files for services with Dynamic Resource Pools.
[**preUpgradeCheckCommand**](ClustersResourceApi.md#preUpgradeCheckCommand) | **POST** /clusters/{clusterName}/commands/preUpgradeCheck | Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other.
[**readCluster**](ClustersResourceApi.md#readCluster) | **GET** /clusters/{clusterName} | Reads information about a cluster.
[**readClusters**](ClustersResourceApi.md#readClusters) | **GET** /clusters | Lists all known clusters.
[**refresh**](ClustersResourceApi.md#refresh) | **POST** /clusters/{clusterName}/commands/refresh | Updates all refreshable configuration files in the cluster.
[**removeAllHosts**](ClustersResourceApi.md#removeAllHosts) | **DELETE** /clusters/{clusterName}/hosts | 
[**removeHost**](ClustersResourceApi.md#removeHost) | **DELETE** /clusters/{clusterName}/hosts/{hostId} | 
[**restartCommand**](ClustersResourceApi.md#restartCommand) | **POST** /clusters/{clusterName}/commands/restart | Restart all services in the cluster.
[**rollingRestart**](ClustersResourceApi.md#rollingRestart) | **POST** /clusters/{clusterName}/commands/rollingRestart | Command to do a \&quot;best-effort\&quot; rolling restart of the given cluster, i.
[**rollingUpgrade**](ClustersResourceApi.md#rollingUpgrade) | **POST** /clusters/{clusterName}/commands/rollingUpgrade | Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don&#39;t support rolling upgrades.
[**startCommand**](ClustersResourceApi.md#startCommand) | **POST** /clusters/{clusterName}/commands/start | Start all services in the cluster.
[**stopCommand**](ClustersResourceApi.md#stopCommand) | **POST** /clusters/{clusterName}/commands/stop | Stop all services in the cluster.
[**updateCluster**](ClustersResourceApi.md#updateCluster) | **PUT** /clusters/{clusterName} | Update an existing cluster.
[**upgradeCdhCommand**](ClustersResourceApi.md#upgradeCdhCommand) | **POST** /clusters/{clusterName}/commands/upgradeCdh | Perform CDH upgrade to the specified version.
[**upgradeServicesCommand**](ClustersResourceApi.md#upgradeServicesCommand) | **POST** /clusters/{clusterName}/commands/upgradeServices | Upgrades the services in the cluster to the CDH5 version.


<a name="addHosts"></a>
# **addHosts**
> ApiHostRefList addHosts(clusterName, body)





### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | 
ApiHostRefList body = new ApiHostRefList(); // ApiHostRefList | 
try {
    ApiHostRefList result = apiInstance.addHosts(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#addHosts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **body** | [**ApiHostRefList**](ApiHostRefList.md)|  | [optional]

### Return type

[**ApiHostRefList**](ApiHostRefList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="autoAssignRoles"></a>
# **autoAssignRoles**
> autoAssignRoles(clusterName)

Automatically assign roles to hosts and create the roles for all the services in a cluster.

Automatically assign roles to hosts and create the roles for all the services in a cluster. <p> Assignments are done based on services and hosts in the cluster, and hardware specifications. If no hosts are added to the cluster, an exception will be thrown preventing any role assignments. Existing roles will be taken into account and their assignments will be not be modified. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
try {
    apiInstance.autoAssignRoles(clusterName);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#autoAssignRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="autoConfigure"></a>
# **autoConfigure**
> autoConfigure(clusterName)

Automatically configures roles and services in a cluster.

Automatically configures roles and services in a cluster. <p> Overwrites some existing configurations. Might create new role config groups. Only default role config groups must exist before calling this endpoint. Other role config groups must not exist. If they do, an exception will be thrown preventing any configuration. Ignores the Cloudera Management Service even if colocated with roles of this cluster. To avoid over-committing the heap on hosts, assign hosts to this cluster that are not being used by the Cloudera Management Service. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
try {
    apiInstance.autoConfigure(clusterName);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#autoConfigure");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="configureForKerberos"></a>
# **configureForKerberos**
> ApiCommand configureForKerberos(clusterName, body)

Command to configure the cluster to use Kerberos for authentication.

Command to configure the cluster to use Kerberos for authentication.  This command will configure all relevant services on a cluster for Kerberos usage.  This command will trigger a GenerateCredentials command to create Kerberos keytabs for all roles in the cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiConfigureForKerberosArguments body = new ApiConfigureForKerberosArguments(); // ApiConfigureForKerberosArguments | Arguments for the configure for kerberos command.
try {
    ApiCommand result = apiInstance.configureForKerberos(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#configureForKerberos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiConfigureForKerberosArguments**](ApiConfigureForKerberosArguments.md)| Arguments for the configure for kerberos command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createClusters"></a>
# **createClusters**
> ApiClusterList createClusters(body)

Creates a collection of clusters.

Creates a collection of clusters.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
ApiClusterList body = new ApiClusterList(); // ApiClusterList | List of clusters to created.
try {
    ApiClusterList result = apiInstance.createClusters(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#createClusters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiClusterList**](ApiClusterList.md)| List of clusters to created. | [optional]

### Return type

[**ApiClusterList**](ApiClusterList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteCluster"></a>
# **deleteCluster**
> ApiCluster deleteCluster(clusterName)

Deletes a cluster.

Deletes a cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | Name of cluster to delete.
try {
    ApiCluster result = apiInstance.deleteCluster(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#deleteCluster");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| Name of cluster to delete. |

### Return type

[**ApiCluster**](ApiCluster.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteClusterCredentialsCommand"></a>
# **deleteClusterCredentialsCommand**
> ApiCommand deleteClusterCredentialsCommand(clusterName)

Delete existing Kerberos credentials for the cluster.

Delete existing Kerberos credentials for the cluster. <p> This command will affect all services that have been configured to use Kerberos, and have existing credentials.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | cluster name
try {
    ApiCommand result = apiInstance.deleteClusterCredentialsCommand(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#deleteClusterCredentialsCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| cluster name |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deployClientConfig"></a>
# **deployClientConfig**
> ApiCommand deployClientConfig(clusterName)

Deploy the cluster-wide client configuration.

Deploy the cluster-wide client configuration.  <p>For each service in the cluster, deploy the service's client configuration to all the hosts that the service runs on.</p>  <p>Available since API v2.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
try {
    ApiCommand result = apiInstance.deployClientConfig(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#deployClientConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deployClusterClientConfig"></a>
# **deployClusterClientConfig**
> ApiCommand deployClusterClientConfig(clusterName, body)

Deploy the Cluster's Kerberos client configuration.

Deploy the Cluster's Kerberos client configuration.  <p> Deploy krb5.conf to hosts in a cluster. Does not deploy to decommissioned hosts or hosts with active processes. </p>  <p> Available since API v7. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
ApiHostRefList body = new ApiHostRefList(); // ApiHostRefList | Hosts in cluster to deploy to. If empty, will target all eligible hosts in the cluster.
try {
    ApiCommand result = apiInstance.deployClusterClientConfig(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#deployClusterClientConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |
 **body** | [**ApiHostRefList**](ApiHostRefList.md)| Hosts in cluster to deploy to. If empty, will target all eligible hosts in the cluster. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enterMaintenanceMode"></a>
# **enterMaintenanceMode**
> ApiCommand enterMaintenanceMode(clusterName)

Put the cluster into maintenance mode.

Put the cluster into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v2.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
try {
    ApiCommand result = apiInstance.enterMaintenanceMode(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#enterMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaintenanceMode"></a>
# **exitMaintenanceMode**
> ApiCommand exitMaintenanceMode(clusterName)

Take the cluster out of maintenance mode.

Take the cluster out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v2.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
try {
    ApiCommand result = apiInstance.exitMaintenanceMode(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#exitMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="expireLogs"></a>
# **expireLogs**
> ApiCommand expireLogs(clusterName, days)

Remove backup and disaster related log files in hdfs.

Remove backup and disaster related log files in hdfs. <p> Available since API v31.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
BigDecimal days = new BigDecimal(); // BigDecimal | logs more than these many days old are purged. -2 to use the existing setting
try {
    ApiCommand result = apiInstance.expireLogs(clusterName, days);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#expireLogs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |
 **days** | **BigDecimal**| logs more than these many days old are purged. -2 to use the existing setting | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="export"></a>
# **export**
> ApiClusterTemplate export(clusterName, exportAutoConfig)

Export the cluster template for the given cluster.

Export the cluster template for the given cluster. If cluster does not have host templates defined it will export host templates based on roles assignment.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | cluster name
Boolean exportAutoConfig = false; // Boolean | export configs set by the auto configuration
try {
    ApiClusterTemplate result = apiInstance.export(clusterName, exportAutoConfig);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#export");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| cluster name |
 **exportAutoConfig** | **Boolean**| export configs set by the auto configuration | [optional] [default to false]

### Return type

[**ApiClusterTemplate**](ApiClusterTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="firstRun"></a>
# **firstRun**
> ApiCommand firstRun(clusterName)

Prepare and start services in a cluster.

Prepare and start services in a cluster.  <p> Perform all the steps needed to prepare each service in a cluster and start the services in order. </p>  <p> Available since API v7. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
try {
    ApiCommand result = apiInstance.firstRun(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#firstRun");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getClientConfig"></a>
# **getClientConfig**
> File getClientConfig(clusterName)

Download a zip-compressed archive of the client configuration, of a specific cluster.

Download a zip-compressed archive of the client configuration, of a specific cluster. Currently, this only includes Kerberos Client Configuration (krb5.conf). For client configuration of services, use the clientConfig endpoint of the services resource. This resource does not require any authentication.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The cluster name.
try {
    File result = apiInstance.getClientConfig(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#getClientConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The cluster name. |

### Return type

[**File**](File.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getKerberosInfo"></a>
# **getKerberosInfo**
> ApiKerberosInfo getKerberosInfo(clusterName)

Provides Cluster Kerberos information.

Provides Cluster Kerberos information

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The cluster name.
try {
    ApiKerberosInfo result = apiInstance.getKerberosInfo(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#getKerberosInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The cluster name. |

### Return type

[**ApiKerberosInfo**](ApiKerberosInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUtilizationReport"></a>
# **getUtilizationReport**
> ApiClusterUtilization getUtilizationReport(clusterName, daysOfWeek, endHourOfDay, from, startHourOfDay, tenantType, to)

Provides the resource utilization of the entire cluster as well as the resource utilization per tenant.

Provides the resource utilization of the entire cluster as well as the resource utilization per tenant. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | cluster name
List<String> daysOfWeek = Arrays.asList("daysOfWeek_example"); // List<String> | The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Monday, and 7 corrensponds to Sunday. All 7 days are included if this is not specified.
Integer endHourOfDay = 23; // Integer | The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified.
String from = "from_example"; // String | Start of the time range to report utilization in ISO 8601 format.
Integer startHourOfDay = 0; // Integer | The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified.
String tenantType = "POOL"; // String | The type of the tenant (POOL or USER).
String to = "now"; // String | End of the the time range to report utilization in ISO 8601 format (defaults to now).
try {
    ApiClusterUtilization result = apiInstance.getUtilizationReport(clusterName, daysOfWeek, endHourOfDay, from, startHourOfDay, tenantType, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#getUtilizationReport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| cluster name |
 **daysOfWeek** | [**List&lt;String&gt;**](String.md)| The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Monday, and 7 corrensponds to Sunday. All 7 days are included if this is not specified. | [optional]
 **endHourOfDay** | **Integer**| The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. | [optional] [default to 23]
 **from** | **String**| Start of the time range to report utilization in ISO 8601 format. | [optional]
 **startHourOfDay** | **Integer**| The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. | [optional] [default to 0]
 **tenantType** | **String**| The type of the tenant (POOL or USER). | [optional] [default to POOL]
 **to** | **String**| End of the the time range to report utilization in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiClusterUtilization**](ApiClusterUtilization.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="inspectHostsCommand"></a>
# **inspectHostsCommand**
> ApiCommand inspectHostsCommand(clusterName)

Runs the host inspector on the configured hosts in the specified cluster.

Runs the host inspector on the configured hosts in the specified cluster.  Available since V8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
try {
    ApiCommand result = apiInstance.inspectHostsCommand(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#inspectHostsCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listActiveCommands"></a>
# **listActiveCommands**
> ApiCommandList listActiveCommands(clusterName, view)

List active cluster commands.

List active cluster commands.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiCommandList result = apiInstance.listActiveCommands(clusterName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#listActiveCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listDfsServices"></a>
# **listDfsServices**
> ApiServiceList listDfsServices(clusterName, view)

List the services that can provide distributed file system (DFS) capabilities in this cluster.

List the services that can provide distributed file system (DFS) capabilities in this cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | cluster name
String view = "summary"; // String | data view required for matching services
try {
    ApiServiceList result = apiInstance.listDfsServices(clusterName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#listDfsServices");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| cluster name |
 **view** | **String**| data view required for matching services | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiServiceList**](ApiServiceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listHosts"></a>
# **listHosts**
> ApiHostRefList listHosts(clusterName)





### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | 
try {
    ApiHostRefList result = apiInstance.listHosts(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#listHosts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |

### Return type

[**ApiHostRefList**](ApiHostRefList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listServiceTypes"></a>
# **listServiceTypes**
> ApiServiceTypeList listServiceTypes(clusterName)

List the supported service types for a cluster.

List the supported service types for a cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The cluster.
try {
    ApiServiceTypeList result = apiInstance.listServiceTypes(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#listServiceTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The cluster. |

### Return type

[**ApiServiceTypeList**](ApiServiceTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="perfInspectorCommand"></a>
# **perfInspectorCommand**
> ApiCommand perfInspectorCommand(clusterName, body)

Run cluster performance diagnostics test.

Run cluster performance diagnostics test.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiClusterPerfInspectorArgs body = new ApiClusterPerfInspectorArgs(); // ApiClusterPerfInspectorArgs | Optional arguments for the command. See ApiClusterPerfInspectorArgs.
try {
    ApiCommand result = apiInstance.perfInspectorCommand(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#perfInspectorCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiClusterPerfInspectorArgs**](ApiClusterPerfInspectorArgs.md)| Optional arguments for the command. See ApiClusterPerfInspectorArgs. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="poolsRefresh"></a>
# **poolsRefresh**
> ApiCommand poolsRefresh(clusterName)

Updates all refreshable configuration files for services with Dynamic Resource Pools.

Updates all refreshable configuration files for services with Dynamic Resource Pools. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
try {
    ApiCommand result = apiInstance.poolsRefresh(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#poolsRefresh");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="preUpgradeCheckCommand"></a>
# **preUpgradeCheckCommand**
> ApiCommand preUpgradeCheckCommand(clusterName, body)

Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other.

Run cluster pre-upgrade check(s) when upgrading from specified version of CDH to the other. <p> Allows the following upgrade checks: <ul> <li>Minor upgrades from any CDH 5 to any CDH 5</li> <li>Major upgrades from any CDH 5 to any CDH 6</li> <li>Minor upgrades from any CDH 6 to any CDH 6</li> <li>Maintenance upgrades or downgrades (a.b.x to a.b.y)</li> </ul>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiCdhUpgradeArgs body = new ApiCdhUpgradeArgs(); // ApiCdhUpgradeArgs | Arguments for the command. See ApiCdhUpgradeArgs.
try {
    ApiCommand result = apiInstance.preUpgradeCheckCommand(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#preUpgradeCheckCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiCdhUpgradeArgs**](ApiCdhUpgradeArgs.md)| Arguments for the command. See ApiCdhUpgradeArgs. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="readCluster"></a>
# **readCluster**
> ApiCluster readCluster(clusterName)

Reads information about a cluster.

Reads information about a cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | Name of cluster to look up.
try {
    ApiCluster result = apiInstance.readCluster(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#readCluster");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| Name of cluster to look up. |

### Return type

[**ApiCluster**](ApiCluster.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readClusters"></a>
# **readClusters**
> ApiClusterList readClusters(view)

Lists all known clusters.

Lists all known clusters.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String view = "summary"; // String | 
try {
    ApiClusterList result = apiInstance.readClusters(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#readClusters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiClusterList**](ApiClusterList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="refresh"></a>
# **refresh**
> ApiCommand refresh(clusterName)

Updates all refreshable configuration files in the cluster.

Updates all refreshable configuration files in the cluster. Will not restart any roles. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
try {
    ApiCommand result = apiInstance.refresh(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#refresh");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="removeAllHosts"></a>
# **removeAllHosts**
> ApiHostRefList removeAllHosts(clusterName)





### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | 
try {
    ApiHostRefList result = apiInstance.removeAllHosts(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#removeAllHosts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |

### Return type

[**ApiHostRefList**](ApiHostRefList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="removeHost"></a>
# **removeHost**
> ApiHostRef removeHost(clusterName, hostId)





### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | 
String hostId = "hostId_example"; // String | 
try {
    ApiHostRef result = apiInstance.removeHost(clusterName, hostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#removeHost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **hostId** | **String**|  |

### Return type

[**ApiHostRef**](ApiHostRef.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="restartCommand"></a>
# **restartCommand**
> ApiCommand restartCommand(clusterName, body)

Restart all services in the cluster.

Restart all services in the cluster. <p> Services are stopped then started in the appropriate order given their dependencies. The command can optionally restart only stale services and their dependencies as well as redeploy client configuration on all services in the cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiRestartClusterArgs body = new ApiRestartClusterArgs(); // ApiRestartClusterArgs | arguments for the restart command.
try {
    ApiCommand result = apiInstance.restartCommand(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#restartCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiRestartClusterArgs**](ApiRestartClusterArgs.md)| arguments for the restart command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rollingRestart"></a>
# **rollingRestart**
> ApiCommand rollingRestart(clusterName, body)

Command to do a \"best-effort\" rolling restart of the given cluster, i.

Command to do a \"best-effort\" rolling restart of the given cluster, i.e. it does plain restart of services that cannot be rolling restarted, followed by first rolling restarting non-slaves and then rolling restarting the slave roles of services that can be rolling restarted. The slave restarts are done host-by-host. <p> Available since API v4. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiRollingRestartClusterArgs body = new ApiRollingRestartClusterArgs(); // ApiRollingRestartClusterArgs | Arguments for the rolling restart command.
try {
    ApiCommand result = apiInstance.rollingRestart(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#rollingRestart");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiRollingRestartClusterArgs**](ApiRollingRestartClusterArgs.md)| Arguments for the rolling restart command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rollingUpgrade"></a>
# **rollingUpgrade**
> ApiCommand rollingUpgrade(clusterName, body)

Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don't support rolling upgrades.

Command to do a rolling upgrade of specific services in the given cluster  This command does not handle any services that don't support rolling upgrades. The command will throw an error and not start if upgrade of any such service is requested.  This command does not upgrade the full CDH Cluster. You should normally use the upgradeCDH Command for upgrading the cluster. This is primarily helpful if you need to need to recover from an upgrade failure or for advanced users to script an alternative to the upgradeCdhCommand.  This command expects the binaries to be available on hosts and activated. It does not change any binaries on the hosts.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiRollingUpgradeServicesArgs body = new ApiRollingUpgradeServicesArgs(); // ApiRollingUpgradeServicesArgs | Arguments for the rolling upgrade command.
try {
    ApiCommand result = apiInstance.rollingUpgrade(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#rollingUpgrade");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiRollingUpgradeServicesArgs**](ApiRollingUpgradeServicesArgs.md)| Arguments for the rolling upgrade command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="startCommand"></a>
# **startCommand**
> ApiCommand startCommand(clusterName)

Start all services in the cluster.

Start all services in the cluster. <p> Services are started in the appropriate order given their dependencies.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
try {
    ApiCommand result = apiInstance.startCommand(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#startCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stopCommand"></a>
# **stopCommand**
> ApiCommand stopCommand(clusterName)

Stop all services in the cluster.

Stop all services in the cluster. <p> Services are stopped in the appropriate order given their dependencies.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
try {
    ApiCommand result = apiInstance.stopCommand(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#stopCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateCluster"></a>
# **updateCluster**
> ApiCluster updateCluster(clusterName, body)

Update an existing cluster.

Update an existing cluster. <p> To update the CDH version, provide the new value in the \"fullVersion\" property. Setting a correct version that matches the actual installed software will ensure the correct version-specific features, such as services, roles, commands, and configurations. This need not be manually set for clusters using parcels. In general this action is only necessary after the CDH packages have been manually updated. Note that a downgrade may be rejected if it would render existing services or roles unusable. For major upgrade, the \"upgradeService\" cluster command should be used instead.</p> <p> To rename the cluster, provide the new name in the \"displayName\" property for API >= 6, or in the \"name\" property for API <=5. <p> Available since API v2.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiCluster body = new ApiCluster(); // ApiCluster | 
try {
    ApiCluster result = apiInstance.updateCluster(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#updateCluster");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiCluster**](ApiCluster.md)|  | [optional]

### Return type

[**ApiCluster**](ApiCluster.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="upgradeCdhCommand"></a>
# **upgradeCdhCommand**
> ApiCommand upgradeCdhCommand(clusterName, body)

Perform CDH upgrade to the specified version.

Perform CDH upgrade to the specified version. <p> Allows the following upgrades: <ul> <li>Major upgrades from any CDH 4 to any CDH 5</li> <li>Minor upgrades from any CDH 5 to any CDH 5</li> <li>Maintenance upgrades or downgrades (a.b.x to a.b.y)</li> </ul> <p> If using packages, CDH packages on all hosts of the cluster must be manually upgraded before this command is issued. <p> The command will upgrade the services and their configuration to the version available in the CDH5 distribution. <p> Unless rolling upgrade options are provided, the entire cluster will experience downtime. If rolling upgrade options are provided, command will do a \"best-effort\" rolling upgrade of the given cluster, i.e. it does plain upgrade of services that cannot be rolling upgraded, followed by first rolling upgrading non-slaves and then rolling restarting the slave roles of services that can be rolling restarted. The slave restarts are done host-by-host. <p> Available since v9. Rolling upgrade is only available with Cloudera Manager Enterprise Edition. A more limited upgrade variant available since v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
ApiCdhUpgradeArgs body = new ApiCdhUpgradeArgs(); // ApiCdhUpgradeArgs | Arguments for the command. See ApiCdhUpgradeArgs.
try {
    ApiCommand result = apiInstance.upgradeCdhCommand(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#upgradeCdhCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |
 **body** | [**ApiCdhUpgradeArgs**](ApiCdhUpgradeArgs.md)| Arguments for the command. See ApiCdhUpgradeArgs. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="upgradeServicesCommand"></a>
# **upgradeServicesCommand**
> ApiCommand upgradeServicesCommand(clusterName)

Upgrades the services in the cluster to the CDH5 version.

Upgrades the services in the cluster to the CDH5 version. <p> This command requires that the CDH packages in the hosts used by the cluster be upgraded to CDH5 before this command is issued. Once issued, this command will stop all running services before proceeding. <p> If parcels are used instead of CDH system packages then the following steps need to happen in order: <ol> <li>Stop all services manually</li> <li>Activate parcel</li> <li>Run this upgrade command</li> </ol> The command will upgrade the services and their configuration to the version available in the CDH5 distribution.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClustersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClustersResourceApi apiInstance = new ClustersResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster.
try {
    ApiCommand result = apiInstance.upgradeServicesCommand(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClustersResourceApi#upgradeServicesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

