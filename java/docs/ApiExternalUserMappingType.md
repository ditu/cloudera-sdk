
# ApiExternalUserMappingType

## Enum


* `LDAP` (value: `"LDAP"`)

* `SAML_SCRIPT` (value: `"SAML_SCRIPT"`)

* `SAML_ATTRIBUTE` (value: `"SAML_ATTRIBUTE"`)

* `EXTERNAL_PROGRAM` (value: `"EXTERNAL_PROGRAM"`)



