
# ApiNameservice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the nameservice. |  [optional]
**active** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the active NameNode. |  [optional]
**activeFailoverController** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the active NameNode&#39;s failover controller, if configured. |  [optional]
**standBy** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the stand-by NameNode. |  [optional]
**standByFailoverController** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the stand-by NameNode&#39;s failover controller, if configured. |  [optional]
**secondary** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the SecondaryNameNode. |  [optional]
**mountPoints** | **List&lt;String&gt;** | Mount points assigned to this nameservice in a federation. |  [optional]
**healthSummary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Requires \&quot;full\&quot; view. The high-level health status of this nameservice. |  [optional]
**healthChecks** | [**List&lt;ApiHealthCheck&gt;**](ApiHealthCheck.md) | Requires \&quot;full\&quot; view. List of health checks performed on the nameservice. |  [optional]



