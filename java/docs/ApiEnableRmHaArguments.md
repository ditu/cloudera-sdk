
# ApiEnableRmHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newRmHostId** | **String** | Id of host on which second ResourceManager role will be added. |  [optional]
**newRmRoleName** | **String** | Name of the second ResourceManager role to be created (Optional) |  [optional]
**zkServiceName** | **String** | Name of the ZooKeeper service that will be used for auto-failover. This is an optional parameter if the Yarn to ZooKeeper dependency is already set in CM. |  [optional]



