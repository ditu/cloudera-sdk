
# ApiActivityList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiActivity&gt;**](ApiActivity.md) |  |  [optional]



