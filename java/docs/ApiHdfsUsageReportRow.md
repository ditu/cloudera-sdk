
# ApiHdfsUsageReportRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **String** | The date of the report row data. |  [optional]
**user** | **String** | The user being reported. |  [optional]
**size** | [**BigDecimal**](BigDecimal.md) | Total size (in bytes) of the files owned by this user. This does not include replication in HDFS. |  [optional]
**rawSize** | [**BigDecimal**](BigDecimal.md) | Total size (in bytes) of all the replicas of all the files owned by this user. |  [optional]
**numFiles** | [**BigDecimal**](BigDecimal.md) | Number of files owned by this user. |  [optional]



