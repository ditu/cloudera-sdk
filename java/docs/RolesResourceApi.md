# RolesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bulkDeleteRoles**](RolesResourceApi.md#bulkDeleteRoles) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/bulkDelete | Bulk delete roles in a particular service by name.
[**createRoles**](RolesResourceApi.md#createRoles) | **POST** /clusters/{clusterName}/services/{serviceName}/roles | Create new roles in a given service.
[**deleteRole**](RolesResourceApi.md#deleteRole) | **DELETE** /clusters/{clusterName}/services/{serviceName}/roles/{roleName} | Deletes a role from a given service.
[**enterMaintenanceMode**](RolesResourceApi.md#enterMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/enterMaintenanceMode | Put the role into maintenance mode.
[**exitMaintenanceMode**](RolesResourceApi.md#exitMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/exitMaintenanceMode | Take the role out of maintenance mode.
[**getFullLog**](RolesResourceApi.md#getFullLog) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/full | Retrieves the log file for the role&#39;s main process.
[**getMetrics**](RolesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/metrics | Fetch metric readings for a particular role.
[**getStacksLog**](RolesResourceApi.md#getStacksLog) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stacks | Retrieves the stacks log file, if any, for the role&#39;s main process.
[**getStacksLogsBundle**](RolesResourceApi.md#getStacksLogsBundle) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stacksBundle | Download a zip-compressed archive of role stacks logs.
[**getStandardError**](RolesResourceApi.md#getStandardError) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stderr | Retrieves the role&#39;s standard error output.
[**getStandardOutput**](RolesResourceApi.md#getStandardOutput) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/logs/stdout | Retrieves the role&#39;s standard output.
[**impalaDiagnostics**](RolesResourceApi.md#impalaDiagnostics) | **POST** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands/impalaDiagnostics | Collects diagnostics data for an Impala role.
[**listActiveCommands**](RolesResourceApi.md#listActiveCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commands | List active role commands.
[**listCommands**](RolesResourceApi.md#listCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/commandsByName | Lists all the commands that can be executed by name on the provided role.
[**readRole**](RolesResourceApi.md#readRole) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName} | Retrieves detailed information about a role.
[**readRoleConfig**](RolesResourceApi.md#readRoleConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/config | Retrieves the configuration of a specific role.
[**readRoles**](RolesResourceApi.md#readRoles) | **GET** /clusters/{clusterName}/services/{serviceName}/roles | Lists all roles of a given service.
[**updateRoleConfig**](RolesResourceApi.md#updateRoleConfig) | **PUT** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/config | Updates the role configuration with the given values.


<a name="bulkDeleteRoles"></a>
# **bulkDeleteRoles**
> ApiRoleList bulkDeleteRoles(clusterName, serviceName, body)

Bulk delete roles in a particular service by name.

Bulk delete roles in a particular service by name. Fails if any role cannot be found.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | list of role names to be deleted
try {
    ApiRoleList result = apiInstance.bulkDeleteRoles(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#bulkDeleteRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| list of role names to be deleted | [optional]

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createRoles"></a>
# **createRoles**
> ApiRoleList createRoles(clusterName, serviceName, body)

Create new roles in a given service.

Create new roles in a given service.  <table> <thead> <tr> <th>Service Type</th> <th>Available Role Types</th> </tr> </thead> <tbody> <tr> <td>HDFS (CDH3)</td> <td>NAMENODE, DATANODE, SECONDARYNAMENODE, BALANCER, GATEWAY</td> </tr> <tr> <td>HDFS (CDH4)</td> <td>NAMENODE, DATANODE, SECONDARYNAMENODE, BALANCER, HTTPFS, FAILOVERCONTROLLER, GATEWAY, JOURNALNODE</td> </tr> <tr> <td>HDFS (CDH5)</td> <td>NAMENODE, DATANODE, SECONDARYNAMENODE, BALANCER, HTTPFS, FAILOVERCONTROLLER, GATEWAY, JOURNALNODE, NFSGATEWAY</td> </tr> <td>MAPREDUCE</td> <td>JOBTRACKER, TASKTRACKER, GATEWAY, FAILOVERCONTROLLER,</td> </tr> <td>HBASE</td> <td>MASTER, REGIONSERVER, GATEWAY, HBASETHRIFTSERVER, HBASERESTSERVER</td> </tr> <tr> <td>YARN</td> <td>RESOURCEMANAGER, NODEMANAGER, JOBHISTORY, GATEWAY</td> </tr> <tr> <td>OOZIE</td> <td>OOZIE_SERVER</td> </tr> <tr> <td>ZOOKEEPER</td> <td>SERVER</td> </tr> <tr> <td>HUE (CDH3)</td> <td>HUE_SERVER, BEESWAX_SERVER, KT_RENEWER, JOBSUBD</td> </tr> <tr> <td>HUE (CDH4)</td> <td>HUE_SERVER, BEESWAX_SERVER, KT_RENEWER</td> </tr> <tr> <td>HUE (CDH5)</td> <td>HUE_SERVER, KT_RENEWER</td> </tr> <tr> <td>HUE (CDH5 5.5+)</td> <td>HUE_SERVER, KT_RENEWER, HUE_LOAD_BALANCER</td> </tr> <tr> <td>FLUME</td> <td>AGENT</td> </tr> <tr> <td>IMPALA (CDH4)</td> <td>IMPALAD, STATESTORE, CATALOGSERVER</td> </tr> <tr> <td>IMPALA (CDH5)</td> <td>IMPALAD, STATESTORE, CATALOGSERVER</td> </tr> <tr> <td>HIVE</td> <td>HIVESERVER2, HIVEMETASTORE, WEBHCAT, GATEWAY</td> </tr> <tr> <td>SOLR</td> <td>SOLR_SERVER, GATEWAY</td> </tr> <tr> <td>SQOOP</td> <td>SQOOP_SERVER</td> </tr> <tr> <td>SQOOP_CLIENT</td> <td>GATEWAY</td> </tr> <tr> <td>SENTRY</td> <td>SENTRY_SERVER</td> </tr> <tr> <td>ACCUMULO16</td> <td>GARBAGE_COLLECTOR, GATEWAY, ACCUMULO16_MASTER, MONITOR, ACCUMULO16_TSERVER, TRACER</td> </tr> <tr> <td>KMS</td> <td>KMS</td> </tr> <tr> <td>KS_INDEXER</td> <td>HBASE_INDEXER</td> </tr> <tr> <td>SPARK_ON_YARN</td> <td>GATEWAY, SPARK_YARN_HISTORY_SERVER</td> </tr> </tbody>  </table>  When specifying roles to be created, the names provided for each role must not conflict with the names that CM auto-generates for roles. Specifically, names of the form \"<service name>-<role type>-<arbitrary value>\" cannot be used unless the <arbitrary value> is the same one CM would use. If CM detects such a conflict, the error message will indicate what <arbitrary value> is safe to use. Alternately, a differently formatted name should be used.  Since API v6: The role name can be left blank to allow CM to generate the name.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleList body = new ApiRoleList(); // ApiRoleList | Roles to create.
try {
    ApiRoleList result = apiInstance.createRoles(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#createRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleList**](ApiRoleList.md)| Roles to create. | [optional]

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteRole"></a>
# **deleteRole**
> ApiRole deleteRole(clusterName, roleName, serviceName)

Deletes a role from a given service.

Deletes a role from a given service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role name.
String serviceName = "serviceName_example"; // String | 
try {
    ApiRole result = apiInstance.deleteRole(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#deleteRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role name. |
 **serviceName** | **String**|  |

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="enterMaintenanceMode"></a>
# **enterMaintenanceMode**
> ApiCommand enterMaintenanceMode(clusterName, roleName, serviceName)

Put the role into maintenance mode.

Put the role into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role name.
String serviceName = "serviceName_example"; // String | 
try {
    ApiCommand result = apiInstance.enterMaintenanceMode(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#enterMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role name. |
 **serviceName** | **String**|  |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaintenanceMode"></a>
# **exitMaintenanceMode**
> ApiCommand exitMaintenanceMode(clusterName, roleName, serviceName)

Take the role out of maintenance mode.

Take the role out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role name.
String serviceName = "serviceName_example"; // String | 
try {
    ApiCommand result = apiInstance.exitMaintenanceMode(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#exitMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role name. |
 **serviceName** | **String**|  |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFullLog"></a>
# **getFullLog**
> String getFullLog(clusterName, roleName, serviceName)

Retrieves the log file for the role's main process.

Retrieves the log file for the role's main process. <p> If the role is not started, this will be the log file associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to fetch logs from.
String serviceName = "serviceName_example"; // String | 
try {
    String result = apiInstance.getFullLog(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#getFullLog");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to fetch logs from. |
 **serviceName** | **String**|  |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getMetrics"></a>
# **getMetrics**
> ApiMetricList getMetrics(clusterName, roleName, serviceName, from, metrics, to, view)

Fetch metric readings for a particular role.

Fetch metric readings for a particular role. <p> By default, this call will look up all metrics available for the role. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the role, even if no readings are available in the requested window.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The name of the role.
String serviceName = "serviceName_example"; // String | 
String from = "from_example"; // String | Start of the period to query.
List<String> metrics = Arrays.asList("metrics_example"); // List<String> | Filter for which metrics to query.
String to = "now"; // String | End of the period to query.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiMetricList result = apiInstance.getMetrics(clusterName, roleName, serviceName, from, metrics, to, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#getMetrics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The name of the role. |
 **serviceName** | **String**|  |
 **from** | **String**| Start of the period to query. | [optional]
 **metrics** | [**List&lt;String&gt;**](String.md)| Filter for which metrics to query. | [optional]
 **to** | **String**| End of the period to query. | [optional] [default to now]
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getStacksLog"></a>
# **getStacksLog**
> String getStacksLog(clusterName, roleName, serviceName)

Retrieves the stacks log file, if any, for the role's main process.

Retrieves the stacks log file, if any, for the role's main process. Note that not all roles support periodic stacks collection.  The log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to fetch stacks logs from.
String serviceName = "serviceName_example"; // String | 
try {
    String result = apiInstance.getStacksLog(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#getStacksLog");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to fetch stacks logs from. |
 **serviceName** | **String**|  |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getStacksLogsBundle"></a>
# **getStacksLogsBundle**
> getStacksLogsBundle(clusterName, roleName, serviceName)

Download a zip-compressed archive of role stacks logs.

Download a zip-compressed archive of role stacks logs. Note that not all roles support periodic stacks collection.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to fetch the stacks logs bundle from.
String serviceName = "serviceName_example"; // String | 
try {
    apiInstance.getStacksLogsBundle(clusterName, roleName, serviceName);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#getStacksLogsBundle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to fetch the stacks logs bundle from. |
 **serviceName** | **String**|  |

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getStandardError"></a>
# **getStandardError**
> String getStandardError(clusterName, roleName, serviceName)

Retrieves the role's standard error output.

Retrieves the role's standard error output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to fetch stderr from.
String serviceName = "serviceName_example"; // String | 
try {
    String result = apiInstance.getStandardError(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#getStandardError");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to fetch stderr from. |
 **serviceName** | **String**|  |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getStandardOutput"></a>
# **getStandardOutput**
> String getStandardOutput(clusterName, roleName, serviceName)

Retrieves the role's standard output.

Retrieves the role's standard output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to fetch stdout from.
String serviceName = "serviceName_example"; // String | 
try {
    String result = apiInstance.getStandardOutput(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#getStandardOutput");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to fetch stdout from. |
 **serviceName** | **String**|  |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="impalaDiagnostics"></a>
# **impalaDiagnostics**
> ApiCommand impalaDiagnostics(clusterName, roleName, serviceName, body)

Collects diagnostics data for an Impala role.

Collects diagnostics data for an Impala role.  <p> Available since API v31. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role name.
String serviceName = "serviceName_example"; // String | 
ApiImpalaRoleDiagnosticsArgs body = new ApiImpalaRoleDiagnosticsArgs(); // ApiImpalaRoleDiagnosticsArgs | 
try {
    ApiCommand result = apiInstance.impalaDiagnostics(clusterName, roleName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#impalaDiagnostics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role name. |
 **serviceName** | **String**|  |
 **body** | [**ApiImpalaRoleDiagnosticsArgs**](ApiImpalaRoleDiagnosticsArgs.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listActiveCommands"></a>
# **listActiveCommands**
> ApiCommandList listActiveCommands(clusterName, roleName, serviceName, view)

List active role commands.

List active role commands.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to start.
String serviceName = "serviceName_example"; // String | 
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiCommandList result = apiInstance.listActiveCommands(clusterName, roleName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#listActiveCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to start. |
 **serviceName** | **String**|  |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listCommands"></a>
# **listCommands**
> ApiCommandMetadataList listCommands(clusterName, roleName, serviceName)

Lists all the commands that can be executed by name on the provided role.

Lists all the commands that can be executed by name on the provided role.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | the role name.
String serviceName = "serviceName_example"; // String | 
try {
    ApiCommandMetadataList result = apiInstance.listCommands(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#listCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| the role name. |
 **serviceName** | **String**|  |

### Return type

[**ApiCommandMetadataList**](ApiCommandMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRole"></a>
# **readRole**
> ApiRole readRole(clusterName, roleName, serviceName, view)

Retrieves detailed information about a role.

Retrieves detailed information about a role.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role name.
String serviceName = "serviceName_example"; // String | 
String view = "full"; // String | The view to materialize. Defaults to 'full'.
try {
    ApiRole result = apiInstance.readRole(clusterName, roleName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#readRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role name. |
 **serviceName** | **String**|  |
 **view** | **String**| The view to materialize. Defaults to 'full'. | [optional] [default to full] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoleConfig"></a>
# **readRoleConfig**
> ApiConfigList readRoleConfig(clusterName, roleName, serviceName, view)

Retrieves the configuration of a specific role.

Retrieves the configuration of a specific role. Note that the \"full\" view performs validation on the configuration, which could take a few seconds on a large cluster (around 500 nodes or more).

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to look up.
String serviceName = "serviceName_example"; // String | 
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.readRoleConfig(clusterName, roleName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#readRoleConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to look up. |
 **serviceName** | **String**|  |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoles"></a>
# **readRoles**
> ApiRoleList readRoles(clusterName, serviceName, filter, view)

Lists all roles of a given service.

Lists all roles of a given service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
String filter = ""; // String | Optional query to filter the roles by. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: hostname==host1.abc.com;type==DATANODE </p>  Currently supports filtering by: <ul> <li>hostname: The hostname of the host the role is running on.</li> <li>hostId: The unique identifier of the host the role is running on.</li> <li>type: The role's type.</li> </ul>
String view = "summary"; // String | DataView for getting roles. Defaults to 'summary'.
try {
    ApiRoleList result = apiInstance.readRoles(clusterName, serviceName, filter, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#readRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **filter** | **String**| Optional query to filter the roles by. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: hostname==host1.abc.com;type==DATANODE </p>  Currently supports filtering by: <ul> <li>hostname: The hostname of the host the role is running on.</li> <li>hostId: The unique identifier of the host the role is running on.</li> <li>type: The role's type.</li> </ul> | [optional] [default to ]
 **view** | **String**| DataView for getting roles. Defaults to 'summary'. | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateRoleConfig"></a>
# **updateRoleConfig**
> ApiConfigList updateRoleConfig(clusterName, roleName, serviceName, message, body)

Updates the role configuration with the given values.

Updates the role configuration with the given values. <p> If a value is set in the given configuration, it will be added to the role's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RolesResourceApi apiInstance = new RolesResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | The role to modify.
String serviceName = "serviceName_example"; // String | 
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | Configuration changes.
try {
    ApiConfigList result = apiInstance.updateRoleConfig(clusterName, roleName, serviceName, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesResourceApi#updateRoleConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**| The role to modify. |
 **serviceName** | **String**|  |
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| Configuration changes. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

