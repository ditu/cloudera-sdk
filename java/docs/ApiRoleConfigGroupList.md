
# ApiRoleConfigGroupList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiRoleConfigGroup&gt;**](ApiRoleConfigGroup.md) |  |  [optional]



