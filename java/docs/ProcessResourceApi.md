# ProcessResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getConfigFile**](ProcessResourceApi.md#getConfigFile) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/process/configFiles/{configFileName} | Returns the contents of the specified config file.
[**getProcess**](ProcessResourceApi.md#getProcess) | **GET** /clusters/{clusterName}/services/{serviceName}/roles/{roleName}/process | 


<a name="getConfigFile"></a>
# **getConfigFile**
> File getConfigFile(clusterName, configFileName, roleName, serviceName)

Returns the contents of the specified config file.

Returns the contents of the specified config file. A multi-level file name (e.g. hadoop-conf/hdfs-site.xml) is acceptable here.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ProcessResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ProcessResourceApi apiInstance = new ProcessResourceApi();
String clusterName = "clusterName_example"; // String | 
String configFileName = "configFileName_example"; // String | Name of the config file to get.
String roleName = "roleName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
try {
    File result = apiInstance.getConfigFile(clusterName, configFileName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProcessResourceApi#getConfigFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **configFileName** | **String**| Name of the config file to get. |
 **roleName** | **String**|  |
 **serviceName** | **String**|  |

### Return type

[**File**](File.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getProcess"></a>
# **getProcess**
> ApiProcess getProcess(clusterName, roleName, serviceName)





### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ProcessResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ProcessResourceApi apiInstance = new ProcessResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleName = "roleName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
try {
    ApiProcess result = apiInstance.getProcess(clusterName, roleName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProcessResourceApi#getProcess");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleName** | **String**|  |
 **serviceName** | **String**|  |

### Return type

[**ApiProcess**](ApiProcess.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

