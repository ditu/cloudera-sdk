
# ReplicationOption

## Enum


* `METADATA_ONLY` (value: `"METADATA_ONLY"`)

* `METADATA_AND_DATA` (value: `"METADATA_AND_DATA"`)

* `KEEP_DATA_IN_CLOUD` (value: `"KEEP_DATA_IN_CLOUD"`)



