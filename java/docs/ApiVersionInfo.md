
# ApiVersionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** | Version. |  [optional]
**snapshot** | **Boolean** | Whether this build is a development snapshot. |  [optional]
**buildUser** | **String** | The user performing the build. |  [optional]
**buildTimestamp** | **String** | Build timestamp. |  [optional]
**gitHash** | **String** | Source control management hash. |  [optional]



