
# ApiParcelUsageParcel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parcelRef** | [**ApiParcelRef**](ApiParcelRef.md) | Reference to the corresponding Parcel object. |  [optional]
**processCount** | [**BigDecimal**](BigDecimal.md) | How many running processes on the cluster are using the parcel. |  [optional]
**activated** | **Boolean** | Is this parcel currently activated on the cluster. |  [optional]



