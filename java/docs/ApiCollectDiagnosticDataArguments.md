
# ApiCollectDiagnosticDataArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bundleSizeBytes** | [**BigDecimal**](BigDecimal.md) | The maximum approximate bundle size of the output file |  [optional]
**startTime** | **String** | This parameter is ignored between CM 4.5 and CM 5.7 versions. For versions from CM 4.5 to CM 5.7, use endTime and bundleSizeBytes instead.  For CM 5.7+ versions, startTime is an optional parameter that is with endTime and bundleSizeBytes. This was introduced to perform diagnostic data estimation and collection of global diagnostics data for a certain time range. The start time (in ISO 8601 format) of the period to collection statistics for. |  [optional]
**endTime** | **String** | The end time (in ISO 8601 format) of the period to collection statistics for. |  [optional]
**includeInfoLog** | **Boolean** | This parameter is ignored as of CM 4.5. INFO logs are always collected. Whether to include INFO level logs. WARN, ERROR, and FATAL level logs are always included. |  [optional]
**ticketNumber** | **String** | The support ticket number to attach to this data collection. |  [optional]
**comments** | **String** | Comments to include with this data collection. |  [optional]
**clusterName** | **String** | Name of the cluster to collect. If null, collects from all clusters. |  [optional]
**enableMonitorMetricsCollection** | **Boolean** | Flag to enable collection of metrics for chart display. |  [optional]
**roles** | **List&lt;String&gt;** | List of roles for which to get logs and metrics.  If set, this restricts the roles for log and metrics collection to the list specified.  If empty, the default is to get logs for all roles (in the selected cluster, if one is selected).  Introduced in API v10 of the API. |  [optional]



