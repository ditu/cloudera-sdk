
# ApiClusterTemplateConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**value** | **String** |  |  [optional]
**ref** | **String** |  |  [optional]
**variable** | **String** |  |  [optional]
**autoConfig** | **Boolean** |  |  [optional]



