
# ApiHdfsSnapshotResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processedPathCount** | [**BigDecimal**](BigDecimal.md) | Number of processed paths. |  [optional]
**processedPaths** | **List&lt;String&gt;** | The list of processed paths. &lt;p/&gt; This is only available in the full view. |  [optional]
**unprocessedPathCount** | [**BigDecimal**](BigDecimal.md) | Number of unprocessed paths. |  [optional]
**unprocessedPaths** | **List&lt;String&gt;** | The list of unprocessed paths. Note that paths that are currently being processed will also be included in this list. &lt;p/&gt; This is only available in the full view. |  [optional]
**createdSnapshotCount** | [**BigDecimal**](BigDecimal.md) | Number of snapshots created. |  [optional]
**createdSnapshots** | [**List&lt;ApiHdfsSnapshot&gt;**](ApiHdfsSnapshot.md) | List of snapshots created. &lt;p/&gt; This is only available in the full view. |  [optional]
**deletedSnapshotCount** | [**BigDecimal**](BigDecimal.md) | Number of snapshots deleted. |  [optional]
**deletedSnapshots** | [**List&lt;ApiHdfsSnapshot&gt;**](ApiHdfsSnapshot.md) | List of snapshots deleted. &lt;p/&gt; This is only available in the full view. |  [optional]
**creationErrorCount** | [**BigDecimal**](BigDecimal.md) | Number of errors detected when creating snapshots. |  [optional]
**creationErrors** | [**List&lt;ApiHdfsSnapshotError&gt;**](ApiHdfsSnapshotError.md) | List of errors encountered when creating snapshots. &lt;p/&gt; This is only available in the full view. |  [optional]
**deletionErrorCount** | [**BigDecimal**](BigDecimal.md) | Number of errors detected when deleting snapshots. |  [optional]
**deletionErrors** | [**List&lt;ApiHdfsSnapshotError&gt;**](ApiHdfsSnapshotError.md) | List of errors encountered when deleting snapshots. &lt;p/&gt; This is only available in the full view. |  [optional]



