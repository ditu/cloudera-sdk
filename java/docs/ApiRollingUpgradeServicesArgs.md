
# ApiRollingUpgradeServicesArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**upgradeFromCdhVersion** | **String** | Current CDH Version of the services. Example versions are: \&quot;5.1.0\&quot;, \&quot;5.2.2\&quot; or \&quot;5.4.0\&quot; |  [optional]
**upgradeToCdhVersion** | **String** | Target CDH Version for the services. The CDH version should already be present and activated on the nodes. Example versions are: \&quot;5.1.0\&quot;, \&quot;5.2.2\&quot; or \&quot;5.4.0\&quot; |  [optional]
**slaveBatchSize** | [**BigDecimal**](BigDecimal.md) | Number of hosts with slave roles to upgrade at a time. Must be greater than zero. Default is 1. |  [optional]
**sleepSeconds** | [**BigDecimal**](BigDecimal.md) | Number of seconds to sleep between restarts of slave host batches.  Must be greater than or equal to 0. Default is 0. |  [optional]
**slaveFailCountThreshold** | [**BigDecimal**](BigDecimal.md) | The threshold for number of slave host batches that are allowed to fail to restart before the entire command is considered failed.  Must be greater than or equal to 0. Default is 0. &lt;p&gt; This argument is for ADVANCED users only. &lt;/p&gt; |  [optional]
**upgradeServiceNames** | **List&lt;String&gt;** | List of services to upgrade. Only the services that support rolling upgrade should be included. |  [optional]



