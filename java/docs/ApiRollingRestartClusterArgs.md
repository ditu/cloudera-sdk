
# ApiRollingRestartClusterArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slaveBatchSize** | [**BigDecimal**](BigDecimal.md) | Number of hosts with slave roles to restart at a time. Must be greater than zero. Default is 1. |  [optional]
**sleepSeconds** | [**BigDecimal**](BigDecimal.md) | Number of seconds to sleep between restarts of slave host batches. &lt;p&gt; Must be greater than or equal to 0. Default is 0. |  [optional]
**slaveFailCountThreshold** | [**BigDecimal**](BigDecimal.md) | The threshold for number of slave host batches that are allowed to fail to restart before the entire command is considered failed. &lt;p&gt; Must be greater than or equal to 0. Default is 0. &lt;p&gt; This argument is for ADVANCED users only. &lt;/p&gt; |  [optional]
**staleConfigsOnly** | **Boolean** | Restart roles with stale configs only. |  [optional]
**unUpgradedOnly** | **Boolean** | Restart roles that haven&#39;t been upgraded yet. |  [optional]
**redeployClientConfiguration** | **Boolean** | Re-deploy client configuration. Available since API v6. |  [optional]
**rolesToInclude** | [**ApiRolesToInclude**](ApiRolesToInclude.md) | Role types to restart. Default is slave roles only. |  [optional]
**restartServiceNames** | **List&lt;String&gt;** | List of services to restart. |  [optional]



