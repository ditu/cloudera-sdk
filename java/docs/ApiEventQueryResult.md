
# ApiEventQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalResults** | [**BigDecimal**](BigDecimal.md) | The total number of matched results. Some are possibly not shown due to pagination. |  [optional]
**items** | [**List&lt;ApiEvent&gt;**](ApiEvent.md) |  |  [optional]



