
# ApiHostList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiHost&gt;**](ApiHost.md) |  |  [optional]



