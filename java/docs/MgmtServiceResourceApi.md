# MgmtServiceResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autoAssignRoles**](MgmtServiceResourceApi.md#autoAssignRoles) | **PUT** /cm/service/autoAssignRoles | Automatically assign roles to hosts and create the roles for the Cloudera Management Service.
[**autoConfigure**](MgmtServiceResourceApi.md#autoConfigure) | **PUT** /cm/service/autoConfigure | Automatically configures roles of the Cloudera Management Service.
[**deleteCMS**](MgmtServiceResourceApi.md#deleteCMS) | **DELETE** /cm/service | Delete the Cloudera Management Services.
[**enterMaintenanceMode**](MgmtServiceResourceApi.md#enterMaintenanceMode) | **POST** /cm/service/commands/enterMaintenanceMode | Put Cloudera Management Service into maintenance mode.
[**exitMaintenanceMode**](MgmtServiceResourceApi.md#exitMaintenanceMode) | **POST** /cm/service/commands/exitMaintenanceMode | Take Cloudera Management Service out of maintenance mode.
[**listActiveCommands**](MgmtServiceResourceApi.md#listActiveCommands) | **GET** /cm/service/commands | List active Cloudera Management Services commands.
[**listRoleTypes**](MgmtServiceResourceApi.md#listRoleTypes) | **GET** /cm/service/roleTypes | List the supported role types for the Cloudera Management Services.
[**readService**](MgmtServiceResourceApi.md#readService) | **GET** /cm/service | Retrieve information about the Cloudera Management Services.
[**readServiceConfig**](MgmtServiceResourceApi.md#readServiceConfig) | **GET** /cm/service/config | Retrieve the configuration of the Cloudera Management Services.
[**restartCommand**](MgmtServiceResourceApi.md#restartCommand) | **POST** /cm/service/commands/restart | Restart the Cloudera Management Services.
[**setupCMS**](MgmtServiceResourceApi.md#setupCMS) | **PUT** /cm/service | Setup the Cloudera Management Services.
[**startCommand**](MgmtServiceResourceApi.md#startCommand) | **POST** /cm/service/commands/start | Start the Cloudera Management Services.
[**stopCommand**](MgmtServiceResourceApi.md#stopCommand) | **POST** /cm/service/commands/stop | Stop the Cloudera Management Services.
[**updateServiceConfig**](MgmtServiceResourceApi.md#updateServiceConfig) | **PUT** /cm/service/config | Update the Cloudera Management Services configuration.


<a name="autoAssignRoles"></a>
# **autoAssignRoles**
> autoAssignRoles()

Automatically assign roles to hosts and create the roles for the Cloudera Management Service.

Automatically assign roles to hosts and create the roles for the Cloudera Management Service. <p> Assignments are done based on number of hosts in the deployment and hardware specifications. If no hosts are part of the deployment, an exception will be thrown preventing any role assignments. Existing roles will be taken into account and their assignments will be not be modified. The deployment should not have any clusters when calling this endpoint. If it does, an exception will be thrown preventing any role assignments. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    apiInstance.autoAssignRoles();
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#autoAssignRoles");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="autoConfigure"></a>
# **autoConfigure**
> autoConfigure()

Automatically configures roles of the Cloudera Management Service.

Automatically configures roles of the Cloudera Management Service. <p> Overwrites some existing configurations. Only default role config groups must exist before calling this endpoint. Other role config groups must not exist. If they do, an exception will be thrown preventing any configuration. Ignores any clusters (and their services and roles) colocated with the Cloudera Management Service. To avoid over-committing the heap on hosts, place the Cloudera Management Service roles on machines not used by any of the clusters. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    apiInstance.autoConfigure();
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#autoConfigure");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteCMS"></a>
# **deleteCMS**
> ApiService deleteCMS()

Delete the Cloudera Management Services.

Delete the Cloudera Management Services. <p> This method will fail if a CMS instance doesn't already exist.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    ApiService result = apiInstance.deleteCMS();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#deleteCMS");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="enterMaintenanceMode"></a>
# **enterMaintenanceMode**
> ApiCommand enterMaintenanceMode()

Put Cloudera Management Service into maintenance mode.

Put Cloudera Management Service into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v18.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    ApiCommand result = apiInstance.enterMaintenanceMode();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#enterMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaintenanceMode"></a>
# **exitMaintenanceMode**
> ApiCommand exitMaintenanceMode()

Take Cloudera Management Service out of maintenance mode.

Take Cloudera Management Service out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v18.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    ApiCommand result = apiInstance.exitMaintenanceMode();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#exitMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listActiveCommands"></a>
# **listActiveCommands**
> ApiCommandList listActiveCommands(view)

List active Cloudera Management Services commands.

List active Cloudera Management Services commands.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiCommandList result = apiInstance.listActiveCommands(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#listActiveCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listRoleTypes"></a>
# **listRoleTypes**
> ApiRoleTypeList listRoleTypes()

List the supported role types for the Cloudera Management Services.

List the supported role types for the Cloudera Management Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    ApiRoleTypeList result = apiInstance.listRoleTypes();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#listRoleTypes");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleTypeList**](ApiRoleTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readService"></a>
# **readService**
> ApiService readService(view)

Retrieve information about the Cloudera Management Services.

Retrieve information about the Cloudera Management Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
String view = "summary"; // String | 
try {
    ApiService result = apiInstance.readService(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#readService");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readServiceConfig"></a>
# **readServiceConfig**
> ApiServiceConfig readServiceConfig(view)

Retrieve the configuration of the Cloudera Management Services.

Retrieve the configuration of the Cloudera Management Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiServiceConfig result = apiInstance.readServiceConfig(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#readServiceConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="restartCommand"></a>
# **restartCommand**
> ApiCommand restartCommand()

Restart the Cloudera Management Services.

Restart the Cloudera Management Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    ApiCommand result = apiInstance.restartCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#restartCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="setupCMS"></a>
# **setupCMS**
> ApiService setupCMS(body)

Setup the Cloudera Management Services.

Setup the Cloudera Management Services. <p> Configure the CMS instance and create all the management roles. The provided configuration data can be used to set up host mappings for each role, and required configuration such as database connection information for specific roles. <p> Regardless of the list of roles provided in the input data, all management roles are created by this call. The input is used to override any default settings for the specific roles. <p> This method needs a valid CM license to be installed beforehand. <p> This method does not start any services or roles. <p> This method will fail if a CMS instance already exists. <p> Available role types: <ul> <li>SERVICEMONITOR</li> <li>ACTIVITYMONITOR</li> <li>HOSTMONITOR</li> <li>REPORTSMANAGER</li> <li>EVENTSERVER</li> <li>ALERTPUBLISHER</li> <li>NAVIGATOR</li> <li>NAVIGATORMETASERVER</li> </ul>  <p/> REPORTSMANAGER, NAVIGATOR and NAVIGATORMETASERVER are only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
ApiService body = new ApiService(); // ApiService | Role configuration overrides.
try {
    ApiService result = apiInstance.setupCMS(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#setupCMS");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiService**](ApiService.md)| Role configuration overrides. | [optional]

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="startCommand"></a>
# **startCommand**
> ApiCommand startCommand()

Start the Cloudera Management Services.

Start the Cloudera Management Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    ApiCommand result = apiInstance.startCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#startCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stopCommand"></a>
# **stopCommand**
> ApiCommand stopCommand()

Stop the Cloudera Management Services.

Stop the Cloudera Management Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
try {
    ApiCommand result = apiInstance.stopCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#stopCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateServiceConfig"></a>
# **updateServiceConfig**
> ApiServiceConfig updateServiceConfig(message, body)

Update the Cloudera Management Services configuration.

Update the Cloudera Management Services configuration. <p> If a value is set in the given configuration, it will be added to the service's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtServiceResourceApi apiInstance = new MgmtServiceResourceApi();
String message = "message_example"; // String | Optional message describing the changes.
ApiServiceConfig body = new ApiServiceConfig(); // ApiServiceConfig | Configuration changes.
try {
    ApiServiceConfig result = apiInstance.updateServiceConfig(message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtServiceResourceApi#updateServiceConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiServiceConfig**](ApiServiceConfig.md)| Configuration changes. | [optional]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

