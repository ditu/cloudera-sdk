
# ApiCmPeer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the remote CM instance. Immutable during update. |  [optional]
**type** | [**ApiCmPeerType**](ApiCmPeerType.md) | The type of the remote CM instance. Immutable during update.  Available since API v11. |  [optional]
**url** | **String** | The URL of the remote CM instance. Mutable during update. |  [optional]
**username** | **String** | When creating peers, if &#39;clouderaManagerCreatedUser&#39; is true, this should be the remote admin username for creating a user in remote Cloudera Manager. The created remote user will then be stored in the local Cloudera Manager DB and used in later communication. If &#39;clouderaManagerCreatedUser&#39; is false, which is not applicable to REPLICATION peer type, Cloudera Manager will store this username in the local DB directly and use it together with &#39;password&#39; for communication.  Mutable during update. When set during update, if &#39;clouderaManagerCreatedUser&#39; is true, a new user in remote Cloudera Manager is created, the newly created remote user will be stored in the local DB. An attempt to delete the previously created remote user will be made; If &#39;clouderaManagerCreatedUser&#39; is false, the username/password in the local DB will be updated. |  [optional]
**password** | **String** | When creating peers, if &#39;clouderaManagerCreatedUser&#39; is true, this should be the remote admin password for creating a user in remote Cloudera Manager. The created remote user will then be stored in the local Cloudera Manager DB and used in later communication. If &#39;clouderaManagerCreatedUser&#39; is false, which is not applicable to REPLICATION peer type, Cloudera Manager will store this password in the local DB directly and use it together with &#39;username&#39; for communication.  Mutable during update. When set during update, if &#39;clouderaManagerCreatedUser&#39; is true, a new user in remote Cloudera Manager is created, the newly created remote user will be stored in the local DB. An attempt to delete the previously created remote user will be made; If &#39;clouderaManagerCreatedUser&#39; is false, the username/password in the local DB will be updated. |  [optional]
**clouderaManagerCreatedUser** | **Boolean** | If true, Cloudera Manager creates a remote user using the given username/password and stores the created user in local DB for use in later communication. Cloudera Manager will also try to delete the created remote user when deleting such peers.  If false, Cloudera Manager will store the provided username/password in the local DB and use them in later communication. &#39;false&#39; value on this field is not applicable to REPLICATION peer type.  Available since API v11.  Immutable during update. Should not be set when updating peers. |  [optional]



