
# ApiTimeSeriesResponseList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiTimeSeriesResponse&gt;**](ApiTimeSeriesResponse.md) | The list of responses for this query response list. |  [optional]



