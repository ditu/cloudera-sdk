
# ApiRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the role. Optional when creating a role since API v6. If not specified, a name will be automatically generated for the role. |  [optional]
**type** | **String** | The type of the role, e.g. NAMENODE, DATANODE, TASKTRACKER. |  [optional]
**hostRef** | [**ApiHostRef**](ApiHostRef.md) | A reference to the host where this role runs. |  [optional]
**serviceRef** | [**ApiServiceRef**](ApiServiceRef.md) | Readonly. A reference to the parent service. |  [optional]
**roleState** | [**ApiRoleState**](ApiRoleState.md) | Readonly. The configured run state of this role. Whether it&#39;s running, etc. |  [optional]
**commissionState** | [**ApiCommissionState**](ApiCommissionState.md) | Readonly. The commission state of this role. Available since API v2. |  [optional]
**healthSummary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Readonly. The high-level health status of this role. |  [optional]
**configStale** | **Boolean** | Readonly. Expresses whether the role configuration is stale. |  [optional]
**configStalenessStatus** | [**ApiConfigStalenessStatus**](ApiConfigStalenessStatus.md) | Readonly. Expresses the role&#39;s configuration staleness status. Available since API v6. |  [optional]
**healthChecks** | [**List&lt;ApiHealthCheck&gt;**](ApiHealthCheck.md) | Readonly. The list of health checks of this service. |  [optional]
**haStatus** | [**HaStatus**](HaStatus.md) | Readonly. The HA status of this role. |  [optional]
**roleUrl** | **String** | Readonly. Link into the Cloudera Manager web UI for this specific role. |  [optional]
**maintenanceMode** | **Boolean** | Readonly. Whether the role is in maintenance mode. Available since API v2. |  [optional]
**maintenanceOwners** | [**List&lt;ApiEntityType&gt;**](ApiEntityType.md) | Readonly. The list of objects that trigger this role to be in maintenance mode. Available since API v2. |  [optional]
**config** | [**ApiConfigList**](ApiConfigList.md) | The role configuration. Optional. |  [optional]
**roleConfigGroupRef** | [**ApiRoleConfigGroupRef**](ApiRoleConfigGroupRef.md) | Readonly. The reference to the role configuration group of this role. Available since API v3. |  [optional]
**zooKeeperServerMode** | [**ZooKeeperServerMode**](ZooKeeperServerMode.md) | Readonly. The ZooKeeper server mode for this role. Note that for non-ZooKeeper Server roles this will be null. Available since API v6. |  [optional]
**entityStatus** | [**ApiEntityStatus**](ApiEntityStatus.md) | Readonly. The entity status for this role. Available since API v11. |  [optional]



