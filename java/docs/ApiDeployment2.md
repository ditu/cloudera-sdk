
# ApiDeployment2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **String** | Readonly. This timestamp is provided when you request a deployment and is not required (or even read) when creating a deployment. This timestamp is useful if you have multiple deployments saved and want to determine which one to use as a restore point. |  [optional]
**clusters** | [**List&lt;ApiCluster&gt;**](ApiCluster.md) | List of clusters in the system including their services, roles and complete config values. |  [optional]
**hosts** | [**List&lt;ApiHost&gt;**](ApiHost.md) | List of hosts in the system |  [optional]
**users** | [**List&lt;ApiUser2&gt;**](ApiUser2.md) | List of all users in the system |  [optional]
**versionInfo** | [**ApiVersionInfo**](ApiVersionInfo.md) | Full version information about the running Cloudera Manager instance |  [optional]
**managementService** | [**ApiService**](ApiService.md) | The full configuration of the Cloudera Manager management service including all the management roles and their config values |  [optional]
**managerSettings** | [**ApiConfigList**](ApiConfigList.md) | The full configuration of Cloudera Manager itself including licensing info |  [optional]
**allHostsConfig** | [**ApiConfigList**](ApiConfigList.md) | Configuration parameters that apply to all hosts, unless overridden at the host level. Available since API v3. |  [optional]
**peers** | [**List&lt;ApiCmPeer&gt;**](ApiCmPeer.md) | The list of peers configured in Cloudera Manager. Available since API v3. |  [optional]
**hostTemplates** | [**ApiHostTemplateList**](ApiHostTemplateList.md) | The list of all host templates in Cloudera Manager. |  [optional]



