
# ApiEnableLlamaHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newLlamaHostId** | **String** | HostId of the host on which the second Llama role will be created. |  [optional]
**newLlamaRoleName** | **String** | Name of the second Llama role to be created (optional). |  [optional]
**zkServiceName** | **String** | Name of the ZooKeeper service that will be used for auto-failover. This argument may be omitted if the ZooKeeper dependency for Impala is already configured. |  [optional]



