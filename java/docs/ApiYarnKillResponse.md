
# ApiYarnKillResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warning** | **String** | The warning, if any, from the call. We will return a warning if the caller attempts to cancel an application that has already completed. |  [optional]



