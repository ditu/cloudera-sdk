
# ApiImpalaQueryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**queries** | [**List&lt;ApiImpalaQuery&gt;**](ApiImpalaQuery.md) | The list of queries for this response. |  [optional]
**warnings** | **List&lt;String&gt;** | This list of warnings for this response. |  [optional]



