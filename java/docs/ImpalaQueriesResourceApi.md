# ImpalaQueriesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelImpalaQuery**](ImpalaQueriesResourceApi.md#cancelImpalaQuery) | **POST** /clusters/{clusterName}/services/{serviceName}/impalaQueries/{queryId}/cancel | Cancels an Impala Query.
[**getImpalaQueries**](ImpalaQueriesResourceApi.md#getImpalaQueries) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries | Returns a list of queries that satisfy the filter.
[**getImpalaQueryAttributes**](ImpalaQueriesResourceApi.md#getImpalaQueryAttributes) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries/attributes | Returns the list of all attributes that the Service Monitor can associate with Impala queries.
[**getQueryDetails**](ImpalaQueriesResourceApi.md#getQueryDetails) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaQueries/{queryId} | Returns details about the query.


<a name="cancelImpalaQuery"></a>
# **cancelImpalaQuery**
> ApiImpalaCancelResponse cancelImpalaQuery(clusterName, queryId, serviceName)

Cancels an Impala Query.

Cancels an Impala Query. <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ImpalaQueriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ImpalaQueriesResourceApi apiInstance = new ImpalaQueriesResourceApi();
String clusterName = "clusterName_example"; // String | 
String queryId = "queryId_example"; // String | The queryId to cancel
String serviceName = "serviceName_example"; // String | The name of the service
try {
    ApiImpalaCancelResponse result = apiInstance.cancelImpalaQuery(clusterName, queryId, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ImpalaQueriesResourceApi#cancelImpalaQuery");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **queryId** | **String**| The queryId to cancel |
 **serviceName** | **String**| The name of the service |

### Return type

[**ApiImpalaCancelResponse**](ApiImpalaCancelResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getImpalaQueries"></a>
# **getImpalaQueries**
> ApiImpalaQueryResponse getImpalaQueries(clusterName, serviceName, filter, from, limit, offset, to)

Returns a list of queries that satisfy the filter.

Returns a list of queries that satisfy the filter <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ImpalaQueriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ImpalaQueriesResourceApi apiInstance = new ImpalaQueriesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
String filter = ""; // String | A filter to apply to the queries. A basic filter tests the value of an attribute and looks something like 'rowsFetched = 1' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'query_duration > 5s and (user = root or user = myUserName)'. An example of expected full query string in requested URL is: '?filter=(query_duration > 5s and (user = root or user = myUserName))'.
String from = "from_example"; // String | Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time).
Integer limit = 100; // Integer | The maximum number of queries to return. Queries will be returned in the following order: <ul> <li> All executing queries, ordered from longest to shortest running </li> <li> All completed queries order by end time descending. </li> </ul>
Integer offset = 0; // Integer | The offset to start returning queries from. This is useful for paging through lists of queries. Note that this has non-deterministic behavior if executing queries are included in the response because they can disappear from the list while paging. To exclude executing queries from the response and a 'executing = false' clause to your filter.
String to = "now"; // String | End of the period to query in ISO 8601 format (defaults to current time).
try {
    ApiImpalaQueryResponse result = apiInstance.getImpalaQueries(clusterName, serviceName, filter, from, limit, offset, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ImpalaQueriesResourceApi#getImpalaQueries");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |
 **filter** | **String**| A filter to apply to the queries. A basic filter tests the value of an attribute and looks something like 'rowsFetched = 1' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'query_duration > 5s and (user = root or user = myUserName)'. An example of expected full query string in requested URL is: '?filter=(query_duration > 5s and (user = root or user = myUserName))'. | [optional] [default to ]
 **from** | **String**| Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time). | [optional]
 **limit** | **Integer**| The maximum number of queries to return. Queries will be returned in the following order: <ul> <li> All executing queries, ordered from longest to shortest running </li> <li> All completed queries order by end time descending. </li> </ul> | [optional] [default to 100]
 **offset** | **Integer**| The offset to start returning queries from. This is useful for paging through lists of queries. Note that this has non-deterministic behavior if executing queries are included in the response because they can disappear from the list while paging. To exclude executing queries from the response and a 'executing = false' clause to your filter. | [optional] [default to 0]
 **to** | **String**| End of the period to query in ISO 8601 format (defaults to current time). | [optional] [default to now]

### Return type

[**ApiImpalaQueryResponse**](ApiImpalaQueryResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getImpalaQueryAttributes"></a>
# **getImpalaQueryAttributes**
> ApiImpalaQueryAttributeList getImpalaQueryAttributes(clusterName, serviceName)

Returns the list of all attributes that the Service Monitor can associate with Impala queries.

Returns the list of all attributes that the Service Monitor can associate with Impala queries. <p> Examples of attributes include the user who issued the query and the number of HDFS bytes read by the query. <p> These attributes can be used to search for specific Impala queries through the getImpalaQueries API. For example the 'user' attribute could be used in the search 'user = root'. If the attribute is numeric it can also be used as a metric in a tsquery (ie, 'select hdfs_bytes_read from IMPALA_QUERIES'). <p> Note that this response is identical for all Impala services. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ImpalaQueriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ImpalaQueriesResourceApi apiInstance = new ImpalaQueriesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
try {
    ApiImpalaQueryAttributeList result = apiInstance.getImpalaQueryAttributes(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ImpalaQueriesResourceApi#getImpalaQueryAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |

### Return type

[**ApiImpalaQueryAttributeList**](ApiImpalaQueryAttributeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getQueryDetails"></a>
# **getQueryDetails**
> ApiImpalaQueryDetailsResponse getQueryDetails(clusterName, queryId, serviceName, format)

Returns details about the query.

Returns details about the query. Not all queries have details, check the detailsAvailable field from the getQueries response. <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ImpalaQueriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ImpalaQueriesResourceApi apiInstance = new ImpalaQueriesResourceApi();
String clusterName = "clusterName_example"; // String | 
String queryId = "queryId_example"; // String | The queryId to get information about
String serviceName = "serviceName_example"; // String | 
String format = "text"; // String | There are two valid format parameters: <ul> <li> 'text': this is a text based, human readable representation of the Impala runtime profile. </li> <li> 'thrift_encoded': this a compact-thrift, base64 encoded representation of the Impala RuntimeProfile.thrift object. See the Impala documentation for more details. </li> </ul>
try {
    ApiImpalaQueryDetailsResponse result = apiInstance.getQueryDetails(clusterName, queryId, serviceName, format);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ImpalaQueriesResourceApi#getQueryDetails");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **queryId** | **String**| The queryId to get information about |
 **serviceName** | **String**|  |
 **format** | **String**| There are two valid format parameters: <ul> <li> 'text': this is a text based, human readable representation of the Impala runtime profile. </li> <li> 'thrift_encoded': this a compact-thrift, base64 encoded representation of the Impala RuntimeProfile.thrift object. See the Impala documentation for more details. </li> </ul> | [optional] [default to text]

### Return type

[**ApiImpalaQueryDetailsResponse**](ApiImpalaQueryDetailsResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

