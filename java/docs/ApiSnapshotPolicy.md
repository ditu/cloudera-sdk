
# ApiSnapshotPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the snapshot policy. |  [optional]
**description** | **String** | Description of the snapshot policy. |  [optional]
**hourlySnapshots** | [**BigDecimal**](BigDecimal.md) | Number of hourly snapshots to be retained. |  [optional]
**dailySnapshots** | [**BigDecimal**](BigDecimal.md) | Number of daily snapshots to be retained. |  [optional]
**weeklySnapshots** | [**BigDecimal**](BigDecimal.md) | Number of weekly snapshots to be retained. |  [optional]
**monthlySnapshots** | [**BigDecimal**](BigDecimal.md) | Number of monthly snapshots to be retained. |  [optional]
**yearlySnapshots** | [**BigDecimal**](BigDecimal.md) | Number of yearly snapshots to be retained. |  [optional]
**minuteOfHour** | [**BigDecimal**](BigDecimal.md) | Minute in the hour that hourly, daily, weekly, monthly and yearly snapshots should be created. Valid values are 0 to 59. Default value is 0. |  [optional]
**hoursForHourlySnapshots** | [**List&lt;BigDecimal&gt;**](BigDecimal.md) | Hours of the day that hourly snapshots should be created. Valid values are 0 to 23. If this list is null or empty, then hourly snapshots are created for every hour. |  [optional]
**hourOfDay** | [**BigDecimal**](BigDecimal.md) | Hour in the day that daily, weekly, monthly and yearly snapshots should be created. Valid values are 0 to 23. Default value is 0. |  [optional]
**dayOfWeek** | [**BigDecimal**](BigDecimal.md) | Day of the week that weekly snapshots should be created. Valid values are 1 to 7, 1 representing Sunday. Default value is 1. |  [optional]
**dayOfMonth** | [**BigDecimal**](BigDecimal.md) | Day of the month that monthly and yearly snapshots should be created. Values from 1 to 31 are allowed. Additionally 0 to -30 can be used to specify offsets from the last day of the month. Default value is 1. &lt;p/&gt; If this value is invalid for any month for which snapshots are required, the backend will throw an exception. |  [optional]
**monthOfYear** | [**BigDecimal**](BigDecimal.md) | Month of the year that yearly snapshots should be created. Valid values are 1 to 12, 1 representing January. Default value is 1. |  [optional]
**alertOnStart** | **Boolean** | Whether to alert on start of snapshot creation/deletion activity. |  [optional]
**alertOnSuccess** | **Boolean** | Whether to alert on successful completion of snapshot creation/deletion activity. |  [optional]
**alertOnFail** | **Boolean** | Whether to alert on failure of snapshot creation/deletion activity. |  [optional]
**alertOnAbort** | **Boolean** | Whether to alert on abort of snapshot creation/deletion activity. |  [optional]
**hbaseArguments** | [**ApiHBaseSnapshotPolicyArguments**](ApiHBaseSnapshotPolicyArguments.md) | Arguments specific to HBase snapshot policies. |  [optional]
**hdfsArguments** | [**ApiHdfsSnapshotPolicyArguments**](ApiHdfsSnapshotPolicyArguments.md) | Arguments specific to Hdfs snapshot policies. |  [optional]
**lastCommand** | [**ApiSnapshotCommand**](ApiSnapshotCommand.md) | Latest command of this policy. The command might still be active. |  [optional]
**lastSuccessfulCommand** | [**ApiSnapshotCommand**](ApiSnapshotCommand.md) | Last successful command of this policy. Returns null if there has been no successful command. |  [optional]
**paused** | **Boolean** | Whether to pause a snapshot policy, available since V11. |  [optional]



