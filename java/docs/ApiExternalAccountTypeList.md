
# ApiExternalAccountTypeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiExternalAccountType&gt;**](ApiExternalAccountType.md) |  |  [optional]



