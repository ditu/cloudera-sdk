
# ApiMetricSchemaList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiMetricSchema&gt;**](ApiMetricSchema.md) |  |  [optional]



