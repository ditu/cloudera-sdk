# RoleConfigGroupsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRoleConfigGroups**](RoleConfigGroupsResourceApi.md#createRoleConfigGroups) | **POST** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups | Creates new role config groups.
[**deleteRoleConfigGroup**](RoleConfigGroupsResourceApi.md#deleteRoleConfigGroup) | **DELETE** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Deletes a role config group.
[**moveRoles**](RoleConfigGroupsResourceApi.md#moveRoles) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/roles | Moves roles to the specified role config group.
[**moveRolesToBaseGroup**](RoleConfigGroupsResourceApi.md#moveRolesToBaseGroup) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/roles | Moves roles to the base role config group.
[**readConfig**](RoleConfigGroupsResourceApi.md#readConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/config | Returns the current revision of the config for the specified role config group.
[**readRoleConfigGroup**](RoleConfigGroupsResourceApi.md#readRoleConfigGroup) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Returns the information for a role config group.
[**readRoleConfigGroups**](RoleConfigGroupsResourceApi.md#readRoleConfigGroups) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups | Returns the information for all role config groups for a given cluster and service.
[**readRoles**](RoleConfigGroupsResourceApi.md#readRoles) | **GET** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/roles | Returns all roles in the given role config group.
[**updateConfig**](RoleConfigGroupsResourceApi.md#updateConfig) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName}/config | Updates the config for the given role config group.
[**updateRoleConfigGroup**](RoleConfigGroupsResourceApi.md#updateRoleConfigGroup) | **PUT** /clusters/{clusterName}/services/{serviceName}/roleConfigGroups/{roleConfigGroupName} | Updates an existing role config group.


<a name="createRoleConfigGroups"></a>
# **createRoleConfigGroups**
> ApiRoleConfigGroupList createRoleConfigGroups(clusterName, serviceName, body)

Creates new role config groups.

Creates new role config groups. It is not allowed to create base groups (base must be set to false.) <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleConfigGroupList body = new ApiRoleConfigGroupList(); // ApiRoleConfigGroupList | The list of groups to be created.
try {
    ApiRoleConfigGroupList result = apiInstance.createRoleConfigGroups(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#createRoleConfigGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)| The list of groups to be created. | [optional]

### Return type

[**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteRoleConfigGroup"></a>
# **deleteRoleConfigGroup**
> ApiRoleConfigGroup deleteRoleConfigGroup(clusterName, roleConfigGroupName, serviceName)

Deletes a role config group.

Deletes a role config group. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the group to delete.
String serviceName = "serviceName_example"; // String | 
try {
    ApiRoleConfigGroup result = apiInstance.deleteRoleConfigGroup(clusterName, roleConfigGroupName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#deleteRoleConfigGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleConfigGroupName** | **String**| The name of the group to delete. |
 **serviceName** | **String**|  |

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="moveRoles"></a>
# **moveRoles**
> ApiRoleList moveRoles(clusterName, roleConfigGroupName, serviceName, body)

Moves roles to the specified role config group.

Moves roles to the specified role config group.  The roles can be moved from any role config group belonging to the same service. The role type of the destination group must match the role type of the roles. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the group the roles will be moved to.
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the roles to move.
try {
    ApiRoleList result = apiInstance.moveRoles(clusterName, roleConfigGroupName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#moveRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleConfigGroupName** | **String**| The name of the group the roles will be moved to. |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles to move. | [optional]

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="moveRolesToBaseGroup"></a>
# **moveRolesToBaseGroup**
> ApiRoleList moveRolesToBaseGroup(clusterName, serviceName, body)

Moves roles to the base role config group.

Moves roles to the base role config group.  The roles can be moved from any role config group belonging to the same service. The role type of the roles may vary. Each role will be moved to its corresponding base group depending on its role type. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the roles to move.
try {
    ApiRoleList result = apiInstance.moveRolesToBaseGroup(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#moveRolesToBaseGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles to move. | [optional]

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="readConfig"></a>
# **readConfig**
> ApiConfigList readConfig(clusterName, roleConfigGroupName, serviceName, view)

Returns the current revision of the config for the specified role config group.

Returns the current revision of the config for the specified role config group. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the role config group.
String serviceName = "serviceName_example"; // String | 
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.readConfig(clusterName, roleConfigGroupName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#readConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleConfigGroupName** | **String**| The name of the role config group. |
 **serviceName** | **String**|  |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoleConfigGroup"></a>
# **readRoleConfigGroup**
> ApiRoleConfigGroup readRoleConfigGroup(clusterName, roleConfigGroupName, serviceName)

Returns the information for a role config group.

Returns the information for a role config group. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the requested group.
String serviceName = "serviceName_example"; // String | 
try {
    ApiRoleConfigGroup result = apiInstance.readRoleConfigGroup(clusterName, roleConfigGroupName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#readRoleConfigGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleConfigGroupName** | **String**| The name of the requested group. |
 **serviceName** | **String**|  |

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoleConfigGroups"></a>
# **readRoleConfigGroups**
> ApiRoleConfigGroupList readRoleConfigGroups(clusterName, serviceName)

Returns the information for all role config groups for a given cluster and service.

Returns the information for all role config groups for a given cluster and service. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
try {
    ApiRoleConfigGroupList result = apiInstance.readRoleConfigGroups(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#readRoleConfigGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |

### Return type

[**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoles"></a>
# **readRoles**
> ApiRoleList readRoles(clusterName, roleConfigGroupName, serviceName)

Returns all roles in the given role config group.

Returns all roles in the given role config group. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the role config group.
String serviceName = "serviceName_example"; // String | 
try {
    ApiRoleList result = apiInstance.readRoles(clusterName, roleConfigGroupName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#readRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleConfigGroupName** | **String**| The name of the role config group. |
 **serviceName** | **String**|  |

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateConfig"></a>
# **updateConfig**
> ApiConfigList updateConfig(clusterName, roleConfigGroupName, serviceName, message, body)

Updates the config for the given role config group.

Updates the config for the given role config group.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the role config group.
String serviceName = "serviceName_example"; // String | 
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | The new config information for the group.
try {
    ApiConfigList result = apiInstance.updateConfig(clusterName, roleConfigGroupName, serviceName, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#updateConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleConfigGroupName** | **String**| The name of the role config group. |
 **serviceName** | **String**|  |
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| The new config information for the group. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateRoleConfigGroup"></a>
# **updateRoleConfigGroup**
> ApiRoleConfigGroup updateRoleConfigGroup(clusterName, roleConfigGroupName, serviceName, message, body)

Updates an existing role config group.

Updates an existing role config group <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleConfigGroupsResourceApi apiInstance = new RoleConfigGroupsResourceApi();
String clusterName = "clusterName_example"; // String | 
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the group to update.
String serviceName = "serviceName_example"; // String | 
String message = "message_example"; // String | The optional message describing the changes.
ApiRoleConfigGroup body = new ApiRoleConfigGroup(); // ApiRoleConfigGroup | The updated role config group.
try {
    ApiRoleConfigGroup result = apiInstance.updateRoleConfigGroup(clusterName, roleConfigGroupName, serviceName, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleConfigGroupsResourceApi#updateRoleConfigGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **roleConfigGroupName** | **String**| The name of the group to update. |
 **serviceName** | **String**|  |
 **message** | **String**| The optional message describing the changes. | [optional]
 **body** | [**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)| The updated role config group. | [optional]

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

