
# ApiMigrateRolesArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roleNamesToMigrate** | **List&lt;String&gt;** | The list of role names to migrate. |  [optional]
**destinationHostId** | **String** | The ID of the host to which the roles should be migrated. |  [optional]
**clearStaleRoleData** | **Boolean** | Delete existing stale role data, if any. For example, when migrating a NameNode, if the destination host has stale data in the NameNode data directories (possibly because a NameNode role was previously located there), this stale data will be deleted before migrating the role. |  [optional]



