
# ApiHostsPerfInspectorArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceHostList** | [**ApiHostNameList**](ApiHostNameList.md) | Required list of host names which&#39;ll act as source for running network diagnostics test. |  [optional]
**targetHostList** | [**ApiHostNameList**](ApiHostNameList.md) | Required list of host names which&#39;ll act as target for running network diagnostics test. |  [optional]
**pingArgs** | [**ApiPerfInspectorPingArgs**](ApiPerfInspectorPingArgs.md) | Optional ping request arguments. If not specified, default arguments will be used for ping test. |  [optional]



