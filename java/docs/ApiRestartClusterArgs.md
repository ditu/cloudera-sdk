
# ApiRestartClusterArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**restartOnlyStaleServices** | **Boolean** | Only restart services that have stale configuration and their dependent services. |  [optional]
**redeployClientConfiguration** | **Boolean** | Re-deploy client configuration for all services in the cluster. |  [optional]
**restartServiceNames** | **List&lt;String&gt;** | Only restart services that are specified and their dependent services. Available since V11. |  [optional]



