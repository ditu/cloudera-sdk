
# ApiUser2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The username, which is unique within a Cloudera Manager installation. |  [optional]
**password** | **String** | Returns the user password. &lt;p&gt; Passwords are not returned when querying user information, so this property will always be empty when reading information from a server. |  [optional]
**authRoles** | [**List&lt;ApiAuthRoleRef&gt;**](ApiAuthRoleRef.md) | A list of ApiAuthRole that this user possesses. |  [optional]
**pwHash** | **String** | NOTE: Only available in the \&quot;export\&quot; view |  [optional]
**pwSalt** | [**BigDecimal**](BigDecimal.md) | NOTE: Only available in the \&quot;export\&quot; view |  [optional]
**pwLogin** | **Boolean** | NOTE: Only available in the \&quot;export\&quot; view |  [optional]



