
# ApiEnableLlamaRmArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**llama1HostId** | **String** | HostId of the host on which the first Llama role will be created. |  [optional]
**llama1RoleName** | **String** | Name of the first Llama role to be created (optional). |  [optional]
**llama2HostId** | **String** | HostId of the host on which the second Llama role will be created. |  [optional]
**llama2RoleName** | **String** | Name of the second Llama role to be created (optional). |  [optional]
**zkServiceName** | **String** | Name of the ZooKeeper service that will be used for auto-failover. Only relevant when enabling Llama RM in HA mode (i.e., when two Llama roles are being created). This argument may be omitted if the ZooKeeper dependency for Impala is already configured. |  [optional]
**skipRestart** | **Boolean** | Skip the restart of Yarn, Impala, and their dependent services, and don&#39;t deploy client configuration. Default is false (i.e., by default, the services are restarted and client configuration is deployed). |  [optional]



