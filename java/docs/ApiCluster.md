
# ApiCluster

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the cluster. &lt;p&gt; Immutable since API v6. &lt;p&gt; Prior to API v6, will contain the display name of the cluster. |  [optional]
**displayName** | **String** | The display name of the cluster that is shown in the UI. &lt;p&gt; Available since API v6. |  [optional]
**version** | [**ApiClusterVersion**](ApiClusterVersion.md) | The CDH version of the cluster. |  [optional]
**fullVersion** | **String** | The full CDH version of the cluster. The expected format is three dot separated version numbers, e.g. \&quot;4.2.1\&quot; or \&quot;5.0.0\&quot;. The full version takes precedence over the version field during cluster creation. &lt;p&gt; Available since API v6. |  [optional]
**maintenanceMode** | **Boolean** | Readonly. Whether the cluster is in maintenance mode. Available since API v2. |  [optional]
**maintenanceOwners** | [**List&lt;ApiEntityType&gt;**](ApiEntityType.md) | Readonly. The list of objects that trigger this cluster to be in maintenance mode. Available since API v2. |  [optional]
**services** | [**List&lt;ApiService&gt;**](ApiService.md) | Optional. Used during import/export of settings. |  [optional]
**parcels** | [**List&lt;ApiParcel&gt;**](ApiParcel.md) | Optional. Used during import/export of settings. Available since API v4. |  [optional]
**clusterUrl** | **String** | Readonly. Link into the Cloudera Manager web UI for this specific cluster. &lt;p&gt; Available since API v10. |  [optional]
**hostsUrl** | **String** | Readonly. Link into the Cloudera Manager web UI for host table for this cluster. &lt;p&gt; Available since API v11. |  [optional]
**entityStatus** | [**ApiEntityStatus**](ApiEntityStatus.md) | Readonly. The entity status for this cluster. Available since API v11. |  [optional]
**uuid** | **String** | Readonly. The UUID of the cluster. &lt;p&gt; Available since API v15. |  [optional]



