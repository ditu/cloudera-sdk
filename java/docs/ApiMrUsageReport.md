
# ApiMrUsageReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiMrUsageReportRow&gt;**](ApiMrUsageReportRow.md) | A list of per-user usage information at the requested time granularity. |  [optional]



