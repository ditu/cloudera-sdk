
# ApiCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**BigDecimal**](BigDecimal.md) | The command ID. |  [optional]
**name** | **String** | The command name. |  [optional]
**startTime** | **String** | The start time. |  [optional]
**endTime** | **String** | The end time, if the command is finished. |  [optional]
**active** | **Boolean** | Whether the command is currently active. |  [optional]
**success** | **Boolean** | If the command is finished, whether it was successful. |  [optional]
**resultMessage** | **String** | If the command is finished, the result message. |  [optional]
**resultDataUrl** | **String** | URL to the command&#39;s downloadable result data, if any exists. |  [optional]
**clusterRef** | [**ApiClusterRef**](ApiClusterRef.md) | Reference to the cluster (for cluster commands only). |  [optional]
**serviceRef** | [**ApiServiceRef**](ApiServiceRef.md) | Reference to the service (for service commands only). |  [optional]
**roleRef** | [**ApiRoleRef**](ApiRoleRef.md) | Reference to the role (for role commands only). |  [optional]
**hostRef** | [**ApiHostRef**](ApiHostRef.md) | Reference to the host (for host commands only). |  [optional]
**parent** | [**ApiCommand**](ApiCommand.md) | Reference to the parent command, if any. |  [optional]
**children** | [**ApiCommandList**](ApiCommandList.md) | List of child commands. Only available in the full view. &lt;p&gt; The list contains only the summary view of the children. |  [optional]
**canRetry** | **Boolean** | If the command can be retried. Available since V11 |  [optional]



