
# ApiTenantUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenantName** | **String** | Name of the tenant. |  [optional]
**cpuUtilizationPercentage** | [**BigDecimal**](BigDecimal.md) | Percentage of CPU resource used by workloads. |  [optional]
**memoryUtilizationPercentage** | [**BigDecimal**](BigDecimal.md) | Percentage of memory used by workloads. |  [optional]



