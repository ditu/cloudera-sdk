
# ApiHostRefList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiHostRef&gt;**](ApiHostRef.md) |  |  [optional]



