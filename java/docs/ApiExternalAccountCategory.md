
# ApiExternalAccountCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Represents an identifier for a category. |  [optional]
**displayName** | **String** | Represents a localized display name for a category. |  [optional]
**description** | **String** | Represents a localized description for a category. |  [optional]



