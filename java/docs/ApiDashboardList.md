
# ApiDashboardList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiDashboard&gt;**](ApiDashboard.md) |  |  [optional]



