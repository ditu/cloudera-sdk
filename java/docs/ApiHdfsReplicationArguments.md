
# ApiHdfsReplicationArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceService** | [**ApiServiceRef**](ApiServiceRef.md) | The service to replicate from. |  [optional]
**sourcePath** | **String** | The path to replicate. |  [optional]
**destinationPath** | **String** | The destination to replicate to. |  [optional]
**mapreduceServiceName** | **String** | The mapreduce service to use for the replication job. |  [optional]
**schedulerPoolName** | **String** | Name of the scheduler pool to use when submitting the MapReduce job. Currently supports the capacity and fair schedulers. The option is ignored if a different scheduler is configured. |  [optional]
**userName** | **String** | The user which will execute the MapReduce job. Required if running with Kerberos enabled. |  [optional]
**sourceUser** | **String** | The user which will perform operations on source cluster. Required if running with Kerberos enabled. |  [optional]
**numMaps** | [**BigDecimal**](BigDecimal.md) | The number of mappers to use for the mapreduce replication job. |  [optional]
**dryRun** | **Boolean** | Whether to perform a dry run. Defaults to false. |  [optional]
**bandwidthPerMap** | [**BigDecimal**](BigDecimal.md) | The maximum bandwidth (in MB) per mapper in the mapreduce replication job. |  [optional]
**abortOnError** | **Boolean** | Whether to abort on a replication failure. Defaults to false. |  [optional]
**removeMissingFiles** | **Boolean** | Whether to delete destination files that are missing in source. Defaults to false. |  [optional]
**preserveReplicationCount** | **Boolean** | Whether to preserve the HDFS replication count. Defaults to false. |  [optional]
**preserveBlockSize** | **Boolean** | Whether to preserve the HDFS block size. Defaults to false. |  [optional]
**preservePermissions** | **Boolean** | Whether to preserve the HDFS owner, group and permissions. Defaults to false. Starting from V10, it also preserves ACLs. Defaults to null (no preserve). ACLs is preserved if both clusters enable ACL support, and replication ignores any ACL related failures. |  [optional]
**logPath** | **String** | The HDFS path where the replication log files should be written to. |  [optional]
**skipChecksumChecks** | **Boolean** | Whether to skip checksum based file validation during replication. Defaults to false. |  [optional]
**skipListingChecksumChecks** | **Boolean** | Whether to skip checksum based file comparison during replication. Defaults to false. |  [optional]
**skipTrash** | **Boolean** | Whether to permanently delete destination files that are missing in source. Defaults to null. |  [optional]
**replicationStrategy** | [**ReplicationStrategy**](ReplicationStrategy.md) | The strategy for distributing the file replication tasks among the mappers of the MR job associated with a replication. Default is ReplicationStrategy#STATIC. |  [optional]
**preserveXAttrs** | **Boolean** | Whether to preserve XAttrs, default to false This is introduced in V10. To preserve XAttrs, both CDH versions should be &gt;&#x3D; 5.2. Replication fails if either cluster does not support XAttrs. |  [optional]
**exclusionFilters** | **List&lt;String&gt;** | Specify regular expression strings to match full paths of files and directories matching source paths and exclude them from the replication. Optional. Available since V11. |  [optional]
**raiseSnapshotDiffFailures** | **Boolean** |  |  [optional]



