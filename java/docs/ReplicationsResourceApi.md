# ReplicationsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**collectDiagnosticData**](ReplicationsResourceApi.md#collectDiagnosticData) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/collectDiagnosticData | Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.
[**createSchedules**](ReplicationsResourceApi.md#createSchedules) | **POST** /clusters/{clusterName}/services/{serviceName}/replications | Creates one or more replication schedules.
[**deleteAllSchedules**](ReplicationsResourceApi.md#deleteAllSchedules) | **DELETE** /clusters/{clusterName}/services/{serviceName}/replications | Deletes all existing replication schedules.
[**deleteSchedule**](ReplicationsResourceApi.md#deleteSchedule) | **DELETE** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Deletes an existing replication schedule.
[**getReplicationState**](ReplicationsResourceApi.md#getReplicationState) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/replicationState | returns the replication state.
[**readHistory**](ReplicationsResourceApi.md#readHistory) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/history | Returns a list of commands triggered by a schedule.
[**readSchedule**](ReplicationsResourceApi.md#readSchedule) | **GET** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Returns information for a specific replication schedule.
[**readSchedules**](ReplicationsResourceApi.md#readSchedules) | **GET** /clusters/{clusterName}/services/{serviceName}/replications | Returns information for all replication schedules.
[**runCopyListing**](ReplicationsResourceApi.md#runCopyListing) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/hdfsCopyListing | Run the hdfs copy listing command.
[**runSchedule**](ReplicationsResourceApi.md#runSchedule) | **POST** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId}/run | Run the schedule immediately.
[**updateSchedule**](ReplicationsResourceApi.md#updateSchedule) | **PUT** /clusters/{clusterName}/services/{serviceName}/replications/{scheduleId} | Updates an existing replication schedule.


<a name="collectDiagnosticData"></a>
# **collectDiagnosticData**
> ApiCommand collectDiagnosticData(clusterName, scheduleId, serviceName, view, body)

Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.

Collect diagnostic data for a schedule, optionally for a subset of commands on that schedule, matched by schedule ID.  The returned command's resultDataUrl property, upon the commands completion, will refer to the generated diagnostic data. Available since API v11.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
BigDecimal scheduleId = new BigDecimal(); // BigDecimal | Schedule ID
String serviceName = "serviceName_example"; // String | The service name.
String view = "summary"; // String | view to materialize
ApiReplicationDiagnosticsCollectionArgs body = new ApiReplicationDiagnosticsCollectionArgs(); // ApiReplicationDiagnosticsCollectionArgs | Replication collection arguments
try {
    ApiCommand result = apiInstance.collectDiagnosticData(clusterName, scheduleId, serviceName, view, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#collectDiagnosticData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **scheduleId** | **BigDecimal**| Schedule ID |
 **serviceName** | **String**| The service name. |
 **view** | **String**| view to materialize | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]
 **body** | [**ApiReplicationDiagnosticsCollectionArgs**](ApiReplicationDiagnosticsCollectionArgs.md)| Replication collection arguments | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createSchedules"></a>
# **createSchedules**
> ApiReplicationScheduleList createSchedules(clusterName, serviceName, body)

Creates one or more replication schedules.

Creates one or more replication schedules. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
ApiReplicationScheduleList body = new ApiReplicationScheduleList(); // ApiReplicationScheduleList | List of the replication schedules to create.
try {
    ApiReplicationScheduleList result = apiInstance.createSchedules(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#createSchedules");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **body** | [**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)| List of the replication schedules to create. | [optional]

### Return type

[**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAllSchedules"></a>
# **deleteAllSchedules**
> ApiReplicationScheduleList deleteAllSchedules(clusterName, serviceName)

Deletes all existing replication schedules.

Deletes all existing replication schedules. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiReplicationScheduleList result = apiInstance.deleteAllSchedules(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#deleteAllSchedules");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteSchedule"></a>
# **deleteSchedule**
> ApiReplicationSchedule deleteSchedule(clusterName, scheduleId, serviceName)

Deletes an existing replication schedule.

Deletes an existing replication schedule. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
BigDecimal scheduleId = new BigDecimal(); // BigDecimal | Id of an existing replication schedule.
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiReplicationSchedule result = apiInstance.deleteSchedule(clusterName, scheduleId, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#deleteSchedule");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **scheduleId** | **BigDecimal**| Id of an existing replication schedule. |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiReplicationSchedule**](ApiReplicationSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReplicationState"></a>
# **getReplicationState**
> ApiReplicationState getReplicationState(clusterName, serviceName, view)

returns the replication state.

returns the replication state. for example if incremental export is enabled, etc

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
String view = "summary"; // String | view to materialize
try {
    ApiReplicationState result = apiInstance.getReplicationState(clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#getReplicationState");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **view** | **String**| view to materialize | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiReplicationState**](ApiReplicationState.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readHistory"></a>
# **readHistory**
> ApiReplicationCommandList readHistory(clusterName, scheduleId, serviceName, limit, offset, view)

Returns a list of commands triggered by a schedule.

Returns a list of commands triggered by a schedule.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
BigDecimal scheduleId = new BigDecimal(); // BigDecimal | Id of an existing replication schedule.
String serviceName = "serviceName_example"; // String | The service name.
Integer limit = 20; // Integer | Maximum number of commands to retrieve.
Integer offset = 0; // Integer | Index of first command to retrieve.
String view = "summary"; // String | The view to materialize.
try {
    ApiReplicationCommandList result = apiInstance.readHistory(clusterName, scheduleId, serviceName, limit, offset, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#readHistory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **scheduleId** | **BigDecimal**| Id of an existing replication schedule. |
 **serviceName** | **String**| The service name. |
 **limit** | **Integer**| Maximum number of commands to retrieve. | [optional] [default to 20]
 **offset** | **Integer**| Index of first command to retrieve. | [optional] [default to 0]
 **view** | **String**| The view to materialize. | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiReplicationCommandList**](ApiReplicationCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readSchedule"></a>
# **readSchedule**
> ApiReplicationSchedule readSchedule(clusterName, scheduleId, serviceName, view)

Returns information for a specific replication schedule.

Returns information for a specific replication schedule. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
BigDecimal scheduleId = new BigDecimal(); // BigDecimal | Id of an existing replication schedule.
String serviceName = "serviceName_example"; // String | The service name.
String view = "summary"; // String | The view to materialize.
try {
    ApiReplicationSchedule result = apiInstance.readSchedule(clusterName, scheduleId, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#readSchedule");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **scheduleId** | **BigDecimal**| Id of an existing replication schedule. |
 **serviceName** | **String**| The service name. |
 **view** | **String**| The view to materialize. | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiReplicationSchedule**](ApiReplicationSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readSchedules"></a>
# **readSchedules**
> ApiReplicationScheduleList readSchedules(clusterName, serviceName, limits)

Returns information for all replication schedules.

Returns information for all replication schedules. <p> Available since API v11.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
String limits = "limits_example"; // String | Various limits on contents of returned schedules.
try {
    ApiReplicationScheduleList result = apiInstance.readSchedules(clusterName, serviceName, limits);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#readSchedules");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **limits** | **String**| Various limits on contents of returned schedules. | [optional]

### Return type

[**ApiReplicationScheduleList**](ApiReplicationScheduleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="runCopyListing"></a>
# **runCopyListing**
> ApiCommand runCopyListing(clusterName, serviceName, body)

Run the hdfs copy listing command.

Run the hdfs copy listing command <p> The copy listing command will be triggered with the provided arguments <p> Available since API v18. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
String body = "body_example"; // String | 
try {
    ApiCommand result = apiInstance.runCopyListing(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#runCopyListing");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **body** | **String**|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="runSchedule"></a>
# **runSchedule**
> ApiCommand runSchedule(clusterName, scheduleId, serviceName, dryRun)

Run the schedule immediately.

Run the schedule immediately. <p> The replication command will be triggered with the configured arguments, and will be recorded in the schedule's history. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
BigDecimal scheduleId = new BigDecimal(); // BigDecimal | Id of an existing replication schedule.
String serviceName = "serviceName_example"; // String | The service name.
Boolean dryRun = false; // Boolean | Whether to execute a dry run.
try {
    ApiCommand result = apiInstance.runSchedule(clusterName, scheduleId, serviceName, dryRun);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#runSchedule");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **scheduleId** | **BigDecimal**| Id of an existing replication schedule. |
 **serviceName** | **String**| The service name. |
 **dryRun** | **Boolean**| Whether to execute a dry run. | [optional] [default to false]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateSchedule"></a>
# **updateSchedule**
> ApiReplicationSchedule updateSchedule(clusterName, scheduleId, serviceName, body)

Updates an existing replication schedule.

Updates an existing replication schedule. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ReplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ReplicationsResourceApi apiInstance = new ReplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
BigDecimal scheduleId = new BigDecimal(); // BigDecimal | Id of an existing replication schedule.
String serviceName = "serviceName_example"; // String | The service name.
ApiReplicationSchedule body = new ApiReplicationSchedule(); // ApiReplicationSchedule | 
try {
    ApiReplicationSchedule result = apiInstance.updateSchedule(clusterName, scheduleId, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationsResourceApi#updateSchedule");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **scheduleId** | **BigDecimal**| Id of an existing replication schedule. |
 **serviceName** | **String**| The service name. |
 **body** | [**ApiReplicationSchedule**](ApiReplicationSchedule.md)|  | [optional]

### Return type

[**ApiReplicationSchedule**](ApiReplicationSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

