
# ApiHdfsUsageReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastUpdateTime** | **String** | The time when HDFS usage info was last collected. No information beyond this time can be provided. |  [optional]
**items** | [**List&lt;ApiHdfsUsageReportRow&gt;**](ApiHdfsUsageReportRow.md) | A list of per-user usage information at the requested time granularity. |  [optional]



