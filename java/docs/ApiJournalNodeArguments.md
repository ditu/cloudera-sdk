
# ApiJournalNodeArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jnName** | **String** | Name of new JournalNode to be created. (Optional) |  [optional]
**jnHostId** | **String** | ID of the host where the new JournalNode will be created. |  [optional]
**jnEditsDir** | **String** | Path to the JournalNode edits directory. Need not be specified if it is already set at RoleConfigGroup level. |  [optional]



