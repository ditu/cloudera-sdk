
# ApiHost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostId** | **String** | A unique host identifier. This is not the same as the hostname (FQDN). It is a distinct value that remains the same even if the hostname changes. |  [optional]
**ipAddress** | **String** | The host IP address. This field is not mutable after the initial creation. |  [optional]
**hostname** | **String** | The hostname. This field is not mutable after the initial creation. |  [optional]
**rackId** | **String** | The rack ID for this host. |  [optional]
**lastHeartbeat** | **String** | Readonly. Requires \&quot;full\&quot; view. When the host agent sent the last heartbeat. |  [optional]
**roleRefs** | [**List&lt;ApiRoleRef&gt;**](ApiRoleRef.md) | Readonly. Requires \&quot;full\&quot; view. The list of roles assigned to this host. |  [optional]
**healthSummary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Readonly. Requires \&quot;full\&quot; view. The high-level health status of this host. |  [optional]
**healthChecks** | [**List&lt;ApiHealthCheck&gt;**](ApiHealthCheck.md) | Readonly. Requires \&quot;full\&quot; view. The list of health checks performed on the host, with their results. |  [optional]
**hostUrl** | **String** | Readonly. A URL into the Cloudera Manager web UI for this specific host. |  [optional]
**maintenanceMode** | **Boolean** | Readonly. Whether the host is in maintenance mode. Available since API v2. |  [optional]
**commissionState** | [**ApiCommissionState**](ApiCommissionState.md) | Readonly. The commission state of this role. Available since API v2. |  [optional]
**maintenanceOwners** | [**List&lt;ApiEntityType&gt;**](ApiEntityType.md) | Readonly. The list of objects that trigger this host to be in maintenance mode. Available since API v2. |  [optional]
**config** | [**ApiConfigList**](ApiConfigList.md) |  |  [optional]
**numCores** | [**BigDecimal**](BigDecimal.md) | Readonly. The number of logical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v4. |  [optional]
**numPhysicalCores** | [**BigDecimal**](BigDecimal.md) | Readonly. The number of physical CPU cores on this host. Only populated after the host has heartbeated to the server. Available since API v9. |  [optional]
**totalPhysMemBytes** | [**BigDecimal**](BigDecimal.md) | Readonly. The amount of physical RAM on this host, in bytes. Only populated after the host has heartbeated to the server. Available since API v4. |  [optional]
**entityStatus** | [**ApiEntityStatus**](ApiEntityStatus.md) | Readonly. The entity status for this host. Available since API v11. |  [optional]
**clusterRef** | [**ApiClusterRef**](ApiClusterRef.md) | Readonly. A reference to the enclosing cluster. This might be null if the host is not yet assigned to a cluster. Available since API v11. |  [optional]



