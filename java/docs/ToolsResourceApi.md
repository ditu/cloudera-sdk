# ToolsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**echo**](ToolsResourceApi.md#echo) | **GET** /tools/echo | Echoes the provided message back to the caller.
[**echoError**](ToolsResourceApi.md#echoError) | **GET** /tools/echoError | Throws an error containing the given input message.


<a name="echo"></a>
# **echo**
> ApiEcho echo(message)

Echoes the provided message back to the caller.

Echoes the provided message back to the caller.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ToolsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ToolsResourceApi apiInstance = new ToolsResourceApi();
String message = "Hello, World!"; // String | The message to echo back
try {
    ApiEcho result = apiInstance.echo(message);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ToolsResourceApi#echo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **String**| The message to echo back | [optional] [default to Hello, World!]

### Return type

[**ApiEcho**](ApiEcho.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="echoError"></a>
# **echoError**
> ApiEcho echoError(message)

Throws an error containing the given input message.

Throws an error containing the given input message. This is what an error response looks like.  <pre>    {      \"message\": \"An error message\",      \"causes\": [ \"A list of causes\", \"Potentially null\" ]    }  </pre>  <p>The <em>message</em> field contains a description of the error. The <em>causes</em> field, if not null, contains a list of causes for the error. </p>  <p>Note that this <strong>never</strong> returns an echoMessage. Instead, the result (and all error results) has the above structure. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ToolsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ToolsResourceApi apiInstance = new ToolsResourceApi();
String message = "Default error message"; // String | The error message to echo
try {
    ApiEcho result = apiInstance.echoError(message);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ToolsResourceApi#echoError");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **String**| The error message to echo | [optional] [default to Default error message]

### Return type

[**ApiEcho**](ApiEcho.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

