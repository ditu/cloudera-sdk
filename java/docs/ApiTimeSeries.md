
# ApiTimeSeries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**ApiTimeSeriesMetadata**](ApiTimeSeriesMetadata.md) | Metadata for the metric. |  [optional]
**data** | [**List&lt;ApiTimeSeriesData&gt;**](ApiTimeSeriesData.md) | List of metric data points. |  [optional]



