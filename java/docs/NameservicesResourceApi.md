# NameservicesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMetrics**](NameservicesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices/{nameservice}/metrics | Fetch metric readings for a particular nameservice.
[**listNameservices**](NameservicesResourceApi.md#listNameservices) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices | List the nameservices of an HDFS service.
[**readNameservice**](NameservicesResourceApi.md#readNameservice) | **GET** /clusters/{clusterName}/services/{serviceName}/nameservices/{nameservice} | Retrieve information about a nameservice.


<a name="getMetrics"></a>
# **getMetrics**
> ApiMetricList getMetrics(clusterName, nameservice, serviceName, from, metrics, to, view)

Fetch metric readings for a particular nameservice.

Fetch metric readings for a particular nameservice. <p> By default, this call will look up all metrics available. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available, even if no readings are available in the requested window.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.NameservicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

NameservicesResourceApi apiInstance = new NameservicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String nameservice = "nameservice_example"; // String | The nameservice.
String serviceName = "serviceName_example"; // String | The service name.
String from = "from_example"; // String | Start of the period to query.
List<String> metrics = Arrays.asList("metrics_example"); // List<String> | Filter for which metrics to query.
String to = "now"; // String | End of the period to query.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiMetricList result = apiInstance.getMetrics(clusterName, nameservice, serviceName, from, metrics, to, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NameservicesResourceApi#getMetrics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **nameservice** | **String**| The nameservice. |
 **serviceName** | **String**| The service name. |
 **from** | **String**| Start of the period to query. | [optional]
 **metrics** | [**List&lt;String&gt;**](String.md)| Filter for which metrics to query. | [optional]
 **to** | **String**| End of the period to query. | [optional] [default to now]
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listNameservices"></a>
# **listNameservices**
> ApiNameserviceList listNameservices(clusterName, serviceName, view)

List the nameservices of an HDFS service.

List the nameservices of an HDFS service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.NameservicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

NameservicesResourceApi apiInstance = new NameservicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiNameserviceList result = apiInstance.listNameservices(clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NameservicesResourceApi#listNameservices");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiNameserviceList**](ApiNameserviceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readNameservice"></a>
# **readNameservice**
> ApiNameservice readNameservice(clusterName, nameservice, serviceName, view)

Retrieve information about a nameservice.

Retrieve information about a nameservice.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.NameservicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

NameservicesResourceApi apiInstance = new NameservicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String nameservice = "nameservice_example"; // String | The nameservice to retrieve.
String serviceName = "serviceName_example"; // String | The service name.
String view = "summary"; // String | The view to materialize. Defaults to 'full'.
try {
    ApiNameservice result = apiInstance.readNameservice(clusterName, nameservice, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NameservicesResourceApi#readNameservice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **nameservice** | **String**| The nameservice to retrieve. |
 **serviceName** | **String**| The service name. |
 **view** | **String**| The view to materialize. Defaults to 'full'. | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiNameservice**](ApiNameservice.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

