
# ApiShutdownReadiness

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | [**ShutdownReadinessState**](ShutdownReadinessState.md) | Shutdown readiness state |  [optional]



