
# ApiActivityStatus

## Enum


* `UNKNOWN` (value: `"UNKNOWN"`)

* `SUBMITTED` (value: `"SUBMITTED"`)

* `STARTED` (value: `"STARTED"`)

* `SUSPENDED` (value: `"SUSPENDED"`)

* `FAILED` (value: `"FAILED"`)

* `KILLED` (value: `"KILLED"`)

* `SUCCEEDED` (value: `"SUCCEEDED"`)

* `ASSUMED_SUCCEEDED` (value: `"ASSUMED_SUCCEEDED"`)



