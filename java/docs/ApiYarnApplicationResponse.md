
# ApiYarnApplicationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applications** | [**List&lt;ApiYarnApplication&gt;**](ApiYarnApplication.md) | The list of applications for this response. |  [optional]
**warnings** | **List&lt;String&gt;** | This list of warnings for this response. |  [optional]



