
# ApiEntityStatus

## Enum


* `UNKNOWN` (value: `"UNKNOWN"`)

* `NONE` (value: `"NONE"`)

* `STOPPED` (value: `"STOPPED"`)

* `DOWN` (value: `"DOWN"`)

* `UNKNOWN_HEALTH` (value: `"UNKNOWN_HEALTH"`)

* `DISABLED_HEALTH` (value: `"DISABLED_HEALTH"`)

* `CONCERNING_HEALTH` (value: `"CONCERNING_HEALTH"`)

* `BAD_HEALTH` (value: `"BAD_HEALTH"`)

* `GOOD_HEALTH` (value: `"GOOD_HEALTH"`)

* `STARTING` (value: `"STARTING"`)

* `STOPPING` (value: `"STOPPING"`)

* `HISTORY_NOT_AVAILABLE` (value: `"HISTORY_NOT_AVAILABLE"`)



