
# ApiActivityType

## Enum


* `UNKNOWN` (value: `"UNKNOWN"`)

* `OOZIE` (value: `"OOZIE"`)

* `PIG` (value: `"PIG"`)

* `HIVE` (value: `"HIVE"`)

* `MR` (value: `"MR"`)

* `STREAMING` (value: `"STREAMING"`)



