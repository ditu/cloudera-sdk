
# ApiReplicationScheduleList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiReplicationSchedule&gt;**](ApiReplicationSchedule.md) |  |  [optional]



