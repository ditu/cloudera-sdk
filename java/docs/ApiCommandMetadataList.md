
# ApiCommandMetadataList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiCommandMetadata&gt;**](ApiCommandMetadata.md) | The list of command metadata objects. |  [optional]



