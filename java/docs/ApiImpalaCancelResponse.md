
# ApiImpalaCancelResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warning** | **String** | The warning response. If there was no warning this will be null. |  [optional]



