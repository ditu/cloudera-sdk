
# ApiRoleConfigGroupRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roleConfigGroupName** | **String** | The name of the role config group, which uniquely identifies it in a CM installation. |  [optional]



