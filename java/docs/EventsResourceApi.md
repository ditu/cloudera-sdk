# EventsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**readEvent**](EventsResourceApi.md#readEvent) | **GET** /events/{eventId} | Returns a specific event in the system.
[**readEvents**](EventsResourceApi.md#readEvents) | **GET** /events | Allows you to query events in the system.


<a name="readEvent"></a>
# **readEvent**
> ApiEvent readEvent(eventId)

Returns a specific event in the system.

Returns a specific event in the system

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.EventsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

EventsResourceApi apiInstance = new EventsResourceApi();
String eventId = "eventId_example"; // String | The UUID of the event to read
try {
    ApiEvent result = apiInstance.readEvent(eventId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventsResourceApi#readEvent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **String**| The UUID of the event to read |

### Return type

[**ApiEvent**](ApiEvent.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readEvents"></a>
# **readEvents**
> ApiEventQueryResult readEvents(maxResults, query, resultOffset)

Allows you to query events in the system.

Allows you to query events in the system.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.EventsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

EventsResourceApi apiInstance = new EventsResourceApi();
Integer maxResults = 100; // Integer | The maximum number of events to return.
String query = "query_example"; // String | The query to perform to find events in the system. It accepts querying the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>alert==true</dt> <dd>looks for alerts.</dd> <dt>alert==true;attributes.host!=flaky.mysite.com</dt> <dd>looks for alerts, but exclude those with the host attribute of \"flaky.mysite.com\".</dd> <dt>category==log_event;attributes.log_level==ERROR</dt> <dd>looks for error log events. Event attribute matching is case sensitive.</dd> <dt>attributes.service==hbase1;content==hlog</dt> <dd>looks for any events from the \"hbase1\" service that mention \"hlog\".</dd> <dt>attributes.service==hbase1;content!=hlog</dt> <dd>looks for any events from the \"hbase1\" service that do not mention \"hlog\".<br/> A query must not contain only negative constraints (<em>!=</em>). It returns empty results because there is nothing to perform exclusion on.</dd> <dt>attributes.role_type==NAMENODE;severity==critical important</dt> <dd>looks for any important or critical events related to all NameNodes.</dd> <dt>severity==critical;timeReceived=ge=2012-05-04T00:00;timeReceived=lt=2012-05-04T00:10</dt> <dd>looks for critical events received between the given 10 minute range. <br/> When polling for events, use <em>timeReceived</em> instead of <em>timeOccurred</em> because events arrive out of order.</dd> </dl>  You may query any fields present in the ApiEvent object. You can also query by event attribute values using the <em>attributes.*</em> syntax. Values for date time fields (e.g. <em>timeOccurred</em>, <em>timeReceived</em>) should be ISO8601 timestamps. <p> The other valid comparators are <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively. These comparators are only applicable for date time fields.
Integer resultOffset = 0; // Integer | Specified the offset of events to return.
try {
    ApiEventQueryResult result = apiInstance.readEvents(maxResults, query, resultOffset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventsResourceApi#readEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **maxResults** | **Integer**| The maximum number of events to return. | [optional] [default to 100]
 **query** | **String**| The query to perform to find events in the system. It accepts querying the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>alert==true</dt> <dd>looks for alerts.</dd> <dt>alert==true;attributes.host!=flaky.mysite.com</dt> <dd>looks for alerts, but exclude those with the host attribute of \"flaky.mysite.com\".</dd> <dt>category==log_event;attributes.log_level==ERROR</dt> <dd>looks for error log events. Event attribute matching is case sensitive.</dd> <dt>attributes.service==hbase1;content==hlog</dt> <dd>looks for any events from the \"hbase1\" service that mention \"hlog\".</dd> <dt>attributes.service==hbase1;content!=hlog</dt> <dd>looks for any events from the \"hbase1\" service that do not mention \"hlog\".<br/> A query must not contain only negative constraints (<em>!=</em>). It returns empty results because there is nothing to perform exclusion on.</dd> <dt>attributes.role_type==NAMENODE;severity==critical important</dt> <dd>looks for any important or critical events related to all NameNodes.</dd> <dt>severity==critical;timeReceived=ge=2012-05-04T00:00;timeReceived=lt=2012-05-04T00:10</dt> <dd>looks for critical events received between the given 10 minute range. <br/> When polling for events, use <em>timeReceived</em> instead of <em>timeOccurred</em> because events arrive out of order.</dd> </dl>  You may query any fields present in the ApiEvent object. You can also query by event attribute values using the <em>attributes.*</em> syntax. Values for date time fields (e.g. <em>timeOccurred</em>, <em>timeReceived</em>) should be ISO8601 timestamps. <p> The other valid comparators are <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively. These comparators are only applicable for date time fields. | [optional]
 **resultOffset** | **Integer**| Specified the offset of events to return. | [optional] [default to 0]

### Return type

[**ApiEventQueryResult**](ApiEventQueryResult.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

