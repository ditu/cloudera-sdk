
# ApiReplicationCommandList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiReplicationCommand&gt;**](ApiReplicationCommand.md) |  |  [optional]



