
# ApiHiveUDF

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**database** | **String** | Name of the database to which this UDF belongs. |  [optional]
**signature** | **String** | UDF signature, includes the UDF name and parameter types. |  [optional]



