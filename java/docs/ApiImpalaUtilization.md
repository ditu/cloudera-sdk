
# ApiImpalaUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalQueries** | [**BigDecimal**](BigDecimal.md) | Total number of queries submitted to Impala. |  [optional]
**successfulQueries** | [**BigDecimal**](BigDecimal.md) | Number of queries that finished successfully. |  [optional]
**oomQueries** | [**BigDecimal**](BigDecimal.md) | Number of queries that failed due to insufficient memory. |  [optional]
**timeOutQueries** | [**BigDecimal**](BigDecimal.md) | Number of queries that timed out while waiting for resources in a pool. |  [optional]
**rejectedQueries** | [**BigDecimal**](BigDecimal.md) | Number of queries that were rejected by Impala because the pool was full. |  [optional]
**successfulQueriesPercentage** | [**BigDecimal**](BigDecimal.md) | Percentage of queries that finished successfully. |  [optional]
**oomQueriesPercentage** | [**BigDecimal**](BigDecimal.md) | Percentage of queries that failed due to insufficient memory. |  [optional]
**timeOutQueriesPercentage** | [**BigDecimal**](BigDecimal.md) | Percentage of queries that timed out while waiting for resources in a pool. |  [optional]
**rejectedQueriesPercentage** | [**BigDecimal**](BigDecimal.md) | Percentage of queries that were rejected by Impala because the pool was full. |  [optional]
**avgWaitTimeInQueue** | [**BigDecimal**](BigDecimal.md) | Average time, in milliseconds, spent by a query in an Impala pool while waiting for resources. |  [optional]
**peakAllocationTimestampMS** | [**BigDecimal**](BigDecimal.md) | The time when Impala reserved the maximum amount of memory for queries. |  [optional]
**maxAllocatedMemory** | [**BigDecimal**](BigDecimal.md) | The maximum memory (in bytes) that was reserved by Impala for executing queries. |  [optional]
**maxAllocatedMemoryPercentage** | [**BigDecimal**](BigDecimal.md) | The maximum percentage of memory that was reserved by Impala for executing queries. |  [optional]
**utilizedAtMaxAllocated** | [**BigDecimal**](BigDecimal.md) | The amount of memory (in bytes) used by Impala for running queries at the time when maximum memory was reserved. |  [optional]
**utilizedAtMaxAllocatedPercentage** | [**BigDecimal**](BigDecimal.md) | The percentage of memory used by Impala for running queries at the time when maximum memory was reserved. |  [optional]
**peakUsageTimestampMS** | [**BigDecimal**](BigDecimal.md) | The time when Impala used the maximum amount of memory for queries. |  [optional]
**maxUtilizedMemory** | [**BigDecimal**](BigDecimal.md) | The maximum memory (in bytes) that was used by Impala for executing queries. |  [optional]
**maxUtilizedMemoryPercentage** | [**BigDecimal**](BigDecimal.md) | The maximum percentage of memory that was used by Impala for executing queries. |  [optional]
**allocatedAtMaxUtilized** | [**BigDecimal**](BigDecimal.md) | The amount of memory (in bytes) reserved by Impala at the time when it was using the maximum memory for executing queries. |  [optional]
**allocatedAtMaxUtilizedPercentage** | [**BigDecimal**](BigDecimal.md) | The percentage of memory reserved by Impala at the time when it was using the maximum memory for executing queries. |  [optional]
**distributionUtilizedByImpalaDaemon** | [**ApiImpalaUtilizationHistogram**](ApiImpalaUtilizationHistogram.md) | Distribution of memory used per Impala daemon for executing queries at the time Impala used the maximum memory. |  [optional]
**distributionAllocatedByImpalaDaemon** | [**ApiImpalaUtilizationHistogram**](ApiImpalaUtilizationHistogram.md) | Distribution of memory reserved per Impala daemon for executing queries at the time Impala used the maximum memory. |  [optional]
**tenantUtilizations** | [**ApiImpalaTenantUtilizationList**](ApiImpalaTenantUtilizationList.md) | A list of tenant utilization reports. |  [optional]
**errorMessage** | **String** | error message of utilization report. |  [optional]



