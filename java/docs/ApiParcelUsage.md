
# ApiParcelUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**racks** | [**List&lt;ApiParcelUsageRack&gt;**](ApiParcelUsageRack.md) | The racks that contain hosts that are part of this cluster. |  [optional]
**parcels** | [**List&lt;ApiParcelUsageParcel&gt;**](ApiParcelUsageParcel.md) | The parcel&#39;s that are activated and/or in-use on this cluster. |  [optional]



