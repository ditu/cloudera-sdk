
# ApiClusterVersion

## Enum


* `CDH3` (value: `"CDH3"`)

* `CDH3U4X` (value: `"CDH3u4X"`)

* `CDH4` (value: `"CDH4"`)

* `CDH5` (value: `"CDH5"`)

* `CDH6` (value: `"CDH6"`)

* `UNKNOWN` (value: `"UNKNOWN"`)



