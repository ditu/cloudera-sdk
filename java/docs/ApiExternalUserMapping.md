
# ApiExternalUserMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the external mapping |  [optional]
**type** | [**ApiExternalUserMappingType**](ApiExternalUserMappingType.md) | The type of the external mapping |  [optional]
**uuid** | **String** | Readonly. The UUID of the authRole. &lt;p&gt; |  [optional]
**authRoles** | [**List&lt;ApiAuthRoleRef&gt;**](ApiAuthRoleRef.md) | A list of ApiAuthRole that this user possesses. |  [optional]



