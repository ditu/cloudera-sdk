
# ApiClusterTemplateInstantiator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clusterName** | **String** |  |  [optional]
**hosts** | [**List&lt;ApiClusterTemplateHostInfo&gt;**](ApiClusterTemplateHostInfo.md) |  |  [optional]
**variables** | [**List&lt;ApiClusterTemplateVariable&gt;**](ApiClusterTemplateVariable.md) |  |  [optional]
**roleConfigGroups** | [**List&lt;ApiClusterTemplateRoleConfigGroupInfo&gt;**](ApiClusterTemplateRoleConfigGroupInfo.md) |  |  [optional]



