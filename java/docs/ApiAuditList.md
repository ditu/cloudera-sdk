
# ApiAuditList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiAudit&gt;**](ApiAudit.md) |  |  [optional]



