
# ApiUserSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The username associated with the session. &lt;p&gt; This will be the same value shown to the logged in user in the UI, which will normally be the same value they typed when logging in, but it is possible that in certain external authentication scenarios, it will differ from that value. |  [optional]
**remoteAddr** | **String** | The remote IP address for the session. &lt;p&gt; This will be the remote IP address for the last request made as part of this session. It is not guaranteed to be the same IP address as was previously used, or the address used to initiate the session. |  [optional]
**lastRequest** | **String** | The date and time of the last request received as part of this session. &lt;p&gt; This will be returned in ISO 8601 format from the REST API. |  [optional]



