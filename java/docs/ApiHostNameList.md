
# ApiHostNameList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | **List&lt;String&gt;** | List of host names. |  [optional]



