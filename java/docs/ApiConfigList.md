
# ApiConfigList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiConfig&gt;**](ApiConfig.md) |  |  [optional]



