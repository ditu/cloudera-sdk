
# ApiServiceRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peerName** | **String** | The name of the CM peer corresponding to the remote CM that manages the referenced service. This should only be set when referencing a remote service. |  [optional]
**clusterName** | **String** | The enclosing cluster for this service. |  [optional]
**serviceName** | **String** | The service name. |  [optional]



