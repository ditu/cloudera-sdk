
# ApiImpalaQueryAttributeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiImpalaQueryAttribute&gt;**](ApiImpalaQueryAttribute.md) |  |  [optional]



