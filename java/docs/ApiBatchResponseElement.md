
# ApiBatchResponseElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusCode** | [**BigDecimal**](BigDecimal.md) | Read-only. The HTTP status code of the response. |  [optional]
**response** | **Object** | Read-only. The (optional) serialized body of the response, in the representation produced by the corresponding API endpoint, such as application/json. |  [optional]



