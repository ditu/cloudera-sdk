
# ApiYarnTenantUtilizationList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiYarnTenantUtilization&gt;**](ApiYarnTenantUtilization.md) |  |  [optional]



