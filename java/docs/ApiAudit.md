
# ApiAudit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **String** | When the audit event was captured. |  [optional]
**service** | **String** | Service name associated with this audit. |  [optional]
**username** | **String** | The user who performed this operation. |  [optional]
**impersonator** | **String** | The impersonating user (or the proxy user) who submitted this operation. This is usually applicable when using services like Oozie or Hue, who can be configured to impersonate other users and submit jobs. |  [optional]
**ipAddress** | **String** | The IP address that the client connected from. |  [optional]
**command** | **String** | The command/operation that was requested. |  [optional]
**resource** | **String** | The resource that the operation was performed on. |  [optional]
**operationText** | **String** | The full text of the requested operation. E.g. the full Hive query. &lt;p&gt; Available since API v5. |  [optional]
**allowed** | **Boolean** | Whether the operation was allowed or denied by the authorization system. |  [optional]



