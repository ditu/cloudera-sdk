
# ApiClusterList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiCluster&gt;**](ApiCluster.md) |  |  [optional]



