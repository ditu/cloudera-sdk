
# ApiTimeSeriesEntityAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the of the attribute. This name uniquely identifies this attribute. |  [optional]
**displayName** | **String** | Display name of the attribute. |  [optional]
**description** | **String** | Description of the attribute. |  [optional]
**isValueCaseSensitive** | **Boolean** | Returns whether to treat attribute values as case-sensitive. |  [optional]



