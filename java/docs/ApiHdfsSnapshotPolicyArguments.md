
# ApiHdfsSnapshotPolicyArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pathPatterns** | **List&lt;String&gt;** | The path patterns specifying the paths. Paths matching any of them will be eligible for snapshot creation. &lt;p/&gt; The pattern matching characters that can be specific are those supported by HDFS. please see the documentation for HDFS globs for more details. |  [optional]



