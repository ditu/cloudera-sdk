
# ApiParcelUsageHost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostRef** | [**ApiHostRef**](ApiHostRef.md) | A reference to the corresponding Host object. |  [optional]
**roles** | [**List&lt;ApiParcelUsageRole&gt;**](ApiParcelUsageRole.md) | A collection of the roles present on the host. |  [optional]



