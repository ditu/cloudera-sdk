
# ApiAuthRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  |  [optional]
**clusters** | [**List&lt;ApiClusterRef&gt;**](ApiClusterRef.md) |  |  [optional]
**users** | [**List&lt;ApiUser2Ref&gt;**](ApiUser2Ref.md) |  |  [optional]
**externalUserMappings** | [**List&lt;ApiExternalUserMappingRef&gt;**](ApiExternalUserMappingRef.md) |  |  [optional]
**baseRole** | [**ApiAuthRoleRef**](ApiAuthRoleRef.md) | A role this user possesses. In Cloudera Enterprise Datahub Edition, possible values are: &lt;ul&gt; &lt;li&gt;&lt;b&gt;ROLE_ADMIN&lt;/b&gt;&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_USER&lt;/b&gt;&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_LIMITED&lt;/b&gt;: Added in Cloudera Manager 5.0&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_OPERATOR&lt;/b&gt;: Added in Cloudera Manager 5.1&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_CONFIGURATOR&lt;/b&gt;: Added in Cloudera Manager 5.1&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_CLUSTER_ADMIN&lt;/b&gt;: Added in Cloudera Manager 5.2&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_BDR_ADMIN&lt;/b&gt;: Added in Cloudera Manager 5.2&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_NAVIGATOR_ADMIN&lt;/b&gt;: Added in Cloudera Manager 5.2&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_USER_ADMIN&lt;/b&gt;: Added in Cloudera Manager 5.2&lt;/li&gt; &lt;li&gt;&lt;b&gt;ROLE_KEY_ADMIN&lt;/b&gt;: Added in Cloudera Manager 5.5&lt;/li&gt; &lt;/ul&gt; An empty role implies ROLE_USER. &lt;p&gt; |  [optional]
**uuid** | **String** | Readonly. The UUID of the authRole. &lt;p&gt; |  [optional]
**isCustom** | **Boolean** |  |  [optional]



