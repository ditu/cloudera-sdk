
# ApiReplicationSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**BigDecimal**](BigDecimal.md) | The schedule id. |  [optional]
**displayName** | **String** | The schedule display name. |  [optional]
**description** | **String** | The schedule description. |  [optional]
**startTime** | **String** | The time at which the scheduled activity is triggered for the first time. |  [optional]
**endTime** | **String** | The time after which the scheduled activity will no longer be triggered. |  [optional]
**interval** | [**BigDecimal**](BigDecimal.md) | The duration between consecutive triggers of a scheduled activity. |  [optional]
**intervalUnit** | [**ApiScheduleInterval**](ApiScheduleInterval.md) | The unit for the repeat interval. |  [optional]
**nextRun** | **String** | Readonly. The time the scheduled command will run next. |  [optional]
**paused** | **Boolean** | The paused state for the schedule. The scheduled activity will not be triggered as long as the scheduled is paused. |  [optional]
**alertOnStart** | **Boolean** | Whether to alert on start of the scheduled activity. |  [optional]
**alertOnSuccess** | **Boolean** | Whether to alert on successful completion of the scheduled activity. |  [optional]
**alertOnFail** | **Boolean** | Whether to alert on failure of the scheduled activity. |  [optional]
**alertOnAbort** | **Boolean** | Whether to alert on abort of the scheduled activity. |  [optional]
**hdfsArguments** | [**ApiHdfsReplicationArguments**](ApiHdfsReplicationArguments.md) | Arguments for HDFS replication commands. |  [optional]
**hiveArguments** | [**ApiHiveReplicationArguments**](ApiHiveReplicationArguments.md) | Arguments for Hive replication commands. |  [optional]
**hdfsCloudArguments** | [**ApiHdfsCloudReplicationArguments**](ApiHdfsCloudReplicationArguments.md) | Arguments for HDFS cloud replication commands. |  [optional]
**history** | [**List&lt;ApiReplicationCommand&gt;**](ApiReplicationCommand.md) | List of active and/or finished commands for this schedule. |  [optional]
**active** | **Boolean** | Read-only field that is true if this schedule is currently active, false if not. Available since API v11. |  [optional]
**hiveCloudArguments** | [**ApiHiveCloudReplicationArguments**](ApiHiveCloudReplicationArguments.md) | Arguments for Hive cloud replication commands. |  [optional]



