# RoleCommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**formatCommand**](RoleCommandsResourceApi.md#formatCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsFormat | Format HDFS NameNodes.
[**hdfsBootstrapStandByCommand**](RoleCommandsResourceApi.md#hdfsBootstrapStandByCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsBootstrapStandBy | Bootstrap HDFS stand-by NameNodes.
[**hdfsEnterSafemode**](RoleCommandsResourceApi.md#hdfsEnterSafemode) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsEnterSafemode | Enter safemode for namenodes.
[**hdfsFinalizeMetadataUpgrade**](RoleCommandsResourceApi.md#hdfsFinalizeMetadataUpgrade) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsFinalizeMetadataUpgrade | Finalize HDFS NameNode metadata upgrade.
[**hdfsInitializeAutoFailoverCommand**](RoleCommandsResourceApi.md#hdfsInitializeAutoFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsInitializeAutoFailover | Initialize HDFS HA failover controller metadata.
[**hdfsInitializeSharedDirCommand**](RoleCommandsResourceApi.md#hdfsInitializeSharedDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsInitializeSharedDir | Initialize HDFS NameNodes&#39; shared edit directory.
[**hdfsLeaveSafemode**](RoleCommandsResourceApi.md#hdfsLeaveSafemode) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsLeaveSafemode | Leave safemode for namenodes.
[**hdfsSaveNamespace**](RoleCommandsResourceApi.md#hdfsSaveNamespace) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hdfsSaveNamespace | Save namespace for namenodes.
[**jmapDump**](RoleCommandsResourceApi.md#jmapDump) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jmapDump | Run the jmapDump diagnostic command.
[**jmapHisto**](RoleCommandsResourceApi.md#jmapHisto) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jmapHisto | Run the jmapHisto diagnostic command.
[**jstack**](RoleCommandsResourceApi.md#jstack) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/jstack | Run the jstack diagnostic command.
[**lsof**](RoleCommandsResourceApi.md#lsof) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/lsof | Run the lsof diagnostic command.
[**refreshCommand**](RoleCommandsResourceApi.md#refreshCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/refresh | Refresh a role&#39;s data.
[**restartCommand**](RoleCommandsResourceApi.md#restartCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/restart | Restart a set of role instances.
[**roleCommandByName**](RoleCommandsResourceApi.md#roleCommandByName) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/{commandName} | Execute a role command by name.
[**startCommand**](RoleCommandsResourceApi.md#startCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/start | Start a set of role instances.
[**stopCommand**](RoleCommandsResourceApi.md#stopCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/stop | Stop a set of role instances.
[**syncHueDbCommand**](RoleCommandsResourceApi.md#syncHueDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/hueSyncDb | Create / update the Hue database schema.
[**zooKeeperCleanupCommand**](RoleCommandsResourceApi.md#zooKeeperCleanupCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/zooKeeperCleanup | Cleanup a list of ZooKeeper server roles.
[**zooKeeperInitCommand**](RoleCommandsResourceApi.md#zooKeeperInitCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/roleCommands/zooKeeperInit | Initialize a list of ZooKeeper server roles.


<a name="formatCommand"></a>
# **formatCommand**
> ApiBulkCommandList formatCommand(clusterName, serviceName, body)

Format HDFS NameNodes.

Format HDFS NameNodes. <p> Submit a format request to a list of NameNodes on a service. Note that trying to format a previously formatted NameNode will fail. <p> Note about high availability: when two NameNodes are working in an HA pair, only one of them should be formatted. <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the NameNodes to format.
try {
    ApiBulkCommandList result = apiInstance.formatCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#formatCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the NameNodes to format. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsBootstrapStandByCommand"></a>
# **hdfsBootstrapStandByCommand**
> ApiBulkCommandList hdfsBootstrapStandByCommand(clusterName, serviceName, body)

Bootstrap HDFS stand-by NameNodes.

Bootstrap HDFS stand-by NameNodes. <p> Submit a request to synchronize HDFS NameNodes with their assigned HA partners. The command requires that the target NameNodes are part of existing HA pairs, which can be accomplished by setting the nameservice configuration parameter in the NameNode's configuration. <p> The HA partner must already be formatted and running for this command to run.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the stand-by NameNodes to bootstrap.
try {
    ApiBulkCommandList result = apiInstance.hdfsBootstrapStandByCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#hdfsBootstrapStandByCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the stand-by NameNodes to bootstrap. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsEnterSafemode"></a>
# **hdfsEnterSafemode**
> ApiBulkCommandList hdfsEnterSafemode(clusterName, serviceName, body)

Enter safemode for namenodes.

Enter safemode for namenodes <p/> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | NameNodes for which to enter safemode.
try {
    ApiBulkCommandList result = apiInstance.hdfsEnterSafemode(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#hdfsEnterSafemode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to enter safemode. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsFinalizeMetadataUpgrade"></a>
# **hdfsFinalizeMetadataUpgrade**
> ApiBulkCommandList hdfsFinalizeMetadataUpgrade(clusterName, serviceName, body)

Finalize HDFS NameNode metadata upgrade.

Finalize HDFS NameNode metadata upgrade. <p/> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | NameNodes for which to finalize the upgrade.
try {
    ApiBulkCommandList result = apiInstance.hdfsFinalizeMetadataUpgrade(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#hdfsFinalizeMetadataUpgrade");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to finalize the upgrade. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsInitializeAutoFailoverCommand"></a>
# **hdfsInitializeAutoFailoverCommand**
> ApiBulkCommandList hdfsInitializeAutoFailoverCommand(clusterName, serviceName, body)

Initialize HDFS HA failover controller metadata.

Initialize HDFS HA failover controller metadata. <p> The controllers being initialized must already exist and be properly configured. The command will make sure the needed data is initialized for the controller to work. <p> Only one controller per nameservice needs to be initialized.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the controllers to initialize.
try {
    ApiBulkCommandList result = apiInstance.hdfsInitializeAutoFailoverCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#hdfsInitializeAutoFailoverCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the controllers to initialize. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsInitializeSharedDirCommand"></a>
# **hdfsInitializeSharedDirCommand**
> ApiBulkCommandList hdfsInitializeSharedDirCommand(clusterName, serviceName, body)

Initialize HDFS NameNodes' shared edit directory.

Initialize HDFS NameNodes' shared edit directory. <p> Shared edit directories are used when two HDFS NameNodes are operating as a high-availability pair. This command initializes the shared directory to include the necessary metadata. <p> The provided role names should reflect one of the NameNodes in the respective HA pair; the role must be stopped and its data directory must already have been formatted. The shared edits directory must be empty for this command to succeed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the NameNodes.
try {
    ApiBulkCommandList result = apiInstance.hdfsInitializeSharedDirCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#hdfsInitializeSharedDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the NameNodes. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsLeaveSafemode"></a>
# **hdfsLeaveSafemode**
> ApiBulkCommandList hdfsLeaveSafemode(clusterName, serviceName, body)

Leave safemode for namenodes.

Leave safemode for namenodes <p/> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | NameNodes for which to leave safemode.
try {
    ApiBulkCommandList result = apiInstance.hdfsLeaveSafemode(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#hdfsLeaveSafemode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to leave safemode. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsSaveNamespace"></a>
# **hdfsSaveNamespace**
> ApiBulkCommandList hdfsSaveNamespace(clusterName, serviceName, body)

Save namespace for namenodes.

Save namespace for namenodes <p/> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | NameNodes for which to save namespace.
try {
    ApiBulkCommandList result = apiInstance.hdfsSaveNamespace(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#hdfsSaveNamespace");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| NameNodes for which to save namespace. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jmapDump"></a>
# **jmapDump**
> ApiBulkCommandList jmapDump(clusterName, serviceName, body)

Run the jmapDump diagnostic command.

Run the jmapDump diagnostic command. The command runs the jmap utility to capture a dump of the role's java heap. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to jmap.
try {
    ApiBulkCommandList result = apiInstance.jmapDump(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#jmapDump");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jmapHisto"></a>
# **jmapHisto**
> ApiBulkCommandList jmapHisto(clusterName, serviceName, body)

Run the jmapHisto diagnostic command.

Run the jmapHisto diagnostic command. The command runs the jmap utility to capture a histogram of the objects on the role's java heap. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to jmap.
try {
    ApiBulkCommandList result = apiInstance.jmapHisto(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#jmapHisto");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jstack"></a>
# **jstack**
> ApiBulkCommandList jstack(clusterName, serviceName, body)

Run the jstack diagnostic command.

Run the jstack diagnostic command. The command runs the jstack utility to capture a role's java thread stacks. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to jstack.
try {
    ApiBulkCommandList result = apiInstance.jstack(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#jstack");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jstack. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="lsof"></a>
# **lsof**
> ApiBulkCommandList lsof(clusterName, serviceName, body)

Run the lsof diagnostic command.

Run the lsof diagnostic command. This command runs the lsof utility to list a role's open files. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to lsof.
try {
    ApiBulkCommandList result = apiInstance.lsof(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#lsof");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to lsof. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="refreshCommand"></a>
# **refreshCommand**
> ApiBulkCommandList refreshCommand(clusterName, serviceName, body)

Refresh a role's data.

Refresh a role's data. <p> For MapReduce services, this command should be executed on JobTracker roles. It refreshes the role's queue and node information. <p> For HDFS services, this command should be executed on NameNode or DataNode roles. For NameNodes, it refreshes the role's node list. For DataNodes, it refreshes the role's data directory list and other configuration. <p> For YARN services, this command should be executed on ResourceManager roles. It refreshes the role's queue and node information. <p> Available since API v1. DataNode data directories refresh available since API v10.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the roles.
try {
    ApiBulkCommandList result = apiInstance.refreshCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#refreshCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="restartCommand"></a>
# **restartCommand**
> ApiBulkCommandList restartCommand(clusterName, serviceName, body)

Restart a set of role instances.

Restart a set of role instances <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The name of the roles to restart.
try {
    ApiBulkCommandList result = apiInstance.restartCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#restartCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The name of the roles to restart. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="roleCommandByName"></a>
# **roleCommandByName**
> ApiBulkCommandList roleCommandByName(clusterName, commandName, serviceName, body)

Execute a role command by name.

Execute a role command by name. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String commandName = "commandName_example"; // String | the name of command to execute.
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the roles to run this command on.
try {
    ApiBulkCommandList result = apiInstance.roleCommandByName(clusterName, commandName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#roleCommandByName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **commandName** | **String**| the name of command to execute. |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the roles to run this command on. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="startCommand"></a>
# **startCommand**
> ApiBulkCommandList startCommand(clusterName, serviceName, body)

Start a set of role instances.

Start a set of role instances. <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the roles to start.
try {
    ApiBulkCommandList result = apiInstance.startCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#startCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles to start. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="stopCommand"></a>
# **stopCommand**
> ApiBulkCommandList stopCommand(clusterName, serviceName, body)

Stop a set of role instances.

Stop a set of role instances. <p> Bulk command operations are not atomic, and may contain partial failures. The returned list will contain references to all successful commands, and a list of error messages identifying the roles on which the command failed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The role type.
try {
    ApiBulkCommandList result = apiInstance.stopCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#stopCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The role type. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="syncHueDbCommand"></a>
# **syncHueDbCommand**
> ApiBulkCommandList syncHueDbCommand(clusterName, serviceName, body)

Create / update the Hue database schema.

Create / update the Hue database schema. <p> This command is to be run whenever a new database has been specified or, as necessary, after an upgrade. <p> This request should be sent to Hue servers only.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the Hue server roles.
try {
    ApiBulkCommandList result = apiInstance.syncHueDbCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#syncHueDbCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the Hue server roles. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="zooKeeperCleanupCommand"></a>
# **zooKeeperCleanupCommand**
> ApiBulkCommandList zooKeeperCleanupCommand(clusterName, serviceName, body)

Cleanup a list of ZooKeeper server roles.

Cleanup a list of ZooKeeper server roles. <p> This command removes snapshots and transaction log files kept by ZooKeeper for backup purposes. Refer to the ZooKeeper documentation for more details.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the roles.
try {
    ApiBulkCommandList result = apiInstance.zooKeeperCleanupCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#zooKeeperCleanupCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="zooKeeperInitCommand"></a>
# **zooKeeperInitCommand**
> ApiBulkCommandList zooKeeperInitCommand(clusterName, serviceName, body)

Initialize a list of ZooKeeper server roles.

Initialize a list of ZooKeeper server roles. <p> This applies to ZooKeeper services from CDH4. Before ZooKeeper server roles can be used, they need to be initialized.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.RoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

RoleCommandsResourceApi apiInstance = new RoleCommandsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The names of the roles.
try {
    ApiBulkCommandList result = apiInstance.zooKeeperInitCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoleCommandsResourceApi#zooKeeperInitCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The names of the roles. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

