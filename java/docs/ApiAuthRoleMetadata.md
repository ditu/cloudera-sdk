
# ApiAuthRoleMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  |  [optional]
**uuid** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**authorities** | [**List&lt;ApiAuthRoleAuthority&gt;**](ApiAuthRoleAuthority.md) |  |  [optional]
**allowedScopes** | **List&lt;String&gt;** |  |  [optional]



