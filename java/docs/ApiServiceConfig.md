
# ApiServiceConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiConfig&gt;**](ApiConfig.md) |  |  [optional]
**roleTypeConfigs** | [**List&lt;ApiRoleTypeConfig&gt;**](ApiRoleTypeConfig.md) | List of role type configurations. Only available up to API v2. |  [optional]



