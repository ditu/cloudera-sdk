
# ApiReplicationDiagnosticsCollectionArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commands** | [**ApiCommandList**](ApiCommandList.md) | Commands to limit diagnostics to. By default, the most recent 10 commands on the schedule will be used. |  [optional]
**ticketNumber** | **String** | Ticket number to which this bundle must be associated with. |  [optional]
**comments** | **String** | Additional comments for the bundle. |  [optional]
**phoneHome** | **Boolean** | Whether the diagnostics bundle must be uploaded to Cloudera. |  [optional]



