
# ApiImpalaUtilizationHistogramBinList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiImpalaUtilizationHistogramBin&gt;**](ApiImpalaUtilizationHistogramBin.md) |  |  [optional]



