# AuthServiceResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autoAssignRoles**](AuthServiceResourceApi.md#autoAssignRoles) | **PUT** /cm/authService/autoAssignRoles | Automatically assign roles to hosts and create the roles for the Authentication Service.
[**autoConfigure**](AuthServiceResourceApi.md#autoConfigure) | **PUT** /cm/authService/autoConfigure | Automatically configures roles of the Authentication Service.
[**delete**](AuthServiceResourceApi.md#delete) | **DELETE** /cm/authService | Delete the Authentication Service.
[**enterMaintenanceMode**](AuthServiceResourceApi.md#enterMaintenanceMode) | **POST** /cm/authService/commands/enterMaintenanceMode | Put the Authentication Service into maintenance mode.
[**exitMaintenanceMode**](AuthServiceResourceApi.md#exitMaintenanceMode) | **POST** /cm/authService/commands/exitMaintenanceMode | Take the Authentication Service out of maintenance mode.
[**listActiveCommands**](AuthServiceResourceApi.md#listActiveCommands) | **GET** /cm/authService/commands | List active Authentication Service commands.
[**listRoleTypes**](AuthServiceResourceApi.md#listRoleTypes) | **GET** /cm/authService/roleTypes | List the supported role types for the Authentication Service.
[**readService**](AuthServiceResourceApi.md#readService) | **GET** /cm/authService | Retrieve information about the Authentication Services.
[**readServiceConfig**](AuthServiceResourceApi.md#readServiceConfig) | **GET** /cm/authService/config | 
[**restartCommand**](AuthServiceResourceApi.md#restartCommand) | **POST** /cm/authService/commands/restart | Restart the Authentication Service.
[**setup**](AuthServiceResourceApi.md#setup) | **PUT** /cm/authService | Setup the Authentication Service.
[**startCommand**](AuthServiceResourceApi.md#startCommand) | **POST** /cm/authService/commands/start | Start the Authentication Service.
[**stopCommand**](AuthServiceResourceApi.md#stopCommand) | **POST** /cm/authService/commands/stop | Stop the Authentication Service.
[**updateServiceConfig**](AuthServiceResourceApi.md#updateServiceConfig) | **PUT** /cm/authService/config | 


<a name="autoAssignRoles"></a>
# **autoAssignRoles**
> autoAssignRoles()

Automatically assign roles to hosts and create the roles for the Authentication Service.

Automatically assign roles to hosts and create the roles for the Authentication Service. <p> Assignments are done based on number of hosts in the deployment and hardware specifications. If no hosts are part of the deployment, an exception will be thrown preventing any role assignments. Existing roles will be taken into account and their assignments will be not be modified. The deployment should not have any clusters when calling this endpoint. If it does, an exception will be thrown preventing any role assignments.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    apiInstance.autoAssignRoles();
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#autoAssignRoles");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="autoConfigure"></a>
# **autoConfigure**
> autoConfigure()

Automatically configures roles of the Authentication Service.

Automatically configures roles of the Authentication Service. <p> Overwrites some existing configurations. Only default role config groups must exist before calling this endpoint. Other role config groups must not exist. If they do, an exception will be thrown preventing any configuration. Ignores any clusters (and their services and roles) colocated with the Authentication Service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    apiInstance.autoConfigure();
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#autoConfigure");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="delete"></a>
# **delete**
> ApiService delete()

Delete the Authentication Service.

Delete the Authentication Service. <p> This method will fail if a CMS instance doesn't already exist.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiService result = apiInstance.delete();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#delete");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="enterMaintenanceMode"></a>
# **enterMaintenanceMode**
> ApiCommand enterMaintenanceMode()

Put the Authentication Service into maintenance mode.

Put the Authentication Service into maintenance mode. This is a synchronous command. The result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiCommand result = apiInstance.enterMaintenanceMode();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#enterMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaintenanceMode"></a>
# **exitMaintenanceMode**
> ApiCommand exitMaintenanceMode()

Take the Authentication Service out of maintenance mode.

Take the Authentication Service out of maintenance mode. This is a synchronous command. The result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiCommand result = apiInstance.exitMaintenanceMode();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#exitMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listActiveCommands"></a>
# **listActiveCommands**
> ApiCommandList listActiveCommands(view)

List active Authentication Service commands.

List active Authentication Service commands.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiCommandList result = apiInstance.listActiveCommands(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#listActiveCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listRoleTypes"></a>
# **listRoleTypes**
> ApiRoleTypeList listRoleTypes()

List the supported role types for the Authentication Service.

List the supported role types for the Authentication Service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiRoleTypeList result = apiInstance.listRoleTypes();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#listRoleTypes");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleTypeList**](ApiRoleTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readService"></a>
# **readService**
> ApiService readService()

Retrieve information about the Authentication Services.

Retrieve information about the Authentication Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiService result = apiInstance.readService();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#readService");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readServiceConfig"></a>
# **readServiceConfig**
> ApiServiceConfig readServiceConfig(view)





### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
String view = "summary"; // String | 
try {
    ApiServiceConfig result = apiInstance.readServiceConfig(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#readServiceConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="restartCommand"></a>
# **restartCommand**
> ApiCommand restartCommand()

Restart the Authentication Service.

Restart the Authentication Service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiCommand result = apiInstance.restartCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#restartCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="setup"></a>
# **setup**
> ApiService setup(body)

Setup the Authentication Service.

Setup the Authentication Service. <p> Configure the Auth Service instance with the information given in the ApiService. The provided configuration data can be used to set up host mappings for each role, and required configuration such as database connection information for specific roles. <p> This method needs a valid CM license to be installed beforehand. <p> This method does not start any services or roles. <p> This method will fail if a Auth Service instance already exists. <p> Available role types: <ul> <li>AUTHSRV</li> </ul>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
ApiService body = new ApiService(); // ApiService | Role configuration overrides.
try {
    ApiService result = apiInstance.setup(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#setup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiService**](ApiService.md)| Role configuration overrides. | [optional]

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="startCommand"></a>
# **startCommand**
> ApiCommand startCommand()

Start the Authentication Service.

Start the Authentication Service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiCommand result = apiInstance.startCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#startCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stopCommand"></a>
# **stopCommand**
> ApiCommand stopCommand()

Stop the Authentication Service.

Stop the Authentication Service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
try {
    ApiCommand result = apiInstance.stopCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#stopCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateServiceConfig"></a>
# **updateServiceConfig**
> ApiServiceConfig updateServiceConfig(message, body)





### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceResourceApi apiInstance = new AuthServiceResourceApi();
String message = "message_example"; // String | 
ApiServiceConfig body = new ApiServiceConfig(); // ApiServiceConfig | 
try {
    ApiServiceConfig result = apiInstance.updateServiceConfig(message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceResourceApi#updateServiceConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **String**|  | [optional]
 **body** | [**ApiServiceConfig**](ApiServiceConfig.md)|  | [optional]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

