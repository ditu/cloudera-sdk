
# ApiHealthCheck

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Unique name of this health check. |  [optional]
**summary** | [**ApiHealthSummary**](ApiHealthSummary.md) | The summary status of this check. |  [optional]
**explanation** | **String** | The explanation of this health check. Available since v11. |  [optional]
**suppressed** | **Boolean** | Whether this health test is suppressed. A suppressed health test is not considered when computing an entity&#39;s overall health. Available since v11. |  [optional]



