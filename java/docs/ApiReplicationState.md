
# ApiReplicationState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incrementalExportEnabled** | **Boolean** | returns if incremental export is enabled for the given Hive service. Not applicable for HDFS service. |  [optional]



