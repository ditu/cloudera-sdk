
# ApiExternalAccountType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Represents the immutable name for this account. |  [optional]
**categoryName** | **String** | Represents the category of this account. |  [optional]
**type** | **String** | Represents the type for this account. |  [optional]
**displayName** | **String** | Represents the localized display name for this account. |  [optional]
**description** | **String** | Represents the localized description for this account type. |  [optional]
**allowedAccountConfigs** | [**ApiConfigList**](ApiConfigList.md) | Represents the list of allowed account configs. |  [optional]



