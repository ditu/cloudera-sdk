
# ApiDisableJtHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeName** | **String** | Name of the JobTracker that will be active after HA is disabled. |  [optional]



