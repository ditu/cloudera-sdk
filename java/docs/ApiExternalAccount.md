
# ApiExternalAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Represents the intial name of the account; used to uniquely identify this account. |  [optional]
**displayName** | **String** | Represents a modifiable label to identify this account for user-visible purposes. |  [optional]
**createdTime** | **String** | Represents the time of creation for this account. |  [optional]
**lastModifiedTime** | **String** | Represents the last modification time for this account. |  [optional]
**typeName** | **String** | Represents the Type ID of a supported external account type. The type represented by this field dictates which configuration options must be defined for this account. |  [optional]
**accountConfigs** | [**ApiConfigList**](ApiConfigList.md) | Represents the account configuration for this account.  When an account is retrieved from the server, the configs returned must match allowed configuration for the type of this account.  When specified for creation of a new account or for the update of an existing account, this field must include every required configuration parameter specified in the type&#39;s definition, with the account configuration&#39;s value field specified to represent the specific configuration desired for this account. |  [optional]



