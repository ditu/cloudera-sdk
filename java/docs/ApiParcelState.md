
# ApiParcelState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**progress** | [**BigDecimal**](BigDecimal.md) | The progress of the state transition. |  [optional]
**totalProgress** | [**BigDecimal**](BigDecimal.md) | The total amount that #getProgress() needs to get to. |  [optional]
**count** | [**BigDecimal**](BigDecimal.md) | The current hosts that have completed. |  [optional]
**totalCount** | [**BigDecimal**](BigDecimal.md) | The total amount that #getCount() needs to get to. |  [optional]
**errors** | **List&lt;String&gt;** | The errors that exist for this parcel. |  [optional]
**warnings** | **List&lt;String&gt;** | The warnings that exist for this parcel. |  [optional]



