
# ApiTenantUtilizationList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiTenantUtilization&gt;**](ApiTenantUtilization.md) |  |  [optional]



