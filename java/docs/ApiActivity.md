
# ApiActivity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Activity name. |  [optional]
**type** | [**ApiActivityType**](ApiActivityType.md) | Activity type. Whether it&#39;s an MR job, a Pig job, a Hive query, etc. |  [optional]
**parent** | **String** | The name of the parent activity. |  [optional]
**startTime** | **String** | The start time of this activity. |  [optional]
**finishTime** | **String** | The finish time of this activity. |  [optional]
**id** | **String** | Activity id, which is unique within a MapReduce service. |  [optional]
**status** | [**ApiActivityStatus**](ApiActivityStatus.md) | Activity status. |  [optional]
**user** | **String** | The user who submitted this activity. |  [optional]
**group** | **String** | The user-group of this activity. |  [optional]
**inputDir** | **String** | The input data directory of the activity. An HDFS url. |  [optional]
**outputDir** | **String** | The output result directory of the activity. An HDFS url. |  [optional]
**mapper** | **String** | The mapper class. |  [optional]
**combiner** | **String** | The combiner class. |  [optional]
**reducer** | **String** | The reducer class. |  [optional]
**queueName** | **String** | The scheduler queue this activity is in. |  [optional]
**schedulerPriority** | **String** | The scheduler priority of this activity. |  [optional]



