
# ApiHdfsFailoverArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nameservice** | **String** | Nameservice for which to enable automatic failover. |  [optional]
**zooKeeperService** | [**ApiServiceRef**](ApiServiceRef.md) | The ZooKeeper service to use. |  [optional]
**activeFCName** | **String** | Name of the failover controller to create for the active NameNode. |  [optional]
**standByFCName** | **String** | Name of the failover controller to create for the stand-by NameNode. |  [optional]



