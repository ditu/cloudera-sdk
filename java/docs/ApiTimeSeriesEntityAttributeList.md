
# ApiTimeSeriesEntityAttributeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiTimeSeriesEntityAttribute&gt;**](ApiTimeSeriesEntityAttribute.md) |  |  [optional]



