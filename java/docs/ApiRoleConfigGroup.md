
# ApiRoleConfigGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Readonly. The unique name of this role config group. |  [optional]
**roleType** | **String** | Readonly. The type of the roles in this group. |  [optional]
**base** | **Boolean** | Readonly. Indicates whether this is a base group. |  [optional]
**config** | [**ApiConfigList**](ApiConfigList.md) | The configuration for this group. Optional. |  [optional]
**displayName** | **String** | The display name of this group. |  [optional]
**serviceRef** | [**ApiServiceRef**](ApiServiceRef.md) | Readonly. The service reference (service name and cluster name) of this group. |  [optional]



