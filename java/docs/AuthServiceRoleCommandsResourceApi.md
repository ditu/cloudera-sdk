# AuthServiceRoleCommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restartCommand**](AuthServiceRoleCommandsResourceApi.md#restartCommand) | **POST** /cm/authService/roleCommands/restart | Restart a set of Authentication Service roles.
[**startCommand**](AuthServiceRoleCommandsResourceApi.md#startCommand) | **POST** /cm/authService/roleCommands/start | Start a set of Authentication Service roles.
[**stopCommand**](AuthServiceRoleCommandsResourceApi.md#stopCommand) | **POST** /cm/authService/roleCommands/stop | Stop a set of Authentication Service roles.


<a name="restartCommand"></a>
# **restartCommand**
> ApiBulkCommandList restartCommand(body)

Restart a set of Authentication Service roles.

Restart a set of Authentication Service roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRoleCommandsResourceApi apiInstance = new AuthServiceRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The roles to restart.
try {
    ApiBulkCommandList result = apiInstance.restartCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRoleCommandsResourceApi#restartCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to restart. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="startCommand"></a>
# **startCommand**
> ApiBulkCommandList startCommand(body)

Start a set of Authentication Service roles.

Start a set of Authentication Service roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRoleCommandsResourceApi apiInstance = new AuthServiceRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The roles to start.
try {
    ApiBulkCommandList result = apiInstance.startCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRoleCommandsResourceApi#startCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to start. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="stopCommand"></a>
# **stopCommand**
> ApiBulkCommandList stopCommand(body)

Stop a set of Authentication Service roles.

Stop a set of Authentication Service roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRoleCommandsResourceApi apiInstance = new AuthServiceRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The roles to stop.
try {
    ApiBulkCommandList result = apiInstance.stopCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRoleCommandsResourceApi#stopCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to stop. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

