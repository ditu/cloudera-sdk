
# ApiExternalAccountList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiExternalAccount&gt;**](ApiExternalAccount.md) |  |  [optional]



