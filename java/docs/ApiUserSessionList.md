
# ApiUserSessionList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiUserSession&gt;**](ApiUserSession.md) |  |  [optional]



