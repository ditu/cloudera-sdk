
# ApiTimeSeriesEntityType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Returns the name of the entity type. This name uniquely identifies this entity type. |  [optional]
**category** | **String** | Returns the category of the entity type. |  [optional]
**nameForCrossEntityAggregateMetrics** | **String** | Returns the string to use to pluralize the name of the entity for cross entity aggregate metrics. |  [optional]
**displayName** | **String** | Returns the display name of the entity type. |  [optional]
**description** | **String** | Returns the description of the entity type. |  [optional]
**immutableAttributeNames** | **List&lt;String&gt;** | Returns the list of immutable attributes for this entity type. Immutable attributes values for an entity may not change over its lifetime. |  [optional]
**mutableAttributeNames** | **List&lt;String&gt;** | Returns the list of mutable attributes for this entity type. Mutable attributes for an entity may change over its lifetime. |  [optional]
**entityNameFormat** | **List&lt;String&gt;** | Returns a list of attribute names that will be used to construct entity names for entities of this type. The attributes named here must be immutable attributes of this type or a parent type. |  [optional]
**entityDisplayNameFormat** | **String** | Returns a format string that will be used to construct the display name of entities of this type. If this returns null the entity name would be used as the display name.  The entity attribute values are used to replace $attribute name portions of this format string. For example, an entity with roleType \&quot;DATANODE\&quot; and hostname \&quot;foo.com\&quot; will have a display name \&quot;DATANODE (foo.com)\&quot; if the format is \&quot;$roleType ($hostname)\&quot;. |  [optional]
**parentMetricEntityTypeNames** | **List&lt;String&gt;** | Returns a list of metric entity type names which are parents of this metric entity type. A metric entity type inherits the attributes of its ancestors. For example a role metric entity type has its service as a parent. A service metric entity type has a cluster as a parent. The role type inherits its cluster name attribute through its service parent. Only parent ancestors should be returned here. In the example given, only the service metric entity type should be specified in the parent list. |  [optional]



