
# ApiImpalaTenantUtilizationList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiImpalaTenantUtilization&gt;**](ApiImpalaTenantUtilization.md) |  |  [optional]



