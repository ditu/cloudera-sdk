
# ApiYarnTenantUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenantName** | **String** | Name of the tenant. |  [optional]
**avgYarnCpuAllocation** | [**BigDecimal**](BigDecimal.md) | Average number of VCores allocated to YARN applications of the tenant. |  [optional]
**avgYarnCpuUtilization** | [**BigDecimal**](BigDecimal.md) | Average number of VCores used by YARN applications of the tenant. |  [optional]
**avgYarnCpuUnusedCapacity** | [**BigDecimal**](BigDecimal.md) | Average unused VCores of the tenant. |  [optional]
**avgYarnCpuSteadyFairShare** | [**BigDecimal**](BigDecimal.md) | Average steady fair share VCores. |  [optional]
**avgYarnPoolAllocatedCpuDuringContention** | [**BigDecimal**](BigDecimal.md) | Average allocated Vcores with pending containers. |  [optional]
**avgYarnPoolFairShareCpuDuringContention** | [**BigDecimal**](BigDecimal.md) | Average fair share VCores with pending containers. |  [optional]
**avgYarnPoolSteadyFairShareCpuDuringContention** | [**BigDecimal**](BigDecimal.md) | Average steady fair share VCores with pending containers. |  [optional]
**avgYarnContainerWaitRatio** | [**BigDecimal**](BigDecimal.md) | Average percentage of pending containers for the pool during periods of contention. |  [optional]
**avgYarnMemoryAllocation** | [**BigDecimal**](BigDecimal.md) | Average memory allocated to YARN applications of the tenant. |  [optional]
**avgYarnMemoryUtilization** | [**BigDecimal**](BigDecimal.md) | Average memory used by YARN applications of the tenant. |  [optional]
**avgYarnMemoryUnusedCapacity** | [**BigDecimal**](BigDecimal.md) | Average unused memory of the tenant. |  [optional]
**avgYarnMemorySteadyFairShare** | [**BigDecimal**](BigDecimal.md) | Average steady fair share memory. |  [optional]
**avgYarnPoolAllocatedMemoryDuringContention** | [**BigDecimal**](BigDecimal.md) | Average allocated memory with pending containers. |  [optional]
**avgYarnPoolFairShareMemoryDuringContention** | [**BigDecimal**](BigDecimal.md) | Average fair share memory with pending containers. |  [optional]
**avgYarnPoolSteadyFairShareMemoryDuringContention** | [**BigDecimal**](BigDecimal.md) | Average steady fair share memory with pending containers. |  [optional]



