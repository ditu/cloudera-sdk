
# ApiAuthRoleList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiAuthRole&gt;**](ApiAuthRole.md) |  |  [optional]



