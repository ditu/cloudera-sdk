
# ApiTimeSeriesCrossEntityMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxEntityDisplayName** | **String** | The display name of the entity that had the maximum value for the cross-entity aggregate metric. |  [optional]
**maxEntityName** | **String** | The name of the entity that had the maximum value for the cross-entity aggregate metric. &lt;p&gt; Available since API v11. |  [optional]
**minEntityDisplayName** | **String** | The display name of the entity that had the minimum value for the cross-entity aggregate metric. |  [optional]
**minEntityName** | **String** | The name of the entity that had the minimum value for the cross-entity aggregate metric. &lt;p&gt; Available since API v11. |  [optional]
**numEntities** | [**BigDecimal**](BigDecimal.md) | The number of entities covered by this point. For a raw cross-entity point this number is exact. For a rollup point this number is an average, since the number of entities being aggregated can change over the aggregation period. |  [optional]



