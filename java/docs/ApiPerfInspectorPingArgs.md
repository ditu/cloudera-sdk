
# ApiPerfInspectorPingArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pingTimeoutSecs** | [**BigDecimal**](BigDecimal.md) | Timeout in seconds for the ping request to each target host. If not specified, defaults to 10 seconds. |  [optional]
**pingCount** | [**BigDecimal**](BigDecimal.md) | Number of iterations of the ping request to each target host. If not specified, defaults to 10 count. |  [optional]
**pingPacketSizeBytes** | [**BigDecimal**](BigDecimal.md) | Packet size in bytes for each ping request. If not specified, defaults to 56 bytes. |  [optional]



