
# ApiHBaseSnapshotResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processedTableCount** | [**BigDecimal**](BigDecimal.md) | Number of processed tables. |  [optional]
**processedTables** | **List&lt;String&gt;** | The list of processed tables. &lt;p/&gt; This is only available in the full view. |  [optional]
**unprocessedTableCount** | [**BigDecimal**](BigDecimal.md) | Number of unprocessed tables. |  [optional]
**unprocessedTables** | **List&lt;String&gt;** | The list of unprocessed tables. Note that tables that are currently being processed will also be included in this list. &lt;p/&gt; This is only available in the full view. |  [optional]
**createdSnapshotCount** | [**BigDecimal**](BigDecimal.md) | Number of snapshots created. |  [optional]
**createdSnapshots** | [**List&lt;ApiHBaseSnapshot&gt;**](ApiHBaseSnapshot.md) | List of snapshots created. &lt;p/&gt; This is only available in the full view. |  [optional]
**deletedSnapshotCount** | [**BigDecimal**](BigDecimal.md) | Number of snapshots deleted. |  [optional]
**deletedSnapshots** | [**List&lt;ApiHBaseSnapshot&gt;**](ApiHBaseSnapshot.md) | List of snapshots deleted. &lt;p/&gt; This is only available in the full view. |  [optional]
**creationErrorCount** | [**BigDecimal**](BigDecimal.md) | Number of errors detected when creating snapshots. |  [optional]
**creationErrors** | [**List&lt;ApiHBaseSnapshotError&gt;**](ApiHBaseSnapshotError.md) | List of errors encountered when creating snapshots. &lt;p/&gt; This is only available in the full view. |  [optional]
**deletionErrorCount** | [**BigDecimal**](BigDecimal.md) | Number of errors detected when deleting snapshots. |  [optional]
**deletionErrors** | [**List&lt;ApiHBaseSnapshotError&gt;**](ApiHBaseSnapshotError.md) | List of errors encountered when deleting snapshots. &lt;p/&gt; This is only available in the full view. |  [optional]



