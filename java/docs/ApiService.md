
# ApiService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the service. |  [optional]
**type** | **String** | The type of the service, e.g. HDFS, MAPREDUCE, HBASE. |  [optional]
**clusterRef** | [**ApiClusterRef**](ApiClusterRef.md) | Readonly. A reference to the enclosing cluster. |  [optional]
**serviceState** | [**ApiServiceState**](ApiServiceState.md) | Readonly. The configured run state of this service. Whether it&#39;s running, etc. |  [optional]
**healthSummary** | [**ApiHealthSummary**](ApiHealthSummary.md) | Readonly. The high-level health status of this service. |  [optional]
**configStale** | **Boolean** | Readonly. Expresses whether the service configuration is stale. |  [optional]
**configStalenessStatus** | [**ApiConfigStalenessStatus**](ApiConfigStalenessStatus.md) | Readonly. Expresses the service&#39;s configuration staleness status which is based on the staleness status of its roles. Available since API v6. |  [optional]
**clientConfigStalenessStatus** | [**ApiConfigStalenessStatus**](ApiConfigStalenessStatus.md) | Readonly. Expresses the service&#39;s client configuration staleness status which is marked as stale if any of the service&#39;s hosts have missing client configurations or if any of the deployed client configurations are stale. Available since API v6. |  [optional]
**healthChecks** | [**List&lt;ApiHealthCheck&gt;**](ApiHealthCheck.md) | Readonly. The list of health checks of this service. |  [optional]
**serviceUrl** | **String** | Readonly. Link into the Cloudera Manager web UI for this specific service. |  [optional]
**roleInstancesUrl** | **String** | Readonly. Link into the Cloudera Manager web UI for role instances table for this specific service. Available since API v11. |  [optional]
**maintenanceMode** | **Boolean** | Readonly. Whether the service is in maintenance mode. Available since API v2. |  [optional]
**maintenanceOwners** | [**List&lt;ApiEntityType&gt;**](ApiEntityType.md) | Readonly. The list of objects that trigger this service to be in maintenance mode. Available since API v2. |  [optional]
**config** | [**ApiServiceConfig**](ApiServiceConfig.md) | Configuration of the service being created. Optional. |  [optional]
**roles** | [**List&lt;ApiRole&gt;**](ApiRole.md) | The list of service roles. Optional. |  [optional]
**displayName** | **String** | The display name for the service that is shown in the UI. Available since API v2. |  [optional]
**roleConfigGroups** | [**List&lt;ApiRoleConfigGroup&gt;**](ApiRoleConfigGroup.md) | The list of role configuration groups in this service. Optional. Available since API v3. |  [optional]
**replicationSchedules** | [**List&lt;ApiReplicationSchedule&gt;**](ApiReplicationSchedule.md) | The list of replication schedules for this service. Optional. Available since API v6. |  [optional]
**snapshotPolicies** | [**List&lt;ApiSnapshotPolicy&gt;**](ApiSnapshotPolicy.md) | The list of snapshot policies for this service. Optional. Available since API v6. |  [optional]
**entityStatus** | [**ApiEntityStatus**](ApiEntityStatus.md) | Readonly. The entity status for this service. Available since API v11. |  [optional]



