
# ApiImpalaQueryAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the attribute. This name can be used in filters, for example &#39;user&#39; could be used in the filter &#39;user &#x3D; root&#39;. |  [optional]
**type** | **String** | The type of the attribute. Valid types are STRING, NUMBER, BOOLEAN, BYTES, MILLISECONDS, BYTES_PER_SECOND, BYTE_SECONDS. |  [optional]
**displayName** | **String** | The display name for the attribute. |  [optional]
**supportsHistograms** | **Boolean** | Whether the Service Monitor can generate a histogram of the distribution of the attribute across queries. |  [optional]
**description** | **String** | The description of the attribute. |  [optional]



