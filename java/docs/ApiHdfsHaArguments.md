
# ApiHdfsHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeName** | **String** | Name of the active NameNode. |  [optional]
**activeSharedEditsPath** | **String** | Path to the shared edits directory on the active NameNode&#39;s host. Ignored if Quorum-based Storage is being enabled. |  [optional]
**standByName** | **String** | Name of the stand-by Namenode. |  [optional]
**standBySharedEditsPath** | **String** | Path to the shared edits directory on the stand-by NameNode&#39;s host. Ignored if Quorum-based Storage is being enabled. |  [optional]
**nameservice** | **String** | Nameservice that identifies the HA pair. |  [optional]
**startDependentServices** | **Boolean** | Whether to re-start dependent services. Defaults to true. |  [optional]
**deployClientConfigs** | **Boolean** | Whether to re-deploy client configurations. Defaults to true. |  [optional]
**enableQuorumStorage** | **Boolean** | This parameter has been deprecated as of CM 5.0, where HA is only supported using Quorum-based Storage. &lt;p&gt; Whether to enable Quorum-based Storage.  Enabling Quorum-based Storage requires a minimum of three and an odd number of JournalNodes to be created and configured before enabling HDFS HA. &lt;p&gt; Available since API v2. |  [optional]



