
# ApiBatchRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiBatchRequestElement&gt;**](ApiBatchRequestElement.md) |  |  [optional]



