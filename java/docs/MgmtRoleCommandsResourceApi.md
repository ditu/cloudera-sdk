# MgmtRoleCommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jmapDump**](MgmtRoleCommandsResourceApi.md#jmapDump) | **POST** /cm/service/roleCommands/jmapDump | Run the jmapDump diagnostic command.
[**jmapHisto**](MgmtRoleCommandsResourceApi.md#jmapHisto) | **POST** /cm/service/roleCommands/jmapHisto | Run the jmapHisto diagnostic command.
[**jstack**](MgmtRoleCommandsResourceApi.md#jstack) | **POST** /cm/service/roleCommands/jstack | Run the jstack diagnostic command.
[**lsof**](MgmtRoleCommandsResourceApi.md#lsof) | **POST** /cm/service/roleCommands/lsof | Run the lsof diagnostic command.
[**restartCommand**](MgmtRoleCommandsResourceApi.md#restartCommand) | **POST** /cm/service/roleCommands/restart | Restart a set of Cloudera Management Services roles.
[**startCommand**](MgmtRoleCommandsResourceApi.md#startCommand) | **POST** /cm/service/roleCommands/start | Start a set of Cloudera Management Services roles.
[**stopCommand**](MgmtRoleCommandsResourceApi.md#stopCommand) | **POST** /cm/service/roleCommands/stop | Stop a set of Cloudera Management Services roles.


<a name="jmapDump"></a>
# **jmapDump**
> ApiBulkCommandList jmapDump(body)

Run the jmapDump diagnostic command.

Run the jmapDump diagnostic command. The command runs the jmap utility to capture a dump of the role's java heap. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleCommandsResourceApi apiInstance = new MgmtRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to jmap.
try {
    ApiBulkCommandList result = apiInstance.jmapDump(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleCommandsResourceApi#jmapDump");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jmapHisto"></a>
# **jmapHisto**
> ApiBulkCommandList jmapHisto(body)

Run the jmapHisto diagnostic command.

Run the jmapHisto diagnostic command. The command runs the jmap utility to capture a histogram of the objects on the role's java heap. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleCommandsResourceApi apiInstance = new MgmtRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to jmap.
try {
    ApiBulkCommandList result = apiInstance.jmapHisto(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleCommandsResourceApi#jmapHisto");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jmap. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jstack"></a>
# **jstack**
> ApiBulkCommandList jstack(body)

Run the jstack diagnostic command.

Run the jstack diagnostic command. The command runs the jstack utility to capture a role's java thread stacks. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleCommandsResourceApi apiInstance = new MgmtRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to jstack.
try {
    ApiBulkCommandList result = apiInstance.jstack(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleCommandsResourceApi#jstack");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to jstack. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="lsof"></a>
# **lsof**
> ApiBulkCommandList lsof(body)

Run the lsof diagnostic command.

Run the lsof diagnostic command. This command runs the lsof utility to list a role's open files. <p/> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleCommandsResourceApi apiInstance = new MgmtRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | the names of the roles to lsof.
try {
    ApiBulkCommandList result = apiInstance.lsof(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleCommandsResourceApi#lsof");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| the names of the roles to lsof. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="restartCommand"></a>
# **restartCommand**
> ApiBulkCommandList restartCommand(body)

Restart a set of Cloudera Management Services roles.

Restart a set of Cloudera Management Services roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleCommandsResourceApi apiInstance = new MgmtRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The roles to restart.
try {
    ApiBulkCommandList result = apiInstance.restartCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleCommandsResourceApi#restartCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to restart. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="startCommand"></a>
# **startCommand**
> ApiBulkCommandList startCommand(body)

Start a set of Cloudera Management Services roles.

Start a set of Cloudera Management Services roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleCommandsResourceApi apiInstance = new MgmtRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The roles to start.
try {
    ApiBulkCommandList result = apiInstance.startCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleCommandsResourceApi#startCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to start. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="stopCommand"></a>
# **stopCommand**
> ApiBulkCommandList stopCommand(body)

Stop a set of Cloudera Management Services roles.

Stop a set of Cloudera Management Services roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleCommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleCommandsResourceApi apiInstance = new MgmtRoleCommandsResourceApi();
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | The roles to stop.
try {
    ApiBulkCommandList result = apiInstance.stopCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleCommandsResourceApi#stopCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| The roles to stop. | [optional]

### Return type

[**ApiBulkCommandList**](ApiBulkCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

