
# ApiKerberosInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kerberized** | **Boolean** |  |  [optional]
**kdcType** | **String** |  |  [optional]
**kerberosRealm** | **String** |  |  [optional]
**kdcHost** | **String** |  |  [optional]
**adminHost** | **String** |  |  [optional]
**domain** | **List&lt;String&gt;** |  |  [optional]



