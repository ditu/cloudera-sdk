
# ApiServiceList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiService&gt;**](ApiService.md) |  |  [optional]



