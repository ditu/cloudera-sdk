
# ApiHostTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the host template. Unique across clusters. |  [optional]
**clusterRef** | [**ApiClusterRef**](ApiClusterRef.md) | Readonly. A reference to the cluster the host template belongs to. |  [optional]
**roleConfigGroupRefs** | [**List&lt;ApiRoleConfigGroupRef&gt;**](ApiRoleConfigGroupRef.md) | The role config groups belonging to this host tempalte. |  [optional]



