# ParcelResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateCommand**](ParcelResourceApi.md#activateCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/activate | A synchronous command that activates the parcel on the cluster.
[**cancelDistributionCommand**](ParcelResourceApi.md#cancelDistributionCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/cancelDistribution | A synchronous command that cancels the parcel distribution.
[**cancelDownloadCommand**](ParcelResourceApi.md#cancelDownloadCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/cancelDownload | A synchronous command that cancels the parcel download.
[**deactivateCommand**](ParcelResourceApi.md#deactivateCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/deactivate | A synchronous command that deactivates the parcel on the cluster.
[**readParcel**](ParcelResourceApi.md#readParcel) | **GET** /clusters/{clusterName}/parcels/products/{product}/versions/{version} | Retrieves detailed information about a parcel.
[**removeDownloadCommand**](ParcelResourceApi.md#removeDownloadCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/removeDownload | A synchronous command that removes the downloaded parcel.
[**startDistributionCommand**](ParcelResourceApi.md#startDistributionCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startDistribution | A synchronous command that starts the distribution of the parcel to the cluster.
[**startDownloadCommand**](ParcelResourceApi.md#startDownloadCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startDownload | A synchronous command that starts the parcel download.
[**startRemovalOfDistributionCommand**](ParcelResourceApi.md#startRemovalOfDistributionCommand) | **POST** /clusters/{clusterName}/parcels/products/{product}/versions/{version}/commands/startRemovalOfDistribution | A synchronous command that removes the distribution from the hosts in the cluster.


<a name="activateCommand"></a>
# **activateCommand**
> ApiCommand activateCommand(clusterName, product, version)

A synchronous command that activates the parcel on the cluster.

A synchronous command that activates the parcel on the cluster. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.activateCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#activateCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="cancelDistributionCommand"></a>
# **cancelDistributionCommand**
> ApiCommand cancelDistributionCommand(clusterName, product, version)

A synchronous command that cancels the parcel distribution.

A synchronous command that cancels the parcel distribution. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.cancelDistributionCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#cancelDistributionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="cancelDownloadCommand"></a>
# **cancelDownloadCommand**
> ApiCommand cancelDownloadCommand(clusterName, product, version)

A synchronous command that cancels the parcel download.

A synchronous command that cancels the parcel download. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.cancelDownloadCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#cancelDownloadCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deactivateCommand"></a>
# **deactivateCommand**
> ApiCommand deactivateCommand(clusterName, product, version)

A synchronous command that deactivates the parcel on the cluster.

A synchronous command that deactivates the parcel on the cluster. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.deactivateCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#deactivateCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readParcel"></a>
# **readParcel**
> ApiParcel readParcel(clusterName, product, version)

Retrieves detailed information about a parcel.

Retrieves detailed information about a parcel.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiParcel result = apiInstance.readParcel(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#readParcel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiParcel**](ApiParcel.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="removeDownloadCommand"></a>
# **removeDownloadCommand**
> ApiCommand removeDownloadCommand(clusterName, product, version)

A synchronous command that removes the downloaded parcel.

A synchronous command that removes the downloaded parcel. <p> Since it is synchronous, the result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.removeDownloadCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#removeDownloadCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="startDistributionCommand"></a>
# **startDistributionCommand**
> ApiCommand startDistributionCommand(clusterName, product, version)

A synchronous command that starts the distribution of the parcel to the cluster.

A synchronous command that starts the distribution of the parcel to the cluster. <p> Since it is synchronous, the result is known immediately upon return. In order to see the progress of the distribution, a call to ParcelResource#readParcel() needs to be made.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.startDistributionCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#startDistributionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="startDownloadCommand"></a>
# **startDownloadCommand**
> ApiCommand startDownloadCommand(clusterName, product, version)

A synchronous command that starts the parcel download.

A synchronous command that starts the parcel download. <p> Since it is synchronous, the result is known immediately upon return. In order to see the progress of the download, a call to ParcelResource#readParcel() needs to be made.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.startDownloadCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#startDownloadCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="startRemovalOfDistributionCommand"></a>
# **startRemovalOfDistributionCommand**
> ApiCommand startRemovalOfDistributionCommand(clusterName, product, version)

A synchronous command that removes the distribution from the hosts in the cluster.

A synchronous command that removes the distribution from the hosts in the cluster. <p> Since it is synchronous, the result is known immediately upon return. In order to see the progress of the removal, a call to ParcelResource#readParcel() needs to be made.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelResourceApi apiInstance = new ParcelResourceApi();
String clusterName = "clusterName_example"; // String | 
String product = "product_example"; // String | the product
String version = "version_example"; // String | the version
try {
    ApiCommand result = apiInstance.startRemovalOfDistributionCommand(clusterName, product, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelResourceApi#startRemovalOfDistributionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **product** | **String**| the product |
 **version** | **String**| the version |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

