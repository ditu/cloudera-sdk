
# ApiProcess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configFiles** | **List&lt;String&gt;** | List of config files supplied to the process. |  [optional]



