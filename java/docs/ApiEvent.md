
# ApiEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for this event. |  [optional]
**content** | **String** | The content payload of this event. |  [optional]
**timeOccurred** | **String** | When the event was generated. |  [optional]
**timeReceived** | **String** | When the event was stored by Cloudera Manager. Events do not arrive in the order that they are generated. If you are writing an event poller, this is a useful field to query. |  [optional]
**category** | [**ApiEventCategory**](ApiEventCategory.md) | The category of this event -- whether it is a health event, an audit event, an activity event, etc. |  [optional]
**severity** | [**ApiEventSeverity**](ApiEventSeverity.md) | The severity of the event. |  [optional]
**alert** | **Boolean** | Whether the event is promoted to an alert according to configuration. |  [optional]
**attributes** | [**List&lt;ApiEventAttribute&gt;**](ApiEventAttribute.md) | A list of key-value attribute pairs. |  [optional]



