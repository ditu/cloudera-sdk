
# ApiBatchResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiBatchResponseElement&gt;**](ApiBatchResponseElement.md) |  |  [optional]
**success** | **Boolean** | Read-only. True if every response element&#39;s ApiBatchResponseElement#getStatusCode() is in the range [200, 300), false otherwise. |  [optional]



