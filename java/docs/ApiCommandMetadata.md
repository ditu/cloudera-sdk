
# ApiCommandMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of of the command. |  [optional]
**argSchema** | **String** | The command arguments schema.  This is in the form of json schema and describes the structure of the command arguments. If null, the command does not take arguments. |  [optional]



