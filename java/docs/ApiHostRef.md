
# ApiHostRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostId** | **String** | The unique host ID. |  [optional]
**hostname** | **String** | The hostname. Available since v31. |  [optional]



