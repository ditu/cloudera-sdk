
# ApiClusterUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCpuCores** | [**BigDecimal**](BigDecimal.md) | Average number of CPU cores available in the cluster during the report window. |  [optional]
**avgCpuUtilization** | [**BigDecimal**](BigDecimal.md) | Average CPU consumption for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. |  [optional]
**maxCpuUtilization** | [**BigDecimal**](BigDecimal.md) | Maximum CPU consumption for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. |  [optional]
**avgCpuDailyPeak** | [**BigDecimal**](BigDecimal.md) | Average daily peak CPU consumption for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. |  [optional]
**avgWorkloadCpu** | [**BigDecimal**](BigDecimal.md) | Average CPU consumption by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. |  [optional]
**maxWorkloadCpu** | [**BigDecimal**](BigDecimal.md) | Maximum CPU consumption by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. |  [optional]
**avgWorkloadCpuDailyPeak** | [**BigDecimal**](BigDecimal.md) | Average daily peak CPU consumption by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. |  [optional]
**totalMemory** | [**BigDecimal**](BigDecimal.md) | Average physical memory (in bytes) available in the cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. |  [optional]
**avgMemoryUtilization** | [**BigDecimal**](BigDecimal.md) | Average memory consumption (as percentage of total memory) for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. |  [optional]
**maxMemoryUtilization** | [**BigDecimal**](BigDecimal.md) | Maximum memory consumption (as percentage of total memory) for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. |  [optional]
**avgMemoryDailyPeak** | [**BigDecimal**](BigDecimal.md) | Average daily peak memory consumption (as percentage of total memory) for the entire cluster during the report window. This includes consumption by user workloads in YARN and Impala, as well as consumption by all services running in the cluster. |  [optional]
**avgWorkloadMemory** | [**BigDecimal**](BigDecimal.md) | Average memory consumption (as percentage of total memory) by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. |  [optional]
**maxWorkloadMemory** | [**BigDecimal**](BigDecimal.md) | Maximum memory consumption (as percentage of total memory) by workloads that ran on the cluster. This includes consumption by user workloads in YARN and Impala |  [optional]
**avgWorkloadMemoryDailyPeak** | [**BigDecimal**](BigDecimal.md) | Average daily peak memory consumption (as percentage of total memory) by workloads that ran on the cluster during the report window. This includes consumption by user workloads in YARN and Impala. |  [optional]
**tenantUtilizations** | [**ApiTenantUtilizationList**](ApiTenantUtilizationList.md) | A list of tenant utilization reports. |  [optional]
**maxCpuUtilizationTimestampMs** | [**BigDecimal**](BigDecimal.md) | Timestamp corresponding to maximum CPU utilization for the entire cluster during the report window. |  [optional]
**maxMemoryUtilizationTimestampMs** | [**BigDecimal**](BigDecimal.md) | Timestamp corresponding to maximum memory utilization for the entire cluster during the report window. |  [optional]
**maxWorkloadCpuTimestampMs** | [**BigDecimal**](BigDecimal.md) | Timestamp corresponds to maximum CPU consumption by workloads that ran on the cluster during the report window. |  [optional]
**maxWorkloadMemoryTimestampMs** | [**BigDecimal**](BigDecimal.md) | Timestamp corresponds to maximum memory resource consumption by workloads that ran on the cluster during the report window. |  [optional]
**errorMessage** | **String** | Error message while generating utilization report. |  [optional]



