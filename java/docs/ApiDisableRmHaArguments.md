
# ApiDisableRmHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeName** | **String** | Name of the ResourceManager that will be active after HA is disabled. |  [optional]



