# WatchedDirResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addWatchedDirectory**](WatchedDirResourceApi.md#addWatchedDirectory) | **POST** /clusters/{clusterName}/services/{serviceName}/watcheddir | Adds a directory to the watching list.
[**listWatchedDirectories**](WatchedDirResourceApi.md#listWatchedDirectories) | **GET** /clusters/{clusterName}/services/{serviceName}/watcheddir | Lists all the watched directories.
[**removeWatchedDirectory**](WatchedDirResourceApi.md#removeWatchedDirectory) | **DELETE** /clusters/{clusterName}/services/{serviceName}/watcheddir/{directoryPath} | Removes a directory from the watching list.


<a name="addWatchedDirectory"></a>
# **addWatchedDirectory**
> ApiWatchedDir addWatchedDirectory(clusterName, serviceName, body)

Adds a directory to the watching list.

Adds a directory to the watching list. <p> Available since API v14. Only available with Cloudera Manager Enterprise Edition. <p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.WatchedDirResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

WatchedDirResourceApi apiInstance = new WatchedDirResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
ApiWatchedDir body = new ApiWatchedDir(); // ApiWatchedDir | The directory to be added.
try {
    ApiWatchedDir result = apiInstance.addWatchedDirectory(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WatchedDirResourceApi#addWatchedDirectory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **body** | [**ApiWatchedDir**](ApiWatchedDir.md)| The directory to be added. | [optional]

### Return type

[**ApiWatchedDir**](ApiWatchedDir.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listWatchedDirectories"></a>
# **listWatchedDirectories**
> ApiWatchedDirList listWatchedDirectories(clusterName, serviceName)

Lists all the watched directories.

Lists all the watched directories. <p> Available since API v14. Only available with Cloudera Manager Enterprise Edition. <p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.WatchedDirResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

WatchedDirResourceApi apiInstance = new WatchedDirResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiWatchedDirList result = apiInstance.listWatchedDirectories(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WatchedDirResourceApi#listWatchedDirectories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiWatchedDirList**](ApiWatchedDirList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="removeWatchedDirectory"></a>
# **removeWatchedDirectory**
> ApiWatchedDir removeWatchedDirectory(clusterName, directoryPath, serviceName)

Removes a directory from the watching list.

Removes a directory from the watching list. <p> Available since API v14. Only available with Cloudera Manager Enterprise Edition. <p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.WatchedDirResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

WatchedDirResourceApi apiInstance = new WatchedDirResourceApi();
String clusterName = "clusterName_example"; // String | 
String directoryPath = "directoryPath_example"; // String | The directory path to be removed.
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiWatchedDir result = apiInstance.removeWatchedDirectory(clusterName, directoryPath, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WatchedDirResourceApi#removeWatchedDirectory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **directoryPath** | **String**| The directory path to be removed. |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiWatchedDir**](ApiWatchedDir.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

