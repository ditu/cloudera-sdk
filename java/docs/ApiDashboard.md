
# ApiDashboard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Returns the dashboard name. |  [optional]
**json** | **String** | Returns the json structure for the dashboard. This should be treated as an opaque blob. |  [optional]



