# TimeSeriesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEntityTypeAttributes**](TimeSeriesResourceApi.md#getEntityTypeAttributes) | **GET** /timeseries/entityTypeAttributes | Retrieve all metric entity type attributes monitored by Cloudera Manager.
[**getEntityTypes**](TimeSeriesResourceApi.md#getEntityTypes) | **GET** /timeseries/entityTypes | Retrieve all metric entity types monitored by Cloudera Manager.
[**getMetricSchema**](TimeSeriesResourceApi.md#getMetricSchema) | **GET** /timeseries/schema | Retrieve schema for all metrics.
[**queryTimeSeries**](TimeSeriesResourceApi.md#queryTimeSeries) | **GET** /timeseries | Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.
[**queryTimeSeries_0**](TimeSeriesResourceApi.md#queryTimeSeries_0) | **POST** /timeseries | Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.


<a name="getEntityTypeAttributes"></a>
# **getEntityTypeAttributes**
> ApiTimeSeriesEntityAttributeList getEntityTypeAttributes()

Retrieve all metric entity type attributes monitored by Cloudera Manager.

Retrieve all metric entity type attributes monitored by Cloudera Manager. <p/> Available since API v11.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.TimeSeriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

TimeSeriesResourceApi apiInstance = new TimeSeriesResourceApi();
try {
    ApiTimeSeriesEntityAttributeList result = apiInstance.getEntityTypeAttributes();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeSeriesResourceApi#getEntityTypeAttributes");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiTimeSeriesEntityAttributeList**](ApiTimeSeriesEntityAttributeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEntityTypes"></a>
# **getEntityTypes**
> ApiTimeSeriesEntityTypeList getEntityTypes()

Retrieve all metric entity types monitored by Cloudera Manager.

Retrieve all metric entity types monitored by Cloudera Manager. It is guaranteed that parent types appear before their children. <p/> Available since API v11.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.TimeSeriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

TimeSeriesResourceApi apiInstance = new TimeSeriesResourceApi();
try {
    ApiTimeSeriesEntityTypeList result = apiInstance.getEntityTypes();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeSeriesResourceApi#getEntityTypes");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiTimeSeriesEntityTypeList**](ApiTimeSeriesEntityTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMetricSchema"></a>
# **getMetricSchema**
> ApiMetricSchemaList getMetricSchema()

Retrieve schema for all metrics.

Retrieve schema for all metrics <p/> The schema is fixed for a product version. The schema may change for an API versions <p/> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.TimeSeriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

TimeSeriesResourceApi apiInstance = new TimeSeriesResourceApi();
try {
    ApiMetricSchemaList result = apiInstance.getMetricSchema();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeSeriesResourceApi#getMetricSchema");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiMetricSchemaList**](ApiMetricSchemaList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="queryTimeSeries"></a>
# **queryTimeSeries**
> ApiTimeSeriesResponseList queryTimeSeries(contentType, desiredRollup, from, mustUseDesiredRollup, query, to)

Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.

Retrieve time-series data from the Cloudera Manager (CM) time-series data store using a tsquery.  Please see the <a href=\"http://tiny.cloudera.com/cm_tsquery\"> tsquery language documentation</a>. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.TimeSeriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

TimeSeriesResourceApi apiInstance = new TimeSeriesResourceApi();
String contentType = "application/json"; // String | to return the response in. The content types \"application/json\" and \"text/csv\" are supported. This defaults to \"application/json\". If \"text/csv\" is specified then we return one row per time series data point, and we don't return any of the metadata.
String desiredRollup = "RAW"; // String | Aggregate rollup level desired for the response data. Valid values are RAW, TEN_MINUTELY, HOURLY, SIX_HOURLY, DAILY, and WEEKLY. Note that if the mustUseDesiredRollup parameter is not set, then the monitoring server can decide to return a different rollup level.
String from = "from_example"; // String | Start of the period to query in ISO 8601 format (defaults to 5 minutes before the end of the period).
Boolean mustUseDesiredRollup = false; // Boolean | If set then the tsquery will return data with the desired aggregate rollup level.
String query = "query_example"; // String | Tsquery to run against the CM time-series data store.
String to = "now"; // String | End of the period to query in ISO 8601 format (defaults to current time).
try {
    ApiTimeSeriesResponseList result = apiInstance.queryTimeSeries(contentType, desiredRollup, from, mustUseDesiredRollup, query, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeSeriesResourceApi#queryTimeSeries");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**| to return the response in. The content types \"application/json\" and \"text/csv\" are supported. This defaults to \"application/json\". If \"text/csv\" is specified then we return one row per time series data point, and we don't return any of the metadata. | [optional] [default to application/json]
 **desiredRollup** | **String**| Aggregate rollup level desired for the response data. Valid values are RAW, TEN_MINUTELY, HOURLY, SIX_HOURLY, DAILY, and WEEKLY. Note that if the mustUseDesiredRollup parameter is not set, then the monitoring server can decide to return a different rollup level. | [optional] [default to RAW]
 **from** | **String**| Start of the period to query in ISO 8601 format (defaults to 5 minutes before the end of the period). | [optional]
 **mustUseDesiredRollup** | **Boolean**| If set then the tsquery will return data with the desired aggregate rollup level. | [optional] [default to false]
 **query** | **String**| Tsquery to run against the CM time-series data store. | [optional]
 **to** | **String**| End of the period to query in ISO 8601 format (defaults to current time). | [optional] [default to now]

### Return type

[**ApiTimeSeriesResponseList**](ApiTimeSeriesResponseList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="queryTimeSeries_0"></a>
# **queryTimeSeries_0**
> ApiTimeSeriesResponseList queryTimeSeries_0(body)

Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request.

Retrieve time-series data from the Cloudera Manager (CM) time-series data store accepting HTTP POST request. This method differs from queryTimeSeries() in v6 that this could accept query strings that are longer than HTTP GET request limit.  Available since API v11.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.TimeSeriesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

TimeSeriesResourceApi apiInstance = new TimeSeriesResourceApi();
ApiTimeSeriesRequest body = new ApiTimeSeriesRequest(); // ApiTimeSeriesRequest | Request object containing information used when retrieving timeseries data.
try {
    ApiTimeSeriesResponseList result = apiInstance.queryTimeSeries_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeSeriesResourceApi#queryTimeSeries_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiTimeSeriesRequest**](ApiTimeSeriesRequest.md)| Request object containing information used when retrieving timeseries data. | [optional]

### Return type

[**ApiTimeSeriesResponseList**](ApiTimeSeriesResponseList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

