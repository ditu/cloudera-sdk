
# ApiParcelUsageRack

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hosts** | [**List&lt;ApiParcelUsageHost&gt;**](ApiParcelUsageHost.md) | A collection of the hosts in the rack. |  [optional]
**rackId** | **String** | The rack ID for the rack. |  [optional]



