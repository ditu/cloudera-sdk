# CmPeersResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPeer**](CmPeersResourceApi.md#createPeer) | **POST** /cm/peers | Create a new Cloudera Manager peer.
[**deletePeer**](CmPeersResourceApi.md#deletePeer) | **DELETE** /cm/peers/{peerName} | Delete Cloudera Manager peer.
[**listPeers**](CmPeersResourceApi.md#listPeers) | **GET** /cm/peers | Retrieves all configured Cloudera Manager peers.
[**readPeer**](CmPeersResourceApi.md#readPeer) | **GET** /cm/peers/{peerName} | Fetch information about an existing Cloudera Manager peer.
[**testPeer**](CmPeersResourceApi.md#testPeer) | **POST** /cm/peers/{peerName}/commands/test | Test the connectivity of a peer.
[**updatePeer**](CmPeersResourceApi.md#updatePeer) | **PUT** /cm/peers/{peerName} | Update information for a Cloudera Manager peer.


<a name="createPeer"></a>
# **createPeer**
> ApiCmPeer createPeer(body)

Create a new Cloudera Manager peer.

Create a new Cloudera Manager peer. <p> The remote server will be contacted so that a user can be created for use by the new peer. The <i>username</i> and <i>password</i> properties of the provided peer object should contain credentials of a valid admin user on the remote server. A timeout of 10 seconds is enforced when contacting the remote server. <p> It is recommended to run the remote server with TLS enabled, since creating and using peers involve transferring credentials over the network. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> Type field in ApiCmPeer is available since API v11. if not specified when making createPeer() call, 'REPLICATION' type peer will be created.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CmPeersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CmPeersResourceApi apiInstance = new CmPeersResourceApi();
ApiCmPeer body = new ApiCmPeer(); // ApiCmPeer | Peer to create (see above).
try {
    ApiCmPeer result = apiInstance.createPeer(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CmPeersResourceApi#createPeer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiCmPeer**](ApiCmPeer.md)| Peer to create (see above). | [optional]

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deletePeer"></a>
# **deletePeer**
> ApiCmPeer deletePeer(peerName, type)

Delete Cloudera Manager peer.

Delete Cloudera Manager peer. <p> An attempt will be made to contact the peer server, so that the configured user can be deleted.. Errors while contacting the remote server are non-fatal. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CmPeersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CmPeersResourceApi apiInstance = new CmPeersResourceApi();
String peerName = "peerName_example"; // String | Name of peer to delete.
String type = "type_example"; // String | Type of peer to delete. If null, REPLICATION peer type will be deleted.
try {
    ApiCmPeer result = apiInstance.deletePeer(peerName, type);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CmPeersResourceApi#deletePeer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peerName** | **String**| Name of peer to delete. |
 **type** | **String**| Type of peer to delete. If null, REPLICATION peer type will be deleted. | [optional] [enum: REPLICATION, STATUS_AGGREGATION]

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listPeers"></a>
# **listPeers**
> ApiCmPeerList listPeers()

Retrieves all configured Cloudera Manager peers.

Retrieves all configured Cloudera Manager peers. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition. <p> When accessed via API version before v11, only REPLICATION type peers will be returned.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CmPeersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CmPeersResourceApi apiInstance = new CmPeersResourceApi();
try {
    ApiCmPeerList result = apiInstance.listPeers();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CmPeersResourceApi#listPeers");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCmPeerList**](ApiCmPeerList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readPeer"></a>
# **readPeer**
> ApiCmPeer readPeer(peerName, type)

Fetch information about an existing Cloudera Manager peer.

Fetch information about an existing Cloudera Manager peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CmPeersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CmPeersResourceApi apiInstance = new CmPeersResourceApi();
String peerName = "peerName_example"; // String | Name of peer to retrieve.
String type = "type_example"; // String | Type of peer to retrieve. If null, REPLICATION peer type will be returned.
try {
    ApiCmPeer result = apiInstance.readPeer(peerName, type);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CmPeersResourceApi#readPeer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peerName** | **String**| Name of peer to retrieve. |
 **type** | **String**| Type of peer to retrieve. If null, REPLICATION peer type will be returned. | [optional] [enum: REPLICATION, STATUS_AGGREGATION]

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="testPeer"></a>
# **testPeer**
> ApiCommand testPeer(peerName, type)

Test the connectivity of a peer.

Test the connectivity of a peer. <p> Available since API v11. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CmPeersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CmPeersResourceApi apiInstance = new CmPeersResourceApi();
String peerName = "peerName_example"; // String | Name of peer to test.
String type = "type_example"; // String | Type of peer to test. If null, REPLICATION peer type will be tested.
try {
    ApiCommand result = apiInstance.testPeer(peerName, type);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CmPeersResourceApi#testPeer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peerName** | **String**| Name of peer to test. |
 **type** | **String**| Type of peer to test. If null, REPLICATION peer type will be tested. | [optional] [enum: REPLICATION, STATUS_AGGREGATION]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updatePeer"></a>
# **updatePeer**
> ApiCmPeer updatePeer(peerName, body)

Update information for a Cloudera Manager peer.

Update information for a Cloudera Manager peer. <p> In administrator credentials are provided in the peer information, they will be used to establish new credentials with the remote server. This can be used in case the old credentials are not working anymore. An attempt will be made to delete the old credentials if new ones are successfully created. <p> If changing the peer's URL, an attempt will be made to contact the old Cloudera Manager to delete the existing credentials. <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CmPeersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CmPeersResourceApi apiInstance = new CmPeersResourceApi();
String peerName = "peerName_example"; // String | Name of peer to update.
ApiCmPeer body = new ApiCmPeer(); // ApiCmPeer | Updated peer information.
try {
    ApiCmPeer result = apiInstance.updatePeer(peerName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CmPeersResourceApi#updatePeer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peerName** | **String**| Name of peer to update. |
 **body** | [**ApiCmPeer**](ApiCmPeer.md)| Updated peer information. | [optional]

### Return type

[**ApiCmPeer**](ApiCmPeer.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

