
# ApiDisableNnHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeNnName** | **String** | Name of the NamdeNode role that is going to be active after High Availability is disabled. |  [optional]
**snnHostId** | **String** | Id of the host where the new SecondaryNameNode will be created. |  [optional]
**snnCheckpointDirList** | **List&lt;String&gt;** | List of directories used for checkpointing by the new SecondaryNameNode. |  [optional]
**snnName** | **String** | Name of the new SecondaryNameNode role (Optional). |  [optional]



