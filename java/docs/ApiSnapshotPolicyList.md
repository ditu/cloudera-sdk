
# ApiSnapshotPolicyList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiSnapshotPolicy&gt;**](ApiSnapshotPolicy.md) |  |  [optional]



