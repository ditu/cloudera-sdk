# AuthServiceRolesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRoles**](AuthServiceRolesResourceApi.md#createRoles) | **POST** /cm/authService/roles | Create new roles in the Authentication Services.
[**deleteRole**](AuthServiceRolesResourceApi.md#deleteRole) | **DELETE** /cm/authService/roles/{roleName} | Delete a role from the Authentication Services.
[**enterMaintenanceMode**](AuthServiceRolesResourceApi.md#enterMaintenanceMode) | **POST** /cm/authService/roles/{roleName}/commands/enterMaintenanceMode | Put the Authentication Service role into maintenance mode.
[**exitMaintenanceMode**](AuthServiceRolesResourceApi.md#exitMaintenanceMode) | **POST** /cm/authService/roles/{roleName}/commands/exitMaintenanceMode | Take the Authentication Service role out of maintenance mode.
[**getFullLog**](AuthServiceRolesResourceApi.md#getFullLog) | **GET** /cm/authService/roles/{roleName}/logs/full | Retrieves the log file for the role&#39;s main process.
[**getStacksLog**](AuthServiceRolesResourceApi.md#getStacksLog) | **GET** /cm/authService/roles/{roleName}/logs/stacks | Retrieves the stacks log file, if any, for the role&#39;s main process.
[**getStacksLogsBundle**](AuthServiceRolesResourceApi.md#getStacksLogsBundle) | **GET** /cm/authService/roles/{roleName}/logs/stacksBundle | Download a zip-compressed archive of role stacks logs.
[**getStandardError**](AuthServiceRolesResourceApi.md#getStandardError) | **GET** /cm/authService/roles/{roleName}/logs/stderr | Retrieves the role&#39;s standard error output.
[**getStandardOutput**](AuthServiceRolesResourceApi.md#getStandardOutput) | **GET** /cm/authService/roles/{roleName}/logs/stdout | Retrieves the role&#39;s standard output.
[**listActiveCommands**](AuthServiceRolesResourceApi.md#listActiveCommands) | **GET** /cm/authService/roles/{roleName}/commands | List active role commands.
[**readRole**](AuthServiceRolesResourceApi.md#readRole) | **GET** /cm/authService/roles/{roleName} | Retrieve detailed information about a Authentication Services role.
[**readRoleConfig**](AuthServiceRolesResourceApi.md#readRoleConfig) | **GET** /cm/authService/roles/{roleName}/config | Retrieve the configuration of a specific Authentication Services role.
[**readRoles**](AuthServiceRolesResourceApi.md#readRoles) | **GET** /cm/authService/roles | List all roles of the Authentication Services.
[**updateRoleConfig**](AuthServiceRolesResourceApi.md#updateRoleConfig) | **PUT** /cm/authService/roles/{roleName}/config | Update the configuration of a Authentication Services role.


<a name="createRoles"></a>
# **createRoles**
> ApiRoleList createRoles(body)

Create new roles in the Authentication Services.

Create new roles in the Authentication Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
ApiRoleList body = new ApiRoleList(); // ApiRoleList | Roles to create.
try {
    ApiRoleList result = apiInstance.createRoles(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#createRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiRoleList**](ApiRoleList.md)| Roles to create. | [optional]

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteRole"></a>
# **deleteRole**
> ApiRole deleteRole(roleName)

Delete a role from the Authentication Services.

Delete a role from the Authentication Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role name.
try {
    ApiRole result = apiInstance.deleteRole(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#deleteRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role name. |

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="enterMaintenanceMode"></a>
# **enterMaintenanceMode**
> ApiCommand enterMaintenanceMode(roleName)

Put the Authentication Service role into maintenance mode.

Put the Authentication Service role into maintenance mode.This is a synchronous command. The result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role name.
try {
    ApiCommand result = apiInstance.enterMaintenanceMode(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#enterMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaintenanceMode"></a>
# **exitMaintenanceMode**
> ApiCommand exitMaintenanceMode(roleName)

Take the Authentication Service role out of maintenance mode.

Take the Authentication Service role out of maintenance mode. This is a synchronous command. The result is known immediately upon return.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role name.
try {
    ApiCommand result = apiInstance.exitMaintenanceMode(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#exitMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFullLog"></a>
# **getFullLog**
> String getFullLog(roleName)

Retrieves the log file for the role's main process.

Retrieves the log file for the role's main process. <p> If the role is not started, this will be the log file associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role to fetch logs from.
try {
    String result = apiInstance.getFullLog(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#getFullLog");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role to fetch logs from. |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getStacksLog"></a>
# **getStacksLog**
> String getStacksLog(roleName)

Retrieves the stacks log file, if any, for the role's main process.

Retrieves the stacks log file, if any, for the role's main process. Note that not all roles support periodic stacks collection.  The log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role to fetch stacks logs from.
try {
    String result = apiInstance.getStacksLog(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#getStacksLog");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role to fetch stacks logs from. |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getStacksLogsBundle"></a>
# **getStacksLogsBundle**
> getStacksLogsBundle(roleName)

Download a zip-compressed archive of role stacks logs.

Download a zip-compressed archive of role stacks logs. Note that not all roles support periodic stacks collection.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role to fetch the stacks logs bundle from.
try {
    apiInstance.getStacksLogsBundle(roleName);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#getStacksLogsBundle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role to fetch the stacks logs bundle from. |

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getStandardError"></a>
# **getStandardError**
> String getStandardError(roleName)

Retrieves the role's standard error output.

Retrieves the role's standard error output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role to fetch stderr from.
try {
    String result = apiInstance.getStandardError(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#getStandardError");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role to fetch stderr from. |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getStandardOutput"></a>
# **getStandardOutput**
> String getStandardOutput(roleName)

Retrieves the role's standard output.

Retrieves the role's standard output. <p> If the role is not started, this will be the output associated with the last time the role was run. <p> Log files are returned as plain text (type \"text/plain\").

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role to fetch stdout from.
try {
    String result = apiInstance.getStandardOutput(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#getStandardOutput");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role to fetch stdout from. |

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="listActiveCommands"></a>
# **listActiveCommands**
> ApiCommandList listActiveCommands(roleName, view)

List active role commands.

List active role commands.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role name.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiCommandList result = apiInstance.listActiveCommands(roleName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#listActiveCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role name. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRole"></a>
# **readRole**
> ApiRole readRole(roleName)

Retrieve detailed information about a Authentication Services role.

Retrieve detailed information about a Authentication Services role.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role name.
try {
    ApiRole result = apiInstance.readRole(roleName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#readRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role name. |

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoleConfig"></a>
# **readRoleConfig**
> ApiConfigList readRoleConfig(roleName, view)

Retrieve the configuration of a specific Authentication Services role.

Retrieve the configuration of a specific Authentication Services role.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role to look up.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.readRoleConfig(roleName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#readRoleConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role to look up. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoles"></a>
# **readRoles**
> ApiRoleList readRoles()

List all roles of the Authentication Services.

List all roles of the Authentication Services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
try {
    ApiRoleList result = apiInstance.readRoles();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#readRoles");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateRoleConfig"></a>
# **updateRoleConfig**
> ApiConfigList updateRoleConfig(roleName, message, body)

Update the configuration of a Authentication Services role.

Update the configuration of a Authentication Services role. <p> If a value is set in the given configuration, it will be added to the role's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthServiceRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthServiceRolesResourceApi apiInstance = new AuthServiceRolesResourceApi();
String roleName = "roleName_example"; // String | The role to modify.
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | Configuration changes.
try {
    ApiConfigList result = apiInstance.updateRoleConfig(roleName, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthServiceRolesResourceApi#updateRoleConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleName** | **String**| The role to modify. |
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| Configuration changes. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

