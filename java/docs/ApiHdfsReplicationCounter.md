
# ApiHdfsReplicationCounter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**value** | [**BigDecimal**](BigDecimal.md) |  |  [optional]



