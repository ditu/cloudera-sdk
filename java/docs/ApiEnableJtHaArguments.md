
# ApiEnableJtHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newJtHostId** | **String** | Id of host on which second JobTracker role will be added. |  [optional]
**forceInitZNode** | **Boolean** | Initialize the ZNode even if it already exists. This can happen if JobTracker HA was enabled before and then disabled. Disable operation doesn&#39;t delete this ZNode. Defaults to true. |  [optional]
**zkServiceName** | **String** | Name of the ZooKeeper service that will be used for auto-failover. This is an optional parameter if the MapReduce to ZooKeeper dependency is already set in CM. |  [optional]
**newJtRoleName** | **String** | Name of the second JobTracker role to be created (Optional) |  [optional]
**fc1RoleName** | **String** | Name of first Failover Controller role to be created. This is the Failover Controller co-located with the current JobTracker (Optional) |  [optional]
**fc2RoleName** | **String** | Name of second Failover Controller role to be created. This is the Failover Controller co-located with the new JobTracker (Optional) |  [optional]
**logicalName** | **String** | Logical name of the JobTracker pair. If value is not provided, \&quot;logicaljt\&quot; is used as the default. The name can contain only alphanumeric characters and \&quot;-\&quot;. &lt;p&gt; Available since API v8. |  [optional]



