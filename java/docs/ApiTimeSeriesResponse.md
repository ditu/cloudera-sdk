
# ApiTimeSeriesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeSeries** | [**List&lt;ApiTimeSeries&gt;**](ApiTimeSeries.md) | The time series data for this single query response. |  [optional]
**warnings** | **List&lt;String&gt;** | The warnings for this single query response. |  [optional]
**timeSeriesQuery** | **String** | The query for this single query response. |  [optional]



