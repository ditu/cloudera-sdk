# ParcelsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getParcelUsage**](ParcelsResourceApi.md#getParcelUsage) | **GET** /clusters/{clusterName}/parcels/usage | Retrieve details parcel usage information for the cluster.
[**readParcels**](ParcelsResourceApi.md#readParcels) | **GET** /clusters/{clusterName}/parcels | Lists all parcels that the cluster has access to.


<a name="getParcelUsage"></a>
# **getParcelUsage**
> ApiParcelUsage getParcelUsage(clusterName)

Retrieve details parcel usage information for the cluster.

Retrieve details parcel usage information for the cluster. This describes which processes, roles and hosts are using which parcels.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelsResourceApi apiInstance = new ParcelsResourceApi();
String clusterName = "clusterName_example"; // String | 
try {
    ApiParcelUsage result = apiInstance.getParcelUsage(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelsResourceApi#getParcelUsage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |

### Return type

[**ApiParcelUsage**](ApiParcelUsage.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readParcels"></a>
# **readParcels**
> ApiParcelList readParcels(clusterName, view)

Lists all parcels that the cluster has access to.

Lists all parcels that the cluster has access to.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ParcelsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ParcelsResourceApi apiInstance = new ParcelsResourceApi();
String clusterName = "clusterName_example"; // String | 
String view = "summary"; // String | 
try {
    ApiParcelList result = apiInstance.readParcels(clusterName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelsResourceApi#readParcels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiParcelList**](ApiParcelList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

