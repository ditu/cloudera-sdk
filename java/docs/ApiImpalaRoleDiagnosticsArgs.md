
# ApiImpalaRoleDiagnosticsArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticketNumber** | **String** | The support ticket number to attach to this data collection. |  [optional]
**comments** | **String** | Comments to include with this data collection. |  [optional]
**stacksCount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**stacksIntervalSeconds** | [**BigDecimal**](BigDecimal.md) | Interval between stack collections. |  [optional]
**jmap** | **Boolean** |  |  [optional]
**gcore** | **Boolean** |  |  [optional]
**minidumpsCount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**minidumpsIntervalSeconds** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**phoneHome** | **Boolean** |  |  [optional]



