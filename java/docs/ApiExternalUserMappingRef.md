
# ApiExternalUserMappingRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** | The uuid of the external user mapping, which uniquely identifies it in a CM installation. |  [optional]
**name** | **String** | The name of the mapping. |  [optional]
**type** | [**ApiExternalUserMappingType**](ApiExternalUserMappingType.md) | The type of the mapping. |  [optional]



