# ActivitiesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMetrics**](ActivitiesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/metrics | Fetch metric readings for a particular activity.
[**readActivities**](ActivitiesResourceApi.md#readActivities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities | Read all activities in the system.
[**readActivity**](ActivitiesResourceApi.md#readActivity) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId} | Returns a specific activity in the system.
[**readChildActivities**](ActivitiesResourceApi.md#readChildActivities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/children | Returns the child activities.
[**readSimilarActivities**](ActivitiesResourceApi.md#readSimilarActivities) | **GET** /clusters/{clusterName}/services/{serviceName}/activities/{activityId}/similar | Returns a list of similar activities.


<a name="getMetrics"></a>
# **getMetrics**
> ApiMetricList getMetrics(activityId, clusterName, serviceName, from, metrics, to, view)

Fetch metric readings for a particular activity.

Fetch metric readings for a particular activity. <p> By default, this call will look up all metrics available for the activity. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the activity, even if no readings are available in the requested window.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ActivitiesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ActivitiesResourceApi apiInstance = new ActivitiesResourceApi();
String activityId = "activityId_example"; // String | The name of the activity.
String clusterName = "clusterName_example"; // String | The name of the cluster.
String serviceName = "serviceName_example"; // String | The name of the service.
String from = "from_example"; // String | Start of the period to query.
List<String> metrics = Arrays.asList("metrics_example"); // List<String> | Filter for which metrics to query.
String to = "now"; // String | End of the period to query.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiMetricList result = apiInstance.getMetrics(activityId, clusterName, serviceName, from, metrics, to, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivitiesResourceApi#getMetrics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityId** | **String**| The name of the activity. |
 **clusterName** | **String**| The name of the cluster. |
 **serviceName** | **String**| The name of the service. |
 **from** | **String**| Start of the period to query. | [optional]
 **metrics** | [**List&lt;String&gt;**](String.md)| Filter for which metrics to query. | [optional]
 **to** | **String**| End of the period to query. | [optional] [default to now]
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readActivities"></a>
# **readActivities**
> ApiActivityList readActivities(clusterName, serviceName, maxResults, query, resultOffset, view)

Read all activities in the system.

Read all activities in the system

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ActivitiesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ActivitiesResourceApi apiInstance = new ActivitiesResourceApi();
String clusterName = "clusterName_example"; // String | The name of the cluster
String serviceName = "serviceName_example"; // String | The name of the service
Integer maxResults = 100; // Integer | The maximum number of activities to return.
String query = "status==started;parent=="; // String | The query to perform to find activities in the system. By default, this call returns top level (i.e. root) activities that have currently started. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>status==started;parent==</dt> <dd>looks for running root activities. This is also the default query.</dd> <dt>status==failed;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for failed activities after the given date time.</dd> <dt>name==Pi Estimator;startTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for activities started after the given date time, with the name of \"Pi Estimator\".</dd> <dt>startTime=lt=2012-01-02T00:00:00.000Z;finishTime=ge=2012-01-01T00:00:00.000Z</dt> <dd>looks for activities that are active on 2012 New Year's Day. Note that they may start before or finish after that day.</dd> <dt>status==failed;parent==000014-20120425161321-oozie-joe</dt> <dd>looks for failed child activities of the given parent activity id.</dd> <dt>status==started;metrics.cpu_user=gt=10</dt> <dd>looks for started activities that are using more than 10 cores per second.</dd> <dt>type==hive;metrics.user==bc;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for all hive queries submitted by user bc after the given date time.</dd> </dl>  You may query any fields present in the ApiActivity object. You can also query by activity metric values using the <em>metrics.*</em> syntax. Values for date time fields should be ISO8601 timestamps. <p> The valid comparators are <em>==</em>, <em>!=</em>, <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"==\", \"!=\", \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively.
Integer resultOffset = 0; // Integer | Specified the offset of activities to return.
String view = "summary"; // String | The view of the activities to materialize
try {
    ApiActivityList result = apiInstance.readActivities(clusterName, serviceName, maxResults, query, resultOffset, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivitiesResourceApi#readActivities");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**| The name of the cluster |
 **serviceName** | **String**| The name of the service |
 **maxResults** | **Integer**| The maximum number of activities to return. | [optional] [default to 100]
 **query** | **String**| The query to perform to find activities in the system. By default, this call returns top level (i.e. root) activities that have currently started. <p> The query specifies the intersection of a list of constraints, joined together with semicolons (without spaces). For example: </p> <dl> <dt>status==started;parent==</dt> <dd>looks for running root activities. This is also the default query.</dd> <dt>status==failed;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for failed activities after the given date time.</dd> <dt>name==Pi Estimator;startTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for activities started after the given date time, with the name of \"Pi Estimator\".</dd> <dt>startTime=lt=2012-01-02T00:00:00.000Z;finishTime=ge=2012-01-01T00:00:00.000Z</dt> <dd>looks for activities that are active on 2012 New Year's Day. Note that they may start before or finish after that day.</dd> <dt>status==failed;parent==000014-20120425161321-oozie-joe</dt> <dd>looks for failed child activities of the given parent activity id.</dd> <dt>status==started;metrics.cpu_user=gt=10</dt> <dd>looks for started activities that are using more than 10 cores per second.</dd> <dt>type==hive;metrics.user==bc;finishTime=gt=2012-04-01T20:30:00.000Z</dt> <dd>looks for all hive queries submitted by user bc after the given date time.</dd> </dl>  You may query any fields present in the ApiActivity object. You can also query by activity metric values using the <em>metrics.*</em> syntax. Values for date time fields should be ISO8601 timestamps. <p> The valid comparators are <em>==</em>, <em>!=</em>, <em>=lt=</em>, <em>=le=</em>, <em>=ge=</em>, and <em>=gt=</em>. They stand for \"==\", \"!=\", \"&lt;\", \"&lt;=\", \"&gt;=\", \"&gt;\" respectively. | [optional] [default to status&#x3D;&#x3D;started;parent&#x3D;&#x3D;]
 **resultOffset** | **Integer**| Specified the offset of activities to return. | [optional] [default to 0]
 **view** | **String**| The view of the activities to materialize | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiActivityList**](ApiActivityList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readActivity"></a>
# **readActivity**
> ApiActivity readActivity(activityId, clusterName, serviceName, view)

Returns a specific activity in the system.

Returns a specific activity in the system

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ActivitiesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ActivitiesResourceApi apiInstance = new ActivitiesResourceApi();
String activityId = "activityId_example"; // String | The id of the activity to retrieve
String clusterName = "clusterName_example"; // String | The name of the cluster
String serviceName = "serviceName_example"; // String | The name of the service
String view = "summary"; // String | The view of the activity to materialize
try {
    ApiActivity result = apiInstance.readActivity(activityId, clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivitiesResourceApi#readActivity");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityId** | **String**| The id of the activity to retrieve |
 **clusterName** | **String**| The name of the cluster |
 **serviceName** | **String**| The name of the service |
 **view** | **String**| The view of the activity to materialize | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiActivity**](ApiActivity.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readChildActivities"></a>
# **readChildActivities**
> ApiActivityList readChildActivities(activityId, clusterName, serviceName, maxResults, resultOffset, view)

Returns the child activities.

Returns the child activities

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ActivitiesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ActivitiesResourceApi apiInstance = new ActivitiesResourceApi();
String activityId = "activityId_example"; // String | The id of the activity
String clusterName = "clusterName_example"; // String | The name of the cluster
String serviceName = "serviceName_example"; // String | The name of the service
Integer maxResults = 100; // Integer | The maximum number of activities to return.
Integer resultOffset = 0; // Integer | Specified the offset of activities to return.
String view = "summary"; // String | The view of the children to materialize
try {
    ApiActivityList result = apiInstance.readChildActivities(activityId, clusterName, serviceName, maxResults, resultOffset, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivitiesResourceApi#readChildActivities");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityId** | **String**| The id of the activity |
 **clusterName** | **String**| The name of the cluster |
 **serviceName** | **String**| The name of the service |
 **maxResults** | **Integer**| The maximum number of activities to return. | [optional] [default to 100]
 **resultOffset** | **Integer**| Specified the offset of activities to return. | [optional] [default to 0]
 **view** | **String**| The view of the children to materialize | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiActivityList**](ApiActivityList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readSimilarActivities"></a>
# **readSimilarActivities**
> ApiActivityList readSimilarActivities(activityId, clusterName, serviceName, view)

Returns a list of similar activities.

Returns a list of similar activities

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ActivitiesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ActivitiesResourceApi apiInstance = new ActivitiesResourceApi();
String activityId = "activityId_example"; // String | The id of the activity
String clusterName = "clusterName_example"; // String | The name of the cluster
String serviceName = "serviceName_example"; // String | The name of the service
String view = "summary"; // String | The view of the activities to materialize
try {
    ApiActivityList result = apiInstance.readSimilarActivities(activityId, clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivitiesResourceApi#readSimilarActivities");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityId** | **String**| The id of the activity |
 **clusterName** | **String**| The name of the cluster |
 **serviceName** | **String**| The name of the service |
 **view** | **String**| The view of the activities to materialize | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiActivityList**](ApiActivityList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

