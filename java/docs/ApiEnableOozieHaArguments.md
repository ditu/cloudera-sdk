
# ApiEnableOozieHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newOozieServerHostIds** | **List&lt;String&gt;** | IDs of the hosts on which new Oozie Servers will be added. |  [optional]
**newOozieServerRoleNames** | **List&lt;String&gt;** | Names of the new Oozie Servers. This is an optional argument, but if provided, it should match the length of host IDs provided. |  [optional]
**zkServiceName** | **String** | Name of the ZooKeeper service that will be used for Oozie HA. This is an optional parameter if the Oozie to ZooKeeper dependency is already set in CM. |  [optional]
**loadBalancerHostname** | **String** | Hostname of the load balancer used for Oozie HA. Optional if load balancer host and ports are already set in CM. |  [optional]
**loadBalancerPort** | [**BigDecimal**](BigDecimal.md) | HTTP port of the load balancer used for Oozie HA. Optional if load balancer host and ports are already set in CM. |  [optional]
**loadBalancerSslPort** | [**BigDecimal**](BigDecimal.md) | HTTPS port of the load balancer used for Oozie HA when SSL is enabled. This port is only used for oozie.base.url -- the callback is always on HTTP. Optional if load balancer host and ports are already set in CM. |  [optional]
**loadBalancerHostPort** | **String** | Address of the load balancer used for Oozie HA. This is an optional parameter if this config is already set in CM. |  [optional]



