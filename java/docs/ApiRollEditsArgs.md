
# ApiRollEditsArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nameservice** | **String** | Nameservice whose edits need to be rolled. Required only if HDFS service is federated. |  [optional]



