# AuthRoleMetadatasResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**readAuthRolesMetadata**](AuthRoleMetadatasResourceApi.md#readAuthRolesMetadata) | **GET** /authRoleMetadatas | Returns a list of the auth roles&#39; metadata for the built-in roles.


<a name="readAuthRolesMetadata"></a>
# **readAuthRolesMetadata**
> ApiAuthRoleMetadataList readAuthRolesMetadata(view)

Returns a list of the auth roles' metadata for the built-in roles.

Returns a list of the auth roles' metadata for the built-in roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthRoleMetadatasResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthRoleMetadatasResourceApi apiInstance = new AuthRoleMetadatasResourceApi();
String view = "summary"; // String | 
try {
    ApiAuthRoleMetadataList result = apiInstance.readAuthRolesMetadata(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthRoleMetadatasResourceApi#readAuthRolesMetadata");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiAuthRoleMetadataList**](ApiAuthRoleMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

