# MgmtRoleConfigGroupsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**readConfig**](MgmtRoleConfigGroupsResourceApi.md#readConfig) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName}/config | Returns the current revision of the config for the specified role config group in the Cloudera Management Services.
[**readRoleConfigGroup**](MgmtRoleConfigGroupsResourceApi.md#readRoleConfigGroup) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName} | Returns the information for a given role config group in the Cloudera Management Services.
[**readRoleConfigGroups**](MgmtRoleConfigGroupsResourceApi.md#readRoleConfigGroups) | **GET** /cm/service/roleConfigGroups | Returns the information for all role config groups in the Cloudera Management Services.
[**readRoles**](MgmtRoleConfigGroupsResourceApi.md#readRoles) | **GET** /cm/service/roleConfigGroups/{roleConfigGroupName}/roles | Returns all roles in the given role config group in the Cloudera Management Services.
[**updateConfig**](MgmtRoleConfigGroupsResourceApi.md#updateConfig) | **PUT** /cm/service/roleConfigGroups/{roleConfigGroupName}/config | Updates the config for the given role config group in the Cloudera Management Services.
[**updateRoleConfigGroup**](MgmtRoleConfigGroupsResourceApi.md#updateRoleConfigGroup) | **PUT** /cm/service/roleConfigGroups/{roleConfigGroupName} | Updates an existing role config group in the Cloudera Management Services.


<a name="readConfig"></a>
# **readConfig**
> ApiConfigList readConfig(roleConfigGroupName, view)

Returns the current revision of the config for the specified role config group in the Cloudera Management Services.

Returns the current revision of the config for the specified role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleConfigGroupsResourceApi apiInstance = new MgmtRoleConfigGroupsResourceApi();
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the role config group.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.readConfig(roleConfigGroupName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleConfigGroupsResourceApi#readConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleConfigGroupName** | **String**| The name of the role config group. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoleConfigGroup"></a>
# **readRoleConfigGroup**
> ApiRoleConfigGroup readRoleConfigGroup(roleConfigGroupName)

Returns the information for a given role config group in the Cloudera Management Services.

Returns the information for a given role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleConfigGroupsResourceApi apiInstance = new MgmtRoleConfigGroupsResourceApi();
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the requested group.
try {
    ApiRoleConfigGroup result = apiInstance.readRoleConfigGroup(roleConfigGroupName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleConfigGroupsResourceApi#readRoleConfigGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleConfigGroupName** | **String**| The name of the requested group. |

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoleConfigGroups"></a>
# **readRoleConfigGroups**
> ApiRoleConfigGroupList readRoleConfigGroups()

Returns the information for all role config groups in the Cloudera Management Services.

Returns the information for all role config groups in the Cloudera Management Services. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleConfigGroupsResourceApi apiInstance = new MgmtRoleConfigGroupsResourceApi();
try {
    ApiRoleConfigGroupList result = apiInstance.readRoleConfigGroups();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleConfigGroupsResourceApi#readRoleConfigGroups");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRoleConfigGroupList**](ApiRoleConfigGroupList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readRoles"></a>
# **readRoles**
> ApiRoleList readRoles(roleConfigGroupName)

Returns all roles in the given role config group in the Cloudera Management Services.

Returns all roles in the given role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleConfigGroupsResourceApi apiInstance = new MgmtRoleConfigGroupsResourceApi();
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the role config group.
try {
    ApiRoleList result = apiInstance.readRoles(roleConfigGroupName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleConfigGroupsResourceApi#readRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleConfigGroupName** | **String**| The name of the role config group. |

### Return type

[**ApiRoleList**](ApiRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateConfig"></a>
# **updateConfig**
> ApiConfigList updateConfig(roleConfigGroupName, message, body)

Updates the config for the given role config group in the Cloudera Management Services.

Updates the config for the given role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleConfigGroupsResourceApi apiInstance = new MgmtRoleConfigGroupsResourceApi();
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the role config group.
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | The new config information for the group.
try {
    ApiConfigList result = apiInstance.updateConfig(roleConfigGroupName, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleConfigGroupsResourceApi#updateConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleConfigGroupName** | **String**| The name of the role config group. |
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| The new config information for the group. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateRoleConfigGroup"></a>
# **updateRoleConfigGroup**
> ApiRoleConfigGroup updateRoleConfigGroup(roleConfigGroupName, message, body)

Updates an existing role config group in the Cloudera Management Services.

Updates an existing role config group in the Cloudera Management Services. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.MgmtRoleConfigGroupsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

MgmtRoleConfigGroupsResourceApi apiInstance = new MgmtRoleConfigGroupsResourceApi();
String roleConfigGroupName = "roleConfigGroupName_example"; // String | The name of the group to update.
String message = "message_example"; // String | The optional message describing the changes.
ApiRoleConfigGroup body = new ApiRoleConfigGroup(); // ApiRoleConfigGroup | The updated role config group.
try {
    ApiRoleConfigGroup result = apiInstance.updateRoleConfigGroup(roleConfigGroupName, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MgmtRoleConfigGroupsResourceApi#updateRoleConfigGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roleConfigGroupName** | **String**| The name of the group to update. |
 **message** | **String**| The optional message describing the changes. | [optional]
 **body** | [**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)| The updated role config group. | [optional]

### Return type

[**ApiRoleConfigGroup**](ApiRoleConfigGroup.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

