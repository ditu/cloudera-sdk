
# ApiEnableSentryHaArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newSentryHostId** | **String** | Id of host on which new Sentry Server role will be added. |  [optional]
**newSentryRoleName** | **String** | Name of the new Sentry Server role to be created. This is an optional argument. |  [optional]
**zkServiceName** | **String** | Name of the ZooKeeper service that will be used for Sentry HA. This is an optional parameter if the Sentry to ZooKeeper dependency is already set in CM. |  [optional]
**rrcArgs** | [**ApiSimpleRollingRestartClusterArgs**](ApiSimpleRollingRestartClusterArgs.md) |  |  [optional]



