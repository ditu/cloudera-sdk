
# ApiHBaseSnapshot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**snapshotName** | **String** | Snapshot name. |  [optional]
**tableName** | **String** | Name of the table this snapshot is for. |  [optional]
**creationTime** | **String** | Snapshot creation time. |  [optional]
**storage** | [**Storage**](Storage.md) | The location where a snapshot is stored. |  [optional]



