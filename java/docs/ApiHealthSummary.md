
# ApiHealthSummary

## Enum


* `DISABLED` (value: `"DISABLED"`)

* `HISTORY_NOT_AVAILABLE` (value: `"HISTORY_NOT_AVAILABLE"`)

* `NOT_AVAILABLE` (value: `"NOT_AVAILABLE"`)

* `GOOD` (value: `"GOOD"`)

* `CONCERNING` (value: `"CONCERNING"`)

* `BAD` (value: `"BAD"`)



