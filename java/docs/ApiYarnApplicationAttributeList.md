
# ApiYarnApplicationAttributeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiYarnApplicationAttribute&gt;**](ApiYarnApplicationAttribute.md) |  |  [optional]



