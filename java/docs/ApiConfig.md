
# ApiConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Readonly. The canonical name that identifies this configuration parameter. |  [optional]
**value** | **String** | The user-defined value. When absent, the default value (if any) will be used. Can also be absent, when enumerating allowed configs. |  [optional]
**required** | **Boolean** | Readonly. Requires \&quot;full\&quot; view. Whether this configuration is required for the object. If any required configuration is not set, operations on the object may not work. |  [optional]
**_default** | **String** | Readonly. Requires \&quot;full\&quot; view. The default value. |  [optional]
**displayName** | **String** | Readonly. Requires \&quot;full\&quot; view. A user-friendly name of the parameters, as would have been shown in the web UI. |  [optional]
**description** | **String** | Readonly. Requires \&quot;full\&quot; view. A textual description of the parameter. |  [optional]
**relatedName** | **String** | Readonly. Requires \&quot;full\&quot; view. If applicable, contains the related configuration variable used by the source project. |  [optional]
**sensitive** | **Boolean** | Readonly. Whether this configuration is sensitive, i.e. contains information such as passwords, which might affect how the value of this configuration might be shared by the caller.  Available since v14. |  [optional]
**validationState** | [**ValidationState**](ValidationState.md) | Readonly. Requires \&quot;full\&quot; view. State of the configuration parameter after validation. |  [optional]
**validationMessage** | **String** | Readonly. Requires \&quot;full\&quot; view. A message explaining the parameter&#39;s validation state. |  [optional]
**validationWarningsSuppressed** | **Boolean** | Readonly. Requires \&quot;full\&quot; view. Whether validation warnings associated with this parameter are suppressed. In general, suppressed validation warnings are hidden in the Cloudera Manager UI. Configurations that do not produce warnings will not contain this field. |  [optional]



