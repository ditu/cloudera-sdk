
# ApiUser2Ref

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the user, which uniquely identifies it in a CM installation. |  [optional]



