# AuthRolesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAuthRoles**](AuthRolesResourceApi.md#createAuthRoles) | **POST** /authRoles | Creates a list of auth roles.
[**deleteAuthRole**](AuthRolesResourceApi.md#deleteAuthRole) | **DELETE** /authRoles/{uuid} | Deletes an auth role from the system.
[**readAuthRole**](AuthRolesResourceApi.md#readAuthRole) | **GET** /authRoles/{uuid} | Returns detailed information about an auth role.
[**readAuthRoles**](AuthRolesResourceApi.md#readAuthRoles) | **GET** /authRoles | Returns a list of the auth roles configured in the system.
[**readAuthRolesMetadata**](AuthRolesResourceApi.md#readAuthRolesMetadata) | **GET** /authRoles/metadata | Returns a list of the auth roles&#39; metadata for the built-in roles.
[**updateAuthRole**](AuthRolesResourceApi.md#updateAuthRole) | **PUT** /authRoles/{uuid} | Updates the given auth role&#39;s information.


<a name="createAuthRoles"></a>
# **createAuthRoles**
> ApiAuthRoleList createAuthRoles(body)

Creates a list of auth roles.

Creates a list of auth roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthRolesResourceApi apiInstance = new AuthRolesResourceApi();
ApiAuthRoleList body = new ApiAuthRoleList(); // ApiAuthRoleList | List of auth roles to create.
try {
    ApiAuthRoleList result = apiInstance.createAuthRoles(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthRolesResourceApi#createAuthRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiAuthRoleList**](ApiAuthRoleList.md)| List of auth roles to create. | [optional]

### Return type

[**ApiAuthRoleList**](ApiAuthRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAuthRole"></a>
# **deleteAuthRole**
> ApiAuthRole deleteAuthRole(uuid)

Deletes an auth role from the system.

Deletes an auth role from the system. <p/>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthRolesResourceApi apiInstance = new AuthRolesResourceApi();
String uuid = "uuid_example"; // String | The uuid of the auth role to delete.
try {
    ApiAuthRole result = apiInstance.deleteAuthRole(uuid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthRolesResourceApi#deleteAuthRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**| The uuid of the auth role to delete. |

### Return type

[**ApiAuthRole**](ApiAuthRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readAuthRole"></a>
# **readAuthRole**
> ApiAuthRole readAuthRole(uuid)

Returns detailed information about an auth role.

Returns detailed information about an auth role.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthRolesResourceApi apiInstance = new AuthRolesResourceApi();
String uuid = "uuid_example"; // String | The auth role to read.
try {
    ApiAuthRole result = apiInstance.readAuthRole(uuid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthRolesResourceApi#readAuthRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**| The auth role to read. |

### Return type

[**ApiAuthRole**](ApiAuthRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readAuthRoles"></a>
# **readAuthRoles**
> ApiAuthRoleList readAuthRoles(view)

Returns a list of the auth roles configured in the system.

Returns a list of the auth roles configured in the system.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthRolesResourceApi apiInstance = new AuthRolesResourceApi();
String view = "summary"; // String | 
try {
    ApiAuthRoleList result = apiInstance.readAuthRoles(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthRolesResourceApi#readAuthRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiAuthRoleList**](ApiAuthRoleList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readAuthRolesMetadata"></a>
# **readAuthRolesMetadata**
> ApiAuthRoleMetadataList readAuthRolesMetadata(view)

Returns a list of the auth roles' metadata for the built-in roles.

Returns a list of the auth roles' metadata for the built-in roles.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthRolesResourceApi apiInstance = new AuthRolesResourceApi();
String view = "summary"; // String | 
try {
    ApiAuthRoleMetadataList result = apiInstance.readAuthRolesMetadata(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthRolesResourceApi#readAuthRolesMetadata");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiAuthRoleMetadataList**](ApiAuthRoleMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateAuthRole"></a>
# **updateAuthRole**
> ApiAuthRole updateAuthRole(uuid, body)

Updates the given auth role's information.

Updates the given auth role's information.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.AuthRolesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

AuthRolesResourceApi apiInstance = new AuthRolesResourceApi();
String uuid = "uuid_example"; // String | Uuid of the auth role being updated.
ApiAuthRole body = new ApiAuthRole(); // ApiAuthRole | The auth role information.
try {
    ApiAuthRole result = apiInstance.updateAuthRole(uuid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthRolesResourceApi#updateAuthRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**| Uuid of the auth role being updated. |
 **body** | [**ApiAuthRole**](ApiAuthRole.md)| The auth role information. | [optional]

### Return type

[**ApiAuthRole**](ApiAuthRole.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

