
# ApiScmDbInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scmDbType** | [**ScmDbType**](ScmDbType.md) | Cloudera Manager server&#39;s db type |  [optional]
**embeddedDbUsed** | **Boolean** | Whether Cloudera Manager server is using embedded DB |  [optional]



