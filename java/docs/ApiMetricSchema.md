
# ApiMetricSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the metric. This name is guaranteed to be unique among the metrics. |  [optional]
**displayName** | **String** | Display name of the metric. |  [optional]
**description** | **String** | Description of the metric. |  [optional]
**isCounter** | **Boolean** | Is the metric a counter. A counter tracks the total count since a process / host started. The rate of change of a counter may often be more interesting than the raw value of a counter. |  [optional]
**unitNumerator** | **String** | Numerator for the unit of the metric. |  [optional]
**unitDenominator** | **String** | Denominator for the unit of the metric. |  [optional]
**aliases** | **List&lt;String&gt;** | Aliases for the metric. An alias is unique per metric (per source and version) but is not globally unique. Aliases usually refer to previous names for the metric as metrics are renamed or replaced. |  [optional]
**sources** | [**List&lt;Map&lt;String, String&gt;&gt;**](Map.md) | Sources for the metric. Each source entry contains the name of the source and a list of versions for which this source is valid |  [optional]



