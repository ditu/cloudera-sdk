
# ApiScheduleInterval

## Enum


* `MINUTE` (value: `"MINUTE"`)

* `HOUR` (value: `"HOUR"`)

* `DAY` (value: `"DAY"`)

* `WEEK` (value: `"WEEK"`)

* `MONTH` (value: `"MONTH"`)

* `YEAR` (value: `"YEAR"`)



