
# ApiHdfsDisableHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeName** | **String** | Name of the the NameNode to be kept. |  [optional]
**secondaryName** | **String** | Name of the SecondaryNamenode to associate with the active NameNode. |  [optional]
**startDependentServices** | **Boolean** | Whether to re-start dependent services. Defaults to true. |  [optional]
**deployClientConfigs** | **Boolean** | Whether to re-deploy client configurations. Defaults to true. |  [optional]
**disableQuorumStorage** | **Boolean** | Whether to disable Quorum-based Storage. Defaults to false.  Available since API v2. |  [optional]



