
# ApiDisableLlamaHaArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activeName** | **String** | Name of the Llama role that will be active after HA is disabled. |  [optional]



