
# ApiHiveReplicationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phase** | **String** | Phase the replication is in. &lt;p/&gt; If the replication job is still active, this will contain a string describing the current phase. This will be one of: EXPORT, DATA or IMPORT, for, respectively, exporting the source metastore information, replicating table data (if configured), and importing metastore information in the target. &lt;p/&gt; This value will not be present if the replication is not active. &lt;p/&gt; Available since API v4. |  [optional]
**tableCount** | [**BigDecimal**](BigDecimal.md) | Number of tables that were successfully replicated. Available since API v4. |  [optional]
**tables** | [**List&lt;ApiHiveTable&gt;**](ApiHiveTable.md) | The list of tables successfully replicated. &lt;p/&gt; Since API v4, this is only available in the full view. |  [optional]
**impalaUDFCount** | [**BigDecimal**](BigDecimal.md) | Number of impala UDFs that were successfully replicated. Available since API v6. |  [optional]
**hiveUDFCount** | [**BigDecimal**](BigDecimal.md) | Number of hive UDFs that were successfully replicated. Available since API v14. |  [optional]
**impalaUDFs** | [**List&lt;ApiImpalaUDF&gt;**](ApiImpalaUDF.md) | The list of Impala UDFs successfully replicated. Available since API v6 in the full view. |  [optional]
**hiveUDFs** | [**List&lt;ApiHiveUDF&gt;**](ApiHiveUDF.md) | The list of Impala UDFs successfully replicated. Available since API v6 in the full view. |  [optional]
**errorCount** | [**BigDecimal**](BigDecimal.md) | Number of errors detected during replication job. Available since API v4. |  [optional]
**errors** | [**List&lt;ApiHiveReplicationError&gt;**](ApiHiveReplicationError.md) | List of errors encountered during replication. &lt;p/&gt; Since API v4, this is only available in the full view. |  [optional]
**dataReplicationResult** | [**ApiHdfsReplicationResult**](ApiHdfsReplicationResult.md) | Result of table data replication, if performed. |  [optional]
**dryRun** | **Boolean** | Whether this was a dry run. |  [optional]
**runAsUser** | **String** | Name of the of proxy user, if any. Available since API v11. |  [optional]
**runOnSourceAsUser** | **String** | Name of the source proxy user, if any. Available since API v18. |  [optional]
**statsAvailable** | **Boolean** | Whether stats are available to display or not. Available since API v19. |  [optional]
**dbProcessed** | [**BigDecimal**](BigDecimal.md) | Number of Db&#39;s Imported/Exported. Available since API v19. |  [optional]
**tableProcessed** | [**BigDecimal**](BigDecimal.md) | Number of Tables Imported/Exported. Available since API v19. |  [optional]
**partitionProcessed** | [**BigDecimal**](BigDecimal.md) | Number of Partitions Imported/Exported. Available since API v19. |  [optional]
**functionProcessed** | [**BigDecimal**](BigDecimal.md) | Number of Functions Imported/Exported. Available since API v19. |  [optional]
**indexProcessed** | [**BigDecimal**](BigDecimal.md) | Number of Indexes Imported/Exported. Available since API v19. |  [optional]
**statsProcessed** | [**BigDecimal**](BigDecimal.md) | Number of Table and Partitions Statistics Imported/Exported. Available since API v19. |  [optional]
**dbExpected** | [**BigDecimal**](BigDecimal.md) | Number of Db&#39;s Expected. Available since API v19. |  [optional]
**tableExpected** | [**BigDecimal**](BigDecimal.md) | Number of Tables Expected. Available since API v19. |  [optional]
**partitionExpected** | [**BigDecimal**](BigDecimal.md) | Number of Partitions Expected. Available since API v19. |  [optional]
**functionExpected** | [**BigDecimal**](BigDecimal.md) | Number of Functions Expected. Available since API v19. |  [optional]
**indexExpected** | [**BigDecimal**](BigDecimal.md) | Number of Indexes Expected. Available since API v19. |  [optional]
**statsExpected** | [**BigDecimal**](BigDecimal.md) | Number of Table and Partition Statistics Expected. Available since API v19. |  [optional]



