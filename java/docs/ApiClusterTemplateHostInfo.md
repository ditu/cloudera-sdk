
# ApiClusterTemplateHostInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostName** | **String** |  |  [optional]
**hostNameRange** | **String** |  |  [optional]
**rackId** | **String** |  |  [optional]
**hostTemplateRefName** | **String** |  |  [optional]
**roleRefNames** | **List&lt;String&gt;** |  |  [optional]



