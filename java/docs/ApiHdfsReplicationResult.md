
# ApiHdfsReplicationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**progress** | [**BigDecimal**](BigDecimal.md) | The file copy progress percentage. |  [optional]
**throughput** | [**BigDecimal**](BigDecimal.md) | The data throughput in KB/s. |  [optional]
**remainingTime** | [**BigDecimal**](BigDecimal.md) | The time remaining for mapper phase (seconds). |  [optional]
**estimatedCompletionTime** | **String** | The estimated completion time for the mapper phase. |  [optional]
**counters** | [**List&lt;ApiHdfsReplicationCounter&gt;**](ApiHdfsReplicationCounter.md) | The counters collected from the replication job. &lt;p/&gt; Starting with API v4, the full list of counters is only available in the full view. |  [optional]
**numFilesDryRun** | [**BigDecimal**](BigDecimal.md) | The number of files found to copy. |  [optional]
**numBytesDryRun** | [**BigDecimal**](BigDecimal.md) | The number of bytes found to copy. |  [optional]
**numFilesExpected** | [**BigDecimal**](BigDecimal.md) | The number of files expected to be copied. |  [optional]
**numBytesExpected** | [**BigDecimal**](BigDecimal.md) | The number of bytes expected to be copied. |  [optional]
**numFilesCopied** | [**BigDecimal**](BigDecimal.md) | The number of files actually copied. |  [optional]
**numBytesCopied** | [**BigDecimal**](BigDecimal.md) | The number of bytes actually copied. |  [optional]
**numFilesSkipped** | [**BigDecimal**](BigDecimal.md) | The number of files that were unchanged and thus skipped during copying. |  [optional]
**numBytesSkipped** | [**BigDecimal**](BigDecimal.md) | The aggregate number of bytes in the skipped files. |  [optional]
**numFilesDeleted** | [**BigDecimal**](BigDecimal.md) | The number of files deleted since they were present at destination, but missing from source. |  [optional]
**numFilesCopyFailed** | [**BigDecimal**](BigDecimal.md) | The number of files for which copy failed. |  [optional]
**numBytesCopyFailed** | [**BigDecimal**](BigDecimal.md) | The aggregate number of bytes in the files for which copy failed. |  [optional]
**setupError** | **String** | The error that happened during job setup, if any. |  [optional]
**jobId** | **String** | Read-only. The MapReduce job ID for the replication job. Available since API v4. &lt;p/&gt; This can be used to query information about the replication job from the MapReduce server where it was executed. Refer to the \&quot;/activities\&quot; resource for services for further details. |  [optional]
**jobDetailsUri** | **String** | Read-only. The URI (relative to the CM server&#39;s root) where to find the Activity Monitor page for the job. Available since API v4. |  [optional]
**dryRun** | **Boolean** | Whether this was a dry run. |  [optional]
**snapshottedDirs** | **List&lt;String&gt;** | The list of directories for which snapshots were taken and used as part of this replication. |  [optional]
**runAsUser** | **String** | Returns run-as user name. Available since API v11. |  [optional]
**runOnSourceAsUser** | **String** | Returns run-as user name for source cluster. Available since API v18. |  [optional]
**failedFiles** | **List&lt;String&gt;** | The list of files that failed during replication. Available since API v11. |  [optional]



