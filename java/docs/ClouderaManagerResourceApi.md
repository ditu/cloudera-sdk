# ClouderaManagerResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**beginTrial**](ClouderaManagerResourceApi.md#beginTrial) | **POST** /cm/trial/begin | Begin trial license.
[**clustersPerfInspectorCommand**](ClouderaManagerResourceApi.md#clustersPerfInspectorCommand) | **POST** /cm/commands/clustersPerfInspector | Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
[**collectDiagnosticDataCommand**](ClouderaManagerResourceApi.md#collectDiagnosticDataCommand) | **POST** /cm/commands/collectDiagnosticData | Collect diagnostic data from hosts managed by Cloudera Manager.
[**deleteCredentialsCommand**](ClouderaManagerResourceApi.md#deleteCredentialsCommand) | **POST** /cm/commands/deleteCredentials | Delete existing Kerberos credentials.
[**endTrial**](ClouderaManagerResourceApi.md#endTrial) | **POST** /cm/trial/end | End trial license.
[**generateCredentialsCommand**](ClouderaManagerResourceApi.md#generateCredentialsCommand) | **POST** /cm/commands/generateCredentials | Generate missing Kerberos credentials.
[**getConfig**](ClouderaManagerResourceApi.md#getConfig) | **GET** /cm/config | Retrieve the Cloudera Manager settings.
[**getDeployment2**](ClouderaManagerResourceApi.md#getDeployment2) | **GET** /cm/deployment | Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc.
[**getKerberosInfo**](ClouderaManagerResourceApi.md#getKerberosInfo) | **GET** /cm/kerberosInfo | Provides Cloudera Manager Kerberos information.
[**getKerberosPrincipals**](ClouderaManagerResourceApi.md#getKerberosPrincipals) | **GET** /cm/kerberosPrincipals | Returns the Kerberos principals needed by the services being managed by Cloudera Manager.
[**getLicensedFeatureUsage**](ClouderaManagerResourceApi.md#getLicensedFeatureUsage) | **GET** /cm/licensedFeatureUsage | Retrieve a summary of licensed feature usage.
[**getLog**](ClouderaManagerResourceApi.md#getLog) | **GET** /cm/log | Returns the entire contents of the Cloudera Manager log file.
[**getScmDbInfo**](ClouderaManagerResourceApi.md#getScmDbInfo) | **GET** /cm/scmDbInfo | Provides Cloudera Manager server&#39;s database information.
[**getShutdownReadiness**](ClouderaManagerResourceApi.md#getShutdownReadiness) | **GET** /cm/shutdownReadiness | Retrieve Cloudera Manager&#39;s readiness for shutdown and destroy.
[**getVersion**](ClouderaManagerResourceApi.md#getVersion) | **GET** /cm/version | Provides version information of Cloudera Manager itself.
[**hostInstallCommand**](ClouderaManagerResourceApi.md#hostInstallCommand) | **POST** /cm/commands/hostInstall | Perform installation on a set of hosts.
[**hostsDecommissionCommand**](ClouderaManagerResourceApi.md#hostsDecommissionCommand) | **POST** /cm/commands/hostsDecommission | Decommission the given hosts.
[**hostsOfflineOrDecommissionCommand**](ClouderaManagerResourceApi.md#hostsOfflineOrDecommissionCommand) | **POST** /cm/commands/hostsOfflineOrDecommission | Decommission the given hosts.
[**hostsPerfInspectorCommand**](ClouderaManagerResourceApi.md#hostsPerfInspectorCommand) | **POST** /cm/commands/hostsPerfInspector | Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.
[**hostsRecommissionAndExitMaintenanceModeCommand**](ClouderaManagerResourceApi.md#hostsRecommissionAndExitMaintenanceModeCommand) | **POST** /cm/commands/hostsRecommissionAndExitMaintenanceMode | Recommission and exit maintenance on the given hosts.
[**hostsRecommissionCommand**](ClouderaManagerResourceApi.md#hostsRecommissionCommand) | **POST** /cm/commands/hostsRecommission | Recommission the given hosts.
[**hostsRecommissionWithStartCommand**](ClouderaManagerResourceApi.md#hostsRecommissionWithStartCommand) | **POST** /cm/commands/hostsRecommissionWithStart | Recommission the given hosts.
[**hostsStartRolesCommand**](ClouderaManagerResourceApi.md#hostsStartRolesCommand) | **POST** /cm/commands/hostsStartRoles | Start all the roles on the given hosts.
[**importAdminCredentials**](ClouderaManagerResourceApi.md#importAdminCredentials) | **POST** /cm/commands/importAdminCredentials | Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.
[**importClusterTemplate**](ClouderaManagerResourceApi.md#importClusterTemplate) | **POST** /cm/importClusterTemplate | Create cluster as per the given cluster template.
[**importKerberosPrincipal**](ClouderaManagerResourceApi.md#importKerberosPrincipal) | **POST** /cm/commands/importKerberosPrincipal | Imports the Kerberos credentials for the specified principal which can then be used to add to a role&#39;s keytab by running Generate Credentials command.
[**inspectHostsCommand**](ClouderaManagerResourceApi.md#inspectHostsCommand) | **POST** /cm/commands/inspectHosts | Runs the host inspector on the configured hosts.
[**listActiveCommands**](ClouderaManagerResourceApi.md#listActiveCommands) | **GET** /cm/commands | List active global commands.
[**readLicense**](ClouderaManagerResourceApi.md#readLicense) | **GET** /cm/license | Retrieve information about the Cloudera Manager license.
[**refreshParcelRepos**](ClouderaManagerResourceApi.md#refreshParcelRepos) | **POST** /cm/commands/refreshParcelRepos | .
[**updateConfig**](ClouderaManagerResourceApi.md#updateConfig) | **PUT** /cm/config | Update the Cloudera Manager settings.
[**updateDeployment2**](ClouderaManagerResourceApi.md#updateDeployment2) | **PUT** /cm/deployment | Apply the supplied deployment description to the system.
[**updateLicense**](ClouderaManagerResourceApi.md#updateLicense) | **POST** /cm/license | Updates the Cloudera Manager license.


<a name="beginTrial"></a>
# **beginTrial**
> beginTrial()

Begin trial license.

Begin trial license.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    apiInstance.beginTrial();
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#beginTrial");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="clustersPerfInspectorCommand"></a>
# **clustersPerfInspectorCommand**
> ApiCommand clustersPerfInspectorCommand(body)

Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

Run performance diagnostics test against specified clusters in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiClustersPerfInspectorArgs body = new ApiClustersPerfInspectorArgs(); // ApiClustersPerfInspectorArgs | Required arguments for the command. See ApiClustersPerfInspectorArgs.
try {
    ApiCommand result = apiInstance.clustersPerfInspectorCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#clustersPerfInspectorCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiClustersPerfInspectorArgs**](ApiClustersPerfInspectorArgs.md)| Required arguments for the command. See ApiClustersPerfInspectorArgs. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="collectDiagnosticDataCommand"></a>
# **collectDiagnosticDataCommand**
> ApiCommand collectDiagnosticDataCommand(body)

Collect diagnostic data from hosts managed by Cloudera Manager.

Collect diagnostic data from hosts managed by Cloudera Manager. <p> After the command has completed, the ApiCommand will contain a resultDataUrl from where you can download the result. <p/> Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiCollectDiagnosticDataArguments body = new ApiCollectDiagnosticDataArguments(); // ApiCollectDiagnosticDataArguments | The command arguments.
try {
    ApiCommand result = apiInstance.collectDiagnosticDataCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#collectDiagnosticDataCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiCollectDiagnosticDataArguments**](ApiCollectDiagnosticDataArguments.md)| The command arguments. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteCredentialsCommand"></a>
# **deleteCredentialsCommand**
> ApiCommand deleteCredentialsCommand(deleteCredentialsMode)

Delete existing Kerberos credentials.

Delete existing Kerberos credentials. <p> This command will affect all services that have been configured to use Kerberos, and have existing credentials. In V18 this takes a new paramater to determine whether it needs to delete all credentials or just unused ones.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String deleteCredentialsMode = "all"; // String | this can be set to \"all\" or \"unused\"
try {
    ApiCommand result = apiInstance.deleteCredentialsCommand(deleteCredentialsMode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#deleteCredentialsCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deleteCredentialsMode** | **String**| this can be set to \"all\" or \"unused\" | [optional] [default to all]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="endTrial"></a>
# **endTrial**
> endTrial()

End trial license.

End trial license.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    apiInstance.endTrial();
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#endTrial");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="generateCredentialsCommand"></a>
# **generateCredentialsCommand**
> ApiCommand generateCredentialsCommand()

Generate missing Kerberos credentials.

Generate missing Kerberos credentials. <p> This command will affect all services that have been configured to use Kerberos, and haven't had their credentials generated yet.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiCommand result = apiInstance.generateCredentialsCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#generateCredentialsCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getConfig"></a>
# **getConfig**
> ApiConfigList getConfig(view)

Retrieve the Cloudera Manager settings.

Retrieve the Cloudera Manager settings.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String view = "summary"; // String | The view to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.getConfig(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| The view to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDeployment2"></a>
# **getDeployment2**
> ApiDeployment2 getDeployment2(view)

Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc.

Retrieve full description of the entire Cloudera Manager deployment including all hosts, clusters, services, roles, users, settings, etc. <p/> This object can be used to reconstruct your entire deployment <p/> Note: Only users with sufficient privileges are allowed to call this. <ul> <li>Full Administrators</li> <li>Cluster Administrators (but Navigator config will be redacted)</li> </ul> <p/> Note: starting with v19, the deployment information contains a newer version of users ApiUser2 that can hold granular permissions.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String view = "export"; // String | May be one of \"export\" (default) or \"export_redacted\".  The latter replaces configurations that are sensitive with the word \"REDACTED\".
try {
    ApiDeployment2 result = apiInstance.getDeployment2(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getDeployment2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| May be one of \"export\" (default) or \"export_redacted\".  The latter replaces configurations that are sensitive with the word \"REDACTED\". | [optional] [default to export] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiDeployment2**](ApiDeployment2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKerberosInfo"></a>
# **getKerberosInfo**
> ApiKerberosInfo getKerberosInfo()

Provides Cloudera Manager Kerberos information.

Provides Cloudera Manager Kerberos information

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiKerberosInfo result = apiInstance.getKerberosInfo();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getKerberosInfo");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiKerberosInfo**](ApiKerberosInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKerberosPrincipals"></a>
# **getKerberosPrincipals**
> ApiPrincipalList getKerberosPrincipals(missingOnly)

Returns the Kerberos principals needed by the services being managed by Cloudera Manager.

Returns the Kerberos principals needed by the services being managed by Cloudera Manager.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
Boolean missingOnly = true; // Boolean | Whether to include only those principals which do not already exist in Cloudera Manager's database.
try {
    ApiPrincipalList result = apiInstance.getKerberosPrincipals(missingOnly);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getKerberosPrincipals");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **missingOnly** | **Boolean**| Whether to include only those principals which do not already exist in Cloudera Manager's database. | [optional]

### Return type

[**ApiPrincipalList**](ApiPrincipalList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLicensedFeatureUsage"></a>
# **getLicensedFeatureUsage**
> ApiLicensedFeatureUsage getLicensedFeatureUsage()

Retrieve a summary of licensed feature usage.

Retrieve a summary of licensed feature usage. <p/> This command will return information about what Cloudera Enterprise licensed features are in use in the clusters being managed by this Cloudera Manager, as well as totals for usage across all clusters. <p/> The specific features described can vary between different versions of Cloudera Manager. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiLicensedFeatureUsage result = apiInstance.getLicensedFeatureUsage();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getLicensedFeatureUsage");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiLicensedFeatureUsage**](ApiLicensedFeatureUsage.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLog"></a>
# **getLog**
> String getLog()

Returns the entire contents of the Cloudera Manager log file.

Returns the entire contents of the Cloudera Manager log file

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    String result = apiInstance.getLog();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getLog");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getScmDbInfo"></a>
# **getScmDbInfo**
> ApiScmDbInfo getScmDbInfo()

Provides Cloudera Manager server's database information.

Provides Cloudera Manager server's database information

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiScmDbInfo result = apiInstance.getScmDbInfo();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getScmDbInfo");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiScmDbInfo**](ApiScmDbInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getShutdownReadiness"></a>
# **getShutdownReadiness**
> ApiShutdownReadiness getShutdownReadiness(lastActivityTime)

Retrieve Cloudera Manager's readiness for shutdown and destroy.

Retrieve Cloudera Manager's readiness for shutdown and destroy. Applications that wish to destroy Cloudera Manager and its managed cluster should poll this API, repeatedly if necessary, to respect its readiness.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String lastActivityTime = "lastActivityTime_example"; // String | End time of the last known activity/workload against the managed clusters, in ISO 8601 format.
try {
    ApiShutdownReadiness result = apiInstance.getShutdownReadiness(lastActivityTime);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getShutdownReadiness");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lastActivityTime** | **String**| End time of the last known activity/workload against the managed clusters, in ISO 8601 format. | [optional]

### Return type

[**ApiShutdownReadiness**](ApiShutdownReadiness.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVersion"></a>
# **getVersion**
> ApiVersionInfo getVersion()

Provides version information of Cloudera Manager itself.

Provides version information of Cloudera Manager itself.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiVersionInfo result = apiInstance.getVersion();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#getVersion");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiVersionInfo**](ApiVersionInfo.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hostInstallCommand"></a>
# **hostInstallCommand**
> ApiCommand hostInstallCommand(body)

Perform installation on a set of hosts.

Perform installation on a set of hosts. <p/> This command installs Cloudera Manager Agent on a set of hosts. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiHostInstallArguments body = new ApiHostInstallArguments(); // ApiHostInstallArguments | Hosts to perform installation on
try {
    ApiCommand result = apiInstance.hostInstallCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostInstallCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostInstallArguments**](ApiHostInstallArguments.md)| Hosts to perform installation on | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hostsDecommissionCommand"></a>
# **hostsDecommissionCommand**
> ApiCommand hostsDecommissionCommand(body)

Decommission the given hosts.

Decommission the given hosts. All slave roles on the hosts will be decommissioned. All other roles will be stopped.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiHostNameList body = new ApiHostNameList(); // ApiHostNameList | 
try {
    ApiCommand result = apiInstance.hostsDecommissionCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostsDecommissionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hostsOfflineOrDecommissionCommand"></a>
# **hostsOfflineOrDecommissionCommand**
> ApiCommand hostsOfflineOrDecommissionCommand(offlineTimeout, body)

Decommission the given hosts.

Decommission the given hosts. All slave roles on the hosts will be offlined or decommissioned with preference being offlined if supported by the service. <p> Currently the offline operation is only supported by HDFS, where the offline operation will put DataNodes into <em>HDFS IN MAINTENANCE</em> state which prevents unnecessary re-replication which could occur if decommissioned. <p> All other roles on the hosts will be stopped. <p> The <em>offlineTimeout</em> parameter is used to specify a timeout for offline. For HDFS, when the timeout expires, the DataNode will automatically transition out of <em>HDFS IN MAINTENANCE</em> state, back to <em>HDFS IN SERVICE</em> state. <p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
BigDecimal offlineTimeout = new BigDecimal(); // BigDecimal | offline timeout in seconds. Specify as null to get the default timeout (4 hours). Ignored if service does not support he offline operation.
ApiHostNameList body = new ApiHostNameList(); // ApiHostNameList | list of host names to decommission.
try {
    ApiCommand result = apiInstance.hostsOfflineOrDecommissionCommand(offlineTimeout, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostsOfflineOrDecommissionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offlineTimeout** | **BigDecimal**| offline timeout in seconds. Specify as null to get the default timeout (4 hours). Ignored if service does not support he offline operation. | [optional]
 **body** | [**ApiHostNameList**](ApiHostNameList.md)| list of host names to decommission. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hostsPerfInspectorCommand"></a>
# **hostsPerfInspectorCommand**
> ApiCommand hostsPerfInspectorCommand(body)

Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

Run performance diagnostics test against specified hosts in ApiHostsPerfInspectorArgs  User must be Full Administrator or Global Cluster Administrator.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiHostsPerfInspectorArgs body = new ApiHostsPerfInspectorArgs(); // ApiHostsPerfInspectorArgs | Required arguments for the command. See ApiHostsPerfInspectorArgs.
try {
    ApiCommand result = apiInstance.hostsPerfInspectorCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostsPerfInspectorCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostsPerfInspectorArgs**](ApiHostsPerfInspectorArgs.md)| Required arguments for the command. See ApiHostsPerfInspectorArgs. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hostsRecommissionAndExitMaintenanceModeCommand"></a>
# **hostsRecommissionAndExitMaintenanceModeCommand**
> ApiCommand hostsRecommissionAndExitMaintenanceModeCommand(recommissionType, body)

Recommission and exit maintenance on the given hosts.

Recommission and exit maintenance on the given hosts. The recommission step may optionally start roles as well.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String recommissionType = "recommission"; // String | 
ApiHostNameList body = new ApiHostNameList(); // ApiHostNameList | 
try {
    ApiCommand result = apiInstance.hostsRecommissionAndExitMaintenanceModeCommand(recommissionType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostsRecommissionAndExitMaintenanceModeCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recommissionType** | **String**|  | [optional] [default to recommission] [enum: RECOMMISSION, RECOMMISSION_WITH_START]
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hostsRecommissionCommand"></a>
# **hostsRecommissionCommand**
> ApiCommand hostsRecommissionCommand(body)

Recommission the given hosts.

Recommission the given hosts. All slave roles on the hosts will be recommissioned. Roles are not started after this command. Use hostsStartRoles command for that.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiHostNameList body = new ApiHostNameList(); // ApiHostNameList | 
try {
    ApiCommand result = apiInstance.hostsRecommissionCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostsRecommissionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hostsRecommissionWithStartCommand"></a>
# **hostsRecommissionWithStartCommand**
> ApiCommand hostsRecommissionWithStartCommand(body)

Recommission the given hosts.

Recommission the given hosts. If slave roles support start when decommissioned, start those roles before recommission. All slave roles on the hosts will be recommissioned.  Warning: Evolving. This method may change in the future and does not offer standard compatibility guarantees. Recommission the given hosts. If possible, start those roles before recommission. All slave roles on the hosts will be recommissioned. Do not use without guidance from Cloudera.  Currently, only HDFS DataNodes will be started by this command.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiHostNameList body = new ApiHostNameList(); // ApiHostNameList | 
try {
    ApiCommand result = apiInstance.hostsRecommissionWithStartCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostsRecommissionWithStartCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hostsStartRolesCommand"></a>
# **hostsStartRolesCommand**
> ApiCommand hostsStartRolesCommand(body)

Start all the roles on the given hosts.

Start all the roles on the given hosts.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
ApiHostNameList body = new ApiHostNameList(); // ApiHostNameList | 
try {
    ApiCommand result = apiInstance.hostsStartRolesCommand(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#hostsStartRolesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostNameList**](ApiHostNameList.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="importAdminCredentials"></a>
# **importAdminCredentials**
> ApiCommand importAdminCredentials(password, username)

Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.

Imports the KDC Account Manager credentials needed by Cloudera Manager to create kerberos principals needed by CDH services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String password = "password_example"; // String | Password for the Account Manager.  return Information about the submitted command.
String username = "username_example"; // String | Username of the Account Manager. Full name including the Kerberos realm must be specified.
try {
    ApiCommand result = apiInstance.importAdminCredentials(password, username);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#importAdminCredentials");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password** | **String**| Password for the Account Manager.  return Information about the submitted command. | [optional]
 **username** | **String**| Username of the Account Manager. Full name including the Kerberos realm must be specified. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="importClusterTemplate"></a>
# **importClusterTemplate**
> ApiCommand importClusterTemplate(addRepositories, body)

Create cluster as per the given cluster template.

Create cluster as per the given cluster template

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
Boolean addRepositories = false; // Boolean | if true the parcels repositories in the cluster template will be added.
ApiClusterTemplate body = new ApiClusterTemplate(); // ApiClusterTemplate | cluster template
try {
    ApiCommand result = apiInstance.importClusterTemplate(addRepositories, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#importClusterTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addRepositories** | **Boolean**| if true the parcels repositories in the cluster template will be added. | [optional] [default to false]
 **body** | [**ApiClusterTemplate**](ApiClusterTemplate.md)| cluster template | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="importKerberosPrincipal"></a>
# **importKerberosPrincipal**
> ApiCommand importKerberosPrincipal(kvno, password, principal)

Imports the Kerberos credentials for the specified principal which can then be used to add to a role's keytab by running Generate Credentials command.

Imports the Kerberos credentials for the specified principal which can then be used to add to a role's keytab by running Generate Credentials command.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
BigDecimal kvno = new BigDecimal(); // BigDecimal | Key-version number of the password.  return Information about the submitted command.
String password = "password_example"; // String | Password for the Kerberos principal. Cloudera Manager will encrypt the principal and password and use it when needed for a daemon.
String principal = "principal_example"; // String | Name of the principal. Full name including the Kerberos realm must be specified. If it already exists, it will be overwritten.
try {
    ApiCommand result = apiInstance.importKerberosPrincipal(kvno, password, principal);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#importKerberosPrincipal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kvno** | **BigDecimal**| Key-version number of the password.  return Information about the submitted command. | [optional]
 **password** | **String**| Password for the Kerberos principal. Cloudera Manager will encrypt the principal and password and use it when needed for a daemon. | [optional]
 **principal** | **String**| Name of the principal. Full name including the Kerberos realm must be specified. If it already exists, it will be overwritten. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="inspectHostsCommand"></a>
# **inspectHostsCommand**
> ApiCommand inspectHostsCommand()

Runs the host inspector on the configured hosts.

Runs the host inspector on the configured hosts.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiCommand result = apiInstance.inspectHostsCommand();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#inspectHostsCommand");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listActiveCommands"></a>
# **listActiveCommands**
> ApiCommandList listActiveCommands(view)

List active global commands.

List active global commands.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiCommandList result = apiInstance.listActiveCommands(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#listActiveCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readLicense"></a>
# **readLicense**
> ApiLicense readLicense()

Retrieve information about the Cloudera Manager license.

Retrieve information about the Cloudera Manager license.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiLicense result = apiInstance.readLicense();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#readLicense");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiLicense**](ApiLicense.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="refreshParcelRepos"></a>
# **refreshParcelRepos**
> ApiCommand refreshParcelRepos()

.

<p> Submit a command to refresh parcels information. </p> <p> This API could be used following two scenarios.<br> - User updated Cloudera Manager's local parcel repository. <br> - User updated remote parcel locations. <p> User wants to invoke this API to make sure that Cloudera Manager gets latest parcels information. User can then monitor the returned command before proceeding to the next step. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
try {
    ApiCommand result = apiInstance.refreshParcelRepos();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#refreshParcelRepos");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateConfig"></a>
# **updateConfig**
> ApiConfigList updateConfig(message, body)

Update the Cloudera Manager settings.

Update the Cloudera Manager settings. <p> If a value is set in the given configuration, it will be added to the manager's settings, replacing any existing entry. If a value is unset (its value is null), the existing the setting will be erased. <p> Settings that are not listed in the input will maintain their current values.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | Settings to update.
try {
    ApiConfigList result = apiInstance.updateConfig(message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#updateConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| Settings to update. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateDeployment2"></a>
# **updateDeployment2**
> ApiDeployment2 updateDeployment2(deleteCurrentDeployment, body)

Apply the supplied deployment description to the system.

Apply the supplied deployment description to the system. This will create the clusters, services, hosts and other objects specified in the argument. This call does not allow for any merge conflicts. If an entity already exists in the system, this call will fail. You can request, however, that all entities in the system are deleted before instantiating the new ones. <p/> You may specify a complete or partial deployment, e.g. you can provide host info with no clusters.  However, if you request that the current deployment be deleted, you are required to specify at least one admin user or this call will fail. This is to protect you from creating a system that cannot be logged into again. <p/> If there are any errors creating (or optionally deleting) a deployment, all changes will be rolled back leaving the system exactly as it was before calling this method.  The system will never be left in a state where part of the deployment is created and other parts are not. <p/> If the submitted deployment contains entities that require Cloudera Enterprise license, then the license should be provided to Cloudera Manager before making this API call.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
Boolean deleteCurrentDeployment = false; // Boolean | If true, the current deployment is deleted before the specified deployment is applied
ApiDeployment2 body = new ApiDeployment2(); // ApiDeployment2 | The deployment to create
try {
    ApiDeployment2 result = apiInstance.updateDeployment2(deleteCurrentDeployment, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#updateDeployment2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deleteCurrentDeployment** | **Boolean**| If true, the current deployment is deleted before the specified deployment is applied | [optional] [default to false]
 **body** | [**ApiDeployment2**](ApiDeployment2.md)| The deployment to create | [optional]

### Return type

[**ApiDeployment2**](ApiDeployment2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateLicense"></a>
# **updateLicense**
> ApiLicense updateLicense(body)

Updates the Cloudera Manager license.

Updates the Cloudera Manager license. <p> After a new license is installed, the Cloudera Manager needs to be restarted for the changes to take effect. <p> The license file should be uploaded using a request with content type \"multipart/form-data\", instead of being encoded into a JSON representation.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ClouderaManagerResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ClouderaManagerResourceApi apiInstance = new ClouderaManagerResourceApi();
File body = new File("/path/to/file.txt"); // File | 
try {
    ApiLicense result = apiInstance.updateLicense(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ClouderaManagerResourceApi#updateLicense");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **File**|  | [optional]

### Return type

[**ApiLicense**](ApiLicense.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

