
# ApiRoleState

## Enum


* `HISTORY_NOT_AVAILABLE` (value: `"HISTORY_NOT_AVAILABLE"`)

* `UNKNOWN` (value: `"UNKNOWN"`)

* `STARTING` (value: `"STARTING"`)

* `STARTED` (value: `"STARTED"`)

* `BUSY` (value: `"BUSY"`)

* `STOPPING` (value: `"STOPPING"`)

* `STOPPED` (value: `"STOPPED"`)

* `NA` (value: `"NA"`)



