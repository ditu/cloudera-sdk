
# ApiSnapshotCommandList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiSnapshotCommand&gt;**](ApiSnapshotCommand.md) |  |  [optional]



