# BatchResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**execute**](BatchResourceApi.md#execute) | **POST** /batch | Executes a batch of API requests in one database transaction.


<a name="execute"></a>
# **execute**
> ApiBatchResponse execute(body)

Executes a batch of API requests in one database transaction.

Executes a batch of API requests in one database transaction. If any request fails, execution halts and the transaction is rolled back.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.BatchResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

BatchResourceApi apiInstance = new BatchResourceApi();
ApiBatchRequest body = new ApiBatchRequest(); // ApiBatchRequest | Batch of request to execute.
try {
    ApiBatchResponse result = apiInstance.execute(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BatchResourceApi#execute");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiBatchRequest**](ApiBatchRequest.md)| Batch of request to execute. | [optional]

### Return type

[**ApiBatchResponse**](ApiBatchResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

