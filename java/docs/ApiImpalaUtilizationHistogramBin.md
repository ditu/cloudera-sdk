
# ApiImpalaUtilizationHistogramBin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startPointInclusive** | [**BigDecimal**](BigDecimal.md) | start point (inclusive) of the histogram bin. |  [optional]
**endPointExclusive** | [**BigDecimal**](BigDecimal.md) | end point (exclusive) of the histogram bin. |  [optional]
**numberOfImpalaDaemons** | [**BigDecimal**](BigDecimal.md) | Number of Impala daemons. |  [optional]



