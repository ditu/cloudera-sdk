
# ApiClusterTemplateService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refName** | **String** |  |  [optional]
**serviceType** | **String** |  |  [optional]
**serviceConfigs** | [**List&lt;ApiClusterTemplateConfig&gt;**](ApiClusterTemplateConfig.md) |  |  [optional]
**roleConfigGroups** | [**List&lt;ApiClusterTemplateRoleConfigGroup&gt;**](ApiClusterTemplateRoleConfigGroup.md) |  |  [optional]
**roles** | [**List&lt;ApiClusterTemplateRole&gt;**](ApiClusterTemplateRole.md) |  |  [optional]
**displayName** | **String** |  |  [optional]



