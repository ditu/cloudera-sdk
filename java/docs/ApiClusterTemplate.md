
# ApiClusterTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cdhVersion** | **String** |  |  [optional]
**products** | [**List&lt;ApiProductVersion&gt;**](ApiProductVersion.md) |  |  [optional]
**services** | [**List&lt;ApiClusterTemplateService&gt;**](ApiClusterTemplateService.md) |  |  [optional]
**hostTemplates** | [**List&lt;ApiClusterTemplateHostTemplate&gt;**](ApiClusterTemplateHostTemplate.md) |  |  [optional]
**displayName** | **String** |  |  [optional]
**cmVersion** | **String** |  |  [optional]
**instantiator** | [**ApiClusterTemplateInstantiator**](ApiClusterTemplateInstantiator.md) |  |  [optional]
**repositories** | **List&lt;String&gt;** |  |  [optional]



