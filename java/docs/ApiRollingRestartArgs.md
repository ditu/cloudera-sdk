
# ApiRollingRestartArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slaveBatchSize** | [**BigDecimal**](BigDecimal.md) | Number of slave roles to restart at a time. Must be greater than zero. Default is 1.  Please note that for HDFS, this number should be less than the replication factor (default 3) to ensure data availability during rolling restart. |  [optional]
**sleepSeconds** | [**BigDecimal**](BigDecimal.md) | Number of seconds to sleep between restarts of slave role batches.  Must be greater than or equal to 0. Default is 0. |  [optional]
**slaveFailCountThreshold** | [**BigDecimal**](BigDecimal.md) | The threshold for number of slave batches that are allowed to fail to restart before the entire command is considered failed.  Must be greather than or equal to 0. Default is 0. &lt;p&gt; This argument is for ADVANCED users only. &lt;/p&gt; |  [optional]
**staleConfigsOnly** | **Boolean** | Restart roles with stale configs only. |  [optional]
**unUpgradedOnly** | **Boolean** | Restart roles that haven&#39;t been upgraded yet. |  [optional]
**restartRoleTypes** | **List&lt;String&gt;** | Role types to restart. If not specified, all startable roles are restarted.  Both role types and role names should not be specified. |  [optional]
**restartRoleNames** | **List&lt;String&gt;** | List of specific roles to restart. If none are specified, then all roles of specified role types are restarted.  Both role types and role names should not be specified. |  [optional]



