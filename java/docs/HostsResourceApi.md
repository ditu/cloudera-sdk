# HostsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createHosts**](HostsResourceApi.md#createHosts) | **POST** /hosts | .
[**deleteAllHosts**](HostsResourceApi.md#deleteAllHosts) | **DELETE** /hosts | Delete all hosts in the system.
[**deleteHost**](HostsResourceApi.md#deleteHost) | **DELETE** /hosts/{hostId} | Delete a host from the system.
[**enterMaintenanceMode**](HostsResourceApi.md#enterMaintenanceMode) | **POST** /hosts/{hostId}/commands/enterMaintenanceMode | Put the host into maintenance mode.
[**exitMaintenanceMode**](HostsResourceApi.md#exitMaintenanceMode) | **POST** /hosts/{hostId}/commands/exitMaintenanceMode | Take the host out of maintenance mode.
[**generateHostCerts**](HostsResourceApi.md#generateHostCerts) | **POST** /hosts/{hostId}/commands/generateHostCerts | Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.
[**getMetrics**](HostsResourceApi.md#getMetrics) | **GET** /hosts/{hostId}/metrics | Fetch metric readings for a host.
[**migrateRoles**](HostsResourceApi.md#migrateRoles) | **POST** /hosts/{hostId}/commands/migrateRoles | Migrate roles to a different host.
[**readHost**](HostsResourceApi.md#readHost) | **GET** /hosts/{hostId} | Returns a specific Host in the system.
[**readHostConfig**](HostsResourceApi.md#readHostConfig) | **GET** /hosts/{hostId}/config | Retrieves the configuration of a specific host.
[**readHosts**](HostsResourceApi.md#readHosts) | **GET** /hosts | Returns the hostIds for all hosts in the system.
[**updateHost**](HostsResourceApi.md#updateHost) | **PUT** /hosts/{hostId} | .
[**updateHostConfig**](HostsResourceApi.md#updateHostConfig) | **PUT** /hosts/{hostId}/config | Updates the host configuration with the given values.


<a name="createHosts"></a>
# **createHosts**
> ApiHostList createHosts(body)

.

<p>Create one or more hosts.</p> <p>You must specify at least the hostname and ipAddress in the request objects. If no hostId is specified, it will be set to the hostname.  It is an error to try and create host with the same hostId as another host.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
ApiHostList body = new ApiHostList(); // ApiHostList | The list of hosts to create
try {
    ApiHostList result = apiInstance.createHosts(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#createHosts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiHostList**](ApiHostList.md)| The list of hosts to create | [optional]

### Return type

[**ApiHostList**](ApiHostList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAllHosts"></a>
# **deleteAllHosts**
> ApiHostList deleteAllHosts()

Delete all hosts in the system.

Delete all hosts in the system

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
try {
    ApiHostList result = apiInstance.deleteAllHosts();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#deleteAllHosts");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiHostList**](ApiHostList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteHost"></a>
# **deleteHost**
> ApiHost deleteHost(hostId)

Delete a host from the system.

Delete a host from the system

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host to remove
try {
    ApiHost result = apiInstance.deleteHost(hostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#deleteHost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host to remove |

### Return type

[**ApiHost**](ApiHost.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="enterMaintenanceMode"></a>
# **enterMaintenanceMode**
> ApiCommand enterMaintenanceMode(hostId)

Put the host into maintenance mode.

Put the host into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p>Available since API v2.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host
try {
    ApiCommand result = apiInstance.enterMaintenanceMode(hostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#enterMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaintenanceMode"></a>
# **exitMaintenanceMode**
> ApiCommand exitMaintenanceMode(hostId)

Take the host out of maintenance mode.

Take the host out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host
try {
    ApiCommand result = apiInstance.exitMaintenanceMode(hostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#exitMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="generateHostCerts"></a>
# **generateHostCerts**
> ApiCommand generateHostCerts(hostId, body)

Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.

Generates (or regenerates) a key and certificate for this host if Auto-TLS is enabled.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host to generate a certificate for.
ApiGenerateHostCertsArguments body = new ApiGenerateHostCertsArguments(); // ApiGenerateHostCertsArguments | 
try {
    ApiCommand result = apiInstance.generateHostCerts(hostId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#generateHostCerts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host to generate a certificate for. |
 **body** | [**ApiGenerateHostCertsArguments**](ApiGenerateHostCertsArguments.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMetrics"></a>
# **getMetrics**
> ApiMetricList getMetrics(hostId, from, ifs, metrics, queryNw, queryStorage, storageIds, to, view)

Fetch metric readings for a host.

Fetch metric readings for a host. <p> By default, this call will look up all metrics available for the host. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the role, even if no readings are available in the requested window. <p> Host metrics also include per-network interface and per-storage device metrics. Since collecting this data incurs in more overhead, query parameters can be used to choose which network interfaces and storage devices to query, or to these metrics altogether. <p> Storage metrics are collected at different levels; for example, per-disk and per-partition metrics are available. The \"storageIds\" parameter can be used to filter specific storage IDs. <p> In the returned data, the network interfaces and storage IDs can be identified by looking at the \"context\" property of the metric objects.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The host's ID.
String from = "from_example"; // String | Start of the period to query.
List<String> ifs = Arrays.asList("ifs_example"); // List<String> | Network interfaces to query for metrics (default = all).
List<String> metrics = Arrays.asList("metrics_example"); // List<String> | Filter for which metrics to query.
Boolean queryNw = true; // Boolean | Whether to query for network interface metrics.
Boolean queryStorage = true; // Boolean | Whether to query for storage metrics.
List<String> storageIds = Arrays.asList("storageIds_example"); // List<String> | Storage context IDs to query for metrics (default = all).
String to = "now"; // String | End of the period to query.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiMetricList result = apiInstance.getMetrics(hostId, from, ifs, metrics, queryNw, queryStorage, storageIds, to, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#getMetrics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The host's ID. |
 **from** | **String**| Start of the period to query. | [optional]
 **ifs** | [**List&lt;String&gt;**](String.md)| Network interfaces to query for metrics (default = all). | [optional]
 **metrics** | [**List&lt;String&gt;**](String.md)| Filter for which metrics to query. | [optional]
 **queryNw** | **Boolean**| Whether to query for network interface metrics. | [optional] [default to true]
 **queryStorage** | **Boolean**| Whether to query for storage metrics. | [optional] [default to true]
 **storageIds** | [**List&lt;String&gt;**](String.md)| Storage context IDs to query for metrics (default = all). | [optional]
 **to** | **String**| End of the period to query. | [optional] [default to now]
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="migrateRoles"></a>
# **migrateRoles**
> ApiCommand migrateRoles(hostId, body)

Migrate roles to a different host.

Migrate roles to a different host. <p> This command applies only to HDFS NameNode, JournalNode, and Failover Controller roles. In order to migrate these roles: <ul> <li>HDFS High Availability must be enabled, using quorum-based storage.</li> <li>HDFS must not be configured to use a federated nameservice.</li> </ul> <b>Migrating a NameNode or JournalNode role requires cluster downtime</b>. HDFS, along with all of its dependent services, will be stopped at the beginning of the migration process, and restarted at its conclusion. <p>If the active NameNode is selected for migration, a manual failover will be performed before the role is migrated. The role will remain in standby mode after the migration is complete. <p>When migrating a NameNode role, the co-located Failover Controller role must be migrated as well if automatic failover is enabled. The Failover Controller role name must be included in the list of role names to migrate specified in the arguments to this command (it will not be included implicitly). This command does not allow a Failover Controller role to be moved by itself, although it is possible to move a JournalNode independently. <p> Available since API v10.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host on which the roles to migrate currently reside
ApiMigrateRolesArguments body = new ApiMigrateRolesArguments(); // ApiMigrateRolesArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.migrateRoles(hostId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#migrateRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host on which the roles to migrate currently reside |
 **body** | [**ApiMigrateRolesArguments**](ApiMigrateRolesArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="readHost"></a>
# **readHost**
> ApiHost readHost(hostId, view)

Returns a specific Host in the system.

Returns a specific Host in the system.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host to read.
String view = "full"; // String | The view to materialize. Defaults to 'full'.
try {
    ApiHost result = apiInstance.readHost(hostId, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#readHost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host to read. |
 **view** | **String**| The view to materialize. Defaults to 'full'. | [optional] [default to full] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiHost**](ApiHost.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readHostConfig"></a>
# **readHostConfig**
> ApiConfigList readHostConfig(hostId, view)

Retrieves the configuration of a specific host.

Retrieves the configuration of a specific host.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.readHostConfig(hostId, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#readHostConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readHosts"></a>
# **readHosts**
> ApiHostList readHosts(view)

Returns the hostIds for all hosts in the system.

Returns the hostIds for all hosts in the system.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String view = "summary"; // String | The view to materialize
try {
    ApiHostList result = apiInstance.readHosts(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#readHosts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**| The view to materialize | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiHostList**](ApiHostList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateHost"></a>
# **updateHost**
> ApiHost updateHost(hostId, body)

.

<p>Update an existing host in the system.</p> <p>Currently, only updating the rackId is supported.  All other fields of the host will be ignored.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The hostId to update
ApiHost body = new ApiHost(); // ApiHost | The updated host object.
try {
    ApiHost result = apiInstance.updateHost(hostId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#updateHost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The hostId to update |
 **body** | [**ApiHost**](ApiHost.md)| The updated host object. | [optional]

### Return type

[**ApiHost**](ApiHost.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateHostConfig"></a>
# **updateHostConfig**
> ApiConfigList updateHostConfig(hostId, message, body)

Updates the host configuration with the given values.

Updates the host configuration with the given values. <p> If a value is set in the given configuration, it will be added to the host's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostsResourceApi apiInstance = new HostsResourceApi();
String hostId = "hostId_example"; // String | The ID of the host.
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | Configuration changes.
try {
    ApiConfigList result = apiInstance.updateHostConfig(hostId, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostsResourceApi#updateHostConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostId** | **String**| The ID of the host. |
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| Configuration changes. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

