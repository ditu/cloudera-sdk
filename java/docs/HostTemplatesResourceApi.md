# HostTemplatesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**applyHostTemplate**](HostTemplatesResourceApi.md#applyHostTemplate) | **POST** /clusters/{clusterName}/hostTemplates/{hostTemplateName}/commands/applyHostTemplate | Applies a host template to a collection of hosts.
[**createHostTemplates**](HostTemplatesResourceApi.md#createHostTemplates) | **POST** /clusters/{clusterName}/hostTemplates | Creates new host templates.
[**deleteHostTemplate**](HostTemplatesResourceApi.md#deleteHostTemplate) | **DELETE** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Deletes a host template.
[**readHostTemplate**](HostTemplatesResourceApi.md#readHostTemplate) | **GET** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Retrieves information about a host template.
[**readHostTemplates**](HostTemplatesResourceApi.md#readHostTemplates) | **GET** /clusters/{clusterName}/hostTemplates | Lists all host templates in a cluster.
[**updateHostTemplate**](HostTemplatesResourceApi.md#updateHostTemplate) | **PUT** /clusters/{clusterName}/hostTemplates/{hostTemplateName} | Updates an existing host template.


<a name="applyHostTemplate"></a>
# **applyHostTemplate**
> ApiCommand applyHostTemplate(clusterName, hostTemplateName, startRoles, body)

Applies a host template to a collection of hosts.

Applies a host template to a collection of hosts. This will create a role for each role config group on each of the hosts. <p> The provided hosts must not have any existing roles on them and if the cluster is not using parcels, the hosts must have a CDH version matching that of the cluster version. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostTemplatesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostTemplatesResourceApi apiInstance = new HostTemplatesResourceApi();
String clusterName = "clusterName_example"; // String | 
String hostTemplateName = "hostTemplateName_example"; // String | Host template to apply.
Boolean startRoles = true; // Boolean | Whether to start the newly created roles or not.
ApiHostRefList body = new ApiHostRefList(); // ApiHostRefList | List of hosts to apply the host template to.
try {
    ApiCommand result = apiInstance.applyHostTemplate(clusterName, hostTemplateName, startRoles, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostTemplatesResourceApi#applyHostTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **hostTemplateName** | **String**| Host template to apply. |
 **startRoles** | **Boolean**| Whether to start the newly created roles or not. | [optional]
 **body** | [**ApiHostRefList**](ApiHostRefList.md)| List of hosts to apply the host template to. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createHostTemplates"></a>
# **createHostTemplates**
> ApiHostTemplateList createHostTemplates(clusterName, body)

Creates new host templates.

Creates new host templates. <p> Host template names must be unique across clusters. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostTemplatesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostTemplatesResourceApi apiInstance = new HostTemplatesResourceApi();
String clusterName = "clusterName_example"; // String | 
ApiHostTemplateList body = new ApiHostTemplateList(); // ApiHostTemplateList | The list of host templates to create.
try {
    ApiHostTemplateList result = apiInstance.createHostTemplates(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostTemplatesResourceApi#createHostTemplates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **body** | [**ApiHostTemplateList**](ApiHostTemplateList.md)| The list of host templates to create. | [optional]

### Return type

[**ApiHostTemplateList**](ApiHostTemplateList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteHostTemplate"></a>
# **deleteHostTemplate**
> ApiHostTemplate deleteHostTemplate(clusterName, hostTemplateName)

Deletes a host template.

Deletes a host template. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostTemplatesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostTemplatesResourceApi apiInstance = new HostTemplatesResourceApi();
String clusterName = "clusterName_example"; // String | 
String hostTemplateName = "hostTemplateName_example"; // String | Host template to delete.
try {
    ApiHostTemplate result = apiInstance.deleteHostTemplate(clusterName, hostTemplateName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostTemplatesResourceApi#deleteHostTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **hostTemplateName** | **String**| Host template to delete. |

### Return type

[**ApiHostTemplate**](ApiHostTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readHostTemplate"></a>
# **readHostTemplate**
> ApiHostTemplate readHostTemplate(clusterName, hostTemplateName)

Retrieves information about a host template.

Retrieves information about a host template. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostTemplatesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostTemplatesResourceApi apiInstance = new HostTemplatesResourceApi();
String clusterName = "clusterName_example"; // String | 
String hostTemplateName = "hostTemplateName_example"; // String | 
try {
    ApiHostTemplate result = apiInstance.readHostTemplate(clusterName, hostTemplateName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostTemplatesResourceApi#readHostTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **hostTemplateName** | **String**|  |

### Return type

[**ApiHostTemplate**](ApiHostTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readHostTemplates"></a>
# **readHostTemplates**
> ApiHostTemplateList readHostTemplates(clusterName)

Lists all host templates in a cluster.

Lists all host templates in a cluster. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostTemplatesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostTemplatesResourceApi apiInstance = new HostTemplatesResourceApi();
String clusterName = "clusterName_example"; // String | 
try {
    ApiHostTemplateList result = apiInstance.readHostTemplates(clusterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostTemplatesResourceApi#readHostTemplates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |

### Return type

[**ApiHostTemplateList**](ApiHostTemplateList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateHostTemplate"></a>
# **updateHostTemplate**
> ApiHostTemplate updateHostTemplate(clusterName, hostTemplateName, body)

Updates an existing host template.

Updates an existing host template. <p> Can be used to update the role config groups in a host template or rename it. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.HostTemplatesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

HostTemplatesResourceApi apiInstance = new HostTemplatesResourceApi();
String clusterName = "clusterName_example"; // String | 
String hostTemplateName = "hostTemplateName_example"; // String | Host template with updated fields.
ApiHostTemplate body = new ApiHostTemplate(); // ApiHostTemplate | 
try {
    ApiHostTemplate result = apiInstance.updateHostTemplate(clusterName, hostTemplateName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HostTemplatesResourceApi#updateHostTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **hostTemplateName** | **String**| Host template with updated fields. |
 **body** | [**ApiHostTemplate**](ApiHostTemplate.md)|  | [optional]

### Return type

[**ApiHostTemplate**](ApiHostTemplate.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

