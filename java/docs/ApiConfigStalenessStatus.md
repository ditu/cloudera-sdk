
# ApiConfigStalenessStatus

## Enum


* `FRESH` (value: `"FRESH"`)

* `STALE_REFRESHABLE` (value: `"STALE_REFRESHABLE"`)

* `STALE` (value: `"STALE"`)



