
# ApiClusterPerfInspectorArgs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pingArgs** | [**ApiPerfInspectorPingArgs**](ApiPerfInspectorPingArgs.md) | Optional ping request arguments. If not specified, default arguments will be used for ping test. |  [optional]



