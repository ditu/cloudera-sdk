
# ApiHiveTable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**database** | **String** | Name of the database to which this table belongs. |  [optional]
**tableName** | **String** | Name of the table. When used as input for a replication job, this can be a regular expression that matches several table names. Refer to the Hive documentation for the syntax of regular expressions. |  [optional]



