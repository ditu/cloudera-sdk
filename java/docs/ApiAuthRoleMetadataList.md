
# ApiAuthRoleMetadataList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiAuthRoleMetadata&gt;**](ApiAuthRoleMetadata.md) |  |  [optional]



