
# ApiHBaseSnapshotPolicyArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tableRegExps** | **List&lt;String&gt;** | The regular expressions specifying the tables. Tables matching any of them will be eligible for snapshot creation. |  [optional]
**storage** | [**Storage**](Storage.md) | The location where the snapshots should be stored. |  [optional]



