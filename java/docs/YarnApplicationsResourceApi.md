# YarnApplicationsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getYarnApplicationAttributes**](YarnApplicationsResourceApi.md#getYarnApplicationAttributes) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnApplications/attributes | Returns the list of all attributes that the Service Monitor can associate with YARN applications.
[**getYarnApplications**](YarnApplicationsResourceApi.md#getYarnApplications) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnApplications | Returns a list of applications that satisfy the filter.
[**killYarnApplication**](YarnApplicationsResourceApi.md#killYarnApplication) | **POST** /clusters/{clusterName}/services/{serviceName}/yarnApplications/{applicationId}/kill | Kills an YARN Application.


<a name="getYarnApplicationAttributes"></a>
# **getYarnApplicationAttributes**
> ApiYarnApplicationAttributeList getYarnApplicationAttributes(clusterName, serviceName)

Returns the list of all attributes that the Service Monitor can associate with YARN applications.

Returns the list of all attributes that the Service Monitor can associate with YARN applications. <p> Examples of attributes include the user who ran the application and the number of maps completed by the application. <p> These attributes can be used to search for specific YARN applications through the getYarnApplications API. For example the 'user' attribute could be used in the search 'user = root'. If the attribute is numeric it can also be used as a metric in a tsquery (ie, 'select maps_completed from YARN_APPLICATIONS'). <p> Note that this response is identical for all YARN services. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.YarnApplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

YarnApplicationsResourceApi apiInstance = new YarnApplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
try {
    ApiYarnApplicationAttributeList result = apiInstance.getYarnApplicationAttributes(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling YarnApplicationsResourceApi#getYarnApplicationAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |

### Return type

[**ApiYarnApplicationAttributeList**](ApiYarnApplicationAttributeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getYarnApplications"></a>
# **getYarnApplications**
> ApiYarnApplicationResponse getYarnApplications(clusterName, serviceName, filter, from, limit, offset, to)

Returns a list of applications that satisfy the filter.

Returns a list of applications that satisfy the filter <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.YarnApplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

YarnApplicationsResourceApi apiInstance = new YarnApplicationsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
String filter = ""; // String | A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like 'executing = true' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'application_duration > 5s and (user = root or user = myUserName').
String from = "from_example"; // String | Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time).
Integer limit = 100; // Integer | The maximum number of applications to return. Applications will be returned in the following order: <ul> <li> All executing applications, ordered from longest to shortest running </li> <li> All completed applications order by end time descending. </li> </ul>
Integer offset = 0; // Integer | The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a 'executing = false' clause to your filter.
String to = "now"; // String | End of the period to query in ISO 8601 format (defaults to now).
try {
    ApiYarnApplicationResponse result = apiInstance.getYarnApplications(clusterName, serviceName, filter, from, limit, offset, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling YarnApplicationsResourceApi#getYarnApplications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |
 **filter** | **String**| A filter to apply to the applications. A basic filter tests the value of an attribute and looks something like 'executing = true' or 'user = root'. Multiple basic filters can be combined into a complex expression using standard and / or boolean logic and parenthesis. An example of a complex filter is: 'application_duration > 5s and (user = root or user = myUserName'). | [optional] [default to ]
 **from** | **String**| Start of the period to query in ISO 8601 format (defaults to 5 minutes before the 'to' time). | [optional]
 **limit** | **Integer**| The maximum number of applications to return. Applications will be returned in the following order: <ul> <li> All executing applications, ordered from longest to shortest running </li> <li> All completed applications order by end time descending. </li> </ul> | [optional] [default to 100]
 **offset** | **Integer**| The offset to start returning applications from. This is useful for paging through lists of applications. Note that this has non-deterministic behavior if executing applications are included in the response because they can disappear from the list while paging. To exclude executing applications from the response and a 'executing = false' clause to your filter. | [optional] [default to 0]
 **to** | **String**| End of the period to query in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiYarnApplicationResponse**](ApiYarnApplicationResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="killYarnApplication"></a>
# **killYarnApplication**
> ApiYarnKillResponse killYarnApplication(applicationId, clusterName, serviceName)

Kills an YARN Application.

Kills an YARN Application <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.YarnApplicationsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

YarnApplicationsResourceApi apiInstance = new YarnApplicationsResourceApi();
String applicationId = "applicationId_example"; // String | The applicationId to kill
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
try {
    ApiYarnKillResponse result = apiInstance.killYarnApplication(applicationId, clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling YarnApplicationsResourceApi#killYarnApplication");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **applicationId** | **String**| The applicationId to kill |
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |

### Return type

[**ApiYarnKillResponse**](ApiYarnKillResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

