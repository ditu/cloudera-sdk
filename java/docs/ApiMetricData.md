
# ApiMetricData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **String** | When the metric reading was collected. |  [optional]
**value** | [**BigDecimal**](BigDecimal.md) | The value of the metric. |  [optional]



