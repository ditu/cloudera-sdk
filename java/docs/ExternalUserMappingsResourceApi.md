# ExternalUserMappingsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExternalUserMappings**](ExternalUserMappingsResourceApi.md#createExternalUserMappings) | **POST** /externalUserMappings | Creates a list of external user mappings.
[**deleteExternalUserMapping**](ExternalUserMappingsResourceApi.md#deleteExternalUserMapping) | **DELETE** /externalUserMappings/{uuid} | Deletes an external user mapping from the system.
[**readExternalUserMapping**](ExternalUserMappingsResourceApi.md#readExternalUserMapping) | **GET** /externalUserMappings/{uuid} | Returns detailed information about an external user mapping.
[**readExternalUserMappings**](ExternalUserMappingsResourceApi.md#readExternalUserMappings) | **GET** /externalUserMappings | Returns a list of the external user mappings configured in the system.
[**updateExternalUserMapping**](ExternalUserMappingsResourceApi.md#updateExternalUserMapping) | **PUT** /externalUserMappings/{uuid} | Updates the given external user mapping&#39;s information.


<a name="createExternalUserMappings"></a>
# **createExternalUserMappings**
> ApiExternalUserMappingList createExternalUserMappings(body)

Creates a list of external user mappings.

Creates a list of external user mappings

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalUserMappingsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalUserMappingsResourceApi apiInstance = new ExternalUserMappingsResourceApi();
ApiExternalUserMappingList body = new ApiExternalUserMappingList(); // ApiExternalUserMappingList | List of external user mappings to create.
try {
    ApiExternalUserMappingList result = apiInstance.createExternalUserMappings(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalUserMappingsResourceApi#createExternalUserMappings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiExternalUserMappingList**](ApiExternalUserMappingList.md)| List of external user mappings to create. | [optional]

### Return type

[**ApiExternalUserMappingList**](ApiExternalUserMappingList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteExternalUserMapping"></a>
# **deleteExternalUserMapping**
> ApiExternalUserMapping deleteExternalUserMapping(uuid)

Deletes an external user mapping from the system.

Deletes an external user mapping from the system. <p/>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalUserMappingsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalUserMappingsResourceApi apiInstance = new ExternalUserMappingsResourceApi();
String uuid = "uuid_example"; // String | The uuid of the external user mapping to delete.
try {
    ApiExternalUserMapping result = apiInstance.deleteExternalUserMapping(uuid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalUserMappingsResourceApi#deleteExternalUserMapping");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**| The uuid of the external user mapping to delete. |

### Return type

[**ApiExternalUserMapping**](ApiExternalUserMapping.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readExternalUserMapping"></a>
# **readExternalUserMapping**
> ApiExternalUserMapping readExternalUserMapping(uuid)

Returns detailed information about an external user mapping.

Returns detailed information about an external user mapping.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalUserMappingsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalUserMappingsResourceApi apiInstance = new ExternalUserMappingsResourceApi();
String uuid = "uuid_example"; // String | The external user mapping to read.
try {
    ApiExternalUserMapping result = apiInstance.readExternalUserMapping(uuid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalUserMappingsResourceApi#readExternalUserMapping");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**| The external user mapping to read. |

### Return type

[**ApiExternalUserMapping**](ApiExternalUserMapping.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readExternalUserMappings"></a>
# **readExternalUserMappings**
> ApiExternalUserMappingList readExternalUserMappings(view)

Returns a list of the external user mappings configured in the system.

Returns a list of the external user mappings configured in the system.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalUserMappingsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalUserMappingsResourceApi apiInstance = new ExternalUserMappingsResourceApi();
String view = "summary"; // String | 
try {
    ApiExternalUserMappingList result = apiInstance.readExternalUserMappings(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalUserMappingsResourceApi#readExternalUserMappings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiExternalUserMappingList**](ApiExternalUserMappingList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateExternalUserMapping"></a>
# **updateExternalUserMapping**
> ApiExternalUserMapping updateExternalUserMapping(uuid, body)

Updates the given external user mapping's information.

Updates the given external user mapping's information.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalUserMappingsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalUserMappingsResourceApi apiInstance = new ExternalUserMappingsResourceApi();
String uuid = "uuid_example"; // String | Uuid of the external user mapping being updated.
ApiExternalUserMapping body = new ApiExternalUserMapping(); // ApiExternalUserMapping | The external user mapping information.
try {
    ApiExternalUserMapping result = apiInstance.updateExternalUserMapping(uuid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalUserMappingsResourceApi#updateExternalUserMapping");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**| Uuid of the external user mapping being updated. |
 **body** | [**ApiExternalUserMapping**](ApiExternalUserMapping.md)| The external user mapping information. | [optional]

### Return type

[**ApiExternalUserMapping**](ApiExternalUserMapping.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

