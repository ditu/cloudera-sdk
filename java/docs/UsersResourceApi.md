# UsersResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUsers2**](UsersResourceApi.md#createUsers2) | **POST** /users | Creates a list of users.
[**deleteUser2**](UsersResourceApi.md#deleteUser2) | **DELETE** /users/{userName} | Deletes a user from the system.
[**getSessions**](UsersResourceApi.md#getSessions) | **GET** /users/sessions | Return a list of the sessions associated with interactive authenticated users in Cloudera Manager.
[**readUser2**](UsersResourceApi.md#readUser2) | **GET** /users/{userName} | Returns detailed information about a user.
[**readUsers2**](UsersResourceApi.md#readUsers2) | **GET** /users | Returns a list of the user names configured in the system.
[**updateUser2**](UsersResourceApi.md#updateUser2) | **PUT** /users/{userName} | Updates the given user&#39;s information.


<a name="createUsers2"></a>
# **createUsers2**
> ApiUser2List createUsers2(body)

Creates a list of users.

Creates a list of users. <p> When creating new users, the <i>password</i> property of each user should be their plain text password. The returned user information will not contain any password information. <p/>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.UsersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

UsersResourceApi apiInstance = new UsersResourceApi();
ApiUser2List body = new ApiUser2List(); // ApiUser2List | List of users to create.
try {
    ApiUser2List result = apiInstance.createUsers2(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersResourceApi#createUsers2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiUser2List**](ApiUser2List.md)| List of users to create. | [optional]

### Return type

[**ApiUser2List**](ApiUser2List.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteUser2"></a>
# **deleteUser2**
> ApiUser2 deleteUser2(userName)

Deletes a user from the system.

Deletes a user from the system. <p/>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.UsersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

UsersResourceApi apiInstance = new UsersResourceApi();
String userName = "userName_example"; // String | The name of the user to delete.
try {
    ApiUser2 result = apiInstance.deleteUser2(userName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersResourceApi#deleteUser2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userName** | **String**| The name of the user to delete. |

### Return type

[**ApiUser2**](ApiUser2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSessions"></a>
# **getSessions**
> ApiUserSessionList getSessions()

Return a list of the sessions associated with interactive authenticated users in Cloudera Manager.

Return a list of the sessions associated with interactive authenticated users in Cloudera Manager. <p> Note that these sessions are only associated with users who log into the web interface. API users will not appear.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.UsersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

UsersResourceApi apiInstance = new UsersResourceApi();
try {
    ApiUserSessionList result = apiInstance.getSessions();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersResourceApi#getSessions");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiUserSessionList**](ApiUserSessionList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readUser2"></a>
# **readUser2**
> ApiUser2 readUser2(userName)

Returns detailed information about a user.

Returns detailed information about a user.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.UsersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

UsersResourceApi apiInstance = new UsersResourceApi();
String userName = "userName_example"; // String | The user to read.
try {
    ApiUser2 result = apiInstance.readUser2(userName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersResourceApi#readUser2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userName** | **String**| The user to read. |

### Return type

[**ApiUser2**](ApiUser2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readUsers2"></a>
# **readUsers2**
> ApiUser2List readUsers2(view)

Returns a list of the user names configured in the system.

Returns a list of the user names configured in the system.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.UsersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

UsersResourceApi apiInstance = new UsersResourceApi();
String view = "summary"; // String | 
try {
    ApiUser2List result = apiInstance.readUsers2(view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersResourceApi#readUsers2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiUser2List**](ApiUser2List.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateUser2"></a>
# **updateUser2**
> ApiUser2 updateUser2(userName, body)

Updates the given user's information.

Updates the given user's information. Note that the user's name cannot be changed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.UsersResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

UsersResourceApi apiInstance = new UsersResourceApi();
String userName = "userName_example"; // String | User name being updated.
ApiUser2 body = new ApiUser2(); // ApiUser2 | The user information.
try {
    ApiUser2 result = apiInstance.updateUser2(userName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersResourceApi#updateUser2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userName** | **String**| User name being updated. |
 **body** | [**ApiUser2**](ApiUser2.md)| The user information. | [optional]

### Return type

[**ApiUser2**](ApiUser2.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

