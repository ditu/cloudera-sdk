
# ApiClusterTemplateRoleConfigGroupInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rcgRefName** | **String** |  |  [optional]
**name** | **String** |  |  [optional]



