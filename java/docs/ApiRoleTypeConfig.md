
# ApiRoleTypeConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiConfig&gt;**](ApiConfig.md) |  |  [optional]
**roleType** | **String** | The role type. |  [optional]



