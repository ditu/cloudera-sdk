
# ApiEventSeverity

## Enum


* `UNKNOWN` (value: `"UNKNOWN"`)

* `INFORMATIONAL` (value: `"INFORMATIONAL"`)

* `IMPORTANT` (value: `"IMPORTANT"`)

* `CRITICAL` (value: `"CRITICAL"`)



