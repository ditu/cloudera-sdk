
# ApiNameserviceList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiNameservice&gt;**](ApiNameservice.md) |  |  [optional]



