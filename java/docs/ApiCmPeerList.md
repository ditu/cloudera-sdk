
# ApiCmPeerList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiCmPeer&gt;**](ApiCmPeer.md) |  |  [optional]



