
# ApiMetricList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiMetric&gt;**](ApiMetric.md) |  |  [optional]



