# ExternalAccountsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAccount**](ExternalAccountsResourceApi.md#createAccount) | **POST** /externalAccounts/create | Create a new external account.
[**deleteAccount**](ExternalAccountsResourceApi.md#deleteAccount) | **DELETE** /externalAccounts/delete/{name} | Delete an external account, specifying its name.
[**externalAccountCommandByName**](ExternalAccountsResourceApi.md#externalAccountCommandByName) | **POST** /externalAccounts/account/{name}/commands/{commandName} | Executes a command on the external account specified by name.
[**getSupportedCategories**](ExternalAccountsResourceApi.md#getSupportedCategories) | **GET** /externalAccounts/supportedCategories | List of external account categories supported by this Cloudera Manager.
[**getSupportedTypes**](ExternalAccountsResourceApi.md#getSupportedTypes) | **GET** /externalAccounts/supportedTypes/{categoryName} | List of external account types supported by this Cloudera Manager by category.
[**listExternalAccountCommands**](ExternalAccountsResourceApi.md#listExternalAccountCommands) | **GET** /externalAccounts/typeInfo/{typeName}/commandsByName | Lists all the commands that can be executed by name on the provided external account type.
[**readAccount**](ExternalAccountsResourceApi.md#readAccount) | **GET** /externalAccounts/account/{name} | Get a single external account by account name.
[**readAccountByDisplayName**](ExternalAccountsResourceApi.md#readAccountByDisplayName) | **GET** /externalAccounts/accountByDisplayName/{displayName} | Get a single external account by display name.
[**readAccounts**](ExternalAccountsResourceApi.md#readAccounts) | **GET** /externalAccounts/type/{typeName} | Get a list of external accounts for a specific account type.
[**readConfig**](ExternalAccountsResourceApi.md#readConfig) | **GET** /externalAccounts/account/{name}/config | Get configs of external account for the given account name.
[**updateAccount**](ExternalAccountsResourceApi.md#updateAccount) | **PUT** /externalAccounts/update | Update an external account.
[**updateConfig**](ExternalAccountsResourceApi.md#updateConfig) | **PUT** /externalAccounts/account/{name}/config | Upadate configs of external account for the given account name.


<a name="createAccount"></a>
# **createAccount**
> ApiExternalAccount createAccount(body)

Create a new external account.

Create a new external account. Account names and display names must be unique, i.e. they must not share names or display names with an existing account. Server generates an account ID for the requested account.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
ApiExternalAccount body = new ApiExternalAccount(); // ApiExternalAccount | 
try {
    ApiExternalAccount result = apiInstance.createAccount(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#createAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiExternalAccount**](ApiExternalAccount.md)|  | [optional]

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAccount"></a>
# **deleteAccount**
> ApiExternalAccount deleteAccount(name)

Delete an external account, specifying its name.

Delete an external account, specifying its name.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String name = "name_example"; // String | 
try {
    ApiExternalAccount result = apiInstance.deleteAccount(name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#deleteAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="externalAccountCommandByName"></a>
# **externalAccountCommandByName**
> ApiCommand externalAccountCommandByName(commandName, name)

Executes a command on the external account specified by name.

Executes a command on the external account specified by name. <p> Available since API v16.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String commandName = "commandName_example"; // String | The command name.
String name = "name_example"; // String | The external account name.
try {
    ApiCommand result = apiInstance.externalAccountCommandByName(commandName, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#externalAccountCommandByName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commandName** | **String**| The command name. |
 **name** | **String**| The external account name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSupportedCategories"></a>
# **getSupportedCategories**
> ApiExternalAccountCategoryList getSupportedCategories()

List of external account categories supported by this Cloudera Manager.

List of external account categories supported by this Cloudera Manager.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
try {
    ApiExternalAccountCategoryList result = apiInstance.getSupportedCategories();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#getSupportedCategories");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiExternalAccountCategoryList**](ApiExternalAccountCategoryList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSupportedTypes"></a>
# **getSupportedTypes**
> ApiExternalAccountTypeList getSupportedTypes(categoryName)

List of external account types supported by this Cloudera Manager by category.

List of external account types supported by this Cloudera Manager by category.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String categoryName = "categoryName_example"; // String | 
try {
    ApiExternalAccountTypeList result = apiInstance.getSupportedTypes(categoryName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#getSupportedTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryName** | **String**|  |

### Return type

[**ApiExternalAccountTypeList**](ApiExternalAccountTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listExternalAccountCommands"></a>
# **listExternalAccountCommands**
> ApiCommandMetadataList listExternalAccountCommands(typeName)

Lists all the commands that can be executed by name on the provided external account type.

Lists all the commands that can be executed by name on the provided external account type. <p> Available since API v16.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String typeName = "typeName_example"; // String | The external account type name
try {
    ApiCommandMetadataList result = apiInstance.listExternalAccountCommands(typeName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#listExternalAccountCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **typeName** | **String**| The external account type name |

### Return type

[**ApiCommandMetadataList**](ApiCommandMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readAccount"></a>
# **readAccount**
> ApiExternalAccount readAccount(name, view)

Get a single external account by account name.

Get a single external account by account name.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String name = "name_example"; // String | 
String view = "view_example"; // String | 
try {
    ApiExternalAccount result = apiInstance.readAccount(name, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#readAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **view** | **String**|  | [optional] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readAccountByDisplayName"></a>
# **readAccountByDisplayName**
> ApiExternalAccount readAccountByDisplayName(displayName, view)

Get a single external account by display name.

Get a single external account by display name.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String displayName = "displayName_example"; // String | 
String view = "view_example"; // String | 
try {
    ApiExternalAccount result = apiInstance.readAccountByDisplayName(displayName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#readAccountByDisplayName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **displayName** | **String**|  |
 **view** | **String**|  | [optional] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readAccounts"></a>
# **readAccounts**
> ApiExternalAccountList readAccounts(typeName, view)

Get a list of external accounts for a specific account type.

Get a list of external accounts for a specific account type.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String typeName = "typeName_example"; // String | 
String view = "view_example"; // String | 
try {
    ApiExternalAccountList result = apiInstance.readAccounts(typeName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#readAccounts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **typeName** | **String**|  |
 **view** | **String**|  | [optional] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiExternalAccountList**](ApiExternalAccountList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readConfig"></a>
# **readConfig**
> ApiConfigList readConfig(name, view)

Get configs of external account for the given account name.

Get configs of external account for the given account name.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String name = "name_example"; // String | The external account name
String view = "summary"; // String | The view to materialize, either \"summary\" or \"full\".
try {
    ApiConfigList result = apiInstance.readConfig(name, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#readConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The external account name |
 **view** | **String**| The view to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateAccount"></a>
# **updateAccount**
> ApiExternalAccount updateAccount(body)

Update an external account.

Update an external account.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
ApiExternalAccount body = new ApiExternalAccount(); // ApiExternalAccount | 
try {
    ApiExternalAccount result = apiInstance.updateAccount(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#updateAccount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiExternalAccount**](ApiExternalAccount.md)|  | [optional]

### Return type

[**ApiExternalAccount**](ApiExternalAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateConfig"></a>
# **updateConfig**
> ApiConfigList updateConfig(name, message, body)

Upadate configs of external account for the given account name.

Upadate configs of external account for the given account name.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ExternalAccountsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ExternalAccountsResourceApi apiInstance = new ExternalAccountsResourceApi();
String name = "name_example"; // String | The external account name
String message = "message_example"; // String | Optional message describing the changes.
ApiConfigList body = new ApiConfigList(); // ApiConfigList | Settings to update.
try {
    ApiConfigList result = apiInstance.updateConfig(name, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExternalAccountsResourceApi#updateConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The external account name |
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiConfigList**](ApiConfigList.md)| Settings to update. | [optional]

### Return type

[**ApiConfigList**](ApiConfigList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

