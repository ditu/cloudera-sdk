# CommandsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**abortCommand**](CommandsResourceApi.md#abortCommand) | **POST** /commands/{commandId}/abort | Abort a running command.
[**readCommand**](CommandsResourceApi.md#readCommand) | **GET** /commands/{commandId} | Retrieve detailed information on an asynchronous command.
[**retry**](CommandsResourceApi.md#retry) | **POST** /commands/{commandId}/retry | Try to rerun a command.


<a name="abortCommand"></a>
# **abortCommand**
> ApiCommand abortCommand(commandId)

Abort a running command.

Abort a running command.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CommandsResourceApi apiInstance = new CommandsResourceApi();
BigDecimal commandId = new BigDecimal(); // BigDecimal | The command id.
try {
    ApiCommand result = apiInstance.abortCommand(commandId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CommandsResourceApi#abortCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commandId** | **BigDecimal**| The command id. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readCommand"></a>
# **readCommand**
> ApiCommand readCommand(commandId)

Retrieve detailed information on an asynchronous command.

Retrieve detailed information on an asynchronous command.  <p>Cloudera Manager keeps the results and statuses of asynchronous commands, which have non-negative command IDs. On the other hand, synchronous commands complete immediately, and their results are passed back in the return object of the command execution API call. Outside of that return object, there is no way to check the result of a synchronous command.</p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CommandsResourceApi apiInstance = new CommandsResourceApi();
BigDecimal commandId = new BigDecimal(); // BigDecimal | The command id.
try {
    ApiCommand result = apiInstance.readCommand(commandId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CommandsResourceApi#readCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commandId** | **BigDecimal**| The command id. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="retry"></a>
# **retry**
> ApiCommand retry(commandId)

Try to rerun a command.

Try to rerun a command.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.CommandsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

CommandsResourceApi apiInstance = new CommandsResourceApi();
BigDecimal commandId = new BigDecimal(); // BigDecimal | ID of the command that needs to be run.
try {
    ApiCommand result = apiInstance.retry(commandId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CommandsResourceApi#retry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commandId** | **BigDecimal**| ID of the command that needs to be run. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

