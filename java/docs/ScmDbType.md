
# ScmDbType

## Enum


* `MYSQL` (value: `"MYSQL"`)

* `POSTGRESQL` (value: `"POSTGRESQL"`)

* `HSQL` (value: `"HSQL"`)

* `ORACLE` (value: `"ORACLE"`)

* `DERBY` (value: `"DERBY"`)

* `SQLITE3` (value: `"SQLITE3"`)



