
# ApiYarnUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avgCpuUtilization** | [**BigDecimal**](BigDecimal.md) | Average number of VCores used by YARN applications during the report window. |  [optional]
**maxCpuUtilization** | [**BigDecimal**](BigDecimal.md) | Maximum number of VCores used by YARN applications during the report window. |  [optional]
**avgCpuDailyPeak** | [**BigDecimal**](BigDecimal.md) | Average daily peak VCores used by YARN applications during the report window. The number is computed by first finding the maximum resource consumption per day and then taking their mean. |  [optional]
**maxCpuUtilizationTimestampMs** | [**BigDecimal**](BigDecimal.md) | Timestamp corresponds to maximum number of VCores used by YARN applications during the report window. |  [optional]
**avgCpuUtilizationPercentage** | [**BigDecimal**](BigDecimal.md) | Average percentage of VCores used by YARN applications during the report window. |  [optional]
**maxCpuUtilizationPercentage** | [**BigDecimal**](BigDecimal.md) | Maximum percentage of VCores used by YARN applications during the report window. |  [optional]
**avgCpuDailyPeakPercentage** | [**BigDecimal**](BigDecimal.md) | Average daily peak percentage of VCores used by YARN applications during the report window. |  [optional]
**avgMemoryUtilization** | [**BigDecimal**](BigDecimal.md) | Average memory used by YARN applications during the report window. |  [optional]
**maxMemoryUtilization** | [**BigDecimal**](BigDecimal.md) | Maximum memory used by YARN applications during the report window. |  [optional]
**avgMemoryDailyPeak** | [**BigDecimal**](BigDecimal.md) | Average daily peak memory used by YARN applications during the report window. The number is computed by first finding the maximum resource consumption per day and then taking their mean. |  [optional]
**maxMemoryUtilizationTimestampMs** | [**BigDecimal**](BigDecimal.md) | Timestamp corresponds to maximum memory used by YARN applications during the report window. |  [optional]
**avgMemoryUtilizationPercentage** | [**BigDecimal**](BigDecimal.md) | Average percentage memory used by YARN applications during the report window. |  [optional]
**maxMemoryUtilizationPercentage** | [**BigDecimal**](BigDecimal.md) | Maximum percentage of memory used by YARN applications during the report window. |  [optional]
**avgMemoryDailyPeakPercentage** | [**BigDecimal**](BigDecimal.md) | Average daily peak percentage of memory used by YARN applications during the report window. |  [optional]
**tenantUtilizations** | [**ApiYarnTenantUtilizationList**](ApiYarnTenantUtilizationList.md) | A list of tenant utilization reports. |  [optional]
**errorMessage** | **String** | error message of utilization report. |  [optional]



