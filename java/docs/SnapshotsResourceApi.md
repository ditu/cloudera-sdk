# SnapshotsResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPolicies**](SnapshotsResourceApi.md#createPolicies) | **POST** /clusters/{clusterName}/services/{serviceName}/snapshots/policies | Creates one or more snapshot policies.
[**deletePolicy**](SnapshotsResourceApi.md#deletePolicy) | **DELETE** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Deletes an existing snapshot policy.
[**readHistory**](SnapshotsResourceApi.md#readHistory) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName}/history | Returns a list of commands triggered by a snapshot policy.
[**readPolicies**](SnapshotsResourceApi.md#readPolicies) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies | Returns information for all snapshot policies.
[**readPolicy**](SnapshotsResourceApi.md#readPolicy) | **GET** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Returns information for a specific snapshot policy.
[**updatePolicy**](SnapshotsResourceApi.md#updatePolicy) | **PUT** /clusters/{clusterName}/services/{serviceName}/snapshots/policies/{policyName} | Updates an existing snapshot policy.


<a name="createPolicies"></a>
# **createPolicies**
> ApiSnapshotPolicyList createPolicies(clusterName, serviceName, body)

Creates one or more snapshot policies.

Creates one or more snapshot policies.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.SnapshotsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

SnapshotsResourceApi apiInstance = new SnapshotsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiSnapshotPolicyList body = new ApiSnapshotPolicyList(); // ApiSnapshotPolicyList | List of the snapshot policies to create.
try {
    ApiSnapshotPolicyList result = apiInstance.createPolicies(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SnapshotsResourceApi#createPolicies");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiSnapshotPolicyList**](ApiSnapshotPolicyList.md)| List of the snapshot policies to create. | [optional]

### Return type

[**ApiSnapshotPolicyList**](ApiSnapshotPolicyList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deletePolicy"></a>
# **deletePolicy**
> ApiSnapshotPolicy deletePolicy(clusterName, policyName, serviceName)

Deletes an existing snapshot policy.

Deletes an existing snapshot policy.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.SnapshotsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

SnapshotsResourceApi apiInstance = new SnapshotsResourceApi();
String clusterName = "clusterName_example"; // String | 
String policyName = "policyName_example"; // String | Name of an existing snapshot policy.
String serviceName = "serviceName_example"; // String | 
try {
    ApiSnapshotPolicy result = apiInstance.deletePolicy(clusterName, policyName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SnapshotsResourceApi#deletePolicy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **policyName** | **String**| Name of an existing snapshot policy. |
 **serviceName** | **String**|  |

### Return type

[**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readHistory"></a>
# **readHistory**
> ApiSnapshotCommandList readHistory(clusterName, policyName, serviceName, limit, offset, view)

Returns a list of commands triggered by a snapshot policy.

Returns a list of commands triggered by a snapshot policy.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.SnapshotsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

SnapshotsResourceApi apiInstance = new SnapshotsResourceApi();
String clusterName = "clusterName_example"; // String | 
String policyName = "policyName_example"; // String | Name of an existing snapshot policy.
String serviceName = "serviceName_example"; // String | 
Integer limit = 20; // Integer | Maximum number of commands to retrieve.
Integer offset = 0; // Integer | Index of first command to retrieve.
String view = "summary"; // String | The view to materialize.
try {
    ApiSnapshotCommandList result = apiInstance.readHistory(clusterName, policyName, serviceName, limit, offset, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SnapshotsResourceApi#readHistory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **policyName** | **String**| Name of an existing snapshot policy. |
 **serviceName** | **String**|  |
 **limit** | **Integer**| Maximum number of commands to retrieve. | [optional] [default to 20]
 **offset** | **Integer**| Index of first command to retrieve. | [optional] [default to 0]
 **view** | **String**| The view to materialize. | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiSnapshotCommandList**](ApiSnapshotCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readPolicies"></a>
# **readPolicies**
> ApiSnapshotPolicyList readPolicies(clusterName, serviceName, view)

Returns information for all snapshot policies.

Returns information for all snapshot policies.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.SnapshotsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

SnapshotsResourceApi apiInstance = new SnapshotsResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
String view = "summary"; // String | The view to materialize.
try {
    ApiSnapshotPolicyList result = apiInstance.readPolicies(clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SnapshotsResourceApi#readPolicies");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **view** | **String**| The view to materialize. | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiSnapshotPolicyList**](ApiSnapshotPolicyList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readPolicy"></a>
# **readPolicy**
> ApiSnapshotPolicy readPolicy(clusterName, policyName, serviceName, view)

Returns information for a specific snapshot policy.

Returns information for a specific snapshot policy.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.SnapshotsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

SnapshotsResourceApi apiInstance = new SnapshotsResourceApi();
String clusterName = "clusterName_example"; // String | 
String policyName = "policyName_example"; // String | Name of an existing snapshot policy.
String serviceName = "serviceName_example"; // String | 
String view = "summary"; // String | The view to materialize.
try {
    ApiSnapshotPolicy result = apiInstance.readPolicy(clusterName, policyName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SnapshotsResourceApi#readPolicy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **policyName** | **String**| Name of an existing snapshot policy. |
 **serviceName** | **String**|  |
 **view** | **String**| The view to materialize. | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updatePolicy"></a>
# **updatePolicy**
> ApiSnapshotPolicy updatePolicy(clusterName, policyName, serviceName, body)

Updates an existing snapshot policy.

Updates an existing snapshot policy.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.SnapshotsResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

SnapshotsResourceApi apiInstance = new SnapshotsResourceApi();
String clusterName = "clusterName_example"; // String | 
String policyName = "policyName_example"; // String | Name of an existing snapshot policy.
String serviceName = "serviceName_example"; // String | 
ApiSnapshotPolicy body = new ApiSnapshotPolicy(); // ApiSnapshotPolicy | Modified policy.
try {
    ApiSnapshotPolicy result = apiInstance.updatePolicy(clusterName, policyName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SnapshotsResourceApi#updatePolicy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **policyName** | **String**| Name of an existing snapshot policy. |
 **serviceName** | **String**|  |
 **body** | [**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)| Modified policy. | [optional]

### Return type

[**ApiSnapshotPolicy**](ApiSnapshotPolicy.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

