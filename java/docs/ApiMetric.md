
# ApiMetric

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the metric. |  [optional]
**context** | **String** | Context the metric is associated with. |  [optional]
**unit** | **String** | Unit of the metric values. |  [optional]
**data** | [**List&lt;ApiMetricData&gt;**](ApiMetricData.md) | List of readings retrieved from the monitors. |  [optional]
**displayName** | **String** | Requires \&quot;full\&quot; view. User-friendly display name for the metric. |  [optional]
**description** | **String** | Requires \&quot;full\&quot; view. Description of the metric. |  [optional]



