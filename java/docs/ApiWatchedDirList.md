
# ApiWatchedDirList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiWatchedDir&gt;**](ApiWatchedDir.md) |  |  [optional]



