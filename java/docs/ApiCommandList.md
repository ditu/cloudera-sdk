
# ApiCommandList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;ApiCommand&gt;**](ApiCommand.md) |  |  [optional]



