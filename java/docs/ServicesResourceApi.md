# ServicesResourceApi

All URIs are relative to *https://localhost/api/v31*

Method | HTTP request | Description
------------- | ------------- | -------------
[**collectYarnApplicationDiagnostics**](ServicesResourceApi.md#collectYarnApplicationDiagnostics) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnApplicationDiagnosticsCollection | Collect the Diagnostics data for Yarn applications.
[**createBeeswaxWarehouseCommand**](ServicesResourceApi.md#createBeeswaxWarehouseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueCreateHiveWarehouse | Create the Beeswax role&#39;s Hive warehouse directory, on Hue services.
[**createHBaseRootCommand**](ServicesResourceApi.md#createHBaseRootCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hbaseCreateRoot | Creates the root directory of an HBase service.
[**createHiveUserDirCommand**](ServicesResourceApi.md#createHiveUserDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateHiveUserDir | Create the Hive user directory.
[**createHiveWarehouseCommand**](ServicesResourceApi.md#createHiveWarehouseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateHiveWarehouse | Create the Hive warehouse directory, on Hive services.
[**createImpalaUserDirCommand**](ServicesResourceApi.md#createImpalaUserDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateUserDir | Create the Impala user directory.
[**createOozieDb**](ServicesResourceApi.md#createOozieDb) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createOozieDb | Creates the Oozie Database Schema in the configured database.
[**createServices**](ServicesResourceApi.md#createServices) | **POST** /clusters/{clusterName}/services | Creates a list of services.
[**createSolrHdfsHomeDirCommand**](ServicesResourceApi.md#createSolrHdfsHomeDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createSolrHdfsHomeDir | Creates the home directory of a Solr service in HDFS.
[**createSqoopUserDirCommand**](ServicesResourceApi.md#createSqoopUserDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/createSqoopUserDir | Creates the user directory of a Sqoop service in HDFS.
[**createYarnCmContainerUsageInputDirCommand**](ServicesResourceApi.md#createYarnCmContainerUsageInputDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnCreateCmContainerUsageInputDirCommand | Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics.
[**createYarnJobHistoryDirCommand**](ServicesResourceApi.md#createYarnJobHistoryDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnCreateJobHistoryDirCommand | Create the Yarn job history directory.
[**createYarnNodeManagerRemoteAppLogDirCommand**](ServicesResourceApi.md#createYarnNodeManagerRemoteAppLogDirCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnNodeManagerRemoteAppLogDirCommand | Create the Yarn NodeManager remote application log directory.
[**decommissionCommand**](ServicesResourceApi.md#decommissionCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/decommission | Decommission roles of a service.
[**deleteService**](ServicesResourceApi.md#deleteService) | **DELETE** /clusters/{clusterName}/services/{serviceName} | Deletes a service from the system.
[**deployClientConfigCommand**](ServicesResourceApi.md#deployClientConfigCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/deployClientConfig | Deploy a service&#39;s client configuration.
[**disableJtHaCommand**](ServicesResourceApi.md#disableJtHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableJtHa | Disable high availability (HA) for JobTracker.
[**disableLlamaHaCommand**](ServicesResourceApi.md#disableLlamaHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaDisableLlamaHa | Not Supported.
[**disableLlamaRmCommand**](ServicesResourceApi.md#disableLlamaRmCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaDisableLlamaRm | Not Supported.
[**disableOozieHaCommand**](ServicesResourceApi.md#disableOozieHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieDisableHa | Disable high availability (HA) for Oozie.
[**disableRmHaCommand**](ServicesResourceApi.md#disableRmHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableRmHa | Disable high availability (HA) for ResourceManager.
[**disableSentryHaCommand**](ServicesResourceApi.md#disableSentryHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/disableSentryHa | Disable high availability (HA) for Sentry service.
[**enableJtHaCommand**](ServicesResourceApi.md#enableJtHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableJtHa | Enable high availability (HA) for a JobTracker.
[**enableLlamaHaCommand**](ServicesResourceApi.md#enableLlamaHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaEnableLlamaHa | Not Supported.
[**enableLlamaRmCommand**](ServicesResourceApi.md#enableLlamaRmCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaEnableLlamaRm | Not Supported.
[**enableOozieHaCommand**](ServicesResourceApi.md#enableOozieHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieEnableHa | Enable high availability (HA) for Oozie service.
[**enableRmHaCommand**](ServicesResourceApi.md#enableRmHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableRmHa | Enable high availability (HA) for a YARN ResourceManager.
[**enableSentryHaCommand**](ServicesResourceApi.md#enableSentryHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enableSentryHa | Enable high availability (HA) for Sentry service.
[**enterMaintenanceMode**](ServicesResourceApi.md#enterMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/enterMaintenanceMode | Put the service into maintenance mode.
[**exitMaintenanceMode**](ServicesResourceApi.md#exitMaintenanceMode) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/exitMaintenanceMode | Take the service out of maintenance mode.
[**firstRun**](ServicesResourceApi.md#firstRun) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/firstRun | Prepare and start a service.
[**getClientConfig**](ServicesResourceApi.md#getClientConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/clientConfig | Download a zip-compressed archive of the client configuration, of a specific service.
[**getHdfsUsageReport**](ServicesResourceApi.md#getHdfsUsageReport) | **GET** /clusters/{clusterName}/services/{serviceName}/reports/hdfsUsageReport | Fetch the HDFS usage report.
[**getImpalaUtilization**](ServicesResourceApi.md#getImpalaUtilization) | **GET** /clusters/{clusterName}/services/{serviceName}/impalaUtilization | Provides the resource utilization of the Impala service as well as the resource utilization per tenant.
[**getMetrics**](ServicesResourceApi.md#getMetrics) | **GET** /clusters/{clusterName}/services/{serviceName}/metrics | Fetch metric readings for a particular service.
[**getMrUsageReport**](ServicesResourceApi.md#getMrUsageReport) | **GET** /clusters/{clusterName}/services/{serviceName}/reports/mrUsageReport | Fetch the MR usage report.
[**getYarnUtilization**](ServicesResourceApi.md#getYarnUtilization) | **GET** /clusters/{clusterName}/services/{serviceName}/yarnUtilization | Provides the resource utilization of the yarn service as well as the resource utilization per tenant.
[**hbaseUpgradeCommand**](ServicesResourceApi.md#hbaseUpgradeCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hbaseUpgrade | Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5.
[**hdfsCreateTmpDir**](ServicesResourceApi.md#hdfsCreateTmpDir) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsCreateTmpDir | Creates a tmp directory on the HDFS filesystem.
[**hdfsDisableAutoFailoverCommand**](ServicesResourceApi.md#hdfsDisableAutoFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableAutoFailover | Disable auto-failover for a highly available HDFS nameservice.
[**hdfsDisableHaCommand**](ServicesResourceApi.md#hdfsDisableHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableHa | Disable high availability (HA) for an HDFS NameNode.
[**hdfsDisableNnHaCommand**](ServicesResourceApi.md#hdfsDisableNnHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsDisableNnHa | Disable High Availability (HA) with Automatic Failover for an HDFS NameNode.
[**hdfsEnableAutoFailoverCommand**](ServicesResourceApi.md#hdfsEnableAutoFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableAutoFailover | Enable auto-failover for an HDFS nameservice.
[**hdfsEnableHaCommand**](ServicesResourceApi.md#hdfsEnableHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableHa | Enable high availability (HA) for an HDFS NameNode.
[**hdfsEnableNnHaCommand**](ServicesResourceApi.md#hdfsEnableNnHaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsEnableNnHa | Enable High Availability (HA) with Automatic Failover for an HDFS NameNode.
[**hdfsFailoverCommand**](ServicesResourceApi.md#hdfsFailoverCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsFailover | Initiate a failover in an HDFS HA NameNode pair.
[**hdfsFinalizeRollingUpgrade**](ServicesResourceApi.md#hdfsFinalizeRollingUpgrade) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsFinalizeRollingUpgrade | Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version.
[**hdfsRollEditsCommand**](ServicesResourceApi.md#hdfsRollEditsCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsRollEdits | Roll the edits of an HDFS NameNode or Nameservice.
[**hdfsUpgradeMetadataCommand**](ServicesResourceApi.md#hdfsUpgradeMetadataCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hdfsUpgradeMetadata | Upgrade HDFS Metadata as part of a major version upgrade.
[**hiveCreateMetastoreDatabaseCommand**](ServicesResourceApi.md#hiveCreateMetastoreDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateMetastoreDatabase | Create the Hive Metastore Database.
[**hiveCreateMetastoreDatabaseTablesCommand**](ServicesResourceApi.md#hiveCreateMetastoreDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveCreateMetastoreDatabaseTables | Create the Hive Metastore Database tables.
[**hiveUpdateMetastoreNamenodesCommand**](ServicesResourceApi.md#hiveUpdateMetastoreNamenodesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveUpdateMetastoreNamenodes | Update Hive Metastore to point to a NameNode&#39;s Nameservice name instead of hostname.
[**hiveUpgradeMetastoreCommand**](ServicesResourceApi.md#hiveUpgradeMetastoreCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveUpgradeMetastore | Upgrade Hive Metastore as part of a major version upgrade.
[**hiveValidateMetastoreSchemaCommand**](ServicesResourceApi.md#hiveValidateMetastoreSchemaCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hiveValidateMetastoreSchema | Validate the Hive Metastore Schema.
[**hueDumpDbCommand**](ServicesResourceApi.md#hueDumpDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueDumpDb | Runs Hue&#39;s dumpdata command.
[**hueLoadDbCommand**](ServicesResourceApi.md#hueLoadDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueLoadDb | Runs Hue&#39;s loaddata command.
[**hueSyncDbCommand**](ServicesResourceApi.md#hueSyncDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/hueSyncDb | Runs Hue&#39;s syncdb command.
[**impalaCreateCatalogDatabaseCommand**](ServicesResourceApi.md#impalaCreateCatalogDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateCatalogDatabase | .
[**impalaCreateCatalogDatabaseTablesCommand**](ServicesResourceApi.md#impalaCreateCatalogDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/impalaCreateCatalogDatabaseTables | .
[**importMrConfigsIntoYarn**](ServicesResourceApi.md#importMrConfigsIntoYarn) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/importMrConfigsIntoYarn | Import MapReduce configuration into Yarn, overwriting Yarn configuration.
[**initSolrCommand**](ServicesResourceApi.md#initSolrCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/initSolr | Initializes the Solr service in Zookeeper.
[**installMrFrameworkJars**](ServicesResourceApi.md#installMrFrameworkJars) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/installMrFrameworkJars | Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it.
[**installOozieShareLib**](ServicesResourceApi.md#installOozieShareLib) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/installOozieShareLib | Creates directory for Oozie user in HDFS and installs the ShareLib in it.
[**ksMigrateToSentry**](ServicesResourceApi.md#ksMigrateToSentry) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/migrateToSentry | Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer.
[**listActiveCommands**](ServicesResourceApi.md#listActiveCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/commands | List active service commands.
[**listRoleTypes**](ServicesResourceApi.md#listRoleTypes) | **GET** /clusters/{clusterName}/services/{serviceName}/roleTypes | List the supported role types for a service.
[**listServiceCommands**](ServicesResourceApi.md#listServiceCommands) | **GET** /clusters/{clusterName}/services/{serviceName}/commandsByName | Lists all the commands that can be executed by name on the provided service.
[**offlineCommand**](ServicesResourceApi.md#offlineCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/offline | Offline roles of a service.
[**oozieCreateEmbeddedDatabaseCommand**](ServicesResourceApi.md#oozieCreateEmbeddedDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieCreateEmbeddedDatabase | Create the Oozie Server Database.
[**oozieDumpDatabaseCommand**](ServicesResourceApi.md#oozieDumpDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieDumpDatabase | Dump the Oozie Server Database.
[**oozieLoadDatabaseCommand**](ServicesResourceApi.md#oozieLoadDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieLoadDatabase | Load the Oozie Server Database from dump.
[**oozieUpgradeDbCommand**](ServicesResourceApi.md#oozieUpgradeDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/oozieUpgradeDb | Upgrade Oozie Database schema as part of a major version upgrade.
[**readService**](ServicesResourceApi.md#readService) | **GET** /clusters/{clusterName}/services/{serviceName} | Retrieves details information about a service.
[**readServiceConfig**](ServicesResourceApi.md#readServiceConfig) | **GET** /clusters/{clusterName}/services/{serviceName}/config | Retrieves the configuration of a specific service.
[**readServices**](ServicesResourceApi.md#readServices) | **GET** /clusters/{clusterName}/services | Lists all services registered in the cluster.
[**recommissionCommand**](ServicesResourceApi.md#recommissionCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/recommission | Recommission roles of a service.
[**recommissionWithStartCommand**](ServicesResourceApi.md#recommissionWithStartCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/recommissionWithStart | Start and recommission roles of a service.
[**restartCommand**](ServicesResourceApi.md#restartCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/restart | Restart the service.
[**rollingRestart**](ServicesResourceApi.md#rollingRestart) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/rollingRestart | Command to run rolling restart of roles in a service.
[**sentryCreateDatabaseCommand**](ServicesResourceApi.md#sentryCreateDatabaseCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryCreateDatabase | Create the Sentry Server Database.
[**sentryCreateDatabaseTablesCommand**](ServicesResourceApi.md#sentryCreateDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryCreateDatabaseTables | Create the Sentry Server Database tables.
[**sentryUpgradeDatabaseTablesCommand**](ServicesResourceApi.md#sentryUpgradeDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sentryUpgradeDatabaseTables | Upgrade the Sentry Server Database tables.
[**serviceCommandByName**](ServicesResourceApi.md#serviceCommandByName) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/{commandName} | Executes a command on the service specified by name.
[**solrBootstrapCollectionsCommand**](ServicesResourceApi.md#solrBootstrapCollectionsCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrBootstrapCollections | Bootstraps Solr Collections after the CDH upgrade.
[**solrBootstrapConfigCommand**](ServicesResourceApi.md#solrBootstrapConfigCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrBootstrapConfig | Bootstraps Solr config during the CDH upgrade.
[**solrConfigBackupCommand**](ServicesResourceApi.md#solrConfigBackupCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrConfigBackup | Backs up Solr configuration metadata before CDH upgrade.
[**solrMigrateSentryPrivilegesCommand**](ServicesResourceApi.md#solrMigrateSentryPrivilegesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrMigrateSentryPrivilegesCommand | Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service.
[**solrReinitializeStateForUpgradeCommand**](ServicesResourceApi.md#solrReinitializeStateForUpgradeCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrReinitializeStateForUpgrade | Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state.
[**solrValidateMetadataCommand**](ServicesResourceApi.md#solrValidateMetadataCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/solrValidateMetadata | Validates Solr metadata and configurations.
[**sqoopCreateDatabaseTablesCommand**](ServicesResourceApi.md#sqoopCreateDatabaseTablesCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sqoopCreateDatabaseTables | Create the Sqoop2 Server Database tables.
[**sqoopUpgradeDbCommand**](ServicesResourceApi.md#sqoopUpgradeDbCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/sqoopUpgradeDb | Upgrade Sqoop Database schema as part of a major version upgrade.
[**startCommand**](ServicesResourceApi.md#startCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/start | Start the service.
[**stopCommand**](ServicesResourceApi.md#stopCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/stop | Stop the service.
[**switchToMr2**](ServicesResourceApi.md#switchToMr2) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/switchToMr2 | Change the cluster to use MR2 instead of MR1.
[**updateService**](ServicesResourceApi.md#updateService) | **PUT** /clusters/{clusterName}/services/{serviceName} | Updates service information.
[**updateServiceConfig**](ServicesResourceApi.md#updateServiceConfig) | **PUT** /clusters/{clusterName}/services/{serviceName}/config | Updates the service configuration with the given values.
[**yarnFormatStateStore**](ServicesResourceApi.md#yarnFormatStateStore) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/yarnFormatStateStore | Formats the state store in ZooKeeper used for Resource Manager High Availability.
[**zooKeeperCleanupCommand**](ServicesResourceApi.md#zooKeeperCleanupCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/zooKeeperCleanup | Clean up all running server instances of a ZooKeeper service.
[**zooKeeperInitCommand**](ServicesResourceApi.md#zooKeeperInitCommand) | **POST** /clusters/{clusterName}/services/{serviceName}/commands/zooKeeperInit | Initializes all the server instances of a ZooKeeper service.


<a name="collectYarnApplicationDiagnostics"></a>
# **collectYarnApplicationDiagnostics**
> ApiCommand collectYarnApplicationDiagnostics(clusterName, serviceName, body)

Collect the Diagnostics data for Yarn applications.

Collect the Diagnostics data for Yarn applications <p> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the YARN service on which to run the command.
ApiYarnApplicationDiagnosticsCollectionArgs body = new ApiYarnApplicationDiagnosticsCollectionArgs(); // ApiYarnApplicationDiagnosticsCollectionArgs | Arguments used for collecting diagnostics data for Yarn applications
try {
    ApiCommand result = apiInstance.collectYarnApplicationDiagnostics(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#collectYarnApplicationDiagnostics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the YARN service on which to run the command. |
 **body** | [**ApiYarnApplicationDiagnosticsCollectionArgs**](ApiYarnApplicationDiagnosticsCollectionArgs.md)| Arguments used for collecting diagnostics data for Yarn applications | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createBeeswaxWarehouseCommand"></a>
# **createBeeswaxWarehouseCommand**
> ApiCommand createBeeswaxWarehouseCommand(clusterName, serviceName)

Create the Beeswax role's Hive warehouse directory, on Hue services.

Create the Beeswax role's Hive warehouse directory, on Hue services.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Hue service name.
try {
    ApiCommand result = apiInstance.createBeeswaxWarehouseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createBeeswaxWarehouseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Hue service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createHBaseRootCommand"></a>
# **createHBaseRootCommand**
> ApiCommand createHBaseRootCommand(clusterName, serviceName)

Creates the root directory of an HBase service.

Creates the root directory of an HBase service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HBase service name.
try {
    ApiCommand result = apiInstance.createHBaseRootCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createHBaseRootCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HBase service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createHiveUserDirCommand"></a>
# **createHiveUserDirCommand**
> ApiCommand createHiveUserDirCommand(clusterName, serviceName)

Create the Hive user directory.

Create the Hive user directory <p> Available since API v4. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Hive service name.
try {
    ApiCommand result = apiInstance.createHiveUserDirCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createHiveUserDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Hive service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createHiveWarehouseCommand"></a>
# **createHiveWarehouseCommand**
> ApiCommand createHiveWarehouseCommand(clusterName, serviceName)

Create the Hive warehouse directory, on Hive services.

Create the Hive warehouse directory, on Hive services. <p> Available since API v3. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Hive service name.
try {
    ApiCommand result = apiInstance.createHiveWarehouseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createHiveWarehouseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Hive service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createImpalaUserDirCommand"></a>
# **createImpalaUserDirCommand**
> ApiCommand createImpalaUserDirCommand(clusterName, serviceName)

Create the Impala user directory.

Create the Impala user directory <p> Available since API v6. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Impala service name.
try {
    ApiCommand result = apiInstance.createImpalaUserDirCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createImpalaUserDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Impala service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createOozieDb"></a>
# **createOozieDb**
> ApiCommand createOozieDb(clusterName, serviceName)

Creates the Oozie Database Schema in the configured database.

Creates the Oozie Database Schema in the configured database. This command does not create database. This command creates only tables required by Oozie. To create database, please refer to oozieCreateEmbeddedDatabase()  <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Oozie service on which to run the command.
try {
    ApiCommand result = apiInstance.createOozieDb(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createOozieDb");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Oozie service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createServices"></a>
# **createServices**
> ApiServiceList createServices(clusterName, body)

Creates a list of services.

Creates a list of services. <p> There are typically two service creation strategies: <ol> <li> The caller may choose to set up a new service piecemeal, by first creating the service itself (without any roles or configuration), and then create the roles, and then specify configuration. </li> <li> Alternatively, the caller can pack all the information in one call, by fully specifying the fields in the com.cloudera.api.model.ApiService object, with <ul> <li>service config and role type config, and</li> <li>role to host assignment.</li> </ul> </li> </ol>  <table> <thead> <tr> <th>Cluster Version</th> <th>Available Service Types</th> </tr> </thead> <tbody> <tr> <td>CDH4</td> <td>HDFS, MAPREDUCE, HBASE, OOZIE, ZOOKEEPER, HUE, YARN, IMPALA, FLUME, HIVE, SOLR, SQOOP, KS_INDEXER</td> </tr> <tr> <td>CDH5</td> <td>HDFS, MAPREDUCE, HBASE, OOZIE, ZOOKEEPER, HUE, YARN, IMPALA, FLUME, HIVE, SOLR, SQOOP, KS_INDEXER, SQOOP_CLIENT, SENTRY, ACCUMULO16, KMS, SPARK_ON_YARN, KAFKA </td> </tr> </tbody> </table>  As of V6, GET /{clusterName}/serviceTypes should be used to get the service types available to the cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
ApiServiceList body = new ApiServiceList(); // ApiServiceList | Details of the services to create.
try {
    ApiServiceList result = apiInstance.createServices(clusterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createServices");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **body** | [**ApiServiceList**](ApiServiceList.md)| Details of the services to create. | [optional]

### Return type

[**ApiServiceList**](ApiServiceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createSolrHdfsHomeDirCommand"></a>
# **createSolrHdfsHomeDirCommand**
> ApiCommand createSolrHdfsHomeDirCommand(clusterName, serviceName)

Creates the home directory of a Solr service in HDFS.

Creates the home directory of a Solr service in HDFS.  <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Solr service name.
try {
    ApiCommand result = apiInstance.createSolrHdfsHomeDirCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createSolrHdfsHomeDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createSqoopUserDirCommand"></a>
# **createSqoopUserDirCommand**
> ApiCommand createSqoopUserDirCommand(clusterName, serviceName)

Creates the user directory of a Sqoop service in HDFS.

Creates the user directory of a Sqoop service in HDFS.  <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Sqoop service name.
try {
    ApiCommand result = apiInstance.createSqoopUserDirCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createSqoopUserDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Sqoop service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createYarnCmContainerUsageInputDirCommand"></a>
# **createYarnCmContainerUsageInputDirCommand**
> ApiCommand createYarnCmContainerUsageInputDirCommand(clusterName, serviceName)

Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics.

Creates the HDFS directory where YARN container usage metrics are stored by NodeManagers for CM to read and aggregate into app usage metrics. <p> Available since API v13. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The YARN service name.
try {
    ApiCommand result = apiInstance.createYarnCmContainerUsageInputDirCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createYarnCmContainerUsageInputDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The YARN service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createYarnJobHistoryDirCommand"></a>
# **createYarnJobHistoryDirCommand**
> ApiCommand createYarnJobHistoryDirCommand(clusterName, serviceName)

Create the Yarn job history directory.

Create the Yarn job history directory <p> Available since API v6. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The YARN service name.
try {
    ApiCommand result = apiInstance.createYarnJobHistoryDirCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createYarnJobHistoryDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The YARN service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createYarnNodeManagerRemoteAppLogDirCommand"></a>
# **createYarnNodeManagerRemoteAppLogDirCommand**
> ApiCommand createYarnNodeManagerRemoteAppLogDirCommand(clusterName, serviceName)

Create the Yarn NodeManager remote application log directory.

Create the Yarn NodeManager remote application log directory <p> Available since API v6. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The YARN service name.
try {
    ApiCommand result = apiInstance.createYarnNodeManagerRemoteAppLogDirCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#createYarnNodeManagerRemoteAppLogDirCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The YARN service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="decommissionCommand"></a>
# **decommissionCommand**
> ApiCommand decommissionCommand(clusterName, serviceName, body)

Decommission roles of a service.

Decommission roles of a service. <p> For HBase services, the list should contain names of RegionServers to decommission. <p> For HDFS services, the list should contain names of DataNodes to decommission.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HBase service name.
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | List of role names to decommision.
try {
    ApiCommand result = apiInstance.decommissionCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#decommissionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HBase service name. |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to decommision. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteService"></a>
# **deleteService**
> ApiService deleteService(clusterName, serviceName)

Deletes a service from the system.

Deletes a service from the system.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service to delete.
try {
    ApiService result = apiInstance.deleteService(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#deleteService");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service to delete. |

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deployClientConfigCommand"></a>
# **deployClientConfigCommand**
> ApiCommand deployClientConfigCommand(clusterName, serviceName, body)

Deploy a service's client configuration.

Deploy a service's client configuration. <p> The client configuration is deployed to the hosts where the given roles are running. <p/> Added in v3: passing null for the role name list will deploy client configs to all known service roles. Added in v6: passing an empty role name list will deploy client configs to all known service roles. <p/> In Cloudera Manager 5.3 and newer, client configurations are fully managed, meaning that the server maintains state about which client configurations should exist and be managed by alternatives, and the agents actively rectify their hosts with this state. Consequently, if this API call is made with a specific set of roles, Cloudera Manager will deactivate, from alternatives, any deployed client configs from any non-gateway roles that are <em>not</em> specified as arguments. Gateway roles are always preserved, and calling this API with an empty or null argument continues to deploy to all roles. <p/>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | List of role names.
try {
    ApiCommand result = apiInstance.deployClientConfigCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#deployClientConfigCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="disableJtHaCommand"></a>
# **disableJtHaCommand**
> ApiCommand disableJtHaCommand(clusterName, serviceName, body)

Disable high availability (HA) for JobTracker.

Disable high availability (HA) for JobTracker.  As part of disabling HA, any services that depend on the MapReduce service being modified will be stopped. The command arguments provide options to specify name of JobTracker that will be preserved. The Command will redeploy the client configurations for services of the cluster after HA has been disabled.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The MapReduce service name.
ApiDisableJtHaArguments body = new ApiDisableJtHaArguments(); // ApiDisableJtHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.disableJtHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#disableJtHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The MapReduce service name. |
 **body** | [**ApiDisableJtHaArguments**](ApiDisableJtHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="disableLlamaHaCommand"></a>
# **disableLlamaHaCommand**
> ApiCommand disableLlamaHaCommand(clusterName, serviceName, body)

Not Supported.

Not Supported. Llama was removed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiDisableLlamaHaArguments body = new ApiDisableLlamaHaArguments(); // ApiDisableLlamaHaArguments | 
try {
    ApiCommand result = apiInstance.disableLlamaHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#disableLlamaHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiDisableLlamaHaArguments**](ApiDisableLlamaHaArguments.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="disableLlamaRmCommand"></a>
# **disableLlamaRmCommand**
> ApiCommand disableLlamaRmCommand(clusterName, serviceName)

Not Supported.

Not Supported. Llama was removed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
try {
    ApiCommand result = apiInstance.disableLlamaRmCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#disableLlamaRmCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="disableOozieHaCommand"></a>
# **disableOozieHaCommand**
> ApiCommand disableOozieHaCommand(clusterName, serviceName, body)

Disable high availability (HA) for Oozie.

Disable high availability (HA) for Oozie.  As part of disabling HA, any services that depend on the Oozie service being modified will be stopped. The command arguments provide options to specify name of Oozie Server that will be preserved. After deleting, other Oozie servers, all the services that were stopped are restarted.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Oozie service name.
ApiDisableOozieHaArguments body = new ApiDisableOozieHaArguments(); // ApiDisableOozieHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.disableOozieHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#disableOozieHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Oozie service name. |
 **body** | [**ApiDisableOozieHaArguments**](ApiDisableOozieHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="disableRmHaCommand"></a>
# **disableRmHaCommand**
> ApiCommand disableRmHaCommand(clusterName, serviceName, body)

Disable high availability (HA) for ResourceManager.

Disable high availability (HA) for ResourceManager.  As part of disabling HA, any services that depend on the YARN service being modified will be stopped. The command arguments provide options to specify name of ResourceManager that will be preserved. The command will redeploy the client configurations for services of the cluster after HA has been disabled.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The YARN service name.
ApiDisableRmHaArguments body = new ApiDisableRmHaArguments(); // ApiDisableRmHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.disableRmHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#disableRmHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The YARN service name. |
 **body** | [**ApiDisableRmHaArguments**](ApiDisableRmHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="disableSentryHaCommand"></a>
# **disableSentryHaCommand**
> ApiCommand disableSentryHaCommand(clusterName, serviceName, body)

Disable high availability (HA) for Sentry service.

Disable high availability (HA) for Sentry service. <p> This command only applies to CDH 5.13+ Sentry services. <p> The command will keep exactly one Sentry server, on the specified host, and update the ZooKeeper configs needed for Sentry. <p> All services that depend on HDFS will be restarted after enabling Sentry HA. <p> Note: Sentry doesn't support Rolling Restart.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Sentry service name.
ApiDisableSentryHaArgs body = new ApiDisableSentryHaArgs(); // ApiDisableSentryHaArgs | An instance of ApiDisableSentryHaArgs representing the arguments to the command.
try {
    ApiCommand result = apiInstance.disableSentryHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#disableSentryHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Sentry service name. |
 **body** | [**ApiDisableSentryHaArgs**](ApiDisableSentryHaArgs.md)| An instance of ApiDisableSentryHaArgs representing the arguments to the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enableJtHaCommand"></a>
# **enableJtHaCommand**
> ApiCommand enableJtHaCommand(clusterName, serviceName, body)

Enable high availability (HA) for a JobTracker.

Enable high availability (HA) for a JobTracker. <p> This command only applies to CDH4 MapReduce services. <p> The command will create a new JobTracker on the specified host and then create an active/standby pair with the existing JobTracker. Autofailover will be enabled using ZooKeeper. A ZNode will be created for this purpose. Command arguments provide option to forcefully create this ZNode if one already exists. A node may already exists if JobTracker was previously enabled in HA mode but HA mode was disabled later on. The ZNode is not deleted when HA is disabled. <p> As part of enabling HA, any services that depends on the MapReduce service being modified will be stopped. Command will redeploy the client configurations for services of the cluster after HA has been enabled.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The MapReduce service name.
ApiEnableJtHaArguments body = new ApiEnableJtHaArguments(); // ApiEnableJtHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.enableJtHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#enableJtHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The MapReduce service name. |
 **body** | [**ApiEnableJtHaArguments**](ApiEnableJtHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enableLlamaHaCommand"></a>
# **enableLlamaHaCommand**
> ApiCommand enableLlamaHaCommand(clusterName, serviceName, body)

Not Supported.

Not Supported. Llama was removed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiEnableLlamaHaArguments body = new ApiEnableLlamaHaArguments(); // ApiEnableLlamaHaArguments | 
try {
    ApiCommand result = apiInstance.enableLlamaHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#enableLlamaHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiEnableLlamaHaArguments**](ApiEnableLlamaHaArguments.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enableLlamaRmCommand"></a>
# **enableLlamaRmCommand**
> ApiCommand enableLlamaRmCommand(clusterName, serviceName, body)

Not Supported.

Not Supported. Llama was removed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiEnableLlamaRmArguments body = new ApiEnableLlamaRmArguments(); // ApiEnableLlamaRmArguments | 
try {
    ApiCommand result = apiInstance.enableLlamaRmCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#enableLlamaRmCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiEnableLlamaRmArguments**](ApiEnableLlamaRmArguments.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enableOozieHaCommand"></a>
# **enableOozieHaCommand**
> ApiCommand enableOozieHaCommand(clusterName, serviceName, body)

Enable high availability (HA) for Oozie service.

Enable high availability (HA) for Oozie service. <p> This command only applies to CDH5+ Oozie services. <p> The command will create new Oozie Servers on the specified hosts and set the ZooKeeper and Load Balancer configs needed for Oozie HA. <p> As part of enabling HA, any services that depends on the Oozie service being modified will be stopped and restarted after enabling Oozie HA.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Oozie service name.
ApiEnableOozieHaArguments body = new ApiEnableOozieHaArguments(); // ApiEnableOozieHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.enableOozieHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#enableOozieHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Oozie service name. |
 **body** | [**ApiEnableOozieHaArguments**](ApiEnableOozieHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enableRmHaCommand"></a>
# **enableRmHaCommand**
> ApiCommand enableRmHaCommand(clusterName, serviceName, body)

Enable high availability (HA) for a YARN ResourceManager.

Enable high availability (HA) for a YARN ResourceManager. <p> This command only applies to CDH5+ YARN services. <p> The command will create a new ResourceManager on the specified host and then create an active/standby pair with the existing ResourceManager. Autofailover will be enabled using ZooKeeper. <p> As part of enabling HA, any services that depends on the YARN service being modified will be stopped. Command will redeploy the client configurations for services of the cluster after HA has been enabled.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The YARN service name.
ApiEnableRmHaArguments body = new ApiEnableRmHaArguments(); // ApiEnableRmHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.enableRmHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#enableRmHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The YARN service name. |
 **body** | [**ApiEnableRmHaArguments**](ApiEnableRmHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enableSentryHaCommand"></a>
# **enableSentryHaCommand**
> ApiCommand enableSentryHaCommand(clusterName, serviceName, body)

Enable high availability (HA) for Sentry service.

Enable high availability (HA) for Sentry service. <p> This command only applies to CDH 5.13+ Sentry services. <p> The command will create a new Sentry server on the specified host and set the ZooKeeper configs needed for Sentry HA. <p> As part of enabling HA, all services that depend on HDFS will be restarted after enabling Sentry HA. <p> Note: Sentry doesn't support Rolling Restart.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Sentry service name.
ApiEnableSentryHaArgs body = new ApiEnableSentryHaArgs(); // ApiEnableSentryHaArgs | An instance of ApiEnableSentryHaArgs representing the arguments to the command.
try {
    ApiCommand result = apiInstance.enableSentryHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#enableSentryHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Sentry service name. |
 **body** | [**ApiEnableSentryHaArgs**](ApiEnableSentryHaArgs.md)| An instance of ApiEnableSentryHaArgs representing the arguments to the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enterMaintenanceMode"></a>
# **enterMaintenanceMode**
> ApiCommand enterMaintenanceMode(clusterName, serviceName)

Put the service into maintenance mode.

Put the service into maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiCommand result = apiInstance.enterMaintenanceMode(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#enterMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaintenanceMode"></a>
# **exitMaintenanceMode**
> ApiCommand exitMaintenanceMode(clusterName, serviceName)

Take the service out of maintenance mode.

Take the service out of maintenance mode. This is a synchronous command. The result is known immediately upon return.  <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiCommand result = apiInstance.exitMaintenanceMode(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#exitMaintenanceMode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="firstRun"></a>
# **firstRun**
> ApiCommand firstRun(clusterName, serviceName)

Prepare and start a service.

Prepare and start a service.  <p> Perform all the steps needed to prepare the service and start it. </p>  <p> Available since API v7. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the cluster.
try {
    ApiCommand result = apiInstance.firstRun(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#firstRun");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the cluster. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getClientConfig"></a>
# **getClientConfig**
> File getClientConfig(clusterName, serviceName)

Download a zip-compressed archive of the client configuration, of a specific service.

Download a zip-compressed archive of the client configuration, of a specific service. This resource does not require any authentication.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
try {
    File result = apiInstance.getClientConfig(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#getClientConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |

### Return type

[**File**](File.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getHdfsUsageReport"></a>
# **getHdfsUsageReport**
> ApiHdfsUsageReport getHdfsUsageReport(clusterName, serviceName, aggregation, from, nameservice, to)

Fetch the HDFS usage report.

Fetch the HDFS usage report. For the requested time range, at the specified aggregation intervals, the report shows HDFS disk usages per user. <p> This call supports returning JSON or CSV, as determined by the \"Accept\" header of application/json or text/csv. <p> Available since API v4. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
String aggregation = "daily"; // String | The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\".
String from = "from_example"; // String | The (optional) start time of the report in ISO 8601 format ( defaults to 24 hours before \"to\" time).
String nameservice = "nameservice_example"; // String | The (optional) HDFS nameservice. Required for HA setup.
String to = "now"; // String | The (optional) end time of the report in ISO 8601 format ( defaults to now).
try {
    ApiHdfsUsageReport result = apiInstance.getHdfsUsageReport(clusterName, serviceName, aggregation, from, nameservice, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#getHdfsUsageReport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **aggregation** | **String**| The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\". | [optional] [default to daily] [enum: DAILY, HOURLY, WEEKLY]
 **from** | **String**| The (optional) start time of the report in ISO 8601 format ( defaults to 24 hours before \"to\" time). | [optional]
 **nameservice** | **String**| The (optional) HDFS nameservice. Required for HA setup. | [optional]
 **to** | **String**| The (optional) end time of the report in ISO 8601 format ( defaults to now). | [optional] [default to now]

### Return type

[**ApiHdfsUsageReport**](ApiHdfsUsageReport.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

<a name="getImpalaUtilization"></a>
# **getImpalaUtilization**
> ApiImpalaUtilization getImpalaUtilization(clusterName, serviceName, daysOfWeek, endHourOfDay, from, startHourOfDay, tenantType, to)

Provides the resource utilization of the Impala service as well as the resource utilization per tenant.

Provides the resource utilization of the Impala service as well as the resource utilization per tenant. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | service name
List<String> daysOfWeek = Arrays.asList("daysOfWeek_example"); // List<String> | The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified.
Integer endHourOfDay = 23; // Integer | The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified.
String from = "from_example"; // String | Start of the time range to report utilization in ISO 8601 format.
Integer startHourOfDay = 0; // Integer | The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified.
String tenantType = "POOL"; // String | The type of the tenant (POOL or USER).
String to = "now"; // String | End of the the time range to report utilization in ISO 8601 format (defaults to now).
try {
    ApiImpalaUtilization result = apiInstance.getImpalaUtilization(clusterName, serviceName, daysOfWeek, endHourOfDay, from, startHourOfDay, tenantType, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#getImpalaUtilization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| service name |
 **daysOfWeek** | [**List&lt;String&gt;**](String.md)| The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified. | [optional]
 **endHourOfDay** | **Integer**| The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. | [optional] [default to 23]
 **from** | **String**| Start of the time range to report utilization in ISO 8601 format. | [optional]
 **startHourOfDay** | **Integer**| The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. | [optional] [default to 0]
 **tenantType** | **String**| The type of the tenant (POOL or USER). | [optional] [default to POOL]
 **to** | **String**| End of the the time range to report utilization in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiImpalaUtilization**](ApiImpalaUtilization.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMetrics"></a>
# **getMetrics**
> ApiMetricList getMetrics(clusterName, serviceName, from, metrics, to, view)

Fetch metric readings for a particular service.

Fetch metric readings for a particular service. <p> By default, this call will look up all metrics available for the service. If only specific metrics are desired, use the <i>metrics</i> parameter. <p> By default, the returned results correspond to a 5 minute window based on the provided end time (which defaults to the current server time). The <i>from</i> and <i>to</i> parameters can be used to control the window being queried. A maximum window of 3 hours is enforced. <p> When requesting a \"full\" view, aside from the extended properties of the returned metric data, the collection will also contain information about all metrics available for the service, even if no readings are available in the requested window. <p> HDFS services that have more than one nameservice will not expose any metrics. Instead, the nameservices should be queried separately. <p/>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service.
String from = "from_example"; // String | Start of the period to query.
List<String> metrics = Arrays.asList("metrics_example"); // List<String> | Filter for which metrics to query.
String to = "now"; // String | End of the period to query.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiMetricList result = apiInstance.getMetrics(clusterName, serviceName, from, metrics, to, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#getMetrics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service. |
 **from** | **String**| Start of the period to query. | [optional]
 **metrics** | [**List&lt;String&gt;**](String.md)| Filter for which metrics to query. | [optional]
 **to** | **String**| End of the period to query. | [optional] [default to now]
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiMetricList**](ApiMetricList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMrUsageReport"></a>
# **getMrUsageReport**
> ApiMrUsageReport getMrUsageReport(clusterName, serviceName, aggregation, from, to)

Fetch the MR usage report.

Fetch the MR usage report. For the requested time range, at the specified aggregation intervals, the report shows job CPU usages (and other metrics) per user. <p> This call supports returning JSON or CSV, as determined by the \"Accept\" header of application/json or text/csv. <p> Available since API v4. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The MR service name.
String aggregation = "daily"; // String | The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\".
String from = "from_example"; // String | The (optional) start time of the report in ISO 8601 format (defaults to 24 hours before \"to\" time).
String to = "now"; // String | The (optional) end time of the report in ISO 8601 format (defaults to now).
try {
    ApiMrUsageReport result = apiInstance.getMrUsageReport(clusterName, serviceName, aggregation, from, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#getMrUsageReport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The MR service name. |
 **aggregation** | **String**| The (optional) aggregation period for the data. Supports \"hourly\", \"daily\" (default) and \"weekly\". | [optional] [default to daily] [enum: DAILY, HOURLY, WEEKLY]
 **from** | **String**| The (optional) start time of the report in ISO 8601 format (defaults to 24 hours before \"to\" time). | [optional]
 **to** | **String**| The (optional) end time of the report in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiMrUsageReport**](ApiMrUsageReport.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

<a name="getYarnUtilization"></a>
# **getYarnUtilization**
> ApiYarnUtilization getYarnUtilization(clusterName, serviceName, daysOfWeek, endHourOfDay, from, startHourOfDay, tenantType, to)

Provides the resource utilization of the yarn service as well as the resource utilization per tenant.

Provides the resource utilization of the yarn service as well as the resource utilization per tenant. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | service name
List<String> daysOfWeek = Arrays.asList("daysOfWeek_example"); // List<String> | The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified.
Integer endHourOfDay = 23; // Integer | The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified.
String from = "from_example"; // String | Start of the time range to report utilization in ISO 8601 format.
Integer startHourOfDay = 0; // Integer | The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified.
String tenantType = "POOL"; // String | The type of the tenant (POOL or USER).
String to = "now"; // String | End of the the time range to report utilization in ISO 8601 format (defaults to now).
try {
    ApiYarnUtilization result = apiInstance.getYarnUtilization(clusterName, serviceName, daysOfWeek, endHourOfDay, from, startHourOfDay, tenantType, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#getYarnUtilization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| service name |
 **daysOfWeek** | [**List&lt;String&gt;**](String.md)| The days of the week for which the user wants to report utilization. Days is a list of number between 1 to 7, where 1 corresponds to Mon. and 7 corresponds to Sun. All 7 days are included if this is not specified. | [optional]
 **endHourOfDay** | **Integer**| The end hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 23 if this is not specified. | [optional] [default to 23]
 **from** | **String**| Start of the time range to report utilization in ISO 8601 format. | [optional]
 **startHourOfDay** | **Integer**| The start hour of a day for which the user wants to report utilization. The hour is a number between [0-23]. Default value is 0 if this is not specified. | [optional] [default to 0]
 **tenantType** | **String**| The type of the tenant (POOL or USER). | [optional] [default to POOL]
 **to** | **String**| End of the the time range to report utilization in ISO 8601 format (defaults to now). | [optional] [default to now]

### Return type

[**ApiYarnUtilization**](ApiYarnUtilization.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hbaseUpgradeCommand"></a>
# **hbaseUpgradeCommand**
> ApiCommand hbaseUpgradeCommand(clusterName, serviceName)

Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5.

Upgrade HBase data in HDFS and ZooKeeper as part of upgrade from CDH4 to CDH5. <p/> This is required in order to run HBase after upgrade. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HBase service name.
try {
    ApiCommand result = apiInstance.hbaseUpgradeCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hbaseUpgradeCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HBase service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hdfsCreateTmpDir"></a>
# **hdfsCreateTmpDir**
> ApiCommand hdfsCreateTmpDir(clusterName, serviceName)

Creates a tmp directory on the HDFS filesystem.

Creates a tmp directory on the HDFS filesystem. <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the HDFS service on which to run the command.
try {
    ApiCommand result = apiInstance.hdfsCreateTmpDir(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsCreateTmpDir");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the HDFS service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hdfsDisableAutoFailoverCommand"></a>
# **hdfsDisableAutoFailoverCommand**
> ApiCommand hdfsDisableAutoFailoverCommand(clusterName, serviceName, body)

Disable auto-failover for a highly available HDFS nameservice.

Disable auto-failover for a highly available HDFS nameservice. <p> The command will modify the nameservice's NameNodes configuration to disable automatic failover, and delete the existing failover controllers. <p> The ZooKeeper dependency of the service will not be removed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
String body = "body_example"; // String | The nameservice name.
try {
    ApiCommand result = apiInstance.hdfsDisableAutoFailoverCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsDisableAutoFailoverCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **body** | **String**| The nameservice name. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

<a name="hdfsDisableHaCommand"></a>
# **hdfsDisableHaCommand**
> ApiCommand hdfsDisableHaCommand(clusterName, serviceName, body)

Disable high availability (HA) for an HDFS NameNode.

Disable high availability (HA) for an HDFS NameNode. <p> The NameNode to be kept must be running before HA can be disabled. <p> As part of disabling HA, any services that depend on the HDFS service being modified will be stopped. The command arguments provide options to re-start these services and to re-deploy the client configurations for services of the cluster after HA has been disabled.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
ApiHdfsDisableHaArguments body = new ApiHdfsDisableHaArguments(); // ApiHdfsDisableHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.hdfsDisableHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsDisableHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **body** | [**ApiHdfsDisableHaArguments**](ApiHdfsDisableHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsDisableNnHaCommand"></a>
# **hdfsDisableNnHaCommand**
> ApiCommand hdfsDisableNnHaCommand(clusterName, serviceName, body)

Disable High Availability (HA) with Automatic Failover for an HDFS NameNode.

Disable High Availability (HA) with Automatic Failover for an HDFS NameNode. <p> As part of disabling HA, any services that depend on the HDFS service being modified will be stopped. The command will delete the Standby NameNode associated with the specified NameNode. Any FailoverControllers associated with the NameNode's nameservice are also deleted. A SecondaryNameNode is created on the host specified by the arugments. <p> If no nameservices uses Quorum Journal after HA is disabled for the specified nameservice, then all JournalNodes are also deleted. <p> Then, HDFS service is restarted and all services that were stopped are started again afterwards. Finally, client configs for HDFS and its depedents will be re-deployed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
ApiDisableNnHaArguments body = new ApiDisableNnHaArguments(); // ApiDisableNnHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.hdfsDisableNnHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsDisableNnHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **body** | [**ApiDisableNnHaArguments**](ApiDisableNnHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsEnableAutoFailoverCommand"></a>
# **hdfsEnableAutoFailoverCommand**
> ApiCommand hdfsEnableAutoFailoverCommand(clusterName, serviceName, body)

Enable auto-failover for an HDFS nameservice.

Enable auto-failover for an HDFS nameservice. <p> This command requires that the nameservice exists, and HA has been configured for that nameservice. <p> The command will create the needed failover controllers, perform the needed initialization and configuration, and will start the new roles. The existing NameNodes which are part of the nameservice will be re-started in the process. <p> This process may require changing the service's configuration, to add a dependency on the provided ZooKeeper service. This will be done if such a dependency has not been configured yet, and will cause roles that are not affected by this command to show an \"outdated configuration\" status. <p> If a ZooKeeper dependency has already been set up by some other means, it does not need to be provided in the command arguments.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
ApiHdfsFailoverArguments body = new ApiHdfsFailoverArguments(); // ApiHdfsFailoverArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.hdfsEnableAutoFailoverCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsEnableAutoFailoverCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **body** | [**ApiHdfsFailoverArguments**](ApiHdfsFailoverArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsEnableHaCommand"></a>
# **hdfsEnableHaCommand**
> ApiCommand hdfsEnableHaCommand(clusterName, serviceName, body)

Enable high availability (HA) for an HDFS NameNode.

Enable high availability (HA) for an HDFS NameNode. <p> The command will set up the given \"active\" and \"stand-by\" NameNodes as an HA pair. Both nodes need to already exist. <p> If there is a SecondaryNameNode associated with either given NameNode instance, it will be deleted. <p> Note that while the shared edits path may be different for both nodes, they need to point to the same underlying storage (e.g., an NFS share). <p> As part of enabling HA, any services that depend on the HDFS service being modified will be stopped. The command arguments provide options to re-start these services and to re-deploy the client configurations for services of the cluster after HA has been enabled.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
ApiHdfsHaArguments body = new ApiHdfsHaArguments(); // ApiHdfsHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.hdfsEnableHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsEnableHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **body** | [**ApiHdfsHaArguments**](ApiHdfsHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsEnableNnHaCommand"></a>
# **hdfsEnableNnHaCommand**
> ApiCommand hdfsEnableNnHaCommand(clusterName, serviceName, body)

Enable High Availability (HA) with Automatic Failover for an HDFS NameNode.

Enable High Availability (HA) with Automatic Failover for an HDFS NameNode. <p> The command will create a Standby NameNode for the given nameservice and create FailoverControllers for both Active and Standby NameNodes. The SecondaryNameNode associated with the Active NameNode will be deleted. <p> The command will also create JournalNodes needed for HDFS HA if they do not already exist. <p> As part of enabling HA, any services that depend on the HDFS service being modified will be stopped. They will be restarted after HA has been enabled. Finally, client configs for HDFS and its depedents will be re-deployed.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
ApiEnableNnHaArguments body = new ApiEnableNnHaArguments(); // ApiEnableNnHaArguments | Arguments for the command.
try {
    ApiCommand result = apiInstance.hdfsEnableNnHaCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsEnableNnHaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **body** | [**ApiEnableNnHaArguments**](ApiEnableNnHaArguments.md)| Arguments for the command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsFailoverCommand"></a>
# **hdfsFailoverCommand**
> ApiCommand hdfsFailoverCommand(clusterName, serviceName, force, body)

Initiate a failover in an HDFS HA NameNode pair.

Initiate a failover in an HDFS HA NameNode pair. <p> The arguments should contain the names of the two NameNodes in the HA pair. The first one should be the currently active NameNode, the second one the NameNode to be made active.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
Boolean force = false; // Boolean | Whether to force failover.
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | Names of the NameNodes in the HA pair.
try {
    ApiCommand result = apiInstance.hdfsFailoverCommand(clusterName, serviceName, force, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsFailoverCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **force** | **Boolean**| Whether to force failover. | [optional] [default to false]
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| Names of the NameNodes in the HA pair. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsFinalizeRollingUpgrade"></a>
# **hdfsFinalizeRollingUpgrade**
> ApiCommand hdfsFinalizeRollingUpgrade(clusterName, serviceName)

Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version.

Finalizes the rolling upgrade for HDFS by updating the NameNode metadata permanently to the next version. Should be done after doing a rolling upgrade to a CDH version >= 5.2.0. <p> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
try {
    ApiCommand result = apiInstance.hdfsFinalizeRollingUpgrade(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsFinalizeRollingUpgrade");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hdfsRollEditsCommand"></a>
# **hdfsRollEditsCommand**
> ApiCommand hdfsRollEditsCommand(clusterName, serviceName, body)

Roll the edits of an HDFS NameNode or Nameservice.

Roll the edits of an HDFS NameNode or Nameservice. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
ApiRollEditsArgs body = new ApiRollEditsArgs(); // ApiRollEditsArgs | Arguments to the Roll Edits command.
try {
    ApiCommand result = apiInstance.hdfsRollEditsCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsRollEditsCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |
 **body** | [**ApiRollEditsArgs**](ApiRollEditsArgs.md)| Arguments to the Roll Edits command. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="hdfsUpgradeMetadataCommand"></a>
# **hdfsUpgradeMetadataCommand**
> ApiCommand hdfsUpgradeMetadataCommand(clusterName, serviceName)

Upgrade HDFS Metadata as part of a major version upgrade.

Upgrade HDFS Metadata as part of a major version upgrade. <p/> When doing a major version upgrade for HDFS, it is necessary to start HDFS in a special mode where it will do any necessary upgrades of stored metadata. Trying to start HDFS normally will result in an error message and the NameNode(s) failing to start. <p/> The metadata upgrade must eventually be finalized, using the hdfsFinalizeMetadataUpgrade command on the NameNode. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The HDFS service name.
try {
    ApiCommand result = apiInstance.hdfsUpgradeMetadataCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hdfsUpgradeMetadataCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The HDFS service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hiveCreateMetastoreDatabaseCommand"></a>
# **hiveCreateMetastoreDatabaseCommand**
> ApiCommand hiveCreateMetastoreDatabaseCommand(clusterName, serviceName)

Create the Hive Metastore Database.

Create the Hive Metastore Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database needs to be created in the embedded postgresql database for a Hive service. This command should usually be followed by a call to hiveCreateMetastoreDatabaseTables. <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Hive service on which to run the command.
try {
    ApiCommand result = apiInstance.hiveCreateMetastoreDatabaseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hiveCreateMetastoreDatabaseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Hive service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hiveCreateMetastoreDatabaseTablesCommand"></a>
# **hiveCreateMetastoreDatabaseTablesCommand**
> ApiCommand hiveCreateMetastoreDatabaseTablesCommand(clusterName, serviceName)

Create the Hive Metastore Database tables.

Create the Hive Metastore Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only Available when all Hive Metastore Servers are stopped. <p> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Hive service on which to run the command.
try {
    ApiCommand result = apiInstance.hiveCreateMetastoreDatabaseTablesCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hiveCreateMetastoreDatabaseTablesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Hive service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hiveUpdateMetastoreNamenodesCommand"></a>
# **hiveUpdateMetastoreNamenodesCommand**
> ApiCommand hiveUpdateMetastoreNamenodesCommand(clusterName, serviceName)

Update Hive Metastore to point to a NameNode's Nameservice name instead of hostname.

Update Hive Metastore to point to a NameNode's Nameservice name instead of hostname. <p> <strong>Back up the Hive Metastore Database before running this command.</strong> <p> This command is to be run after enabling HDFS High Availability. Only available when all Hive Metastore Servers are stopped. <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Hive service on which to run the command.
try {
    ApiCommand result = apiInstance.hiveUpdateMetastoreNamenodesCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hiveUpdateMetastoreNamenodesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Hive service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hiveUpgradeMetastoreCommand"></a>
# **hiveUpgradeMetastoreCommand**
> ApiCommand hiveUpgradeMetastoreCommand(clusterName, serviceName)

Upgrade Hive Metastore as part of a major version upgrade.

Upgrade Hive Metastore as part of a major version upgrade. <p/> When doing a major version upgrade for Hive, it is necessary to upgrade data in the metastore database. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Hive service name.
try {
    ApiCommand result = apiInstance.hiveUpgradeMetastoreCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hiveUpgradeMetastoreCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Hive service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hiveValidateMetastoreSchemaCommand"></a>
# **hiveValidateMetastoreSchemaCommand**
> ApiCommand hiveValidateMetastoreSchemaCommand(clusterName, serviceName)

Validate the Hive Metastore Schema.

Validate the Hive Metastore Schema. <p> This command checks the Hive metastore schema for any errors and corruptions. This command is to be run on two instances: <li>After the Hive Metastore database tables are created.</li> <li>Both before and after upgrading the Hive metastore database schema./li> * <p> Available since API v17.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Hive service on which to run the command.
try {
    ApiCommand result = apiInstance.hiveValidateMetastoreSchemaCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hiveValidateMetastoreSchemaCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Hive service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hueDumpDbCommand"></a>
# **hueDumpDbCommand**
> ApiCommand hueDumpDbCommand(clusterName, serviceName)

Runs Hue's dumpdata command.

Runs Hue's dumpdata command.  Available since API v10.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
try {
    ApiCommand result = apiInstance.hueDumpDbCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hueDumpDbCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hueLoadDbCommand"></a>
# **hueLoadDbCommand**
> ApiCommand hueLoadDbCommand(clusterName, serviceName)

Runs Hue's loaddata command.

Runs Hue's loaddata command.  Available since API v10.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
try {
    ApiCommand result = apiInstance.hueLoadDbCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hueLoadDbCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="hueSyncDbCommand"></a>
# **hueSyncDbCommand**
> ApiCommand hueSyncDbCommand(clusterName, serviceName)

Runs Hue's syncdb command.

Runs Hue's syncdb command.  Available since API v10.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
try {
    ApiCommand result = apiInstance.hueSyncDbCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#hueSyncDbCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="impalaCreateCatalogDatabaseCommand"></a>
# **impalaCreateCatalogDatabaseCommand**
> ApiCommand impalaCreateCatalogDatabaseCommand(clusterName, serviceName)

.

<strong>Not needed in CM 5.0.0 Release, since Impala Catalog Database is not yet available in CDH as of this release.</strong> Create the Impala Catalog Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database needs to be created in the embedded postgresql database for the Impala Catalog Server. This command should usually be followed by a call to impalaCreateCatalogDatabaseTables. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Impala service on which to run the command.
try {
    ApiCommand result = apiInstance.impalaCreateCatalogDatabaseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#impalaCreateCatalogDatabaseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Impala service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="impalaCreateCatalogDatabaseTablesCommand"></a>
# **impalaCreateCatalogDatabaseTablesCommand**
> ApiCommand impalaCreateCatalogDatabaseTablesCommand(clusterName, serviceName)

.

<strong>Not needed in CM 5.0.0 Release, since Impala Catalog Database is not yet available in CDH as of this release.</strong> Create the Impala Catalog Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only available when all Impala Catalog Servers are stopped. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Impala service on which to run the command.
try {
    ApiCommand result = apiInstance.impalaCreateCatalogDatabaseTablesCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#impalaCreateCatalogDatabaseTablesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Impala service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="importMrConfigsIntoYarn"></a>
# **importMrConfigsIntoYarn**
> ApiCommand importMrConfigsIntoYarn(clusterName, serviceName)

Import MapReduce configuration into Yarn, overwriting Yarn configuration.

Import MapReduce configuration into Yarn, overwriting Yarn configuration. <p> You will lose existing Yarn configuration. Read all MapReduce configuration, role assignments, and role configuration groups and update Yarn with corresponding values. MR1 configuration will be converted into the equivalent MR2 configuration. <p> Before running this command, Yarn must be stopped and MapReduce must exist with valid configuration. <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Yarn service on which to run the command.
try {
    ApiCommand result = apiInstance.importMrConfigsIntoYarn(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#importMrConfigsIntoYarn");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Yarn service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="initSolrCommand"></a>
# **initSolrCommand**
> ApiCommand initSolrCommand(clusterName, serviceName)

Initializes the Solr service in Zookeeper.

Initializes the Solr service in Zookeeper.  <p> Available since API v4.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Solr service name.
try {
    ApiCommand result = apiInstance.initSolrCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#initSolrCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="installMrFrameworkJars"></a>
# **installMrFrameworkJars**
> ApiCommand installMrFrameworkJars(clusterName, serviceName)

Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it.

Creates an HDFS directory to hold the MapReduce2 framework JARs (if necessary), and uploads the framework JARs to it. <p> This command is run automatically when starting a YARN service for the first time, or when upgrading an existing YARN service. It can also be run manually to ensure that the latest version of the framework JARS is installed. <p> Available since API v30. <p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the YARN service on which to run the command.
try {
    ApiCommand result = apiInstance.installMrFrameworkJars(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#installMrFrameworkJars");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the YARN service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="installOozieShareLib"></a>
# **installOozieShareLib**
> ApiCommand installOozieShareLib(clusterName, serviceName)

Creates directory for Oozie user in HDFS and installs the ShareLib in it.

Creates directory for Oozie user in HDFS and installs the ShareLib in it. <p/> This command should be re-run after a major version upgrade to refresh the ShareLib to the latest version. <p/> Available since API v3. <p/>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Oozie service on which to run the command.
try {
    ApiCommand result = apiInstance.installOozieShareLib(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#installOozieShareLib");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Oozie service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="ksMigrateToSentry"></a>
# **ksMigrateToSentry**
> ApiCommand ksMigrateToSentry(clusterName, serviceName)

Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer.

Migrates the HBase Indexer policy-based permissions to Sentry, by invoking the SentryConfigToolIndexer. <p> Note: <li> <ul>KeyStore Indexer service should be in Stopped state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> <p> Available since API v30.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the KeyStore Indexer service name.
try {
    ApiCommand result = apiInstance.ksMigrateToSentry(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#ksMigrateToSentry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the KeyStore Indexer service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listActiveCommands"></a>
# **listActiveCommands**
> ApiCommandList listActiveCommands(clusterName, serviceName, view)

List active service commands.

List active service commands.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to which the role belongs.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiCommandList result = apiInstance.listActiveCommands(clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#listActiveCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to which the role belongs. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiCommandList**](ApiCommandList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listRoleTypes"></a>
# **listRoleTypes**
> ApiRoleTypeList listRoleTypes(clusterName, serviceName)

List the supported role types for a service.

List the supported role types for a service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to modify.
try {
    ApiRoleTypeList result = apiInstance.listRoleTypes(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#listRoleTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to modify. |

### Return type

[**ApiRoleTypeList**](ApiRoleTypeList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listServiceCommands"></a>
# **listServiceCommands**
> ApiCommandMetadataList listServiceCommands(clusterName, serviceName)

Lists all the commands that can be executed by name on the provided service.

Lists all the commands that can be executed by name on the provided service.  <p> Available since API v6. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiCommandMetadataList result = apiInstance.listServiceCommands(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#listServiceCommands");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiCommandMetadataList**](ApiCommandMetadataList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="offlineCommand"></a>
# **offlineCommand**
> ApiCommand offlineCommand(clusterName, serviceName, timeout, body)

Offline roles of a service.

Offline roles of a service. <p> Currently the offline operation is only supported by HDFS. <p> For HDFS, the offline operation will put DataNodes into <em>HDFS IN MAINTENANCE</em> state which prevents unnecessary re-replication which could occur if decommissioned. <p> The <em>timeout</em> parameter is used to specify a timeout for offline. For HDFS, when the timeout expires, the DataNode will automatically transition out of <em>HDFS IN MAINTENANCE</em> state, back to <em>HDFS IN SERVICE</em> state. <p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
BigDecimal timeout = new BigDecimal(); // BigDecimal | Offline timeout in seconds. Offlined roles will automatically transition from offline state to normal state after timeout. Specify as null to get the default timeout (4 hours).
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | List of role names to offline.
try {
    ApiCommand result = apiInstance.offlineCommand(clusterName, serviceName, timeout, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#offlineCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **timeout** | **BigDecimal**| Offline timeout in seconds. Offlined roles will automatically transition from offline state to normal state after timeout. Specify as null to get the default timeout (4 hours). | [optional]
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to offline. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="oozieCreateEmbeddedDatabaseCommand"></a>
# **oozieCreateEmbeddedDatabaseCommand**
> ApiCommand oozieCreateEmbeddedDatabaseCommand(clusterName, serviceName)

Create the Oozie Server Database.

Create the Oozie Server Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database need to be created in the embedded postgresql database for an Oozie service. This command should usually be followed by a call to createOozieDb. <p> Available since API v10.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Oozie service on which to run the command.
try {
    ApiCommand result = apiInstance.oozieCreateEmbeddedDatabaseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#oozieCreateEmbeddedDatabaseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Oozie service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="oozieDumpDatabaseCommand"></a>
# **oozieDumpDatabaseCommand**
> ApiCommand oozieDumpDatabaseCommand(clusterName, serviceName)

Dump the Oozie Server Database.

Dump the Oozie Server Database.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
try {
    ApiCommand result = apiInstance.oozieDumpDatabaseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#oozieDumpDatabaseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="oozieLoadDatabaseCommand"></a>
# **oozieLoadDatabaseCommand**
> ApiCommand oozieLoadDatabaseCommand(clusterName, serviceName)

Load the Oozie Server Database from dump.

Load the Oozie Server Database from dump.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The name of the service
try {
    ApiCommand result = apiInstance.oozieLoadDatabaseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#oozieLoadDatabaseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The name of the service |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="oozieUpgradeDbCommand"></a>
# **oozieUpgradeDbCommand**
> ApiCommand oozieUpgradeDbCommand(clusterName, serviceName)

Upgrade Oozie Database schema as part of a major version upgrade.

Upgrade Oozie Database schema as part of a major version upgrade. <p/> When doing a major version upgrade for Oozie, it is necessary to upgrade the schema of its database before Oozie can run successfully. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Oozie service name.
try {
    ApiCommand result = apiInstance.oozieUpgradeDbCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#oozieUpgradeDbCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Oozie service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readService"></a>
# **readService**
> ApiService readService(clusterName, serviceName, view)

Retrieves details information about a service.

Retrieves details information about a service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
String view = "full"; // String | DataView to materialize. Defaults to 'full'.
try {
    ApiService result = apiInstance.readService(clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#readService");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **view** | **String**| DataView to materialize. Defaults to 'full'. | [optional] [default to full] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readServiceConfig"></a>
# **readServiceConfig**
> ApiServiceConfig readServiceConfig(clusterName, serviceName, view)

Retrieves the configuration of a specific service.

Retrieves the configuration of a specific service. <p> The \"summary\" view contains only the configured parameters, and configuration for role types that contain configured parameters. <p> The \"full\" view contains all available configuration parameters for the service and its role types. This mode performs validation on the configuration, which could take a few seconds on a large cluster (around 500 nodes or more).

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to query.
String view = "summary"; // String | The view of the data to materialize, either \"summary\" or \"full\".
try {
    ApiServiceConfig result = apiInstance.readServiceConfig(clusterName, serviceName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#readServiceConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to query. |
 **view** | **String**| The view of the data to materialize, either \"summary\" or \"full\". | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="readServices"></a>
# **readServices**
> ApiServiceList readServices(clusterName, view)

Lists all services registered in the cluster.

Lists all services registered in the cluster.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String view = "summary"; // String | 
try {
    ApiServiceList result = apiInstance.readServices(clusterName, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#readServices");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **view** | **String**|  | [optional] [default to summary] [enum: EXPORT, EXPORT_REDACTED, FULL, FULL_WITH_HEALTH_CHECK_EXPLANATION, SUMMARY]

### Return type

[**ApiServiceList**](ApiServiceList.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="recommissionCommand"></a>
# **recommissionCommand**
> ApiCommand recommissionCommand(clusterName, serviceName, body)

Recommission roles of a service.

Recommission roles of a service. <p> The list should contain names of slave roles to recommission. </p>  <p> Available since API v2. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the service on which to run the command.
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | List of role names to recommision.
try {
    ApiCommand result = apiInstance.recommissionCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#recommissionCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the service on which to run the command. |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to recommision. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="recommissionWithStartCommand"></a>
# **recommissionWithStartCommand**
> ApiCommand recommissionWithStartCommand(clusterName, serviceName, body)

Start and recommission roles of a service.

Start and recommission roles of a service. <p> The list should contain names of slave roles to start and recommission. </p>  <p> Warning: Evolving. This method may change in the future and does not offer standard compatibility guarantees. Only support by HDFS. Do not use without guidance from Cloudera. </p>  <p> Available since API v15. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the service on which to run the command.
ApiRoleNameList body = new ApiRoleNameList(); // ApiRoleNameList | List of role names to recommision.
try {
    ApiCommand result = apiInstance.recommissionWithStartCommand(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#recommissionWithStartCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the service on which to run the command. |
 **body** | [**ApiRoleNameList**](ApiRoleNameList.md)| List of role names to recommision. | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="restartCommand"></a>
# **restartCommand**
> ApiCommand restartCommand(clusterName, serviceName)

Restart the service.

Restart the service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to start.
try {
    ApiCommand result = apiInstance.restartCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#restartCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to start. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rollingRestart"></a>
# **rollingRestart**
> ApiCommand rollingRestart(clusterName, serviceName, body)

Command to run rolling restart of roles in a service.

Command to run rolling restart of roles in a service. The sequence is: <ol> <li>Restart all the non-slave roles <li>If slaves are present restart them in batches of size specified in RollingRestartCmdArgs <li>Perform any post-command needed after rolling restart </ol> <p> Available since API v3. Only available with Cloudera Manager Enterprise Edition.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | 
ApiRollingRestartArgs body = new ApiRollingRestartArgs(); // ApiRollingRestartArgs | 
try {
    ApiCommand result = apiInstance.rollingRestart(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#rollingRestart");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**|  |
 **body** | [**ApiRollingRestartArgs**](ApiRollingRestartArgs.md)|  | [optional]

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sentryCreateDatabaseCommand"></a>
# **sentryCreateDatabaseCommand**
> ApiCommand sentryCreateDatabaseCommand(clusterName, serviceName)

Create the Sentry Server Database.

Create the Sentry Server Database. Only works with embedded postgresql database. <p> This command is to be run whenever a new user and database need to be created in the embedded postgresql database for a Sentry service. This command should usually be followed by a call to sentryCreateDatabaseTables. <p> Available since API v7.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Sentry service on which to run the command.
try {
    ApiCommand result = apiInstance.sentryCreateDatabaseCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#sentryCreateDatabaseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Sentry service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="sentryCreateDatabaseTablesCommand"></a>
# **sentryCreateDatabaseTablesCommand**
> ApiCommand sentryCreateDatabaseTablesCommand(clusterName, serviceName)

Create the Sentry Server Database tables.

Create the Sentry Server Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only Available when Sentry Server is stopped. <p> Available since API v7.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Sentry service on which to run the command.
try {
    ApiCommand result = apiInstance.sentryCreateDatabaseTablesCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#sentryCreateDatabaseTablesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Sentry service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="sentryUpgradeDatabaseTablesCommand"></a>
# **sentryUpgradeDatabaseTablesCommand**
> ApiCommand sentryUpgradeDatabaseTablesCommand(clusterName, serviceName)

Upgrade the Sentry Server Database tables.

Upgrade the Sentry Server Database tables. <p> This command is to be run whenever Sentry requires an upgrade to its database tables. <p> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Sentry service on which to run the command.
try {
    ApiCommand result = apiInstance.sentryUpgradeDatabaseTablesCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#sentryUpgradeDatabaseTablesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Sentry service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="serviceCommandByName"></a>
# **serviceCommandByName**
> ApiCommand serviceCommandByName(clusterName, commandName, serviceName)

Executes a command on the service specified by name.

Executes a command on the service specified by name. <p> Available since API v6. </p>

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String commandName = "commandName_example"; // String | The command name.
String serviceName = "serviceName_example"; // String | The service name.
try {
    ApiCommand result = apiInstance.serviceCommandByName(clusterName, commandName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#serviceCommandByName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **commandName** | **String**| The command name. |
 **serviceName** | **String**| The service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="solrBootstrapCollectionsCommand"></a>
# **solrBootstrapCollectionsCommand**
> ApiCommand solrBootstrapCollectionsCommand(clusterName, serviceName)

Bootstraps Solr Collections after the CDH upgrade.

Bootstraps Solr Collections after the CDH upgrade. <p> Note: This is only needed for upgrading to CDH6.0. <p> Available since API v30.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Solr service name.
try {
    ApiCommand result = apiInstance.solrBootstrapCollectionsCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#solrBootstrapCollectionsCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="solrBootstrapConfigCommand"></a>
# **solrBootstrapConfigCommand**
> ApiCommand solrBootstrapConfigCommand(clusterName, serviceName)

Bootstraps Solr config during the CDH upgrade.

Bootstraps Solr config during the CDH upgrade. <p> Note: This is only needed for upgrading to CDH6.0. <p> Available since API v30.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Solr service name.
try {
    ApiCommand result = apiInstance.solrBootstrapConfigCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#solrBootstrapConfigCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="solrConfigBackupCommand"></a>
# **solrConfigBackupCommand**
> ApiCommand solrConfigBackupCommand(clusterName, serviceName)

Backs up Solr configuration metadata before CDH upgrade.

Backs up Solr configuration metadata before CDH upgrade. <p> Note: <li> <ul>Solr service should be in Stopped state.</ul> <ul>HDFS and Zookeeper services should in Running state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> </p> Available since API v30.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Solr service name.
try {
    ApiCommand result = apiInstance.solrConfigBackupCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#solrConfigBackupCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="solrMigrateSentryPrivilegesCommand"></a>
# **solrMigrateSentryPrivilegesCommand**
> ApiCommand solrMigrateSentryPrivilegesCommand(clusterName, serviceName)

Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service.

Migrates Sentry privileges to new model compatible to support more granular permissions if Solr is configured with a Sentry service. <p> Note: <li> <ul>Solr service should be in Stopped state.</ul> <ul>HDFS, Zookeeper, and Sentry services should in Running state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> <p> Available since API v30.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Solr service name.
try {
    ApiCommand result = apiInstance.solrMigrateSentryPrivilegesCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#solrMigrateSentryPrivilegesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="solrReinitializeStateForUpgradeCommand"></a>
# **solrReinitializeStateForUpgradeCommand**
> ApiCommand solrReinitializeStateForUpgradeCommand(clusterName, serviceName)

Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state.

Reinitializes the Solr state by clearing the Solr HDFS data directory, the Solr data directory, and the Zookeeper state. <p> Note: <li> <ul>Solr service should be in Stopped state.</ul> <ul>HDFS and Zookeeper services should in Running state.</ul> <ul>This is only needed for upgrading to CDH6.0.</ul> <p> Available since API v30.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Solr service name.
try {
    ApiCommand result = apiInstance.solrReinitializeStateForUpgradeCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#solrReinitializeStateForUpgradeCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="solrValidateMetadataCommand"></a>
# **solrValidateMetadataCommand**
> ApiCommand solrValidateMetadataCommand(clusterName, serviceName)

Validates Solr metadata and configurations.

Validates Solr metadata and configurations. <p> Note: This is only needed for upgrading to CDH6.0. <p> Available since API v30.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | A String representing the Solr service name.
try {
    ApiCommand result = apiInstance.solrValidateMetadataCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#solrValidateMetadataCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| A String representing the Solr service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="sqoopCreateDatabaseTablesCommand"></a>
# **sqoopCreateDatabaseTablesCommand**
> ApiCommand sqoopCreateDatabaseTablesCommand(clusterName, serviceName)

Create the Sqoop2 Server Database tables.

Create the Sqoop2 Server Database tables. <p> This command is to be run whenever a new database has been specified. Will do nothing if tables already exist. Will not perform an upgrade. Only available when Sqoop2 Server is stopped. <p> Available since API v10.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Sentry service on which to run the command.
try {
    ApiCommand result = apiInstance.sqoopCreateDatabaseTablesCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#sqoopCreateDatabaseTablesCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Sentry service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="sqoopUpgradeDbCommand"></a>
# **sqoopUpgradeDbCommand**
> ApiCommand sqoopUpgradeDbCommand(clusterName, serviceName)

Upgrade Sqoop Database schema as part of a major version upgrade.

Upgrade Sqoop Database schema as part of a major version upgrade. <p/> When doing a major version upgrade for Sqoop, it is necessary to upgrade the schema of its database before Sqoop can run successfully. <p/> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The Sqoop service name.
try {
    ApiCommand result = apiInstance.sqoopUpgradeDbCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#sqoopUpgradeDbCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The Sqoop service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="startCommand"></a>
# **startCommand**
> ApiCommand startCommand(clusterName, serviceName)

Start the service.

Start the service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to start.
try {
    ApiCommand result = apiInstance.startCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#startCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to start. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stopCommand"></a>
# **stopCommand**
> ApiCommand stopCommand(clusterName, serviceName)

Stop the service.

Stop the service.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to stop.
try {
    ApiCommand result = apiInstance.stopCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#stopCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to stop. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="switchToMr2"></a>
# **switchToMr2**
> ApiCommand switchToMr2(clusterName, serviceName)

Change the cluster to use MR2 instead of MR1.

Change the cluster to use MR2 instead of MR1. Services will be restarted. <p> Will perform the following steps: <ul> <li>Update all services that depend on MapReduce to instead depend on Yarn. </li> <li>Stop MapReduce</li> <li>Start Yarn (MR2 Included)</li> <li>Deploy Yarn (MR2) Client Configuration</li> </ul> <p> Available since API v6.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | Name of the Yarn service on which to run the command.
try {
    ApiCommand result = apiInstance.switchToMr2(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#switchToMr2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| Name of the Yarn service on which to run the command. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateService"></a>
# **updateService**
> ApiService updateService(clusterName, serviceName, body)

Updates service information.

Updates service information. <p/> This method will update only writable fields of the service information. Currently this only includes the service display name. <p/> Available since API v3.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service name.
ApiService body = new ApiService(); // ApiService | Updated service information.
try {
    ApiService result = apiInstance.updateService(clusterName, serviceName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#updateService");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service name. |
 **body** | [**ApiService**](ApiService.md)| Updated service information. | [optional]

### Return type

[**ApiService**](ApiService.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateServiceConfig"></a>
# **updateServiceConfig**
> ApiServiceConfig updateServiceConfig(clusterName, serviceName, message, body)

Updates the service configuration with the given values.

Updates the service configuration with the given values. <p> If a value is set in the given configuration, it will be added to the service's configuration, replacing any existing entries. If a value is unset (its value is null), the existing configuration for the attribute will be erased, if any. <p> Attributes that are not listed in the input will maintain their current values in the configuration.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to modify.
String message = "message_example"; // String | Optional message describing the changes.
ApiServiceConfig body = new ApiServiceConfig(); // ApiServiceConfig | Configuration changes.
try {
    ApiServiceConfig result = apiInstance.updateServiceConfig(clusterName, serviceName, message, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#updateServiceConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to modify. |
 **message** | **String**| Optional message describing the changes. | [optional]
 **body** | [**ApiServiceConfig**](ApiServiceConfig.md)| Configuration changes. | [optional]

### Return type

[**ApiServiceConfig**](ApiServiceConfig.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="yarnFormatStateStore"></a>
# **yarnFormatStateStore**
> ApiCommand yarnFormatStateStore(clusterName, serviceName)

Formats the state store in ZooKeeper used for Resource Manager High Availability.

Formats the state store in ZooKeeper used for Resource Manager High Availability. Typically used while moving from non-secure to secure cluster or vice-versa. <p> Available since API v8.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The YARN service name.
try {
    ApiCommand result = apiInstance.yarnFormatStateStore(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#yarnFormatStateStore");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The YARN service name. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="zooKeeperCleanupCommand"></a>
# **zooKeeperCleanupCommand**
> ApiCommand zooKeeperCleanupCommand(clusterName, serviceName)

Clean up all running server instances of a ZooKeeper service.

Clean up all running server instances of a ZooKeeper service. <p> This command removes snapshots and transaction log files kept by ZooKeeper for backup purposes. Refer to the ZooKeeper documentation for more details.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to start.
try {
    ApiCommand result = apiInstance.zooKeeperCleanupCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#zooKeeperCleanupCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to start. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="zooKeeperInitCommand"></a>
# **zooKeeperInitCommand**
> ApiCommand zooKeeperInitCommand(clusterName, serviceName)

Initializes all the server instances of a ZooKeeper service.

Initializes all the server instances of a ZooKeeper service. <p> ZooKeeper server roles need to be initialized before they can be used.

### Example
```java
// Import classes:
//import com.cloudera.api.swagger.client.ApiClient;
//import com.cloudera.api.swagger.client.ApiException;
//import com.cloudera.api.swagger.client.Configuration;
//import com.cloudera.api.swagger.client.auth.*;
//import com.cloudera.api.swagger.ServicesResourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basic
HttpBasicAuth basic = (HttpBasicAuth) defaultClient.getAuthentication("basic");
basic.setUsername("YOUR USERNAME");
basic.setPassword("YOUR PASSWORD");

ServicesResourceApi apiInstance = new ServicesResourceApi();
String clusterName = "clusterName_example"; // String | 
String serviceName = "serviceName_example"; // String | The service to start.
try {
    ApiCommand result = apiInstance.zooKeeperInitCommand(clusterName, serviceName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServicesResourceApi#zooKeeperInitCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clusterName** | **String**|  |
 **serviceName** | **String**| The service to start. |

### Return type

[**ApiCommand**](ApiCommand.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

