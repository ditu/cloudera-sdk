
# ApiGenerateHostCertsArguments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sshPort** | [**BigDecimal**](BigDecimal.md) | SSH port. If unset, defaults to 22. |  [optional]
**userName** | **String** | The username used to authenticate with the hosts. Root access to your hosts is required to install Cloudera packages. The installer will connect to your hosts via SSH and log in either directly as root or as another user with password-less sudo privileges to become root. |  [optional]
**password** | **String** | The password used to authenticate with the hosts. Specify either this or a private key. For password-less login, use an empty string as password. |  [optional]
**privateKey** | **String** | The private key to authenticate with the hosts. Specify either this or a password. &lt;br&gt; The private key, if specified, needs to be a standard PEM-encoded key as a single string, with all line breaks replaced with the line-feed control character &#39;\\n&#39;. &lt;br&gt; A value will typically look like the following string: &lt;br&gt; -----BEGIN RSA PRIVATE KEY-----\\n[base-64 encoded key]\\n-----END RSA PRIVATE KEY----- &lt;br&gt; |  [optional]
**passphrase** | **String** | The passphrase associated with the private key used to authenticate with the hosts (optional). |  [optional]



