
# ApiClusterRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clusterName** | **String** | The name of the cluster, which uniquely identifies it in a CM installation. |  [optional]
**displayName** | **String** | The display name of the cluster. This is available from v30. |  [optional]



